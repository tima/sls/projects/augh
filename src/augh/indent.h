
/* Usage : Indentation

This header defines some structures and functions to help with output indentation.

*/

#ifndef _INDENT_H_
#define _INDENT_H_

#include <stdio.h>


//================================================
// Structures
//================================================

typedef struct {
	char      c;  // The character to print. Usually '\t' or ' '.
	unsigned  n;  // Number of characters per level
	unsigned  l;  // Level ID
} indent_t;


//================================================
// Functions
//================================================

static inline void Indent_SetLevel(indent_t* dest, unsigned level) {
	dest->l = level;
}
static inline void Indent_IncLevel(indent_t* dest, unsigned levels) {
	dest->l += levels;
}
static inline void Indent_SubLevel(indent_t* dest, unsigned levels) {
	if(levels>dest->l) dest->l = 0;
	else dest->l -= levels;
}

static inline void Indent_Init(indent_t* dest, char c, unsigned n, unsigned l) {
	dest->c = c;
	dest->n = n;
	dest->l = l;
}
static inline void Indent_InitEmpty(indent_t* dest) {
	dest->c = '\0';
	dest->n = 0;
	dest->l = 0;
}
static inline indent_t Indent_Make(char c, unsigned n, unsigned l) {
	indent_t indent;
	Indent_Init(&indent, c, n, l);
	return indent;
}

static inline void Indent_Copy(indent_t* dest, indent_t* src) {
	if(src==NULL) Indent_InitEmpty(dest);
	else *dest = *src;
}
static inline void Indent_Copy_AddLevels(indent_t* dest, indent_t* src, unsigned levels) {
	if(src==NULL) Indent_InitEmpty(dest);
	else {
		*dest = *src;
		dest->l += levels;
	}
}

static inline indent_t Indent_Make_AddLevels(indent_t* src, unsigned levels) {
	indent_t indent;
	Indent_Copy_AddLevels(&indent, src, levels);
	return indent;
}

static inline indent_t Indent_Make_IncLevel(indent_t* src) {
	indent_t indent;
	Indent_Copy_AddLevels(&indent, src, 1);
	return indent;
}

// Print the indentation

static inline void Indent_FPrint(FILE* F, indent_t* indent) {
	for(unsigned l=0; l<indent->l; l++) for(unsigned n=0; n<indent->n; n++) fputc(indent->c, F);
}
static inline void Indent_FPrint_CheckNull(FILE* F, indent_t* indent) {
	if(indent==NULL) return;
	for(unsigned l=0; l<indent->l; l++) for(unsigned n=0; n<indent->n; n++) fputc(indent->c, F);
}
static inline void Indent_FPrint_AddLevel(FILE* F, indent_t* indent, unsigned level) {
	for(unsigned l=0; l<indent->l + level; l++) for(unsigned n=0; n<indent->n; n++) fputc(indent->c, F);
}
static inline void Indent_Print(indent_t* indent) {
	Indent_FPrint(stdout, indent);
}
static inline void Indent_Print_CheckNull(indent_t* indent) {
	Indent_FPrint_CheckNull(stdout, indent);
}
static inline void Indent_Print_AddLevel(indent_t* indent, unsigned level) {
	Indent_FPrint_AddLevel(stdout, indent, level);
}

#define findentprintf(f, i, ...) do { Indent_FPrint_CheckNull(f, i); fprintf(f, __VA_ARGS__); } while(0)
#define indentprintf(i, ...)     do { Indent_Print_CheckNull(i); printf(__VA_ARGS__); } while(0)
#define findentprint(f, i)       do { Indent_FPrint_CheckNull(f, i); } while(0)
#define indentprint(i)           do { Indent_Print_CheckNull(i); } while(0)

#define findentincprintf(f, i, inc, ...) do { if(i!=NULL) Indent_FPrint_AddLevel(f, i, inc); fprintf(f, __VA_ARGS__); } while(0)
#define indentincprintf(i, inc, ...)     do { if(i!=NULL) Indent_Print_AddLevel(i, inc); printf(__VA_ARGS__); } while(0)
#define findentincprint(f, i, inc)       do { if(i!=NULL) Indent_FPrint_AddLevel(f, i, inc); } while(0)
#define indentincprint(i, inc)           do { if(i!=NULL) Indent_Print_AddLevel(i, inc); } while(0)

// Other useful functions with tabs only

static inline void Indent_FPrintTabs(FILE* F, unsigned nb) {
	for(unsigned i=0; i<nb; i++) fputc('\t', F);
}
static inline void Indent_PrintTabs(unsigned nb) {
	for(unsigned i=0; i<nb; i++) putchar('\t');
}

#define ftabprintf(f, n, ...) do { Indent_FPrintTabs(f, n); fprintf(f, __VA_ARGS__); } while(0)
#define tabprintf(n, ...)     do { Indent_PrintTabs(n); printf(__VA_ARGS__); } while(0)
#define ftabprint(f, n)       do { Indent_FPrintTabs(f, n); } while(0)
#define tabprint(n)           do { Indent_PrintTabs(n); } while(0)



#endif  // _INDENT_H_

