/****************************
 * AVL Balanced binary tree *
 ****************************
 *
 * Copyright: TIMA Laboratory - SLS Team
 * Authors : Damien Hedde, Adrien Prost-Boucle
 *
 *
 * Mandatory definitions :
 *
 * AVL_prefix      Prefix of every type/function names in this file.
 *                 in this file every name is 'AVL_somename', 'AVL_' will
 *                 be replaced (by the preprocessor) by AVL_prefix.
 *                 (example: with "#define AVL_prefix myavl", the node type
 *                 will be myavl_node_t)
 * AVL_data_t      Type of the data stored in the tree.
 * AVL_key_t       Type of the key used to sort the tree.
 *
 * Other optional definitions :
 *
 * AVL_key_from_data
 *                 Defines how to get the sorting key from the data.
 *                 Example : #define AVL_key_from_data(d) (d)
 * AVL_key_from_pdata
 *                 When the data is a big structure, it can be better to use a pointer.
 *                 Example : #define AVL_key_from_pdata(d) (d)->my_field
 *
 * Main types :
 *
 * AVL_tree_t      The tree type (ie: the root)
 * AVL_node_t      The node containing the key and data
 * AVL_pool_t      Pool of nodes
 *
 * Note: trees and pools are not allocated by this code and have to be done
 * in user code.
 *
 * Main functions:
 *
 * void AVL_init(AVL_tree_t *tree)
 *                 Initialize the 'tree' structure. 'tree' must be non-null.
 *                 If AVL_DEFAULT_POOL is set, the default pool is used.
 * void AVL_set_pool(AVL_tree_t *tree, AVL_pool_t *pool)
 *                 Set or change the pool the tree is associated to. 'tree' must be non-null.
 *                 Never use this function after initialization time.
 *                 'pool' can be NULL, in that case the tree will have no pool associated.
 * void AVL_reset(AVL_tree_t *tree)
 *                 tree != NULL
 *                 remove all nodes from the tree
 * bool AVL_find(AVL_tree_t *tree, AVL_key_t key, AVL_node_t **node)
 *                 tree != NULL, node != NULL
 *                 Find a node with the given 'key'.
 *                 Returns true if one is found.
 *                 *node is either set to the found node or to the
 *                 previous/next node in key order.
 * bool AVL_add(AVL_tree_t *tree, AVL_key_t key, AVL_node_t **node)
 *                 tree != NULL, node != NULL
 *                 Add (if the key is not already in the tree) a new node.
 *                 *node have to be either NULL, or an ancester of the
 *                 existing(/previous/next if not already existing) node.
 *                 Note: using the result of find with the same key is valid.
 *                 Returns true if a node is added.
 *                 *node is set to the existing/new node.
 * void AVL_rem(AVL_tree_t *tree, AVL_node_t* node)
 *                 tree != NULL, node != NULL
 *                 Remove the 'node' from the 'tree'.
 * AVL_node_t *AVL_first(AVL_tree_t *tree)
 * AVL_node_t *AVL_last(AVL_tree_t *tree)
 *                 tree != NULL
 *                 Returns the first/last node in key order.
 *                 Returns NULL if the tree is empty.
 * AVL_node_t *AVL_prev(AVL_tree_t *tree, AVl_node_t *node)
 * AVL_node_t *AVL_next(AVL_tree_t *tree, AVl_node_t *node)
 *                 tree != NULL, node != NULL
 *                 Returns the prev/next node of 'node' in key order.
 *                 Returns NULL if there is no such node.
 * AVL_node_t *AVL_dup(AVL_tree_t *tree, const AVl_tree_t *srctree)
 *                 _tree_ should be intialized
 *                 duplicate _srctree_ into _tree_ (identical tree (with new node), key and data are copied)
 *
 * IMPORTANT NOTE : Many other functions are not yet documented here.
 *
 * Pool parameters:
 *
 * AVL_POOL_ARRAY_SIZE
 *                 Number of nodes allocated when we need one.
 *                 Default is 100.
 *
 * Pool functions:
 *
 * void AVL_pool_init (AVL_pool_t *pool)
 *                 pool != NULL
 *                 Initialize a pool structure.
 * void AVL_pool_term (AVL_pool_t *pool)
 *                 pool != NULL
 *                 Free all elements allocated in this pool.
 *
 * Additional parameters:
 *
 * AVL_LINKED      If defined, the nodes are linked in key and reverse order,
 *                 allowing to fasten the walk through the nodes.
 *
 * AVL_DEFAULT_POOL
 *                 Set a default pool for all newly created trees.
 *                 This header declares the pool, but does not instanciate it.
 *                 That pool must be instanciated and initialize it separately.
 *
 * AVL_ALLOC       Use a custom memory allocation function. Default function : malloc(), from stdlib.h..
 *                 Example : #define AVL_ALLOC(size) calloc(1, size)
 *                 Example : #define AVL_ALLOC(size) my_alloc_func(my_data, size)
 *
 * AVL_FREE        Similarly, use a custom free function. Default function : free(), from stdlib.h.
 *
 */


// Include headers

#include <stdbool.h>

#ifdef AVL_DEBUG
	#include <assert.h>
	#define AVL_PRINT
#endif

#ifdef AVL_PRINT
	#include <stdio.h>
#endif

#if !defined(AVL_ALLOC) || !defined(AVL_FREE)
	#include <stdlib.h>
#endif

// Standard checks

#ifndef AVL_prefix
	#error "AVL_prefix is required."
#endif

#ifndef AVL_data_t
	#error "AVL_data_t is required."
#endif

#ifndef AVL_key_t
	#error "AVL_key_t is required."
#endif

#if defined(AVL_key_from_data) && defined(AVL_key_from_pdata)
	#error "Only one way to get the sorting key must be used."
#endif

#if defined(AVL_SHARED_INST) && defined(AVL_SHARED_DECL)
	#undef AVL_SHARED_DECL
#endif

// Internal definitions

#define AVL_CONCAT(x,y) x ## _ ## y
#define AVL_XCONCAT(x,y) AVL_CONCAT(x,y)

#define AVL_side_t  AVL_XCONCAT(AVL_prefix, side_t)
#define AVL_LEFT    AVL_XCONCAT(AVL_prefix, LEFT)
#define AVL_RIGHT   AVL_XCONCAT(AVL_prefix, RIGHT)

#define AVL_tree_t  AVL_XCONCAT(AVL_prefix, tree_t)
#define AVL_node_t  AVL_XCONCAT(AVL_prefix, node_t)
#define AVL_pool_t  AVL_XCONCAT(AVL_prefix, pool_t)
#define AVL_array_t AVL_XCONCAT(AVL_prefix, array_t)

#define AVL_pool_init     AVL_XCONCAT(AVL_prefix, pool_init)
#define AVL_pool_clear    AVL_XCONCAT(AVL_prefix, pool_clear)
#define AVL_pool_alloc    AVL_XCONCAT(AVL_prefix, pool_alloc)
#define AVL_pool_free     AVL_XCONCAT(AVL_prefix, pool_free)
#define AVL_pool_nodesalloc  AVL_XCONCAT(AVL_prefix, pool_nodesalloc)

#define AVL_node_get_key  AVL_XCONCAT(AVL_prefix, node_get_key)
#define AVL_node_set_key  AVL_XCONCAT(AVL_prefix, node_set_key)

#define AVL_node_create   AVL_XCONCAT(AVL_prefix, node_create)
#define AVL_node_delete   AVL_XCONCAT(AVL_prefix, node_delete)
#define AVL_balance       AVL_XCONCAT(AVL_prefix, balance)
#define AVL_rot           AVL_XCONCAT(AVL_prefix, rot)

#define AVL_print         AVL_XCONCAT(AVL_prefix, print)
#define AVL_print_r       AVL_XCONCAT(AVL_prefix, print_r)
#define AVL_check_tree    AVL_XCONCAT(AVL_prefix, check_tree)
#define AVL_check_subtree AVL_XCONCAT(AVL_prefix, check_subtree)

#define AVL_init        AVL_XCONCAT(AVL_prefix, init)
#define AVL_set_pool    AVL_XCONCAT(AVL_prefix, set_pool)
#define AVL_reset       AVL_XCONCAT(AVL_prefix, reset)
#define AVL_reset_cb    AVL_XCONCAT(AVL_prefix, reset_cb)
#define AVL_root        AVL_XCONCAT(AVL_prefix, root)
#define AVL_root_data   AVL_XCONCAT(AVL_prefix, root_data)
#define AVL_first       AVL_XCONCAT(AVL_prefix, first)
#define AVL_last        AVL_XCONCAT(AVL_prefix, last)
#define AVL_deepest     AVL_XCONCAT(AVL_prefix, deepest)
#define AVL_deepest_rem       AVL_XCONCAT(AVL_prefix, deepest_rem)
#define AVL_deepest_data      AVL_XCONCAT(AVL_prefix, deepest_data)
#define AVL_deepest_data_rem  AVL_XCONCAT(AVL_prefix, deepest_data_rem)
#define AVL_prev        AVL_XCONCAT(AVL_prefix, prev)
#define AVL_next        AVL_XCONCAT(AVL_prefix, next)
#define AVL_find        AVL_XCONCAT(AVL_prefix, find)
#define AVL_add         AVL_XCONCAT(AVL_prefix, add)
#define AVL_rem         AVL_XCONCAT(AVL_prefix, rem)

#define AVL_dup_subtree   AVL_XCONCAT(AVL_prefix, dup_subtree)
#define AVL_dup         AVL_XCONCAT(AVL_prefix, dup)

#ifdef AVL_VERBOSE
	#define AVL_debug(fmt, ...) fprintf(stderr, fmt, ##__VA_ARGS__)
#else
	#define AVL_debug(fmt, ...)
#endif

#ifndef AVL_PRINT_OUTPUT
	#ifdef AVL_DEBUG
		#define AVL_PRINT_OUTPUT stderr
	#else
		#define AVL_PRINT_OUTPUT stdout
	#endif
#endif

#ifndef AVL_ALLOC
	#define AVL_ALLOC malloc
#endif
#ifndef AVL_FREE
	#define AVL_FREE free
#endif

// AVL_FUNC_ATTR macro contains the attributes of the share-able functions
// All other functions are static inline
#if defined(AVL_SHARED_INST) || defined(AVL_SHARED_DECL)
	#define AVL_FUNC_ATTR
#else
	#define AVL_FUNC_ATTR static __attribute((__unused__))
#endif


//======================================================================
// Definition of structures
//======================================================================

#ifndef AVL_SHARED_INST

typedef enum AVL_side_t {
	AVL_LEFT = 0,
	AVL_RIGHT = 1
} AVL_side_t;

typedef struct AVL_node_t AVL_node_t;
struct AVL_node_t {
	AVL_data_t       data;

	#if !defined(AVL_key_from_data) && !defined(AVL_key_from_pdata)
	AVL_key_t        key;
	#endif

	AVL_node_t      *father;
	AVL_node_t      *child[2]; // 0->child[AVL_LEFT] node 1->child[AVL_RIGHT] node
	signed char      balance;  // balance of this subtree (right - left in number of nodes)
	enum AVL_side_t  side:8;   // side of this node in father node

	#ifdef AVL_LINKED
	AVL_node_t      *neighbor[2];
	#endif
};

#ifndef AVL_POOL_ARRAY_SIZE
	#define AVL_POOL_ARRAY_SIZE 100
#endif

typedef struct AVL_array_t AVL_array_t;
struct AVL_array_t {
	AVL_array_t *next;
	AVL_node_t   nodes[AVL_POOL_ARRAY_SIZE];
};
typedef struct AVL_pool_t {
	AVL_node_t  *list;  // linked list (using father field) of free node
	AVL_array_t *array; // linked list of allocated array
	unsigned int nbnode;
} AVL_pool_t;

typedef struct AVL_tree_t {
	AVL_node_t   *root;

	#ifdef AVL_LINKED
	AVL_node_t   *first;
	AVL_node_t   *last;
	#endif

	#ifndef AVL_POOL_INSTANCE
	AVL_pool_t   *pool;
	#endif

	#ifdef AVL_STAT
	unsigned int  nbnode;
	#endif

} AVL_tree_t ;

#endif  // AVL_SHARED_DECL


//======================================================================
// Functions
//======================================================================

#ifndef AVL_SHARED_INST

// Get and set the key value(s) from the node structure
static inline AVL_key_t AVL_node_get_key(const AVL_node_t* node) {
	#ifdef AVL_key_from_data
	return AVL_key_from_data(node->data);
	#endif
	#ifdef AVL_key_from_pdata
	return AVL_key_from_pdata(&node->data);
	#endif
	#if !defined(AVL_key_from_data) && !defined(AVL_key_from_pdata)
	return node->key;
	#endif
}
static inline void AVL_node_set_key(AVL_node_t* node, AVL_key_t key) {
	#if !defined(AVL_key_from_data) && !defined(AVL_key_from_pdata)
	node->key = key;
	#endif
}

#endif  // not AVL_SHARED_INST

#ifdef AVL_PRINT

// Print a subtree to the given depth
AVL_FUNC_ATTR int AVL_print_r (const AVL_node_t *node, unsigned int depth,
      const AVL_node_t *father, enum AVL_side_t side, int cur)
#ifdef AVL_SHARED_DECL
;
#else
{
   int res = 0;
   if (node && depth > 0)
      res += AVL_print_r(node->child[AVL_LEFT], depth - 1, node, AVL_LEFT, cur + 1);

   {
      char buffer[2*cur+2];
      char bufr[2*cur+2];
      char bufl[2*cur+2];
      for (int i = 0; i < 2*cur; i += 1) {
         buffer[i] = ' ';
         bufr[i] = ' ';
         bufl[i] = ' ';
      }
      buffer[2*cur] = cur?'+':'$';
      buffer[2*cur+1] = '\0';
      bufr[2*cur] = '\n';
      bufr[2*cur+1] = '\0';
      bufl[2*cur] = '\n';
      bufl[2*cur+1] = '\0';
      if (cur > 0) {
         buffer[2*cur - 2] = '+';
         buffer[2*cur - 1] = '-';
         if (side == AVL_LEFT)
            bufr[2*cur - 2] = '/';
         else
            bufl[2*cur - 2] = '\\';
      }
      for (int i = cur - 2; i >= 0; i -= 1) {
         if (father->side != side) {
            buffer[2*i] = (father->side==AVL_LEFT)?'/':'\\';
            bufr[2*i]   = (father->side==AVL_LEFT)?'/':'\\';
            bufl[2*i]   = (father->side==AVL_LEFT)?'/':'\\';
         }
         side = father->side;
         father = father->father;
      }
      if (node && depth) {
         res += 1;
         bufr[0] = '\0';
         bufl[0] = '\0';
      }
      fprintf(AVL_PRINT_OUTPUT, "%s%s (", bufl, buffer);
      if (node) {
         fprintf(AVL_PRINT_OUTPUT, "%p[0x%llx]", node, (long long) AVL_node_get_key(node));
#if defined(AVL_PRINT_DATA)
         {
            char str[11];
            AVL_PRINT_DATA(&node->data, str);
            str[10] = '\0';
            fprintf(AVL_PRINT_OUTPUT, ", Data=%s", str);
         }
#endif
         fprintf(AVL_PRINT_OUTPUT, ", Bal=%d)", node->balance);
      } else
         fprintf(AVL_PRINT_OUTPUT, "null)");
      fprintf(AVL_PRINT_OUTPUT, "\n%s", bufr);
   }

   if (node && depth > 0)
         res += AVL_print_r(node->child[AVL_RIGHT], depth - 1, node, AVL_RIGHT, cur + 1);

   return res;
}
#endif  // not AVL_SHARED_DECL

#ifdef AVL_DEBUG
AVL_FUNC_ATTR AVL_check_subtree (const AVL_node_t *node, bool print);
#endif

AVL_FUNC_ATTR void AVL_print (const AVL_node_t *node, int depth)
#ifdef AVL_SHARED_DECL
;
#else
{
	int cnt = AVL_print_r(node, depth, NULL, 0, 0);
	fprintf(AVL_PRINT_OUTPUT,"%d nodes diplayed\n", cnt);
	fflush(AVL_PRINT_OUTPUT);
	#ifdef AVL_DEBUG
	AVL_check_subtree(node, false);
	#endif
}
#endif  // not AVL_SHARED_DECL

#endif /* AVL_PRINT */

// Check if a tree/subtree is correct
#ifdef AVL_DEBUG

#define AVL_CHECK(cond) \
	do { \
		if (!(cond)) { \
			ok = false; \
			fprintf(stderr, "failed{node %p [0x%llx]}: (line %d)" #cond "\n", \
				node, \
				(unsigned long long) ((node != NULL)?AVL_node_get_key(node):0), \
				__LINE__); \
		} \
	} while (0)

AVL_FUNC_ATTR int AVL_check_subtree (const AVL_node_t *node, bool print)
#ifdef AVL_SHARED_DECL
;
#else
{
   bool ok = true;
   if (node == NULL)
      return 0;
   if (node->father) {
      AVL_CHECK(node->father->child[node->side] == node);
   }

   if (node->child[AVL_LEFT]) {
      AVL_CHECK(node->child[AVL_LEFT]->side == AVL_LEFT);
      AVL_CHECK(AVL_node_get_key(node->child[AVL_LEFT]) < AVL_node_get_key(node));
   }
   if (node->child[AVL_RIGHT]) {
      AVL_CHECK(node->child[AVL_RIGHT]->side == AVL_RIGHT);
      AVL_CHECK(AVL_node_get_key(node->child[AVL_RIGHT]) > AVL_node_get_key(node));
   }

   int r = AVL_check_subtree(node->child[AVL_RIGHT], print);
   int l = AVL_check_subtree(node->child[AVL_LEFT], print);
   if (r < 0 || l < 0)
      return -1;

   AVL_CHECK(node->balance == (r - l));
   AVL_CHECK(node->balance * node->balance <= 1);

   if (!ok) {
      if (print)
         AVL_print(node, 10);
      return -1;
   }
   return 1 + ((r>l)?r:l);
}
#endif  // not AVL_SHARED_DECL

AVL_FUNC_ATTR int AVL_check_tree (const AVL_tree_t *tree)
#ifdef AVL_SHARED_DECL
;
#else
{
   bool ok = true;
   if (tree->root) {
      AVL_node_t *node = tree->root;
      AVL_CHECK(tree->root->father == NULL);
   }

   if(AVL_check_subtree(tree->root, true) < 0 || !ok)
      return -1;

#ifdef AVL_LINKED
   if (tree->root) {
      AVL_node_t *node = tree->root;
      while (node->child[AVL_LEFT])
         node = node->child[AVL_LEFT];
      AVL_CHECK(node->neighbor[AVL_LEFT] == NULL);
      do {
         if (node->neighbor[AVL_RIGHT] == NULL)
            break;
         AVL_CHECK(node->neighbor[AVL_RIGHT]->neighbor[AVL_LEFT] == node);
         AVL_CHECK(AVL_node_get_key(node) < AVL_node_get_key(node->neighbor[AVL_RIGHT]));
         node = node->neighbor[AVL_RIGHT];
      } while (1);
      AVL_CHECK(node->child[AVL_RIGHT] == NULL);
   }
#endif

   if (!ok) {
      AVL_print(tree->root, 16);
      return -1;
   }

   return 0;
}
#endif  // not AVL_SHARED_DECL

#endif  // AVL_DEBUG


//======================================================================
// Functions for the pool
//======================================================================

// Pool declaration
#ifdef AVL_DEFAULT_POOL
extern AVL_pool_t AVL_DEFAULT_POOL;
#endif

// Pool instance
#ifdef AVL_POOL_INSTANCE

#ifdef AVL_SHARED_INST
AVL_pool_t AVL_POOL_INSTANCE = {
 .list = NULL,
 .array = NULL,
 .nbnode = 0,
};
#endif

#ifdef AVL_SHARED_DECL
extern AVL_pool_t AVL_POOL_INSTANCE;
#endif

#if !defined(AVL_SHARED_INST) && !defined(AVL_SHARED_DECL)
static AVL_pool_t AVL_POOL_INSTANCE = {
 .list = NULL,
 .array = NULL,
 .nbnode = 0,
};
#endif

#endif  // AVL_POOL_INSTANCE

#ifndef AVL_SHARED_INST

// Initialize a pool
static inline void AVL_pool_init (AVL_pool_t *pool) {
	pool->list = NULL;
	pool->array = NULL;
	pool->nbnode = 0;
	return;
}

#endif // not AVL_SHARED_INST

// terminate a pool
AVL_FUNC_ATTR void AVL_pool_clear (AVL_pool_t *pool)
#ifdef AVL_SHARED_DECL
;
#else
{
	AVL_array_t *cur = pool->array;
	while (cur != NULL) {
		AVL_array_t *tmp = cur;
		cur = cur->next;
		AVL_FREE(tmp);
	}
}
#endif // not AVL_SHARED_DECL

// Allocate a pool
AVL_FUNC_ATTR AVL_pool_t * AVL_pool_alloc ()
#ifdef AVL_SHARED_DECL
;
#else
{
	AVL_pool_t *pool = AVL_ALLOC(sizeof(AVL_pool_t));
	AVL_pool_init(pool);
	return pool;
}
#endif // not AVL_SHARED_DECL

// Delete a pool
AVL_FUNC_ATTR void AVL_pool_free (AVL_pool_t *pool)
#ifdef AVL_SHARED_DECL
;
#else
{
	AVL_pool_clear(pool);
	AVL_FREE(pool);
}
#endif // not AVL_SHARED_DECL

// Add a node array to the pool
AVL_FUNC_ATTR void AVL_pool_nodesalloc (AVL_pool_t *pool)
#ifdef AVL_SHARED_DECL
;
#else
{
	AVL_array_t *array = AVL_ALLOC(sizeof(AVL_array_t));
	array->next = pool->array;
	pool->array = array;
	for (int i = 0; i < AVL_POOL_ARRAY_SIZE - 1; i += 1) {
		 array->nodes[i].father = &array->nodes[i+1];
	}
	array->nodes[AVL_POOL_ARRAY_SIZE - 1].father = pool->list;
	pool->list = &array->nodes[0];
	pool->nbnode += AVL_POOL_ARRAY_SIZE;
}
#endif // not AVL_SHARED_DECL


//======================================================================
// Internal functions
//======================================================================

#ifndef AVL_SHARED_INST

// Create a new node
static inline AVL_node_t * AVL_node_create (AVL_tree_t *tree) {
	#ifdef AVL_STAT
	tree->nbnode += 1;
	#endif

	#ifdef AVL_POOL_INSTANCE

	if (AVL_POOL_INSTANCE.list == NULL) AVL_pool_nodesalloc(&AVL_POOL_INSTANCE);

	AVL_node_t *node = AVL_POOL_INSTANCE.list;
	AVL_POOL_INSTANCE.list = node->father;

	#else

	AVL_pool_t *pool = tree->pool;
	if (pool == NULL) return AVL_ALLOC(sizeof(AVL_node_t));
	if (pool->list == NULL) AVL_pool_nodesalloc(pool);

	AVL_node_t *node = pool->list;
	pool->list = node->father;

	#endif

	return node;
}

// Delete a node
static inline void AVL_node_delete (AVL_tree_t *tree, AVL_node_t *node) {
	#ifdef AVL_STAT
	tree->nbnode -= 1;
	#endif

	#ifdef AVL_POOL_INSTANCE
	node->father = AVL_POOL_INSTANCE.list;
	AVL_POOL_INSTANCE.list = node;
	#else
	AVL_pool_t *pool = tree->pool;
	if(pool!=NULL) {
		node->father = pool->list;
		pool->list = node;
	}
	else {
		AVL_FREE(node);
	}
	#endif

}

#endif  // AVL_SHARED_INST


//======================================================================
// Creation of trees
//======================================================================

#ifndef AVL_SHARED_INST

// Initialize a tree
static inline void AVL_init (AVL_tree_t *tree) {
	tree->root = NULL;
	#ifdef AVL_LINKED
	tree->last = NULL;
	tree->first = NULL;
	#endif

	#ifndef AVL_POOL_INSTANCE

	#if defined(AVL_DEFAULT_POOL) && !defined(AVL_DEBUG_MALLOC)
	tree->pool = &AVL_DEFAULT_POOL;
	#else
	tree->pool = NULL;
	#endif

	#endif  // AVL_POOL_INSTANCE

	#ifdef AVL_STAT
	tree->nbnode = 0;
	#endif
}

#endif  // not AVL_SHARED_INST


#ifndef AVL_POOL_INSTANCE
// Set a particular pool to a tree
static inline void AVL_set_pool (AVL_tree_t *tree, AVL_pool_t *pool) {
	tree->pool = pool;
}
#endif  // AVL_POOL_INSTANCE

// Reset a tree (empty it)
#define AVL_reset_callback_def(name) void(*name)(AVL_node_t *)

AVL_FUNC_ATTR void AVL_reset_cb (AVL_tree_t *tree, AVL_reset_callback_def(cb))
#ifdef AVL_SHARED_DECL
;
#else
{
	AVL_node_t *todo = tree->root;
	while (todo) {
		AVL_node_t *cur = todo;
		todo = cur->father;
		if (cur->child[AVL_LEFT]) {
			cur->child[AVL_LEFT]->father = todo;
			todo = cur->child[AVL_LEFT];
		}
		if (cur->child[AVL_RIGHT]) {
			cur->child[AVL_RIGHT]->father = todo;
			todo = cur->child[AVL_RIGHT];
		}
		if (cb != NULL)
			cb(cur);

		AVL_node_delete(tree, cur);
	}
	tree->root = NULL;
	#ifdef AVL_LINKED
	tree->last = NULL;
	tree->first = NULL;
	#endif
}
#endif // not AVL_SHARED_DECL

#ifndef AVL_SHARED_INST
static inline void AVL_reset (AVL_tree_t *tree) {
	AVL_reset_cb(tree, NULL);
}
#endif  // not AVL_SHARED_INST

#undef AVL_reset_callback_def

// AVL_root, AVL_root_data
#ifndef AVL_SHARED_INST
static inline AVL_node_t* AVL_root (const AVL_tree_t *tree) {
	return tree->root;
}
static inline AVL_data_t AVL_root_data (const AVL_tree_t *tree) {
	return tree->root->data;
}
#endif // not AVL_SHARED_INST

// AVL_first, AVL_last
#ifdef AVL_LINKED

#ifndef AVL_SHARED_INST

static inline AVL_node_t* AVL_first (const AVL_tree_t *tree) {
	return tree->first;
}
static inline AVL_node_t* AVL_last (const AVL_tree_t *tree) {
	return tree->last;
}

#endif // not AVL_SHARED_INST

#else

AVL_FUNC_ATTR AVL_node_t* AVL_first (const AVL_tree_t *tree)
#ifdef AVL_SHARED_DECL
;
#else
{
	AVL_node_t *node = tree->root;
	AVL_node_t *prev = NULL;
	while (node) {
		prev = node;
		node = node->child[AVL_LEFT];
	}
	return prev;
}
#endif // not AVL_SHARED_DECL

AVL_FUNC_ATTR AVL_node_t* AVL_last (const AVL_tree_t *tree)
#ifdef AVL_SHARED_DECL
;
#else
{
	AVL_node_t *node = tree->root;
	AVL_node_t *prev = NULL;
	while (node) {
		prev = node;
		node = node->child[AVL_RIGHT];
	}
	return prev;
}
#endif // not AVL_SHARED_DECL

#endif  // AVL_LINKED

// Find a node
// *node is set to the found node
// (or to a previous/next node in the tree when the search is not sucessful)
AVL_FUNC_ATTR bool AVL_find (const AVL_tree_t *tree, const AVL_key_t key, AVL_node_t** node)
#ifdef AVL_SHARED_DECL
;
#else
{
	*node = NULL;
	AVL_node_t *cur = tree->root;
	while (cur != NULL) {
		*node = cur;
		if (key < AVL_node_get_key(cur))
			cur = cur->child[AVL_LEFT];
		else if (key > AVL_node_get_key(cur))
			cur = cur->child[AVL_RIGHT];
		else
			return true;
	}
	return false;
}
#endif // not AVL_SHARED_DECL

// Find the previous node
#ifdef AVL_LINKED

#ifndef AVL_SHARED_INST

static inline AVL_node_t* AVL_prev (AVL_tree_t* tree __attribute__((__unused__)), AVL_node_t* node) {
	return node->neighbor[AVL_LEFT];
}
static inline AVL_node_t* AVL_next (AVL_tree_t* tree __attribute__((__unused__)), AVL_node_t* node) {
	return node->neighbor[AVL_RIGHT];
}

#endif // not AVL_SHARED_INST

#else

AVL_FUNC_ATTR AVL_node_t* AVL_prev (AVL_tree_t* tree __attribute__((__unused__)), AVL_node_t* node)
#ifdef AVL_SHARED_DECL
;
#else
{
	#ifdef DEBUG
	assert(node != NULL);
	#endif
	if (node->child[AVL_LEFT]) {
		node = node->child[AVL_LEFT];
		while (node->child[AVL_RIGHT])
			node = node->child[AVL_RIGHT];
		return node;
	}
	enum AVL_side_t side = node->side;
	node = node->father;
	while (node && side == AVL_LEFT) {
		side = node->side;
		node = node->father;
	}
	return node;
}
#endif // not AVL_SHARED_DECL

AVL_FUNC_ATTR AVL_node_t* AVL_next (AVL_tree_t* tree __attribute__((__unused__)), AVL_node_t* node)
#ifdef AVL_SHARED_DECL
;
#else
{
	#ifdef DEBUG
	assert(node != NULL);
	#endif
	if (node->child[AVL_RIGHT]) {
		node = node->child[AVL_RIGHT];
		while (node->child[AVL_LEFT])
			node = node->child[AVL_LEFT];
		return node;
	}
	enum AVL_side_t side = node->side;
	node = node->father;
	while (node && side == AVL_RIGHT) {
		side = node->side;
		node = node->father;
	}
	return node;
}
#endif // not AVL_SHARED_DECL

#endif  // AVL_LINKED

/*
 * rotate the root of the given subtree
 * return the new subtree root
 *
 * *** side == AVL_RIGHT
 *
 * the root must have a left child
 *
 *         Root             L
 *         / \             / \
 *        /   \           /   \
 *       L     R   ==>   LL    Root
 *      / \                   / \
 *     LL  LR                LR  R
 *
 *  *** side == AVL_LEFT
 * the root must have a right child
 *
 *         Root             R
 *         / \             / \
 *        /   \           /   \
 *       L     R   ==>  Root   RR
 *            / \       / \
 *           RL  RR    L   RL
 */
AVL_FUNC_ATTR AVL_node_t* AVL_rot(AVL_tree_t* tree, AVL_node_t* root, enum AVL_side_t side)
#ifdef AVL_SHARED_DECL
;
#else
{
	AVL_debug("    AVL_rot(%p, %p, %d)\n", tree, root, side);

	const enum AVL_side_t invside = 1 - side;
	// node is the new root

	AVL_node_t *node = root->child[invside];

	// set up father link of node
	node->father = root->father;
	node->side = root->side;
	if (node->father != NULL)
		node->father->child[node->side] = node;
	else
		tree->root = node;

	// move node->child[side] to root->child[invside]
	root->child[invside] = node->child[side];
	if (root->child[invside]) {
		root->child[invside]->father = root;
		root->child[invside]->side = invside;
	}

	// set up link between node and root
	node->child[side] = root;
	root->father = node;
	root->side = side;

	return node;
}
#endif  // not AVL_SHARED_DECL

/*
 * balance the tree
 * add tell if we'are adding or removing a node
 * childs of node have to be well balanced
 * AVL_balance must be called on the father of child add/del
 */
AVL_FUNC_ATTR void AVL_balance(AVL_tree_t* tree, AVL_node_t* node, bool add)
#ifdef AVL_SHARED_DECL
;
#else
{
   AVL_debug("  AVL_balance(%p, %p(0x%llx), %s)\n", tree, node, (unsigned long long) (node?AVL_node_get_key(node):0), add?"add":"rem");

   while (node) {
      AVL_debug("  -> balance(%p(0x%llx))\n", node, (unsigned long long) AVL_node_get_key(node));
      //AVL_print(node, 4);

      /*
       * diff will contains the variation of node depth
       * 0 means we can stop
       */
      int diff = 0;

      /*
       * the following will balance 'node' subtree (if needed) and
       * compute its depth variation
       */

      switch (node->balance) {
         case 0:
            /*
             * node was unbalanced and is now balanced.
             *
             * addition:
             *    the least deep child increased
             *
             * deletion:
             *    the deepest child decreased
             *    node's depth decrease
             */
            if (!add)
               diff = -1;
            break;

         case 1:
         case -1:
            /*
             *
             * addition:
             *    * depth increases (node was balanced before 'add')
             *
             * deletion:
             *    * depth remains unchanged (node was balanced before 'rm')
             */
            if (add)
               diff = 1;
            break;

         case -2:
            switch (node->child[AVL_LEFT]->balance) {
               case -1:
                  /*
                   *       N(n)(-2)              L(n-1)(0)
                   *         / \                    / \
                   *        /   \                  /   \
                   *       /     \                /     \
                   * L(n-1)(-1)  R(n-3)  ==>  LL(n-2)  N(n-2)(0)
                   *     / \                            / \
                   *    /   \                          /   \
                   *   /     \                        /     \
                   * LL(n-2)  LR(n-3)            LR(n-3)    R(n-3)
                   *
                   * addition:
                   *    L increased: subtree depth remains unchanged (n-1)
                   * deletion:
                   *    R decreased: subtree depth decreases from n to n-1
                   */
                  node = AVL_rot(tree, node, AVL_RIGHT);
                  node->balance = 0;
                  node->child[AVL_RIGHT]->balance = 0;
                  if (!add)
                     diff = -1;
                  break;
               case 0:
                  /*
                   *       N(n)(-2)               L(n)(1)
                   *         / \                    / \
                   *        /   \                  /   \
                   *       /     \                /     \
                   * L(n-1)(0)   R(n-3)  ==>  LL(n-2)  N(n-1)(-1)
                   *     / \                            / \
                   *    /   \                          /   \
                   *   /     \                        /     \
                   * LL(n-2)  LR(n-2)            LR(n-2)    R(n-3)
                   *
                   * addition:
                   *    L increased: subtree depth increases from n-1 to n
                   * deletion:
                   *    R decreased: subtree depth remains unchanged (n)
                   */
                  node = AVL_rot(tree, node, AVL_RIGHT);
                  node->balance = 1;
                  node->child[AVL_RIGHT]->balance = -1;
                  if (add)
                     diff = 1;
                  break;
               case 1:
                  /*
                   *     N(n)(-2)                N                     LR(n-1)(0)
                   *        / \                 / \                      /  \
                   *       /   \               /   \                    /    \
                   *      /     \             /     \                  /      \
                   * L(n-1)(+1)  R(n-3)  ==> LR      R     =>  L(n-2)(-1/0)  N(n-2)(0/+1)
                   *     / \                 / \                     / \        / \
                   *    /   \               /   \                   /   \      /   \
                   *   /     \             /     \                 /     \    /     \
                   * LL(n-3) LR(n-2)(?)   L      LRR           LL(n-3)   LRL LRR   R(n-3)
                   *          / \        / \                            (n-3/n-4)
                   *        LRL LRR     LL  LRL
                   *       (n-3/n-4)
                   *
                   * addition:
                   *    L increased: subtree depth reamins unchanged (n-1)
                   * deletion:
                   *    R decreased: subtree depth decreases from n to n-1
                   */
                  {
                     int bal = node->child[AVL_LEFT]->child[AVL_RIGHT]->balance;
                     AVL_rot(tree, node->child[AVL_LEFT], AVL_LEFT);
                     node = AVL_rot(tree, node, AVL_RIGHT);
                     node->balance = 0;
                     node->child[AVL_LEFT]->balance = 0 - ((bal*(bal+1)) >> 1);// 0/-1 -> 0, 1 -> -1
                     node->child[AVL_RIGHT]->balance = (bal*(bal-1)) >> 1;// 0/1 -> 0, -1 -> 1
                  }
                  if (!add)
                     diff = -1;
                  break;
               default://should not occur
#ifdef AVL_DEBUG
                  AVL_print(node, 10);
#endif
                  break;
            }
            break;

         case 2:
            switch (node->child[AVL_RIGHT]->balance) {
               case 1:
                  /*
                   *      N(n)(+2)                  N(n-1)(0)
                   *         / \                       / \
                   *        /   \                     /   \
                   *       /     \                   /     \
                   *    L(n-3)  R(n-1)(1)  ==>     (n-2)(0)  RR(n-2)
                   *              / \               / \
                   *             /   \             /   \
                   *            /     \           /     \
                   *         RL(n-3)  RR(n-2)  L(n-3)   RL(n-3)
                   *
                   * addition:
                   *    R increased: subtree depth remains unchanged (n-1)
                   * deletion:
                   *    L decreased: subtree depth decreases from n to n-1
                   */
                  node = AVL_rot(tree, node, AVL_LEFT);
                  node->balance = 0;
                  node->child[AVL_LEFT]->balance = 0;
                  if (!add)
                     diff = -1;
                  break;
               case 0:
                  /*
                   *      N(n)(+2)                    N(n)(-1)
                   *         / \                       / \
                   *        /   \                     /   \
                   *       /     \                   /     \
                   *    L(n-3)  R(n-1)(0)  ==>     (n-1)(1)  RR(n-2)
                   *              / \               / \
                   *             /   \             /   \
                   *            /     \           /     \
                   *         RL(n-2)  RR(n-2)  L(n-3)   RL(n-2)
                   *
                   * addition:
                   *    R increased: subtree depth increases from n-1 to n
                   * deletion:
                   *    L decreased: subtree remains unchanged (n)
                   */
                  node = AVL_rot(tree, node, AVL_LEFT);
                  node->balance = -1;
                  node->child[AVL_LEFT]->balance = 1;
                  if (add)
                     diff = 1;
                  break;
               case -1:
                  /*
                   *       N(n)(+2)                 N                        RL(n-1)(0)
                   *         / \                   / \                         / \
                   *        /   \                 /   \                       /   \
                   *       /     \               /     \                     /     \
                   *    L(n-3)  R(n-1)(-1) ==>  L       RL       ==>  N(n-2)(-1/0)  R(n-2)(0/+1)
                   *              / \                  / \               / \         / \
                   *             /   \                /   \             /   \       /   \
                   *            /     \              /     \           /     \     /     \
                   *      RL(n-2)(?)  RR(n-3)      RLL      R       L(n-3)   RLL RLR     RR(n-3)
                   *         / \                           / \              (n-3/n-4)
                   *       RLL RLR                       RLR  RR
                   *      (n-3/n-4)
                   *
                   * addition:
                   *    R increased: subtree depth remains unchanged (n-1)
                   * deletion:
                   *    L decreased: subtree depth decreases from n to n-1
                   */
                  {
                     int bal = node->child[AVL_RIGHT]->child[AVL_LEFT]->balance;
                     AVL_rot(tree, node->child[AVL_RIGHT], AVL_RIGHT);
                     node = AVL_rot(tree, node, AVL_LEFT);
                     node->balance = 0;
                     node->child[AVL_LEFT]->balance = 0 - ((bal*(bal+1)) >> 1);// 0/-1 -> 0, 1 -> -1
                     node->child[AVL_RIGHT]->balance = (bal*(bal-1)) >> 1;// 0/1 -> 0, -1 -> 1
                  }
                  if (!add)
                     diff = -1;
                  break;
               default://should not occur
#ifdef AVL_DEBUG
                  AVL_print(node, 10);
#endif
                  break;
            }
            break;

         default://should not occur
#ifdef AVL_DEBUG
            AVL_print(node, 10);
#endif
            break;
      }


      if (diff == 0) // depth of current subtree remains unchanged
         break;

      /*
       * update father balance
       */
      if (node->father) {
         node->father->balance += diff * ((node->side << 1) - 1); // += -1/0/+1 * -1(left)/+1(right)
      }

#ifdef AVL_DEBUG
      assert(AVL_check_subtree(node, true) >= 0);
#endif

      node = node->father;
   }
#ifdef AVL_DEBUG
   assert(AVL_check_tree(tree) >= 0);
#endif
}
#endif  // not AVL_SHARED_DECL

/*
 * add a node in 'tree' with the given 'key'
 * '*node' must be
 *    either NULL
 *    or the future previous/next node
 *    or an ancester of the future previous/next node.
 *    (the 'find' result is fine for example)
 *
 * return false if key is already in the tree, true if key is added
 * return in '*node' the added or existing node
 */
AVL_FUNC_ATTR bool AVL_add (AVL_tree_t *tree, const AVL_key_t key, AVL_node_t **node)
#ifdef AVL_SHARED_DECL
;
#else
{

   // Find a place to insert
   AVL_node_t *cur = tree->root;
   AVL_node_t *last = NULL;
   enum AVL_side_t side = 0;

   AVL_debug("AVL_add(%p, 0x%x, %p)\n", tree, key, *node);

   if (*node != NULL) {
      cur = *node;
      last = cur->father;
      side = cur->side;
   }
   while (cur != NULL) {
      if (key < AVL_node_get_key(cur))
         side = AVL_LEFT;
      else if (key > AVL_node_get_key(cur))
         side = AVL_RIGHT;
      else
         break;
      last = cur;
      cur = cur->child[side];
   }

   // The key is already there
   if (cur != NULL) {
      *node = cur;
      return false;
   }

   // creating node
   cur = AVL_node_create(tree);
   cur->father = last;
   cur->child[AVL_LEFT] = NULL;
   cur->child[AVL_RIGHT] = NULL;
   cur->balance  = 0;
   AVL_node_set_key(cur, (AVL_key_t)key);
   cur->side = side;
   *node = cur;
#ifdef AVL_LINKED
   cur->neighbor[1-side] = last;
#endif

   // The tree was empty
   if (last == NULL) {
#ifdef AVL_LINKED
      cur->neighbor[side] = NULL;
      tree->first = cur;
      tree->last = cur;
#endif
      tree->root = cur;
      return true;
   }

   // finish neighbor links
#ifdef AVL_LINKED
   cur->neighbor[side] = last->neighbor[side];
   last->neighbor[side] = cur;
   if (cur->neighbor[side])
      cur->neighbor[side]->neighbor[1-side] = cur;
   else {
      if (side == AVL_LEFT)
         tree->first = cur;
      else
         tree->last = cur;
   }
#endif

   // Add in tree and balance
   last->child[side] = cur;
   last->balance += (2 * side) - 1; // 0 -> -=1, 1 -> += 1

   AVL_balance(tree, last, true);

#ifdef AVL_DEBUG
   assert(AVL_check_tree(tree) >= 0);
#endif

   return true;
}
#endif  // AVL_SHARED_DECL

// Remove a node
AVL_FUNC_ATTR void AVL_rem (AVL_tree_t *tree, AVL_node_t *node)
#ifdef AVL_SHARED_DECL
;
#else
{

   AVL_node_t *bal = NULL;
   int diff = 0;

   AVL_debug("AVL_rem(%p, %p(%x))\n", tree, node, AVL_node_get_key(node));

   if (node->child[AVL_LEFT] != NULL || node->child[AVL_RIGHT] != NULL) {

      /*
       * find the nearest node 'repl' in the bigest subtree of node
       * this node is either a leaf or has one leaf child
       * since we will remove node we can replace node by repl
       * in the tree
       */
      enum AVL_side_t side;
      if (node->balance < 0)
         side = AVL_LEFT;
      else
         side = AVL_RIGHT;
      enum AVL_side_t invside = 1 - side;
      AVL_node_t *repl = node->child[side];
      // repl is not null, since node has at least one child
      // and we take a child in its deepest subtree
      while (repl->child[invside] != NULL)
         repl = repl->child[invside];

      /*
       * remove repl from the tree
       * and replace it by its child (if it has one)
       * (which is (and stay) a leaf)
       * note that 'repl->father' may be 'node'
       */
      repl->father->child[repl->side] = repl->child[side];
      if (repl->child[side]) {
         repl->child[side]->father = repl->father;
         repl->child[side]->side = repl->side;
      }
      repl->father->balance += 1 - (repl->side << 1);// 0 -> +=1, 1 -> -=1
      bal = (repl->father == node) ? repl : repl->father;

      /*
       * replace node by repl in the tree
       */
      repl->father = node->father;
      repl->child[AVL_LEFT] = node->child[AVL_LEFT];
      repl->child[AVL_RIGHT] = node->child[AVL_RIGHT];
      repl->balance = node->balance;
      repl->side = node->side;
      if (repl->child[AVL_LEFT])
         repl->child[AVL_LEFT]->father = repl;
      if (repl->child[AVL_RIGHT])
         repl->child[AVL_RIGHT]->father = repl;

      if (node->father)
         node->father->child[node->side] = repl;
      else
         tree->root = repl;

   } else {
      bal = node->father;
      if (node->father) {
         node->father->child[node->side] = NULL;
         node->father->balance += diff = 1 - (node->side << 1); // 0 -> +=1, 1 -> -=1
      } else {
         tree->root = NULL;
      }
   }

   /* remove neighbor links */
#ifdef AVL_LINKED
   if (node->neighbor[AVL_LEFT])
      node->neighbor[AVL_LEFT]->neighbor[AVL_RIGHT] = node->neighbor[AVL_RIGHT];
   else
      tree->first = node->neighbor[AVL_RIGHT];
   if (node->neighbor[AVL_RIGHT])
      node->neighbor[AVL_RIGHT]->neighbor[AVL_LEFT] = node->neighbor[AVL_LEFT];
   else
      tree->last = node->neighbor[AVL_LEFT];
#endif

   /* remove node and balance */
   AVL_node_delete(tree, node);
   AVL_balance(tree, bal, false);

   return;
}
#endif  // AVL_SHARED_DECL


AVL_FUNC_ATTR AVL_node_t* AVL_deepest (AVL_tree_t *tree)
#ifdef AVL_SHARED_DECL
;
#else
{
	AVL_node_t *node = tree->root;
	while (1) {
		if     (node->balance < 0) node = node->child[AVL_LEFT];
		else if(node->balance > 0) node = node->child[AVL_RIGHT];
		else if(node->child[AVL_LEFT]!=NULL) node = node->child[AVL_LEFT];
		else if(node->child[AVL_RIGHT]!=NULL) node = node->child[AVL_RIGHT];
		else break;
	}
	return node;
}
#endif // not AVL_SHARED_DECL

// AVL_deepest_data, AVL_deepest_rem, AVL_deepest_data_rem
#ifndef AVL_SHARED_INST
static inline AVL_data_t AVL_deepest_data (AVL_tree_t *tree) {
	AVL_node_t *node = AVL_deepest(tree);
	return node->data;
}
static inline void AVL_deepest_rem (AVL_tree_t *tree) {
	AVL_node_t *node = AVL_deepest(tree);
	// This is a very fast remove, because we know no tree balancing is needed
	AVL_rem(tree, node);
}
static inline AVL_data_t AVL_deepest_data_rem (AVL_tree_t *tree) {
	AVL_node_t *node = AVL_deepest(tree);
	AVL_data_t data = node->data;
	// This is a very fast remove, because we know no tree balancing is needed
	AVL_rem(tree, node);
	return data;
}
#endif  // not AVL_SHARED_INST


#define AVL_find_data        AVL_XCONCAT(AVL_prefix, find_data)
#define AVL_find_pdata       AVL_XCONCAT(AVL_prefix, find_pdata)
#define AVL_add_pdata        AVL_XCONCAT(AVL_prefix, add_pdata)
#define AVL_add_overwrite    AVL_XCONCAT(AVL_prefix, add_overwrite)
#define AVL_add_keep         AVL_XCONCAT(AVL_prefix, add_keep)

#ifndef AVL_SHARED_INST

// Find a the data of a node
// *data is set to the data of the found node
// (or not modified if not found)
static inline bool AVL_find_data (const AVL_tree_t *tree, const AVL_key_t key, AVL_data_t *data) {
	AVL_node_t* node = NULL;
	bool b = AVL_find(tree, key, &node);
	if(b==true) *data = node->data;
	return b;
}

// Find a the pointer to the data of a node
// *data is set to the address of the data of the found node
// (or not modified if not found)
static inline bool AVL_find_pdata (const AVL_tree_t *tree, const AVL_key_t key, AVL_data_t **data) {
	AVL_node_t* node = NULL;
	bool b = AVL_find(tree, key, &node);
	if(b==true) *data = &node->data;
	return b;
}

// Add a node and return the pointer to the data
static inline bool AVL_add_pdata (AVL_tree_t *tree, const AVL_key_t key, AVL_data_t **data) {
	AVL_node_t* node = NULL;
	bool b = AVL_add(tree, key, &node);
	*data = &node->data;
	return b;
}

// Add a node and overwrite the data if it already exists
static inline bool AVL_add_overwrite (AVL_tree_t *tree, const AVL_key_t key, AVL_data_t data) {
	AVL_node_t* node = NULL;
	bool b = AVL_add(tree, key, &node);
	node->data = data;
	return b;
}

// Add a node but keep the data if it already exists
static inline bool AVL_add_keep (AVL_tree_t *tree, const AVL_key_t key, AVL_data_t data) {
	AVL_node_t* node = NULL;
	bool b = AVL_add(tree, key, &node);
	if(b==true) node->data = data;
	return b;
}

#endif  // not AVL_SHARED_INST


// AVL_dup
#if 1  // Production version

// Duplicate a subtree
#ifdef AVL_LINKED
AVL_FUNC_ATTR AVL_node_t * AVL_dup_subtree (const AVL_node_t *subtree, AVL_tree_t *tree, AVL_node_t *father,
      AVL_node_t **first, AVL_node_t **last)
   //we store (return) the extreme nodes of the built subtree in *first and *last
#else
AVL_FUNC_ATTR AVL_node_t * AVL_dup_subtree (const AVL_node_t *subtree, AVL_tree_t *tree, AVL_node_t *father)
#endif
#ifdef AVL_SHARED_DECL
;
#else
{
	if (subtree == NULL) {
		#ifdef AVL_LINKED
		*first = NULL;
		*last  = NULL;
		#endif
		return NULL;
	}

	AVL_node_t *node = AVL_node_create(tree);

	// Recursively build sub-subtrees of current subtree
	#ifdef AVL_LINKED
	if ((node->child[AVL_LEFT] = AVL_dup_subtree(subtree->child[AVL_LEFT], tree, node, first, &node->neighbor[AVL_LEFT])))
		node->neighbor[AVL_LEFT]->neighbor[AVL_RIGHT] = node;
	else
		*first = node;
	if ((node->child[AVL_RIGHT] = AVL_dup_subtree(subtree->child[AVL_RIGHT], tree, node, &node->neighbor[AVL_RIGHT], last)))
		node->neighbor[AVL_RIGHT]->neighbor[AVL_LEFT] = node;
	else
		*last = node;
	#else
	node->child[AVL_LEFT]  = AVL_dup_subtree(subtree->child[AVL_LEFT], tree, node);
	node->child[AVL_RIGHT] = AVL_dup_subtree(subtree->child[AVL_RIGHT], tree, node);
	#endif

	// Fill the current node
	node->father  = father;
	node->balance = subtree->balance;
	node->side = subtree->side;
	node->data = subtree->data;
	AVL_node_set_key(node, AVL_node_get_key(subtree));

	return node;
}
#endif  // not AVL_SHARED_DECL


// Duplicate a tree
// Need an initialized tree, can be different pools
AVL_FUNC_ATTR void AVL_dup (AVL_tree_t *tree, const AVL_tree_t *srctree)
#ifdef AVL_SHARED_DECL
;
#else
{
	if (tree->root!=NULL) AVL_reset(tree);
	#ifdef AVL_LINKED
	tree->root = AVL_dup_subtree(srctree->root, tree, NULL, &tree->first, &tree->last);
	#else
	tree->root = AVL_dup_subtree(srctree->root, tree, NULL);
	#endif
	#ifdef AVL_DEBUG
	AVL_check_tree(tree);
	#endif
}
#endif  // not AVL_SHARED_DECL

#endif

#if 0  // Version for development/debug of the AVL module

// Need an initialized tree, can be different pools
AVL_FUNC_ATTR void AVL_dup (AVL_tree_t *tree, const AVL_tree_t *srctree)
#ifdef AVL_SHARED_DECL
;
#else
{
	if (tree->root!=NULL) AVL_reset(tree);
	for(AVL_node_t* node=AVL_first((AVL_tree_t*)srctree); node!=NULL; node=AVL_next((AVL_tree_t*)srctree, node)) {
		AVL_add_overwrite(tree, AVL_node_get_key((const AVL_node_t*)node), node->data);
	}
}
#endif  // not AVL_SHARED_DECL

#endif


#undef AVL_find_data
#undef AVL_find_pdata
#undef AVL_add_pdata
#undef AVL_add_overwrite
#undef AVL_add_keep



#define AVL_isempty          AVL_XCONCAT(AVL_prefix, isempty)
#define AVL_isthere          AVL_XCONCAT(AVL_prefix, isthere)
#define AVL_find_node        AVL_XCONCAT(AVL_prefix, find_node)
#define AVL_get_node         AVL_XCONCAT(AVL_prefix, get_node)
#define AVL_add_node         AVL_XCONCAT(AVL_prefix, add_node)
#define AVL_rem_key          AVL_XCONCAT(AVL_prefix, rem_key)
#define AVL_findadd_key      AVL_XCONCAT(AVL_prefix, findadd_key)
#define AVL_count            AVL_XCONCAT(AVL_prefix, count)

#ifndef AVL_SHARED_INST

// Same as AVL_find but renamed for clarity
static inline bool AVL_find_node (const AVL_tree_t *tree, const AVL_key_t key, AVL_node_t **node) {
	return AVL_find(tree, key, node);
}

// Return the node pointer
static inline AVL_node_t* AVL_get_node (const AVL_tree_t *tree, const AVL_key_t key) {
	AVL_node_t *node = NULL;
	bool b = AVL_find(tree, key, &node);
	if(b==false) return NULL;
	return node;
}

// Same as AVL_add but renamed for clarity
static inline bool AVL_add_node (AVL_tree_t *tree, const AVL_key_t key, AVL_node_t **node) {
	return AVL_add(tree, key, node);
}

// Check if a tree is empty
static inline bool AVL_isempty (const AVL_tree_t *tree) {
	return tree->root==NULL ? true : false;
}

// Check if a node is present
static inline bool AVL_isthere (const AVL_tree_t *tree, const AVL_key_t key) {
	AVL_node_t* node = NULL;
	bool b = AVL_find(tree, key, &node);
	return b;
}

// Find a node, add if needed.
static inline AVL_node_t* AVL_findadd_key (AVL_tree_t *tree, const AVL_key_t key) {
	AVL_node_t* node = NULL;
	AVL_add(tree, key, &node);
	return node;
}

#endif // not AVL_SHARED_INST

// Remove the node associated with the given key
AVL_FUNC_ATTR bool AVL_rem_key (AVL_tree_t *tree, const AVL_key_t key)
#ifdef AVL_SHARED_DECL
;
#else
{
	AVL_node_t* node = NULL;
	bool b = AVL_find(tree, key, &node);
	if(b==true) AVL_rem(tree, node);
	return b;
}
#endif  // not AVL_SHARED_DECL

#ifdef AVL_STAT

#ifndef AVL_SHARED_INST
static inline unsigned AVL_count (const AVL_tree_t *tree) {
	return tree->nbnode;
}
#endif // not AVL_SHARED_INST

#else

// Count the number of nodes
AVL_FUNC_ATTR unsigned AVL_count (const AVL_tree_t *tree)
#ifdef AVL_SHARED_DECL
;
#else
{

	#ifdef AVL_LINKED

	unsigned count = 0;
	for(AVL_node_t* node = AVL_first((AVL_tree_t*)tree); node!=NULL; node=AVL_next((AVL_tree_t*)tree, node)) count++;
	return count;

	#else  // #ifdef AVL_LINKED

	unsigned count_subtree (AVL_node_t *subtree) {
		if (subtree == NULL) return 0;
		unsigned count = 1;
		count += count_subtree(subtree->child[AVL_LEFT]);
		count += count_subtree(subtree->child[AVL_RIGHT]);
		return count;
	}
	return count_subtree(tree->root);

	#endif  // #ifdef AVL_LINKED

}
#endif  // not AVL_SHARED_DECL

#endif  // #ifdef AVL_STAT

#undef AVL_isempty
#undef AVL_isthere
#undef AVL_find_node
#undef AVL_get_node
#undef AVL_add_node
#undef AVL_findadd_key
#undef AVL_count


#undef AVL_prefix
#undef AVL_key_t
#undef AVL_data_t

#ifdef AVL_key_from_data
	#undef AVL_key_from_data
#endif
#ifdef AVL_key_from_pdata
	#undef AVL_key_from_pdata
#endif

#undef AVL_node_get_key
#undef AVL_node_set_key

#undef AVL_side_t
#undef AVL_LEFT
#undef AVL_RIGHT

#undef AVL_tree_t
#undef AVL_node_t
#undef AVL_pool_t
#undef AVL_array_t

#undef AVL_pool_init
#undef AVL_pool_clear
#undef AVL_pool_alloc
#undef AVL_pool_free
#undef AVL_pool_nodesalloc

#undef AVL_node_create
#undef AVL_node_delete

#undef AVL_init
#undef AVL_set_pool
#undef AVL_reset
#undef AVL_reset_cb
#undef AVL_root
#undef AVL_root_data
#undef AVL_first
#undef AVL_last
#undef AVL_deepest
#undef AVL_deepest_rem
#undef AVL_deepest_data
#undef AVL_deepest_data_rem
#undef AVL_prev
#undef AVL_next
#undef AVL_find
#undef AVL_add
#undef AVL_rem
#undef AVL_balance
#undef AVL_rot
#undef AVL_print
#undef AVL_print_r
#undef AVL_check_subtree
#undef AVL_check_tree
#undef AVL_dup_subtree
#undef AVL_dup

#undef AVL_FUNC_ATTR

#undef AVL_POOL_ARRAY_SIZE
#ifdef AVL_POOL_INSTANCE
	#undef AVL_POOL_INSTANCE
#endif
#ifdef AVL_DEFAULT_POOL
	#undef AVL_DEFAULT_POOL
#endif

#undef AVL_CONCAT
#undef AVL_XCONCAT

#ifdef AVL_LINKED
	#undef AVL_LINKED
#endif

#ifdef AVL_DEBUG
	#undef AVL_DEBUG
#endif

#ifdef AVL_CHECK
	#undef AVL_CHECK
#endif

#undef AVL_PRINT_OUTPUT

#ifdef AVL_PRINT_DATA
	#undef AVL_PRINT_DATA
#endif

#ifdef AVL_STAT
	#undef AVL_STAT
#endif

#ifdef AVL_PRINT
	#undef AVL_PRINT
#endif

#ifdef AVL_ALLOC
	#undef AVL_ALLOC
#endif
#ifdef AVL_FREE
	#undef AVL_FREE
#endif


