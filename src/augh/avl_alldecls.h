
// WARNING Don't include this file directly in a source file
// This header must be included after a macro definition,
// AVL_SHARED_INST or AVL_SHARED_DECL



//===============================================
// AVL tree : data=key type = pointer
//===============================================

#define AVL_prefix           avl_p
#define AVL_data_t           void*
#define AVL_key_t            void*
#define AVL_key_from_data(d) (d)
#define AVL_POOL_ARRAY_SIZE  1000
#define AVL_POOL_INSTANCE    AVL_P_POOL
#define AVL_LINKED

#include "avl.h"

#define avl_p_foreach(tree,node) \
	for(avl_p_node_t* node = avl_p_first(tree); node!=NULL; node=avl_p_next(tree, node))



//===============================================
// AVL tree : key & data types = pointer
//===============================================

#define AVL_prefix           avl_pp
#define AVL_key_t            void*
#define AVL_data_t           void*
#define AVL_POOL_ARRAY_SIZE  1000
#define AVL_POOL_INSTANCE    AVL_PP_POOL
#define AVL_LINKED

#include "avl.h"

#define avl_pp_foreach(tree,node) \
	for(avl_pp_node_t* node = avl_pp_first(tree); node!=NULL; node=avl_pp_next(tree, node))



//===============================================
// AVL tree : key = integer, data = pointer
//===============================================

#define AVL_prefix           avl_ip
#define AVL_key_t            int
#define AVL_data_t           void*
#define AVL_POOL_ARRAY_SIZE  1000
#define AVL_POOL_INSTANCE    AVL_IP_POOL
#define AVL_LINKED

#include "avl.h"

#define avl_ip_foreach(tree,node) \
	for(avl_ip_node_t* node = avl_ip_first(tree); node!=NULL; node=avl_ip_next(tree, node))

#define avl_ip_foreach_inc(tree,node) \
	for(avl_ip_node_t* node = avl_ip_first(tree); node!=NULL; node=avl_ip_next(tree, node))

#define avl_ip_foreach_dec(tree,node) \
	for(avl_ip_node_t* node = avl_ip_last(tree); node!=NULL; node=avl_ip_prev(tree, node))



//===============================================
// AVL tree : key = pointer, data = integer
//===============================================

#define AVL_prefix           avl_pi
#define AVL_key_t            void*
#define AVL_data_t           int
#define AVL_POOL_ARRAY_SIZE  1000
#define AVL_POOL_INSTANCE    AVL_PI_POOL
#define AVL_LINKED

#include "avl.h"

#define avl_pi_foreach(tree,node) \
	for(avl_pi_node_t* node = avl_pi_first(tree); node!=NULL; node=avl_pi_next(tree, node))

#define avl_pi_foreach_inc(tree,node) \
	for(avl_pi_node_t* node = avl_pi_first(tree); node!=NULL; node=avl_pi_next(tree, node))

#define avl_pi_foreach_dec(tree,node) \
	for(avl_pi_node_t* node = avl_pi_last(tree); node!=NULL; node=avl_pi_prev(tree, node))



//===============================================
// AVL tree : key = integer, data = integer
//===============================================

#define AVL_prefix           avl_i_i
#define AVL_key_t            int
#define AVL_data_t           int
#define AVL_POOL_ARRAY_SIZE  1000
#define AVL_POOL_INSTANCE    AVL_I_I_POOL
#define AVL_LINKED

#include "avl.h"

#define avl_i_i_foreach(tree,node) \
	for(avl_i_i_node_t* node = avl_i_i_first(tree); node!=NULL; node=avl_i_i_next(tree, node))

#define avl_i_i_foreach_inc(tree,node) \
	for(avl_i_i_node_t* node = avl_i_i_first(tree); node!=NULL; node=avl_i_i_next(tree, node))

#define avl_i_i_foreach_dec(tree,node) \
	for(avl_i_i_node_t* node = avl_i_i_last(tree); node!=NULL; node=avl_i_i_prev(tree, node))
