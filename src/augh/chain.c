
#include <stdio.h>   // For check functions
#include <stdlib.h>  // For allocations

#include "chain.h"


// The size of the allocations
#define ALLOC_CHAIN_BATCH 1000



//====================================================================
// Various functions for Alliance lists
//====================================================================

// The destination list is modified.
chain_list* ChainList_Merge(chain_list* dest, chain_list* src) {
	for(chain_list* scan=src; scan!=NULL; scan=scan->NEXT) {
		if(ChainList_IsDataHere(dest, scan->DATA)!=0) continue;
		dest = addchain(dest, scan->DATA);
	}
	return dest;
}

// Allocates a new list
chain_list* ChainList_Unique(chain_list* list) {
	return ChainList_Merge(NULL, list);
}

// deldatachain() does it, but this version returns the list if the data was not found (instead of NULL)
chain_list* ChainList_Remove(chain_list* list, void* data) {
	chain_list* prev = NULL;
	for(chain_list* scan=list; scan!=NULL; scan=scan->NEXT) {
		if(scan->DATA==data) {
			if(prev==NULL) { list=list->NEXT; scan->NEXT=NULL; freechain(scan); return list; }
			else { prev->NEXT=scan->NEXT; scan->NEXT=NULL; freechain(scan); return list; }
		}
		prev = scan;
	}
	return list;  // Data not found.
}

void ChainNum_Print_fbse(num_list* list, FILE* F, char* beg, char* sep, char* end) {
	if(beg!=NULL) fputs(beg, F);
	bool first = true;
	foreach(list, scan) {
		if(first==true) first = false; else { if(sep!=NULL) fputs(sep, F); else fputs(", ", F); }
		fprintf(F, "%ld", scan->DATA);
	}
	if(end!=NULL) fputs(end, F);
}

ptype_list* ChainPType_Concat(ptype_list* head, ptype_list* tail) {
	if(head==NULL) return tail;
	if(tail==NULL) return head;
	// Go at the end of the head and append the tail
	ptype_list* end = head;
	while(end->NEXT!=NULL) end = end->NEXT;
	end->NEXT = tail;
	return head;
}

ptype_list* ChainPTtpe_Remove_Element(ptype_list* list, ptype_list* elt) {
	ptype_list* prev = NULL;
	for(ptype_list* scan=list; scan!=NULL; scan=scan->NEXT) {
		if(scan==elt) {
			if(prev==NULL) { list=list->NEXT; scan->NEXT=NULL; freeptype(scan); return list; }
			else { prev->NEXT=scan->NEXT; scan->NEXT=NULL; freeptype(scan); return list; }
		}
		prev = scan;
	}
	return list;  // Data not found.
}

ptype_list* ChainPTtpe_Remove_Data(ptype_list* list, void* data) {
	ptype_list* prev = NULL;
	for(ptype_list* scan=list; scan!=NULL; scan=scan->NEXT) {
		if(scan->DATA==data) {
			if(prev==NULL) { list=list->NEXT; scan->NEXT=NULL; freeptype(scan); return list; }
			else { prev->NEXT=scan->NEXT; scan->NEXT=NULL; freeptype(scan); return list; }
		}
		prev = scan;
	}
	return list;  // Data not found.
}

ptype_list* ChainPTtpe_Remove_Data_xorType(ptype_list* list, void* data, long flag) {
	ptype_list* prev = NULL;
	for(ptype_list* scan=list; scan!=NULL; scan=scan->NEXT) {
		if(scan->DATA==data) {
			int newFlag = scan->TYPE ^ flag;
			if(newFlag==0) {
				if(prev==NULL) { list=list->NEXT; scan->NEXT=NULL; freeptype(scan); return list; }
				else { prev->NEXT=scan->NEXT; scan->NEXT=NULL; freeptype(scan); return list; }
			}
			scan->TYPE = newFlag;
		}
		prev = scan;
	}
	return list;  // Data not found.
}

ptype_list* ChainPType_Reverse(ptype_list* list) {
	ptype_list* new_list = NULL;
	ptype_list* next_list;
	for( ; list!=NULL; list=next_list) {
		next_list = list->NEXT;
		list->NEXT = new_list;
		new_list = list;
	}
	return new_list;
}

// The input list is modified !
ptype_list* ChainPType_Sort_Inc(ptype_list* list) {
	ptype_list* new_list = NULL;
	while(list!=NULL) {
		ptype_list* list_next = list->NEXT;
		// If the new list is empty, or
		// If the new element has to be placed before the first.
		if(new_list==NULL || new_list->TYPE >= list->TYPE) {
			list->NEXT = new_list;
			new_list = list;
		}
		// Insert the element in the right place
		else {
			foreach(new_list, scanNew) {
				ptype_list* next_new = scanNew->NEXT;
				if(next_new==NULL || next_new->TYPE >= list->TYPE) {
					scanNew->NEXT = list;
					list->NEXT = next_new;
					break;
				}
			}
		}
		// Switch to the next element
		list = list_next;
	}  // Scan the elements to sort
	return new_list;
}
ptype_list* ChainPType_Sort_Dec(ptype_list* list) {
	ptype_list* new_list = NULL;
	while(list!=NULL) {
		ptype_list* list_next = list->NEXT;
		// If the new list is empty, or
		// If the new element has to be placed before the first.
		if(new_list==NULL || new_list->TYPE <= list->TYPE) {
			list->NEXT = new_list;
			new_list = list;
		}
		// Insert the element in the right place
		else {
			foreach(new_list, scanNew) {
				ptype_list* next_new = scanNew->NEXT;
				if(next_new==NULL || next_new->TYPE <= list->TYPE) {
					scanNew->NEXT = list;
					list->NEXT = next_new;
					break;
				}
			}
		}
		// Switch to the next element
		list = list_next;
	}  // Scan the elements to sort
	return new_list;
}

// Check inclusions
// The DATA is the object, TYPE is the amount.
// Return true is each tested DATA is present in the ref list, and the amount is enough.
bool ChainPType_CheckInclude(ptype_list* list_ref, ptype_list* list_tested) {
	foreach(list_tested, elt_test) {
		ptype_list* elt_ref = ChainPType_SearchData(list_ref, elt_test->DATA);
		if(elt_ref==NULL) return false;
		if(elt_ref->TYPE<elt_test->TYPE) return false;
	}
	return true;
}

bitype_list* ChainBiType_Concat(bitype_list* head, bitype_list* tail) {
	if(head==NULL) return tail;
	if(tail==NULL) return head;
	// Go at the end of the head and append the tail
	bitype_list* end = head;
	while(end->NEXT!=NULL) end = end->NEXT;
	end->NEXT = tail;
	return head;
}



//===============================================
// Chain PD
//===============================================

// Create a pool of type : chain_pd_t

#define POOL_prefix      pool_chain_pd
#define POOL_data_t      chain_pd_t
#define POOL_ALLOC_SIZE  1000

#define POOL_INSTANCE             POOL_CHAIN_PD
#define POOL_INSTANCE_ATTRIBUTES  static

#define POOL_FUNC_ATTRIBUTES      static inline

#include "pool.h"

// Functions

chain_pd_t* chain_pd_new() {
	return pool_chain_pd_pop(&POOL_CHAIN_PD);
}
void chain_pd_del(chain_pd_t* elt) {
	pool_chain_pd_push(&POOL_CHAIN_PD, elt);
}

chain_pd_t* chain_pd_dup_list(chain_pd_t* elt) {
	chain_pd_t* first_elt = NULL;
	chain_pd_t* prev_elt = NULL;
	for(; elt!=NULL; elt=elt->NEXT) {
		chain_pd_t *new_elt = chain_pd_dup(elt);
		if(prev_elt!=NULL) prev_elt->NEXT = new_elt;
		else first_elt = new_elt;
		prev_elt = new_elt;
	}
	return first_elt;
}

// The input list is modified !
chain_pd_t* chain_pd_sort_inc(chain_pd_t* list) {
	chain_pd_t* new_list = NULL;
	while(list!=NULL) {
		chain_pd_t* list_next = list->NEXT;
		// If the new list is empty, or
		// If the new element has to be placed before the first.
		if(new_list==NULL || new_list->d >= list->d) {
			list->NEXT = new_list;
			new_list = list;
		}
		// Insert the element in the right place
		else {
			foreach(new_list, scanNew) {
				chain_pd_t* next_new = scanNew->NEXT;
				if(next_new==NULL || next_new->d >= list->d) {
					scanNew->NEXT = list;
					list->NEXT = next_new;
					break;
				}
			}
		}
		// Switch to the next element
		list = list_next;
	}  // Scan the elements to sort
	return new_list;
}
chain_pd_t* chain_pd_sort_dec(chain_pd_t* list) {
	chain_pd_t* new_list = NULL;
	while(list!=NULL) {
		chain_pd_t* list_next = list->NEXT;
		// If the new list is empty, or
		// If the new element has to be placed before the first.
		if(new_list==NULL || new_list->d <= list->d) {
			list->NEXT = new_list;
			new_list = list;
		}
		// Insert the element in the right place
		else {
			foreach(new_list, scanNew) {
				chain_pd_t* next_new = scanNew->NEXT;
				if(next_new==NULL || next_new->d <= list->d) {
					scanNew->NEXT = list;
					list->NEXT = next_new;
					break;
				}
			}
		}
		// Switch to the next element
		list = list_next;
	}  // Scan the elements to sort
	return new_list;
}



//===============================================
// All AVL trees
//===============================================

#define AVL_SHARED_INST

#include "avl_alldecls.h"

void* avl_p_GetIdx(avl_p_tree_t* tree, unsigned idx) {
	unsigned i = 0;
	avl_p_node_t* scan = avl_p_first(tree);
	while(scan!=NULL) {
		if(i==idx) return scan->data;
		scan = avl_p_next(tree, scan);
		i++;
	}
	return NULL;
}

int avl_pi_addflag(avl_pi_tree_t* tree, void* key, int flag) {
	avl_pi_node_t* node = NULL;
	bool b = avl_pi_add(tree, key, &node);
	if(b==true) node->data = 0;
	node->data |= flag;
	return node->data;
}

int avl_pi_rmflag(avl_pi_tree_t* tree, void* key, int flag) {
	avl_pi_node_t* node = NULL;
	bool b = avl_pi_find(tree, key, &node);
	if(b==false) return 0;
	node->data ^= flag;
	if(node->data==0) { avl_pi_rem(tree, node); return 0; }
	return node->data;
}


