
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <assert.h>
#include <math.h>

#include "techno.h"
#include "techno_delay.h"
#include "../netlist/netlist.h"



// Allocation wrappers

static techno_delay_precomp_t* Techno_Delay_PrecompState_New(netlist_fsm_state_t* state) {
	techno_delay_precomp_t* precomp = calloc(1, sizeof(techno_delay_precomp_t));
	precomp->state = state;
	avl_p_init(&precomp->mux_inputs);
	return precomp;
}
static void Techno_Delay_PrecompState_Free(techno_delay_precomp_t* precomp) {
	avl_p_reset(&precomp->mux_inputs);
	freechain(precomp->write_comp);
	freechain(precomp->write_access);
	free(precomp);
}

static techno_delay_precomp_t* Techno_Delay_PrecompState_AddGet(techno_delay_t* data, netlist_fsm_state_t* state) {
	avl_pp_node_t* pp_node = NULL;
	bool b = avl_pp_add(&data->precomp_perstate, state, &pp_node);
	if(b==true) pp_node->data = Techno_Delay_PrecompState_New(state);
	return pp_node->data;
}
static techno_delay_precomp_t* Techno_Delay_PrecompState_Get(techno_delay_t* data, netlist_fsm_state_t* state) {
	avl_pp_node_t* pp_node = NULL;
	bool b = avl_pp_find(&data->precomp_perstate, state, &pp_node);
	if(b==false) return NULL;
	return pp_node->data;
}

double* Techno_Delay_PortDelays_GetAdd(techno_delay_t* data, netlist_port_t* port) {
	avl_pp_node_t* pp_node = NULL;
	bool b = avl_pp_add(&data->ports_delays, port, &pp_node);
	if(b==true) {
		double* delays = malloc(port->width * sizeof(double));
		for(unsigned i=0; i<port->width; i++) delays[i] = -1;
		pp_node->data = delays;
	}
	return pp_node->data;
}
double* Techno_Delay_PortDelays_Get(techno_delay_t* data, netlist_port_t* port) {
	avl_pp_node_t* pp_node = NULL;
	bool b = avl_pp_add(&data->ports_delays, port, &pp_node);
	if(b==false) return NULL;
	return pp_node->data;
}



// Main processing functions

void Techno_Delay_Process_PortOut(techno_delay_t* data, netlist_port_t* port, double* delays) {
	// Check if it's already set
	if(avl_p_isthere(&data->ports_used, port)==true) return;

	// Top-level ports
	if(port->component==data->Implem->netlist.top) {
		// FIXME Set phony delays
		for(unsigned i=0; i<port->width; i++) delays[i] = 0;
		avl_p_add_overwrite(&data->ports_used, port, port);
		return;
	}

	// Check combinatorial loop
	if(avl_p_isthere(&data->combloop_outports, port)==true) {
		printf("Warning: Detected combinatorial loop through port '%s' of component '%s' model '%s'\n",
			port->name, port->component->name, port->component->model->name
		);
		for(unsigned i=0; i<port->width; i++) delays[i] = 0;
		avl_p_add_overwrite(&data->ports_used, port, port);
		return;
	}
	// Mark the port as being processed, to detect combinatorial loops
	avl_p_add_overwrite(&data->combloop_outports, port, port);

	// Here we do only component-specific stuff

	netlist_comp_t* comp = port->component;
	if(comp->model->func_propag_delays==NULL) {
		// Print a warning, but not too often
		avl_pi_node_t* node_pi = NULL;
		bool b = avl_pi_add(&data->warn_nopropfunc_comp, comp->model->name, &node_pi);
		if(b==false) node_pi->data ++;
		else {
			node_pi->data = 0;
			printf("Warning: No delay propagation function is defined for component model '%s'.\n", comp->model->name);
		}
		// Set an arbitrary delay
		for(unsigned i=0; i<port->width; i++) delays[i] = 0;
	}
	else {
		comp->model->func_propag_delays(data, port, delays);
	}

	// Flag the port as processed
	avl_p_add_overwrite(&data->ports_used, port, port);

	// Un-flag this port about combinatorial loop
	avl_p_rem_key(&data->combloop_outports, port);
}
// Input = an input port of a component
// As output, the corresponding array of delays in the main data structure is filled
void Techno_Delay_Process_PortIn(techno_delay_t* data, netlist_port_t* port, double* delays) {
	// Check if it's already set
	if(avl_p_isthere(&data->ports_used, port)==true) return;
	// Compute all delays
	// The ValCat lists elements from left to right, so begin by the leftmost index
	unsigned bitidx = port->width-1;
	foreach(port->source, scanValcat) {
		netlist_val_cat_t* valcat = scanValcat->DATA;
		if(valcat->source!=NULL) {
			// Get the delays of that source port
			double* delays_src = Techno_Delay_PortDelays_GetAdd(data, valcat->source);
			// Launch computing of delay upto that source port
			Techno_Delay_Process_PortOut(data, valcat->source, delays_src);
			// Set delays
			if(valcat->right==valcat->left) {
				for(int i=valcat->width-1; i>=0; i--) delays[bitidx--] = delays_src[valcat->right];
			}
			else {
				for(int i=valcat->left; i>=(int)valcat->right; i--) delays[bitidx--] = delays_src[i];
			}
		}
		else {  // Literal
			for(int i=valcat->width-1; i>=0; i--) delays[bitidx--] = 0;
		}
	}
	// Mark the port as processed
	avl_p_add_overwrite(&data->ports_used, port, port);
}
// Return the max datapath delay to readh this input port
double Techno_Delay_Process_PortIn_GetMax(techno_delay_t* data, netlist_port_t* port) {
	double *delays = Techno_Delay_PortDelays_GetAdd(data, port);
	Techno_Delay_Process_PortIn(data, port, delays);
	double d = 0;
	for(unsigned i=0; i<port->width; i++) d = GetMax(d, delays[i]);
	return d;
}

// A utility function to list the FSM states where an FSM outval is at 1
// Only for 1-bit ports
static chain_list* Techno_Delay_FsmOutval_ListStates_val1(techno_delay_t* data, netlist_fsm_outval_t* outval) {
	netlist_comp_t* fsm = data->Implem->netlist.fsm;
	netlist_fsm_t* fsm_data = fsm->data;

	assert(outval->port->width==1);

	// Check if already computed
	avl_pp_node_t* pp_node = NULL;
	bool b = avl_pp_find(&data->precomp_fsmoutstates, outval, &pp_node);
	if(b==true) return dupchain(pp_node->data);

	// Compute a fresh list
	chain_list* list_states = NULL;
	// If the default value is 1, invert the states
	if(outval->default_val!=NULL && outval->default_val[0]=='1') {
		// List the states where the port is NOT set to 1
		avl_p_tree_t tree_actstate;
		avl_p_init(&tree_actstate);
		foreach(outval->actions, scanAction) {
			netlist_fsm_action_t* action = scanAction->DATA;
			if(action->value[0]!='0') continue;
			avl_p_add_overwrite(&tree_actstate, action->state, action->state);
		}
		foreach(fsm_data->states, scanState) {
			netlist_fsm_state_t* state = scanState->DATA;
			if(avl_p_isthere(&tree_actstate, state)==true) continue;
			list_states = addchain(list_states, state);
		}
		avl_p_reset(&tree_actstate);
	}
	else {
		foreach(outval->actions, scanAction) {
			netlist_fsm_action_t* action = scanAction->DATA;
			if(action->value[0]=='0') continue;
			list_states = addchain(list_states, action->state);
		}
	}

	// Keep a copy of the list
	pp_node = NULL;
	b = avl_pp_add(&data->precomp_fsmoutstates, outval, &pp_node);
	if(b==false && pp_node->data!=NULL) freechain(pp_node->data);
	pp_node->data = dupchain(list_states);

	// Return the list
	return list_states;
}

// FIXME only a reduced subset of cases is handled
static chain_list* Techno_Delay_Port_GetFsmStates_set1(techno_delay_t* data, netlist_port_t* port) {
	netlist_comp_t* fsm = data->Implem->netlist.fsm;

	assert(port->width==1);

	// Find the FSM output port the Enable port is attached to
	if(port->source==NULL) {
		printf("Warning: Port '%s' of component '%s' model '%s': no source.\n",
			port->name, port->component->name, port->component->model->name
		);
		return NULL;
	}
	netlist_val_cat_t* valcat = port->source->DATA;
	if(valcat->literal!=NULL) {
		printf("Warning: Port '%s' of component '%s' model '%s': source is literal: %s.\n",
			port->name, port->component->name, port->component->model->name, valcat->literal
		);
		return NULL;
	}
	netlist_port_t* scan_port = valcat->source;
	if(scan_port->width!=1) {
		printf("Warning: Port '%s' of component '%s' model '%s': source is port of width %u.\n",
			port->name, port->component->name, port->component->model->name, scan_port->width
		);
		return NULL;
	}

	bool path_not_handled = false;
	while(scan_port->component->model==NETLIST_COMP_SIGNAL) {
		netlist_signal_t* sig_data = scan_port->component->data;
		if(sig_data->width!=1) { path_not_handled = true; break; }
		if(sig_data->port_in->source==NULL) { path_not_handled = true; break; }
		valcat = sig_data->port_in->source->DATA;
		if(valcat->literal!=NULL) { path_not_handled = true; break; }
		scan_port = valcat->source;
		if(scan_port->width!=1) { path_not_handled = true; break; }
	}
	if(path_not_handled==true || scan_port->component!=fsm) {
		printf("Warning: Port '%s' of component '%s' model '%s': source path not handled.\n",
			port->name, port->component->name, port->component->model->name
		);
		return NULL;
	}

	// The FSM source is found. Add the corresponding States.
	netlist_fsm_outval_t* outval = Netlist_Comp_Fsm_GetOutVal(scan_port);
	if(outval==NULL) {
		printf("Warning: Port '%s' of component '%s' model '%s': linked to FSM port '%s' without FSM outval structure.\n",
			port->name, port->component->name, port->component->model->name, scan_port->name
		);
		return NULL;
	}

	// Get the states where the output is active
	return Techno_Delay_FsmOutval_ListStates_val1(data, outval);
}

// FIXME also handle FIFO writes, and paths involving a top-level port?
// FIXME also compute the max frequency the design can internally run at
//   including delay of FSM transition function
//   and delay of unbuffered Write Enable inputs flagged with only-last-cycle
// FIXME take into account delay on FSM inputs. So, scan the rules for branch condition at each state...
//   Add a CB function to comp model for Write to a port
//   This could be used to check timing on handshake signals for FIFO...

// Get max delay for a State. Also separately get the max delay on WE signals.
static void Techno_Delay_State_launchrecurs(
	techno_delay_t* data, techno_delay_precomp_t* precomp, double* p_max_delay, double* p_max_delay_we
) {
	netlist_comp_t* fsm = data->Implem->netlist.fsm;

	// Clear previously used data structures
	avl_p_reset(&data->combloop_outports);
	avl_p_foreach(&data->ports_used, scanPort) {
		netlist_port_t* port = scanPort->data;
		avl_pp_node_t* pp_node = NULL;
		avl_pp_find(&data->ports_delays, port, &pp_node);
		assert(pp_node!=NULL);
		double *delays = pp_node->data;
		for(unsigned i=0; i<port->width; i++) delays[i] = -1;
	}
	avl_p_reset(&data->ports_used);

	// Set the state being computed (maybe unuseful?)
	data->curstate = precomp;

	double max_delay = 0;
	double max_delay_we = 0;

	// Scan all components that are destination
	foreach(precomp->write_comp, scanComp) {
		netlist_comp_t* comp = scanComp->DATA;
		if(comp->model->func_write_comp==NULL) {
			printf("Warning: Component model '%s' has no Write Comp callback function. Skipped.\n", comp->model->name);
			continue;
		}
		double delay = 0, delay_we = 0;
		comp->model->func_write_comp(data, comp, &delay, &delay_we);
		max_delay = GetMax(max_delay, delay);
		max_delay_we = GetMax(max_delay_we, delay_we);
	}
	// Scan all accesses that are destination
	foreach(precomp->write_access, scanAccess) {
		netlist_access_t* access = scanAccess->DATA;
		netlist_comp_model_t* model = access->component->model;
		if(model->func_write_access==NULL) {
			printf("Warning: Component model '%s' has no Write Access callback function. Skipped.\n", model->name);
			continue;
		}
		double delay = 0, delay_we = 0;
		model->func_write_access(data, access, &delay, &delay_we);
		max_delay = GetMax(max_delay, delay);
		max_delay_we = GetMax(max_delay_we, delay_we);
	}

	// Get the transition delay of the FSM
	if(fsm->model->func_trans_delay==NULL) {
		printf("Warning: Component model '%s' has no Trans Delay callback function. Skipped.\n", fsm->model->name);
	}
	else {
		double delay = 0;
		fsm->model->func_trans_delay(data, fsm, &delay);
		max_delay = GetMax(max_delay, delay);
	}

	*p_max_delay = max_delay;
	*p_max_delay_we = max_delay_we;
}

// This is the main function that launches post-map analysis and back-annotation of the Hier graph.
void Techno_Delay(implem_t* Implem) {
	techno_delay_t* data = calloc(1, sizeof(techno_delay_t));

	// Initialize storage structures
	data->Implem = Implem;
	avl_pi_init(&data->warn_nopropfunc_comp);
	avl_pp_init(&data->precomp_perstate);
	avl_pp_init(&data->precomp_muxinstates);
	avl_pp_init(&data->precomp_fsmoutstates);
	avl_pp_init(&data->ports_delays);
	data->curstate = NULL;
	avl_p_init(&data->ports_used);
	avl_p_init(&data->combloop_outports);

	netlist_comp_t* fsm = Implem->netlist.fsm;
	if(fsm==NULL) {
		printf("Warning: No FSM is present. Post-map delay analysis is not performed.\n");
		return;
	}

	netlist_fsm_t* fsm_data = fsm->data;

	// Clear previous mapper annotations
	Hier_Latency_ClearMapper(Implem);

	// Precompute MUX input usage
	avl_pp_foreach(&Implem->netlist.top->children, scanComp) {
		netlist_comp_t* comp = scanComp->data;
		if(comp->model!=NETLIST_COMP_MUX) continue;
		// Scan all input accesses
		avl_pp_foreach(&comp->accesses, scanAccess) {
			netlist_access_t* access = scanAccess->data;
			netlist_mux_access_t* muxIn_data = access->data;
			// Find the FSM states where the Enable port is active
			chain_list* list_states = Techno_Delay_Port_GetFsmStates_set1(data, muxIn_data->port_en);
			if(list_states==NULL) continue;
			// Add to the tree of precomputed stuff per state
			foreach(list_states, scanState) {
				netlist_fsm_state_t* state = scanState->DATA;
				techno_delay_precomp_t* precomp = Techno_Delay_PrecompState_AddGet(data, state);
				avl_p_add_overwrite(&precomp->mux_inputs, access, access);
			}
			// Save the list of states in the tree
			avl_pp_node_t* pp_node = NULL;
			bool b = avl_pp_add(&data->precomp_muxinstates, access, &pp_node);
			if(b==false && pp_node->data!=NULL) freechain(pp_node->data);
			pp_node->data = list_states;
		}
	}

	// Precompute the lists of Write operations for each state
	avl_pp_foreach(&Implem->netlist.top->children, scanComp) {
		netlist_comp_t* comp = scanComp->data;
		if(comp->model==NETLIST_COMP_REGISTER) {
			netlist_register_t* reg_data = comp->data;
			// Find the FSM states where the Write Enable port is active
			chain_list* list_states = Techno_Delay_Port_GetFsmStates_set1(data, reg_data->port_en);
			if(list_states==NULL) {
				printf("Warning: Register '%s': Could not get the Write Enable states.\n", comp->name);
				continue;
			}
			// Add to the tree of precomputed stuff per state
			foreach(list_states, scanState) {
				netlist_fsm_state_t* state = scanState->DATA;
				techno_delay_precomp_t* precomp = Techno_Delay_PrecompState_AddGet(data, state);
				precomp->write_comp = addchain(precomp->write_comp, comp);
			}
			// Clean
			freechain(list_states);
		}
		else if(comp->model==NETLIST_COMP_MEMORY) {
			// Scan all accesses
			avl_pp_foreach(&comp->accesses, scanAccess) {
				netlist_access_t* access = scanAccess->data;
				// Find the FSM states where the Write Enable port is active
				chain_list* list_states = NULL;
				if(access->model==NETLIST_ACCESS_MEM_WA) {
					netlist_access_mem_addr_t* memA_data = access->data;
					list_states = Techno_Delay_Port_GetFsmStates_set1(data, memA_data->port_e);
				}
				else if(access->model==NETLIST_ACCESS_MEM_D) {
					netlist_access_mem_direct_t* memD_data = access->data;
					if(memD_data->port_we==NULL) continue;
					list_states = Techno_Delay_Port_GetFsmStates_set1(data, memD_data->port_we);
				}
				else continue;
				if(list_states==NULL) {
					printf("Warning: Access '%s' model '%s' of component '%s' model '%s': Could not get the Write Enable states.\n",
						access->name, access->model->name, comp->name, comp->model->name
					);
					continue;
				}
				// Add to the tree of precomputed stuff per state
				foreach(list_states, scanState) {
					netlist_fsm_state_t* state = scanState->DATA;
					techno_delay_precomp_t* precomp = Techno_Delay_PrecompState_AddGet(data, state);
					precomp->write_access = addchain(precomp->write_access, access);
				}
				// Clean
				freechain(list_states);
			}
		}

	}  // Scan all components

	// Launch recusive scan for each State

	Netlist_Comp_Fsm_SetStateIdx(Implem->netlist.fsm);
	Netlist_Comp_Fsm_PrevRules_Build(Implem->netlist.fsm);
	// Note: the retiming counters are supposed to be already set (done during mapping)

	printf("INFO: Post-mapping timing analysis:");
	unsigned states_fixed_nb = 0;

	// Scan all FSM states
	foreach(fsm_data->states, scanState) {
		netlist_fsm_state_t* state = scanState->DATA;
		techno_delay_precomp_t* precomp = Techno_Delay_PrecompState_Get(data, state);
		if(precomp==NULL) continue;

		// From each destination component/access, recursively scan the datapath
		double max_delay = 0;
		double max_delay_we = 0;
		Techno_Delay_State_launchrecurs(data, precomp, &max_delay, &max_delay_we);
		//dbgprintf("FSM state %u: delay is %g ns\n", state->index, max_delay);

		unsigned clk_nb = 1;
		if(Implem->synth_target->clk_period_ns > 0) {
			clk_nb = ceil(max_delay / Implem->synth_target->clk_period_ns);
		}
		// Decreasing state duration is unsafe because not all paths are analyzed yet.
		// So only an increase of the state duration is allowed, for safety.
		if(clk_nb < state->cycles_nb) clk_nb = state->cycles_nb;

		// Back-annotate the Hier graph
		// FIXME Missing getting delay of each Action
		hier_node_t* hierNode = Map_FsmState2Node(Implem->netlist.mapping, state);
		if(hierNode!=NULL) {
			if(hierNode->TYPE==HIERARCHY_STATE) {
				hier_state_t* state_data = &hierNode->NODE.STATE;
				state_data->time.map_delay = max_delay;
				state_data->time.map_nb_clk = clk_nb;
				if(state_data->time.map_delay > Implem->time.map_max_action_delay) Implem->time.map_max_action_delay = state_data->time.map_delay;
				if(state_data->time.map_nb_clk > Implem->time.map_max_cycles_per_state) Implem->time.map_max_cycles_per_state = state_data->time.map_nb_clk;
			}
		}

		// Update per-State timing annotations
		if(Implem->synth_target->clk_period_ns > 0) {
			int diff_clk_nb = clk_nb - state->cycles_nb;
			if(diff_clk_nb!=0) {
				printf(" %+i", diff_clk_nb);
				states_fixed_nb++;
				state->cycles_nb = clk_nb;
			}
			if(max_delay_we > Implem->synth_target->clk_period_ns) {
				printf(" WARNING Delay on WE signal (%g ns) exceeds clock period (%g ns) \n", max_delay_we, Implem->synth_target->clk_period_ns);
			}
		}

	}  // Scan all FSM states

	if(Implem->synth_target->clk_period_ns > 0) {
		printf(" (states fixed: %u)\n", states_fixed_nb);
	}
	else {
		printf(" max delay: %g ns\n", Implem->time.map_max_action_delay);
	}

	// Free data

	avl_pp_foreach(&data->precomp_perstate, scan) Techno_Delay_PrecompState_Free(scan->data);
	avl_pp_foreach(&data->ports_delays, scan) free(scan->data);

	avl_pi_reset(&data->warn_nopropfunc_comp);
	avl_pp_reset(&data->precomp_perstate);
	avl_pp_reset(&data->precomp_muxinstates);
	avl_pp_reset(&data->precomp_fsmoutstates);
	avl_pp_reset(&data->ports_delays);
	avl_p_reset(&data->ports_used);
	avl_p_reset(&data->combloop_outports);

	free(data);
}

