
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <assert.h>
#include <inttypes.h>

#include "auto.h"
#include "hierarchy.h"
#include "linelist.h"



// Internal function to merge blocks.
// Note: insertion is insert States of BBs before the States of the ref BBs, to preserve branch conditions.
// So we are inserting a Block that should be BEFORE the ref Block.
static int Hier_MergeBlocks(hier_node_t* destBlock, hier_node_t* destBlockEndNode, hier_node_t* prevBlock, hier_node_t* prevBlockEndNode, int flags) {

	do {

		if(destBlock->TYPE != prevBlock->TYPE) return __LINE__;

		if(destBlock->TYPE==HIERARCHY_BB) {
			hier_node_t* dupNode = Hier_Node_Dup(prevBlock);
			hier_node_t* body = dupNode->NODE.BB.body;
			if(body!=NULL) {
				Hier_CutLinkBefore(body);
				Hier_FreeNode(dupNode);
				hier_bb_t* destbb_data = &destBlock->NODE.BB;
				if(destbb_data->body==NULL) {
					destbb_data->body = body;
					destBlock->CHILDREN = addchain(destBlock->CHILDREN, body);
				}
				else {
					Hier_InsertLevelBeforeNode(destbb_data->body, body);
				}
			}
		}

		else if(destBlock->TYPE==HIERARCHY_LOOP) {
			hier_loop_t* destloop_data = &destBlock->NODE.LOOP;
			hier_loop_t* prevloop_data = &prevBlock->NODE.LOOP;
			// Check integrity
			if( (destloop_data->body_after==NULL) != (prevloop_data->body_after==NULL) ) return __LINE__;
			if( (destloop_data->body_before==NULL) != (prevloop_data->body_before==NULL) ) return __LINE__;
			// Merge blocks
			if(destloop_data->body_after!=NULL) {
				int z = Hier_MergeBlocks(destloop_data->body_after, NULL, prevloop_data->body_after, NULL, flags);
				if(z!=0) return z;
			}
			if(destloop_data->body_before!=NULL) {
				int z = Hier_MergeBlocks(destloop_data->body_before, NULL, prevloop_data->body_before, NULL, flags);
				if(z!=0) return z;
			}
		}

		else if(destBlock->TYPE==HIERARCHY_LABEL) {
			// Just check that there is no Jump that leads there
			hier_label_t* destlabel_data = &destBlock->NODE.LABEL;
			hier_label_t* prevlabel_data = &prevBlock->NODE.LABEL;
			if(prevlabel_data->jumps!=NULL) return __LINE__;
			if(destlabel_data->jumps!=NULL) return __LINE__;
		}

		else {
			dbgprintf("Merge failed because node type %u '%s' is not handled\n", destBlock->TYPE, Hier_NodeType2str(destBlock->TYPE));
			return __LINE__;
		}

		destBlock = destBlock->NEXT;
		prevBlock = prevBlock->NEXT;

		// Check if end is reached
		if( (destBlock==destBlockEndNode) != (prevBlock==prevBlockEndNode) ) {
			dbgprintf("Merge failed because the two blocks don't end together\n");
			return __LINE__;
		}
		if(destBlock==destBlockEndNode) break;
		if(prevBlock==prevBlockEndNode) break;

	} while(1);

	return 0;
}

// Argument: a list of Label nodes
// Labels are assumed to be in the same Level
// The nodes between Labels are merged inside the first block
// Constraints:
// - No symbol written in a block can be read in another block
// - No ports
// - Memory accesses: tolerate write conflicts only if the appropriate flag is given
hier_node_t* Hier_Merge(chain_list* list_labels, int flags) {
	if(ChainList_Count(list_labels) < 3) return NULL;

	// FIXME Check that Labels are in the same Level
	// FIXME Check that symbols written in each Block are not read in other Blocks

	// Note: it is the control flow of the last Block that is kept
	// So, first get the last Block, then insert the previous Blocks.

	// Reverse the list of Labels
	chain_list* revlist_labels = reverse(dupchain(list_labels));

	hier_node_t* nextLabel = revlist_labels->DATA;
	hier_node_t* prevLabel = revlist_labels->NEXT->DATA;

	// Here is the destination Block. Other blocks are duplicated and merged inside.
	dbgprintf("Creating destination Block from Labels %s and %s\n", prevLabel->NODE.LABEL.name, nextLabel->NODE.LABEL.name);
	hier_node_t* destBlock = Hier_Level_Dup_stopnode(prevLabel->NEXT, nextLabel);

	chain_list* scanPrevLabel = revlist_labels->NEXT->NEXT;
	nextLabel = prevLabel;
	while(scanPrevLabel!=NULL) {
		prevLabel = scanPrevLabel->DATA;
		hier_node_t* curBlock = prevLabel->NEXT;

		dbgprintf("Beginning merge between Labels %s and %s\n", prevLabel->NODE.LABEL.name, nextLabel->NODE.LABEL.name);

		int z = Hier_MergeBlocks(destBlock, NULL, curBlock, nextLabel, flags);
		if(z!=0) {
			dbgprintf("Merge failed with error code %i\n", z);
			Hier_FreeNodeLevel(destBlock);
			freechain(revlist_labels);
			return NULL;
		}

		nextLabel = prevLabel;
		scanPrevLabel = scanPrevLabel->NEXT;
	}

	freechain(revlist_labels);

	return destBlock;
}

int Hier_Merge_Replace(implem_t* Implem, chain_list* list_labels, int flags) {
	if(ChainList_Count(list_labels) < 3) return __LINE__;

	// Get the first and last Labels
	hier_node_t* firstLabel = list_labels->DATA;
	chain_list* scan = list_labels;
	while(scan->NEXT != NULL) scan = scan->NEXT;
	hier_node_t* lastLabel = scan->DATA;

	hier_node_t* newBlock = Hier_Merge(list_labels, flags);
	if(newBlock==NULL) return __LINE__;

	// Cut links at Labels and free the previous blocks
	hier_node_t* prev_level = firstLabel->NEXT;
	Hier_CutLinkAfter(firstLabel);
	Hier_CutLinkBefore(lastLabel);

	// Free the previous blocks
	Hier_Lists_RemoveLevel(Implem->H, prev_level);
	// Add the new block to lists of nodes
	Hier_Lists_AddLevel(Implem->H, newBlock);

	// Temporarily link both Labels and link the new block in between
	Hier_Nodes_Link(firstLabel, lastLabel);
	Hier_InsertLevelAfterNode(firstLabel, newBlock);

	return 0;
}


