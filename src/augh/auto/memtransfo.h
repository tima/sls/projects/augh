
#ifndef _MEMTRANSFO_H_
#define _MEMTRANSFO_H_

#include "auto.h"


int ArrayOccur_LittAddr(implem_t* Implem, char* name, avl_ip_tree_t* tree_address);

int ReplaceMem_comp(implem_t* Implem, netlist_comp_t* comp);
int ReplaceMem_name(implem_t* Implem, char* name);
int ReplaceMem_ByName_verbose(implem_t* Implem, char* name);

int Mem_InsertReadBuf(implem_t* Implem, netlist_comp_t* comp);

netlist_comp_t* Mem_GetReadBuf_HierOnly(implem_t* Implem, netlist_comp_t* comp);


#endif  // _MEMTRANSFO_H_

