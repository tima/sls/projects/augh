
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <time.h>    // To display the scheduling time

#include "../chain.h"
#include "auto.h"
#include "linelist.h"
#include "hwres.h"
#include "addops.h"
#include "../hvex/hvex_dico.h"
#include "../hvex/hvex_misc.h"

#include "../netlist/netlist.h"
#include "../netlist/netlist_comps.h"
#include "../netlist/netlist_access.h"



//=========================================================
// Allocation functions
//=========================================================

// Create a pool of type : hw_res_t

#define POOL_prefix      pool_hwres
#define POOL_data_t      hw_res_t
#define POOL_ALLOC_SIZE  1000

#define POOL_INSTANCE             POOL_HWRES
#define POOL_INSTANCE_ATTRIBUTES  static

#include "../pool.h"

// Allocation wrappers

hw_res_t* hwres_new() {
	hw_res_t* elt = pool_hwres_pop(&POOL_HWRES);
	memset(elt, 0, sizeof(*elt));
	elt->direct_avail = false;
	elt->direct_need = false;
	return elt;
}
void hwres_free(hw_res_t* elt) {
	if(elt==NULL) return;
	VexList_Free(elt->vexes_r);
	VexList_Free(elt->vexes_w);
	pool_hwres_push(&POOL_HWRES, elt);
}


// Create a pool of type : hwres_heap_t

#define POOL_prefix      pool_hwres_heap
#define POOL_data_t      hwres_heap_t
#define POOL_ALLOC_SIZE  1000

#define POOL_INSTANCE             POOL_HWRES_HEAP
#define POOL_INSTANCE_ATTRIBUTES  static

#include "../pool.h"

// Allocation wrappers

hwres_heap_t* hwres_heap_new() {
	hwres_heap_t* heap = pool_hwres_heap_pop(&POOL_HWRES_HEAP);
	memset(heap, 0, sizeof(*heap));
	avl_pp_init(&heap->type_reg);
	avl_pp_init(&heap->type_mem);
	avl_pp_init(&heap->type_ops);
	return heap;
}
void hwres_heap_free(hwres_heap_t* heap) {
	if(heap==NULL) return;
	// Free the data
	avl_pp_foreach(&heap->type_reg, avl_node) hwres_free(avl_node->data);
	avl_pp_reset(&heap->type_reg);
	avl_pp_foreach(&heap->type_mem, avl_node) hwres_free(avl_node->data);
	avl_pp_reset(&heap->type_mem);
	avl_pp_foreach(&heap->type_ops, avl_node) hwres_free(avl_node->data);
	avl_pp_reset(&heap->type_ops);
	// Free the heap structure
	pool_hwres_heap_push(&POOL_HWRES_HEAP, heap);
}



//=========================================================
// Misc Functions
//=========================================================

void hwres_print(hw_res_t* ptr_hwres) {
	printf("Resource Type : ");
	if(ptr_hwres->type==HWRES_TYPE_REG) {
		printf("REG name '%s'", ptr_hwres->name);
		printf(" read=%d write=%d", ptr_hwres->ports_r, ptr_hwres->ports_w);
	}
	else if(ptr_hwres->type==HWRES_TYPE_MEM) {
		printf("MEM name '%s'", ptr_hwres->name);
		printf(" RA %d (vex %u) WA %d (vex %u)",
			ptr_hwres->ports_r, ChainList_Count(ptr_hwres->vexes_r),
			ptr_hwres->ports_w, ChainList_Count(ptr_hwres->vexes_w)
		);
		printf(" direct %c%c", ptr_hwres->direct_avail==true ? 'A' : '-', ptr_hwres->direct_need==true ? 'N' : '-');
		if(ptr_hwres->direct_avail==true || ptr_hwres->direct_need==true) {
			printf(" (RA %d WA %d)", ptr_hwres->ports_r_ifdirect, ptr_hwres->ports_w_ifdirect);
		}
	}
	else if(ptr_hwres->type==HWRES_TYPE_OP) {
		printf("OP name '%s'", ptr_hwres->name);
		printf(" nb %d (vex %u)", ptr_hwres->number, ChainList_Count(ptr_hwres->vexes_r));
	}
	printf("\n");
}

ptype_list* hwres_heap_to_op_names(hwres_heap_t* heap) {
	ptype_list* list_names = NULL;
	avl_pp_foreach(&heap->type_ops, avl_node) {
		hw_res_t* elt = avl_node->data;
		list_names = addptype(list_names, elt->number, elt->name);
	}
	return list_names;
}

hw_res_t* hwres_dup(hw_res_t* elt) {
	hw_res_t *new_elt = hwres_new();
	*new_elt = *elt;
	new_elt->vexes_r = VexList_Dup(new_elt->vexes_r);
	new_elt->vexes_w = VexList_Dup(new_elt->vexes_w);
	return new_elt;
}

hwres_heap_t* hwres_heap_dup(hwres_heap_t* heap) {
	if(heap==NULL) return NULL;
	hwres_heap_t* newheap = hwres_heap_new();

	avl_pp_dup(&newheap->type_reg, &heap->type_reg);
	avl_pp_foreach(&newheap->type_reg, avl_node) avl_node->data = hwres_dup(avl_node->data);

	avl_pp_dup(&newheap->type_mem, &heap->type_mem);
	avl_pp_foreach(&newheap->type_mem, avl_node) avl_node->data = hwres_dup(avl_node->data);

	avl_pp_dup(&newheap->type_ops, &heap->type_ops);
	avl_pp_foreach(&newheap->type_ops, avl_node) avl_node->data = hwres_dup(avl_node->data);

	return newheap;
}

void hwres_heap_print(hwres_heap_t* heap, indent_t* indent) {
	if(heap==NULL) return;
	avl_pp_foreach(&heap->type_reg, avl_node) {
		indentprint(indent);
		hwres_print(avl_node->data);
	}
	avl_pp_foreach(&heap->type_mem, avl_node) {
		indentprint(indent);
		hwres_print(avl_node->data);
	}
	avl_pp_foreach(&heap->type_ops, avl_node) {
		indentprint(indent);
		hwres_print(avl_node->data);
	}
}



//=========================================================
// Clean
//=========================================================

// Remove from the list the resources needed, handle VEX sharing.
// return 0 if OK, !=0 if not enough resources were present
static bool hwres_elt_isempty(hw_res_t* hwres_elt) {

	if(hwres_elt->type==HWRES_TYPE_REG) {
		if(hwres_elt->ports_w>0) return false;
		return true;
	}

	else if(hwres_elt->type==HWRES_TYPE_MEM) {
		if(hwres_elt->ports_w>0) return false;
		if(hwres_elt->ports_r>0) return false;
		if(hwres_elt->ports_w_ifdirect>0) return false;
		if(hwres_elt->ports_r_ifdirect>0) return false;
		if(hwres_elt->vexes_r!=NULL) return false;
		if(hwres_elt->vexes_w!=NULL) return false;
		if(hwres_elt->direct_avail==true) return false;
		if(hwres_elt->direct_need==true) return false;
		return true;
	}

	// Here it is a "computing" operator
	else if(hwres_elt->type==HWRES_TYPE_OP) {
		if(hwres_elt->number>0) return false;
		if(hwres_elt->vexes_r!=NULL) return false;
		return true;
	}

	return true;
}

void hwres_heap_remove_vex(hwres_heap_t* heap) {

	// Nothing to remove about registers/signals

	// Memory accesses
	avl_pp_foreach(&heap->type_mem, avl_node) {
		hw_res_t* elt = avl_node->data;
		VexList_Free(elt->vexes_r);
		elt->vexes_r = NULL;
		VexList_Free(elt->vexes_w);
		elt->vexes_w = NULL;
	}

	// Operators
	avl_pp_foreach(&heap->type_ops, avl_node) {
		hw_res_t* elt = avl_node->data;
		VexList_Free(elt->vexes_r);
		elt->vexes_r = NULL;
	}

}

void hwres_heap_remove_empty(hwres_heap_t* heap) {
	avl_pp_node_t* avl_node;
	avl_pp_node_t* next_node;

	for(avl_node=avl_pp_first(&heap->type_reg); avl_node!=NULL; avl_node=next_node) {
		next_node = avl_pp_next(&heap->type_reg, avl_node);
		if(hwres_elt_isempty(avl_node->data)==false) continue;
		hwres_free(avl_node->data);
		avl_pp_rem(&heap->type_reg, avl_node);
	}

	for(avl_node=avl_pp_first(&heap->type_mem); avl_node!=NULL; avl_node=next_node) {
		next_node = avl_pp_next(&heap->type_mem, avl_node);
		if(hwres_elt_isempty(avl_node->data)==false) continue;
		hwres_free(avl_node->data);
		avl_pp_rem(&heap->type_mem, avl_node);
	}

	for(avl_node=avl_pp_first(&heap->type_ops); avl_node!=NULL; avl_node=next_node) {
		next_node = avl_pp_next(&heap->type_ops, avl_node);
		if(hwres_elt_isempty(avl_node->data)==false) continue;
		hwres_free(avl_node->data);
		avl_pp_rem(&heap->type_ops, avl_node);
	}
}

void hwres_heap_remove_ops(hwres_heap_t* heap) {
	if(heap==NULL) return;
	// Free the data
	avl_pp_foreach(&heap->type_ops, avl_node) hwres_free(avl_node->data);
	avl_pp_reset(&heap->type_ops);
}

bool hwres_heap_isempty(hwres_heap_t* heap) {
	if(heap==NULL) return true;
	if(avl_pp_isempty(&heap->type_reg)==false) return false;
	if(avl_pp_isempty(&heap->type_mem)==false) return false;
	if(avl_pp_isempty(&heap->type_ops)==false) return false;
	return true;
}
bool hwres_heap_isempty_noreg(hwres_heap_t* heap) {
	if(heap==NULL) return true;
	if(avl_pp_isempty(&heap->type_mem)==false) return false;
	if(avl_pp_isempty(&heap->type_ops)==false) return false;
	return true;
}

// Compare the content of two heaps
// Return zero of equal
int hwres_heap_cmp(hwres_heap_t* heap0, hwres_heap_t* heap1) {

	if( avl_pp_count(&heap0->type_reg) != avl_pp_count(&heap1->type_reg) ) return __LINE__;
	if( avl_pp_count(&heap0->type_mem) != avl_pp_count(&heap1->type_mem) ) return __LINE__;
	if( avl_pp_count(&heap0->type_ops) != avl_pp_count(&heap1->type_ops) ) return __LINE__;

	avl_pp_foreach(&heap0->type_reg, node0) {
		hw_res_t* elt0 = node0->data;

		hw_res_t* elt1 = hwres_heap_reg_find(heap1, elt0->name);
		if(elt1==NULL) return __LINE__;

		if(elt0->ports_r!=elt1->ports_r) return __LINE__;
		if(elt0->ports_w!=elt1->ports_w) return __LINE__;
	}

	avl_pp_foreach(&heap0->type_mem, node0) {
		hw_res_t* elt0 = node0->data;

		hw_res_t* elt1 = hwres_heap_mem_find(heap1, elt0->name);
		if(elt1==NULL) return __LINE__;

		if(elt0->direct_avail!=elt1->direct_avail) return __LINE__;
		if(elt0->direct_need!=elt1->direct_need) return __LINE__;

		if(elt0->ports_r!=elt1->ports_r) return __LINE__;
		if(elt0->ports_w!=elt1->ports_w) return __LINE__;
		if(elt0->ports_r_ifdirect!=elt1->ports_r_ifdirect) return __LINE__;
		if(elt0->ports_w_ifdirect!=elt1->ports_w_ifdirect) return __LINE__;

		if( (elt0->vexes_r==NULL) != (elt1->vexes_r==NULL) ) return __LINE__;
		if( (elt0->vexes_w==NULL) != (elt1->vexes_w==NULL) ) return __LINE__;
		if( VexList_Compare_SameOper(elt0->vexes_r, elt1->vexes_r)!=0 ) return __LINE__;
		if( VexList_Compare_SameOper(elt0->vexes_w, elt1->vexes_w)!=0 ) return __LINE__;
	}

	avl_pp_foreach(&heap0->type_ops, node0) {
		hw_res_t* elt0 = node0->data;

		hw_res_t* elt1 = hwres_heap_op_find(heap1, elt0->name);
		if(elt1==NULL) return __LINE__;

		if(elt0->number!=elt1->number) return __LINE__;

		if( (elt0->vexes_r==NULL) != (elt1->vexes_r==NULL) ) return __LINE__;
		if(elt0->vexes_r!=NULL && elt1->vexes_r!=NULL) {
			if( VexList_Compare_SameOper(elt0->vexes_r, elt1->vexes_r)!=0 ) return __LINE__;
		}
	}

	return 0;
}



//=========================================================
// Add needed resources
//=========================================================

void hwres_heap_add_heap_needed(hwres_heap_t* heap, hwres_heap_t* src) {

	avl_pp_foreach(&src->type_reg, src_node) {
		hw_res_t* src_elt = src_node->data;
		avl_pp_node_t* avl_node = NULL;
		int z = avl_pp_add(&heap->type_reg, src_elt->name, &avl_node);
		if(z==true) avl_node->data = hwres_dup(src_elt);
		else {
			hw_res_t* dest_elt = avl_node->data;
			dest_elt->ports_w += src_elt->ports_w;
		}
	}

	avl_pp_foreach(&src->type_mem, src_node) {
		hw_res_t* src_elt = src_node->data;
		avl_pp_node_t* avl_node = NULL;
		int z = avl_pp_add(&heap->type_mem, src_elt->name, &avl_node);
		if(z==true) avl_node->data = hwres_dup(src_elt);
		else {
			hw_res_t* dest_elt = avl_node->data;
			// No sharing for the Write ports. Addresses kept because of direct access.
			foreach(src_elt->vexes_w, scanvex) {
				hvex_t* vex_needed = scanvex->DATA;
				dest_elt->vexes_w = addchain(dest_elt->vexes_w, hvex_dup(vex_needed));
				dest_elt->ports_w ++;
				if(vex_needed->model!=HVEX_LITERAL) dest_elt->ports_w_ifdirect ++;
			}
			// For Read ports, merge with similar VEX already listed.
			// The expressions are the accessed addresses.
			foreach(src_elt->vexes_r, scanvex) {
				hvex_t* vex_needed = scanvex->DATA;
				if(vex_needed!=NULL && VexList_IsHere_SameOper(dest_elt->vexes_r, vex_needed)) continue;
				dest_elt->vexes_r = addchain(dest_elt->vexes_r, hvex_dup(vex_needed));
				dest_elt->ports_r ++;
				if(vex_needed->model!=HVEX_LITERAL) dest_elt->ports_r_ifdirect ++;
			}
		}
	}

	avl_pp_foreach(&src->type_ops, src_node) {
		hw_res_t* src_elt = src_node->data;
		avl_pp_node_t* avl_node = NULL;
		int z = avl_pp_add(&heap->type_ops, src_elt->name, &avl_node);
		if(z==true) avl_node->data = hwres_dup(src_elt);
		else {
			hw_res_t* dest_elt = avl_node->data;
			// Merge with similar VEX already listed
			foreach(src_elt->vexes_r, scanvex) {
				hvex_t* vex_needed = scanvex->DATA;
				if(vex_needed!=NULL && VexList_IsHere_SameOper(dest_elt->vexes_r, vex_needed)) continue;
				dest_elt->vexes_r = addchain(dest_elt->vexes_r, hvex_dup(vex_needed));
				dest_elt->number ++;
			}
		}
	}

}

void hwres_heap_add_heap_avail(hwres_heap_t* heap, hwres_heap_t* src) {

	avl_pp_foreach(&src->type_reg, src_node) {
		hw_res_t* src_elt = src_node->data;
		hw_res_t* dest_elt = hwres_heap_reg_getadd(heap, src_elt->name);
		dest_elt->ports_r += src_elt->ports_r;
		dest_elt->ports_w += src_elt->ports_w;
	}

	avl_pp_foreach(&src->type_mem, src_node) {
		hw_res_t* src_elt = src_node->data;
		hw_res_t* dest_elt = hwres_heap_mem_getadd(heap, src_elt->name);
		dest_elt->ports_r += src_elt->ports_r;
		dest_elt->vexes_r = VexList_Merge_Dup(dest_elt->vexes_r, src_elt->vexes_r);
		dest_elt->ports_w += src_elt->ports_w;
		dest_elt->vexes_w = VexList_Merge_Dup(dest_elt->vexes_w, src_elt->vexes_w);
	}

	avl_pp_foreach(&src->type_ops, src_node) {
		hw_res_t* src_elt = src_node->data;
		hw_res_t* dest_elt = hwres_heap_op_getadd(heap, src_elt->name);
		dest_elt->number += src_elt->number;
		dest_elt->vexes_r = VexList_Merge_Dup(dest_elt->vexes_r, src_elt->vexes_r);
	}

}



//=========================================================
// Build the needed available resources
//=========================================================

// src contains needed resources (not modified)
// dest contains available resources (modified)
void hwres_heap_getmax_need(hwres_heap_t* dest, hwres_heap_t* src) {

	// Registers/signals/ports
	avl_pp_foreach(&src->type_reg, src_node) {
		hw_res_t* src_elt = src_node->data;
		hw_res_t* dest_elt = hwres_heap_reg_getadd(dest, src_elt->name);
		if(src_elt->ports_w > dest_elt->ports_w) dest_elt->ports_w = src_elt->ports_w;
		if(src_elt->ports_r > dest_elt->ports_r) dest_elt->ports_r = src_elt->ports_r;
	}

	// Memory accesses
	avl_pp_foreach(&src->type_mem, src_node) {
		hw_res_t* src_elt = src_node->data;
		hw_res_t* dest_elt = hwres_heap_mem_getadd(dest, src_elt->name);
		if(src_elt->direct_need==true) dest_elt->direct_need = true;
		if(src_elt->ports_w > dest_elt->ports_w) dest_elt->ports_w = src_elt->ports_w;
		if(src_elt->ports_r > dest_elt->ports_r) dest_elt->ports_r = src_elt->ports_r;
		if(src_elt->ports_w_ifdirect > dest_elt->ports_w_ifdirect) dest_elt->ports_w_ifdirect = src_elt->ports_w_ifdirect;
		if(src_elt->ports_r_ifdirect > dest_elt->ports_r_ifdirect) dest_elt->ports_r_ifdirect = src_elt->ports_r_ifdirect;
	}

	// Operators
	avl_pp_foreach(&src->type_ops, src_node) {
		hw_res_t* src_elt = src_node->data;
		hw_res_t* dest_elt = hwres_heap_op_getadd(dest, src_elt->name);
		if(src_elt->number > dest_elt->number) dest_elt->number = src_elt->number;
	}

}

// Remove from heap0 what is already needed in heap1
void hwres_heap_sub_shareneed(hwres_heap_t* heap0, hwres_heap_t* heap1) {

	// Nothing to share about registers

	// The memory accesses
	avl_pp_foreach(&heap0->type_mem, node0) {
		hw_res_t* elt0 = node0->data;

		hw_res_t* elt1 = hwres_heap_mem_find(heap1, elt0->name);
		if(elt1==NULL) continue;

		// Handle Direct accesses

		if(elt1->direct_need==true) elt0->direct_need = false;

		// Handle Read accesses

		chain_list* remain_vex = NULL;
		unsigned remain_vex_nb = 0;

		foreach(elt0->vexes_r, scan0) {
			hvex_t* vex_needed = scan0->DATA;
			if(elt1->direct_avail==true || elt1->direct_need==true) {
				if(vex_needed->model==HVEX_LITERAL) continue;
			}
			// If the VEX exists, don't add it in the remaining VEX
			if(VexList_IsHere_SameOper(elt1->vexes_r, vex_needed)==true) continue;
			// Add the VEX in the remaining VEX
			remain_vex = addchain(remain_vex, hvex_dup(vex_needed));
			remain_vex_nb ++;
		}

		// Replace in heap0
		VexList_Free(elt0->vexes_r);
		elt0->vexes_r = remain_vex;
		elt0->ports_r = remain_vex_nb;
		// Testing whether direct access is available is not needed
		elt0->ports_r_ifdirect = remain_vex_nb;

	}  // Memory accesses

	// The operators
	avl_pp_foreach(&heap0->type_ops, node0) {
		hw_res_t* elt0 = node0->data;

		hw_res_t* elt1 = hwres_heap_op_find(heap1, elt0->name);
		if(elt1==NULL) continue;

		chain_list* remain_vex = NULL;
		unsigned remain_vex_nb = 0;

		foreach(elt0->vexes_r, scan0) {
			hvex_t* vex_needed = scan0->DATA;
			// If the VEX exists, don't add it in the remaining VEX
			if(VexList_IsHere_SameOper(elt1->vexes_r, vex_needed)==true) continue;
			// Add the VEX in the remaining VEX
			remain_vex = addchain(remain_vex, hvex_dup(vex_needed));
			remain_vex_nb ++;
		}

		// Replace in heap0
		VexList_Free(elt0->vexes_r);
		elt0->vexes_r = remain_vex;
		elt0->number = remain_vex_nb;

	}  // Operators

}

// Remove from heap_avail (and add vexes) what is needed in heap_need
int hwres_heap_sub_fromavail(hwres_heap_t* heap_avail, hwres_heap_t* heap_need, bool noerr, bool subsrc) {

	// The registers/signals
	avl_pp_foreach(&heap_need->type_reg, node_need) {
		hw_res_t* elt_need = node_need->data;

		hw_res_t* elt_avail = hwres_heap_reg_find(heap_avail, elt_need->name);
		if(elt_avail==NULL) {
			if(noerr==false) return __LINE__;
			else continue;
		}

		// The Write accesses

		if(elt_need->ports_w > 0) {
			if(elt_avail->ports_w < 0) {  // Unlimited
				if(subsrc==true) elt_need->ports_w = 0;
			}
			else if(elt_need->ports_w <= elt_avail->ports_w) {
				elt_avail->ports_w -= elt_need->ports_w;
				if(subsrc==true) elt_need->ports_w = 0;
			}
			else {  // Not enough accesses
				if(noerr==false) return __LINE__;
				if(subsrc==true) elt_need->ports_w -= elt_avail->ports_w;
				elt_avail->ports_w = 0;
			}
		}

		// The Read accesses

		if(elt_need->ports_r > 0) {
			if(elt_avail->ports_r < 0) {  // Unlimited
				if(subsrc==true) elt_need->ports_r = 0;
			}
			else if(elt_need->ports_r <= elt_avail->ports_r) {
				elt_avail->ports_r -= elt_need->ports_r;
				if(subsrc==true) elt_need->ports_r = 0;
			}
			else {  // Not enough accesses
				if(noerr==false) return __LINE__;
				if(subsrc==true) elt_need->ports_r -= elt_avail->ports_r;
				elt_avail->ports_r = 0;
			}
		}
	}

	// The memory accesses
	avl_pp_foreach(&heap_need->type_mem, node_need) {
		hw_res_t* elt_need = node_need->data;

		hw_res_t* elt_avail = hwres_heap_mem_find(heap_avail, elt_need->name);
		if(elt_avail==NULL) {
			if(noerr==false) return __LINE__;
			else continue;
		}

		// Handle the Direct accesses

		if(elt_avail->direct_avail==true) {
			if(subsrc==true) elt_need->direct_need = false;
		}

		// Handle the Write accesses
		// Note: The VEX addresses are not shared for Write accesses

		if(elt_need->vexes_w!=NULL) {
			// The remaining VEX in elt_need
			chain_list* remain_vex = NULL;
			unsigned remain_vex_nb = 0;

			foreach(elt_need->vexes_w, scanvex) {
				hvex_t* vex_needed = scanvex->DATA;
				if(elt_avail->direct_avail==true) {
					if(vex_needed->model==HVEX_LITERAL) continue;
				}
				// A new port must be taken
				if(elt_avail->ports_w < 1) {
					if(noerr==false) { VexList_Free(remain_vex); return __LINE__; }
					if(subsrc==true) {
						remain_vex = addchain(remain_vex, hvex_dup(vex_needed));
						remain_vex_nb ++;
					}
				}
				else {
					elt_avail->vexes_w = addchain(elt_avail->vexes_w, hvex_dup(vex_needed));
					elt_avail->ports_w --;
				}
			}
			if(subsrc==true) {
				VexList_Free(elt_need->vexes_w);
				elt_need->vexes_w = remain_vex;
				elt_need->ports_w = remain_vex_nb;
				// Here, checking whether direct access is available is not useful
				elt_need->ports_w_ifdirect = remain_vex_nb;
			}
		}  // Write accesses

		// Handle the Read accesses

		if(elt_need->vexes_r!=NULL) {
			// The remaining VEX in elt_need
			chain_list* remain_vex = NULL;
			unsigned remain_vex_nb = 0;

			foreach(elt_need->vexes_r, scanvex) {
				hvex_t* vex_needed = scanvex->DATA;
				if(elt_avail->direct_avail==true) {
					if(vex_needed->model==HVEX_LITERAL) continue;
				}
				if(VexList_IsHere_SameOper(elt_avail->vexes_r, vex_needed)==true) continue;
				// A new port must be taken
				if(elt_avail->ports_r < 1) {
					if(noerr==false) { VexList_Free(remain_vex); return __LINE__; }
					if(subsrc==true) {
						remain_vex = addchain(remain_vex, hvex_dup(vex_needed));
						remain_vex_nb ++;
					}
				}
				else {
					elt_avail->vexes_r = addchain(elt_avail->vexes_r, hvex_dup(vex_needed));
					elt_avail->ports_r --;
				}
			}
			if(subsrc==true) {
				VexList_Free(elt_need->vexes_r);
				elt_need->vexes_r = remain_vex;
				elt_need->ports_r = remain_vex_nb;
				// Here, checking whether direct access is available is not useful
				elt_need->ports_r_ifdirect = remain_vex_nb;
			}
		}  // Read accesses

	}  // Memory accesses

	// Operators
	avl_pp_foreach(&heap_need->type_ops, node_need) {
		hw_res_t* elt_need = node_need->data;

		hw_res_t* elt_avail = hwres_heap_op_find(heap_avail, elt_need->name);
		if(elt_avail==NULL) {
			if(noerr==false) return __LINE__;
			else continue;
		}

		if(elt_need->vexes_r!=NULL) {
			// The remaining VEX in elt_need
			chain_list* remain_vex = NULL;
			unsigned remain_vex_nb = 0;

			foreach(elt_need->vexes_r, scanvex) {
				hvex_t* vex_needed = scanvex->DATA;
				if(VexList_IsHere_SameOper(elt_avail->vexes_r, vex_needed)==true) continue;
				// A new operator must be used
				if(elt_avail->number < 1) {
					if(noerr==false) { VexList_Free(remain_vex); return __LINE__; }
					if(subsrc==true) {
						remain_vex = addchain(remain_vex, hvex_dup(vex_needed));
						remain_vex_nb ++;
					}
				}
				else {
					elt_avail->vexes_r = addchain(elt_avail->vexes_r, hvex_dup(vex_needed));
					elt_avail->number --;
				}
			}
			if(subsrc==true) {
				VexList_Free(elt_need->vexes_r);
				elt_need->vexes_r = remain_vex;
				elt_need->number = remain_vex_nb;
			}
		}

	}  // Operators

	return 0;
}



//=========================================================
// Versions meant to be used only for freedom degrees
//=========================================================

void hwres_heap_add_heap_avail_forFD(hwres_heap_t* heap, hwres_heap_t* src) {

	// Registers/signals/ports
	avl_pp_foreach(&src->type_reg, src_node) {
		hw_res_t* src_elt = src_node->data;
		hw_res_t* dest_elt = hwres_heap_reg_getadd(heap, src_elt->name);
		dest_elt->ports_r += src_elt->ports_r;
		dest_elt->ports_w += src_elt->ports_w;
	}

	// The memory accesses
	avl_pp_foreach(&src->type_mem, src_node) {
		hw_res_t* src_elt = src_node->data;
		hw_res_t* dest_elt = hwres_heap_mem_getadd(heap, src_elt->name);
		dest_elt->ports_r += src_elt->ports_r;
		dest_elt->ports_w += src_elt->ports_w;
	}

	// Operators
	avl_pp_foreach(&src->type_ops, src_node) {
		hw_res_t* src_elt = src_node->data;
		hw_res_t* dest_elt = hwres_heap_op_getadd(heap, src_elt->name);
		dest_elt->number += src_elt->number;
	}

}

// Check if some needed resources are compatible with some available resources
bool hwres_heap_check_avail_forFD(hwres_heap_t* heap_avail, hwres_heap_t* heap_need) {

	avl_pp_foreach(&heap_need->type_reg, node_need) {
		hw_res_t* elt_need = node_need->data;
		// Get the available resource
		hw_res_t* elt_avail = hwres_heap_reg_find(heap_avail, elt_need->name);
		if(elt_avail==NULL) return false;
		// Check content
		if(elt_avail->ports_r >= 0 && elt_need->ports_r > elt_avail->ports_r) return false;
		if(elt_avail->ports_w >= 0 && elt_need->ports_w > elt_avail->ports_w) return false;
	}

	avl_pp_foreach(&heap_need->type_mem, node_need) {
		hw_res_t* elt_need = node_need->data;
		// Get the available resource
		hw_res_t* elt_avail = hwres_heap_mem_find(heap_avail, elt_need->name);
		if(elt_avail==NULL) return false;
		// Check content
		if(elt_avail->direct_avail==true) {
			if(elt_need->ports_r_ifdirect > elt_avail->ports_r) return false;
			if(elt_need->ports_w_ifdirect > elt_avail->ports_w) return false;
		}
		else {
			if(elt_need->direct_need==true) return false;
			if(elt_need->ports_r > elt_avail->ports_r) return false;
			if(elt_need->ports_w > elt_avail->ports_w) return false;
		}
	}

	avl_pp_foreach(&heap_need->type_ops, node_need) {
		hw_res_t* elt_need = node_need->data;
		// Get the available resource
		hw_res_t* elt_avail = hwres_heap_op_find(heap_avail, elt_need->name);
		if(elt_avail==NULL) return false;
		// Check content
		if(elt_need->number > elt_avail->number) return false;
	}

	return true;
}

// Remove from heap_need what is already provided in heap_avail (no VEX)
// heap_avail is not modified
// Note: This is mostly needed for freedom degree- related operations
void hwres_heap_sub_fromavail_forFD(hwres_heap_t* heap_avail, hwres_heap_t* heap_need) {

	// Registers/signals/ports
	avl_pp_foreach(&heap_need->type_reg, node_need) {
		hw_res_t* elt_need = node_need->data;

		hw_res_t* elt_avail = hwres_heap_reg_find(heap_avail, elt_need->name);
		if(elt_avail==NULL) continue;

		// Subtract
		if(elt_avail->ports_w > elt_need->ports_w) elt_need->ports_w = 0;
		else elt_need->ports_w -= elt_avail->ports_w;

		if(elt_avail->ports_r < 0) elt_need->ports_r = 0;
		else {
			if(elt_avail->ports_r > elt_need->ports_r) elt_need->ports_r = 0;
			else elt_need->ports_r -= elt_avail->ports_r;
		}
	}  // Memory accesses

	// The memory accesses
	avl_pp_foreach(&heap_need->type_mem, node_need) {
		hw_res_t* elt_need = node_need->data;

		hw_res_t* elt_avail = hwres_heap_mem_find(heap_avail, elt_need->name);
		if(elt_avail==NULL) continue;

		// Subtract
		if(elt_avail->ports_w > elt_need->ports_w) elt_need->ports_w = 0;
		else elt_need->ports_w -= elt_avail->ports_w;
		if(elt_avail->ports_r > elt_need->ports_r) elt_need->ports_r = 0;
		else elt_need->ports_r -= elt_avail->ports_r;
		if(elt_avail->direct_avail==true) elt_need->direct_need = false;
	}  // Memory accesses

	// Operators
	avl_pp_foreach(&heap_need->type_ops, node_need) {
		hw_res_t* elt_need = node_need->data;

		hw_res_t* elt_avail = hwres_heap_op_find(heap_avail, elt_need->name);
		if(elt_avail==NULL) continue;

		// Subtract
		if(elt_avail->number > elt_need->number) elt_need->number = 0;
		else elt_need->number -= elt_avail->number;
	}  // Operators

}

// Check whether what is needed in heap0 is already needed in heap1
// Note: This is mostly needed for freedom degree- related operations
bool hwres_heap_check_need_included_forFD(hwres_heap_t* heap1, hwres_heap_t* heap0) {

	// Registers/signals/ports
	avl_pp_foreach(&heap0->type_reg, node0) {
		hw_res_t* elt0 = node0->data;

		hw_res_t* elt1 = hwres_heap_reg_find(heap1, elt0->name);
		if(elt1==NULL) return false;

		// Checks
		if(elt0->ports_w > elt1->ports_w) return false;
		if(elt0->ports_r > elt1->ports_r) return false;
	}  // Memory accesses

	// The memory accesses
	avl_pp_foreach(&heap0->type_mem, node0) {
		hw_res_t* elt0 = node0->data;

		hw_res_t* elt1 = hwres_heap_mem_find(heap1, elt0->name);
		if(elt1==NULL) return false;

		// Checks
		if(elt0->ports_w > elt1->ports_w) return false;
		if(elt0->ports_r > elt1->ports_r) return false;
		if(elt0->direct_need==true && elt1->direct_need==false) return false;
	}

	// The operators
	avl_pp_foreach(&heap0->type_ops, node0) {
		hw_res_t* elt0 = node0->data;

		hw_res_t* elt1 = hwres_heap_op_find(heap1, elt0->name);
		if(elt1==NULL) return false;

		// Checks
		if(elt0->number > elt1->number) return false;
	}

	return true;
}

// Compare the content of two heaps
// Return zero of equal
int hwres_heap_cmp_forFD(hwres_heap_t* heap0, hwres_heap_t* heap1) {

	if( avl_pp_count(&heap0->type_reg) != avl_pp_count(&heap1->type_reg) ) return __LINE__;
	if( avl_pp_count(&heap0->type_mem) != avl_pp_count(&heap1->type_mem) ) return __LINE__;
	if( avl_pp_count(&heap0->type_ops) != avl_pp_count(&heap1->type_ops) ) return __LINE__;

	avl_pp_foreach(&heap0->type_reg, node0) {
		hw_res_t* elt0 = node0->data;

		hw_res_t* elt1 = hwres_heap_reg_find(heap1, elt0->name);
		if(elt1==NULL) return __LINE__;

		if(elt0->ports_r!=elt1->ports_r) return __LINE__;
		if(elt0->ports_w!=elt1->ports_w) return __LINE__;
	}

	avl_pp_foreach(&heap0->type_mem, node0) {
		hw_res_t* elt0 = node0->data;

		hw_res_t* elt1 = hwres_heap_mem_find(heap1, elt0->name);
		if(elt1==NULL) return __LINE__;

		if(elt0->direct_avail!=elt1->direct_avail) return __LINE__;
		if(elt0->direct_need!=elt1->direct_need) return __LINE__;

		if(elt0->ports_r!=elt1->ports_r) return __LINE__;
		if(elt0->ports_w!=elt1->ports_w) return __LINE__;
		if(elt0->ports_r_ifdirect!=elt1->ports_r_ifdirect) return __LINE__;
		if(elt0->ports_w_ifdirect!=elt1->ports_w_ifdirect) return __LINE__;
	}

	avl_pp_foreach(&heap0->type_ops, node0) {
		hw_res_t* elt0 = node0->data;

		hw_res_t* elt1 = hwres_heap_op_find(heap1, elt0->name);
		if(elt1==NULL) return __LINE__;

		if(elt0->number!=elt1->number) return __LINE__;
	}

	return 0;
}



//=========================================================
// Build needed resources
//=========================================================

static void hwres_heap_makeadd_needed_reg(hwres_heap_t* heap, char* name, int num_r, int num_w) {
	hw_res_t* elt = hwres_heap_reg_getadd(heap, name);
	elt->ports_w += num_w;
	elt->ports_r += num_r;
}

// The VEX is an address. Duplicated inside, if needed.
static inline void hwres_elt_add_needed_mem_vex_w(hw_res_t* elt, hvex_t* vex_addr) {
	// No sharing for Write ports, keep addresses for direct access simplification.
	if(vex_addr!=NULL) {
		elt->vexes_w = addchain(elt->vexes_w, hvex_dup(vex_addr));
		elt->ports_w ++;
		if(vex_addr->model!=HVEX_LITERAL) elt->ports_w_ifdirect ++;
	}
	else {
		elt->ports_w ++;
		elt->ports_w_ifdirect ++;
	}
}
static inline void hwres_elt_add_needed_mem_vex_r(hw_res_t* elt, hvex_t* vex_addr) {
	// No sharing for Write ports, keep addresses for direct access simplification.
	if(vex_addr!=NULL && VexList_IsHere_SameOper(elt->vexes_r, vex_addr)==false) {
		elt->vexes_r = addchain(elt->vexes_r, hvex_dup(vex_addr));
		elt->ports_r ++;
		if(vex_addr->model!=HVEX_LITERAL) elt->ports_r_ifdirect ++;
	}
	else {
		elt->ports_r ++;
		elt->ports_r_ifdirect ++;
	}
}
static inline void hwres_elt_add_needed_mem_vex_w_nonull(hw_res_t* elt, hvex_t* vex_addr) {
	// No sharing for Write ports, keep addresses for direct access simplification.
	elt->vexes_w = addchain(elt->vexes_w, hvex_dup(vex_addr));
	elt->ports_w ++;
	if(vex_addr->model!=HVEX_LITERAL) elt->ports_w_ifdirect ++;
}
static inline void hwres_elt_add_needed_mem_vex_r_nonull(hw_res_t* elt, hvex_t* vex_addr) {
	if(VexList_IsHere_SameOper(elt->vexes_r, vex_addr)==false) {
		elt->vexes_r = addchain(elt->vexes_r, hvex_dup(vex_addr));
		elt->ports_r ++;
		if(vex_addr->model!=HVEX_LITERAL) elt->ports_r_ifdirect ++;
	}
}

// The input list is not modified
__attribute__((__unused__))
static void hwres_heap_makeadd_needed_mem_list(hwres_heap_t* heap, char* name, chain_list* vexes_r, chain_list* vexes_w) {
	assert(name!=NULL);
	hw_res_t* elt = hwres_heap_mem_getadd(heap, name);
	foreach(vexes_w, scanvex) hwres_elt_add_needed_mem_vex_w(elt, scanvex->DATA);
	foreach(vexes_r, scanvex) hwres_elt_add_needed_mem_vex_r(elt, scanvex->DATA);
}
static void hwres_heap_makeadd_needed_mem_vex(hwres_heap_t* heap, char* name, hvex_t* vex_r, hvex_t* vex_w) {
	assert(name!=NULL);
	hw_res_t* elt = hwres_heap_mem_getadd(heap, name);
	if(vex_w!=NULL) hwres_elt_add_needed_mem_vex_w_nonull(elt, vex_w);
	if(vex_r!=NULL) hwres_elt_add_needed_mem_vex_r_nonull(elt, vex_r);
}

// The VEX is duplicated inside
static inline void hwres_elt_add_needed_op_vex(hw_res_t* elt, hvex_t* vex) {
	if(vex!=NULL) {
		if(VexList_IsHere_SameOper(elt->vexes_r, vex)==false) {
			elt->vexes_r = addchain(elt->vexes_r, hvex_dup(vex));
			elt->number ++;
		}
	}
	else elt->number ++;
}
static inline void hwres_elt_add_needed_op_vex_nonull(hw_res_t* elt, hvex_t* vex) {
	if(VexList_IsHere_SameOper(elt->vexes_r, vex)==false) {
		elt->vexes_r = addchain(elt->vexes_r, hvex_dup(vex));
		elt->number ++;
	}
}

// The list is not modified
__attribute__((__unused__))
static void hwres_heap_makeadd_needed_op_list(hwres_heap_t* heap, char* type, chain_list* vexes) {
	hw_res_t* elt = hwres_heap_op_getadd(heap, type);
	foreach(vexes, scanvex) hwres_elt_add_needed_op_vex(elt, scanvex->DATA);
}
void hwres_heap_makeadd_needed_op_vex(hwres_heap_t* heap, char* type, hvex_t* vex) {
	hw_res_t* elt = hwres_heap_op_getadd(heap, type);
	hwres_elt_add_needed_op_vex(elt, vex);
}
__attribute__((__unused__))
static void hwres_heap_makeadd_needed_op_nb(hwres_heap_t* heap, char* type, unsigned nb) {
	hw_res_t* elt = hwres_heap_op_getadd(heap, type);
	elt->number += nb;
}


// Study needed resources, internal processes based on HVEX

void hwres_scanvex_init(hwres_scanvex_t* hwres_data) {
	hwres_data->scanned_vex = NULL;
	avl_p_init(&hwres_data->scanned_vexalloc);
	hwres_data->heap_needed = NULL;
	hwres_data->source_lines = NULL;
}

void hwres_scanvex_clear(hwres_scanvex_t* hwres_data) {
	freechain(hwres_data->scanned_vex);
	avl_p_reset(&hwres_data->scanned_vexalloc);
}

// FIXME handle when there are more than 2 operands to the ADD.
// Ditto for other similar operations like MUL, OR, AND (logical ops optional, automatically created)...
void hwres_scanvex_recurs(hvex_t *Expr, hwres_scanvex_t* hwres_data) {
  if(Expr==NULL) return;  // paranoia

	// If that VEX is already listed, do nothing.
	// This implements elimination of common sub-expressions.
	if(VexList_IsHere_SameOper(hwres_data->scanned_vex, Expr)==true) return;

	if(Expr->model==HVEX_LITERAL) {
		return;
	}

	// Case the VEX is a register.
	if(Expr->model==HVEX_VECTOR) {
		hwres_heap_makeadd_needed_reg(hwres_data->heap_needed, hvex_vec_getname(Expr), 1, 0);
	}

	// Read from array
	else if(Expr->model==HVEX_INDEX) {
		hwres_heap_makeadd_needed_mem_vex(hwres_data->heap_needed, hvex_index_getname(Expr), Expr->operands, NULL);
		hwres_scanvex_recurs(Expr->operands, hwres_data);
	}

	// Assignments
	else if(hvex_model_is_asg(Expr->model)==true) {
		hwres_scanvex_recurs(hvex_asg_get_expr(Expr), hwres_data);
		hwres_scanvex_recurs(hvex_asg_get_addr(Expr), hwres_data);
		hwres_scanvex_recurs(hvex_asg_get_cond(Expr), hwres_data);
		char* name = hvex_asg_get_destname(Expr);
		hvex_t* vex_addr = hvex_asg_get_addr(Expr);
		if(vex_addr!=NULL) {
			hwres_heap_makeadd_needed_mem_vex(hwres_data->heap_needed, name, NULL, vex_addr);
		}
		else {
			hwres_heap_makeadd_needed_reg(hwres_data->heap_needed, name, 0, 1);
		}
	}

	// Logic operantions
	else if(
		Expr->model==HVEX_NOT ||
		Expr->model==HVEX_OR  || Expr->model==HVEX_AND  || Expr->model==HVEX_XOR ||
		Expr->model==HVEX_NOR || Expr->model==HVEX_NAND || Expr->model==HVEX_NXOR ||
		Expr->model==HVEX_EQ  || Expr->model==HVEX_NE
	) {
		// The components are automatically generated when needed, counting them here is not necessary.
		// Just scan all the operands
		hvex_foreach(Expr->operands, VexOp) hwres_scanvex_recurs(VexOp, hwres_data);
	}

	// Multiplexers
	else if(Expr->model==HVEX_MUXB) {
		// The component is automatically generated when needed, counting it here is not necessary.
		// Just scan all the operands
		hvex_foreach(Expr->operands, VexOp) hwres_scanvex_recurs(VexOp, hwres_data);
	}
	else if(Expr->model==HVEX_MUXDEC) {
		// The component is automatically generated when needed, counting it here is not necessary.
		// Just scan all the operands
		hvex_foreach(Expr->operands, VexOp) hwres_scanvex_recurs(VexOp, hwres_data);
	}
	else if(Expr->model==HVEX_MUXDECIN) {
		// This is just an operand of MUXDEC, is does no imply hardware.
		// Just scan all the operands
		hvex_foreach(Expr->operands, VexOp) hwres_scanvex_recurs(VexOp, hwres_data);
	}

	// Arithmetic operations
	else if(Expr->model==HVEX_ADD || Expr->model==HVEX_ADDCIN) {
		if( (NETLIST_COMP_ADD->flags & NETLIST_COMPMOD_FLAG_CANSHARE)!=0 ) {
			hwres_heap_makeadd_needed_op_vex(hwres_data->heap_needed, NETLIST_COMP_ADD->name, Expr);
		}
		hvex_foreach(Expr->operands, VexOp) hwres_scanvex_recurs(VexOp, hwres_data);
	}
	else if(
		Expr->model==HVEX_SUB || Expr->model==HVEX_NEG ||
		Expr->model==HVEX_LT  || Expr->model==HVEX_LE  || Expr->model==HVEX_GT  || Expr->model==HVEX_GE ||
		Expr->model==HVEX_LTS || Expr->model==HVEX_LES || Expr->model==HVEX_GTS || Expr->model==HVEX_GES
	) {
		if( (NETLIST_COMP_SUB->flags & NETLIST_COMPMOD_FLAG_CANSHARE)!=0 ) {
			hwres_heap_makeadd_needed_op_vex(hwres_data->heap_needed, NETLIST_COMP_SUB->name, Expr);
		}
		hvex_foreach(Expr->operands, VexOp) hwres_scanvex_recurs(VexOp, hwres_data);
	}
	else if(Expr->model==HVEX_MUL) {
		if( (NETLIST_COMP_MUL->flags & NETLIST_COMPMOD_FLAG_CANSHARE)!=0 ) {
			hwres_heap_makeadd_needed_op_vex(hwres_data->heap_needed, NETLIST_COMP_MUL->name, Expr);
		}
		hvex_foreach(Expr->operands, VexOp) hwres_scanvex_recurs(VexOp, hwres_data);
	}
	else if(Expr->model==HVEX_DIVQ || Expr->model==HVEX_DIVR) {
		hwres_heap_makeadd_needed_op_vex(hwres_data->heap_needed, NETLIST_COMP_DIVQR->name, Expr);
		if( (NETLIST_COMP_DIVQR->flags & NETLIST_COMPMOD_FLAG_CANSHARE)!=0 ) {
			hvex_foreach(Expr->operands, VexOp) hwres_scanvex_recurs(VexOp, hwres_data);
		}
	}

	else if(Expr->model==HVEX_SHL) {
		hvex_t* operand_shift = Expr->operands->next;
		if(operand_shift->model!=HVEX_LITERAL) {
			if( (NETLIST_COMP_SHL->flags & NETLIST_COMPMOD_FLAG_CANSHARE)!=0 ) {
				hwres_heap_makeadd_needed_op_vex(hwres_data->heap_needed, NETLIST_COMP_SHL->name, Expr);
			}
		}
		hvex_foreach(Expr->operands, VexOp) hwres_scanvex_recurs(VexOp, hwres_data);
	}

	else if(Expr->model==HVEX_SHR) {
		hvex_t* operand_shift = Expr->operands->next;
		if(operand_shift->model!=HVEX_LITERAL) {
			if( (NETLIST_COMP_SHR->flags & NETLIST_COMPMOD_FLAG_CANSHARE)!=0 ) {
				hwres_heap_makeadd_needed_op_vex(hwres_data->heap_needed, NETLIST_COMP_SHR->name, Expr);
			}
		}
		hvex_foreach(Expr->operands, VexOp) hwres_scanvex_recurs(VexOp, hwres_data);
	}

	else if(Expr->model==HVEX_ROTL) {
		hvex_t* operand_shift = Expr->operands->next;
		if(operand_shift->model!=HVEX_LITERAL) {
			if( (NETLIST_COMP_ROTL->flags & NETLIST_COMPMOD_FLAG_CANSHARE)!=0 ) {
				hwres_heap_makeadd_needed_op_vex(hwres_data->heap_needed, NETLIST_COMP_ROTL->name, Expr);
			}
		}
		hvex_foreach(Expr->operands, VexOp) hwres_scanvex_recurs(VexOp, hwres_data);
	}
	else if(Expr->model==HVEX_ROTR) {
		hvex_t* operand_shift = Expr->operands->next;
		if(operand_shift->model!=HVEX_LITERAL) {
			if( (NETLIST_COMP_ROTR->flags & NETLIST_COMPMOD_FLAG_CANSHARE)!=0 ) {
				hwres_heap_makeadd_needed_op_vex(hwres_data->heap_needed, NETLIST_COMP_ROTR->name, Expr);
			}
		}
		hvex_foreach(Expr->operands, VexOp) hwres_scanvex_recurs(VexOp, hwres_data);
	}

	// Operations that imply no particular hardware
	else if(Expr->model==HVEX_CONCAT || Expr->model==HVEX_REPEAT || Expr->model==HVEX_RESIZE) {
		// Only scan operands
		hvex_foreach(Expr->operands, VexOp) hwres_scanvex_recurs(VexOp, hwres_data);
	}

	// Operation has a generic callback
	else if(Expr->model->cb_needed_res_hvex != NULL) {
		Expr->model->cb_needed_res_hvex(Expr, hwres_data);
	}

	// Operation is not handled, or there is nothing to do.
	else {
		printf("ERROR %s:%d : HVEX model '%s' not handled.\n", __FILE__, __LINE__, Expr->model->name);
		if(hwres_data->source_lines!=NULL) LineList_Print_be(hwres_data->source_lines, "  Source lines: ", "\n");
		abort();
	}

	hwres_data->scanned_vex = addchain(hwres_data->scanned_vex, Expr);
}

// Build the list of required hardware resources to execute one particular VEX
hwres_heap_t* hwres_heap_getneed_vex(hvex_t* Expr) {
	hwres_scanvex_t hwres_data;
	hwres_scanvex_init(&hwres_data);

	hwres_data.heap_needed = hwres_heap_new();

	// Detect the operations and comparisons that are used
	hwres_scanvex_recurs(Expr, &hwres_data);

	hwres_heap_t* heap_needed = hwres_data.heap_needed;
	hwres_scanvex_clear(&hwres_data);

	return heap_needed;
}
hwres_heap_t* hwres_heap_getneed_hierstate(hier_node_t* node) {
	hwres_scanvex_t hwres_data;
	hwres_scanvex_init(&hwres_data);

	hwres_data.heap_needed = hwres_heap_new();

	// Detect the operations and comparisons that are used
	hier_state_t* state_data = &node->NODE.STATE;
	foreach(state_data->actions, scan) {
		hier_action_t* action = scan->DATA;
		hwres_data.source_lines = action->source_lines;
		hwres_scanvex_recurs(action->expr, &hwres_data);
	}

	hwres_heap_t* heap_needed = hwres_data.heap_needed;
	hwres_scanvex_clear(&hwres_data);

	return heap_needed;
}
hwres_heap_t* hwres_heap_getneed_actwithinline(hier_action_t* action) {
	hwres_scanvex_t hwres_data;
	hwres_scanvex_init(&hwres_data);

	hwres_data.heap_needed = hwres_heap_new();

	chain_list* list_inl_acts = addchain(NULL, action);
	list_inl_acts = Hier_ListActions_AddInline(list_inl_acts);

	// Detect the operations and comparisons that are used
	foreach(list_inl_acts, scan) {
		hier_action_t* this_action = scan->DATA;
		hwres_data.source_lines = this_action->source_lines;
		hwres_scanvex_recurs(this_action->expr, &hwres_data);
	}

	hwres_heap_t* heap_needed = hwres_data.heap_needed;
	hwres_scanvex_clear(&hwres_data);

	freechain(list_inl_acts);

	return heap_needed;
}

static void hwres_heap_getneed_level_internal(hier_node_t* node, hwres_heap_t* heap, bool do_level) {
	while(node!=NULL) {
		// Process local Actions
		if(node->TYPE==HIERARCHY_STATE) {
			hier_state_t* trans_data = &node->NODE.STATE;
			if(trans_data->actions!=NULL) {
				hwres_heap_t* heapTrans = hwres_heap_getneed_hierstate(node);
				hwres_heap_getmax_need(heap, heapTrans);
				hwres_heap_free(heapTrans);
			}
		}
		// Process children
		foreach(node->CHILDREN, scan) hwres_heap_getneed_level_internal(scan->DATA, heap, true);
		// Next node in the level
		if(do_level==false) return;
		node = node->NEXT;
	}
}
hwres_heap_t* hwres_heap_getneed_node(hier_node_t* node) {
	hwres_heap_t* heap = hwres_heap_new();
	hwres_heap_getneed_level_internal(node, heap, false);
	return heap;
}
hwres_heap_t* hwres_heap_getneed_level(hier_node_t* node) {
	hwres_heap_t* heap = hwres_heap_new();
	hwres_heap_getneed_level_internal(node, heap, true);
	return heap;
}

hwres_heap_t* hwres_heap_getneed(implem_t* Implem) {
	hwres_heap_t* heap_need = hwres_heap_new();

	avl_p_foreach(&Implem->H->NODES[HIERARCHY_STATE], scan) {
		hier_node_t* node = scan->data;
		hier_state_t* state_data = &node->NODE.STATE;
		if(state_data->actions==NULL) continue;
		hwres_heap_t* heapState = hwres_heap_getneed_hierstate(node);
		hwres_heap_getmax_need(heap_need, heapState);
		hwres_heap_free(heapState);
	}

	return heap_need;
}


// Study needed resources, internal processes based on VexAlloc
// With extensive operator sharing

// FIXME Don't do the search recursively, because sub-expressions are shared
// Do the scan non-recursively, consider only the hvex_dico_elt_t nodes
void hwres_scanvexalloc_recurs(hvex_dico_ref_t* ref, hwres_scanvex_t* hwres_data) {
	hvex_dico_elt_t* elt = ref->ref.elt;

	// If that VEXALLOC is already listed, do nothing.
	// This implements elimination of common sub-expressions.
	if(avl_p_isthere(&hwres_data->scanned_vexalloc, elt)==true) return;
	avl_p_add_overwrite(&hwres_data->scanned_vexalloc, elt, elt);

	if(elt->model==HVEX_LITERAL) ;

	// Read from port/signal/register
	else if(elt->model==HVEX_VECTOR) {
		hwres_heap_makeadd_needed_reg(hwres_data->heap_needed, elt->data, 1, 0);
	}

	// Read from array/memory
	else if(elt->model==HVEX_INDEX) {
		hvex_dico_ref_t* ref_dest = elt->args->DATA;
		assert(ref_dest->ref.elt->model==HVEX_VECTOR);
		char* name = ref_dest->ref.elt->data;

		hvex_dico_ref_t* ref_addr = elt->args->NEXT->DATA;
		hvex_t* vex_addr = VexAlloc_GenVex_Ref(ref_addr);

		hwres_heap_makeadd_needed_mem_vex(hwres_data->heap_needed, name, vex_addr, NULL);
		hvex_free(vex_addr);

		hwres_scanvexalloc_recurs(ref_addr, hwres_data);
	}

	// Assignments
	else if(hvex_model_is_asg(elt->model)==true) {
		hvex_dico_ref_t* ref_dest = elt->args->DATA;
		assert(ref_dest->ref.elt->model==HVEX_VECTOR);
		char* name = ref_dest->ref.elt->data;

		foreach(elt->args->NEXT, scan) hwres_scanvexalloc_recurs(scan->DATA, hwres_data);

		if(elt->model==HVEX_ASG || elt->model==HVEX_ASG_EN) {
			hwres_heap_makeadd_needed_reg(hwres_data->heap_needed, name, 0, 1);
		}
		else {
			hvex_dico_ref_t* ref_addr = elt->args->NEXT->DATA;
			hvex_t* vex_addr = VexAlloc_GenVex_Ref(ref_addr);
			hwres_heap_makeadd_needed_mem_vex(hwres_data->heap_needed, name, NULL, vex_addr);
			hvex_free(vex_addr);
		}
	}

	// Logic operantions
	else if(
		elt->model==HVEX_NOT ||
		elt->model==HVEX_OR  || elt->model==HVEX_AND  || elt->model==HVEX_XOR ||
		elt->model==HVEX_NOR || elt->model==HVEX_NAND || elt->model==HVEX_NXOR ||
		elt->model==HVEX_EQ  || elt->model==HVEX_NE
	) {
		// The components are automatically generated when needed.
		// Just scan all the operands
		foreach(elt->args, scan) hwres_scanvexalloc_recurs(scan->DATA, hwres_data);
	}

	// Multiplexers
	else if(elt->model==HVEX_MUXB) {
		// The component is automatically generated when needed.
		// Just scan all the operands
		foreach(elt->args, scan) hwres_scanvexalloc_recurs(scan->DATA, hwres_data);
	}
	else if(elt->model==HVEX_MUXDEC) {
		// The component is automatically generated when needed.
		// Just scan all the operands
		foreach(elt->args, scan) hwres_scanvexalloc_recurs(scan->DATA, hwres_data);
	}
	else if(elt->model==HVEX_MUXDECIN) {
		// This is just an operand of MUXDEC, is does no imply hardware.
		// Just scan all the operands
		foreach(elt->args, scan) hwres_scanvexalloc_recurs(scan->DATA, hwres_data);
	}

	// Arithmetic operations
	else if(elt->model==HVEX_ADD || elt->model==HVEX_ADDCIN) {
		if( (NETLIST_COMP_ADD->flags & NETLIST_COMPMOD_FLAG_CANSHARE)!=0 ) {
			hvex_t* Expr = VexAlloc_GenVex_Ref(ref);
			hwres_heap_makeadd_needed_op_vex(hwres_data->heap_needed, NETLIST_COMP_ADD->name, Expr);
			hvex_free(Expr);
		}
		foreach(elt->args, scan) hwres_scanvexalloc_recurs(scan->DATA, hwres_data);
	}
	else if(
		elt->model==HVEX_SUB || elt->model==HVEX_NEG ||
		elt->model==HVEX_LT  || elt->model==HVEX_LE  || elt->model==HVEX_GT  || elt->model==HVEX_GE ||
		elt->model==HVEX_LTS || elt->model==HVEX_LES || elt->model==HVEX_GTS || elt->model==HVEX_GES
	) {
		if( (NETLIST_COMP_SUB->flags & NETLIST_COMPMOD_FLAG_CANSHARE)!=0 ) {
			hvex_t* Expr = VexAlloc_GenVex_Ref(ref);
			hwres_heap_makeadd_needed_op_vex(hwres_data->heap_needed, NETLIST_COMP_SUB->name, Expr);
			hvex_free(Expr);
		}
		foreach(elt->args, scan) hwres_scanvexalloc_recurs(scan->DATA, hwres_data);
	}
	else if(elt->model==HVEX_MUL) {
		if( (NETLIST_COMP_MUL->flags & NETLIST_COMPMOD_FLAG_CANSHARE)!=0 ) {
			hvex_t* Expr = VexAlloc_GenVex_Ref(ref);
			hwres_heap_makeadd_needed_op_vex(hwres_data->heap_needed, NETLIST_COMP_MUL->name, Expr);
			hvex_free(Expr);
		}
		foreach(elt->args, scan) hwres_scanvexalloc_recurs(scan->DATA, hwres_data);
	}
	else if(elt->model==HVEX_DIVQ || elt->model==HVEX_DIVR) {
		if( (NETLIST_COMP_DIVQR->flags & NETLIST_COMPMOD_FLAG_CANSHARE)!=0 ) {
			hvex_t* Expr = VexAlloc_GenVex_Ref(ref);
			hwres_heap_makeadd_needed_op_vex(hwres_data->heap_needed, NETLIST_COMP_DIVQR->name, Expr);
			hvex_free(Expr);
		}
		foreach(elt->args, scan) hwres_scanvexalloc_recurs(scan->DATA, hwres_data);
	}

	// Operations that use no particular hardware
	else if(elt->model==HVEX_CONCAT || elt->model==HVEX_REPEAT || elt->model==HVEX_RESIZE) {
		// Only scan operands
		foreach(elt->args, scan) hwres_scanvexalloc_recurs(scan->DATA, hwres_data);
	}

	// Shifts and rotations
	else if(elt->model==HVEX_SHL) {
		if( (NETLIST_COMP_SHL->flags & NETLIST_COMPMOD_FLAG_CANSHARE)!=0 ) {
			hvex_t* Expr = VexAlloc_GenVex_Ref(ref);
			hwres_heap_makeadd_needed_op_vex(hwres_data->heap_needed, NETLIST_COMP_SHL->name, Expr);
			hvex_free(Expr);
		}
		foreach(elt->args, scan) hwres_scanvexalloc_recurs(scan->DATA, hwres_data);
	}
	else if(elt->model==HVEX_SHR) {
		if( (NETLIST_COMP_SHR->flags & NETLIST_COMPMOD_FLAG_CANSHARE)!=0 ) {
			hvex_t* Expr = VexAlloc_GenVex_Ref(ref);
			hwres_heap_makeadd_needed_op_vex(hwres_data->heap_needed, NETLIST_COMP_SHR->name, Expr);
			hvex_free(Expr);
		}
		foreach(elt->args, scan) hwres_scanvexalloc_recurs(scan->DATA, hwres_data);
	}
	else if(elt->model==HVEX_ROTL) {
		if( (NETLIST_COMP_ROTL->flags & NETLIST_COMPMOD_FLAG_CANSHARE)!=0 ) {
			hvex_t* Expr = VexAlloc_GenVex_Ref(ref);
			hwres_heap_makeadd_needed_op_vex(hwres_data->heap_needed, NETLIST_COMP_ROTL->name, Expr);
			hvex_free(Expr);
		}
		foreach(elt->args, scan) hwres_scanvexalloc_recurs(scan->DATA, hwres_data);
	}
	else if(elt->model==HVEX_ROTR) {
		if( (NETLIST_COMP_ROTR->flags & NETLIST_COMPMOD_FLAG_CANSHARE)!=0 ) {
			hvex_t* Expr = VexAlloc_GenVex_Ref(ref);
			hwres_heap_makeadd_needed_op_vex(hwres_data->heap_needed, NETLIST_COMP_ROTR->name, Expr);
			hvex_free(Expr);
		}
		foreach(elt->args, scan) hwres_scanvexalloc_recurs(scan->DATA, hwres_data);
	}

	// Operation has a generic callback
	else if(elt->model->cb_needed_res_vexalloc != NULL) {
		elt->model->cb_needed_res_vexalloc(ref, hwres_data);
	}

	// Operation is not handled, or there is nothing to do.
	else {
		printf("ERROR %s:%d : VEX model '%s' not handled.\n", __FILE__, __LINE__, elt->model->name);
		if(hwres_data->source_lines!=NULL) LineList_Print_be(hwres_data->source_lines, "  Source lines: ", "\n");
		abort();
	}
}

hwres_heap_t* hwres_heap_getneed_vexalloc_ref(hvex_dico_ref_t* ref) {
	hwres_scanvex_t hwres_data;
	hwres_scanvex_init(&hwres_data);

	hwres_data.heap_needed = hwres_heap_new();

	// Detect the operations and comparisons that are used
	hwres_scanvexalloc_recurs(ref, &hwres_data);

	hwres_heap_t* heap_needed = hwres_data.heap_needed;
	hwres_scanvex_clear(&hwres_data);

	return heap_needed;
}
hwres_heap_t* hwres_heap_getneed_hierstate_vexalloc(hier_node_t* node) {
	hier_state_t* state_data = &node->NODE.STATE;

	hwres_scanvex_t hwres_data;
	hwres_scanvex_init(&hwres_data);

	hwres_data.heap_needed = hwres_heap_new();

	// The dictionary of expressions
	hvex_dico_t vexalloc;
	VexAlloc_Init(&vexalloc);
	// Link between HierAction and VexAlloc_Ref
	// This way, operator sharing done in VexAlloc is kept
	avl_pp_tree_t tree_act2ref;
	avl_pp_init(&tree_act2ref);

	// Insert all expressions in the dictionary
	foreach(state_data->actions, scan) {
		hier_action_t* action = scan->DATA;
		hvex_dico_ref_t* ref = VexAlloc_Add(&vexalloc, action->expr);
		avl_pp_add_overwrite(&tree_act2ref, action, ref);
	}

	// Apply more extensive operator sharing
	VexAlloc_Share_Expensive(&vexalloc);

	// Build the needed resources from the VexAlloc dictionary
	foreach(state_data->actions, scan) {
		hier_action_t* action = scan->DATA;
		hwres_data.source_lines = action->source_lines;
		// Get the corresponding expression in the dictionary
		hvex_dico_ref_t* ExprAlloc = NULL;
		avl_pp_find_data(&tree_act2ref, action, (void**)&ExprAlloc);
		assert(ExprAlloc!=NULL);
		// Process this expression
		hwres_scanvexalloc_recurs(ExprAlloc, &hwres_data);
	}

	// Clean
	VexAlloc_Clear(&vexalloc);
	avl_pp_reset(&tree_act2ref);

	hwres_heap_t* heap_needed = hwres_data.heap_needed;
	hwres_scanvex_clear(&hwres_data);

	return heap_needed;
}
hwres_heap_t* hwres_heap_getneed_actwithinline_vexalloc(hier_action_t* action, avl_pp_tree_t* tree_act2ref) {
	hwres_scanvex_t hwres_data;
	hwres_scanvex_init(&hwres_data);

	hwres_data.heap_needed = hwres_heap_new();

	chain_list* list_inl_acts = addchain(NULL, action);
	list_inl_acts = Hier_ListActions_AddInline(list_inl_acts);

	// Detect the operations and comparisons that are used
	foreach(list_inl_acts, scan) {
		hier_action_t* this_action = scan->DATA;
		// Get the corresponding expression in the dictionary
		hvex_dico_ref_t* ExprAlloc = NULL;
		avl_pp_find_data(tree_act2ref, this_action, (void**)&ExprAlloc);
		assert(ExprAlloc!=NULL);
		// Process this expression
		hwres_data.source_lines = this_action->source_lines;
		hwres_scanvexalloc_recurs(ExprAlloc, &hwres_data);
	}

	hwres_heap_t* heap_needed = hwres_data.heap_needed;
	hwres_scanvex_clear(&hwres_data);

	freechain(list_inl_acts);

	return heap_needed;
}

static void hwres_heap_getneed_level_vexalloc_internal(hier_node_t* node, hwres_heap_t* heap, bool do_level) {
	while(node!=NULL) {
		// Process local Actions
		if(node->TYPE==HIERARCHY_STATE) {
			hier_state_t* trans_data = &node->NODE.STATE;
			if(trans_data->actions!=NULL) {
				hwres_heap_t* heapTrans = hwres_heap_getneed_hierstate_vexalloc(node);
				hwres_heap_getmax_need(heap, heapTrans);
				hwres_heap_free(heapTrans);
			}
		}
		// Process children
		foreach(node->CHILDREN, scan) hwres_heap_getneed_level_vexalloc_internal(scan->DATA, heap, true);
		// Next node in the level
		if(do_level==false) return;
		node = node->NEXT;
	}
}
hwres_heap_t* hwres_heap_getneed_node_vexalloc(hier_node_t* node) {
	hwres_heap_t* heap = hwres_heap_new();
	hwres_heap_getneed_level_vexalloc_internal(node, heap, false);
	return heap;
}
hwres_heap_t* hwres_heap_getneed_level_vexalloc(hier_node_t* node) {
	hwres_heap_t* heap = hwres_heap_new();
	hwres_heap_getneed_level_vexalloc_internal(node, heap, true);
	return heap;
}

hwres_heap_t* hwres_heap_getneed_vexalloc(implem_t* Implem) {
	hwres_heap_t* heap_need = hwres_heap_new();

	avl_p_foreach(&Implem->H->NODES[HIERARCHY_STATE], scan) {
		hier_node_t* node = scan->data;
		hier_state_t* state_data = &node->NODE.STATE;
		if(state_data->actions==NULL) continue;
		hwres_heap_t* heapState = hwres_heap_getneed_hierstate_vexalloc(node);
		hwres_heap_getmax_need(heap_need, heapState);
		hwres_heap_free(heapState);
	}

	return heap_need;
}



//=========================================================
// Detection of available resources
//=========================================================

void hwres_heap_add_avail_reg(hwres_heap_t* heap, char* name, int num_r, int num_w) {
	hw_res_t* elt = hwres_new();
	elt->type = HWRES_TYPE_REG;
	elt->name = name;
	elt->ports_r = num_r;
	elt->ports_w = num_w;
	// Add in the heap
	avl_pp_node_t* avl_node = NULL;
	avl_pp_add(&heap->type_reg, elt->name, &avl_node);
	avl_node->data = elt;
}
void hwres_heap_add_avail_mem(hwres_heap_t* heap, char* name, int num_r, int num_w, bool direct) {
	hw_res_t* elt = hwres_new();
	elt->type = HWRES_TYPE_MEM;
	elt->name = name;
	elt->ports_r = num_r;
	elt->ports_w = num_w;
	elt->direct_avail = direct;
	// Add in the heap
	avl_pp_node_t* avl_node = NULL;
	avl_pp_add(&heap->type_mem, elt->name, &avl_node);
	avl_node->data = elt;
}
void hwres_heap_add_avail_op(hwres_heap_t* heap, char* name, int number) {
	hw_res_t* elt = hwres_heap_op_getadd(heap, name);
	elt->number += number;
}


hwres_heap_t* hwres_heap_getavail(implem_t* Implem) {
	// Get the components from the netlist

	hwres_heap_t* heap = hwres_heap_new();

	avl_pp_foreach(&Implem->netlist.top->children, scanComp) {
		netlist_comp_t* comp = scanComp->data;

		if(comp->model==NETLIST_COMP_SIGNAL) {
			hwres_heap_add_avail_reg(heap, comp->name, -1, 1);
		}
		else if(comp->model==NETLIST_COMP_REGISTER) {
			hwres_heap_add_avail_reg(heap, comp->name, -1, 1);
		}
		else if(comp->model==NETLIST_COMP_MEMORY) {
			netlist_memory_t* comp_mem = comp->data;
			hwres_heap_add_avail_mem(heap, comp->name, comp_mem->ports_ra_nb, comp_mem->ports_wa_nb, comp_mem->direct_en);
		}
		else if(comp->model==NETLIST_COMP_PINGPONG_IN || comp->model==NETLIST_COMP_PINGPONG_OUT) {
			netlist_pingpong_t* comp_pp = comp->data;
			hwres_heap_add_avail_mem(heap, comp->name, comp_pp->ports_ra_nb, comp_pp->ports_wa_nb, comp_pp->is_direct);
		}
		else if(comp->model==NETLIST_COMP_ADD) {
			hwres_heap_add_avail_op(heap, comp->model->name, 1);
		}
		else if(comp->model==NETLIST_COMP_SUB) {
			hwres_heap_add_avail_op(heap, comp->model->name, 1);
		}
		else if(comp->model==NETLIST_COMP_MUL) {
			hwres_heap_add_avail_op(heap, comp->model->name, 1);
		}
		else if(comp->model==NETLIST_COMP_DIVQR) {
			hwres_heap_add_avail_op(heap, comp->model->name, 1);
		}
		else if(comp->model==NETLIST_COMP_SHL) {
			hwres_heap_add_avail_op(heap, comp->model->name, 1);
		}
		else if(comp->model==NETLIST_COMP_SHR) {
			hwres_heap_add_avail_op(heap, comp->model->name, 1);
		}
		else if(comp->model==NETLIST_COMP_ROTL) {
			hwres_heap_add_avail_op(heap, comp->model->name, 1);
		}
		else if(comp->model==NETLIST_COMP_ROTR) {
			hwres_heap_add_avail_op(heap, comp->model->name, 1);
		}

		// Handle components with generic callback or declared by plugins
		else if(comp->model->func_comp_hwres_avail != NULL) {
			comp->model->func_comp_hwres_avail(comp, heap);
		}

	}  // Scan the components

	avl_pp_foreach(&Implem->netlist.top->accesses, scanAccess) {
		netlist_access_t* access = scanAccess->data;

		if(access->model==NETLIST_ACCESS_FIFO_IN) {
			netlist_access_fifo_t* fifo_data = access->data;
			hwres_heap_add_avail_reg(heap, fifo_data->port_data->name, -1, 0);
			hwres_heap_add_avail_reg(heap, fifo_data->port_rdy->name, 0, 1);
			hwres_heap_add_avail_reg(heap, fifo_data->port_ack->name, -1, 0);
		}
		else if(access->model==NETLIST_ACCESS_FIFO_OUT) {
			netlist_access_fifo_t* fifo_data = access->data;
			hwres_heap_add_avail_reg(heap, fifo_data->port_data->name, 0, 1);
			hwres_heap_add_avail_reg(heap, fifo_data->port_rdy->name, 0, 1);
			hwres_heap_add_avail_reg(heap, fifo_data->port_ack->name, -1, 0);
		}

		else if(access->model==NETLIST_ACCESS_WIRE_IN) {
			netlist_access_single_port_t* sp_data = access->data;
			hwres_heap_add_avail_reg(heap, sp_data->port->name, -1, 0);
		}
		else if(access->model==NETLIST_ACCESS_WIRE_OUT) {
			netlist_access_single_port_t* sp_data = access->data;
			hwres_heap_add_avail_reg(heap, sp_data->port->name, -1, 1);
		}

		else if(access->model==NETLIST_ACCESS_MEM_RA_EXT) {
			hwres_heap_add_avail_mem(heap, access->name, 1, 0, false);
		}
		else if(access->model==NETLIST_ACCESS_MEM_WA_EXT) {
			hwres_heap_add_avail_mem(heap, access->name, 0, 1, false);
		}

		// FIXME Here, handle accesses declared by plugins

	}  // Scan the Access structures

	// Add the ports
	if(Implem->netlist.port_start!=NULL) {
		hwres_heap_add_avail_reg(heap, Implem->netlist.port_start->name, -1, 0);
	}

	// FIXME there might be ports that are meant to be used, but that are in no Access structure

	return heap;
}

int Resources_Available_Detect(implem_t* Implem) {
	// Get the global hardware resources
	hwres_heap_t* archi_resources = hwres_heap_getavail(Implem);
	if(archi_resources==NULL) return -1;

	if(Implem->SCHED.hwres_available!=NULL) hwres_heap_free(Implem->SCHED.hwres_available);
	Implem->SCHED.hwres_available = archi_resources;

	return 0;
}



//=========================================================
// View available resources
//=========================================================

void Resources_Available_View(implem_t* Implem, indent_t* indent) {
	if(Implem->SCHED.hwres_available==NULL) return;
	hwres_heap_print(Implem->SCHED.hwres_available, indent);
}

