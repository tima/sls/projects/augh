
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <stdbool.h>

#include "auto.h"
#include "linelist.h"



// List of file lines : it is a bitype_list
//   field DATA_FROM is the file NAME - WARNING the name has "" quotes around
//   field DATA_TO is a chain_num of line values (sorted)

void LineList_Free(bitype_list* list) {
	foreach(list, scan) freenum(scan->DATA_TO);
	freebitype(list);
}

bitype_list* LineList_Dup(bitype_list* list) {
	bitype_list* newlist = dupbitype(list);
	foreach(newlist, scan) {
		scan->DATA_TO = ChainNum_Dup(scan->DATA_TO);
	}
	return newlist;
}

bitype_list* LineList_Add(bitype_list* list, char* name, int line) {
	bitype_list* eltname = ChainBiType_SearchFrom(list, name);
	if(eltname==NULL) {
		list = addbitype(list, 0, name, NULL);
		eltname = list;
	}
	num_list* eltline = ChainNum_Search(eltname->DATA_TO, line);
	if(eltline==NULL) eltname->DATA_TO = addnum(eltname->DATA_TO, line);
	return list;
}

void LineList_Print_fbse(FILE* F, bitype_list* list, char* beg, char* sep_nameline, char* sep_lines, char* sep_names, char* end) {
	if(beg!=NULL) fprintf(F, "%s", beg);
	if(list==NULL) fprintf(F, "(none)");
	else {
		foreach(list, scanName) {
			if(scanName!=list) {
				if(sep_names!=NULL) fprintf(F, "%s", sep_names);
				else fprintf(F, ", ");
			}
			fprintf(F, "%s", (char*)scanName->DATA_FROM);
			if(sep_nameline!=NULL) fprintf(F, "%s", sep_nameline);
			else fprintf(F, ":");
			foreach((num_list*)scanName->DATA_TO, scanLine) {
				if(scanLine!=scanName->DATA_TO) {
					if(sep_lines!=NULL) fprintf(F, "%s", sep_lines);
					else fprintf(F, ",");
				}
				fprintf(F, "%ld", scanLine->DATA);
			}
		}
	}
	if(end!=NULL) fprintf(F, "%s", end);
}
void LineList_Print_fbe(FILE* F, bitype_list* list, char* beg, char* end) {
	LineList_Print_fbse(F, list, beg, NULL, NULL, NULL, end);
}
void LineList_Print_f(FILE* F, bitype_list* list) {
	LineList_Print_fbse(F, list, NULL, NULL, NULL, NULL, NULL);
}
void LineList_Print_be(bitype_list* list, char* beg, char* end) {
	LineList_Print_fbse(stdout, list, beg, NULL, NULL, NULL, end);
}
void LineList_Print(bitype_list* list) {
	LineList_Print_fbse(stdout, list, NULL, NULL, NULL, NULL, NULL);
}

void LineList_FPrint_machine(FILE* F, bitype_list* lines) {
	fprintf(F, "%u", ChainBiType_Count(lines));
	foreach(lines, scanName) {
		fprintf(F, " %s %u", (char*)scanName->DATA_FROM, ChainNum_Count(scanName->DATA_TO));
		foreach((num_list*)scanName->DATA_TO, scanLine) fprintf(F, " %lu", scanLine->DATA);
	}
}
bitype_list* LineList_FScan_machine(FILE* F) {
	bitype_list* lines = NULL;
	char buffer[1000];
	unsigned name_nb, line_nb;
	int z;

	//Get the number of names
	z = fscanf(F, "%s", buffer);
	if(z!=1) return NULL;
	name_nb = atoi(buffer);

	for(unsigned idxname=0; idxname<name_nb; idxname++) {
		// Get the name
		z = fscanf(F, "%s", buffer);
		if(z!=1) { LineList_Free(lines); return NULL; }
		char* name = namealloc(buffer);
		// Get the number of lines
		z = fscanf(F, "%s", buffer);
		if(z!=1) { LineList_Free(lines); return NULL; }
		line_nb = atoi(buffer);
		// Get the lines
		for(unsigned idxline=0; idxline<line_nb; idxline++) {
			// Get the line value
			z = fscanf(F, "%s", buffer);
			if(z!=1) { LineList_Free(lines); return NULL; }
			// Add to the list
			lines = LineList_Add(lines, name, atoi(buffer));
		}
	}
	return lines;
}

bool LineList_IsThereNameLine(bitype_list* lines, char* name, long line) {
	if(name!=NULL) {
		bitype_list* elt_name = ChainBiType_SearchFrom(lines, name);
		if(elt_name==NULL) return false;
		return ChainNum_IsDataHere(elt_name->DATA_TO, line);
	}
	else {
		foreach(lines, elt_name) {
			bool b = ChainNum_IsDataHere(elt_name->DATA_TO, line);
			if(b==true) return true;
		}
	}
	return false;
}
bool LineList_IsIncluded(bitype_list* ref, bitype_list* lines) {
	foreach(lines, elt_name) {
		bitype_list* ref_elt_name = ChainBiType_SearchFrom(ref, elt_name->DATA_FROM);
		if(ref_elt_name==NULL) return false;
		foreach((num_list*)elt_name->DATA_TO, elt_line) {
			if(ChainNum_IsDataHere(ref_elt_name->DATA_TO, elt_line->DATA)==false) return false;
		}
	}
	return true;
}


