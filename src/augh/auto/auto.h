
#ifndef _AUTO_H_
#define _AUTO_H_

#include <stdbool.h>

// Take care about the order of the includes here.
#include <mut.h>
#include <aut.h>
#include "../hvex/hvex.h"

typedef struct implem_t  implem_t;

#include "../chain.h"
#include "../minmax.h"
#include "../indent.h"
#include "../chain.h"
#include "../printfm.h"

#include "core.h"
#include "hierarchy.h"
#include "hwres.h"
#include "map.h"
#include "../netlist/netlist.h"
#include "techno.h"



//================================================
// Parameters
//================================================

// Verbose levels
#define VERBOSE_SILENT  0
#define VERBOSE_LEVEL1  1
#define VERBOSE_LEVEL2  2

// Default name of the hardware entity
#define DEFAULT_ENTITY  "top"
#define DEFAULT_VHDL_DIR  "vhdl"


// Some parameters shared between Implem structures
typedef struct augh_globals_t  augh_globals_t;
struct augh_globals_t {
	// Some plugins may need this to initialize: argv[0]
	char const * argv0;

	// To compute the execution time of the program
	struct timespec time_cpu;   // Important: this is relative to the process start time
	struct timespec time_real;  // Important: this is real time

	// Verbosity levels
	struct {
		int   level;
		bool  silent;  // Level = 0
		bool  lvl1;    // Level = 1
		bool  lvl2;    // Level = 2
	} verbose;

	// 0: simple design transcription, no scheduling, nothing
	// 1: simplification + scheduling
	// 2: simplification + scheduling + fast elaboration
	// 3: simplification + scheduling + precise elaboration
	// Default level: 0
	unsigned optim_level;

	// To skip generation of VHDL or synth project files
	bool gen_vhdl;
	bool gen_synth_proj;
	// To stop after generation of VHDL and synth project files
	bool stop_vhdl;

	// A tree that contains all the component models that are base on an Implem
	// Key = component name, data = implem_t*
	avl_pp_tree_t implem_models;
	// The top-level component/implem
	implem_t* top_implem;

	// When this structure is a duplicated one
	// Reference to the original structure
	struct globals_dup {
		// These fields are set/filled only in a duplicated globals structure
		augh_globals_t* father;
		avl_pp_tree_t   pointers_old2new;  // Only Implems
		avl_pp_tree_t   pointers_new2old;  // Only Implems
		// These are the globals created from the current ones
		// This tree contains pointers to the duplicated structures
		avl_p_tree_t    children;
	} dup;
};



//================================================
// The structure containing ALL the data
//================================================

// Note about coherency of information in this structure:
// The name of this Implem is copied in, and must be the same in:
// - the name of the top-level netlist component
// - the field env.entity
// - the field compmod.model_name
// This name is also used as key to index the Implem in env.globals->implem_models
// This name is also used as key in all compmod.tree_comp2implem where this Implem is used as component model
struct implem_t {

	// Parameters from command-line or elsewhere
	struct env {
		// Global parameters (pointer to a structure allocated by the user)
		augh_globals_t* globals;
		// To parse the C
		chain_list*   cflags;  // Data is strings (not malloc'd)
		char*         input_file;
		// To generate things
		char*         entity;
		char*         vhdl_dir;
		char*         vhdl_prefix;  // A prefix for ALL component names, including top
		// What interface to the circuit
		bool          wait_on_start;
		bool          inline_all;
		bool          auto_mem_readbuf;
	} env;

	// The "Hierarchy"
	// A graph that contains all instructions to execute
	hier_t* H;

	// Symbols whose usage extends outside instructions of the states
	//   This is to prevent the simplification functions to delete signals
	//   that are hard-connected to other components, for example
	// Key = symbol name, data = flags
	avl_pi_tree_t symbols_flags;

	// Scheduling data
	struct sched {
		hwres_heap_t*   hwres_available;
	} SCHED;

	// Implementation core
	struct core {
		// To compute the execution time
		struct timespec begtime_cpu;
		struct timespec begtime_real;
		// Current iteration number
		unsigned        iteration;
		// Resources used. Units are LUT6, FFs, etc
		bool            eval_done;  // meaningful for resources and exec time
		bitype_list*    resources_opers;
		ptype_list*     resources_total;
		// Execution time
		minmax_t        execution_time;
		// Freedom degrees
		num_list*       freedom_degrees_skipped_ops;
		chain_list*     freedom_degrees;
		// Reference costs of adding operators or mem ports
		avl_pp_tree_t   refcost_oneop;          // Indexed by component model name
		avl_pp_tree_t   refcost_memport_r_mux;  // Indexed by memory name
		// Impossible freedom degrees
		chain_list*     skipfd_addops;  // Content : a hwres_heap_t
		chain_list*     skipfd_scalar;  // Content : names of the arrays
		// The duplicated Implem, sheduled and mapped, used as reference for weighted FD
		implem_t*       dup_implem;
		// Number of operators currently in excess. It is: curImplem - dupImplem
		// Key = comp model name, data = number
		avl_pi_tree_t   dupimplem_inxs;
	} CORE;

	// When this structure is a duplicated one
	// Reference to the original Implem structure
	struct dup {
		// The current Implem is duplicated
		implem_t*       father;
		avl_pp_tree_t   pointers_old2new;
		avl_pp_tree_t   pointers_new2old;
		// These are the Implem created from the current ones
		// This tree contains pointers to the child Implem structures.
		avl_p_tree_t    children;
	} DUP;

	// The state registers. Used to automatically finish the synthesis.
	struct synth_state {
		// Indicates that the operators are instanciated.
		int alloc;
		// Indicates the the pre-processing has been done :
		int preproc;
		// Indicates that the scheduling of the entire design is done
		// After that step, the timings are accurate.
		int schedule;
		// Indicates that the post-processing has been done :
		// MUX and logic insertion, mapping, simplifications
		// After that step, the resources are accurate.
		int postproc;
	} synth_state;

	// Execution time information.
	struct time {
		double max_state_delay;
		double max_cycles_per_state;
		double map_max_action_delay;
		unsigned map_max_cycles_per_state;
		minmax_t execution_cycles;
		minmax_t map_execution_cycles;
	} time;

	// The netlist representation
	struct netlist {
		// Top-level entity
		netlist_comp_t* top;

		// Direct link to relevant special fields of the circuit.
		netlist_comp_t* fsm;

		// Main control signals
		netlist_comp_t* sig_clock;
		netlist_comp_t* sig_reset;
		netlist_comp_t* sig_start;

		// Top top-level control ports
		// They can be connected to the control signals, or generated by an interface
		//   (or both, for reset)
		netlist_port_t* port_clock;
		netlist_port_t* port_reset;
		netlist_port_t* port_start;

		// Value concatenated to some operator names to avoid collisions of instance names.
		// Incremented at each use.
		unsigned comp_name_index;

		// All mapping data
		map_netlist_data_t* mapping;

	} netlist;

	// Describe the components that are an instance of another Implem
	struct compmod {
		// The name of the component model this Implem corresponds to
		char* model_name;
		// The number of times this component model is used/instantiated in the current design
		// This is used to weight the transformations
		unsigned instance_nb;
		// An additional time weight to apply to transformations on this component model (default = 1)
		double time_weight;
	} compmod;

	// All data related to the targeted hardware
	synth_target_t* synth_target;

};



//================================================
// Misc definitions
//================================================

// Protected symbols

#define SYM_PROTECT_NODEL    0x00001
#define SYM_PROTECT_NOPROPAG 0x00002
#define SYM_PROTECT_NOCIRC   0x00004

#define SYM_FLAG_CANDUP      0x00010
#define SYM_FLAG_NOEXTERN    0x00020  // To prevent creating top-level interfaces

void Implem_SymFlag_Add(implem_t* Implem, const char* name, int flag);
void Implem_SymFlag_Rem(implem_t* Implem, const char* name, int flag);
bool Implem_SymFlag_Chk(implem_t* Implem, const char* name, int flag);
void Implem_SymFlag_FPrint(FILE* F, implem_t* Implem, const char* name);

static inline void Implem_SymFlag_Print(implem_t* Implem, const char* name) {
	Implem_SymFlag_FPrint(stdout, Implem, name);
}

static inline bool Implem_SymFlag_ChkAnyProtect(implem_t* Implem, const char* name) {
	return Implem_SymFlag_Chk(Implem, name, SYM_PROTECT_NODEL | SYM_PROTECT_NOPROPAG | SYM_PROTECT_NOCIRC);
}
static inline bool Implem_SymFlag_ChkAny(implem_t* Implem, const char* name) {
	return Implem_SymFlag_Chk(Implem, name, 0xFFFFFFFF);
}



//================================================
// SplitString functions
//================================================

// The sub- command interpreters can use these functions
chain_list* SplitString_allwords(char* str);
char* SplitString_firstword(char* str, char** after);
char* SplitString_firstword_quoted(char* str, char** after);

char* SplitString_trim_beg(char* str);
char* SplitString_trim_end(char* str);
char* SplitString_trim(char* str);

int   SplitString_istherestring(char* str, const char* token, char** after);
char* SplitString_findword(char* str, const char* token, char** after);
chain_list* SplitString_tokensplit(char* str, const char* token);



//================================================
// Misc functions
//================================================

double ParseWithUnit(char *arg);
double atof_multiplier(const char *arg);

ptype_list* ReadString_NameNum(const char* str);

ptype_list* augh_read_hwlimits(techno_t* techno, const char* string);


#include <time.h>

double TimeSpec_ToDouble(struct timespec *ts);
double TimeSpec_GetDiff_Double(struct timespec *oldtime, struct timespec *newtime);

double TimeSpec_DiffCurrReal_Double(struct timespec *oldtime);
double TimeSpec_DiffCurrCpu_Double(struct timespec *oldtime);

// Important: This returns the system absolute time
static inline void TimeSpec_GetReal(struct timespec *ts) {
	clock_gettime(CLOCK_REALTIME, ts);
}
// Important: This returns the time relative to the process start time
static inline void TimeSpec_GetCpu(struct timespec *ts) {
	clock_gettime(CLOCK_PROCESS_CPUTIME_ID, ts);
}

static inline double TimeGlobals_GetCpu(augh_globals_t* globals) {
	return TimeSpec_DiffCurrCpu_Double(&globals->time_cpu);
}
static inline double TimeGlobals_GetReal(augh_globals_t* globals) {
	return TimeSpec_DiffCurrReal_Double(&globals->time_real);
}

char* GetExePath();
char* GetExePath_CutBin();

bool CheckDirName(char* name);
bool CheckDirName_Verbose(char* name);



//================================================
// Functions
//================================================

augh_globals_t* augh_globals_new();
void augh_globals_free(augh_globals_t* globals);
void augh_globals_freechildren(augh_globals_t* globals);

implem_t* augh_implem_new(augh_globals_t* globals);
void augh_implem_free(implem_t* Implem);
void augh_implem_freechildren(implem_t* Implem);

void OptimLevel_Set(augh_globals_t* globals, unsigned l);

int augh_automatic(implem_t* Implem);

void  Implem_Dup_LinkPointers(implem_t* newImplem, void* oldptr, void* newptr);
void* Implem_Dup_New2Old(implem_t* newImplem, void* newptr);
void* Implem_Dup_Old2New(implem_t* newImplem, void* oldptr);

void augh_postprocess(implem_t* Implem);

implem_t* Implem_Dup_internal(implem_t* Implem, augh_globals_t* new_globals);
static inline implem_t* Implem_Dup(implem_t* Implem) {
	return Implem_Dup_internal(Implem, Implem->env.globals);
}

void Globals_Dup_LinkPointers(augh_globals_t* newGlobals, void* oldptr, void* newptr);
void* Globals_Dup_Old2New(augh_globals_t* newGlobals, void* oldptr);
void* Globals_Dup_Old2New(augh_globals_t* newGlobals, void* oldptr);

augh_globals_t* Globals_Dup(augh_globals_t* globals);

static inline implem_t* augh_modimplem_get(augh_globals_t* globals, const char* name) {
	implem_t* modImplem = NULL;
	avl_pp_find_data(&globals->implem_models, name, (void**)&modImplem);
	return modImplem;
}

// Initialization of all modules of the AUGH lib
void aughlib_init();
void aughlib_clear();



//================================================
// Other inline functions
//================================================

#define lineprintf(msg, fmt, ...) printf(msg " %s:%u - " fmt, __FILE__, __LINE__, ##__VA_ARGS__)

#define DEBUGLINE() printf("DEBUGLINE %s:%u\n", __FILE__, __LINE__)

#ifndef NDEBUG
	// A note about variadic macros: with ## before __VA_ARGS__,
	// the comma just before is deleted when there are no arguments.
	#define dbgprintf(fmt, ...) printf("DEBUG %s:%u - " fmt, __FILE__, __LINE__, ##__VA_ARGS__)
	#define dbgprintfm(fmt, ...) printfm("DEBUG %s:%u - ", __FILE__, __LINE__, fmt, ##__VA_ARGS__)
#else
	#define dbgprintf(fmt, ...) do {} while(0)
	#define dbgprintfm(fmt, ...) do {} while(0)
#endif

#define errprintf(fmt, ...)       printf("ERROR %s:%u - " fmt, __FILE__, __LINE__, ##__VA_ARGS__)
#define errprintfa(fmt, ...) do { printf("ERROR %s:%u - " fmt, __FILE__, __LINE__, ##__VA_ARGS__); abort(); } while(0)

#define Swap(v0, v1) do { typeof(v0) zzzswap = v0; v0 = v1; v1 = zzzswap; } while(0)


unsigned uint32_dec_length(uint32_t val);

// Check if a number is a power of 2
// https://graphics.stanford.edu/~seander/bithacks.html
static inline bool uint32_ispower2(uint32_t v) {
	return (v!=0) && (v & (v - 1))==0;
}

// Count the number of bits needed to store the value
// This also returns 1 for the value 0
unsigned uint_bitsnb(unsigned v);

// Return log in any base
unsigned uint_log_floor(unsigned base, unsigned n);
unsigned uint_log_ceil(unsigned base, unsigned n);

// Return a power of an integer
unsigned uint_pow(unsigned v, unsigned p);

// Round to a power of 2
unsigned uint_rndpow2_ceil(unsigned v);

// Round to a power in any base
unsigned uint_rndpow_floor(unsigned base, unsigned n);
unsigned uint_rndpow_ceil(unsigned base, unsigned n);

// Round to a power in any base, with factor: round to f*base^e
unsigned uint_rndpow_fac_floor(unsigned f, unsigned base, unsigned n);
unsigned uint_rndpow_fac_ceil(unsigned f, unsigned base, unsigned n);

char* str_replace_one_buf(char* str, char c, char c2);



//================================================
// Useful macros
//================================================

#define TestFree(p)     do { if(p!=NULL) free(p); } while(0)
#define TestFreeNull(p) do { if(p!=NULL) { free(p); p = NULL; } } while(0)
#define FreeNull(p)     do { free(p); p = NULL; } while(0)

#define TestDo(p,f)     do { if(p!=NULL) f(p); } while(0)
#define TestDoNull(p,f) do { if(p!=NULL) { f(p); p = NULL; } } while(0)
#define DoNull(p,f)     do { f(p); p = NULL; } while(0)



#endif  // _AUTO_H_

