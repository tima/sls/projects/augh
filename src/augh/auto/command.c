
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <time.h>
#include <errno.h>

#include <sys/resource.h>

#include "../../../config.h"

#include "auto.h"
#include "command_rl.h"
#include "command.h"
#include "hierarchy.h"
#include "techno.h"
#include "core.h"
#include "schedule.h"
#include "forksynth.h"
#include "addops.h"
#include "map.h"
#include "implem_models.h"
#include "../netlist/netlist_cmd.h"
#include "../plugins/plugins.h"



//=================================================
// Local and shared variables
//=================================================

static bool commands_disp = false;
static bool commands_noquote = false;

static avl_pp_tree_t augh_ext_interpreters;



//=================================================
// Library functions
//=================================================

int Command_CmpStr_YesNo(const char* str) {
	if(strcasecmp(str, "yes")==0 ||
	   strcasecmp(str, "1")==0 ||
	   strcasecmp(str, "en")==0 ||
	   strcasecmp(str, "enable")==0 ||
	   strcasecmp(str, "true")==0 ||
	   strcasecmp(str, "set")==0
	) return true;
	if(strcasecmp(str, "no")==0 ||
	   strcasecmp(str, "0")==0 ||
	   strcasecmp(str, "dis")==0 ||
	   strcasecmp(str, "disable")==0 ||
	   strcasecmp(str, "false")==0 ||
	   strcasecmp(str, "unset")==0
	) return false;
	return -1;
}

int Command_PopParam_YesNoDef(command_t* cmd_data, int def) {
	const char* param = Command_PopParam(cmd_data);
	if(param==NULL) return def;
	return Command_CmpStr_YesNo(param);
}
int Command_PopParam_YesNoDef_Verbose(command_t* cmd_data, int def, const char* from_cmd) {
	const char* param = Command_PopParam(cmd_data);
	if(param==NULL) {
		if(def>=0) return def;
		printf("Error: Missing parameter from command '%s'\n", from_cmd);
		return -1;
	}
	int z = Command_CmpStr_YesNo(param);
	if(z<0) {
		printf("Error: Unrecognized parameter '%s' from command '%s'\n", param, from_cmd);
		return -1;
	}
	return z;
}

int Command_PopParam_WholeLine_stralloc_verbose(command_t* cmd_data, const char* cmd, char** pline) {
	char* line = NULL;
	if(cmd_data->callback_editbuf!=NULL) {
		line = cmd_data->callback_editbuf(cmd_data);
		line = SplitString_trim(line);
		line = stralloc(line);
	}
	else {
		line = Command_PopParam_stralloc(cmd_data);
		const char* therest = Command_PopParam(cmd_data);
		if(therest!=NULL) {
			printf("ERROR: Command '%s': Syntax error with current interpreter.\n", cmd);
			*pline = NULL;
			return __LINE__;
		}
	}
	*pline = line;
	return 0;
}



//=================================================
// Built-in interpreter
//=================================================

// Callbacks for built-in interpreter

// The callback to get one word of the command
const char* AughCmd_callback_nextword(command_t* cmd_data) {
	if(commands_noquote==true) SplitString_firstword(cmd_data->data_command, (char**)&cmd_data->data_command);
	return SplitString_firstword_quoted(cmd_data->data_command, (char**)&cmd_data->data_command);
}

// The callback to get the full editable command buffer
char* AughCmd_callback_editbuf(command_t* cmd_data) {
	return cmd_data->data_command;
}

// The callback to display result after command execution
static void Command_callback_result_finish(command_t* cmd_data) {
	int count = 0;
	cmd_data->list_res = reverse(cmd_data->list_res);
	foreach(cmd_data->list_res, scanRes) {
		if(count > 0) printf(" ");
		printf("%s", (char*)scanRes->DATA);
		count++;
	}
	if(count > 0) printf("\n");
}

// Initialization of data structure for built-in interpreter

static void Command_data_init(command_t* cmd_data) {
	memset(cmd_data, 0, sizeof(*cmd_data));
	// Callbacks to process commands
	cmd_data->callback_nextword = AughCmd_callback_nextword;
	cmd_data->callback_editbuf  = AughCmd_callback_editbuf;
	// Callbacks to process results
	cmd_data->callback_result_finish = Command_callback_result_finish;
}

// The AUGH built-in command interpreter
// If the input file F is NULL, stdin is used, interactive mode, with the readline library
static int augh_Command_internal(implem_t *Implem, FILE* F) {
	char* oneline = NULL;
	size_t oneline_size = 0;

	int ret = 0;

	// Data structure to launch execution of commands
	command_t cmd_data_i;
	command_t* cmd_data = &cmd_data_i;

	// Init fields
	Command_data_init(cmd_data);

	do {

		// Get one full line
		if(F!=NULL) {
			int z = getline(&oneline, &oneline_size, F);
			if(z<0) break;
			if(oneline==NULL) break;
			if(oneline[0]==0) break;
		}
		else {
			oneline = command_rl("AUGH > ");
			if(oneline==NULL) break;
		}

		// Remove the comments
		for(char* ptr=oneline; (*ptr)!=0; ptr++) {
			if(*ptr=='#') { *ptr = 0; break; }
		}

		// Remove end spaces
		SplitString_trim_end(oneline);
		if(oneline[0]==0) continue;

		// add this command to the history
		if(F==NULL) command_rl_hist(oneline);
		if(F!=NULL && commands_disp==true) {
			printf("AUGH > %s\n", oneline);
		}

		// Process the commands
		cmd_data->data_command = oneline;
		int z = AughCmd_Exec(Implem, cmd_data);

		// Conditions to stop the interpreter
		if     (cmd_data->control==COMMAND_CTL_EXIT) break;
		else if(cmd_data->control==COMMAND_CTL_MANUAL) {
			if(F!=NULL) {
				fclose(F);
				F = NULL;
				printf("The command interpreter is now in manual mode.\n");
			}
			else {
				printf("You are already in manual mode.\n");
			}
		}
		else if(z!=0 && F!=NULL) { ret = -1; break; }

	} while(1);

	// Clean
	if(oneline!=NULL) free(oneline);

	return ret;
}



//=================================================
// Execute built-in commands
//=================================================

// Just execute the command, don't process command control nor results
// If non-zero is returned, it means error.
static int augh_Command_Exec_noresult(implem_t* Implem, command_t* cmd_data) {

	// DEBUG print commands
	//if(commands!=NULL) printf("DEBUG Command : %s\n", commands);

	const char* cmd = Command_PopParam(cmd_data);
	if(cmd==NULL) {
		printf("Error: No command received. Try 'help'.\n");
		return -1;
	}

	// System commands

	if(strcmp(cmd, "help")==0) {
		printf(
			"\n"
			"Sub-command interpreters:\n"
			"  build                 Building netlist structure.\n"
			"  hier                  Hierarchy.\n"
			"  op                    Operators.\n"
			"  sched                 Scheduling.\n"
			"  techno                Technology library.\n"
			"  forksynth             Separate synthesis in a forked process.\n"
			"  core                  Implementation core.\n"
			"  map                   Mapping.\n"
			"  netlist               Netlist generation.\n"
			"  plugin                Plugins.\n"
			"  impmod                Implem models.\n"
			"\n"
			"Loading source files:\n"
			"  cflags [<flags>]        Set or clear pre-processing flags.\n"
			"  cflags+,  cflags-add [<flags>]\n"
			"                        Add pre-processing flags.\n"
			"  load [<file>]         Load a C source file.\n"
			"                        Default : load the file specified as command-line parameter.\n"
			"  loadmod <model-name> <file>\n"
			"                        Load a C source file as component model.\n"
			"\n"
			"Setting parameters :\n"
			"  target-freq <value>   Set the target frequency. The unit is the Hertz with exponents.\n"
			"                        Examples of syntax : 250M, 32k, 2G\n"
			"  hwlimit <limits>      Set the resource limits for the elaboration core.\n"
			"  hwmult <factor>       Multiply the amount of available resouces. Examples: 0.8, 4, 80%%, 400%%\n"
			"  vhdl-dir <name>       Set the directory where to save the VHDL files.\n"
			"  vhdl-prefix <name>    Set a prefix for the names of all component names and instances.\n"
			"\n"
			"Processing:\n"
			"  synth-min             Perform a minimal synthesis:\n"
			"                        - Scheduling (schedule)\n"
			"                        - Post-processing (postprocess)\n"
			"  elaborate             Launches the elaboration under the resource constraints.\n"
			"                        It also performs mapping and display details about the result.\n"
			"  postprocess           Generate the netlist.\n"
			"  vhdl                  Generate VHDL and ISE project file, if set.\n"
			"  postprocess-all       Launch on all HLS component models.\n"
			"  vhdl-all              Launch on all HLS component models.\n"
			"\n"
			"Back-end synthesis:\n"
			"  backend-gen-prj       Generate synthesis project files.\n"
			"  backend-launch        Launch back-end synthesis.\n"
			"\n"
			"System commands:\n"
			"  exit, quit, stop, x, q       End the current session (sessions can be recursive).\n"
			"  die <success|good|bad|error|failure|fail|clean|ok|<value>>\n"
			"                               Stop the entire program and return a value.\n"
			"  script <filename>            Fetch the commands from the given file.\n"
			"  cd <path>                    Change the current directory.\n"
			"  sh, shell  <command>         Send the command to the system.\n"
			"  commands-disp [<bool>]       Dipslay commands executed from scripts (init=no).\n"
			"  commands-quotes [<bool>]     Built-in interpreter uses quotes (init=yes).\n"
			"  m, multi <token> <commands>  Specify multiple commands separated by the word <token>.\n"
			"  noerr <command>              Don't consider the command errors (useful for display).\n"
			"  manual                       Stop the current script and give control to the user.\n"
			"  augh-resource                Display AUGH system resource usage.\n"
			"  version                      Display AUGH version.\n"
			"  check-integrity              Check integrity and coherence of all internal data structures. Exit on failure.\n"
			"  help                         Display this help.\n"
			"\n"
		);
		return 0;
	}

	// The user wants to end the script
	if(strcmp(cmd, "quit")==0 ||
		 strcmp(cmd, "exit")==0 ||
		 strcmp(cmd, "stop")==0 ||
		 strcmp(cmd, "q")==0 || strcmp(cmd, "x")==0
	) {
		cmd_data->control = COMMAND_CTL_EXIT;
		return 0;
	}

	// The user wants to stop the entire program
	if(strcmp(cmd, "die")==0) {
		cmd_data->control = COMMAND_CTL_DIE;

		const char* param = Command_PopParam(cmd_data);
		if(param==NULL) return EXIT_FAILURE;

		if(strcasecmp(param, "success")==0 ||
		   strcasecmp(param, "good")==0 ||
		   strcasecmp(param, "clean")==0 ||
		   strcasecmp(param, "ok")==0
		) return EXIT_SUCCESS;

		if(strcasecmp(param, "error")==0 ||
		   strcasecmp(param, "failure")==0 || strcasecmp(param, "fail")==0 ||
		   strcasecmp(param, "bad")==0
		) return EXIT_FAILURE;

		if(isdigit(param[0])==0) {
			printf("Note '%s' : Unknown parameter '%s'. Returning EXIT_FAILURE (%d) by default.\n", cmd, param, EXIT_FAILURE);
			return EXIT_FAILURE;
		}

		// Return the specified value
		return atoi(param);
	}

	// The user wants to get manual control
	if(strcmp(cmd, "manual")==0) {
		cmd_data->control = COMMAND_CTL_MANUAL;
		return 0;
	}

	// Display the commands, when executing a script
	if(strcmp(cmd, "commands-disp")==0) {
		int z = Command_PopParam_YesNoDef_Verbose(cmd_data, true, cmd);
		if(z<0) return -1;
		commands_disp = z;
		return 0;
	}
	if(strcmp(cmd, "commands-noquote")==0) {
		int z = Command_PopParam_YesNoDef_Verbose(cmd_data, true, cmd);
		if(z<0) return -1;
		commands_noquote = z;
		return 0;
	}

	if(strcmp(cmd, "script")==0) {
		const char* filename = Command_PopParam(cmd_data);
		if(filename==NULL) {
			printf("ERROR: no script file specified.\n");
			return -1;
		}
		FILE* F = fopen(filename, "rb");
		if(F==NULL) {
			printf("ERROR: the script file '%s' could not be opened.\n", filename);
			return -1;
		}
		int z = augh_Command_internal(Implem, F);
		fclose(F);
		return z;
	}

	if(strcmp(cmd, "shell")==0 || strcmp(cmd, "sh")==0) {
		// Grab the entire line
		char* command = NULL;
		int z = Command_PopParam_WholeLine_stralloc_verbose(cmd_data, cmd, &command);
		if(z!=0) return -1;
		// Execute
		if(command==NULL) {
			printf("ERROR: no command specified.\n");
			return -1;
		}
		fflush(NULL);
		return system(command);
	}

	if(strcmp(cmd, "cd")==0) {
		const char* dir = Command_PopParam(cmd_data);
		if(dir==NULL) {
			printf("ERROR: Missing value for command '%s'.\n", cmd);
			return -1;
		}
		int z = chdir(dir);
		if(z!=0) {
			printf("ERROR: Could not change directory: %s.\n", strerror(errno));
			return -1;
		}
		return 0;
	}

	if(strcmp(cmd, "pause")==0) {
		printf("Press [return] to continue. ");
		char* s = command_rl(NULL);
		if(s!=NULL) free(s);
		return 0;
	}

	if(strcmp(cmd, "echo")==0 || strcmp(cmd, "print")==0) {
		// Grab the entire line
		char* msg = NULL;
		int z = Command_PopParam_WholeLine_stralloc_verbose(cmd_data, cmd, &msg);
		if(z!=0) return -1;
		// Print
		if(msg!=NULL) msg = SplitString_trim_beg(msg);
		if(msg!=NULL) printf("%s", msg);
		putchar('\n');
		return 0;
	}

	if(strcmp(cmd, "m")==0 || strcmp(cmd, "multi")==0) {
		// Get the separator
		const char* separator = Command_PopParam(cmd_data);
		if(separator==NULL) {
			printf("ERROR: Missing separator for command '%s'.\n", cmd);
			return -1;
		}
		// Grab the entire line
		char* commands = NULL;
		int z = Command_PopParam_WholeLine_stralloc_verbose(cmd_data, cmd, &commands);
		if(z!=0) return -1;
		// Execute
		chain_list* list_commands = SplitString_tokensplit(commands, separator);
		foreach(list_commands, scan) {
			int z = AughCmd_Exec(Implem, scan->DATA);
			if(z!=0) { freechain(list_commands); return z; }
		}
		freechain(list_commands);
		return 0;
	}

	if(strcmp(cmd, "noerr")==0) {
		AughCmd_Exec(Implem, cmd_data);
		return 0;
	}

	if(strcmp(cmd, "augh-resource")==0) {
		printf("\n");
		printf("Real time .. %lg seconds\n", TimeGlobals_GetReal(Implem->env.globals));
		printf("CPU time ... %lg seconds\n", TimeGlobals_GetCpu(Implem->env.globals));
		printf("\n");
		// Note: the alias "/proc/self/..." is not used because it would lead
		// to the pid of the "cat" process
		char buffer [1024];
		printf("For reference, here is what /proc/self/statm says:\n");
		sprintf(buffer, "cat /proc/%u/statm", getpid());
		fflush(NULL);
		system(buffer);
		printf("\n");
		printf("For reference, here is what /proc/self/status says about Vm:\n");
		sprintf(buffer, "cat /proc/%u/status | grep Vm", getpid());
		fflush(NULL);
		system(buffer);
		printf("\n");
		return 0;
	}

	if(strcmp(cmd, "version")==0) {
		Command_Result_AddStatic(cmd_data, PACKAGE_VERSION);
		return 0;
	}

	if(strcmp(cmd, "check-integrity")==0) {
		Implem_CheckIntegrity(Implem);
		return 0;
	}

	// Sub- command interpreters

	if(strcmp(cmd, "build")==0) {
		return Build_Command(Implem, cmd_data);
	}
	if(strcmp(cmd, "hier")==0) {
		return Hier_Command(Implem, cmd_data);
	}
	if(strcmp(cmd, "techno")==0) {
		return Techno_Command(Implem, cmd_data);
	}
	if(strcmp(cmd, "op")==0) {
		return addops_command(Implem, cmd_data);
	}
	if(strcmp(cmd, "sched")==0) {
		return Schedule_Command(Implem, cmd_data);
	}
	if(strcmp(cmd, "forksynth")==0) {
		return TestSynth_Command(Implem, cmd_data);
	}
	if(strcmp(cmd, "core")==0) {
		return Core_Command(Implem, cmd_data);
	}
	if(strcmp(cmd, "map")==0) {
		return Map_Command(Implem, cmd_data);
	}
	if(strcmp(cmd, "netlist")==0) {
		return Netlist_Command(Implem, cmd_data);
	}
	if(strcmp(cmd, "plugin")==0) {
		return Plugin_Command(Implem, cmd_data);
	}
	if(strcmp(cmd, "impmod")==0) {
		return ImpMod_Command(Implem, cmd_data);
	}

	// Parameters

	if(strcmp(cmd, "target-freq")==0) {
		const char* param = Command_PopParam(cmd_data);
		if(param==NULL) {
			printf("ERROR: Missing value for command '%s'.\n", cmd);
			return -1;
		}
		double val = atof_multiplier(param);
		if(val<=0) return -1;
		Techno_SynthTarget_SetFrequency(Implem->synth_target, val);
	}
	else if(strcmp(cmd, "hwlimit")==0) {
		const char* param = Command_PopParam(cmd_data);
		if(param==NULL) {
			printf("ERROR: Missing parameter for command '%s'.\n", cmd);
			return -1;
		}
		freeptype(Implem->synth_target->resources);
		Implem->synth_target->resources = augh_read_hwlimits(Implem->synth_target->techno, param);
		if(Implem->synth_target->resources==NULL) {
			printf("ERROR Could not read the resource limits.\n");
			return -1;
		}
	}
	else if(strcmp(cmd, "hwmult")==0) {
		const char* param = Command_PopParam(cmd_data);
		if(param==NULL) {
			printf("ERROR: Missing parameter for command '%s'.\n", cmd);
			return -1;
		}
		double factor = 0;
		sscanf(param, "%lf", &factor);
		if(param[strlen(param)-1]=='%') factor /= 100;
		if(factor<=0) {
			printf("ERROR : Invalid factor value.\n");
			return -1;
		}
		Implem->synth_target->usage_is_default = false;
		foreach(Implem->synth_target->resources, scan) {
			scan->TYPE = scan->TYPE * factor;
		}
	}

	else if(strcmp(cmd, "vhdl-dir")==0) {
		char* param = Command_PopParam_stralloc(cmd_data);
		if(param==NULL) {
			printf("ERROR: Missing parameter for command '%s'.\n", cmd);
			return -1;
		}
		Implem->env.vhdl_dir = param;
	}
	else if(strcmp(cmd, "vhdl-prefix")==0) {
		char* param = Command_PopParam_stralloc(cmd_data);
		if(param==NULL) {
			printf("ERROR: Missing parameter for command '%s'.\n", cmd);
			return -1;
		}
		Implem->env.vhdl_prefix = param;
	}

	// Loading source files

	else if(strcmp(cmd, "cflags")==0) {
		// Grab the entire line
		char* cflags = NULL;
		int z = Command_PopParam_WholeLine_stralloc_verbose(cmd_data, cmd, &cflags);
		if(z!=0) return -1;
		// Update
		freechain(Implem->env.cflags);
		Implem->env.cflags = NULL;
		if(cflags!=NULL) {
			Implem->env.cflags = addchain(NULL, cflags);
		}
	}
	else if(strcmp(cmd, "cflags+")==0 || strcmp(cmd, "cflags-add")==0) {
		// Grab the entire line
		char* cflags = NULL;
		int z = Command_PopParam_WholeLine_stralloc_verbose(cmd_data, cmd, &cflags);
		if(z!=0) return -1;
		// Update
		if(cflags!=NULL) {
			Implem->env.cflags = ChainList_Add_Tail(Implem->env.cflags, cflags);
		}
	}
	else if(strcmp(cmd, "load")==0) {
		if(Implem->H!=NULL) {
			printf("ERROR: a design is already loaded.\n");
			return -1;
		}
		const char* filename = Command_PopParam(cmd_data);
		if(filename==NULL) {
			if(Implem->env.input_file==NULL) {
				printf("ERROR: Missing parameter for command '%s'.\n", cmd);
				return -1;
			}
			filename = Implem->env.input_file;
		}
		int z = Hier_Load(Implem, filename);
		if(z!=0) {
			printf("ERROR: Loading the file '%s' failed.\n", filename);
			return -1;
		}
	}
	else if(strcmp(cmd, "loadmod")==0) {
		const char* modname = Command_PopParam(cmd_data);
		const char* filename = Command_PopParam(cmd_data);
		if(filename==NULL) {
			printf("ERROR: Missing parameters for command '%s'.\n", cmd);
			return -1;
		}
		// Load
		implem_t* modImplem = ImpMod_Load(Implem->env.globals, modname, filename);
		// Note: the error messages are printed by the inner function
		if(modImplem==NULL) return -1;
	}

	// Processes

	else if(strcmp(cmd, "synth-min")==0) {
		if(Implem->H==NULL) {
			printf("Error: no design is loaded, can't execute the command '%s'.\n", cmd);
			return -1;
		}
		Schedule(Implem, SCHED_ALGO_LIST);
		augh_postprocess(Implem);
		Hier_Timing_Update(Implem, false);
		Hier_Timing_Show(Implem);
	}
	else if(strcmp(cmd, "elaborate")==0) {
		if(Implem->H==NULL) {
			printf("Error: no design is loaded, can't execute the command '%s'.\n", cmd);
			return -1;
		}
		if(Implem->synth_state.schedule!=0) {
			printf("Error: The design state is too advanced, can't execute the command '%s'.\n", cmd);
			return -1;
		}
		Core_Elaborate(Implem);
		augh_postprocess(Implem);
		Hier_PrintDetails(Implem->H);
		Hier_Timing_Update(Implem, false);
		Hier_Timing_Show(Implem);
		Hier_ClockCycles_Display(Implem);
	}
	else if(strcmp(cmd, "synth-finish")==0) {
		if(Implem->H==NULL) {
			printf("Error: no design is loaded, can't execute the command '%s'.\n", cmd);
			return -1;
		}
		if(Implem->synth_state.schedule==0) {
			Schedule(Implem, SCHED_ALGO_LIST);
		}
		if(Implem->synth_state.postproc==0) augh_postprocess(Implem);
		Hier_Timing_Update(Implem, false);
		Hier_Timing_Show(Implem);
	}
	else if(strcmp(cmd, "postprocess")==0) {
		augh_postprocess(Implem);
	}
	else if(strcmp(cmd, "postprocess-all")==0) {
		avl_pp_foreach(&Implem->env.globals->implem_models, scanModel) {
			implem_t* modImplem = scanModel->data;
			augh_postprocess(modImplem);
		}
	}

	else if(strcmp(cmd, "vhdl")==0) {
		if(Implem->netlist.top==NULL) {
			printf("Error: The components are incomplete.\n");
			return -1;
		}
		Map_Netlist_GenVHDL(Implem);
	}
	else if(strcmp(cmd, "vhdl-all")==0) {
		avl_pp_foreach(&Implem->env.globals->implem_models, scanModel) {
			implem_t* modImplem = scanModel->data;
			Map_Netlist_GenVHDL(modImplem);
		}
	}

	else if(strcmp(cmd, "backend-gen-prj")==0) {
		if(Implem->synth_target->techno->synth_gen_prj==NULL) {
			printf("Error: The technology '%s' has no function to generate synthesis project files.\n", Implem->synth_target->techno->name);
			return -1;
		}
		return Implem->synth_target->techno->synth_gen_prj(Implem);
	}
	else if(strcmp(cmd, "backend-launch")==0) {
		if(Implem->synth_target->techno->synth_launch==NULL) {
			printf("Error: The technology '%s' has no function to launch back-end synthesis.\n", Implem->synth_target->techno->name);
			return -1;
		}
		return Implem->synth_target->techno->synth_launch(Implem);
	}

	// Here, the command is unknown.

	else {
		printf("Error: Unknown command '%s'.\n", cmd);
		return -1;
	}

	return 0;
}



//=================================================
// Main Command module functions
//=================================================

// Execute and process results
int AughCmd_Exec(implem_t* Implem, command_t* cmd_data) {

	// Execute command
	int z = augh_Command_Exec_noresult(Implem, cmd_data);

	// Process results
	if(cmd_data->callback_result_finish != NULL) {
		cmd_data->callback_result_finish(cmd_data);
	}
	freechain(cmd_data->list_res);
	cmd_data->list_res = NULL;

	// Control
	if(cmd_data->control != 0) {
		if(cmd_data->control == COMMAND_CTL_DIE) {
			exit(z);
		}
	}

	return z;
}

// Wrapper to execute a bare text command - Warning, the command is modified
int AughCmd_Exec_str(implem_t* Implem, char* commands) {

	// Data structure to launch execution of commands
	command_t cmd_data_i;
	command_t* cmd_data = &cmd_data_i;

	// Init callbacks
	Command_data_init(cmd_data);

	// Init fields
	cmd_data->data_command = commands;

	return AughCmd_Exec(Implem, cmd_data);
}

// The input file is stdin, interactive mode, with the readline library
int AughCmd_Interactive(implem_t *Implem) {
	return augh_Command_internal(Implem, NULL);
}

// Execute a script file
// Select the command interpreter according to the file name extension
int AughCmd_Script(implem_t *Implem, const char* filename) {
	char* ext = NULL;

	// Get the file name extension
	if(filename!=NULL) {
		char const * ptr_lastdot = strrchr(filename, '.');
		if(ptr_lastdot!=NULL) {
			ptr_lastdot++;
			if((*ptr_lastdot)!=0) ext = namealloc(ptr_lastdot);
		}
	}

	if(ext != NULL) {
		// Get the interpreter callback
		avl_pp_node_t* pp_node = avl_pp_get_node(&augh_ext_interpreters, ext);
		if(pp_node!=NULL) {
			// Execute with plugin-in interpreter
			augh_cmd_interp_t interp = pp_node->data;
			return interp(Implem, filename);
		}
	}

	// From here, use Augh built-in command interpreter

	FILE* F = NULL;

	// Open the file
	if(filename!=NULL) {
		F = fopen(filename, "rb");
		if(F==NULL) {
			printf("ERROR: The script file '%s' could not be opened.\n", filename);
			return -1;
		}
	}

	// Execute
	int z = augh_Command_internal(Implem, F);

	// Clean
	if(F!=NULL) fclose(F);

	return z;
}

// Only use this to execute .rc files
// return true if the file was found and opened
bool AughCmd_Loadrc(const char* path, implem_t* Implem) {
	FILE* F;
	int z;

	// Check the local .aughrc
	F = fopen(path, "rb");
	if(F==NULL) return false;
	z = augh_Command_internal(Implem, F);
	fclose(F);

	if(z!=0) exit(EXIT_FAILURE);
	return true;
}

// Declare a command interpreter for specific file extensions
void AughCmd_Decl_Interp(const char* ext, augh_cmd_interp_t interp) {
	avl_pp_node_t* pp_node = NULL;
	bool b = avl_pp_add_node(&augh_ext_interpreters, ext, &pp_node);
	if(b==false) {
		printf("Error: A command interpreter for extension '%s' is already set\n", ext);
		exit(EXIT_FAILURE);
	}
	pp_node->data = interp;
}

// Init the Command module
void AughCmd_Init() {
	avl_pp_init(&augh_ext_interpreters);
}

