
#ifndef _CORE_H_
#define _CORE_H_

#include <mut.h>

typedef struct freedom_degree_t  freedom_degree_t;

#include "auto.h"
#include "hierarchy.h"
#include "hwres.h"


//===================================================
// Definition of structures
//===================================================

typedef enum freedom_degree_type_t {
	FD_NONE            = 0,
	FD_ADD_OPS         = 1,
	FD_MEM_INLINE      = 2,
	FD_LOOP_UNROLL     = 3,
	FD_COND_WIRE       = 4,
	FD_PP_DIRECT       = 5,
	FD_MULTI           = 6,  // Contains several freedom degrees
} freedom_degree_type_t;

typedef enum freedom_degree_skip_type_t {
	FD_SKIP_ADDOPS      = 0,
	FD_SKIP_MEM_R       = 1,  // Adding address Read ports
	FD_SKIP_MEM_W       = 2,  // Adding address Write ports
	FD_SKIP_MEM_D       = 3,  // Activating Direct access

	FD_SKIP_INLINE      = 10,
	FD_SKIP_INLINE_RAM  = 4,
	FD_SKIP_INLINE_ROM  = 5,

	FD_SKIP_UNROLL      = 12,
	FD_SKIP_UNROLL_PART = 6,
	FD_SKIP_UNROLL_FULL = 7,

	FD_SKIP_WIRE          = 11,
	FD_SKIP_WIRE_SEQ_AUTO = 8,
	FD_SKIP_WIRE_ONECYCLE = 9,

	FD_SKIP_PP_DIRECT   = 13,
} freedom_degree_skip_type_t;

struct freedom_degree_t {
	freedom_degree_type_t  type;

	// The Implem structure this FD refers to
	implem_t* Implem;

	// The target of the freedom degree
	union {
		// To add operators
		struct {
			hwres_heap_t*   heap;
			ptype_list*     names;
		} addops;
		// For SCALAR_REPLACE, INLINE, PP_DIRECT
		struct {
			char* name;
		} array;
		// For condition wiring and loop unrolling.
		struct {
			hier_node_t*  node;
			union {
				unsigned          unroll_factor;  // Note: 0 for full unroll
				hier_wire_style_t wire_style;
			} d;
		} hier;
		// For freedom degree MULTI
		chain_list*     list_degrees;
	} target;

	// Cost and gain, estimations
	double                 time_gain;
	ptype_list*            resource_cost;
	double                 resource_score;  // max of cost/available for all resource types
	double                 final_score;     // Ratio Time gain / Resource score
};

extern bool core_dupimplem_inxs;

extern bool core_param_weight_fork;
extern unsigned core_param_max_degrees;


//===================================================
// Functions
//===================================================

void Core_ImplemFree(implem_t* Implem);
void Core_ImplemInit(implem_t* Implem);

void Core_DupImplem_EnsureVoid(implem_t* Implem);
implem_t* Core_DupImplem_Synthesize(implem_t* Implem, chain_list* list_fd);
void Core_DupImplem_SetResults(implem_t* Implem, implem_t* dupImplem);

freedom_degree_t* freedom_degree_new();
void freedom_degree_free(freedom_degree_t* degree);
void freedom_degree_freelist(chain_list* list);

ptype_list* FD_HwCost_Op_RefGetMake(implem_t* Implem, char* type_name);
ptype_list* FD_HwCost_MemPortR_MuxOnly_RefGetMake(implem_t* Implem, char* mem_name);
ptype_list* FD_HwCost_MemPortsR(implem_t* Implem, char* mem_name, unsigned ports_nb);
ptype_list* FD_HwCost_HeapRes(implem_t* Implem, hwres_heap_t* heap);
ptype_list* FD_HwCost_HeapRes_Missing_nodup(implem_t* Implem, hwres_heap_t* heap);
ptype_list* FD_HwCost_HeapRes_Missing(implem_t* Implem, hwres_heap_t* heap);
ptype_list* FD_HwCost_Level_Missing(implem_t* Implem, hier_node_t* node);

void FreedomDegrees_Build(implem_t* Implem);
int  FreedomDegrees_SetFinalScore(implem_t* Implem);
void FreedomDegrees_BuildAndScore(implem_t* Implem);

void FreedomDegrees_fprintf(FILE* F, implem_t* Implem, chain_list* list);
int  FreedomDegrees_fscanf(FILE* F, implem_t* Implem, chain_list** res_list);

int Core_FreedomDegree_Apply(implem_t* Implem, freedom_degree_t* degree, unsigned params);
int Core_FreedomDegree_ApplyList(implem_t* Implem, chain_list* list, indent_t* indent, unsigned params);

void Core_FreedomDegree_FlagImpossible(implem_t* Implem, freedom_degree_t* degree);
void Core_FreedomDegree_ClearAllImpossible(implem_t* Implem);

int Core_Elaborate(implem_t* Implem);

int Core_Command(implem_t* Implem, command_t* cmd_data);



#endif  // _CORE_H_

