
/*
Miscellaneous work with operators
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#include "auto.h"
#include "hierarchy.h"
#include "addops.h"
#include "memtransfo.h"
#include "map.h"
#include "command.h"



//==========================================================
// Usage of operators
//==========================================================

void OpUsageStats_Init(op_usage_t* res) {
	res->trans_nb = 0;

	res->out_max_width = 0;
	res->out_max_left = 0;
	res->out_min_right = -1;

	res->op_max_nb = 0;
	res->op_max_width = 0;
	res->op_max_of_min_width = -1;
	res->op_max_widthes = addnum(NULL, 0);
}
void OpUsageStats_Reset(op_usage_t* res) {
	freenum(res->op_max_widthes);
}

void OpUsageStats_Print(op_usage_t* res) {
	printf("TransNb %d", res->trans_nb);

	printf(", OutMaxW %d", res->out_max_width);
	printf(", OutMaxL %d", res->out_max_left);
	printf(", OutMinR %d", res->out_min_right);

	printf(", OpMaxNb %d", res->op_max_nb);
	printf(", OpMaxW %d", res->op_max_width);
	printf(", OpMaxOfMinW %d", res->op_max_of_min_width);
	printf(", OpMaxWidthes");
	foreach(res->op_max_widthes, scan) printf(" %ld", scan->DATA);

	printf("\n");
}

void OpUsageStats_Get_Implem(implem_t* Implem, char* op_type, op_usage_t* res) {

	// Initialize the results
	OpUsageStats_Init(res);

	// Scan all actions to see what could be added
	avl_p_foreach(&Implem->H->NODES[HIERARCHY_STATE], scan) {
		hier_node_t* node = scan->data;
		hier_state_t* trans_data = &node->NODE.STATE;

		unsigned trans_found = 0;
		foreach(trans_data->actions, scanAction) {
			hier_action_t* action = scanAction->DATA;
			// Get the needed resources
			hwres_heap_t* heap = hwres_heap_getneed_vex(action->expr);
			// Get the node with the wanted operator => FIXME Should get it with the tree_find() instead!
			avl_pp_foreach(&heap->type_ops, avl_node) {
				hw_res_t* elt = avl_node->data;
				if(elt->name!=op_type) continue;
				trans_found = 1;
				// Scan the Vexes, get the max width and build the list of operands
				foreach(elt->vexes_r, scanVex) {
					hvex_t* expr = scanVex->DATA;

					// Scan the list of operands
					unsigned operands_nb = 0;
					int op_max_width = 0;
					int op_min_width = -1;
					num_list* cur_max_width = res->op_max_widthes;

					hvex_foreach(expr->operands, VexOp) {
						unsigned operand_width = VexOp->width;
						// For signed operators, add one bit for sign extension.
						// FIXME This is too much a model-specific hack. And it doen't handle unsigned multiplications.
						if(expr->model==HVEX_MUL) {
							if(operand_width<expr->width && hvex_is_unsigned(VexOp)==true) operand_width++;
						}
						// Stats about this operand
						if(operand_width>op_max_width)                   op_max_width = operand_width;
						if(op_min_width<0 || operand_width<op_min_width) op_min_width = operand_width;
						if(operand_width>cur_max_width->DATA)            cur_max_width->DATA = operand_width;

						// Next operand
						if(cur_max_width->NEXT==NULL) cur_max_width->NEXT = addnum(NULL, 0);
						cur_max_width = cur_max_width->NEXT;
						operands_nb++;
					}

					// Save the stats
					if(operands_nb > res->op_max_nb)                                          res->op_max_nb = operands_nb;
					if(op_max_width > res->op_max_width)                                      res->op_max_width = op_max_width;
					if(res->op_max_of_min_width<0 || op_min_width > res->op_max_of_min_width) res->op_max_of_min_width = op_min_width;

					if(expr->width > res->out_max_width)                                      res->out_max_width = expr->width;
					if(expr->left > res->out_max_left)                                        res->out_max_left = expr->left;
					if(res->out_min_right<0 || expr->right < res->out_min_right)              res->out_min_right = expr->right;
				}
			}  // Occurrences of the operator
			// Free the resources
			hwres_heap_free(heap);
		}  // Actions

		if(trans_found!=0) res->trans_nb++;
	}  // Trans

}



//======================================================================
// Generic combinatorial component
//======================================================================

#include "techno_delay.h"
#include "hierarchy_build.h"

// Data structure that is used by : Parser + HVEX model + Comp model
typedef struct op_generic_comb_t  op_generic_comb_t;
struct op_generic_comb_t {
	char* func_name;
	char* comp_name;

	// Misc parameters
	bool is_comb;
	bool inputs_swap;
	double delay;

	// Hardware resources
	ptype_list* resources;

	// List of ports, in user-specified order
	chain_list* list_in_portnames;

	// Port name to size
	avl_pi_tree_t tree_outputs;
	avl_pi_tree_t tree_inputs;

	// HVEX and component models
	hvex_model_t* hvex_model;
	netlist_comp_model_t* comp_model;
};

op_generic_comb_t* Op_Comp_GenericComb_NewData() {

	// Create extra data structure
	op_generic_comb_t* opdata = calloc(1, sizeof(*opdata));

	// Init fields

	opdata->is_comb = false;
	opdata->inputs_swap = false;

	avl_pi_init(&opdata->tree_outputs);
	avl_pi_init(&opdata->tree_inputs);

	return opdata;
}

void Op_Comp_GenericComb_AvailRes(netlist_comp_t* comp, hwres_heap_t* avail_heap) {
	hwres_heap_add_avail_op(avail_heap, comp->model->name, 1);
}

// Netlist-specific callbacks

static ptype_list* Op_Comp_GenericComb_EvalSize(synth_target_t* synth, netlist_comp_t* comp) {
	op_generic_comb_t* opdata = comp->model->data;
	return dupptype(opdata->resources);
}

static void Op_Comp_GenericComb_PropagDelay(techno_delay_t* data, netlist_port_t* port, double* delays) {
	op_generic_comb_t* opdata = port->component->model->data;
	// Get the worst delay on all input bits
	double max_delay_in = 0;
	avl_pp_foreach(&port->component->ports, scanPort) {
		netlist_port_t* loc_port = scanPort->data;
		if(loc_port->direction!=NETLIST_PORT_DIR_IN) continue;
		double* delays_in = Techno_Delay_PortDelays_GetAdd(data, loc_port);
		Techno_Delay_Process_PortIn(data, loc_port, delays_in);
		for(unsigned i=0; i<loc_port->width; i++) max_delay_in = GetMax(max_delay_in, delays_in[i]);
	}
	// Add the delay of the component
	double d = max_delay_in + opdata->delay;
	for(unsigned i=0; i<port->width; i++) delays[i] = d;
}

static netlist_comp_t* Op_Comp_GenericComb_New(char* name, netlist_comp_model_t* model) {
	netlist_comp_t* comp = Netlist_Comp_New(model);
	comp->name = name;

	op_generic_comb_t* opdata = model->data;

	avl_pi_foreach(&opdata->tree_outputs, scan) {
		Netlist_Comp_AddPort_Out(comp, scan->key, scan->data);
	}
	avl_pi_foreach(&opdata->tree_inputs, scan) {
		Netlist_Comp_AddPort_In(comp, scan->key, scan->data);
	}

	return comp;
}

// HVEX callbacks

static map_netlist_datapath_t* Op_Hvex_GenericComb_Map(map_netlist_data_t* map_data, hvex_dico_elt_t* Expr) {
	op_generic_comb_t* opdata = Expr->model->data;
	assert(Expr->args_nb == ChainList_Count(opdata->list_in_portnames));

	map_netlist_datapath_t* datapath = Map_Netlist_Datapath_New();
	datapath->Expr = Expr;

	// Get the expressions to assign

	chain_list* list_datapath_operands = NULL;

	foreach(datapath->Expr->args, scanArg) {
		hvex_dico_ref_t* Expr_operand = scanArg->DATA;
		map_netlist_datapath_t* Datapath_operand = Map_Netlist_Datapath_Expr(map_data, Expr_operand->ref.elt);
		datapath->sub_datapaths = addchain(datapath->sub_datapaths, Datapath_operand);
		list_datapath_operands = addchain(list_datapath_operands, Datapath_operand);
	}

	list_datapath_operands = reverse(list_datapath_operands);

	// Get an available operator, with enough width.
	netlist_comp_model_t* comp_model = opdata->comp_model;
	netlist_comp_t* comp = NULL;

	if( (comp_model->flags & NETLIST_COMPMOD_FLAG_CANSHARE)==0 ) {
		// Create a new component
		char* name = Map_Netlist_MakeInstanceName(map_data->Implem, comp_model);
		comp = comp_model->func_comp_new(name, comp_model);
		Netlist_Comp_SetChild(map_data->Implem->netlist.top, comp);
		Map_CompType_AddComp(map_data, comp);
	}
	else {
		// Get an available component
		map_comptype_t* comptype = Map_CompType_GetAdd(map_data, comp_model->name);
		avl_p_foreach(&comptype->all, scan) {
			netlist_comp_t* comp1 = scan->data;
			bool is_used = avl_p_isthere(&map_data->cur_state.hwusage.comp_used, comp1);
			if(is_used==true) continue;
			comp = comp1;
			break;
		}
	}

	if(comp==NULL) {
		Map_Netlist_Datapath_BuildNew_Op_error(
			map_data, datapath, datapath->Expr->model, comp_model, __LINE__
		);
	}

	// Flag the component as used
	avl_p_add_overwrite(&datapath->hwusage.comp_used, comp, comp);

	// Assign operands
	// Do sign extension for each input, and get the MUX inputs.

	chain_list* scan_in_expr      = datapath->Expr->args;
	chain_list* scan_in_operands  = opdata->list_in_portnames;
	chain_list* scan_in_datapaths = list_datapath_operands;

	while(scan_in_expr != NULL) {
		// Get the input port
		char* name = scan_in_operands->DATA;
		netlist_port_t* port = Netlist_Comp_GetPort(comp, name);
		hvex_dico_ref_t* Expr_operand = scan_in_expr->DATA;
		map_netlist_datapath_t* Datapath_operand = scan_in_datapaths->DATA;
		// Do assignment according to the component being shared of not
		if( (comp_model->flags & NETLIST_COMPMOD_FLAG_CANSHARE)==0 ) {
			assert(port->source==NULL);
			// Resize expression and assign port source
			port->source = Map_Netlist_ValCat_DupResize(Datapath_operand->valcat, Expr_operand, port->width);
		}
		else {
			Map_Netlist_Port_MuxGetAddInput_SetState_FromExpr(
				map_data, datapath, port, Expr_operand, Datapath_operand->valcat
			);
		}
		// Next operand
		scan_in_expr      = scan_in_expr->NEXT;
		scan_in_operands  = scan_in_operands->NEXT;
		scan_in_datapaths = scan_in_datapaths->NEXT;
	}

	// Build the output ValCat expression

	char* name_out = opdata->tree_outputs.root->key;
	netlist_port_t* port_out = Netlist_Comp_GetPort(comp, name_out);

	netlist_val_cat_t* valcat_out = Netlist_ValCat_Make_FullPort(port_out);
	valcat_out->width = datapath->Expr->width;
	valcat_out->left  = datapath->Expr->left;
	valcat_out->right = datapath->Expr->right;

	datapath->valcat = addchain(NULL, valcat_out);

	return datapath;
}

static void Op_Hvex_GenericComb_NeededRes_Vex(hvex_t* vex, hwres_scanvex_t* hwres_data) {
	op_generic_comb_t* opdata = vex->model->data;
	if( (opdata->comp_model->flags & NETLIST_COMPMOD_FLAG_CANSHARE)!=0 ) {
		hwres_heap_makeadd_needed_op_vex(hwres_data->heap_needed, opdata->comp_model->name, vex);
	}
	hvex_foreach(vex->operands, VexOp) hwres_scanvex_recurs(VexOp, hwres_data);
}

static void Op_Hvex_GenericComb_NeededRes_VexAlloc(hvex_dico_ref_t* ref, hwres_scanvex_t* hwres_data) {
	hvex_dico_elt_t* elt = ref->ref.elt;
	op_generic_comb_t* opdata = elt->model->data;
	if( (opdata->comp_model->flags & NETLIST_COMPMOD_FLAG_CANSHARE)!=0 ) {
		hvex_t* Expr = VexAlloc_GenVex_Ref(ref);
		hwres_heap_makeadd_needed_op_vex(hwres_data->heap_needed, opdata->comp_model->name, Expr);
		hvex_free(Expr);
	}
	foreach(elt->args, scan) hwres_scanvexalloc_recurs(scan->DATA, hwres_data);
}

// Parser replacement callback

static int Op_Parser_GenericComb_Builtin(
	builtinfunc_t* builtin, build_symtops_data_t* data, implem_t* modImplem, netlist_comp_t* compInst, hvex_t* Expr
) {
	op_generic_comb_t* opdata = builtin->data;
	// FIXME At least ensure that the number of arguments is correct
	// Simply change the HVEX model
	Expr->model = opdata->hvex_model;
	Expr->data = NULL;
	return 0;
}



//======================================================================
// Local command interpreter
//======================================================================

int addops_command(implem_t* Implem, command_t* cmd_data) {
	const char* cmd = Command_PopParam(cmd_data);

	if(cmd==NULL) {
		printf("Error 'op' : No command received. Try 'help'.\n");
		return -1;
	}

	if(strcmp(cmd, "help")==0) {
		printf(
			"Possible commands :\n"
			"  help            Display this help.\n"
			"  add <list>      Add operators. Syntax example : 'add:1/sub:3'.\n"
			"  mem-add-r <name> <ports_r_nb>\n"
			"                  Add address read ports to a multiport memory.\n"
			"  mem-add-w <name> <ports_w_nb>\n"
			"                  Add address write ports to a multiport memory.\n"
			"  mem-add-d <name>\n"
			"                  Activate direct cell access to a multiport memory.\n"
			"  mem-replace-direct <name>\n"
			"                  Replace an array by separate registers.\n"
			"                  Only applies to arrays accessed with known adresses.\n"
			"  pp-convert-direct <name>\n"
			"                  Convert Ping-Pong component to direct cell access.\n"
			"                  Warning: whether the array is accessed with known adresses is not checked.\n"
			"  list-shared     Display the component models that are shared.\n"
			"  noshare-all     Disable sharing for all component models.\n"
			"  noshare <names>\n"
			"                  Disable component sharing for the specified component models.\n"
			"  auto-mem-readbuf <bool>\n"
			"                  Enable or disable automatic insertion of read buffers.\n"
			"  mem-add-readbuf <names>\n"
			"                  Add a read buffer on the specified memory components.\n"
			"  extern-comp [options]\n"
			"                  Declare components for which implementation is user-provided.\n"
			"    -func <name>  Function name to replaced when parsing.\n"
			"    -comp <name>  Component name for RTL.\n"
			"    -combi        Indicate a purely combinational component.\n"
			"    -shared       Set this component as shared.\n"
			"    -out <name> <size>.\n"
			"                  Declare output port, name and size.\n"
			"    -in <name> <size>\n"
			"                  Declare input port, name and size.\n"
			"    -delay <d>    Set a traversal delay, in ns.\n"
			"    -hwres <res>  Set the resources used by one instance of this component.\n"
		);
		return 0;
	}

	if(strcmp(cmd, "add")==0) {
		const char* str = Command_PopParam(cmd_data);
		if(str==NULL) {
			printf("Error : No list of operators was received.\n");
			return -1;
		}

		// List the operators to add
		ptype_list* list = ReadString_NameNum(str);
		if(list==NULL) {
			printf("Error : Could not parse the list of operators.\n");
			exit(EXIT_FAILURE);
		}

		// Add the operators
		foreach(list, scan) {
			char* model = namealloc(scan->DATA);
			unsigned number = scan->TYPE;
			int z = Map_Netlist_AddSomeOperators(Implem, Netlist_CompModel_Get(model), number);
			if(z!=0) {
				printf("Error : Adding the operators failed.\n");
				exit(EXIT_FAILURE);
			}
		}

		freeptype(list);
	}

	else if(strcmp(cmd, "mem-add-r")==0) {
		// Parameters = name [number]
		char* name = Command_PopParam_namealloc(cmd_data);
		const char* number_str = Command_PopParam(cmd_data);
		if(name==NULL) {
			printf("Error : Missing memory name for command '%s'. Try 'help'.\n", cmd);
			return -1;
		}
		int number = 1;
		if(number_str!=NULL) number = atoi(number_str);
		// Get the component
		netlist_comp_t* comp = Map_Netlist_GetComp_verbose(Implem, name, NETLIST_COMP_MEMORY);
		if(comp==NULL) return -1;
		// Set the parameters to the component
		for(int i=0; i<number; i++) {
			netlist_access_t* memIn = Netlist_Mem_AddAccess_RA(comp);
			if(memIn==NULL) {
				printf("WARNING %s:%d : Adding a Read port for array '%s' failed.\n", __FILE__, __LINE__, name);
				return -1;
			}
		}
	}
	else if(strcmp(cmd, "mem-add-w")==0) {
		// Parameters = name [number]
		char* name = Command_PopParam_namealloc(cmd_data);
		const char* number_str = Command_PopParam(cmd_data);
		if(name==NULL) {
			printf("Error : Missing memory name for command '%s'. Try 'help'.\n", cmd);
			return -1;
		}
		int number = 1;
		if(number_str!=NULL) number = atoi(number_str);
		// Get the component
		netlist_comp_t* comp = Map_Netlist_GetComp_verbose(Implem, name, NETLIST_COMP_MEMORY);
		if(comp==NULL) return -1;
		// Set the parameters to the component
		for(int i=0; i<number; i++) {
			netlist_access_t* memIn = Netlist_Mem_AddAccess_WA(comp);
			if(memIn==NULL) {
				printf("WARNING %s:%d : Adding a Write port for array '%s' failed.\n", __FILE__, __LINE__, name);
				return -1;
			}
		}
	}
	else if(strcmp(cmd, "mem-add-d")==0) {
		// Parameters = name [number]
		char* name = Command_PopParam_namealloc(cmd_data);
		if(name==NULL) {
			printf("Error : Missing component name for command '%s'. Try 'help'.\n", cmd);
			return -1;
		}
		// Get the component
		netlist_comp_t* comp = Map_Netlist_GetComp_verbose(Implem, name, NETLIST_COMP_MEMORY);
		if(comp==NULL) return -1;
		// Set the parameter to the component
		Netlist_Mem_EnableDirect(comp);
	}

	else if(strcmp(cmd, "mem-replace-direct")==0) {
		char* name = Command_PopParam_namealloc(cmd_data);
		if(name==NULL) {
			printf("Error 'op' : Missing memory name, can't execute the command '%s'.\n", cmd);
			return -1;
		}
		return ReplaceMem_ByName_verbose(Implem, name);
	}
	else if(strcmp(cmd, "pp-convert-direct")==0) {
		// Parameters = name [number]
		char* name = Command_PopParam_namealloc(cmd_data);
		if(name==NULL) {
			printf("Error : Missing component name for command '%s'. Try 'help'.\n", cmd);
			return -1;
		}

		// Get the component
		netlist_comp_t* comp = Map_Netlist_GetComponent(Implem, name);
		if(comp==NULL) {
			printf("Error : No component named '%s' was found.\n", name);
			return -1;
		}

		// Apply
		if(comp->model==NETLIST_COMP_PINGPONG_IN) {
			Netlist_PingPong_EnableDirect(comp);
		}
		else if(comp->model==NETLIST_COMP_PINGPONG_OUT) {
			Netlist_PingPong_EnableDirect(comp);
		}
		else {
			printf("Error : The component '%s' of type '%s' is not handled.\n", comp->name, comp->model->name);
			return -1;
		}
	}

	else if(strcmp(cmd, "list-shared")==0) {
		avl_pp_tree_t* tree = Netlist_CompModel_GetTree();
		unsigned count = 0;
		avl_pp_foreach(tree, scan) {
			netlist_comp_model_t* model = scan->data;
			if( (model->flags & NETLIST_COMPMOD_FLAG_CANSHARE)==0 ) continue;
			if(count > 0) printf(" ");
			printf("%s", model->name);
			count ++;
		}
		if(count > 0) printf("\n");
	}
	else if(strcmp(cmd, "noshare-all")==0) {
		avl_pp_tree_t* tree = Netlist_CompModel_GetTree();
		unsigned count = 0;
		avl_pp_foreach(tree, scan) {
			netlist_comp_model_t* model = scan->data;
			if( (model->flags & NETLIST_COMPMOD_FLAG_CANSHARE)==0 ) continue;
			if(count > 0) printf(" ");
			printf("%s", model->name);
			count ++;
			// Clear the flag CANSHARE
			model->flags &= ~NETLIST_COMPMOD_FLAG_CANSHARE;
		}
		if(count > 0) printf("\n");
	}
	else if(strcmp(cmd, "noshare")==0) {
		unsigned count = 0;
		// Scan all model names given
		do {
			char* name = Command_PopParam_namealloc(cmd_data);
			if(name==NULL) break;
			// Get the component model
			netlist_comp_model_t* model = Netlist_CompModel_Get(name);
			if(model==NULL) {
				printf("Error : Unknown component model '%s'.\n", name);
				return -1;
			}
			// Clear the flag CANSHARE, if it is set
			model->flags &= ~NETLIST_COMPMOD_FLAG_CANSHARE;
			count ++;
		} while(1);
		// Emit a warning if no models were specified
		if(count==0) {
			printf("Warning : No component model name was given for command '%s'. Try 'help'.\n", cmd);
		}
	}

	else if(strcmp(cmd, "auto-mem-readbuf")==0) {
		int z = Command_PopParam_YesNoDef_Verbose(cmd_data, true, cmd);
		if(z<0) return -1;
		Implem->env.auto_mem_readbuf = z;
		return 0;
	}
	else if(strcmp(cmd, "mem-add-readbuf")==0) {
		unsigned count = 0;
		// Scan all component names given
		do {
			char* name = Command_PopParam_namealloc(cmd_data);
			if(name==NULL) break;
			// Get the component
			netlist_comp_t* comp = Map_Netlist_GetComp_verbose(Implem, name, NETLIST_COMP_MEMORY);
			if(comp==NULL) return -1;
			// Perform read buffer insertion
			int z = Mem_InsertReadBuf(Implem, comp);
			if(z!=0) {
				printf("Error: Insertion of read buffer failed for component '%s'\n", comp->name);
				return -1;
			}
			count ++;
		} while(1);
		// Emit a warning if no component name was specified
		if(count==0) {
			printf("Warning : No component name was given for command '%s'. Try 'help'.\n", cmd);
		}
		return 0;
	}

	else if(strcmp(cmd, "extern-comp")==0) {
		// Create a new data structure
		op_generic_comb_t* opdata = Op_Comp_GenericComb_NewData();;
		bool is_shared = false;

		// Get options
		unsigned errors_nb = 0;
		do {
			const char* param = Command_PopParam_namealloc(cmd_data);
			if(param==NULL) break;

			if(strcmp(param, "-func")==0) {
				char* name = Command_PopParam_stralloc(cmd_data);
				if(name == NULL) {
					printf("Error 'op' command '%s' : Missing parameters for option '%s'. Try 'help'.\n", cmd, param);
					errors_nb ++;
					break;
				}
				opdata->func_name = name;
			}
			else if(strcmp(param, "-comp")==0) {
				char* name = Command_PopParam_namealloc(cmd_data);
				if(name == NULL) {
					printf("Error 'op' command '%s' : Missing parameters for option '%s'. Try 'help'.\n", cmd, param);
					errors_nb ++;
					break;
				}
				opdata->comp_name = name;
			}

			else if(strcmp(param, "-combi")==0) {
				opdata->is_comb = true;
			}
			else if(strcmp(param, "-shared")==0) {
				is_shared = true;
			}

			else if(strcmp(param, "-out")==0 || strcmp(param, "-in")==0) {
				const char* param_name = Command_PopParam_stralloc(cmd_data);
				const char* param_size = Command_PopParam(cmd_data);
				if(param_size == NULL) {
					printf("Error 'op' command '%s' : Missing parameters for option '%s'. Try 'help'.\n", cmd, param);
					errors_nb ++;
					break;
				}
				unsigned size = atoi(param_size);
				if(size == 0) {
					printf("Error 'op' command '%s' : Invalid size for port '%s'. Try 'help'.\n", cmd, param_name);
					errors_nb ++;
					break;
				}
				if(avl_pi_isthere(&opdata->tree_outputs, param_name)==true || avl_pi_isthere(&opdata->tree_inputs, param_name)==true) {
					printf("Error 'op' command '%s' : Multiple declaration of port '%s'. Try 'help'.\n", cmd, param_name);
					errors_nb ++;
					break;
				}
				if(strcmp(param, "-out")==0) {
					avl_pi_add_overwrite(&opdata->tree_outputs, param_name, size);
				}
				else {
					avl_pi_add_overwrite(&opdata->tree_inputs, param_name, size);
					opdata->list_in_portnames = addchain(opdata->list_in_portnames, (char*)param_name);
				}
			}

			else if(strcmp(param, "-delay")==0) {
				const char* param_delay = Command_PopParam(cmd_data);
				if(param_delay == NULL) {
					printf("Error 'op' command '%s' : Missing parameters for option '%s'. Try 'help'.\n", cmd, param);
					errors_nb ++;
					break;
				}
				double delay = atof(param_delay);
				if(delay < 0) {
					printf("Error 'op' command '%s' : Invalid delay value. Try 'help'.\n", cmd);
					errors_nb ++;
					break;
				}
				opdata->delay = delay;
			}

			else if(strcmp(param, "-hwres")==0) {
				const char* param_res = Command_PopParam(cmd_data);
				if(param_res == NULL) {
					printf("Error 'op' command '%s' : Missing parameters for option '%s'. Try 'help'.\n", cmd, param);
					errors_nb ++;
					break;
				}
				ptype_list* resources = augh_read_hwlimits(Implem->synth_target->techno, param_res);
				if(resources==NULL) {
					printf("Error 'op' command '%s' : Invalid resource format. Try 'help'.\n", cmd);
					errors_nb ++;
					break;
				}
				opdata->resources = resources;
			}

			else {
				printf("Error 'op' : Unknown parameter '%s' for command '%s'. Try 'help'.\n", param, cmd);
				errors_nb ++;
				break;
			}

		} while(1);  // Parse parameters

		if(opdata->func_name == NULL) {
			printf("Error 'op' command '%s' : Missing function name. Try 'help'.\n", cmd);
			errors_nb ++;
		}
		if(opdata->comp_name == NULL) {
			printf("Error 'op' command '%s' : Missing function name. Try 'help'.\n", cmd);
			errors_nb ++;
		}

		if(opdata->is_comb == false) {
			printf("Error 'op' command '%s' : Only combinational components are supported for now. Try 'help'.\n", cmd);
			errors_nb ++;
		}

		if(avl_pi_count(&opdata->tree_outputs) != 1) {
			printf("Error 'op' command '%s' : Must provide exactly 1 output port. Try 'help'.\n", cmd);
			errors_nb ++;
		}
		if(avl_pi_count(&opdata->tree_inputs) == 0) {
			printf("Error 'op' command '%s' : Must provide at least one input port. Try 'help'.\n", cmd);
			errors_nb ++;
		}

		if(errors_nb > 0) {
			// FIXME Mem leak, clear opdata
			return -1;
		}

		// Fix port order
		opdata->list_in_portnames = reverse(opdata->list_in_portnames);

		// Declare the Netlist component model
		netlist_comp_model_t* comp_model = Netlist_CompModel_New();
		comp_model->name = opdata->comp_name;
		comp_model->data = opdata;
		opdata->comp_model = comp_model;
		if(is_shared == true) comp_model->flags |= NETLIST_COMPMOD_FLAG_CANSHARE;
		// Set netlist-specific callbacks
		comp_model->func_eval_size        = Op_Comp_GenericComb_EvalSize;
		comp_model->func_propag_delays    = Op_Comp_GenericComb_PropagDelay;
		comp_model->func_comp_new         = Op_Comp_GenericComb_New;
		comp_model->func_comp_hwres_avail = Op_Comp_GenericComb_AvailRes;
		// FIXME Check output of this
		Netlist_CompModel_Declare(comp_model);

		// Create a new HVEX model node
		hvex_model_t* hvex_model = hvex_model_new();
		hvex_model->name = opdata->func_name;  // FIXME this is arbitrary, also check it
		hvex_model->data = opdata;
		opdata->hvex_model = hvex_model;
		// Set miscellaneous parameters and callbacks
		hvex_model->func_croprel = NULL;
		hvex_model->func_simp = NULL;
		hvex_model->func_fprint = hvex_fprint_opbefore_cb;
		hvex_model->func_check = NULL;  // FIXME
		// Set the generic mapping callback
		hvex_model->cb_map                 = Op_Hvex_GenericComb_Map;
		hvex_model->cb_needed_res_hvex     = Op_Hvex_GenericComb_NeededRes_Vex;
		hvex_model->cb_needed_res_vexalloc = Op_Hvex_GenericComb_NeededRes_VexAlloc;

		// Declare the built-in function
		builtinfunc_t* builtinfunc = Hier_BuiltinFunc_New();
		builtinfunc->func_name = opdata->func_name;
		builtinfunc->func_vex = Op_Parser_GenericComb_Builtin;
		builtinfunc->data = opdata;
		// FIXME Check output of this
		Hier_BuiltinFunc_Add(builtinfunc);

	}  // Command extern-comp

	else {
		printf("Error 'op' : Unknown command '%s'.\n", cmd);
		return -1;
	}

	return 0;
}

