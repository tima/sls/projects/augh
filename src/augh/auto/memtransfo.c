
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#include "auto.h"
#include "map.h"



//======================================================================
// Replacement of arrays by registers.
//======================================================================

// If tree_address!=NULL, the occurrences are stored in the tree
//   Key = address value, data = a bitype_list*
//   Data list usage:
//     TYPE = address value
//     DATA_FROM = a chain_list* of hvex_t*
//     (DATA_TO will be used later for the replacement register)
int ArrayOccur_LittAddr(implem_t* Implem, char* name, avl_ip_tree_t* tree_address) {
	bool all_literal = true;

	// Get the array uses
	chain_list* list_uses = Hier_GetArrayOccur(Implem->H, name);
	//printf("DEBUG %s:%d : Found %u occurrences of array '%s'.\n", __FILE__, __LINE__, ChainList_Count(list_uses), name);

	// Scan all occurrences
	foreach(list_uses, scanOccur) {
		hvex_t* Expr = scanOccur->DATA;
		// Get the index VEX
		hvex_t* exprindex = NULL;
		if(Expr->model==HVEX_INDEX) {
			exprindex = Expr->operands;
		}
		else if(Expr->model==HVEX_ASG_ADDR || Expr->model==HVEX_ASG_ADDR_EN) {
			exprindex = hvex_asg_get_addr(Expr);
		}
		else {
			abort();
		}
		assert(exprindex!=NULL);

		// Ensure it is a literal address
		if(exprindex->model!=HVEX_LITERAL) { all_literal = false; break; }

		// FIXME sometimes it is signed ?
		hvex_set_unsigned(exprindex);

		// Add to the tree
		if(tree_address!=NULL) {
			int val = hvex_lit_evalint(exprindex);
			// Add the occurrence to the list
			bitype_list* address_elt = NULL;
			avl_ip_find_data(tree_address, val, (void**)&address_elt);
			if(address_elt==NULL) {
				address_elt = addbitype(NULL, val, NULL, NULL);
				avl_ip_add_overwrite(tree_address, val, address_elt);
			}
			address_elt->DATA_FROM = addchain(address_elt->DATA_FROM, scanOccur->DATA);
		}
	}  // Scan the occurrences

	// Clean
	freechain(list_uses);

	// If the addresses were not all literal...
	if(all_literal==false) {
		if(tree_address!=NULL) {
			avl_ip_foreach(tree_address, scan) {
				bitype_list* elt = scan->data;
				freechain(elt->DATA_FROM);
				freebitype(elt);
			}
			avl_ip_reset(tree_address);
		}
		return __LINE__;
	}

	return 0;
}


/*
The arrays that are always accessed with constant address are replaced
by variables (one variable per array cell).
The ROMs are inlined.
*/

int ReplaceMem_comp(implem_t* Implem, netlist_comp_t* comp) {
	assert(comp->model==NETLIST_COMP_MEMORY);
	netlist_memory_t* mem_data = comp->data;

	bool is_rom;

	if(mem_data->ports_wa_nb>0) {
		is_rom = false;
		// There should be no initialization
		if(mem_data->init_vex!=NULL) {
			printf("ERROR %s:%d : The memory '%s' can't be replaced because it has an initialization.\n", __FILE__, __LINE__, comp->name);
			return __LINE__;
		}
	}
	else {
		is_rom = true;
		// There should be an initialization
		if(mem_data->init_vex==NULL) {
			printf("ERROR %s:%d : The ROM '%s' can't be replaced because it has no initialization.\n", __FILE__, __LINE__, comp->name);
			return __LINE__;
		}
	}

	// Key is the address value, data is a bitype_list*
	// List usage : TYPE = address value, DATA_FROM = Occurrences of that address, DATA_TO = the replacement register.
	avl_ip_tree_t tree_address;
	avl_ip_init(&tree_address);

	int z = ArrayOccur_LittAddr(Implem, comp->name, &tree_address);
	if(z!=0) {
		return z;
	}

	printf("INFO : Replacing the array '%s', accessed in %u addresses\n", comp->name, avl_ip_count(&tree_address));

	// Get the replacements.

	char buffer[1000];  // For the register names

	if(is_rom==false) {
		avl_ip_foreach(&tree_address, scan) {
			bitype_list* elt = scan->data;
			sprintf(buffer, "%s_reg%lu", comp->name, elt->TYPE);
			char* reg_name = namealloc(buffer);
			elt->DATA_TO = reg_name;
			// Avoid conflict with other component names
			reg_name = Map_Netlist_MakeInstanceName_TryThis(Implem, reg_name);
			// Create the component
			netlist_comp_t* compReg = Netlist_Comp_Reg_New(reg_name, mem_data->data_width);
			Netlist_Comp_SetChild(Implem->netlist.top, compReg);
		}  // Addresses
	}

	// Replace all occurrences. Scan the list again.
	// Take care of the size of the data : we can't do simple replacement, we must adapt to the original VEX width.

	// Type = address value, Data = VEX of the replacement register.
	avl_ip_foreach(&tree_address, scanAddr) {
		bitype_list* eltAddr = scanAddr->data;
		foreach((chain_list*)eltAddr->DATA_FROM, scanOccur) {
			hvex_t* Expr = scanOccur->DATA;  // FIXME Not useful to use double pointer

			// Build the new expression
			hvex_t* newvex;
			if(is_rom==true) {
				newvex = hvex_dup(mem_data->init_vex[eltAddr->TYPE]);
				hvex_cropabs(newvex, Expr->left, Expr->right);
			}
			else {
				newvex = hvex_newvec(eltAddr->DATA_TO, Expr->left, Expr->right);
			}
			hvex_sign_copy(newvex, Expr);

			// Replace the old VEX and set the WIDTH accordingly
			if(Expr->model==HVEX_INDEX) {
				hvex_replace_free(Expr, newvex);
			}
			else if(Expr->model==HVEX_ASG_ADDR) {
				// Remove the address operand
				hvex_t* VexAddr = Expr->operands->next->next;
				hvex_unlinkop(VexAddr);
				hvex_free(VexAddr);
				// Change VEX model
				Expr->model = HVEX_ASG;
				// Replace the destination
				hvex_replace_free(Expr->operands, newvex);
			}
			else if(Expr->model==HVEX_ASG_ADDR_EN) {
				// Remove the address operand
				hvex_t* VexAddr = Expr->operands->next->next;
				hvex_unlinkop(VexAddr);
				hvex_free(VexAddr);
				// Change VEX model
				Expr->model = HVEX_ASG_EN;
				// Replace the destination
				hvex_replace_free(Expr->operands, newvex);
			}
			else {
				abort();
			}
		}  // Occurrences
	}  // Addresses

	// Free the lists
	avl_ip_foreach(&tree_address, scan) {
		bitype_list* elt = scan->data;
		freechain(elt->DATA_FROM);
		freebitype(elt);
	}
	avl_ip_reset(&tree_address);

	// Remove the memory component
	Netlist_Comp_Free(comp);

	return 0;
}

int ReplaceMem_name(implem_t* Implem, char* name) {
	if(name==NULL) return __LINE__;

	// Get the component instance
	netlist_comp_t* comp = Map_Netlist_GetComponent(Implem, name);
	if(comp==NULL) return __LINE__;
	if(comp->model!=NETLIST_COMP_MEMORY) return __LINE__;

	return ReplaceMem_comp(Implem, comp);
}

int ReplaceMem_ByName_verbose(implem_t* Implem, char* name) {
	if(name==NULL) return __LINE__;

	netlist_comp_t* comp = Map_Netlist_GetComp_verbose(Implem, name, NETLIST_COMP_MEMORY);
	if(comp==NULL) return __LINE__;

	int z = ReplaceMem_comp(Implem, comp);
	if(z!=0) {
		printf("Error: Failed to replace the memory '%s' (code %i).\n", name, z);
	}

	return z;
}



//======================================================================
// Insertion of a Read buffer
//======================================================================

// Important: The search for occurrences in State or in Vex must NOT go recursive in address of found occurrences
//   Otherwise there will be double replacement and/or order won't be respected
static chain_list* scanvex_searchindex_addlist(chain_list* list, hvex_t* Expr, const char* name) {
	if(Expr==NULL) return list;

	// Consider the current node
	if(Expr->model==HVEX_INDEX) {
		if(hvex_index_getname(Expr)==name) list = addchain(list, Expr);
		return list;
	}

	// Scan operands
	hvex_foreach(Expr->operands, VexOp) list = scanvex_searchindex_addlist(list, VexOp, name);

	return list;
}

typedef struct readbuf_data_t {
	implem_t*       Implem;
	netlist_comp_t* comp;
	char*           reg_name;      // The name of the actual read buffer
	chain_list*     tmpregs_list;  // A list of namealloc'd names
	unsigned        tmpregs_nb;    // The number of tmp registers currently in the list
	unsigned        tmpreg_idx;    // The current temp reg idx (reset at each State)
} readbuf_data_t;

// A function declaration, for recursion
void Mem_InsertReadBuf_ListOccur(readbuf_data_t* readbuf_data, hier_node_t* nodeState, chain_list* read_occur);

// Replace occurrences of the same address expression
// Create previous States to assign the new read buffer, use other temp regs if needed
// The vex_addr is freed/used inside this function
void Mem_InsertReadBuf_OneAddr(readbuf_data_t* readbuf_data, hier_node_t* nodeState, hvex_t* vex_addr, chain_list* list_occur, bool add_tmpreg) {
	char* comp_name = readbuf_data->comp->name;
	netlist_memory_t* mem_data = readbuf_data->comp->data;

	// Create one previous State to assign the read buffer
	hier_build_bbstact_t bbstact;
	Hier_Build_StAct(&bbstact);
	Hier_Nodes_SetLines(bbstact.state, nodeState->source_lines, false, false);
	Hier_Lists_AddNode(readbuf_data->Implem->H, bbstact.state);
	Hier_InsertLevelBeforeNode(nodeState, bbstact.state);

	// Create the assignment of the read buffer
	bbstact.action->expr = hvex_asg_make(
		hvex_newvec(readbuf_data->reg_name, mem_data->data_width-1, 0),
		NULL,
		hvex_newindex(comp_name, mem_data->data_width-1, 0, vex_addr),
		NULL
	);
	// Add protection flags
	bbstact.action->flags |= HIER_ACT_NOPROPAG_FROM | HIER_ACT_NOPROPAG_TO;

	// In case the address expression also contains reads from the memory,
	//   launch insertion of read buffer on the newly created State
	chain_list* other_read_occur = scanvex_searchindex_addlist(NULL, vex_addr, comp_name);
	if(other_read_occur!=NULL) {
		Mem_InsertReadBuf_ListOccur(readbuf_data, bbstact.state, other_read_occur);
	}

	// The reg name to replace occurrences with
	char* replace_name = readbuf_data->reg_name;

	if(add_tmpreg==true) {
		char* tmpreg_name = NULL;

		if(readbuf_data->tmpreg_idx >= readbuf_data->tmpregs_nb) {
			// Create a new temp register
			tmpreg_name = Map_Netlist_MakeInstanceName_Prefix(readbuf_data->Implem, readbuf_data->reg_name);
			dbgprintf("Creating temp reg '%s' for read buffer on memory '%s'\n", tmpreg_name, comp_name);
			netlist_comp_t* compTmpReg = Netlist_Comp_Reg_New(tmpreg_name, mem_data->data_width);
			Netlist_Comp_SetChild(readbuf_data->Implem->netlist.top, compTmpReg);
			// Add the new name to the list. Add it at the end in order to always get them in the same order.
			readbuf_data->tmpregs_list = append(
				readbuf_data->tmpregs_list, addchain(NULL, tmpreg_name)
			);
			readbuf_data->tmpregs_nb ++;
		}
		else {
			// Get one temp register from the list
			tmpreg_name = ChainList_GetIdx(readbuf_data->tmpregs_list, readbuf_data->tmpreg_idx);
		}
		readbuf_data->tmpreg_idx++;

		// Create another previous State to assign the temp register from the read buffer
		hier_build_bbstact_t bbstact_tmpreg;
		Hier_Build_StAct(&bbstact_tmpreg);
		Hier_Nodes_SetLines(bbstact_tmpreg.state, nodeState->source_lines, false, false);
		Hier_Lists_AddNode(readbuf_data->Implem->H, bbstact_tmpreg.state);
		Hier_InsertLevelBeforeNode(nodeState, bbstact_tmpreg.state);

		// Create the assignment of the read buffer
		bbstact_tmpreg.action->expr = hvex_asg_make(
			hvex_newvec(tmpreg_name, mem_data->data_width-1, 0),
			NULL,
			hvex_newvec(readbuf_data->reg_name, mem_data->data_width-1, 0),
			NULL
		);

		// The INDEX occurrences will be replaced by this name
		replace_name = tmpreg_name;
	}

	// Replace all current occurrences by the read buffer or the temp register
	foreach(list_occur, scanOccur) {
		hvex_t* vex = scanOccur->DATA;
		hvex_free_ops(vex);
		vex->model = HVEX_VECTOR;
		hvex_vec_setname(vex, replace_name);
	}
}

// The list of occurrences is freed inside this function
void Mem_InsertReadBuf_ListOccur(readbuf_data_t* readbuf_data, hier_node_t* nodeState, chain_list* read_occur) {
	if(read_occur==NULL) return;

	// Group these occurrences per address expression
	while(read_occur!=NULL) {

		// Get the address expression of the first occurrence
		hvex_t* first_vex = read_occur->DATA;
		hvex_t* vex_addr = first_vex->operands;

		// Get all other occurrences with the same address expression
		chain_list* cur_occur = addchain(NULL, first_vex);
		chain_list* read_occur_next = NULL;
		foreach(read_occur->NEXT, scanOccur) {
			hvex_t* vex = scanOccur->DATA;
			if(hvex_cmp(vex->operands, vex_addr)!=0) read_occur_next = addchain(read_occur_next, vex);
			else cur_occur = addchain(cur_occur, vex);
		}
		freechain(read_occur);
		read_occur = read_occur_next;

		// Only for the last address expression, we can use the read buffer directly
		bool add_tmpreg = false;
		if(read_occur!=NULL) add_tmpreg = true;

		// Perform insertion of Reg assignment Actions
		Mem_InsertReadBuf_OneAddr(readbuf_data, nodeState, hvex_dup(vex_addr), cur_occur, add_tmpreg);

		// Clean
		freechain(cur_occur);

	}  // Replace all occurrences

}

// Insert Read buffers to large memories to make Read synchronous
// This reveals implementation into hard RAM blocks of the FPGA
int Mem_InsertReadBuf(implem_t* Implem, netlist_comp_t* comp) {
	if(Implem->H==NULL) return 0;

	netlist_memory_t* mem_data = comp->data;

	// Some common safety checks
	if(mem_data->ports_cam_nb != 0) return __LINE__;
	if(mem_data->ports_wa_nb > 1) return __LINE__;
	if(mem_data->direct_en==true) return __LINE__;

	// Create the name of the register
	char buf[1024];
	sprintf(buf, "%s_buf", comp->name);
	char* reg_name = namealloc(buf);
	reg_name = Map_Netlist_MakeInstanceName_TryThis(Implem, reg_name);

	// Create the register
	netlist_comp_t* compReg = Netlist_Comp_Reg_New(reg_name, mem_data->data_width);
	Netlist_Comp_SetChild(Implem->netlist.top, compReg);

	readbuf_data_t readbuf_data;
	memset(&readbuf_data, 0, sizeof(readbuf_data));
	readbuf_data.Implem   = Implem;
	readbuf_data.comp     = comp;
	readbuf_data.reg_name = reg_name;

	// Scan all States
	// Work on a copy of the list of States, because new States are added
	chain_list* list_states = NULL;
	avl_p_foreach(&Implem->H->NODES[HIERARCHY_STATE], scanState) list_states = addchain(list_states, scanState->data);

	// Scan all States of the Implem, perform replacement inside
	foreach(list_states, scanStates) {
		hier_node_t* nodeState = scanStates->DATA;
		hier_state_t* state_data = &nodeState->NODE.STATE;

		// Get all occurrences of read operations to the memory
		chain_list* read_occur = NULL;
		foreach(state_data->actions, scanAction) {
			hier_action_t* action = scanAction->DATA;
			read_occur = scanvex_searchindex_addlist(read_occur, action->expr, comp->name);
		}
		if(read_occur==NULL) continue;

		// Clear data
		readbuf_data.tmpreg_idx = 0;

		// Launch replacement
		Mem_InsertReadBuf_ListOccur(&readbuf_data, nodeState, read_occur);
	}

	// Clean
	freechain(list_states);
	freechain(readbuf_data.tmpregs_list);

	return 0;
}



//======================================================================
// Check whether a mem comp has a read buffer
//======================================================================

typedef struct chkreadbuf_data_t {
	implem_t* Implem;
	netlist_comp_t* compMem;
	// The register, if found
	netlist_comp_t* compReg;
	// A tree to store all Actions the read register be written
	avl_p_tree_t tree_reg_occur;
} chkreadbuf_data_t;

// Check one state: must be max one Read, etc
// Return 0 if OK, otherwise return the line number that triggered error
static int Mem_GetReadBuf_HierOnly_OneState(chkreadbuf_data_t* data, hier_node_t* nodeState) {
	hier_state_t* state_data = &nodeState->NODE.STATE;
	netlist_memory_t* mem_data = data->compMem->data;

	// Ensure there is at most one Action that reads the mem comp
	hier_action_t* state_action = NULL;

	// Scan the Read occurrences from this State
	foreach(state_data->actions, scanAction) {
		hier_action_t* action = scanAction->DATA;

		// Get occurrences from this Action
		chain_list* read_occur = scanvex_searchindex_addlist(NULL, action->expr, data->compMem->name);
		if(read_occur==NULL) continue;
		if(read_occur->NEXT != NULL) { freechain(read_occur); return __LINE__; }
		hvex_t* vex_read_occur = read_occur->DATA;
		freechain(read_occur);

		// Check the assignment
		if(action->expr->model != HVEX_ASG && action->expr->model != HVEX_ASG_EN) return __LINE__;
		hvex_t* vex_expr = hvex_asg_get_expr(action->expr);
		if(vex_read_occur != vex_expr) return __LINE__;
		if(vex_expr->right != 0 || vex_expr->left != mem_data->data_width-1) return __LINE__;

		// Check the destination symbol
		hvex_t* vex_dest = hvex_asg_get_dest(action->expr);
		if(vex_dest->right != 0) return __LINE__;
		char* regname = hvex_vec_getname(vex_dest);
		if(data->compReg != NULL) {
			if(regname != data->compReg->name) return __LINE__;
		}
		else {
			data->compReg = Netlist_Comp_GetChild(data->Implem->netlist.top, regname);
			if(data->compReg==NULL) return __LINE__;
			if(data->compReg->model != NETLIST_COMP_REGISTER) return __LINE__;
		}

		// Check Action flags and uniqueness
		if( (action->flags & HIER_ACT_NOPROPAG_TO) == 0 ) return __LINE__;
		if(state_action != NULL) return __LINE__;
		state_action = action;
		avl_p_add_overwrite(&data->tree_reg_occur, action, action);

	}  // Scan Actions

	return 0;
}

netlist_comp_t* Mem_GetReadBuf_HierOnly(implem_t* Implem, netlist_comp_t* comp) {
	if(Implem->H==NULL) return NULL;

	assert(comp->model == NETLIST_COMP_MEMORY);
	netlist_memory_t* mem_data = comp->data;

	// Some common checks
	if(mem_data->ports_cam_nb != 0) return NULL;
	if(mem_data->ports_wa_nb > 1) return NULL;
	if(mem_data->ports_ra_nb > 1) return NULL;
	if(mem_data->direct_en==true) return NULL;

	// Instantiate the data structure to ease function calls
	chkreadbuf_data_t data;
	data.Implem = Implem;
	data.compMem = comp;
	data.compReg = NULL;
	avl_p_init(&data.tree_reg_occur);

	unsigned errline = 0;

	// Scan all States of the Implem, get all read occurrences
	avl_p_foreach(&Implem->H->NODES[HIERARCHY_STATE], scanState) {
		hier_node_t* nodeState = scanState->data;
		errline = Mem_GetReadBuf_HierOnly_OneState(&data, nodeState);
		if(errline!=0) break;
	}  // Scan all States

	if(data.compReg==NULL || errline!=0) goto EXITPOINT;

	// Check that the reg is written only in the listed Actions
	chain_list* list_act = Hier_GetAct_SymIsDest(Implem->H, data.compReg->name);
	foreach(list_act, scanAction) {
		hier_action_t* action = scanAction->DATA;
		if(avl_p_isthere(&data.tree_reg_occur, action)==false) { errline = __LINE__; break; }
	}
	freechain(list_act);

	EXITPOINT:

	// Clean
	avl_p_reset(&data.tree_reg_occur);

	if(errline!=0) return NULL;
	return data.compReg;
}


