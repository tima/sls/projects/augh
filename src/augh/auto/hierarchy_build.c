
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <math.h>

#include "auto.h"
#include "hierarchy.h"
#include "hierarchy_symuse.h"
#include "addops.h"
#include "memtransfo.h"
#include "map.h"
#include "implem_models.h"
#include "linelist.h"
#include "../hvex/hvex_misc.h"
#include "command.h"

// For the definitions of interface and buffer components
#include "../netlist/netlist.h"
#include "../netlist/netlist_comps.h"
#include "../netlist/netlist_access.h"

#include "hierarchy_build.h"



// A little config
static bool handle_annotations = true;

// Mostly for debug
static bool annot_verbose = false;
static bool datacom_verbose = false;

// All builtin functions.
// Key = function name, data = builtinfunc_t*
static avl_pp_tree_t HBUILD_CB_TREEALL;

#if 0  // These lists contain pointers to some callbacks in the above tree
static chain_list* HBUILD_CB_INTERFACES = NULL;
static chain_list* HBUILD_CB_BUFFERS = NULL;
static chain_list* HBUILD_CB_TRANSFERS = NULL;
#endif



//===================================================
// Creation of hierarchy nodes
//===================================================

// Create a pool of type : hier_node_t

#define POOL_prefix      pool_hier_node
#define POOL_data_t      hier_node_t
#define POOL_ALLOC_SIZE  1000

#define POOL_INSTANCE             POOL_HIER_NODE
#define POOL_INSTANCE_ATTRIBUTES  static

#include "../pool.h"

// Allocation wrappers

static hier_node_t* Hier_Node_New() {
	hier_node_t* node = pool_hier_node_pop(&POOL_HIER_NODE);
	memset(node, 0, sizeof(*node));
	avl_pp_init(&node->symuse);
	return node;
}

hier_node_t* Hier_Node_NewType(hier_type_t type) {
	hier_node_t* node = Hier_Node_New();
	node->TYPE = type;
	// Here, if necessary, initialization of internal data
	switch(type) {
		case HIERARCHY_PROCESS: {
			hier_proc_t* proc_data = &node->NODE.PROCESS;
			proc_data->user.only_declared = false;
			proc_data->user.is_toplevel   = false;
			proc_data->user.no_autoinline = false;
			break;
		}
		case HIERARCHY_LOOP: {
			hier_loop_t* loop_data = &node->NODE.LOOP;
			loop_data->testinfo = Hier_TestInfo_New();
			loop_data->user.iter_nb_given = false;
			loop_data->user.iter_nb_power2_given = false;
			loop_data->user.parallel_given = false;
			break;
		}
		case HIERARCHY_CASE: {
			hier_case_t* case_data = &node->NODE.CASE;
			avl_p_init(&case_data->values);
			case_data->user.never_taken = false;
			case_data->user.wired_wanted = false;
			case_data->user.branch_prob_given = false;
			break;
		}
		default: break;
	}
	return node;
}

void hier_node_del(hier_node_t* elt) {
	pool_hier_node_push(&POOL_HIER_NODE, elt);
}


// Create a pool of type : hier_action_t

#define POOL_prefix      pool_hier_action
#define POOL_data_t      hier_action_t
#define POOL_ALLOC_SIZE  1000

#define POOL_INSTANCE             POOL_HIER_ACTION
#define POOL_INSTANCE_ATTRIBUTES  static

#include "../pool.h"

// Allocation wrappers

hier_action_t* Hier_Action_New(void) {
	hier_action_t* action = pool_hier_action_pop(&POOL_HIER_ACTION);
	memset(action, 0, sizeof(*action));
	avl_pi_init(&action->deps_timed);
	return action;
}

void Hier_Action_Free(hier_action_t* action) {
	pool_hier_action_push(&POOL_HIER_ACTION, action);
}
void Hier_Action_FreeFull(hier_action_t* action) {
	if(action->node!=NULL) {
		hier_state_t* trans_data = &action->node->NODE.STATE;
		trans_data->actions = ChainList_Remove(trans_data->actions, action);
	}
	if(action->expr!=NULL) hvex_free(action->expr);
	avl_pi_reset(&action->deps_timed);
	freechain(action->acts_inline);
	LineList_Free(action->source_lines);
	pool_hier_action_push(&POOL_HIER_ACTION, action);
}


// Create a pool of type : hier_testinfo_t

#define POOL_prefix      pool_hier_testinfo
#define POOL_data_t      hier_testinfo_t
#define POOL_ALLOC_SIZE  100

#define POOL_INSTANCE             POOL_HIER_TESTINFO
#define POOL_INSTANCE_ATTRIBUTES  static

#include "../pool.h"

// Allocation wrappers

hier_testinfo_t* Hier_TestInfo_New() {
	hier_testinfo_t* testinfo = pool_hier_testinfo_pop(&POOL_HIER_TESTINFO);
	memset(testinfo, 0, sizeof(*testinfo));
	Hier_TestInfo_ResetResults(testinfo);
	return testinfo;
}

void Hier_TestInfo_Free(hier_testinfo_t* caseval) {
	pool_hier_testinfo_push(&POOL_HIER_TESTINFO, caseval);
}
void Hier_TestInfo_FreeFull(hier_testinfo_t* testinfo) {
	Hier_TestInfo_Clear(testinfo);
	pool_hier_testinfo_push(&POOL_HIER_TESTINFO, testinfo);
}


// Create a pool of type : builtinfunc_t

#define POOL_prefix      pool_hier_builtinfunc
#define POOL_data_t      builtinfunc_t
#define POOL_ALLOC_SIZE  100

#define POOL_INSTANCE             POOL_HIER_BUILTINFUNC
#define POOL_INSTANCE_ATTRIBUTES  static

#include "../pool.h"

// Allocation wrappers

builtinfunc_t* Hier_BuiltinFunc_New() {
	builtinfunc_t* builtin = pool_hier_builtinfunc_pop(&POOL_HIER_BUILTINFUNC);
	memset(builtin, 0, sizeof(*builtin));
	builtin->call_is_act = false;
	builtin->alone_in_state = false;
	return builtin;
}
void Hier_BuiltinFunc_Free(builtinfunc_t* builtin) {
	pool_hier_builtinfunc_push(&POOL_HIER_BUILTINFUNC, builtin);
}



hier_t* Hier_New(void) {
	hier_t* H = calloc(1, sizeof(hier_t));
	// The lists of nodes
	avl_p_init(&H->NODES_ALL);
	// Nodes per type
	for(unsigned i=0; i<HIERARCHY_TYPES_NB; i++) avl_p_init(&H->NODES[i]);
	return H;
}
void Hier_Free(hier_t* H) {
	Hier_DumpAllHier(H);
	// These trees should already been emptied in Hier_DumpAllHier(), but paranoia.
	avl_p_reset(&H->NODES_ALL);
	for(unsigned i=0; i<HIERARCHY_TYPES_NB; i++) avl_p_reset(&H->NODES[i]);
	free(H);
}

void Hier_Action_Unlink(hier_action_t* action) {
	if(action->node==NULL) return;
	hier_state_t* trans_data = &action->node->NODE.STATE;
	trans_data->actions = ChainList_Remove(trans_data->actions, action);
	action->node = NULL;
}
void Hier_Action_Link(hier_node_t* node, hier_action_t* action) {
	assert(node->TYPE==HIERARCHY_STATE);
	hier_state_t* trans_data = &node->NODE.STATE;
	trans_data->actions = addchain(trans_data->actions, action);
	action->node = node;
}

void Hier_TestInfo_ResetResults(hier_testinfo_t* testinfo) {
	testinfo->is_simple = false;
	testinfo->sig_asg = NULL;
}
void Hier_TestInfo_Clear(hier_testinfo_t* testinfo) {
	if(testinfo->cond!=NULL) hvex_free(testinfo->cond);
}
void Hier_DumpNodeData(hier_node_t* node) {
	// Dump the data specific to the node types
	switch(node->TYPE) {
		case HIERARCHY_PROCESS: {
			hier_proc_t* proc_data = &node->NODE.PROCESS;
			freechain(proc_data->calls);
			freechain(proc_data->returns);
			foreach(proc_data->build.list_args, scan) hvex_free(scan->DATA);
			freechain(proc_data->build.list_args);
			if(proc_data->build.vex_ret!=NULL) hvex_free(proc_data->build.vex_ret);
			break;
		}
		case HIERARCHY_STATE : {
			hier_state_t* node_trans = &node->NODE.STATE;
			while(node_trans->actions!=NULL) Hier_Action_FreeFull(node_trans->actions->DATA);
			break;
		}
		case HIERARCHY_BB : {
			hier_bb_t* bb_data = &node->NODE.BB;
			Hier_NodeBB_ClearSched(node);
			freechain(bb_data->prev_bbs);
			freechain(bb_data->next_bbs);
			break;
		}
		case HIERARCHY_LOOP : {
			hier_loop_t* node_loop = &node->NODE.LOOP;
			Hier_TestInfo_FreeFull(node_loop->testinfo);
			TestDoNull(node_loop->iter.vex_iterator, hvex_free);
			TestDoNull(node_loop->iter.vex_end, hvex_free);
			break;
		}
		case HIERARCHY_SWITCH : {
			hier_switch_t* node_switch = &node->NODE.SWITCH;
			if(node_switch->info.switch_test!=NULL) hvex_free(node_switch->info.switch_test);
			break;
		}
		case HIERARCHY_CASE : {
			hier_case_t* case_data = &node->NODE.CASE;
			avl_p_foreach(&case_data->values, scan) Hier_TestInfo_FreeFull(scan->data);
			avl_p_reset(&case_data->values);
			break;
		}
		case HIERARCHY_LABEL : {
			hier_label_t* label_data = &node->NODE.LABEL;
			freechain(label_data->jumps);
			break;
		}
		default : break;
	}
	// Dump the common data
	freechain(node->CHILDREN);
	LineList_Free(node->source_lines);

	// The symbol usage
	Hier_SymUse_ClearNode(node);
}
void Hier_DumpAllHier(hier_t* H) {
	// Free node data
	avl_p_foreach(&H->NODES_ALL, scan) {
		Hier_DumpNodeData(scan->data);
		hier_node_del(scan->data);
	}
	// Empty the trees of nodes
	avl_p_reset(&H->NODES_ALL);
	for(unsigned i=0; i<HIERARCHY_TYPES_NB; i++) avl_p_reset(&H->NODES[i]);
	H->PROCESS = NULL;
}

void Hier_FreeNode(hier_node_t* node) {
	// Recursively free the lower levels
	foreach(node->CHILDREN, scan) Hier_FreeNodeLevel(scan->DATA);
	// Free the common data
	Hier_DumpNodeData(node);
	// And put this element back in the list of unused.
	hier_node_del(node);
}
void Hier_FreeNodeLevel(hier_node_t* node) {
	while(node!=NULL) {
		hier_node_t* nextnode = node->NEXT;
		Hier_FreeNode(node);
		node = nextnode;
	}
}



//=====================================================================
// Utility functions to build usual Hier constructs
//=====================================================================

hier_node_t* Hier_Build_BBFromState(hier_node_t* nodeState) {
	hier_node_t* nodeBB = Hier_Node_NewType(HIERARCHY_BB);

	nodeBB->NODE.BB.body = nodeState;
	nodeState->PARENT = nodeBB;
	nodeBB->CHILDREN = addchain(nodeBB->CHILDREN, nodeState);

	return nodeBB;
}

hier_node_t* Hier_Build_StAct(hier_build_bbstact_t* bbstact) {

	// Create the State
	hier_node_t* nodeState = Hier_Node_NewType(HIERARCHY_STATE);
	//hier_state_t* state_data = &nodeBB->NODE.STATE;
	if(bbstact!=NULL) bbstact->state = nodeState;

	// Create the Action
	hier_action_t* action = Hier_Action_New();
	if(bbstact!=NULL) bbstact->action = action;
	// Link to the State
	Hier_Action_Link(nodeState, action);

	return nodeState;
}

hier_node_t* Hier_Build_BBState(hier_build_bbstact_t* bbstact) {

	// Create the BB
	hier_node_t* nodeBB = Hier_Node_NewType(HIERARCHY_BB);
	hier_bb_t* bb_data = &nodeBB->NODE.BB;
	if(bbstact!=NULL) bbstact->bb = nodeBB;

	// Create the State
	hier_node_t* nodeState = Hier_Node_NewType(HIERARCHY_STATE);
	if(bbstact!=NULL) bbstact->state = nodeState;

	// Link to the BB
	bb_data->body = nodeState;
	nodeState->PARENT = nodeBB;
	nodeBB->CHILDREN = addchain(nodeBB->CHILDREN, nodeState);

	return nodeBB;
}

hier_node_t* Hier_Build_BBStAct(hier_build_bbstact_t* bbstact) {

	// Create the BB
	hier_node_t* nodeBB = Hier_Node_NewType(HIERARCHY_BB);
	hier_bb_t* bb_data = &nodeBB->NODE.BB;
	if(bbstact!=NULL) bbstact->bb = nodeBB;

	// Create the State
	hier_node_t* nodeState = Hier_Build_StAct(bbstact);
	// Link to the BB
	bb_data->body = nodeState;
	nodeState->PARENT = nodeBB;
	nodeBB->CHILDREN = addchain(nodeBB->CHILDREN, nodeState);

	return nodeBB;
}

hier_node_t* Hier_Build_For_raw(
	hier_build_for_t* forloop,
	char* name_reg, char* name_sig,
	unsigned beg, unsigned iter_nb, unsigned step,
	unsigned width_reg
) {

	// Create the loop node
	hier_node_t* nodeLoop = Hier_Node_NewType(HIERARCHY_LOOP);
	hier_loop_t* loop_data = &nodeLoop->NODE.LOOP;
	forloop->loop = nodeLoop;

	// Create the loop condition
	loop_data->testinfo->cond = hvex_newvec_bit(name_sig);

	// Compute the end value
	unsigned end = beg + iter_nb * step;

	// Create the Init BB
	Hier_Build_BBStAct(&forloop->init);
	forloop->init.action->expr = hvex_asg_make(
		hvex_newvec(name_reg, width_reg-1, 0), NULL,
		hvex_newlit_int(0, width_reg, false), NULL
	);
	// Link with the Loop
	forloop->init.bb->NEXT = nodeLoop;
	nodeLoop->PREV = forloop->init.bb;

	// Create the Test BB
	Hier_Build_BBStAct(&forloop->test);
	forloop->test.action->expr = hvex_asg_make(
		hvex_newvec(name_sig, 0, 0), NULL,
		hvex_newmodel_op2(HVEX_LT, 1,
			hvex_newvec(name_reg, width_reg-1, 0),
			hvex_newlit_int(end, width_reg, false)
		), NULL
	);
	// Link with the Loop
	forloop->test.bb->PARENT = nodeLoop;
	nodeLoop->NODE.LOOP.body_after = forloop->test.bb;
	nodeLoop->CHILDREN = addchain(nodeLoop->CHILDREN, forloop->test.bb);

	// Create the Inc BB
	Hier_Build_BBStAct(&forloop->inc);
	forloop->inc.action->expr = hvex_asg_make(
		hvex_newvec(name_reg, width_reg-1, 0), NULL,
		hvex_newmodel_op2(HVEX_ADD, width_reg,
			hvex_newvec(name_reg, width_reg-1, 0),
			hvex_newlit_int(step, width_reg, false)
		),
		NULL
	);
	// Link with the Loop
	forloop->inc.bb->PARENT = nodeLoop;
	nodeLoop->NODE.LOOP.body_before = forloop->inc.bb;
	nodeLoop->CHILDREN = addchain(nodeLoop->CHILDREN, forloop->inc.bb);

	return forloop->init.bb;
}
// Only for loops that iterate at least one time
// There is no body_before, and only one State in body_after
// The iterator only needs to count from beg to beg + step * (iter_nb-1)
hier_node_t* Hier_Build_For_onecycle(
	hier_build_for_t* forloop,
	char* name_reg, char* name_sig,
	unsigned beg, unsigned iter_nb, unsigned step,
	unsigned width_reg
) {
	assert(iter_nb > 0);

	// Create the loop node
	hier_node_t* nodeLoop = Hier_Node_NewType(HIERARCHY_LOOP);
	hier_loop_t* loop_data = &nodeLoop->NODE.LOOP;
	forloop->loop = nodeLoop;

	// Create the loop condition
	loop_data->testinfo->cond = hvex_newvec(name_sig, 0, 0);

	// Compute the end value. Get out of the loop when the iterator has this value.
	unsigned end = beg + step * (iter_nb-1);

	// Create the Init BB
	Hier_Build_BBStAct(&forloop->init);
	forloop->init.action->expr = hvex_asg_make(
		hvex_newvec(name_reg, width_reg-1, 0), NULL,
		hvex_newlit_int(0, width_reg, false), NULL
	);
	// Link with the Loop
	forloop->init.bb->NEXT = nodeLoop;
	nodeLoop->PREV = forloop->init.bb;

	// Create the Inc/Test BB
	Hier_Build_BBStAct(&forloop->test);

	// Create the Test Action
	forloop->test.action->expr = hvex_asg_make(
		hvex_newvec(name_sig, 0, 0), NULL,
		hvex_newmodel_op2(HVEX_NE, 1,
			hvex_newvec(name_reg, width_reg-1, 0),
			hvex_newlit_int(end, width_reg, false)
		), NULL
	);

	// Create the Inc Action
	hier_action_t* action_inc = Hier_Action_New();
	action_inc->expr = hvex_asg_make(
		hvex_newvec(name_reg, width_reg-1, 0), NULL,
		hvex_newmodel_op2(HVEX_ADD, width_reg,
			hvex_newvec(name_reg, width_reg-1, 0),
			hvex_newlit_int(step, width_reg, false)
		),
		NULL
	);
	Hier_Action_Link(forloop->test.state, action_inc);
	// Set BB and State fields to the Test BB
	forloop->inc.bb = forloop->test.bb;
	forloop->inc.state = forloop->test.state;

	// Link the BB in the Loop
	forloop->test.bb->PARENT = nodeLoop;
	nodeLoop->NODE.LOOP.body_after = forloop->test.bb;
	nodeLoop->CHILDREN = addchain(nodeLoop->CHILDREN, forloop->test.bb);

	return forloop->init.bb;
}



//=====================================================================
// Getting user annotations
//=====================================================================

/* This defines how the parameters are obtained from the Hier graph.
The C parser eliminates the #pramas so we can't use that.

We use special integer variables (no floating-point because of pre-processing).
They are used by assigning a value to them.

'augh_branch_prob'    : The probability of taking this branch, in %
'augh_branch_prob_m'  : The probability of taking this branch, unit 0.1%
'augh_branch_prob_u'  : The probability of taking this branch, unit 0.0001%
'augh_iter_nb'        : The average number of iterations, integer number
'augh_iter_nb_m'      : The average number of iterations, unit 0.001
'augh_iter_parallel'  : The loop iterations are independant from each other.
                        They can be parallelized, or the nodes in the body can be moved when unrolling.

Usage of these variables :
- Branch probability : inside the branch, at the beginning.
  When some cases have no given probability, they are considered to have the same branch probability.
- Iteration number : inside the body, at the beginning.

*/

typedef struct hier_annotation_t {
	bool     wired_found;
	bool     branch_prob_found;
	double   branch_prob;
	bool     iter_nb_found;
	double   iter_nb;
	bool     iter_nb_power2_found;
	unsigned iter_nb_power2;
	bool     parallel_found;
	bool     toplevel_found;
} hier_annotation_t;

// Initialization function
static void Hier_Annotations_Init(hier_annotation_t* annotations) {
	annotations->wired_found = false;
	annotations->branch_prob_found = false;
	annotations->iter_nb_found = false;
	annotations->iter_nb_power2_found = false;
	annotations->parallel_found = false;
	annotations->toplevel_found = false;
}

// Backward search the Hier Actions and detect writes to special variables
static void Hier_GetAnnotations_State(hier_node_t* node, hier_annotation_t* annotations) {
	assert(node->TYPE==HIERARCHY_STATE);
	hier_state_t* trans_data = &node->NODE.STATE;

	// Scan the Actions
	foreach(trans_data->actions, scanAction) {
		hier_action_t* action = scanAction->DATA;
		hvex_t* VexDest = hvex_asg_get_dest(action->expr);
		if(VexDest==NULL) continue;
		char* name = hvex_vec_getname(VexDest);

		void PrintAnnotation() {
			if(annot_verbose==true) {
				printf("INFO: Detected user annotation '%s' at ", name);
				LineList_Print_be(action->source_lines, NULL, "\n");
			}
		}

		if(name==namealloc("augh_branch_wired")) {
			PrintAnnotation();
			annotations->wired_found = true;
		}

		else if(name==namealloc("augh_parallel")) {
			PrintAnnotation();
			annotations->parallel_found = true;
		}

		else if(name==namealloc("augh_branch_prob")) {
			PrintAnnotation();
			hvex_t* Expr = hvex_asg_get_expr(action->expr);
			// Check that the Expr is a literal
			if(Expr->model!=HVEX_LITERAL) {
				printf("Error: The AUGH-specific '%s' symbol must be assigned a literal value at ", name);
				LineList_Print_be(action->source_lines, NULL, ".\n");
				continue;
			}
			int value = hvex_lit_evalint(Expr);
			annotations->branch_prob = value / 100.;
			if(annotations->branch_prob<0 || annotations->branch_prob>1) {
				printf("Error: Invalid branch probability given at ");
				LineList_Print_be(action->source_lines, NULL, ".\n");
				continue;
			}
			annotations->branch_prob_found = true;
		}

		else if(name==namealloc("augh_branch_prob_m")) {
			PrintAnnotation();
			hvex_t* Expr = hvex_asg_get_expr(action->expr);
			// Check that the Expr is a literal
			if(Expr->model!=HVEX_LITERAL) {
				printf("Error: The AUGH-specific '%s' symbol must be assigned a literal value at ", name);
				LineList_Print_be(action->source_lines, NULL, ".\n");
				continue;
			}
			int value = hvex_lit_evalint(Expr);
			annotations->branch_prob = value / 1000.;
			if(annotations->branch_prob<0 || annotations->branch_prob>1) {
				printf("Error: Invalid branch probability given at ");
				LineList_Print_be(action->source_lines, NULL, ".\n");
				continue;
			}
			annotations->branch_prob_found = true;
		}

		else if(name==namealloc("augh_branch_prob_u")) {
			PrintAnnotation();
			hvex_t* Expr = hvex_asg_get_expr(action->expr);
			// Check that the Expr is a literal
			if(Expr->model!=HVEX_LITERAL) {
				printf("Error: The AUGH-specific '%s' symbol must be assigned a literal value at ", name);
				LineList_Print_be(action->source_lines, NULL, ".\n");
				continue;
			}
			int value = hvex_lit_evalint(Expr);
			annotations->branch_prob = value / 1000000.;
			if(annotations->branch_prob<0 || annotations->branch_prob>1) {
				printf("Error: Invalid branch probability given at ");
				LineList_Print_be(action->source_lines, NULL, ".\n");
				continue;
			}
			annotations->branch_prob_found = true;
		}

		else if(name==namealloc("augh_iter_nb")) {
			PrintAnnotation();
			hvex_t* Expr = hvex_asg_get_expr(action->expr);
			// Check that the Expr is a literal
			if(Expr->model!=HVEX_LITERAL) {
				printf("Error: The AUGH-specific '%s' symbol must be assigned a literal value at ", name);
				LineList_Print_be(action->source_lines, NULL, ".\n");
				continue;
			}
			int value = hvex_lit_evalint(Expr);
			annotations->iter_nb = value;
			if(annotations->iter_nb<0) {
				printf("Error: Invalid iteration number given at ");
				LineList_Print_be(action->source_lines, NULL, ".\n");
				continue;
			}
			annotations->iter_nb_found = true;
		}

		else if(name==namealloc("augh_iter_nb_m")) {
			PrintAnnotation();
			hvex_t* Expr = hvex_asg_get_expr(action->expr);
			// Check that the Expr is a literal
			if(Expr->model!=HVEX_LITERAL) {
				printf("Error: The AUGH-specific '%s' symbol must be assigned a literal value at ", name);
				LineList_Print_be(action->source_lines, NULL, ".\n");
				continue;
			}
			int value = hvex_lit_evalint(Expr);
			annotations->iter_nb = value / 1000.;
			if(annotations->iter_nb<0) {
				printf("Error: Invalid iteration number given at ");
				LineList_Print_be(action->source_lines, NULL, ".\n");
				continue;
			}
			annotations->iter_nb_found = true;
		}

		else if(name==namealloc("augh_iter_nb_power2")) {
			PrintAnnotation();
			hvex_t* Expr = hvex_asg_get_expr(action->expr);
			// Check that the Expr is a literal
			if(Expr->model!=HVEX_LITERAL) {
				printf("Error: The AUGH-specific '%s' symbol must be assigned a literal value at ", name);
				LineList_Print_be(action->source_lines, NULL, ".\n");
				continue;
			}
			int value = hvex_lit_evalint(Expr);
			if(value<2 || uint32_ispower2(value)==false) {
				printf("Error: Invalid power of 2 given at ");
				LineList_Print_be(action->source_lines, NULL, ".\n");
				continue;
			}
			annotations->iter_nb_power2 = value;
			annotations->iter_nb_power2_found = true;
		}

		else if(name==namealloc("augh_toplevel")) {
			PrintAnnotation();
			annotations->toplevel_found = true;
		}

	}
}

// Forward search the Hier Actions and detect writes to special variables
static void Hier_GetAnnotations_FromHere(hier_node_t* node, hier_annotation_t* annotations) {
	for( ; node!=NULL; node=node->NEXT) {
		if(node->TYPE==HIERARCHY_BB) {
			hier_bb_t* bb_data = &node->NODE.BB;
			Hier_GetAnnotations_FromHere(bb_data->body, annotations);
		}
		else if(node->TYPE==HIERARCHY_STATE) {
			Hier_GetAnnotations_State(node, annotations);
		}
		else if(node->TYPE==HIERARCHY_LABEL) {
			// Skip Labels: many are created by the parser as target of jumps for statements continue/break
		}
		else {
			break;
		}
	}  // Loop on the nodes of the level.
}

static void Hier_GetAnnotations(implem_t* Implem) {
	hier_annotation_t annotations;

	if(handle_annotations==true) {

		unsigned wired_nb = 0;
		unsigned branch_prob_nb = 0;
		unsigned iter_nb_nb = 0;
		unsigned iter_nb_power2_nb = 0;
		unsigned parallel_nb = 0;
		unsigned toplevel_nb = 0;

		// The loops
		avl_p_foreach(&Implem->H->NODES[HIERARCHY_LOOP], scanNode) {
			hier_node_t* node = scanNode->data;
			hier_loop_t* node_loop = &node->NODE.LOOP;
			if(node_loop->body_after!=NULL) {
				Hier_Annotations_Init(&annotations);
				Hier_GetAnnotations_FromHere(node_loop->body_after, &annotations);
				if(annotations.parallel_found==true) {
					node_loop->user.parallel_given = true;
					parallel_nb++;
				}
				if(annotations.iter_nb_found==true) {
					node_loop->user.iter_nb_given = true;
					node_loop->user.iter_nb = annotations.iter_nb;
					iter_nb_nb++;
				}
				if(annotations.iter_nb_power2_found==true) {
					node_loop->user.iter_nb_power2_given = true;
					node_loop->user.iter_nb_power2 = annotations.iter_nb_power2;
					iter_nb_power2_nb++;
				}
			}
			if(node_loop->body_before!=NULL) {
				Hier_Annotations_Init(&annotations);
				Hier_GetAnnotations_FromHere(node_loop->body_before, &annotations);
				if(annotations.parallel_found==true) {
					node_loop->user.parallel_given = true;
					parallel_nb++;
				}
				if(annotations.iter_nb_found==true) {
					node_loop->user.iter_nb_given = true;
					node_loop->user.iter_nb = annotations.iter_nb;
					iter_nb_nb++;
				}
				if(annotations.iter_nb_power2_found==true) {
					node_loop->user.iter_nb_power2_given = true;
					node_loop->user.iter_nb_power2 = annotations.iter_nb_power2;
					iter_nb_power2_nb++;
				}
			}
		}

		// The SWITCH
		avl_p_foreach(&Implem->H->NODES[HIERARCHY_SWITCH], scanNode) {
			hier_node_t* node = scanNode->data;
			//hier_switch_t* node_switch = &node->NODE.SWITCH;
			double sum_prob = 0;

			// Get the branch probability of the children
			foreach(node->CHILDREN, scanChild) {
				hier_node_t* child = scanChild->DATA;
				hier_case_t* node_case = &child->NODE.CASE;
				Hier_Annotations_Init(&annotations);
				Hier_GetAnnotations_FromHere(node_case->body, &annotations);
				if(annotations.wired_found==true) {
					node_case->user.wired_wanted = true;
					wired_nb++;
				}
				if(annotations.branch_prob_found==true) {
					node_case->user.branch_prob_given = true;
					node_case->user.branch_prob = annotations.branch_prob;
					sum_prob += annotations.branch_prob;
					branch_prob_nb++;
				}
			}

			// Check that the sum of probabilities does not exceed 1
			if(sum_prob>1.01) {
				printf("WARNING : the sum of branch probabilities exceeds 1.\n");
				LineList_Print_be(node->source_lines, "  Source lines: ", "\n");
				foreach(node->CHILDREN, scanChild) {
					hier_node_t* child = scanChild->DATA;
					hier_case_t* node_case = &child->NODE.CASE;
					if(node_case->user.branch_prob_given==true) {
						node_case->user.branch_prob /= sum_prob;
					}
				}
			}

		}

		// The processes
		avl_p_foreach(&Implem->H->NODES[HIERARCHY_PROCESS], scanNode) {
			hier_node_t* node = scanNode->data;
			hier_proc_t* proc_data = &node->NODE.PROCESS;

			Hier_Annotations_Init(&annotations);
			Hier_GetAnnotations_FromHere(node->NEXT, &annotations);

			if(annotations.toplevel_found==true) {
				proc_data->user.is_toplevel = true;
				toplevel_nb++;
			}
		}

		if(annot_verbose==true) {
			unsigned total_annot_nb = wired_nb + branch_prob_nb + iter_nb_nb + iter_nb_power2_nb + parallel_nb + toplevel_nb;
			printf("Info: %u annotations were found:", total_annot_nb);
			if(wired_nb>0)          printf(" wired=%u", wired_nb);
			if(branch_prob_nb>0)    printf(" branch_prob=%u", branch_prob_nb);
			if(iter_nb_nb>0)        printf(" iter_nb=%u", iter_nb_nb);
			if(iter_nb_power2_nb>0) printf(" iter_nb_power2=%u", iter_nb_power2_nb);
			if(parallel_nb>0)       printf(" parallel=%u", parallel_nb);
			if(toplevel_nb>0)       printf(" toplevel=%u", toplevel_nb);
			printf("\n");
		}

	}
	else {
		printf("Warning: Handling of unnotations is disabled.\n");
	}

	// Remove all occurrences to the user annotations
	// Actions as well as variable declarations

	// Build a list of identifiers, simpler to parse
	chain_list* list_names = NULL;
	list_names = addchain(list_names, namealloc("augh_branch_wired"));
	list_names = addchain(list_names, namealloc("augh_parallel"));
	list_names = addchain(list_names, namealloc("augh_branch_prob"));
	list_names = addchain(list_names, namealloc("augh_branch_prob_m"));
	list_names = addchain(list_names, namealloc("augh_branch_prob_u"));
	list_names = addchain(list_names, namealloc("augh_iter_nb"));
	list_names = addchain(list_names, namealloc("augh_iter_nb_m"));
	list_names = addchain(list_names, namealloc("augh_iter_nb_power2"));
	list_names = addchain(list_names, namealloc("augh_toplevel"));

	// Remove the declarations (components)
	foreach(list_names, scanName) {
		char* name = scanName->DATA;
		netlist_comp_t* comp = Map_Netlist_GetComponent(Implem, name);
		if(comp!=NULL) Netlist_Comp_Free(comp);
	}

	// Remove the Actions
	chain_list* list_annot_del = NULL;
	avl_p_foreach(&Implem->H->NODES[HIERARCHY_STATE], scanState) {
		hier_node_t* node = scanState->data;
		hier_state_t* state_data = &node->NODE.STATE;
		chain_list* list_act = NULL;
		// List the actions to remove
		foreach(state_data->actions, scanAction) {
			hier_action_t* action = scanAction->DATA;
			char* name = hvex_asg_get_destname(action->expr);
			if(ChainList_IsDataHere(list_names, name)==true) {
				list_act = addchain(list_act, action);
				list_annot_del = ChainList_Add_NoDup(list_annot_del, name);
			}
		}
		// Remove the actions
		foreach(list_act, scanAction) {
			hier_action_t* action = scanAction->DATA;
			Hier_Action_FreeFull(action);
		}
		// Clean
		freechain(list_act);
	}

	if(annot_verbose==true) {
		printf("Info: Removed usage of annotation symbols:");
		foreach(list_annot_del, scanName) {
			if(scanName!=list_annot_del) printf(",");
			printf(" '%s'", (char*)scanName->DATA);
		}
		printf("\n");
	}
	freechain(list_annot_del);

	// Clean
	freechain(list_names);

}



//=====================================================================
// Get a symbol name from a port, creating intermediate Signal if needed
//=====================================================================

char* Hier_Build_GetSymbolFomPort(implem_t* Implem, netlist_port_t* port) {
	// For top-level ports, the name can be used directly
	if(port->component==Implem->netlist.top) return port->name;

	// Here it is a port of a component inside the top-level
	// We must use an intermediate Signal

	if(port->direction==NETLIST_PORT_DIR_IN) {
		// Reuse an existing source Signal
		if(port->source!=NULL) {
			// Check everything
			if(port->source->NEXT!=NULL) abort();
			netlist_val_cat_t* valcat = port->source->DATA;
			if(valcat->source==NULL) abort();
			bool b = Netlist_ValCat_IsSourceFullPort(valcat, valcat->source);
			if(b==false) abort();
			netlist_comp_t* compSig = valcat->source->component;
			if(compSig->model!=NETLIST_COMP_SIGNAL) abort();
			// Signal found!
			return compSig->name;
		}
		// Create a new Signal component
		char* name = Map_Netlist_MakeInstanceName(Implem, NETLIST_COMP_SIGNAL);
		netlist_comp_t* compSig = Netlist_Comp_Sig_New(name, port->width);
		Netlist_Comp_SetChild(Implem->netlist.top, compSig);
		Implem_SymFlag_Add(Implem, name, SYM_PROTECT_NODEL);
		// Link with our port
		netlist_signal_t* sig_data = compSig->data;
		port->source = Netlist_ValCat_MakeList_FullPort(sig_data->port_out);
		Netlist_PortTargets_SetSource(port);
		return name;
	}

	else if(port->direction==NETLIST_PORT_DIR_OUT) {
		// Reuse an existing target Signal
		if(port->target_ports!=NULL) {
			avl_p_foreach(port->target_ports, scan_port) {
				netlist_port_t* tgport = scan_port->data;
				bool b = Netlist_Port_IsSourceFullPort(tgport, port);
				if(b==false) continue;
				// Check if the target is a Signal component
				netlist_comp_t* compSig = tgport->component;
				if(compSig->model!=NETLIST_COMP_SIGNAL) continue;
				// Signal found!
				return compSig->name;
			}
		}
		// Create a new Signal component
		char* name = Map_Netlist_MakeInstanceName(Implem, NETLIST_COMP_SIGNAL);
		netlist_comp_t* compSig = Netlist_Comp_Sig_New(name, port->width);
		Netlist_Comp_SetChild(Implem->netlist.top, compSig);
		Implem_SymFlag_Add(Implem, name, SYM_PROTECT_NODEL);
		// Link with our port
		netlist_signal_t* sig_data = compSig->data;
		sig_data->port_in->source = Netlist_ValCat_MakeList_FullPort(port);
		Netlist_PortTargets_SetSource(sig_data->port_in);
		return name;
	}

	// Here it's an error
	abort();
	return NULL;
}



//=====================================================================
// Applying transformation of built-in functions
//=====================================================================

builtinfunc_t* Hier_BuiltinFunc_Get(char* name) {
	builtinfunc_t* builtin = NULL;
	avl_pp_find_data(&HBUILD_CB_TREEALL, name, (void**)&builtin);
	return builtin;
}
bool Hier_BuiltinFunc_Add(builtinfunc_t* builtin) {
	avl_pp_node_t* pp_node = NULL;
	bool b = avl_pp_add(&HBUILD_CB_TREEALL, builtin->func_name, &pp_node);
	if(b==false) return false;  // It has already been declared
	pp_node->data = builtin;
	return true;
}
void Hier_BuiltinFunc_Remove(builtinfunc_t* builtin) {
	avl_pp_rem_key(&HBUILD_CB_TREEALL, builtin->func_name);
}

// The results are stored in the tree
// Key = function name, data = chain_list* of vexexpr*
// Note: the field FATHER of ALL vex expr MUST be properly set

// Return the number of errors
int Hier_BuiltinFunc_ScanVex(implem_t* Implem, hvex_t* Expr, avl_pp_tree_t* tree) {
	unsigned errors_nb = 0;

	//dbgprintf("Scanning Vex: "); VexAsg_Print_nl(Expr);

	// Scan the children
	hvex_foreach(Expr->operands, VexOp) {
		errors_nb += Hier_BuiltinFunc_ScanVex(Implem, VexOp, tree);
	}

	// Process the current node
	if(Expr->model!=HVEX_FUNCTION) return errors_nb;

	char* name = hvex_func_getname(Expr);
	builtinfunc_t* builtin = Hier_BuiltinFunc_Get(name);
	if(builtin==NULL) {
		printf("Error: Top-level '%s': Detected call to function '%s', which is not a built-in function.\n", Implem->compmod.model_name, name);
		return errors_nb + 1;
	}

	if(builtin->call_is_act==true) {
		if(hvex_is_father_hvex(Expr)) {
			printf("Error: Top-level '%s': Detected call to function '%s' but not as a entire Action.\n", Implem->compmod.model_name, name);
			errors_nb ++;
		}
	}

	if(builtin->alone_in_state==true) {
		// Get the State node
		hier_action_t* action = Hier_Action_GetFromVex(Expr);
		hier_node_t* nodeState = action->node;
		hier_state_t* state_data = &nodeState->NODE.STATE;
		// Check the number of Actions
		unsigned nb_act = ChainList_Count(state_data->actions);
		if(nb_act>1) {
			printf("Error: Top-level '%s': Detected call to function '%s' in a state with more than one action.\n", Implem->compmod.model_name, name);
			errors_nb ++;
		}
	}

	if(errors_nb > 0) return errors_nb;

	if(datacom_verbose==true) {
		printf("Info: Top-level '%s': Detected call to builtin function '%s'\n", Implem->compmod.model_name,  name);
		printf("  The expression is: "); hvex_print_bn(Expr);
	}

	// Add a node in the result tree
	avl_pp_node_t* pp_node = NULL;
	bool b = avl_pp_add(tree, name, &pp_node);
	if(b==true) pp_node->data = NULL;
	pp_node->data = addchain(pp_node->data, Expr);

	return 0;
}

int Hier_BuiltinFunc_ScanImplem(implem_t* Implem, avl_pp_tree_t* tree) {
	unsigned errors_nb = 0;

	dbgprintf("Detecting builtin function calls...\n");

	avl_p_foreach(&Implem->H->NODES[HIERARCHY_STATE], scanNode) {
		hier_node_t* nodeState = scanNode->data;
		hier_state_t* state_data = &nodeState->NODE.STATE;
		foreach(state_data->actions, scanAction) {
			hier_action_t* action = scanAction->DATA;
			errors_nb += Hier_BuiltinFunc_ScanVex(Implem, action->expr, tree);
		}
	}

	#if 0  // Display, for debug
	dbgprintf("All builtin function calls found:\n");
	avl_pp_foreach(tree, scan) {
		printf("  Function '%s': %u times\n", (char*)scan->key, ChainList_Count(scan->data));
	}
	#endif

	return errors_nb;
}



//=====================================================================
// Creating wait loops
//=====================================================================

/* Description

Create a register for the counter
Create a signal for the test

Create the loop:

	reg = 1; sig_test = iter_nb==0;  // Same single state!
	while(sig_test==false) {
		<Insert the body here>
		reg ++; sig_test = reg==iter_nb;  // One single state!
	}

Also properly set the protection flags on the register, and loop
*/

static hier_node_t* Hier_BuildSleepLoopWithBody_vex(implem_t* Implem, hier_node_t* body, hvex_t* vex_iter_nb) {
	unsigned width = vex_iter_nb->width;

	// Create the register
	char* name_reg = Map_Netlist_MakeInstanceName(Implem, NETLIST_COMP_REGISTER);
	netlist_comp_t* compReg = Netlist_Comp_Reg_New(name_reg, width);
	Netlist_Comp_SetChild(Implem->netlist.top, compReg);

	// Create the test signal
	char* name_sig = Map_Netlist_MakeInstanceName(Implem, NETLIST_COMP_SIGNAL);
	netlist_comp_t* compSig = Netlist_Comp_Sig_New(name_sig, 1);
	Netlist_Comp_SetChild(Implem->netlist.top, compSig);

	hier_build_bbstact_t bbstact;

	// Create the Init Action
	hier_node_t* initBB = Hier_Build_BBStAct(&bbstact);
	// Create the Action: i = 1;
	bbstact.action->expr = hvex_asg_make(
		hvex_newvec(name_reg, width-1, 0),
		NULL,
		hvex_newlit_int(1, width, false),
		NULL
	);
	// Add the test Action at the same State
	hier_action_t* actTest_init = Hier_Action_New();
	Hier_Action_Link(bbstact.state, actTest_init);
	actTest_init->expr = hvex_asg_make(
		hvex_newvec_bit(name_sig),
		NULL,
		hvex_newmodel_op2(HVEX_EQ, 1, hvex_dup(vex_iter_nb), hvex_newlit_int(0, width, false)),
		NULL
	);

	// Create the Increment Action
	hier_node_t* incBB = Hier_Build_BBStAct(&bbstact);
	// Create the Action: i = i+1;
	bbstact.action->expr = hvex_asg_make(
		hvex_newvec(name_reg, width-1, 0),
		NULL,
		hvex_newmodel_op2(HVEX_ADD, width, hvex_newvec(name_reg, width-1, 0), hvex_newlit_int(1, width, false)),
		NULL
	);
	// Add the test Action at the same State
	hier_action_t* actTest_inc = Hier_Action_New();
	Hier_Action_Link(bbstact.state, actTest_inc);
	actTest_inc->expr = hvex_asg_make(
		hvex_newvec_bit(name_sig),
		NULL,
		hvex_newmodel_op2(HVEX_EQ, 1, hvex_newvec(name_reg, width-1, 0), hvex_dup(vex_iter_nb)),
		NULL
	);

	// Create the FOR loop
	hier_node_t* nodeLoop = Hier_Node_NewType(HIERARCHY_LOOP);
	hier_loop_t* loop_data = &nodeLoop->NODE.LOOP;
	loop_data->testinfo->cond = hvex_newnot(hvex_newvec_bit(name_sig));

	// Connect everything
	initBB->NEXT = nodeLoop;
	nodeLoop->PREV = initBB;

	loop_data->body_before = incBB;
	incBB->PARENT = nodeLoop;
	nodeLoop->CHILDREN = addchain(nodeLoop->CHILDREN, incBB);

	// Set protection flags and annotations
	Implem_SymFlag_Add(Implem, compReg->name, SYM_PROTECT_NODEL);
	Implem_SymFlag_Add(Implem, compReg->name, SYM_PROTECT_NOCIRC);

	loop_data->core.full_unroll_impossible = 1;
	loop_data->core.part_unroll_impossible = 1;

	if(vex_iter_nb->model==HVEX_LITERAL) {
		int cycles_nb = hvex_lit_evalint(vex_iter_nb);
		loop_data->user.iter_nb_given = true;
		loop_data->user.iter_nb = cycles_nb;
	}

	// Insert the body, if any
	if(body!=NULL) {
		Hier_InsertLevelBeforeNode(loop_data->body_before, body);
	}

	// Return the level!
	return initBB;
}
static hier_node_t* Hier_BuildSleepLoopWithBody_u(implem_t* Implem, hier_node_t* body, unsigned iter_nb) {
	unsigned width = uint_bitsnb(iter_nb);
	hvex_t* vex_iter_nb = hvex_newlit_int(iter_nb, width, false);
	hier_node_t* newLevel = Hier_BuildSleepLoopWithBody_vex(Implem, body, vex_iter_nb);
	hvex_free(vex_iter_nb);
	return newLevel;
}

static hier_node_t* Hier_BuildSleepLoop_cycles_vex(implem_t* Implem, hvex_t* vex_cycles_nb) {
	return Hier_BuildSleepLoopWithBody_vex(Implem, NULL, vex_cycles_nb);
}
static hier_node_t* Hier_BuildSleepLoop_cycles_u(implem_t* Implem, unsigned cycles_nb) {
	return Hier_BuildSleepLoopWithBody_u(Implem, NULL, cycles_nb);
}

static hier_node_t* Hier_BuildSleepLoop_nsec_vex(implem_t* Implem, hvex_t* vex_nsec_nb) {
	if(Implem->synth_target->clk_period_ns==0) {
		printf("ERROR: Can't build a nanosecond-waiting loop without a target frequency set.\n");
		abort();
	}

	// Get the nb of cycles to get 1us
	unsigned nb_cy_per_ns = ceil(Implem->synth_target->clk_period_ns);
	if(nb_cy_per_ns<5) {
		printf("ERROR: Can't build a nanosecond-waiting loop: nb of cycles per ns too low (%u).\n", nb_cy_per_ns);
		abort();
	}
	// Compensate for the counter init/increment time
	nb_cy_per_ns -= 2;

	// Create the child cysleep() loop
	hier_node_t* cySleep = Hier_BuildSleepLoop_cycles_u(Implem, nb_cy_per_ns);

	// Create the main waiting loop
	hier_node_t* mainLevel = Hier_BuildSleepLoopWithBody_vex(Implem, cySleep, vex_nsec_nb);

	// Return the level!
	return mainLevel;
}

static hier_node_t* Hier_BuildSleepLoop_usec_vex(implem_t* Implem, hvex_t* vex_usec_nb) {
	if(Implem->synth_target->clk_period_ns==0) {
		printf("ERROR: Can't build a microsecond-waiting loop without a target frequency set.\n");
		abort();
	}

	// Get the nb of cycles to get 1us
	unsigned nb_cy_per_us = ceil(1000.0 / Implem->synth_target->clk_period_ns);
	if(nb_cy_per_us<5) {
		printf("ERROR: Can't build a microsecond-waiting loop: nb of cycles per us too low (%u).\n", nb_cy_per_us);
		abort();
	}
	// Compensate for the counter init/increment time
	nb_cy_per_us -= 2;

	// Create the child cysleep() loop
	hier_node_t* cySleep = Hier_BuildSleepLoop_cycles_u(Implem, nb_cy_per_us);

	// Create the main waiting loop
	hier_node_t* mainLevel = Hier_BuildSleepLoopWithBody_vex(Implem, cySleep, vex_usec_nb);

	// Return the level!
	return mainLevel;
}

static hier_node_t* Hier_BuildSleepLoop_sec_vex(implem_t* Implem, hvex_t* vex_sec_nb) {
	if(Implem->synth_target->clk_period_ns==0) {
		printf("ERROR: Can't build a microsecond-waiting loop without a target frequency set.\n");
		abort();
	}

	// Generate three nested loops:
	//  number of seconds, then two loops that iter sqrt(period_nb)

	double periods_per_sec = 1e9 / Implem->synth_target->clk_period_ns;
	// Get the square root of the periods_per_sec to limit the counter width
	unsigned persec_sqrt = ceil(sqrt(periods_per_sec-2));

	// Generate the first nested loop
	hier_node_t* sleepLoop1 = Hier_BuildSleepLoop_cycles_u(Implem, persec_sqrt-2);
	// Then another loop that contains the first
	hier_node_t* sleepLoop2 = Hier_BuildSleepLoopWithBody_u(Implem, sleepLoop1, persec_sqrt-2);

	// Here we have a loop that lasts approx. periods_per_sec-2 cycles

	// Create the main waiting loop
	hier_node_t* mainLevel = Hier_BuildSleepLoopWithBody_vex(Implem, sleepLoop2, vex_sec_nb);

	// Return the level!
	return mainLevel;
}

static int Hier_BuiltinFunc_CycleSleep_vex(
	builtinfunc_t* builtin, build_symtops_data_t* data, implem_t* modImplem, netlist_comp_t* compInst, hvex_t* Expr
) {
	// Get the Action and the State node
	hier_action_t* action = Hier_Action_GetFromVex(Expr);
	hier_node_t* nodeState = action->node;

	// Get the expr for number of units
	hvex_t* vex_nb = Expr->operands;

	// If there are multiple top-levels, create stuff in the current modImplem
	implem_t* Implem = data->Implem;
	if(modImplem!=NULL) Implem = modImplem;

	// Build the replacement Level
	hier_node_t* newLevel = Hier_BuildSleepLoop_cycles_vex(Implem, vex_nb);

	// Change the source lines
	Hier_Nodes_SetLines(newLevel, action->source_lines, true, true);
	Hier_Level_SetExprFather(newLevel);

	// Replace the original Action and State
	Hier_ReplaceState_SplitBB(Implem->H, nodeState, newLevel);

	return 0;
}
static int Hier_BuiltinFunc_NanoSleep_vex(
	builtinfunc_t* builtin, build_symtops_data_t* data, implem_t* modImplem, netlist_comp_t* compInst, hvex_t* Expr
) {
	// Get the Action and the State node
	hier_action_t* action = Hier_Action_GetFromVex(Expr);
	hier_node_t* nodeState = action->node;

	// Get the expr for number of units
	hvex_t* vex_nb = Expr->operands;

	// If there are multiple top-levels, create stuff in the current modImplem
	implem_t* Implem = data->Implem;
	if(modImplem!=NULL) Implem = modImplem;

	// Build the replacement Level
	hier_node_t* newLevel = Hier_BuildSleepLoop_nsec_vex(Implem, vex_nb);

	// Change the source lines
	Hier_Nodes_SetLines(newLevel, action->source_lines, true, true);
	Hier_Level_SetExprFather(newLevel);

	// Replace the original Action and State
	Hier_ReplaceState_SplitBB(Implem->H, nodeState, newLevel);

	return 0;
}
static int Hier_BuiltinFunc_MicroSleep_vex(
	builtinfunc_t* builtin, build_symtops_data_t* data, implem_t* modImplem, netlist_comp_t* compInst, hvex_t* Expr
) {
	// Get the Action and the State node
	hier_action_t* action = Hier_Action_GetFromVex(Expr);
	hier_node_t* nodeState = action->node;

	// Get the expr for number of units
	hvex_t* vex_nb = Expr->operands;

	// If there are multiple top-levels, create stuff in the current modImplem
	implem_t* Implem = data->Implem;
	if(modImplem!=NULL) Implem = modImplem;

	// Build the replacement Level
	hier_node_t* newLevel = Hier_BuildSleepLoop_usec_vex(Implem, vex_nb);

	// Change the source lines
	Hier_Nodes_SetLines(newLevel, action->source_lines, true, true);
	Hier_Level_SetExprFather(newLevel);

	// Replace the original Action and State
	Hier_ReplaceState_SplitBB(Implem->H, nodeState, newLevel);

	return 0;
}
static int Hier_BuiltinFunc_Sleep_vex(
	builtinfunc_t* builtin, build_symtops_data_t* data, implem_t* modImplem, netlist_comp_t* compInst, hvex_t* Expr
) {
	// Get the Action and the State node
	hier_action_t* action = Hier_Action_GetFromVex(Expr);
	hier_node_t* nodeState = action->node;

	// Get the expr for number of units
	hvex_t* vex_nb = Expr->operands;

	// If there are multiple top-levels, create stuff in the current modImplem
	implem_t* Implem = data->Implem;
	if(modImplem!=NULL) Implem = modImplem;

	// Build the replacement Level
	hier_node_t* newLevel = Hier_BuildSleepLoop_sec_vex(Implem, vex_nb);

	// Change the source lines
	Hier_Nodes_SetLines(newLevel, action->source_lines, true, true);
	Hier_Level_SetExprFather(newLevel);

	// Replace the original Action and State
	Hier_ReplaceState_SplitBB(Implem->H, nodeState, newLevel);

	return 0;
}

static int Hier_ReplaceBuiltinFunc(build_symtops_data_t* data, implem_t* modImplem, netlist_comp_t* compInst) {
	int z;

	avl_pp_tree_t tree_vex;
	avl_pp_init(&tree_vex);

	int error_code = 0;

	implem_t* Implem = data->Implem;
	if(modImplem!=NULL) Implem = modImplem;

	// Get all calls of all builtin functions
	Hier_AllActions_SetExprFather(Implem->H);
	z = Hier_BuiltinFunc_ScanImplem(Implem, &tree_vex);
	if(z!=0) { error_code = __LINE__; goto END_POINT; }

	int process_builtin_type(builtinfunc_type_t type) {
		int errors_nb = 0;

		avl_pp_foreach(&tree_vex, scan) {
			char* name = scan->key;
			chain_list* list_vex = scan->data;
			if(list_vex==NULL) continue;  // Paranoia

			// Get the config for this builtin function
			builtinfunc_t* builtin = Hier_BuiltinFunc_Get(name);
			assert(builtin!=NULL);
			if(builtin->type!=type) continue;

			dbgprintf("Top-level '%s': Replacing %u calls to function '%s'...\n",
				Implem->compmod.model_name, ChainList_Count(list_vex), name
			);

			// Replace all occurrences
			foreach(list_vex, scanVex) {
				int z = builtin->func_vex(builtin, data, modImplem, compInst, scanVex->DATA);
				if(z!=0) {
					errprintf("Top-level '%s': Replacing a call to function '%s' failed (code %i)\n", Implem->compmod.model_name, name, z);
					errors_nb++;
				}
			}

		}  // Scan all VEX occurrences

		return errors_nb;
	}

	// This creates the top-level accesses FIFO, WIRE, etc
	z = process_builtin_type(HIER_BUILTIN_TRANSFER);
	if(z!=0) { error_code = __LINE__; goto END_POINT; }

	// This inserts the buffers in front of the top-level FIFO accesses
	process_builtin_type(HIER_BUILTIN_BUF);
	if(z!=0) { error_code = __LINE__; goto END_POINT; }

	// This creates the interface components
	process_builtin_type(HIER_BUILTIN_IF);
	if(z!=0) { error_code = __LINE__; goto END_POINT; }

	// All other built-in functions not related to data interfaces
	process_builtin_type(HIER_BUILTIN_NONE);

	// Remove the function from the Hier
	avl_pp_foreach(&HBUILD_CB_TREEALL, scanBI) {
		builtinfunc_t* builtin = scanBI->data;
		// Get the PROCESS that corresponds to the builtin function
		hier_node_t* nodeProc = NULL;
		hier_proc_t* proc_data = NULL;
		bool found = false;
		avl_p_foreach(&Implem->H->NODES[HIERARCHY_PROCESS], scanProc) {
			nodeProc = scanProc->data;
			proc_data = &nodeProc->NODE.PROCESS;
			if(proc_data->name_orig!=builtin->func_name) continue;
			found = true;
			break;
		}
		if(found==false) continue;
		// Checks
		if(nodeProc->NEXT!=NULL && nodeProc->NEXT->TYPE!=HIERARCHY_RETURN) {
			printf("WARNING: Removing builtin function '%s' even if it has a body.\n", builtin->func_name);
		}
		// Remove the PROCESS node
		// If there were some CALL nodes, remove them
		foreach(proc_data->calls, scanCall) {
			hier_node_t* nodeCall = scanCall->DATA;
			Hier_ReplaceNode_Free(Implem->H, nodeCall, NULL);
		}
		Hier_RemoveLevel_Free(Implem->H, nodeProc);
	}

	// For debug: check integrity of the orig Implem
	// Note: All new Implem have already been checked just before
	#ifndef NDEBUG
	Hier_Check_Integrity(Implem->H);
	Netlist_Debug_CheckIntegrity(Implem->netlist.top);
	#endif

	END_POINT:

	// Clean
	avl_pp_foreach(&tree_vex, scan) freechain(scan->data);
	avl_pp_reset(&tree_vex);

	return error_code;
}



//=====================================================================
// Replace data transfer operations
//=====================================================================

// Declaration
static int Hier_ConnectAccesses_Fifo_Prod2Read(implem_t* Implem, netlist_access_t* access1, netlist_access_t* access2);

// Return an access in the context of the current Implem, or modImplem if specified
netlist_access_t* Hier_Builtin_GetFifoAccess(build_symtops_data_t* data, implem_t* modImplem, netlist_comp_t* compInst, char* fifo_name, bool is_input) {
	implem_t* Implem = data->Implem;

	netlist_access_model_t* wanted_access_model = NULL;
	netlist_access_model_t* opposite_access_model = NULL;
	if(is_input==true) {
		wanted_access_model = NETLIST_ACCESS_FIFO_IN;
		opposite_access_model = NETLIST_ACCESS_FIFO_OUT;
	}
	else {
		wanted_access_model = NETLIST_ACCESS_FIFO_OUT;
		opposite_access_model = NETLIST_ACCESS_FIFO_IN;
	}

	// Handle when we are dealing with a separate top-level
	if(modImplem!=NULL) {

		// If there is already an apropriate access in the modImplem, get it
		netlist_access_t* access = Netlist_Comp_GetAccess(modImplem->netlist.top, fifo_name);
		if(access!=NULL) {
			if(access->model!=wanted_access_model) {
				printf("Error: The top-level access '%s' is model '%s', expected '%s'.\n",
					access->name, access->model->name, wanted_access_model->name
				);
				return NULL;
			}
			return access;
		}

		// Recursive call: Get an appropriate access within the orig Implem
		access = Hier_Builtin_GetFifoAccess(data, NULL, NULL, fifo_name, is_input);
		if(access==NULL) return NULL;
		netlist_access_fifo_t* fifo_data = access->data;

		netlist_access_t* modAccess = NULL;
		netlist_access_t* instAccess = NULL;

		// Create accesses in current modImplem + compInst
		if(is_input==true) {
			modAccess = Netlist_Comp_AddAccess_FifoIn(modImplem->netlist.top, fifo_name, fifo_data->width, false, &data->tree_avoidsym);
			instAccess = Netlist_Access_Dup(modAccess, compInst, NULL);
			// Connect accesses in the orig Implem
			int z = Hier_ConnectAccesses_Fifo_Prod2Read(Implem, access, instAccess);
			if(z!=0) return NULL;
		}
		else {
			modAccess = Netlist_Comp_AddAccess_FifoOut(modImplem->netlist.top, fifo_name, fifo_data->width, false, &data->tree_avoidsym);
			instAccess = Netlist_Access_Dup(modAccess, compInst, NULL);
			// Connect accesses in the orig Implem
			int z = Hier_ConnectAccesses_Fifo_Prod2Read(Implem, instAccess, access);
			if(z!=0) return NULL;
		}

		#ifndef NDEBUG
		Netlist_Debug_CheckIntegrity(Implem->netlist.top);
		#endif

		// Return the modImplem access
		return modAccess;
	}

	// From here, modImplem==NULL and we are only dealing with one Implem

	// Get a component with the required name
	netlist_comp_t* comp = Netlist_Comp_GetChild(Implem->netlist.top, fifo_name);

	// If the component is a Register, create a FIFO out of it (component or top-level access)
	if(comp!=NULL && comp->model==NETLIST_COMP_REGISTER) {

		// Delete the Register component
		netlist_register_t* reg_data = comp->data;
		unsigned reg_width = reg_data->width;
		Netlist_Comp_Free(comp);
		comp = NULL;

		// The symbol is extern: create a top-level FIFO access
		if(Implem_SymFlag_Chk(Implem, fifo_name, SYM_FLAG_NOEXTERN)==false) {
			// Ensure no top-level access with that name exists
			netlist_access_t* TopAccess = Netlist_Comp_GetAccess(Implem->netlist.top, fifo_name);
			if(TopAccess!=NULL) {
				printf("Error: Can't create top-level FIFO '%s' because an access with the same name already exists (model '%s').\n",
					fifo_name, TopAccess->model->name
				);
				return NULL;
			}
			// Create the top-level access
			if(is_input==true) {
				TopAccess = Netlist_Comp_AddAccess_FifoIn(Implem->netlist.top, fifo_name, reg_width, false, &data->tree_avoidsym);
			}
			else {
				TopAccess = Netlist_Comp_AddAccess_FifoOut(Implem->netlist.top, fifo_name, reg_width, false, &data->tree_avoidsym);
			}
			return TopAccess;
		}

		// The symbol is not extern: create a FIFO component (with no connection)
		comp = Netlist_Fifo_New(fifo_name, reg_width, 0);
		Netlist_Comp_SetChild(Implem->netlist.top, comp);
	}

	// If variable comp!=NULL, it means we have to get a FIFO access from that component
	if(comp!=NULL) {
		netlist_access_t* found_access = NULL;
		avl_pp_foreach(&comp->accesses, scanAccess) {
			netlist_access_t* access = scanAccess->data;
			if(access->model != opposite_access_model) continue;
			if(found_access!=NULL) {
				printf("ERROR: More than one FIFO with model '%s' were found in component '%s'.\n", opposite_access_model->name, comp->name);
				return NULL;
			}
			found_access = access;
		}
		if(found_access==NULL) {
			printf("ERROR: No FIFO with model '%s' was found in component '%s'.\n", opposite_access_model->name, comp->name);
		}
		return found_access;
	}

	// Here we have to get a top-level access. It should already exist.
	netlist_access_t* access = Netlist_Comp_GetAccess(Implem->netlist.top, fifo_name);
	if(access==NULL) {
		printf("Error: No component nor top-level access was found for FIFO '%s'.\n", fifo_name);
		return NULL;
	}
	// Check the access model
	if(access->model!=wanted_access_model) {
		printf("Error: The top-level access '%s' is model '%s', expected '%s'.\n",
			access->name, access->model->name, wanted_access_model->name
		);
		return NULL;
	}

	return access;
}




// Build a wait loop and add an Action to read/write from/to the specified VEX
static hier_node_t* Hier_BuildFifoWaitLoop(build_symtops_data_t* data, implem_t* modImplem, netlist_comp_t* compInst, char* fifo_name, bool is_input, bool is_signed, hvex_t* vex) {
	// Get or create the FIFO access
	netlist_access_t* access = Hier_Builtin_GetFifoAccess(data, modImplem, compInst, fifo_name, is_input);
	if(access==NULL) return NULL;
	netlist_access_fifo_t* fifo_data = access->data;

	// If there are multiple top-levels, create stuff in the current modImplem
	implem_t* Implem = data->Implem;
	if(modImplem!=NULL) Implem = modImplem;
	// Get symbol names to talk with the FIFO
	char* name_data = Hier_Build_GetSymbolFomPort(Implem, fifo_data->port_data);
	char* name_rdy = NULL;
	char* name_ack = NULL;
	if(access->component==Implem->netlist.top) {
		name_rdy = Hier_Build_GetSymbolFomPort(Implem, fifo_data->port_rdy);
		name_ack = Hier_Build_GetSymbolFomPort(Implem, fifo_data->port_ack);
	}
	else {
		name_ack = Hier_Build_GetSymbolFomPort(Implem, fifo_data->port_rdy);
		name_rdy = Hier_Build_GetSymbolFomPort(Implem, fifo_data->port_ack);
	}

	// Create the new assignment expression
	hvex_t* new_expr = NULL;

	if(is_input==true) {
		// Action: dest = fifo

		// Build the FIFO expr
		hvex_t* expr_fifo = hvex_newvec(name_data, fifo_data->width-1, 0);
		if(is_signed==true) hvex_set_signed(expr_fifo);
		// Crop or extend to the dest width
		hvex_resize(expr_fifo, vex->width);

		// Build the full assignment Expr
		if(vex->model==HVEX_VECTOR) {
			new_expr = hvex_asg_make(hvex_dup(vex), NULL, expr_fifo, NULL);
		}
		else if(vex->model==HVEX_INDEX) {
			new_expr = hvex_asg_make(
				hvex_newvec(hvex_index_getname(vex), vex->left, vex->right),
				hvex_dup(vex->operands),
				expr_fifo, NULL
			);
		}
		else {
			abort();
		}
	}
	else {
		// Action: fifo = src
		hvex_t* expr_fifo = hvex_newvec(name_data, fifo_data->width-1, 0);
		hvex_t* new_src = hvex_dup(vex);
		hvex_resize(new_src, fifo_data->width);
		new_expr = hvex_asg_make(expr_fifo, NULL, new_src, NULL);
	}

	// Create the LOOP node
	hier_node_t* nodeLoop = Hier_Node_NewType(HIERARCHY_LOOP);
	hier_loop_t* loop_data = &nodeLoop->NODE.LOOP;

	loop_data->testinfo->cond = hvex_newnot(hvex_newvec_bit(name_ack));

	// Create the embedded BB
	hier_build_bbstact_t bbstact;
	Hier_Build_BBStAct(&bbstact);
	// Link to the Loop
	loop_data->body_after = bbstact.bb;
	bbstact.bb->PARENT = nodeLoop;
	nodeLoop->CHILDREN = addchain(nodeLoop->CHILDREN, bbstact.bb);

	// Set the new VEX expression to the Action
	bbstact.action->expr = new_expr;

	// Add an Action for handshake
	hier_action_t* action_rdy = Hier_Action_New();
	Hier_Action_Link(bbstact.state, action_rdy);
	action_rdy->expr = hvex_asg_make(
		hvex_newvec(name_rdy, 0, 0), NULL,
		hvex_newlit_bit('1'), NULL
	);

	return nodeLoop;
}

static int Hier_BuiltinFunc_FifoRead_vex(builtinfunc_t* builtin, build_symtops_data_t* data, implem_t* modImplem, netlist_comp_t* compInst, hvex_t* Expr) {
	// Get the Action and the State node
	hier_action_t* action = Hier_Action_GetFromVex(Expr);
	hier_node_t* nodeState = action->node;

	// Get the FIFO name and destination VEX
	hvex_t* expr_fifo = action->expr->operands;
	hvex_t* expr_dest = action->expr->operands->next;

	char* fifo_name = hvex_vecidx_getname(expr_fifo);

	// Build the replacement Level
	hier_node_t* newLevel = Hier_BuildFifoWaitLoop(data, modImplem, compInst, fifo_name, true, hvex_is_signed(expr_fifo), expr_dest);
	if(newLevel==NULL) return __LINE__;

	// If there are multiple top-levels, create stuff in the current modImplem
	implem_t* Implem = data->Implem;
	if(modImplem!=NULL) Implem = modImplem;

	// Change the source lines
	Hier_Nodes_SetLines(newLevel, action->source_lines, true, true);
	Hier_Level_SetExprFather(newLevel);

	// Replace the original Action and State
	Hier_ReplaceState_SplitBB(Implem->H, nodeState, newLevel);

	return 0;
}
static int Hier_BuiltinFunc_FifoWrite_vex(builtinfunc_t* builtin, build_symtops_data_t* data, implem_t* modImplem, netlist_comp_t* compInst, hvex_t* Expr) {
	// Get the Action and the State node
	hier_action_t* action = Hier_Action_GetFromVex(Expr);
	hier_node_t* nodeState = action->node;

	// Get the FIFO name and source VEX
	hvex_t* expr_fifo = action->expr->operands;
	hvex_t* expr_dest = action->expr->operands->next;

	char* fifo_name = hvex_vecidx_getname(expr_fifo);

	// Build the replacement Level
	hier_node_t* newLevel = Hier_BuildFifoWaitLoop(data, modImplem, compInst, fifo_name, false, hvex_is_signed(expr_fifo), expr_dest);
	if(newLevel==NULL) return __LINE__;

	// If there are multiple top-levels, create stuff in the current modImplem
	implem_t* Implem = data->Implem;
	if(modImplem!=NULL) Implem = modImplem;

	// Change the source lines
	Hier_Nodes_SetLines(newLevel, action->source_lines, true, true);
	Hier_Level_SetExprFather(newLevel);

	// Replace the original Action and State
	Hier_ReplaceState_SplitBB(Implem->H, nodeState, newLevel);

	return 0;
}

// Build a wait loop add an Action to read/write from/to the specified VEX, and an iterator
// Replace this: augh_read_vector(fifo, array, nb);
//      by this: for(i=0; i<nb; i++) augh_read(fifo, array[i]);
static hier_node_t* Hier_BuildFifoVectorLoop(
	build_symtops_data_t* data, implem_t* modImplem, netlist_comp_t* compInst,
	char* fifo_name, bool is_input, bool is_signed, hvex_t* vex, hvex_t* vex_iter, unsigned nb
) {
	// Get or create the FIFO access
	netlist_access_t* access = Hier_Builtin_GetFifoAccess(data, modImplem, compInst, fifo_name, is_input);
	if(access==NULL) return NULL;
	netlist_access_fifo_t* fifo_data = access->data;

	// If there are multiple top-levels, create stuff in the current modImplem
	implem_t* Implem = data->Implem;
	if(modImplem!=NULL) Implem = modImplem;

	// Get symbol names to talk with the FIFO
	char* name_data = Hier_Build_GetSymbolFomPort(Implem, fifo_data->port_data);
	char* name_rdy = NULL;
	char* name_ack = NULL;
	if(access->component==Implem->netlist.top) {
		name_rdy = Hier_Build_GetSymbolFomPort(Implem, fifo_data->port_rdy);
		name_ack = Hier_Build_GetSymbolFomPort(Implem, fifo_data->port_ack);
	}
	else {
		name_ack = Hier_Build_GetSymbolFomPort(Implem, fifo_data->port_rdy);
		name_rdy = Hier_Build_GetSymbolFomPort(Implem, fifo_data->port_ack);
	}

	// Create the new assignment expression
	hvex_t* new_expr = NULL;

	if(is_input==true) {
		// Action: dest = fifo

		// Build the FIFO expr
		hvex_t* expr_fifo = hvex_newvec(name_data, fifo_data->width-1, 0);
		if(is_signed==true) hvex_set_signed(expr_fifo);
		// Crop or extend to the dest width
		hvex_t* new_dest = hvex_dup(vex);
		hvex_resize(expr_fifo, new_dest->width);

		// Build the full assignment Expr
		new_expr = hvex_asg_make(new_dest, hvex_dup(vex_iter), expr_fifo, NULL);
	}
	else {
		// Action: fifo = src

		// Create the INDEX model to read from the array
		hvex_t* new_src = hvex_dup(vex);
		hvex_addop_head(new_src, hvex_dup(vex_iter));
		new_src->model = HVEX_INDEX;

		// Crop or extend to the FIFO width
		hvex_t* expr_fifo = hvex_newvec(name_data, fifo_data->width-1, 0);
		hvex_resize(new_src, fifo_data->width);

		// Build the full assignment Expr
		new_expr = hvex_asg_make(expr_fifo, NULL, new_src, NULL);
	}

	// Create the test signal
	char* name_sig = Map_Netlist_MakeInstanceName_AvoidSyms(Implem, NETLIST_COMP_SIGNAL, &data->tree_avoidsym);
	netlist_comp_t* compSig = Netlist_Comp_Sig_New(name_sig, 1);
	Netlist_Comp_SetChild(Implem->netlist.top, compSig);

	// Create the LOOP node
	hier_node_t* nodeLoop = Hier_Node_NewType(HIERARCHY_LOOP);
	hier_loop_t* loop_data = &nodeLoop->NODE.LOOP;
	loop_data->testinfo->cond = hvex_newvec_bit(name_sig);
	loop_data->user.iter_nb_given = true;
	loop_data->user.iter_nb = nb;

	hier_build_bbstact_t bbstact;

	// Create the embedded BB
	Hier_Build_BBStAct(&bbstact);
	// Link to the Loop
	loop_data->body_after = bbstact.bb;
	bbstact.bb->PARENT = nodeLoop;
	nodeLoop->CHILDREN = addchain(nodeLoop->CHILDREN, bbstact.bb);

	// Set the new VEX expression to the Action
	bbstact.action->expr = new_expr;

	// Add an Action for handshake
	hier_action_t* action_rdy = Hier_Action_New();
	Hier_Action_Link(bbstact.state, action_rdy);
	action_rdy->expr = hvex_asg_make(
		hvex_newvec(name_rdy, 0, 0), NULL,
		hvex_newlit_bit('1'), NULL
	);

	// Add an Action for increment
	hier_action_t* action_inc = Hier_Action_New();
	Hier_Action_Link(bbstact.state, action_inc);
	action_inc->expr = hvex_asg_make(
		hvex_dup(vex_iter), NULL,
		hvex_newmodel_op2(HVEX_ADD, vex_iter->width, hvex_dup(vex_iter), hvex_newlit_int(1, vex_iter->width, false)),
		hvex_newvec_bit(name_ack)
	);

	// Add an Action for Test
	hier_action_t* action_test = Hier_Action_New();
	Hier_Action_Link(bbstact.state, action_test);
	action_test->expr = hvex_asg_make(
		hvex_newvec_bit(name_sig),
		NULL,
		hvex_newmodel_op2(HVEX_OR, 1,
			hvex_newmodel_op2(HVEX_NE, 1, hvex_dup(vex_iter), hvex_newlit_int(nb-1, vex_iter->width, false)),
			hvex_newnot(hvex_newvec_bit(name_ack))
		),
		NULL
	);

	// Create the Init BB and Action
	Hier_Build_BBStAct(&bbstact);
	bbstact.action->expr = hvex_asg_make(
		hvex_dup(vex_iter), NULL,
		hvex_newlit_int(0, vex_iter->width, false),
		NULL
	);
	// Add an Action to set the test Signal
	hier_action_t* action_initsig = Hier_Action_New();
	Hier_Action_Link(bbstact.state, action_initsig);
	action_initsig->expr = hvex_asg_make(hvex_newvec_bit(name_sig), NULL, hvex_newlit_bit('1'), NULL);

	// Link the Init BB to the Loop
	Hier_Nodes_Link(bbstact.bb, nodeLoop);

	return bbstact.bb;
}

// Internal function
static int Hier_BuiltinFunc_FifoVector_ReadWrite_vex(build_symtops_data_t* data, implem_t* modImplem, netlist_comp_t* compInst, hvex_t* Expr, bool is_input) {
	// Get the Action and the State node
	hier_action_t* action = Hier_Action_GetFromVex(Expr);
	hier_node_t* nodeState = action->node;

	// Get the FIFO name and destination VEX
	hvex_t* expr_fifo = action->expr->operands;
	hvex_t* expr_dest = action->expr->operands->next;
	hvex_t* expr_nb   = action->expr->operands->next->next;

	char* fifo_name = hvex_vec_getname(expr_fifo);

	if(expr_dest->model!=HVEX_VECTOR) {
		printf("Error: Access to FIFO '%s' must be with a bare array name\n", fifo_name);
		return -1;
	}
	if(expr_nb->model!=HVEX_LITERAL) {
		printf("Error: Access to FIFO '%s' with a non-literal number of iterations\n", fifo_name);
		return -1;
	}

	int nb = hvex_lit_evalint(expr_nb);
	if(nb<=0) {
		printf("Error: Access to FIFO '%s' with %u iterations\n", fifo_name, nb);
		return -1;
	}

	// If there are multiple top-levels, create stuff in the current modImplem
	implem_t* Implem = data->Implem;
	if(modImplem!=NULL) Implem = modImplem;

	// Get/create the iterator register, avoid existing symbols
	unsigned iter_w = uint_bitsnb(nb-1);
	char* iter_name = Map_Netlist_MakeInstanceName_AvoidSyms(Implem, NETLIST_COMP_REGISTER, &data->tree_avoidsym);
	netlist_comp_t* iter_comp = Netlist_Comp_Reg_New(iter_name, iter_w);
	Netlist_Comp_SetChild(Implem->netlist.top, iter_comp);

	hvex_t* iter_vex = hvex_newvec(iter_name, iter_w-1, 0);

	// Build the replacement Level
	hier_node_t* newLevel = Hier_BuildFifoVectorLoop(
		data, modImplem, compInst,
		fifo_name, is_input, hvex_is_signed(expr_fifo), expr_dest, iter_vex, nb
	);
	if(newLevel==NULL) return -1;

	// Change the source lines
	Hier_Nodes_SetLines(newLevel, action->source_lines, true, true);
	Hier_Level_SetExprFather(newLevel);

	hvex_free(iter_vex);

	// Replace the original Action and State
	Hier_ReplaceState_SplitBB(Implem->H, nodeState, newLevel);

	return 0;
}

static int Hier_BuiltinFunc_FifoVectorRead_vex(builtinfunc_t* builtin, build_symtops_data_t* data, implem_t* modImplem, netlist_comp_t* compInst, hvex_t* Expr) {
	return Hier_BuiltinFunc_FifoVector_ReadWrite_vex(data, modImplem, compInst, Expr, true);
}
static int Hier_BuiltinFunc_FifoVectorWrite_vex(builtinfunc_t* builtin, build_symtops_data_t* data, implem_t* modImplem, netlist_comp_t* compInst, hvex_t* Expr) {
	return Hier_BuiltinFunc_FifoVector_ReadWrite_vex(data, modImplem, compInst, Expr, false);
}



static int Hier_BuiltinFunc_UartCreate_vex(builtinfunc_t* builtin, build_symtops_data_t* data, implem_t* modImplem, netlist_comp_t* compInst, hvex_t* Expr) {
	// Get the Action and the State node
	hier_action_t* action = Hier_Action_GetFromVex(Expr);
	hier_node_t* nodeState = action->node;

	// Get the expr for reg name
	hvex_t* vex_name = Expr->operands;
	// Get the expr for baudrate
	hvex_t* vex_baudrate = Expr->operands->next;
	// Get the expr for parity
	hvex_t* vex_parity = Expr->operands->next->next;

	implem_t* Implem = data->Implem;

	// Check symbol width, ensure it is a register

	if(vex_name->model!=HVEX_VECTOR) {
		printf("ERROR: Illegal VEX type for UART name.\n");
		exit(EXIT_FAILURE);
	}
	char* name = hvex_vec_getname(vex_name);
	netlist_comp_t* comp = Netlist_Comp_GetChild(Implem->netlist.top, name);
	if(comp==NULL) {
		printf("ERROR: For UART creation, component name '%s' not found.\n", name);
		exit(EXIT_FAILURE);
	}
	if(comp->model!=NETLIST_COMP_REGISTER) {
		printf("ERROR: For UART creation, component name '%s' is model '%s', expected '%s'.\n",
			comp->name, comp->model->name, NETLIST_COMP_REGISTER->name
		);
		exit(EXIT_FAILURE);
	}

	netlist_register_t* reg_data = comp->data;
	if(reg_data->width<5 || reg_data->width>8) {
		printf("ERROR: For UART creation, symbol name '%s' has invalid width %u.\n", name, reg_data->width);
		exit(EXIT_FAILURE);
	}

	// Get the baudrate VEX
	if(vex_baudrate->model!=HVEX_LITERAL) {
		printf("ERROR: Illegal VEX type for UART baudrate.\n");
		exit(EXIT_FAILURE);
	}
	int baudrate = hvex_lit_evalint(vex_baudrate);
	if(baudrate<1) {
		printf("ERROR: For UART creation, illegal baudrate value %u.\n", baudrate);
		exit(EXIT_FAILURE);
	}

	// Get the parity value
	if(vex_parity->model!=HVEX_LITERAL) {
		printf("ERROR: Illegal VEX type for UART parity.\n");
		exit(EXIT_FAILURE);
	}
	bool parity = hvex_iszero(vex_parity)==true ? false : true;

	// FIXME Check that the baudrate is a standard value. If not, simply emit a warning.

	// Check that the clock divider is a safe value
	if(Implem->synth_target->clk_freq==0) {
		printf("ERROR: For UART creation, the design clock frequency must be set.\n");
		exit(EXIT_FAILURE);
	}
	unsigned divider = Implem->synth_target->clk_freq / baudrate;
	if(divider<10) {
		printf("ERROR: For UART creation, the obtained clock divider is too low (%u).\n", divider);
		exit(EXIT_FAILURE);
	}

	// Remove the reg component
	unsigned bits_nb = reg_data->width;
	Netlist_Comp_Free(comp);

	// Create the UART component
	comp = Netlist_Uart_New(name, bits_nb, parity, divider);
	Netlist_Comp_SetChild(Implem->netlist.top, comp);

	// Remove the function call
	Hier_ReplaceNode_Free(Implem->H, nodeState, NULL);

	return 0;
}

// Function calls: augh_port_in(reg), augh_port_out(reg)
static int Hier_BuiltinFunc_PortCreate_vex_internal(implem_t* Implem, implem_t* modImplem, netlist_comp_t* compInst, hvex_t* Expr, bool is_input) {
	// Get the Action and the State node
	hier_action_t* action = Hier_Action_GetFromVex(Expr);
	hier_node_t* nodeState = action->node;

	// Get the expr for reg name
	hvex_t* vex_name = Expr->operands;

	// Check symbol width, ensure it is a register

	char* strdir = is_input==true ? "input" : "output";

	if(vex_name->model!=HVEX_VECTOR) {
		printf("ERROR: Illegal VEX type for %s port.\n", strdir);
		return __LINE__;
	}
	char* name = hvex_vec_getname(vex_name);
	netlist_comp_t* comp = Netlist_Comp_GetChild(Implem->netlist.top, name);
	if(comp==NULL) {
		printf("ERROR: For %s port creation, component name '%s' not found.\n", strdir, name);
		return __LINE__;
	}
	if(comp->model!=NETLIST_COMP_REGISTER) {
		printf("ERROR: For %s port creation, component name '%s' is model '%s', expected '%s'.\n",
			strdir, comp->name, comp->model->name, NETLIST_COMP_REGISTER->name
		);
		return __LINE__;
	}

	netlist_register_t* reg_data = comp->data;
	unsigned width = reg_data->width;

	netlist_access_t* access = Netlist_Comp_GetAccess(Implem->netlist.top, name);
	if(access!=NULL) {
		printf("ERROR: For creation of %s port '%s', a top-level access (model '%s') with same name already exists.\n", strdir, name, access->model->name);
		return __LINE__;
	}

	// Create the WIRE access, in or out
	if(is_input==true) {
		// Remove the reg component
		Netlist_Comp_Free(comp);
		Netlist_Comp_AddPortAccess_WireIn(Implem->netlist.top, name, width);
	}
	else {
		netlist_port_t* port = Netlist_Comp_AddPortAccess_WireOut(Implem->netlist.top, name, width);
		// Hard-connect the register to the port
		port->source = Netlist_ValCat_MakeList_FullPort(reg_data->port_out);
		// Flag the register as protected (don't remove even if only written to)
		Implem_SymFlag_Add(Implem, comp->name, SYM_PROTECT_NODEL | SYM_PROTECT_NOPROPAG);
	}

	// Remove the function call
	Hier_ReplaceNode_Free(Implem->H, nodeState, NULL);

	return 0;
}
static int Hier_BuiltinFunc_PortInCreate_vex(builtinfunc_t* builtin, build_symtops_data_t* data, implem_t* modImplem, netlist_comp_t* compInst, hvex_t* Expr) {
	return Hier_BuiltinFunc_PortCreate_vex_internal(data->Implem, modImplem, compInst, Expr, true);
}
static int Hier_BuiltinFunc_PortOutCreate_vex(builtinfunc_t* builtin, build_symtops_data_t* data, implem_t* modImplem, netlist_comp_t* compInst, hvex_t* Expr) {
	return Hier_BuiltinFunc_PortCreate_vex_internal(data->Implem, modImplem, compInst, Expr, false);
}

// Function calls: augh_access_fifo_in(reg), augh_access_fifo_out(reg)
static int Hier_BuiltinFunc_AccFifoCreate_vex_internal(build_symtops_data_t* data, implem_t* modImplem, netlist_comp_t* compInst, hvex_t* Expr, bool is_input) {
	// Get the Action and the State node
	hier_action_t* action = Hier_Action_GetFromVex(Expr);
	hier_node_t* nodeState = action->node;

	// Get the expr for reg name
	hvex_t* vex_name = Expr->operands;

	implem_t* Implem = data->Implem;

	// Check symbol width, ensure it is a register

	char* strdir = is_input==true ? "IN" : "OUT";
	netlist_access_model_t* wanted_model = is_input==true ? NETLIST_ACCESS_FIFO_IN : NETLIST_ACCESS_FIFO_OUT;

	if(vex_name->model!=HVEX_VECTOR) {
		printf("ERROR: Illegal VEX type for FIFO %s access name.\n", strdir);
		exit(EXIT_FAILURE);
	}
	char* name = hvex_vec_getname(vex_name);
	netlist_comp_t* comp = Netlist_Comp_GetChild(Implem->netlist.top, name);
	if(comp==NULL) {
		netlist_access_t* access = Netlist_Comp_GetAccess(Implem->netlist.top, name);
		if(access!=NULL) {
			if(access->model==wanted_model) {
				printf("Note: For FIFO %s access creation: the access '%s' model '%s' already exists. Skipping.\n", strdir, name, access->model->name);
				// Remove the function call
				Hier_ReplaceNode_Free(Implem->H, nodeState, NULL);
				return 0;
			}
			else {
				printf("ERROR: For FIFO %s access creation, the access '%s' already exists but with wrong model: got '%s', expected '%s'.\n",
					strdir, name, access->model->name, wanted_model->name
				);
				exit(EXIT_FAILURE);
			}
		}
		printf("ERROR: For FIFO %s access creation, component name '%s' not found.\n", strdir, name);
		exit(EXIT_FAILURE);
	}
	if(comp->model!=NETLIST_COMP_REGISTER) {
		printf("ERROR: For FIFO %s access creation, component name '%s' is model '%s', expected '%s'.\n",
			strdir, comp->name, comp->model->name, NETLIST_COMP_REGISTER->name
		);
		exit(EXIT_FAILURE);
	}

	netlist_register_t* reg_data = comp->data;
	unsigned width = reg_data->width;

	netlist_access_t* access = Netlist_Comp_GetAccess(Implem->netlist.top, name);
	if(access!=NULL) {
		printf("ERROR: For FIFO %s access creation, a top-level access named '%s' (model '%s') already exists.\n", strdir, name, access->model->name);
		exit(EXIT_FAILURE);
	}

	// Remove the reg component
	Netlist_Comp_Free(comp);

	// Create the FIFO OUT access
	if(is_input==true) {
		Netlist_Comp_AddAccess_FifoIn(Implem->netlist.top, name, width, false, &data->tree_avoidsym);
	}
	else {
		Netlist_Comp_AddAccess_FifoOut(Implem->netlist.top, name, width, false, &data->tree_avoidsym);
	}

	// Remove the function call
	Hier_ReplaceNode_Free(Implem->H, nodeState, NULL);

	return 0;
}
static int Hier_BuiltinFunc_AccFifoInCreate_vex(builtinfunc_t* builtin, build_symtops_data_t* data, implem_t* modImplem, netlist_comp_t* compInst, hvex_t* Expr) {
	return Hier_BuiltinFunc_AccFifoCreate_vex_internal(data, modImplem, compInst, Expr, true);
}
static int Hier_BuiltinFunc_AccFifoOutCreate_vex(builtinfunc_t* builtin, build_symtops_data_t* data, implem_t* modImplem, netlist_comp_t* compInst, hvex_t* Expr) {
	return Hier_BuiltinFunc_AccFifoCreate_vex_internal(data, modImplem, compInst, Expr, false);
}

// Function calls: augh_access_uart_rx(reg), augh_access_uart_tx(reg)
static int Hier_BuiltinFunc_AccUartCreate_vex_internal(implem_t* Implem, implem_t* modImplem, netlist_comp_t* compInst, hvex_t* Expr, bool is_input) {
	// Get the Action and the State node
	hier_action_t* action = Hier_Action_GetFromVex(Expr);
	hier_node_t* nodeState = action->node;

	// Get the expr for reg name
	hvex_t* vex_name = Expr->operands;

	char* strdir = is_input==true ? "Rx" : "Tx";

	// Check symbol width, ensure it is a register

	if(vex_name->model!=HVEX_VECTOR) {
		printf("ERROR: Illegal VEX type for UART %s access name.\n", strdir);
		exit(EXIT_FAILURE);
	}
	char* name = hvex_vec_getname(vex_name);
	netlist_comp_t* comp = Netlist_Comp_GetChild(Implem->netlist.top, name);
	if(comp==NULL) {
		printf("ERROR: For UART %s access creation, component name '%s' not found.\n", strdir, name);
		exit(EXIT_FAILURE);
	}
	if(comp->model!=NETLIST_COMP_REGISTER) {
		printf("ERROR: For UART %s access creation, component name '%s' is model '%s', expected '%s'.\n",
			strdir, comp->name, comp->model->name, NETLIST_COMP_REGISTER->name
		);
		exit(EXIT_FAILURE);
	}

	netlist_register_t* reg_data = comp->data;
	if(reg_data->width != 1) {
		printf("ERROR: For UART %s access creation, register name '%s' has width %u, expected 1.\n", strdir, name, reg_data->width);
		exit(EXIT_FAILURE);
	}

	netlist_access_t* access = Netlist_Comp_GetAccess(Implem->netlist.top, name);
	if(access!=NULL) {
		printf("ERROR: For UART %s access creation, a top-level access named '%s' (model '%s') already exists.\n", strdir, name, access->model->name);
		exit(EXIT_FAILURE);
	}

	// Remove the reg component
	Netlist_Comp_Free(comp);

	// Create the UART RX access
	if(is_input==true) {
		access = Netlist_Comp_AddAccess_Uart_Rx(Implem->netlist.top, name);
	}
	else {
		access = Netlist_Comp_AddAccess_Uart_Tx(Implem->netlist.top, name);
	}

	// Remove the function call
	Hier_ReplaceNode_Free(Implem->H, nodeState, NULL);

	// Rename all occurrences of the previous symbol (the port name is not the access name)
	if(is_input==true) {
		netlist_access_uart_side_t* accuart_data = access->data;
		chain_list* list = Hier_GetSymbolOccur(Implem->H, name);
		foreach(list, scan) {
			hvex_t* Vex = scan->DATA;
			assert(Vex->model==HVEX_VECTOR);
			hvex_vec_setname(Vex, accuart_data->port_uart->name);
		}
		freechain(list);
	}

	return 0;
}
static int Hier_BuiltinFunc_AccUartRxCreate_vex(builtinfunc_t* builtin, build_symtops_data_t* data, implem_t* modImplem, netlist_comp_t* compInst, hvex_t* Expr) {
	return Hier_BuiltinFunc_AccUartCreate_vex_internal(data->Implem, modImplem, compInst, Expr, true);
}
static int Hier_BuiltinFunc_AccUartTxCreate_vex(builtinfunc_t* builtin, build_symtops_data_t* data, implem_t* modImplem, netlist_comp_t* compInst, hvex_t* Expr) {
	return Hier_BuiltinFunc_AccUartCreate_vex_internal(data->Implem, modImplem, compInst, Expr, false);
}


// Non-blocking FIFO related builtin functions

// Function calls: augh_tryread(fifo, data), augh_trywrite(fifo, data)
//   They return bool (the ack value)
// Create the fifo data ASG Action and the fifo ASG Rdy Action. Return the Expr to test the Ack value
static int Hier_BuiltinFunc_FifoTry_vex_internal(build_symtops_data_t* data, implem_t* modImplem, netlist_comp_t* compInst, hvex_t* Expr, bool is_input) {
	// Get the Action and the State node
	hier_action_t* action = Hier_Action_GetFromVex(Expr);
	hier_node_t* node = action->node;

	// Get the FIFO name and destination VEX
	hvex_t* vex_fifoname = Expr->operands;
	hvex_t* vex = Expr->operands->next;

	char* fifo_name = hvex_vec_getname(vex_fifoname);
	bool is_signed = hvex_is_signed(vex_fifoname);

	// Get or create the FIFO access
	netlist_access_t* access = Hier_Builtin_GetFifoAccess(data, modImplem, compInst, fifo_name, is_input);
	if(access==NULL) return __LINE__;
	netlist_access_fifo_t* fifo_data = access->data;

	// If there are multiple top-levels, create stuff in the current modImplem
	implem_t* Implem = data->Implem;
	if(modImplem!=NULL) Implem = modImplem;
	// Get symbol names to talk with the FIFO
	char* name_data = Hier_Build_GetSymbolFomPort(Implem, fifo_data->port_data);
	char* name_rdy = NULL;
	char* name_ack = NULL;
	if(access->component==Implem->netlist.top) {
		name_rdy = Hier_Build_GetSymbolFomPort(Implem, fifo_data->port_rdy);
		name_ack = Hier_Build_GetSymbolFomPort(Implem, fifo_data->port_ack);
	}
	else {
		name_ack = Hier_Build_GetSymbolFomPort(Implem, fifo_data->port_rdy);
		name_rdy = Hier_Build_GetSymbolFomPort(Implem, fifo_data->port_ack);
	}

	// Create the new assignment expression
	hvex_t* new_expr = NULL;

	if(is_input==true) {
		// Action: dest = fifo

		// Build the FIFO expr
		hvex_t* expr_fifo = hvex_newvec(name_data, fifo_data->width-1, 0);
		if(is_signed==true) hvex_set_signed(expr_fifo);
		// Crop or extend to the dest width
		hvex_resize(expr_fifo, vex->width);

		// Build the full assignment Expr
		if(vex->model==HVEX_VECTOR) {
			new_expr = hvex_asg_make(hvex_dup(vex), NULL, expr_fifo, NULL);
		}
		else if(vex->model==HVEX_INDEX) {
			new_expr = hvex_asg_make(
				hvex_newvec(hvex_index_getname(vex), vex->left, vex->right),
				hvex_dup(vex->operands),
				expr_fifo, NULL
			);
		}
		else {
			abort();
		}
	}
	else {
		// Action: fifo = src
		hvex_t* expr_fifo = hvex_newvec(name_data, fifo_data->width-1, 0);
		hvex_t* new_src = hvex_new_resize(hvex_dup(vex), fifo_data->width);
		new_expr = hvex_asg_make(expr_fifo, NULL, new_src, NULL);
	}

	hvex_t* expr_ack = hvex_newvec_bit(name_ack);

	// Create the Action for Data ASG
	hier_action_t* action_data = Hier_Action_New();
	Hier_Action_Link(node, action_data);
	action_data->expr = new_expr;
	Hier_Action_SetExprFather(action_data);
	action_data->source_lines = LineList_Dup(action->source_lines);

	// Create the Action for Ready ASG
	hier_action_t* action_rdy = Hier_Action_New();
	Hier_Action_Link(node, action_rdy);
	action_rdy->expr = hvex_asg_make(hvex_newvec_bit(name_rdy), NULL, hvex_newlit_bit('1'), NULL);
	Hier_Action_SetExprFather(action_rdy);
	action_rdy->source_lines = LineList_Dup(action->source_lines);

	// The 3 Actions MUST stay together. Add flags.
	avl_pi_add_overwrite(&action->deps_timed, action_data, 0);
	avl_pi_add_overwrite(&action->deps_timed, action_rdy, 0);
	avl_pi_add_overwrite(&action_data->deps_timed, action, 0);
	avl_pi_add_overwrite(&action_data->deps_timed, action_rdy, 0);
	avl_pi_add_overwrite(&action_rdy->deps_timed, action, 0);
	avl_pi_add_overwrite(&action_rdy->deps_timed, action_data, 0);

	// Replace the original Vex by the vex_ack
	hvex_replace_free(Expr, expr_ack);

	return 0;
}
static int Hier_BuiltinFunc_FifoTryRead_vex(builtinfunc_t* builtin, build_symtops_data_t* data, implem_t* modImplem, netlist_comp_t* compInst, hvex_t* Expr) {
	return Hier_BuiltinFunc_FifoTry_vex_internal(data, modImplem, compInst, Expr, true);
}
static int Hier_BuiltinFunc_FifoTryWrite_vex(builtinfunc_t* builtin, build_symtops_data_t* data, implem_t* modImplem, netlist_comp_t* compInst, hvex_t* Expr) {
	return Hier_BuiltinFunc_FifoTry_vex_internal(data, modImplem, compInst, Expr, false);
}

// Function calls: aughin_setready(fifo), aughout_setready(fifo)
//                 aughin_setreadyval(fifo, val), aughout_setreadyval(fifo, val)
static int Hier_BuiltinFunc_FifoSetReady_vex_internal(build_symtops_data_t* data, implem_t* modImplem, netlist_comp_t* compInst, hvex_t* Expr, bool is_input) {
	// Get the Action and the State node
	hier_action_t* action = Hier_Action_GetFromVex(Expr);
	//hier_node_t* node = action->node;

	// Get the FIFO name
	hvex_t* expr_fifo = Expr->operands;
	char* fifo_name = hvex_vec_getname(expr_fifo);

	hvex_t* vex_val = NULL;
	if(Expr->operands->next!=NULL) {
		vex_val = hvex_new_cropabs(hvex_dup(Expr->operands->next), 0, 0);
	}
	else {
		vex_val = hvex_newlit_bit('1');
	}

	// Get or create the FIFO access
	netlist_access_t* access = Hier_Builtin_GetFifoAccess(data, modImplem, compInst, fifo_name, is_input);
	if(access==NULL) return __LINE__;
	netlist_access_fifo_t* fifo_data = access->data;

	// If there are multiple top-levels, create stuff in the current modImplem
	implem_t* Implem = data->Implem;
	if(modImplem!=NULL) Implem = modImplem;
	// Get symbol names to talk with the FIFO
	char* name_rdy = NULL;
	if(access->component==Implem->netlist.top) {
		name_rdy = Hier_Build_GetSymbolFomPort(Implem, fifo_data->port_rdy);
	}
	else {
		name_rdy = Hier_Build_GetSymbolFomPort(Implem, fifo_data->port_ack);
	}

	// Change the original Action
	hvex_free(action->expr);
	action->expr = hvex_asg_make(hvex_newvec_bit(name_rdy), NULL, vex_val, NULL);
	Hier_Action_SetExprFather(action);

	return 0;
}
static int Hier_BuiltinFunc_FifoInSetReady_vex(builtinfunc_t* builtin, build_symtops_data_t* data, implem_t* modImplem, netlist_comp_t* compInst, hvex_t* Expr) {
	return Hier_BuiltinFunc_FifoSetReady_vex_internal(data, modImplem, compInst, Expr, true);
}
static int Hier_BuiltinFunc_FifoOutSetReady_vex(builtinfunc_t* builtin, build_symtops_data_t* data, implem_t* modImplem, netlist_comp_t* compInst, hvex_t* Expr) {
	return Hier_BuiltinFunc_FifoSetReady_vex_internal(data, modImplem, compInst, Expr, false);
}

// Function call: aughin_spydata(fifo, data), aughin_setdata(fifo, data)
static int Hier_BuiltinFunc_FifoSpySetData_vex_internal(build_symtops_data_t* data, implem_t* modImplem, netlist_comp_t* compInst, hvex_t* Expr, bool is_input) {
	// Get the Action and the State node
	hier_action_t* action = Hier_Action_GetFromVex(Expr);
	//hier_node_t* node = action->node;

	// Get the FIFO name and destination VEX
	hvex_t* vex_fifoname = Expr->operands;
	hvex_t* vex = Expr->operands->next;

	char* fifo_name = hvex_vec_getname(vex_fifoname);
	bool is_signed = hvex_is_signed(vex_fifoname);

	// Get or create the FIFO access
	netlist_access_t* access = Hier_Builtin_GetFifoAccess(data, modImplem, compInst, fifo_name, is_input);
	if(access==NULL) return __LINE__;
	netlist_access_fifo_t* fifo_data = access->data;

	// If there are multiple top-levels, create stuff in the current modImplem
	implem_t* Implem = data->Implem;
	if(modImplem!=NULL) Implem = modImplem;
	// Get symbol names to talk with the FIFO
	char* name_data = Hier_Build_GetSymbolFomPort(Implem, fifo_data->port_data);

	hvex_t* new_expr = NULL;

	if(is_input==true) {
		// Action: src = fifo
		// Build the FIFO expr
		hvex_t* expr_fifo = hvex_newvec(name_data, fifo_data->width-1, 0);
		if(is_signed==true) hvex_set_signed(expr_fifo);
		// Crop or extend to the dest width
		hvex_resize(expr_fifo, vex->width);

		// Build the full assignment Expr
		if(vex->model==HVEX_VECTOR) {
			new_expr = hvex_asg_make(hvex_dup(vex), NULL, expr_fifo, NULL);
		}
		else if(vex->model==HVEX_VECTOR) {
			new_expr = hvex_asg_make(
				hvex_newvec(hvex_index_getname(vex), vex->left, vex->right),
				hvex_dup(vex->operands),
				expr_fifo, NULL
			);
		}
		else {
			abort();
		}
	}
	else {
		// Action: fifo = src
		hvex_t* expr_fifo = hvex_newvec(name_data, fifo_data->width-1, 0);
		hvex_t* new_src = hvex_new_resize(hvex_dup(vex), fifo_data->width);
		new_expr = hvex_asg_make(expr_fifo, NULL, new_src, NULL);
	}

	// Change the original Action
	hvex_free(action->expr);
	action->expr = new_expr;
	Hier_Action_SetExprFather(action);

	return 0;
}
static int Hier_BuiltinFunc_FifoSpyData_vex(builtinfunc_t* builtin, build_symtops_data_t* data, implem_t* modImplem, netlist_comp_t* compInst, hvex_t* Expr) {
	return Hier_BuiltinFunc_FifoSpySetData_vex_internal(data, modImplem, compInst, Expr, true);
}
static int Hier_BuiltinFunc_FifoSetData_vex(builtinfunc_t* builtin, build_symtops_data_t* data, implem_t* modImplem, netlist_comp_t* compInst, hvex_t* Expr) {
	return Hier_BuiltinFunc_FifoSpySetData_vex_internal(data, modImplem, compInst, Expr, false);
}

// Function call: bool aughin_spyready(fifo), aughout_spyready(fifo)
static int Hier_BuiltinFunc_FifoSpyReady_vex_internal(build_symtops_data_t* data, implem_t* modImplem, netlist_comp_t* compInst, hvex_t* Expr, bool is_input) {
	// Get the Action and the State node
	//hier_action_t* action = Hier_Action_GetFromVex(Expr);
	//hier_node_t* node = action->node;

	// Get the FIFO name and destination VEX
	hvex_t* vex_fifoname = Expr->operands;
	char* fifo_name = hvex_vec_getname(vex_fifoname);

	// Get or create the FIFO access
	netlist_access_t* access = Hier_Builtin_GetFifoAccess(data, modImplem, compInst, fifo_name, is_input);
	if(access==NULL) return __LINE__;
	netlist_access_fifo_t* fifo_data = access->data;

	// If there are multiple top-levels, create stuff in the current modImplem
	implem_t* Implem = data->Implem;
	if(modImplem!=NULL) Implem = modImplem;
	// Get symbol names to talk with the FIFO
	char* name_ack = NULL;
	if(access->component==Implem->netlist.top) {
		name_ack = Hier_Build_GetSymbolFomPort(Implem, fifo_data->port_ack);
	}
	else {
		name_ack = Hier_Build_GetSymbolFomPort(Implem, fifo_data->port_rdy);
	}

	hvex_t* expr_ack = hvex_newvec_bit(name_ack);

	// Replace the original Vex by the vex_ack
	hvex_replace_free(Expr, expr_ack);

	return 0;
}
static int Hier_BuiltinFunc_FifoInSpyReady_vex(builtinfunc_t* builtin, build_symtops_data_t* data, implem_t* modImplem, netlist_comp_t* compInst, hvex_t* Expr) {
	return Hier_BuiltinFunc_FifoSpyReady_vex_internal(data, modImplem, compInst, Expr, true);
}
static int Hier_BuiltinFunc_FifoOutSpyReady_vex(builtinfunc_t* builtin, build_symtops_data_t* data, implem_t* modImplem, netlist_comp_t* compInst, hvex_t* Expr) {
	return Hier_BuiltinFunc_FifoSpyReady_vex_internal(data, modImplem, compInst, Expr, false);
}



//=====================================================================
// Creation of data interfaces
//=====================================================================

// This is meant to be executed BEFORE data buffer creation and replacement of data transfers
// A register only gives the desired channel width
// It is deleted and/or replaced by a more appropriate interface component
static int Hier_InheritBoardInterfaces_FromReg(implem_t* Implem) {
	board_fpga_t* board_fpga = Implem->synth_target->board_fpga;
	if(board_fpga==NULL) return 0;

	// Pre-allocate some names
	char* namealloc_stdin = namealloc("stdin");
	char* namealloc_stdout = namealloc("stdout");

	// List the available registers
	chain_list* list_regs = NULL;
	avl_pp_foreach(&Implem->netlist.top->children, scanComp) {
		netlist_comp_t* comp = scanComp->data;
		if(comp->model==NETLIST_COMP_REGISTER) list_regs = addchain(list_regs, comp);
	}

	// Rescan the entire list
	foreach(list_regs, scanReg) {

		netlist_comp_t* comp = scanReg->DATA;
		netlist_register_t* reg_data = comp->data;

		unsigned width = reg_data->width;
		char* name = comp->name;

		// Get the corresponding board access
		netlist_access_t* BoardAccess = Netlist_Comp_GetAccess(board_fpga->connect, name);
		if(BoardAccess==NULL) {
			// Check if it is stdin/stdout and if there is a corresponding access set in the board
			if     (name==namealloc_stdin  && board_fpga->default_stdin!=NULL)  BoardAccess = board_fpga->default_stdin;
			else if(name==namealloc_stdout && board_fpga->default_stdout!=NULL) BoardAccess = board_fpga->default_stdout;
		}
		if(BoardAccess==NULL) continue;

		// Safety check
		netlist_access_t* TopAccess = Netlist_Comp_GetAccess(Implem->netlist.top, comp->name);
		if(TopAccess!=NULL) {
			printf("Error: Replacement of '%s' by a board access: a top-level access already exists.\n", name);
			exit(EXIT_FAILURE);
		}

		// Skip if the register is not used in the Hier (it could be a simple declaration)
		chain_list* listoccur = Hier_GetSymbolOccur(Implem->H, name);
		if(listoccur==NULL) continue;
		freechain(listoccur);

		// The board defines what the register should be. Replace!

		if(BoardAccess->model==NETLIST_ACCESS_UART) {
			netlist_uart_access_t* boarduart_data = BoardAccess->data;

			printf("Info: Replacing declaration of '%s' by an UART interface.\n", name);

			// Warn if the Register width is different from the UART number of data bits
			if(width!=boarduart_data->bits_nb) {
				printf("Error: Replacement of '%s' by an UART component: width is %u, board declares %u.\n", name, width, boarduart_data->bits_nb);
				exit(EXIT_FAILURE);
			}

			// Remove the register, it will be replaced by a UART component
			Netlist_Comp_Free(comp);

			// Create the top-level access and link to Board accesses for pin attributes
			netlist_access_t* acctop_implem = Netlist_Comp_AddAccess_Uart(Implem->netlist.top, name);
			avl_pp_add_overwrite(&Implem->synth_target->impacc2boardacc, acctop_implem, BoardAccess);

			// Create the UART component
			// FIXME warn if the clock divider is too low
			if(Implem->synth_target->clk_freq==0) {
				printf("Error: Replacement of '%s' by an UART component: a clock frequency must be set.\n", name);
				exit(EXIT_FAILURE);
			}
			unsigned divider = Implem->synth_target->clk_freq / boarduart_data->baudrate;
			if(divider<10) {
				printf("Error: Replacement of '%s' by an UART component: divider value too low: %u.\n", name, divider);
				exit(EXIT_FAILURE);
			}
			netlist_comp_t* comp_uart = Netlist_Uart_New(name, boarduart_data->bits_nb, boarduart_data->parity, divider);
			Netlist_Comp_SetChild(Implem->netlist.top, comp_uart);

			// Connect the UART access and component
			netlist_uart_access_t* acctop_data = acctop_implem->data;
			netlist_uart_t* uart_data = comp_uart->data;
			netlist_uart_access_t* accuart_data = uart_data->access_uart->data;

			// Connection of control ports
			uart_data->port_clk->source = Netlist_ValCat_MakeList_Sig(Implem->netlist.sig_clock);
			uart_data->port_reset->source = Netlist_ValCat_MakeList_Sig(Implem->netlist.sig_reset);
			Netlist_Access_Reset_SetActiveState(uart_data->port_reset->access, Implem->synth_target->reset_active_state);

			// Connection of TX and RX ports
			acctop_data->port_tx->source = Netlist_ValCat_MakeList_FullPort(accuart_data->port_tx);
			accuart_data->port_rx->source = Netlist_ValCat_MakeList_FullPort(acctop_data->port_rx);
			Netlist_PortTargets_SetSource(acctop_data->port_tx);
			Netlist_PortTargets_SetSource(accuart_data->port_rx);
		}

		else if(BoardAccess->model==NETLIST_ACCESS_UART_RX) {
			netlist_access_uart_side_t* boarduart_data = BoardAccess->data;

			printf("Info: Replacing declaration of '%s' by an UART RX interface.\n", name);

			// Remove the register, it will be replaced by an UART component
			Netlist_Comp_Free(comp);

			// Create the top-level access and link to Board accesses for pin attributes
			netlist_access_t* acctop_implem = Netlist_Comp_AddAccess_Uart_Rx(Implem->netlist.top, name);
			avl_pp_add_overwrite(&Implem->synth_target->impacc2boardacc, acctop_implem, BoardAccess);

			// Create the UART RX component
			netlist_comp_t* comp_uart = Netlist_Uart_Rx_New(name, width);
			Netlist_Comp_SetChild(Implem->netlist.top, comp_uart);

			netlist_access_uart_side_t* acctop_data = acctop_implem->data;
			netlist_uart_side_t* uart_data = comp_uart->data;
			netlist_access_uart_side_t* accuart_data = uart_data->access_uart->data;

			// Copy the UART config in new top-level access and new component
			accuart_data->cfg = boarduart_data->cfg;
			acctop_data->cfg  = boarduart_data->cfg;

			// Connection of control ports
			uart_data->port_clk->source = Netlist_ValCat_MakeList_Sig(Implem->netlist.sig_clock);
			Netlist_Access_Clock_SetFrequency(uart_data->port_clk->access, Implem->synth_target->clk_freq);
			uart_data->port_reset->source = Netlist_ValCat_MakeList_Sig(Implem->netlist.sig_reset);
			Netlist_Access_Reset_SetActiveState(uart_data->port_reset->access, Implem->synth_target->reset_active_state);

			// Connection of RX ports
			accuart_data->port_uart->source = Netlist_ValCat_MakeList_FullPort(acctop_data->port_uart);
			Netlist_PortTargets_SetSource(accuart_data->port_uart);
		}
		else if(BoardAccess->model==NETLIST_ACCESS_UART_TX) {
			netlist_access_uart_side_t* boarduart_data = BoardAccess->data;

			printf("Info: Replacing declaration of '%s' by an UART TX interface.\n", name);

			// Remove the register, it will be replaced by an UART component
			Netlist_Comp_Free(comp);

			// Create the top-level access and link to Board accesses for pin attributes
			netlist_access_t* acctop_implem = Netlist_Comp_AddAccess_Uart_Tx(Implem->netlist.top, name);
			avl_pp_add_overwrite(&Implem->synth_target->impacc2boardacc, acctop_implem, BoardAccess);

			// Create the UART TX component
			netlist_comp_t* comp_uart = Netlist_Uart_Tx_New(name, width);
			Netlist_Comp_SetChild(Implem->netlist.top, comp_uart);

			netlist_access_uart_side_t* acctop_data = acctop_implem->data;
			netlist_uart_side_t* uart_data = comp_uart->data;
			netlist_access_uart_side_t* accuart_data = uart_data->access_uart->data;

			// Copy the UART config in new top-level access and new component
			accuart_data->cfg = boarduart_data->cfg;
			acctop_data->cfg  = boarduart_data->cfg;

			// Connection of control ports
			uart_data->port_clk->source = Netlist_ValCat_MakeList_Sig(Implem->netlist.sig_clock);
			Netlist_Access_Clock_SetFrequency(uart_data->port_clk->access, Implem->synth_target->clk_freq);
			uart_data->port_reset->source = Netlist_ValCat_MakeList_Sig(Implem->netlist.sig_reset);
			Netlist_Access_Reset_SetActiveState(uart_data->port_reset->access, Implem->synth_target->reset_active_state);

			// Connection of TX ports
			acctop_data->port_uart->source = Netlist_ValCat_MakeList_FullPort(accuart_data->port_uart);
			Netlist_PortTargets_SetSource(acctop_data->port_uart);
		}

		else if(BoardAccess->model==NETLIST_ACCESS_FIFO_IN) {
			netlist_access_fifo_t* boardfifo_data = BoardAccess->data;

			printf("Info: Replacing declaration of '%s' by a FIFO IN interface.\n", name);

			// Warn if the Register width is different from the data width of the board access
			if(width!=boardfifo_data->width) {
				printf("Error: Replacement of '%s' by a FIFO interface: width is %u, board declares %u.\n", name, width, boardfifo_data->width);
				exit(EXIT_FAILURE);
			}

			// Create the top-level access
			netlist_access_t* acctop_implem = Netlist_Comp_AddAccess_FifoIn(Implem->netlist.top, name, width, false, NULL);
			avl_pp_add_overwrite(&Implem->synth_target->impacc2boardacc, acctop_implem, BoardAccess);

			// Remove the Reg component
			Netlist_Comp_Free(comp);
		}
		else if(BoardAccess->model==NETLIST_ACCESS_FIFO_OUT) {
			netlist_access_fifo_t* boardfifo_data = BoardAccess->data;

			printf("Info: Replacing declaration of '%s' by a FIFO OUT interface.\n", name);

			// Warn if the Register width is different from the data width of the board access
			if(width!=boardfifo_data->width) {
				printf("Error: Replacement of '%s' by a FIFO interface: width is %u, board declares %u.\n", name, width, boardfifo_data->width);
				exit(EXIT_FAILURE);
			}

			// Create the top-level access
			netlist_access_t* acctop_implem = Netlist_Comp_AddAccess_FifoOut(Implem->netlist.top, name, width, false, NULL);
			avl_pp_add_overwrite(&Implem->synth_target->impacc2boardacc, acctop_implem, BoardAccess);

			// Remove the Reg component
			Netlist_Comp_Free(comp);
		}

		else if(BoardAccess->model==NETLIST_ACCESS_WIRE_IN) {
			netlist_access_single_port_t* boardwire_data = BoardAccess->data;

			//printf("Info: Replacing declaration of '%s' by a top-level WIRE IN interface.\n", name);
			printf("Info: Extending declaration of '%s' with a top-level buffered WIRE IN interface.\n", name);

			// Warn if the Register width is different from the board port width
			if(width!=boardwire_data->port->width) {
				printf("Error: Replacement of '%s' by a WIRE interface: width is %u, board declares %u.\n", name, width, boardwire_data->port->width);
				exit(EXIT_FAILURE);
			}

			// Create the top-level access
			netlist_port_t* impwire = Netlist_Comp_AddPortAccess_WireIn(Implem->netlist.top, name, width);
			avl_pp_add_overwrite(&Implem->synth_target->impacc2boardacc, impwire->access, BoardAccess);

			#if 0  // Remove the Reg component
			Netlist_Comp_Free(comp);
			#endif

			#if 1  // Keep the Register and use it to bufferize the FPGA input, to remove glitches

			// The register component is kept
			// Add another register for double buffering
			char buf[strlen(comp->name) + 20];
			sprintf(buf, "%s_dblbuf", comp->name);
			char* newname = Map_Netlist_MakeInstanceName_Prefix(Implem, namealloc(buf));
			netlist_comp_t* newReg = Netlist_Comp_Reg_New(newname, width);
			Netlist_Comp_SetChild(Implem->netlist.top, newReg);
			netlist_register_t* newreg_data = newReg->data;

			// Connect the new Reg to the new top-level port
			newreg_data->port_in->source = Netlist_ValCat_MakeList_FullPort(impwire);
			newreg_data->port_en->source = Netlist_ValCat_MakeList_Literal("1");

			// Connect the original Reg to the new Reg
			reg_data->port_in->source = Netlist_ValCat_MakeList_FullPort(newreg_data->port_out);
			reg_data->port_en->source = Netlist_ValCat_MakeList_Literal("1");

			// Note: There is nothing to check nor replace here because the register is kept
			// Flag the registers as protected (don't remove even if only written to)
			Implem_SymFlag_Add(Implem, comp->name, SYM_PROTECT_NODEL | SYM_PROTECT_NOPROPAG | SYM_PROTECT_NOCIRC);
			Implem_SymFlag_Add(Implem, newReg->name, SYM_PROTECT_NODEL | SYM_PROTECT_NOPROPAG | SYM_PROTECT_NOCIRC);

			#endif
		}
		else if(BoardAccess->model==NETLIST_ACCESS_WIRE_OUT) {
			netlist_access_single_port_t* boardwire_data = BoardAccess->data;

			printf("Info: Extending declaration of '%s' with a top-level WIRE OUT interface.\n", name);

			// Warn if the Register width is different from the board port width
			if(width!=boardwire_data->port->width) {
				printf("Error: Extension of '%s' with a WIRE OUT interface: width is %u, board declares %u.\n", name, width, boardwire_data->port->width);
				exit(EXIT_FAILURE);
			}

			// Create the top-level access
			netlist_port_t* impwire = Netlist_Comp_AddPortAccess_WireOut(Implem->netlist.top, name, width);
			avl_pp_add_overwrite(&Implem->synth_target->impacc2boardacc, impwire->access, BoardAccess);

			// Keep the reg component, connect it to the new top-level port
			chain_list* valcat = Netlist_ValCat_MakeList_Reg(comp);
			valcat = Netlist_ValCat_CropOrExtend(valcat, width, boardwire_data->port->width, '0');
			impwire->source = valcat;

			// Note: There is nothing to check nor replace here because the register is kept
			// Flag the register as protected (don't remove even if only written to)
			Implem_SymFlag_Add(Implem, comp->name, SYM_PROTECT_NODEL | SYM_PROTECT_NOPROPAG | SYM_PROTECT_NOCIRC);
		}

		else {
			// This code fails on reset and clock signals
			// These are already linked at board selection
			// Doing nothing should be better
			#if 0
			printf("Error: Replacement of '%s' by the board access '%s' model '%s': can't be used by user.\n", name, BoardAccess->name, BoardAccess->model->name);
			exit(EXIT_FAILURE);
			#endif
		}

	}  // Scan all Reg components

	// Clean
	freechain(list_regs);

	return 0;
}

#if 0  // FIXME Decide what to do about that now multi-thread and builtins have ben modified

// This function can be used after the built-in function have been replaced
__attribute((__unused__))
static int Hier_InheritBoardInterfaces_FromTopAcc(implem_t* Implem) {
	board_fpga_t* board_fpga = Implem->synth_target->board_fpga;
	if(board_fpga==NULL) return 0;

	int LinkAccesses_Clock(netlist_access_t* TopAcc, netlist_access_t* BoardAcc) {
		double board_freq = Netlist_Access_Clock_GetFrequency(BoardAcc);
		if(board_freq>0) {
			Netlist_Access_Clock_SetFrequency(TopAcc, board_freq);
			if(Implem->synth_target->clk_freq!=0) Techno_SynthTarget_SetFrequency(Implem->synth_target, board_freq);
		}
		avl_pp_add_overwrite(&Implem->synth_target->impacc2boardacc, TopAcc, BoardAcc);
	}
	int LinkAccesses_Clock_check(netlist_access_t* TopAcc, netlist_access_t* BoardAcc) {
		if(BoardAcc->model!=NETLIST_ACCESS_CLOCK) {
			printf("Warning: Top-level clock access '%s': Skipping board access '%s' model '%s'.\n",
				TopAcc->name, BoardAcc->name, BoardAcc->model->name
			);
			return -1;
		}
		netlist_access_single_port_t* accport_data = BoardAcc->data;
		if(accport_data->port->direction!=NETLIST_PORT_DIR_IN) {
			printf("Warning: Top-level clock access '%s': Skipping board clock '%s' which has wrong direction.\n", TopAcc->name, BoardAcc->name);
			return -1;
		}
		LinkAccesses_Clock(TopAcc, BoardAcc);
	}

	int LinkAccesses_Reset(netlist_access_t* TopAcc, netlist_access_t* BoardAcc) {
		char reset_level = Netlist_Access_Reset_GetActiveState(BoardAcc);
		if(reset_level!=0) Netlist_Access_Reset_SetActiveState(TopAcc, reset_level);
		avl_pp_add_overwrite(&Implem->synth_target->impacc2boardacc, TopAcc, BoardAcc);
	}
	int LinkAccesses_Reset_check(netlist_access_t* TopAcc, netlist_access_t* BoardAcc) {
		if(BoardAcc->model!=NETLIST_ACCESS_RESET) {
			printf("Warning: Top-level reset access '%s': Skipping board access '%s' model '%s'.\n",
				TopAcc->name, BoardAcc->name, BoardAcc->model->name
			);
			return -1;
		}
		netlist_access_single_port_t* accport_data = BoardAcc->data;
		if(accport_data->port->direction!=NETLIST_PORT_DIR_IN) {
			printf("Warning: Top-level reset access '%s': Skipping board access '%s' which has wrong direction.\n", TopAcc->name, BoardAcc->name);
			return -1;
		}
		LinkAccesses_Reset(TopAcc, BoardAcc);
	}

	int LinkAccesses_Wire(netlist_access_t* TopAcc, netlist_access_t* BoardAcc) {
		avl_pp_add_overwrite(&Implem->synth_target->impacc2boardacc, TopAcc, BoardAcc);
	}
	int LinkAccesses_Wire_check(netlist_access_t* TopAcc, netlist_access_t* BoardAcc) {
		netlist_access_single_port_t* top_single_data = TopAcc->data;
		if(BoardAcc->model!=TopAcc->model) {
			printf("Warning: Top-level access '%s' model '%s': Skipping board access '%s' model '%s'.\n",
				TopAcc->name, TopAcc->model->name, BoardAcc->name, BoardAcc->model->name
			);
			return -1;
		}
		netlist_access_single_port_t* board_single_data = BoardAcc->data;
		if(top_single_data->port->width!=board_single_data->port->width) {
			printf("Warning: Top-level access '%s' model '%s': Skipping access '%s' model '%s' (width mismatch).\n",
				TopAcc->name, TopAcc->model->name, BoardAcc->name, BoardAcc->model->name
			);
			return -1;
		}
		// Link the two accesses
		LinkAccesses_Wire(TopAcc, BoardAcc);
	}

	int LinkAccesses_Fifo(netlist_access_t* TopAcc, netlist_access_t* BoardAcc) {
		netlist_access_single_port_t* top_single_data = TopAcc->data;

		if(TopAcc->model==NETLIST_ACCESS_FIFO_IN) {

			if(BoardAcc->model==NETLIST_ACCESS_FIFO_IN) {
				netlist_access_single_port_t* board_single_data = BoardAcc->data;
				if(top_single_data->width!=board_single_data->width) {
					printf("Warning: Top-level access '%s' model '%s': Skipping access '%s' model '%s' (width mismatch).\n",
						TopAcc->name, TopAcc->model->name, BoardAcc->name, BoardAcc->model->name
					);
					return -1;
				}
				// Link the two accesses
				avl_pp_add_overwrite(&Implem->synth_target->impacc2boardacc, TopAcc, BoardAcc);
				return 0;
			}
			else if(BoardAcc->model==NETLIST_ACCESS_UART_RX) {
				// Create a component of model UART_RX
				char* name = Map_Netlist_MakeInstanceName(Implem, NETLIST_ACCESS_UART_RX);
				netlist_comp_t* comp = Netlist_Uart_Rx_New(name, top_single_data->width);
				Netlist_Comp_SetChild(Implem->netlist.top, comp);
				// Copy the config from the board uart interface
				netlist_uart_side_t* top_uart_data = comp->data;
				netlist_uart_side_cfg_t* board_uart_fcg = Netlist_Access_UartSide_GetCfg(BoardAcc);
				*top_uart_data->cfg = *board_uart_fcg;

				// Hard-link the FIFO ports together... Create intermediate signal components to do that...
				// Rename all occurrences of the ports/signals in Hier to later map in the right stuff...



				// Remove the top-level access, create another of model NETLIST_ACCESS_UART_RX with same name...
				// Link these...



				// FIXME TODO

			}
			else {
				printf("Warning: Top-level access '%s' model '%s': Incompatible board access '%s' model '%s'.\n",
					TopAcc->name, TopAcc->model->name, BoardAcc->name, BoardAcc->model->name
				);
				return -1;
			}

		}  // End Fifo in

		else {  // FIFO OUT

			if(BoardAcc->model==NETLIST_ACCESS_FIFO_OUT) {
				netlist_access_single_port_t* board_single_data = BoardAcc->data;
				if(top_single_data->width!=board_single_data->width) {
					printf("Warning: Top-level access '%s' model '%s': Skipping access '%s' model '%s' (width mismatch).\n",
						TopAcc->name, TopAcc->model->name, BoardAcc->name, BoardAcc->model->name
					);
					return -1;
				}
				// Link the two accesses
				avl_pp_add_overwrite(&Implem->synth_target->impacc2boardacc, TopAcc, BoardAcc);
				return 0;
			}
			else if(BoardAcc->model==NETLIST_ACCESS_UART_RX) {



				// FIXME TODO
			}
			else {
				printf("Warning: Top-level access '%s' model '%s': Incompatible board access '%s' model '%s'.\n",
					TopAcc->name, TopAcc->model->name, BoardAcc->name, BoardAcc->model->name
				);
				return -1;
			}

		}  // End Fifo out

		return -1;
	}

	// List all top-level accesses
	avl_pp_foreach(&Implem->netlist.top->accesses, scanAcc) {
		netlist_access_t* access = scanAcc->data;
		// Skip accesses that are already linked
		if(avl_pp_isthere(&Implem->synth_target->impacc2boardacc, access)==true) continue;
		// Bet the corresponding board access
		netlist_access_t* BoardAcc = Netlist_Comp_GetAccess(board_fpga->connect, TopAcc->name);
		if(BoardAcc==NULL) continue;

		// Sort per access model
		if     (access->model==NETLIST_ACCESS_CLOCK) {
			LinkAccesses_Clock_check(TopAcc, BoardAcc);
		}
		else if(access->model==NETLIST_ACCESS_RESET) {
			LinkAccesses_Reset_check(TopAcc, BoardAcc);
		}
		else if(access->model==NETLIST_ACCESS_WIRE_IN || access->model==NETLIST_ACCESS_WIRE_OUT) {
			LinkAccesses_Wire_check(TopAcc, BoardAcc);
		}
		else if(access->model==NETLIST_ACCESS_FIFO_IN || access->model==NETLIST_ACCESS_FIFO_OUT) {
			LinkAccesses_Fifo(TopAcc, BoardAcc);
		}
	}

	// Handle board defaults

	// A local utility fuction to find a unique access of a given model
	netlist_access_t* GetUniqueAccModel(netlist_access_model_t* model) {
		unsigned total_nb = 0
		netlist_access_t* TheAcc = NULL;
		avl_pp_foreach(&Implem->netlist.top->accesses, scanAcc) {
			netlist_access_t* access = scanAcc->data;
			if(access->model!=model) continue;
			if(total_nb > 1) return NULL;
			total_nb++;
			// Skip accesses that are already linked
			if(avl_pp_isthere(&Implem->synth_target->impacc2boardacc, access)==true) return NULL;
			// Store this access
			TheAcc = access;
		}
		return TheAcc;
	}

	// Process the clock port
	if(board_fpga->default_clock!=NULL) {
		netlist_access_t* TheClock = GetUniqueAccModel(NETLIST_ACCESS_CLOCK);
		if(TheClock!=NULL) {
			netlist_access_t* BoardAcc = board_fpga->default_clock;
			printf("Info: Linking the top-level clock '%s' to the default board clock '%s'.\n", TopAcc->name, BoardAcc->name);
			LinkAccesses_Clock(TopAcc, BoardAcc);
		}
	}

	// Process the reset port
	if(board_fpga->default_reset!=NULL) {
		netlist_access_t* TheReset = GetUniqueAccModel(NETLIST_ACCESS_Reset);
		if(TheReset!=NULL) {
			netlist_access_t* BoardAcc = board_fpga->default_reset;
			printf("Info: Linking the top-level clock '%s' to the default board clock '%s'.\n", TopAcc->name, BoardAcc->name);
			LinkAccesses_Clock(TopAcc, BoardAcc);
		}
	}

	// FIXME TODO: add stdin, stdout



	return 0;
}

#endif



//=====================================================================
// Connection of data interfaces between components
//=====================================================================

#define CONNECTACC_TOP   0x01
#define CONNECTACC_COMP  0x02

// This lists contains callback functions that connect two access models together
// TYPE is unused
// DATA_FROM = a bitype_list* that describes the situations it can handle
// DATA_TO = the callback function pointer
bitype_list* list_cb_connect_accmod = NULL;

void Hier_ConnectAccesses_AddCB(
	void* func,
	netlist_access_model_t* model1, unsigned flags1,
	netlist_access_model_t* model2, unsigned flags2
) {
	bitype_list* listSit = addbitype(NULL, flags1 | (flags2 << 2), model1, model2);
	list_cb_connect_accmod = addbitype(list_cb_connect_accmod, 0, listSit, func);
}
void Hier_ConnectAccesses_AddCB_SymProdRead(
	void* func,
	netlist_access_model_t* model_top_p, netlist_access_model_t* model_comp_p,
	netlist_access_model_t* model_top_r, netlist_access_model_t* model_comp_r
) {
	bitype_list* listSit = NULL;

	if(model_top_p!=NULL  && model_top_r!=NULL)  listSit = addbitype(listSit, CONNECTACC_TOP  | (CONNECTACC_TOP << 2),  model_top_p,  model_top_r);
	if(model_top_p!=NULL  && model_comp_r!=NULL) listSit = addbitype(listSit, CONNECTACC_TOP  | (CONNECTACC_COMP << 2), model_top_p,  model_comp_r);
	if(model_comp_p!=NULL && model_top_r!=NULL)  listSit = addbitype(listSit, CONNECTACC_COMP | (CONNECTACC_TOP << 2),  model_comp_p, model_top_r);
	if(model_comp_p!=NULL && model_comp_r!=NULL) listSit = addbitype(listSit, CONNECTACC_COMP | (CONNECTACC_COMP << 2), model_comp_p, model_comp_r);

	list_cb_connect_accmod = addbitype(list_cb_connect_accmod, 0, listSit, func);
}
void* Hier_ConnectAccesses_FindCB(implem_t* Implem, netlist_access_t* access1, netlist_access_t* access2) {
	unsigned flags1 = access1->component == Implem->netlist.top ? CONNECTACC_TOP : CONNECTACC_COMP;
	unsigned flags2 = access2->component == Implem->netlist.top ? CONNECTACC_TOP : CONNECTACC_COMP;

	foreach(list_cb_connect_accmod, scanCB) {
		bitype_list* list_situations = scanCB->DATA_FROM;
		foreach(list_situations, scanSit) {
			if(scanSit->DATA_FROM != access1->model || scanSit->DATA_TO != access2->model) continue;
			if( (scanSit->TYPE & flags1)==0 || ( (scanSit->TYPE >> 2) & flags2)==0 ) continue;
			return scanCB->DATA_TO;
		}
	}

	return NULL;
}

// Print a warning about a port source that is being overwritten
// And clean stuff
void ConnectPort_CheckEmptySource(netlist_port_t* port) {
	if(port->source==NULL) return;
	netlist_comp_t* comp = port->component;
	printf("Warning: Overwriting source of port '%s' of comp '%s' model '%s'.\n", port->name, comp->name, comp->model->name);
	Netlist_ValCat_FreeList(port->source);
	port->source = NULL;
}

static void ports_connect_prod2read_nocheck(netlist_port_t* port_prod, netlist_port_t* port_read) {
	port_read->source = Netlist_ValCat_MakeList_FullPort(port_prod);
	Netlist_PortTargets_SetSource(port_read);
}

// Situations: WIRE, producer port to reader port
// - Top WIRE_IN to Top WIRE_OUT
// - Top WIRE_IN to Comp WIRE_IN
// - Comp WIRE_OUT to Top WIRE_OUT
// - Comp WIRE_OUT to Comp WIRE_IN
static int Hier_ConnectAccesses_Wire_Prod2Read(implem_t* Implem, netlist_access_t* access1, netlist_access_t* access2) {
	netlist_access_single_port_t* prod_data = access1->data;
	netlist_access_single_port_t* read_data = access2->data;

	// Checks
	if(prod_data->port->width != read_data->port->width) {
		printf("Error: Can't connect accesses '%s' and '%s': width mismatch.\n", access1->name, access2->name);
		return 1;
	}
	ConnectPort_CheckEmptySource(read_data->port);

	// Do the connection
	ports_connect_prod2read_nocheck(prod_data->port, read_data->port);

	return 0;
}
// Situations: FIFO, producer to reader
// - Top FIFO_IN to Top FIFO_OUT
// - Top FIFO_IN to Comp FIFO_IN
// - Comp FIFO_OUT to Top FIFO_OUT
// - Comp FIFO_OUT to Comp FIFO_IN
static int Hier_ConnectAccesses_Fifo_Prod2Read(implem_t* Implem, netlist_access_t* access1, netlist_access_t* access2) {
	netlist_access_fifo_t* fifo_prod_data = access1->data;
	netlist_access_fifo_t* fifo_read_data = access2->data;

	// Checks
	if(fifo_prod_data->width != fifo_read_data->width) {
		printf("Error: Can't connect accesses '%s' and '%s': width mismatch.\n", access1->name, access2->name);
		return 1;
	}

	if(access1->component == Implem->netlist.top) {
		ConnectPort_CheckEmptySource(fifo_prod_data->port_rdy);
	}
	else {
		ConnectPort_CheckEmptySource(fifo_prod_data->port_ack);
	}

	if(access2->component == Implem->netlist.top) {
		ConnectPort_CheckEmptySource(fifo_read_data->port_rdy);
	}
	else {
		ConnectPort_CheckEmptySource(fifo_read_data->port_ack);
	}

	ConnectPort_CheckEmptySource(fifo_read_data->port_data);

	// Do the connections

	ports_connect_prod2read_nocheck(fifo_prod_data->port_data, fifo_read_data->port_data);

	if(access1->component == Implem->netlist.top && access2->component != Implem->netlist.top) {
		// Connect ack on ack, rdy on rdy
		ports_connect_prod2read_nocheck(fifo_read_data->port_rdy, fifo_prod_data->port_rdy);
		ports_connect_prod2read_nocheck(fifo_prod_data->port_ack, fifo_read_data->port_ack);
	}
	else if(access1->component != Implem->netlist.top && access2->component == Implem->netlist.top) {
		// Connect ack on ack, rdy on rdy
		ports_connect_prod2read_nocheck(fifo_prod_data->port_rdy, fifo_read_data->port_rdy);
		ports_connect_prod2read_nocheck(fifo_read_data->port_ack, fifo_prod_data->port_ack);
	}
	else if(access1->component == Implem->netlist.top && access2->component == Implem->netlist.top) {
		// Both are top-level: Connect ack -> rdy
		ports_connect_prod2read_nocheck(fifo_prod_data->port_ack, fifo_read_data->port_rdy);
		ports_connect_prod2read_nocheck(fifo_read_data->port_ack, fifo_prod_data->port_rdy);
	}
	else if(access1->component != Implem->netlist.top && access2->component != Implem->netlist.top) {
		// Both are comps: Connect rdy -> ack
		ports_connect_prod2read_nocheck(fifo_prod_data->port_rdy, fifo_read_data->port_ack);
		ports_connect_prod2read_nocheck(fifo_read_data->port_rdy, fifo_prod_data->port_ack);
	}
	else {
		abort();
	}

	return 0;
}
// Situations: create an interface UART_RX
// - Top UART_RX to Top FIFO_OUT
// - Top UART_RX to Comp FIFO_IN
// - Comp UART_TX to Top FIFO_OUT
// - Comp UART_TX to Comp FIFO_IN
static int Hier_ConnectAccesses_UartRx_Prod2Read(implem_t* Implem, netlist_access_t* access1, netlist_access_t* access2) {
	netlist_access_uart_side_t* uartacc_data = access1->data;
	netlist_access_fifo_t* fifo_data = access2->data;

	//dbgprintf("FIFO width = %u dataport width = %u\n", fifo_data->width, fifo_data->port_data->width);

	// Checks
	ConnectPort_CheckEmptySource(fifo_data->port_data);
	ConnectPort_CheckEmptySource(fifo_data->port_ack);

	// Create the UART_RX component
	char* comp_name = Map_Netlist_MakeInstanceName(Implem, NETLIST_COMP_UART_RX);
	netlist_comp_t* comp = Netlist_Uart_Rx_New(comp_name, fifo_data->port_data->width);
	Netlist_Comp_SetChild(Implem->netlist.top, comp);

	// Copy config

	netlist_uart_side_t*        compuart_data = comp->data;
	netlist_access_uart_side_t* compuartacc_data = compuart_data->access_uart->data;

	*(compuart_data->cfg) = uartacc_data->cfg;

	// Do the connections

	// Uart access of the comp
	ports_connect_prod2read_nocheck(uartacc_data->port_uart, compuartacc_data->port_uart);

	// FIFO access of the comp
	Hier_ConnectAccesses_Fifo_Prod2Read(Implem, compuart_data->access_fifo, access2);

	return 0;
}
// Ditto with Tx : FIFO to UART
static int Hier_ConnectAccesses_UartTx_Prod2Read(implem_t* Implem, netlist_access_t* access1, netlist_access_t* access2) {
	netlist_access_fifo_t* fifo_data = access1->data;
	netlist_access_uart_side_t* uartacc_data = access2->data;

	//dbgprintf("FIFO width = %u dataport width = %u\n", fifo_data->width, fifo_data->port_data->width);

	// Checks
	ConnectPort_CheckEmptySource(fifo_data->port_ack);

	// Create the UART_TX component
	char* comp_name = Map_Netlist_MakeInstanceName(Implem, NETLIST_COMP_UART_TX);
	netlist_comp_t* comp = Netlist_Uart_Tx_New(comp_name, fifo_data->port_data->width);
	Netlist_Comp_SetChild(Implem->netlist.top, comp);

	// Copy config

	netlist_uart_side_t*        compuart_data = comp->data;
	netlist_access_uart_side_t* compuartacc_data = compuart_data->access_uart->data;

	*(compuart_data->cfg) = uartacc_data->cfg;

	// Do the connections

	// Uart access of the comp
	ports_connect_prod2read_nocheck(compuartacc_data->port_uart, uartacc_data->port_uart);

	// FIFO access of the comp
	Hier_ConnectAccesses_Fifo_Prod2Read(Implem, access1, compuart_data->access_fifo);

	return 0;
}

// Situations: memRA, producer to reader (point of wiew is addess port)
// - Top RA to Top RA_EXT
// - Top RA to Comp RA
// - Comp RA_EXT to Top RA_EXT
// - Comp RA_EXT to Comp RA
static int Hier_ConnectAccesses_MemRA_Prod2Read(implem_t* Implem, netlist_access_t* access1, netlist_access_t* access2) {
	netlist_access_mem_addr_t* prod_data = access1->data;
	netlist_access_mem_addr_t* read_data = access2->data;

	// Checks
	if(prod_data->port_d->width != read_data->port_d->width) {
		printf("Error: Can't connect accesses '%s' and '%s': data width mismatch.\n", access1->name, access2->name);
		return 1;
	}
	if(prod_data->port_a->width != read_data->port_a->width) {
		printf("Error: Can't connect accesses '%s' and '%s': address width mismatch.\n", access1->name, access2->name);
		return 1;
	}

	ConnectPort_CheckEmptySource(prod_data->port_d);
	ConnectPort_CheckEmptySource(read_data->port_a);

	// Do the connections

	ports_connect_prod2read_nocheck(prod_data->port_a, read_data->port_a);
	ports_connect_prod2read_nocheck(read_data->port_d, prod_data->port_d);

	return 0;
}
// Ditto for memWA, producer to reader (point of wiew is addess port)
static int Hier_ConnectAccesses_MemWA_Prod2Read(implem_t* Implem, netlist_access_t* access1, netlist_access_t* access2) {
	netlist_access_mem_addr_t* prod_data = access1->data;
	netlist_access_mem_addr_t* read_data = access2->data;

	// Checks
	if(prod_data->port_d->width != read_data->port_d->width) {
		printf("Error: Can't connect accesses '%s' and '%s': data width mismatch.\n", access1->name, access2->name);
		return 1;
	}
	if(prod_data->port_a->width != read_data->port_a->width) {
		printf("Error: Can't connect accesses '%s' and '%s': address width mismatch.\n", access1->name, access2->name);
		return 1;
	}

	ConnectPort_CheckEmptySource(prod_data->port_d);
	ConnectPort_CheckEmptySource(read_data->port_a);
	ConnectPort_CheckEmptySource(read_data->port_e);

	// Do the connections

	ports_connect_prod2read_nocheck(prod_data->port_a, read_data->port_a);
	ports_connect_prod2read_nocheck(prod_data->port_d, read_data->port_d);
	ports_connect_prod2read_nocheck(prod_data->port_e, read_data->port_e);

	return 0;
}

// Situations: DiffClock, producer to reader
// Only handles Top to Comp
static int Hier_ConnectAccesses_DiffClock_Prod2Read(implem_t* Implem, netlist_access_t* access1, netlist_access_t* access2) {
	netlist_access_diffclock_t* prod_data = access1->data;
	netlist_access_diffclock_t* read_data = access2->data;

	// Checks

	ConnectPort_CheckEmptySource(read_data->port_p);
	ConnectPort_CheckEmptySource(read_data->port_n);

	// Do the connections

	ports_connect_prod2read_nocheck(prod_data->port_p, read_data->port_p);
	ports_connect_prod2read_nocheck(prod_data->port_n, read_data->port_n);

	return 0;
}


// Connection between two accesses, from child component or top-level
// Return zero if success
static int Hier_ConnectAccesses(implem_t* Implem, netlist_access_t* access1, netlist_access_t* access2) {

	void* voidfunc = Hier_ConnectAccesses_FindCB(Implem, access1, access2);
	if(voidfunc==NULL) {
		Swap(access1, access2);
		voidfunc = Hier_ConnectAccesses_FindCB(Implem, access1, access2);
	}
	if(voidfunc==NULL) {
		printf("Error: Don't known how to connect accesses '%s' model '%s' from comp '%s' and '%s' model '%s' from comp '%s'.\n",
			access1->name, access1->model->name, access1->component->name,
			access2->name, access2->model->name, access2->component->name
		);
		return -1;
	}

	int (*func)(implem_t*, netlist_access_t*, netlist_access_t*) = voidfunc;

	return func(Implem, access1, access2);
}



//=====================================================================
// Detect the top-level functions and create associated Implem models
//=====================================================================

// Insert Read buffers to large memories to make Read synchronous
// This reveals implementation into hard RAM blocks of the FPGA
static int Build_PostLoad_MemSyncRead(implem_t* Implem) {
	if(Implem->H==NULL) return 0;
	if(Implem->env.auto_mem_readbuf==false) return 0;

	// Use the techno callback to get the Mem components where adding a read buffer is needed/appropriate
	techno_t* techno = Implem->synth_target->techno;
	if(techno==NULL) return 0;
	if(techno->listmem_readbuf==NULL) return 0;

	// Get list of candidate memory components
	chain_list* list_mem = techno->listmem_readbuf(Implem);
	if(list_mem==NULL) return 0;

	// For each memory component, create one Register that will systematically be used to read from the memory
	foreach(list_mem, scanComp) {
		netlist_comp_t* comp = scanComp->DATA;
		netlist_memory_t* mem_data = comp->data;

		// Some common safety checks
		if(mem_data->ports_cam_nb != 0) continue;
		if(mem_data->ports_wa_nb > 1) continue;
		if(mem_data->direct_en==true) continue;

		dbgprintf("Inserting Read buffer on memory '%s'\n", comp->name);

		// Launch insertion
		int z = Mem_InsertReadBuf(Implem, comp);
		if(z!=0) {
			errprintf("Inserting read buffer on mem component '%s' failed (code %i)\n", comp->name, z);
			abort();
		}

	}  // Scan mem components

	// Clean
	freechain(list_mem);

	#if 0  // To check the functionality that finds mem read buffers

	avl_pp_foreach(&Implem->netlist.top->children, scanComp) {
		netlist_comp_t* comp = scanComp->data;
		if(comp->model != NETLIST_COMP_MEMORY) continue;
		netlist_comp_t* compReg = Mem_GetReadBuf_HierOnly(Implem, comp);
		if(compReg!=NULL) {
			dbgprintf("Read buffer '%s' found on memory '%s'\n", compReg->name, comp->name);
		}
	}

	#endif

	return 0;
}

// Note: This function is called after loading & replacement of builtin functions

/* About shared registers and memories

Let TL1 and TL2 be two top-level ImpMod created after load.
These 2 ImpMod are instantiated in a parent ImpMod, TL0.

About registers
Only ONE top-level ImpMod can write to a register. The others can only read from it.
So a Reg is owned by the ImpMod that writes to it.
However, the registers for input buffers are kept in TL0.

About memories
A memory component can have several R and W ports
So Mem comps are not owned by an ImpMod the way Reg are.
Mem comps are owned by the parent ImpMod TL0.

About memories only written in one Implem
The problem is about the INLINE transformation:
if TL1 owns the mem, and it is read by TL2 only,
then the cost of adding R ports to the mem will be in TL1 but the speedup in TL2.
This would be a nonsense so memory components are always owned by the parent ImpMod.

*/

// Create wait-on-start port and wait loops if needed
static int Build_PostLoad_CheckCreateSigStart(implem_t* Implem) {
	if(Implem->env.wait_on_start==false) return 0;

	// Create signal and top-level port for start
	Map_Netlist_CreateSigStart_check(Implem);

	// Create the waiting loop
	if(Implem->H!=NULL) {
		hier_node_t* nodeLoop = Hier_Node_NewType(HIERARCHY_LOOP);
		hier_loop_t* loop_data = &nodeLoop->NODE.LOOP;
		loop_data->testinfo->cond = hvex_newnot(hvex_newvec_bit(Implem->netlist.sig_start->name));
		// Insert the loop in the Hier
		Hier_Lists_AddLevel(Implem->H, nodeLoop);
		Hier_InsertLevelAfterNode(Implem->H->PROCESS, nodeLoop);
	}

	return 0;
}

// Extract a nodeProc from orig Implem and create a new modImplem
static implem_t* Build_DetectTopLevels_Proc2Implem(implem_t* Implem, hier_node_t* nodeProc) {
	hier_proc_t* proc_data = &nodeProc->NODE.PROCESS;

	// Create an ImpMod with the process name
	implem_t* modImplem = augh_implem_new(Implem->env.globals);
	modImplem->compmod.model_name = proc_data->name_orig;
	modImplem->env.input_file = Implem->env.input_file;
	modImplem->env.wait_on_start = Implem->env.wait_on_start;

	// Some fixes about component names
	modImplem->env.vhdl_prefix = proc_data->name_orig;
	modImplem->env.entity = proc_data->name_orig;
	modImplem->netlist.top->name = proc_data->name_orig;
	modImplem->netlist.top->flags |= NETLIST_COMP_NOPREFIX;

	// Index in the list of component models
	avl_pp_add_overwrite(&Implem->env.globals->implem_models, modImplem->compmod.model_name, modImplem);

	// Copy misc pointers
	modImplem->synth_target = Implem->synth_target;

	// Duplicate all Hier

	// Initialize the structure linking the new and old Implem (only needed for Hier_Dup)
	modImplem->DUP.father = Implem;
	avl_p_add_keep(&Implem->DUP.children, modImplem, modImplem);

	Hier_Dup(modImplem);
	modImplem->H->PROCESS = Hier_GetProc(modImplem->H, proc_data->name_orig);
	assert(modImplem->H->PROCESS!=NULL);

	// Remove the Parent fields, not used anymore
	avl_p_rem_key(&Implem->DUP.children, modImplem);
	avl_pp_reset(&modImplem->DUP.pointers_old2new);
	avl_pp_reset(&modImplem->DUP.pointers_new2old);
	modImplem->DUP.father = NULL;

	// Remove all what's not used in modImplem by its top-level process only
	Hier_RemoveUnusedNodes(modImplem->H);

	// Remove the current nodeProc in Implem (small optim to avoid duplicating it everywhere)
	Hier_RemoveProc(Implem->H, nodeProc);

	return modImplem;
}

// A data structure useful to processing symbols

static void Build_SymTops_DataInit(build_symtops_data_t* data) {
	avl_pp_init(&data->tree_toplevels);
	avl_pp_init(&data->tree_imp2comp);
	avl_p_init(&data->tree_avoidsym);
	avl_pp_init(&data->tree_syms_orig);
	avl_pp_init(&data->tree_syms_acc);
	avl_pp_init(&data->tree_syms_done);
}

static void Build_SymTops_FreeImpTree(avl_pp_tree_t* tree_impoccur) {
	avl_pp_foreach(tree_impoccur, scanImp) freechain(scanImp->data);
	avl_pp_reset(tree_impoccur);
	free(tree_impoccur);
}
static void Build_SymTops_DataClear(build_symtops_data_t* data) {
	avl_pp_reset(&data->tree_toplevels);
	avl_pp_reset(&data->tree_imp2comp);
	avl_p_reset(&data->tree_avoidsym);

	avl_pp_foreach(&data->tree_syms_orig, scanSym) Build_SymTops_FreeImpTree(scanSym->data);
	avl_pp_reset(&data->tree_syms_orig);

	avl_pp_foreach(&data->tree_syms_acc, scanSym) Build_SymTops_FreeImpTree(scanSym->data);
	avl_pp_reset(&data->tree_syms_acc);

	avl_pp_foreach(&data->tree_syms_done, scanSym) Build_SymTops_FreeImpTree(scanSym->data);
	avl_pp_reset(&data->tree_syms_done);
}

static void Build_SymTops_MoveOrig2Done(build_symtops_data_t* data, avl_pp_node_t* pp_node) {
	char* name = pp_node->key;
	void* treeimp = pp_node->data;
	avl_pp_rem(&data->tree_syms_orig, pp_node);
	avl_pp_add_overwrite(&data->tree_syms_done, name, treeimp);
}
static void Build_SymTops_MoveOrig2Acc(build_symtops_data_t* data, avl_pp_node_t* pp_node) {
	char* name = pp_node->key;
	void* treeimp = pp_node->data;
	avl_pp_rem(&data->tree_syms_orig, pp_node);
	avl_pp_add_overwrite(&data->tree_syms_acc, name, treeimp);
}
static void Build_SymTops_MoveAcc2Done(build_symtops_data_t* data, avl_pp_node_t* pp_node) {
	char* name = pp_node->key;
	void* treeimp = pp_node->data;
	avl_pp_rem(&data->tree_syms_acc, pp_node);
	avl_pp_add_overwrite(&data->tree_syms_done, name, treeimp);
}

static netlist_comp_t* Build_SymTops_Imp2Comp(build_symtops_data_t* data, implem_t* modImplem) {
	avl_pp_node_t* pp_node = NULL;
	if(avl_pp_find(&data->tree_imp2comp, modImplem, &pp_node)==false) abort();
	return pp_node->data;
}

// Functions to move components and create new ports with appropriate connections

// Note: This function assumes that the CANDUP flag is present
// Note: In case of error, it prints a message, no need to add one.
static int Build_DetectTopLevels_DupComp(build_symtops_data_t* data, avl_pp_node_t* pp_node_orig, netlist_comp_t* comp, bool hasconn) {
	int error_code = 0;

	if(hasconn==true) {
		printf("Warning: Comp '%s' model '%s' has CANDUP flag but can't duplicate it because it is connected.\n",
			comp->name, comp->model->name
		);
		error_code = __LINE__;
		goto EXIT_POINT;
	}
	if(Implem_SymFlag_ChkAnyProtect(data->Implem, comp->name)==true) {
		printf("Warning: Comp '%s' model '%s' has CANDUP flag but can't move it because of protection flags: ",
			comp->name, comp->model->name
		);
		Implem_SymFlag_Print(data->Implem, comp->name);
		printf("\n");
		error_code = __LINE__;
		goto EXIT_POINT;
	}

	avl_pp_tree_t* tree_impoccur = pp_node_orig->data;

	avl_pp_foreach(tree_impoccur, scanImp) {
		implem_t* modImplem = scanImp->key;
		// Note: the list of occurrences is not useful here
		if(Map_Netlist_GetComponent(modImplem, comp->name)!=NULL) {
			printf("Warning: Can't duplicate comp '%s' model '%s' in Implem '%s' because a component with same name exists.\n",
				comp->name, comp->model->name, modImplem->compmod.model_name
			);
			error_code = __LINE__;
			goto EXIT_POINT;
		}
		netlist_comp_t* dupcomp = Netlist_Comp_Dup(comp);
		Netlist_Comp_SetChild(modImplem->netlist.top, dupcomp);
	}

	// Remove the original component
	Netlist_Comp_Free(comp);

	EXIT_POINT:

	Build_SymTops_MoveOrig2Done(data, pp_node_orig);

	return error_code;
}

// Functions to move components

static int Build_DetectTopLevels_MoveSig(build_symtops_data_t* data, avl_pp_node_t* pp_node_orig, netlist_comp_t* comp) {
	netlist_signal_t* sig_data = comp->data;

	bool hasconn = Netlist_Comp_HasConnections(comp);
	bool hasconn_write = false;
	if(hasconn==true) {
		if(sig_data->port_in->source!=NULL) hasconn_write = true;
	}

	// Duplicate the component if flags allow that
	if(Implem_SymFlag_Chk(data->Implem, comp->name, SYM_FLAG_CANDUP)==true) {
		return Build_DetectTopLevels_DupComp(data, pp_node_orig, comp, hasconn);
	}

	int error_code = 0;
	avl_pp_tree_t* tree_impoccur = pp_node_orig->data;
	unsigned nbimp = avl_pp_count(tree_impoccur);

	// If only one modImp uses this comp, and it has no existing connections, just move it
	if(nbimp==1 && hasconn==false) {
		implem_t* modImplem = tree_impoccur->root->key;
		if(Implem_SymFlag_ChkAnyProtect(data->Implem, comp->name)==true) {
			printf("Error: Not moving comp '%s' model '%s' into ImpMod '%s' because of protection flags: ",
				comp->name, comp->model->name, modImplem->compmod.model_name
			);
			Implem_SymFlag_Print(data->Implem, comp->name);
			printf("\n");
			error_code = __LINE__;
			goto EXIT_POINT;
		}
		assert(Map_Netlist_GetComponent(modImplem, comp->name)==NULL);
		Netlist_Comp_UnlinkChild(comp);
		Netlist_Comp_SetChild(modImplem->netlist.top, comp);
		goto EXIT_POINT;
	}

	implem_t* writeImplem = NULL;
	netlist_port_t* srcport = NULL;  // The source port for all new top-level input ports of Implems

	// If connected write-side, the comp is kept there.
	if(hasconn_write==true) {
		srcport = sig_data->port_out;
	}
	else {

		// Find the Implem that writes to it
		avl_pp_foreach(tree_impoccur, scanImp) {
			implem_t* modImplem = scanImp->key;
			chain_list* list_occur = scanImp->data;
			foreach(list_occur, scanOcur) {
				hvex_t* Expr = scanOcur->DATA;
				if(hvex_IsWriteOperation(Expr)==true) {
					if(writeImplem!=NULL) {
						printf("Error: Sig '%s' is written by at least Implems '%s' and '%s'.\n",
							comp->name, writeImplem->compmod.model_name, modImplem->compmod.model_name
						);
						error_code = __LINE__;
						goto EXIT_POINT;
					}
					writeImplem = modImplem;
					break;
				}
			}
		}
		if(writeImplem==NULL) {
			printf("Error: Sig '%s' is not written by any Implems.\n", comp->name);
			error_code = __LINE__;
			goto EXIT_POINT;
		}

		// Create a top-level output port in the component that writes to the comp
		netlist_port_t* srcport_mod = Netlist_Comp_GetPort(writeImplem->netlist.top, comp->name);
		if(srcport_mod!=NULL) {
			printf("Error: Sig '%s' is written by Implem '%s' but it already has a port with that name.\n",
				comp->name, writeImplem->compmod.model_name
			);
			error_code = __LINE__;
			goto EXIT_POINT;
		}

		// Create a top-level output port in the Implem
		srcport_mod = Netlist_Comp_AddPort_Out(writeImplem->netlist.top, comp->name, sig_data->width);
		// Create the mirror port in the instanciation component
		netlist_comp_t* topinst = Build_SymTops_Imp2Comp(data, writeImplem);
		srcport = Netlist_Comp_AddPort_Out(topinst, comp->name, sig_data->width);

		// In case the comp is connected read-side, update the source of the targets that read the comp
		Netlist_PortTargets_ReplaceOutByPort(sig_data->port_out, srcport);

		// Move the component to the Implem that write to it
		assert(Map_Netlist_GetComponent(writeImplem, comp->name)==NULL);
		Netlist_Comp_UnlinkChild(comp);
		Netlist_Comp_SetChild(writeImplem->netlist.top, comp);

		// Connect the comp to the new output port of its new Implem
		srcport_mod->source = Netlist_ValCat_MakeList_FullPort(sig_data->port_out);
		Netlist_PortTargets_SetSource(srcport_mod);

	}  // End of section that moves the comp to the Implem that writes to it

	// Add input ports to all ImpMods that only read it
	avl_pp_foreach(tree_impoccur, scanImp) {
		implem_t* modImplem = scanImp->key;
		if(writeImplem==modImplem) continue;
		// If needed, ensure that this Implem only read it
		if(writeImplem==NULL) {
			chain_list* list_occur = scanImp->data;
			foreach(list_occur, scanOcur) {
				hvex_t* Expr = scanOcur->DATA;
				if(hvex_IsWriteOperation(Expr)==true) {
					printf("Error: Sig '%s' is connected read-side but Implem '%s' doesn't only read it.\n",
						comp->name, modImplem->compmod.model_name
					);
					error_code = __LINE__;
					goto EXIT_POINT;
				}
			}
		}
		// Here the check passed: the modImplem only reads the register
		// Check that no port already exists
		netlist_port_t* port_mod = Netlist_Comp_GetPort(modImplem->netlist.top, comp->name);
		if(port_mod!=NULL) {
			printf("Error: Sig '%s' is read by Implem '%s' but it already has a port with that name.\n",
				comp->name, modImplem->compmod.model_name
			);
			error_code = __LINE__;
			goto EXIT_POINT;
		}
		// Create a top-level input port in the Implem
		port_mod = Netlist_Comp_AddPort_In(modImplem->netlist.top, comp->name, sig_data->width);
		// Create an input port in the instantiation component
		netlist_comp_t* topinst = Build_SymTops_Imp2Comp(data, modImplem);
		netlist_port_t* port = Netlist_Comp_AddPort_In(topinst, comp->name, sig_data->width);
		// Connect it to the right port (reg comp or output port or Implem)
		port->source = Netlist_ValCat_MakeList_FullPort(srcport);
		Netlist_PortTargets_SetSource(port);
	}

	// Here the comp had been properly processed

	EXIT_POINT:

	Build_SymTops_MoveOrig2Done(data, pp_node_orig);

	return error_code;
}

static int Build_DetectTopLevels_MoveReg(build_symtops_data_t* data, avl_pp_node_t* pp_node_orig, netlist_comp_t* comp) {
	netlist_register_t* reg_data = comp->data;

	bool hasconn = Netlist_Comp_HasConnections(comp);
	bool hasconn_write = false;
	if(hasconn==true) {
		if(reg_data->port_in->source!=NULL || (reg_data->port_en!=NULL && reg_data->port_en->source!=NULL)) {
			hasconn_write = true;
		}
	}

	// Duplicate the component if flags allow that
	if(Implem_SymFlag_Chk(data->Implem, comp->name, SYM_FLAG_CANDUP)==true) {
		return Build_DetectTopLevels_DupComp(data, pp_node_orig, comp, hasconn);
	}

	int error_code = 0;
	avl_pp_tree_t* tree_impoccur = pp_node_orig->data;
	unsigned nbimp = avl_pp_count(tree_impoccur);

	// If only one modImp uses this comp, and it has no existing connections, just move it
	if(nbimp==1 && hasconn==false) {
		implem_t* modImplem = tree_impoccur->root->key;
		if(Implem_SymFlag_ChkAnyProtect(data->Implem, comp->name)==true) {
			printf("Error: Not moving comp '%s' model '%s' into ImpMod '%s' because of protection flags: ",
				comp->name, comp->model->name, modImplem->compmod.model_name
			);
			Implem_SymFlag_Print(data->Implem, comp->name);
			printf("\n");
			error_code = __LINE__;
			goto EXIT_POINT;
		}
		assert(Map_Netlist_GetComponent(modImplem, comp->name)==NULL);
		Netlist_Comp_UnlinkChild(comp);
		Netlist_Comp_SetChild(modImplem->netlist.top, comp);
		goto EXIT_POINT;
	}

	implem_t* writeImplem = NULL;
	netlist_port_t* srcport = NULL;  // The source port for all new top-level input ports of Implems

	// If connected write-side, the comp is kept there.
	if(hasconn_write==true) {
		srcport = reg_data->port_out;
	}
	else {

		// Find the Implem that writes to it
		avl_pp_foreach(tree_impoccur, scanImp) {
			implem_t* modImplem = scanImp->key;
			chain_list* list_occur = scanImp->data;
			foreach(list_occur, scanOcur) {
				hvex_t* Expr = scanOcur->DATA;
				if(hvex_IsWriteOperation(Expr)==true) {
					if(writeImplem!=NULL) {
						printf("Error: Reg '%s' is written by at least Implems '%s' and '%s'.\n",
							comp->name, writeImplem->compmod.model_name, modImplem->compmod.model_name
						);
						error_code = __LINE__;
						goto EXIT_POINT;
					}
					writeImplem = modImplem;
					break;
				}
			}
		}
		if(writeImplem==NULL) {
			printf("Error: Reg '%s' is not written by any Implems.\n", comp->name);
			error_code = __LINE__;
			goto EXIT_POINT;
		}

		// Create a top-level output port in the component that writes to the comp
		netlist_port_t* srcport_mod = Netlist_Comp_GetPort(writeImplem->netlist.top, comp->name);
		if(srcport_mod!=NULL) {
			printf("Error: Reg '%s' is written by Implem '%s' but it already has a port with that name.\n",
				comp->name, writeImplem->compmod.model_name
			);
			error_code = __LINE__;
			goto EXIT_POINT;
		}

		// Create a top-level output port in the Implem
		srcport_mod = Netlist_Comp_AddPort_Out(writeImplem->netlist.top, comp->name, reg_data->width);
		// Create the mirror port in the instanciation component
		netlist_comp_t* topinst = Build_SymTops_Imp2Comp(data, writeImplem);
		srcport = Netlist_Comp_AddPort_Out(topinst, comp->name, reg_data->width);

		// In case the comp is connected read-side, update the source of the targets that read the comp
		Netlist_PortTargets_ReplaceOutByPort(reg_data->port_out, srcport);

		// Move the component to the Implem that write to it
		assert(Map_Netlist_GetComponent(writeImplem, comp->name)==NULL);
		Netlist_Comp_UnlinkChild(comp);
		Netlist_Comp_SetChild(writeImplem->netlist.top, comp);

		// Connect the comp to the new output port of its new Implem
		srcport_mod->source = Netlist_ValCat_MakeList_FullPort(reg_data->port_out);
		Netlist_PortTargets_SetSource(srcport_mod);

	}  // End of section that moves the comp to the Implem that writes to it

	// Add input ports to all ImpMods that only read it
	avl_pp_foreach(tree_impoccur, scanImp) {
		implem_t* modImplem = scanImp->key;
		if(writeImplem==modImplem) continue;
		// If needed, ensure that this Implem only read it
		if(writeImplem==NULL) {
			chain_list* list_occur = scanImp->data;
			foreach(list_occur, scanOcur) {
				hvex_t* Expr = scanOcur->DATA;
				if(hvex_IsWriteOperation(Expr)==true) {
					printf("Error: Reg '%s' is connected read-side but Implem '%s' doesn't only read it.\n",
						comp->name, modImplem->compmod.model_name
					);
					error_code = __LINE__;
					goto EXIT_POINT;
				}
			}
		}
		// Here the check passed: the modImplem only reads the register
		// Check that no port already exists
		netlist_port_t* port_mod = Netlist_Comp_GetPort(modImplem->netlist.top, comp->name);
		if(port_mod!=NULL) {
			printf("Error: Reg '%s' is read by Implem '%s' but it already has a port with that name.\n",
				comp->name, modImplem->compmod.model_name
			);
			error_code = __LINE__;
			goto EXIT_POINT;
		}
		// Create a top-level input port in the Implem
		port_mod = Netlist_Comp_AddPort_In(modImplem->netlist.top, comp->name, reg_data->width);
		// Create an input port in the instantiation component
		netlist_comp_t* topinst = Build_SymTops_Imp2Comp(data, modImplem);
		netlist_port_t* port = Netlist_Comp_AddPort_In(topinst, comp->name, reg_data->width);
		// Connect it to the right port (reg comp or output port or Implem)
		port->source = Netlist_ValCat_MakeList_FullPort(srcport);
		Netlist_PortTargets_SetSource(port);
	}

	// Here the comp had been properly processed

	EXIT_POINT:

	Build_SymTops_MoveOrig2Done(data, pp_node_orig);

	return error_code;
}

static int Build_DetectTopLevels_MoveMem(build_symtops_data_t* data, avl_pp_node_t* pp_node_orig, netlist_comp_t* comp) {
	bool hasconn = Netlist_Comp_HasConnections(comp);

	// Duplicate the component if flags allow that
	if(Implem_SymFlag_Chk(data->Implem, comp->name, SYM_FLAG_CANDUP)==true) {
		return Build_DetectTopLevels_DupComp(data, pp_node_orig, comp, hasconn);
	}

	int error_code = 0;
	avl_pp_tree_t* tree_impoccur = pp_node_orig->data;
	unsigned nbimp = avl_pp_count(tree_impoccur);

	// If only one modImp uses this comp, and it has no existing connections, move it
	if(nbimp==1 && hasconn==false) {
		implem_t* modImplem = tree_impoccur->root->key;
		assert(Map_Netlist_GetComponent(modImplem, comp->name)==NULL);
		Netlist_Comp_UnlinkChild(comp);
		Netlist_Comp_SetChild(modImplem->netlist.top, comp);
		goto EXIT_POINT;
	}

	netlist_memory_t* mem_data = comp->data;

	// Here, the component stays there
	// Create RA and WA ports to all impModels that use it
	// Warning: Each Implem is allowed to do only reads or only writes, and only one port is created
	avl_pp_foreach(tree_impoccur, scanImp) {
		implem_t* modImplem = scanImp->key;
		chain_list* list_occur = scanImp->data;
		bool does_read = false;
		bool does_write = false;
		foreach(list_occur, scanOcur) {
			hvex_t* Expr = scanOcur->DATA;
			if(hvex_IsWriteOperation(Expr)==true) does_write = true;
			else does_read = true;
		}
		if(does_read==true && does_write==true) {
			printf("Warning: Mem '%s' is written and read by Implem '%s'. No connection created.\n",
				comp->name, modImplem->compmod.model_name
			);
			error_code = __LINE__;
			goto EXIT_POINT;
		}
		// Get the instantiation component
		netlist_comp_t* topinst = Build_SymTops_Imp2Comp(data, modImplem);
		// Create accesses in mem and Implem
		if(does_read==true) {
			// Create access in mem component
			netlist_access_t* memacc = Netlist_Mem_AddAccess_RA(comp);
			// Create access in modImplem
			netlist_access_t* impacc_mod = Netlist_AddAccess_RA_Ext(
				modImplem->netlist.top, mem_data->data_width, mem_data->addr_width, comp->name
			);
			// Create the mirror access in instantiation component
			netlist_access_t* impacc = Netlist_Access_Dup(impacc_mod, topinst, NULL);
			// Connect accesses in mem and instantiation component
			int z = Hier_ConnectAccesses(data->Implem, memacc, impacc);
			if(z!=0) {
				printf("Warning: Couldn't connect memRA accesses between mem '%s' and Implem '%s'.\n",
					comp->name, modImplem->compmod.model_name
				);
				error_code = __LINE__;
				goto EXIT_POINT;
			}
		}
		if(does_write==true) {
			// Create access in mem component
			netlist_access_t* memacc = Netlist_Mem_AddAccess_WA(comp);
			// Create access in modImplem
			netlist_access_t* impacc_mod = Netlist_AddAccess_WA_Ext(
				modImplem->netlist.top, mem_data->data_width, mem_data->addr_width, comp->name
			);
			// Create the mirror access in instantiation component
			netlist_access_t* impacc = Netlist_Access_Dup(impacc_mod, topinst, NULL);
			// Connect accesses in mem and instantiation component
			int z = Hier_ConnectAccesses(data->Implem, memacc, impacc);
			if(z!=0) {
				printf("Warning: Couldn't connect memWA accesses between mem '%s' and Implem '%s'.\n",
					comp->name, modImplem->compmod.model_name
				);
				error_code = __LINE__;
				goto EXIT_POINT;
			}
		}
	}  // Process all Implem that use the mem component

	EXIT_POINT:

	Build_SymTops_MoveOrig2Done(data, pp_node_orig);

	return error_code;
}

// A function to apply post-load, per-Implem operations
// Note: Only for Implems that have a Hier graph
static int Build_PostLoad_PerImplem(implem_t* Implem) {
	unsigned errors_nb = 0;

	// Detect control flow
	if(Implem->H!=NULL) {

		// Fix destination bit width in assignments
		// This is because partial assignments can be obtained with fields of structures
		avl_p_foreach(&Implem->H->NODES[HIERARCHY_STATE], scanState) {
			hier_node_t* node = scanState->data;
			hier_state_t* state_data = &node->NODE.STATE;
			foreach(state_data->actions, scanAction) {
				hier_action_t* action = scanAction->DATA;
				hvex_t* vex_dest = hvex_asg_get_dest(action->expr);

				// Resize if needed (paranoia)
				hvex_t* vex_expr = hvex_asg_get_expr(action->expr);
				if(vex_expr->width != vex_dest->width) {
					hvex_resize(vex_expr, vex_dest->width);
				}

				// No function call should remain, but paranoia...
				assert(vex_dest!=NULL);

				char* name = hvex_vec_getname(vex_dest);
				assert(name!=NULL);

				// Get the component
				netlist_comp_t* comp = Netlist_Comp_GetChild(Implem->netlist.top, name);
				netlist_port_t* top_port = Netlist_Comp_GetPort(Implem->netlist.top, name);
				netlist_access_t* top_acc = Netlist_Comp_GetAccess(Implem->netlist.top, name);

				if(comp!=NULL) {

					if(comp->model==NETLIST_COMP_REGISTER) {
						netlist_register_t* reg_data = comp->data;
						// Check whether the assignment must be extended
						if(vex_dest->right==0 && vex_dest->left==reg_data->width-1) continue;
						// Add the missing right bits
						if(vex_dest->right > 0) {
							hvex_catright(vex_expr, hvex_newvec(name, vex_dest->right-1, 0));
						}
						// Add the missing left bits
						if(vex_dest->left < reg_data->width-1) {
							hvex_catleft(vex_expr, hvex_newvec(name, reg_data->width-1, vex_dest->left+1));
						}
						// Update the indexes of the destination
						vex_dest->right = 0;
						vex_dest->left  = reg_data->width - 1;
						vex_dest->width = reg_data->width;
						action->expr->right = 0;
						action->expr->left  = reg_data->width - 1;
						action->expr->width = reg_data->width;
					}
					else if(comp->model==NETLIST_COMP_MEMORY) {
						netlist_memory_t* mem_data = comp->data;
						// Check whether the assignment must be extended
						if(vex_dest->right==0 && vex_dest->left==mem_data->data_width-1) continue;
						// Add the missing right bits
						if(vex_dest->right > 0) {
							hvex_t* vex_miss = hvex_newindex(name, vex_dest->right-1, 0,
								hvex_dup(hvex_asg_get_addr(action->expr))
							);
							hvex_catright(vex_expr, vex_miss);
						}
						// Add the missing left bits
						if(vex_dest->left < mem_data->data_width-1) {
							hvex_t* vex_miss = hvex_newindex(name, mem_data->data_width-1, vex_dest->left+1,
								hvex_dup(hvex_asg_get_addr(action->expr))
							);
							hvex_catleft(vex_expr, vex_miss);
						}
						// Update the indexes of the destination
						vex_dest->right = 0;
						vex_dest->left  = mem_data->data_width - 1;
						vex_dest->width = mem_data->data_width;
						action->expr->right = 0;
						action->expr->left  = mem_data->data_width - 1;
						action->expr->width = mem_data->data_width;
					}
					else if(comp->model==NETLIST_COMP_SIGNAL) {
						netlist_signal_t* sig_data = comp->data;
						// Only full assignments are tolerated
						if(vex_dest->right==0 && vex_dest->left==sig_data->width-1) {}
						else abort();
					}
					else {
						printf("Warning: Symbol '%s' is comp model '%s', don't know if ASG extension is needed\n", name, comp->model->name);
					}

				}

				else if(top_acc!=NULL) {

					// FIXME Merge code with comp MEM
					if(top_acc->model==NETLIST_ACCESS_MEM_WA_EXT) {
						netlist_access_mem_addr_t* memaddr_data = top_acc->data;
						// Check whether the assignment must be extended
						if(vex_dest->right==0 && vex_dest->left==memaddr_data->port_d->width-1) continue;
						// Add the missing right bits
						if(vex_dest->right > 0) {
							hvex_t* vex_miss = hvex_newindex(name, vex_dest->right-1, 0,
								hvex_dup(hvex_asg_get_addr(action->expr))
							);
							hvex_catright(vex_expr, vex_miss);
						}
						// Add the missing left bits
						if(vex_dest->left < memaddr_data->port_d->width-1) {
							hvex_t* vex_miss = hvex_newindex(name, memaddr_data->port_d->width-1, vex_dest->left+1,
								hvex_dup(hvex_asg_get_addr(action->expr))
							);
							hvex_catleft(vex_expr, vex_miss);
						}
						// Update the indexes of the destination
						vex_dest->right = 0;
						vex_dest->left  = memaddr_data->port_d->width - 1;
						vex_dest->width = memaddr_data->port_d->width;
						action->expr->right = 0;
						action->expr->left  = memaddr_data->port_d->width - 1;
						action->expr->width = memaddr_data->port_d->width;
					}
					else {
						printf("Warning: Symbol '%s' is access model '%s', don't know if ASG extension is needed\n", name, top_acc->model->name);
					}

				}

				else if(top_port!=NULL) {
					if(vex_dest->right==0 && vex_dest->left==top_port->width-1) {}
					else abort();
				}

				else {
					errprintf("Nothing corresponds to symbol '%s'\n", name);
					errors_nb++;
				}

			}  // Scan Actions
		}  // Scan States

		if(errors_nb > 0) {
			errprintf("Errors found: %i\n", errors_nb);
			abort();
		}

		// Apply extensive VEX simplification function
		avl_p_foreach(&Implem->H->NODES[HIERARCHY_STATE], scanState) {
			hier_node_t* node = scanState->data;
			hier_state_t* state_data = &node->NODE.STATE;
			foreach(state_data->actions, scanAction) {
				hier_action_t* action = scanAction->DATA;
				hvex_foreach(action->expr->operands, VexOp) hvex_simp(VexOp);
			}  // Scan Actions
		}  // Scan States

	}  // Case: there is a Hier graph

	// Insert read buffers on memory components
	Build_PostLoad_MemSyncRead(Implem);

	// Detect control flow
	if(Implem->H!=NULL) {
		Hier_Update_Control(Implem);

		// If asked to inline all function calls, ensure no Call node remains
		if(Implem->env.inline_all==true && avl_p_isempty(&Implem->H->NODES[HIERARCHY_CALL])==false) {
			errprintf("Top-level '%s': Asked to inlined all functions, but some function calls still remain.\n", Implem->compmod.model_name);
			return -1;
		}

		// Build the needed operators
		Map_Netlist_CompleteComponents(Implem);
	}

	// For debug: check integrity
	#ifndef NDEBUG
	Implem_CheckIntegrity(Implem);
	#endif

	return 0;
}

// Main function to detect top-levels

static int Build_DetectTopLevels(implem_t* Implem) {
	unsigned error_code = 0;

	char* augh_main_name = namealloc("augh_main");

	// The data structure that enables to track which top-level uses which symbol
	build_symtops_data_t data_inst;
	build_symtops_data_t* data = &data_inst;

	data->Implem = Implem;
	Build_SymTops_DataInit(data);

	// List the PROCESS nodes that will become top-levels
	avl_p_foreach(&Implem->H->NODES[HIERARCHY_PROCESS], scanProc) {
		hier_node_t* nodeProc = scanProc->data;
		hier_proc_t* proc_data = &nodeProc->NODE.PROCESS;
		if(proc_data->name_orig==augh_main_name) avl_pp_add_overwrite(&data->tree_toplevels, proc_data->name_orig, nodeProc);
		if(proc_data->user.is_toplevel==true)    avl_pp_add_overwrite(&data->tree_toplevels, proc_data->name_orig, nodeProc);
		if(Implem->H->PROCESS==nodeProc)         avl_pp_add_overwrite(&data->tree_toplevels, proc_data->name_orig, nodeProc);
	}

	unsigned toplevels_nb = avl_pp_count(&data->tree_toplevels);
	if(toplevels_nb==0) {
		printf("Error: Couldn't find a top-level process.\n");
		Build_SymTops_DataClear(data);
		return __LINE__;
	}

	printf("Info: Setting top-level functions:");
	avl_pp_foreach(&data->tree_toplevels, scan) printf(" %s", (char*)scan->key);
	printf(" (total: %u)\n", toplevels_nb);

	// When there is only one top-level, just keep the Implem as-is
	if(toplevels_nb==1) {
		hier_node_t* nodeProc = avl_pp_root_data(&data->tree_toplevels);
		Implem->H->PROCESS = nodeProc;
		Build_SymTops_DataClear(data);

		// Replace built-in functions
		int z = Hier_ReplaceBuiltinFunc(data, NULL, NULL);
		if(z!=0) {
			printf("ERROR : Replacing builtin function calls failed (code %i).\n", z);
			return __LINE__;
		}

		Build_PostLoad_PerImplem(Implem);
		Build_PostLoad_CheckCreateSigStart(Implem);
		Map_Netlist_CheckCreateSystemPorts(Implem);
		return 0;
	}

	dbgprintf("Creating multiple top-level Implems...\n");

	// List all symbols that must be avoided by builtin func when auto creating new ports/accesses in new top-levels
	avl_pp_foreach(&Implem->netlist.top->children, scan) {
		avl_p_add_overwrite(&data->tree_avoidsym, scan->key, scan->key);
	}
	avl_pp_foreach(&Implem->netlist.top->ports, scan) {
		avl_p_add_overwrite(&data->tree_avoidsym, scan->key, scan->key);
	}
	avl_pp_foreach(&Implem->netlist.top->accesses, scan) {
		avl_p_add_overwrite(&data->tree_avoidsym, scan->key, scan->key);
	}

	// Create all Implem models
	avl_pp_foreach(&data->tree_toplevels, scanTL) {
		hier_node_t* nodeProc = scanTL->data;

		// In the tree of toplevels, replace the old nodeProc by the new Implem
		implem_t* modImplem = Build_DetectTopLevels_Proc2Implem(Implem, nodeProc);
		scanTL->data = modImplem;

		// Create a component in the original Implem, it instantiates the modImplem
		netlist_comp_t* comp_inst = Netlist_Comp_Dup_flag(modImplem->netlist.top, false);
		Netlist_Comp_SetChild(Implem->netlist.top, comp_inst);
		// For the netlist module: identify the component as extern
		comp_inst->flags |= NETLIST_COMP_ISEXTERN;
		netlist_top_t* top_data = comp_inst->data;
		top_data->extern_model_name = modImplem->compmod.model_name;

		// Save in a tree to get the component
		avl_pp_add_overwrite(&data->tree_imp2comp, modImplem, comp_inst);
	}

	// Remove all Hier in the orig Implem
	Hier_Free(Implem->H);
	Implem->H = NULL;

	unsigned errors_nb = 0;

	// Replace all built-in functions
	avl_pp_foreach(&data->tree_imp2comp, scan) {
		implem_t* modImplem = scan->key;
		netlist_comp_t* compInst = scan->data;
		int z = Hier_ReplaceBuiltinFunc(data, modImplem, compInst);
		if(z!=0) {
			printf("ERROR: Top-level '%s': Replacing builtin function calls failed (code %i).\n", modImplem->compmod.model_name, z);
			errors_nb ++;
		}
	}

	if(errors_nb > 0) {
		error_code = __LINE__;
		goto EXIT_POINT;
	}

	// Scan each Implem, find symbols (ports and components)
	avl_pp_foreach(&data->tree_toplevels, scanTL) {
		implem_t* modImplem = scanTL->data;

		// Set the field FATHER to all VEX expressions
		Hier_AllActions_SetExprFather(modImplem->H);

		// A local tree to store occurrences
		avl_pp_tree_t tree_occur;
		avl_pp_init(&tree_occur);

		// Get occurrences of all symbols in the local modImplem
		Hier_AllSymbols_GetVexOccur_treelist(modImplem->H, &tree_occur);

		// Sort all occurrences, store them in the global tree
		avl_pp_foreach(&tree_occur, scanSym) {
			char* name = scanSym->key;
			// Get the node of the global tree
			avl_pp_node_t* pp_node = NULL;
			if(avl_pp_add(&data->tree_syms_orig, name, &pp_node)==true) {
				pp_node->data = malloc(sizeof(avl_pp_tree_t));
				avl_pp_init(pp_node->data);
			}
			// Add the current list of occurrences
			avl_pp_add_overwrite(pp_node->data, modImplem, scanSym->data);
		}

		// Clean locally used data.
		// Note: the data lists are already extracted and stored in the global tree.
		avl_pp_reset(&tree_occur);
	}

	// Here we have ALL occurrences of ALL symbols in ALL modImplems
	// Process components first

	while(avl_pp_isempty(&data->tree_syms_orig)==false) {
		avl_pp_node_t* pp_node_orig = avl_pp_deepest(&data->tree_syms_orig);
		char* name = pp_node_orig->key;

		// Get the component, top-level access, top-level port this name refers to
		netlist_comp_t* comp = Map_Netlist_GetComponent(Implem, name);
		netlist_port_t* topport = Netlist_Comp_GetPort(Implem->netlist.top, name);
		netlist_access_t* topacc = Netlist_Comp_GetAccess(Implem->netlist.top, name);

		// Process components

		if(comp!=NULL) {
			if(comp->model==NETLIST_COMP_SIGNAL) {
				int z = Build_DetectTopLevels_MoveSig(data, pp_node_orig, comp);
				if(z!=0) errors_nb++;
			}
			else if(comp->model==NETLIST_COMP_REGISTER) {
				int z = Build_DetectTopLevels_MoveReg(data, pp_node_orig, comp);
				if(z!=0) errors_nb++;
			}
			else if(comp->model==NETLIST_COMP_MEMORY) {
				int z = Build_DetectTopLevels_MoveMem(data, pp_node_orig, comp);
				if(z!=0) errors_nb++;
			}
			else {
				printf("Error: Component '%s': Model '%s' is not handled. Can't dispatch between Implems.\n", comp->name, comp->model->name);
				errors_nb++;
				Build_SymTops_MoveOrig2Done(data, pp_node_orig);
			}
		}
		else if(topacc!=NULL) {
			Build_SymTops_MoveOrig2Acc(data, pp_node_orig);
		}
		else if(topport!=NULL) {
			Build_SymTops_MoveOrig2Acc(data, pp_node_orig);
		}
		else {
			// That symbol is probably private to the top-levels, skip it silently

			#if 0
			errprintf("Nothing corresponds to symbol '%s', used in these functions:", name);
			avl_pp_tree_t* tree_impoccur = pp_node_orig->data;
			avl_pp_foreach(tree_impoccur, scanImp) {
				implem_t* modImplem = scanImp->key;
				printf(" '%s'", modImplem->compmod.model_name);
			}
			printf("\n");
			errors_nb++;
			#endif

			Build_SymTops_MoveOrig2Done(data, pp_node_orig);
		}

	}  // Scan components

	if(errors_nb > 0) {
		printf("ERROR: %u errors were found when scanning symbols.\n", errors_nb);
		// Set an error code
		error_code = __LINE__;
		goto EXIT_POINT;
	}

	// Process top-level ports and accesses

	// Check if some symbols remain
	while(avl_pp_isempty(&data->tree_syms_acc)==false) {
		avl_pp_node_t* pp_node_acc = avl_pp_deepest(&data->tree_syms_acc);
		char* name = pp_node_acc->key;

		// Get the top-level port
		netlist_port_t* topport = Netlist_Comp_GetPort(Implem->netlist.top, name);
		netlist_access_t* topacc = Netlist_Comp_GetAccess(Implem->netlist.top, name);

		// Only handle when the ports is member of an access
		if(topacc==NULL) {
			if(topport==NULL) abort();
			if(topport->access==NULL) {
				printf("Error: Symbol '%s' is top-level port but it is not member of an access. Can't move it.\n", name);
				errors_nb++;
				Build_SymTops_MoveAcc2Done(data, pp_node_acc);
				continue;
			}
			topacc = topport->access;
		}

		// Launch appropriate function to handle the access model

		if(topacc->model==NETLIST_ACCESS_FIFO_IN || topacc->model==NETLIST_ACCESS_FIFO_OUT) {

			// FIXME decide what to do about that, now multi-thread and builtins have been modified

			// It conflicts with what has previously been done by builtin funcs fifo_read and fifo_write
			// However, it still has potential to replace these examples of code:
			// var = fifo;
			// fifo = value;
			dbgprintf("Skipping processing of '%s' (access '%s' model '%s'), used in these functions:", name, topacc->name, topacc->model->name);
			avl_pp_tree_t* tree_impoccur = pp_node_acc->data;
			avl_pp_foreach(tree_impoccur, scanImp) {
				implem_t* modImplem = scanImp->key;
				printf(" '%s'", modImplem->compmod.model_name);
			}
			printf(" (implementation in progress)\n");

			Build_SymTops_MoveAcc2Done(data, pp_node_acc);
		}
		else {
			errprintf("Top-level port '%s' of access '%s': Model '%s' is not handled.\n", name, topacc->name, topacc->model->name);
			errors_nb++;
			Build_SymTops_MoveAcc2Done(data, pp_node_acc);
		}

		// Ensure the current symbol is moved to Done
		// It's needed because the function that processes the access may have stopped at another symbol
		if(avl_pp_isthere(&data->tree_syms_acc, name)==true) {
			Build_SymTops_MoveAcc2Done(data, pp_node_acc);
		}

	}  // Scan occurrences of top-level ports and accesses

	if(errors_nb > 0) {
		errprintf("%u errors were found when scanning symbols for top-level ports.\n", errors_nb);
		// Set an error code
		error_code = __LINE__;
		goto EXIT_POINT;
	}

	// Apply all post-load operations on all new Implems
	// Note: Not needed on the orig Implem because it has no Hier
	avl_pp_foreach(&data->tree_imp2comp, scan) {
		implem_t* modImplem = scan->key;
		Build_PostLoad_PerImplem(modImplem);
	}

	if(errors_nb > 0) {
		error_code = __LINE__;
		goto EXIT_POINT;
	}

	// Create system ports in all Implems
	avl_pp_foreach(&data->tree_imp2comp, scan) {
		implem_t* modImplem = scan->key;
		Build_PostLoad_CheckCreateSigStart(modImplem);
		Map_Netlist_CheckCreateSystemPorts(modImplem);
		// Also create the system ports in the instance
		// FIXME This is very dirty, a more generic solution would be much better
		netlist_comp_t* comp = scan->data;
		if(modImplem->netlist.port_clock!=NULL) {
			Netlist_Access_Dup(modImplem->netlist.port_clock->access, comp, NULL);
		}
		if(modImplem->netlist.port_reset!=NULL) {
			Netlist_Access_Dup(modImplem->netlist.port_reset->access, comp, NULL);
		}
		if(modImplem->netlist.port_start!=NULL) {
			Netlist_Access_Dup(modImplem->netlist.port_start->access, comp, NULL);
		}
	}
	Build_PostLoad_CheckCreateSigStart(Implem);
	Map_Netlist_CheckCreateSystemPorts(Implem);

	// For debug: check integrity of the orig Implem
	// Note: All new Implem have already been checked just before
	#ifndef NDEBUG
	Netlist_Debug_CheckIntegrity(Implem->netlist.top);
	#endif

	EXIT_POINT:

	// If error, free all new Implems
	if(error_code!=0) {
		avl_pp_foreach(&data->tree_imp2comp, scan) {
			implem_t* modImplem = scan->key;
			augh_implem_free(modImplem);
		}
	}

	Build_SymTops_DataClear(data);

	return error_code;
}



//======================================================================
// Command interpreter
//======================================================================

int Build_Command(implem_t* Implem, command_t* cmd_data) {
	const char* cmd = Command_PopParam(cmd_data);
	if(cmd==NULL) {
		printf("Error 'build' : No command received. Try 'help'.\n");
		return -1;
	}

	if(strcmp(cmd, "help")==0) {
		printf(
			"Possible commands:\n"
			"  help                    Display this help.\n"
			"  handle-annot [<bool>]   Handle user annotations [default=yes].\n"
			"  clkdiv <divider>        Insert a clock divider.\n"
			"                          The divider value must be a multiple of 2.\n"
			"                          A clock port must exist and a clock frequency must already be set.\n"
			"Commands about verbosity:\n"
			"  annot-verbose [<bool>]    Select verbosity when detecting user annotations.\n"
			"  datacom-verbose [<bool>]  Select verbosity when manipulating data interfaces.\n"
			"Commands about component models:\n"
			"  inst <model-name> <instance-name> [other instance names]\n"
			"                          Add instances of a component model defined by another C source file.\n"
			"  link [<comp-name1>.]<access-name1> [<comp-name2>.]<access-name2>\n"
			"                          Link two component accesses together, if compatible.\n"
			"                          If the component name is omitted, search the top-level accesses.\n"
			"  fromboard <access-name> [<rename>]\n"
			"                          When an FPGA board is targeted, add a board access to the top-level component.\n"
			"                          By default, the access name is kept. Another access name can be specified.\n"
		);
		return 0;
	}

	// Commands that only set parameters

	if(strcmp(cmd, "handle-annot")==0 || strcmp(cmd, "handle-annotations")==0) {
		int z = Command_PopParam_YesNoDef_Verbose(cmd_data, true, cmd);
		if(z<0) return -1;
		handle_annotations = z;
		return 0;
	}
	else if(strcmp(cmd, "annot-verbose")==0) {
		int z = Command_PopParam_YesNoDef_Verbose(cmd_data, true, cmd);
		if(z<0) return -1;
		annot_verbose = z;
		return 0;
	}
	else if(strcmp(cmd, "datacom-verbose")==0) {
		int z = Command_PopParam_YesNoDef_Verbose(cmd_data, true, cmd);
		if(z<0) return -1;
		datacom_verbose = z;
		return 0;
	}

	// Note: this command should be used BEFORE loading a design
	else if(strcmp(cmd, "clkdiv")==0) {
		const char* param = Command_PopParam(cmd_data);
		if(param==NULL) {
			printf("Error: Missing parameter for command '%s'.\n", cmd);
			return -1;
		}
		unsigned divider = atoi(param);
		if(divider<2 || divider%2!=0) {
			printf("Error: Invalid parameter for command '%s': the divider value must be a multiple of 2.\n", cmd);
			return -1;
		}

		// Check that a clock source port is properly set
		synth_target_t* synth = Implem->synth_target;

		unsigned valid_err = 0;
		if(Implem->netlist.sig_clock==NULL) valid_err = __LINE__;
		if(Implem->netlist.port_clock==NULL) valid_err = __LINE__;
		if(synth->clk_freq==0) valid_err = __LINE__;

		netlist_signal_t* sig_data = NULL;

		if(valid_err==0) {
			sig_data = Implem->netlist.sig_clock->data;
			if(sig_data->port_in->source==NULL) valid_err = __LINE__;
			else {
				if(sig_data->port_in->source->NEXT!=NULL) valid_err = __LINE__;
				else {
					netlist_val_cat_t* valcat = sig_data->port_in->source->DATA;
					if(valcat->source!=Implem->netlist.port_clock) valid_err = __LINE__;
				}
			}
		}

		if(valid_err!=0) {
			printf("Error: Invalid situation to use the command '%s' [code %u].\n", cmd, valid_err);
			return -1;
		}

		// Create the divider component
		char* name = Map_Netlist_MakeInstanceName(Implem, NETLIST_COMP_CLKDIV);
		netlist_comp_t* comp = Netlist_ClkDiv_New(name, divider);
		Netlist_Comp_SetChild(Implem->netlist.top, comp);

		netlist_clkdiv_t* clkdiv_data = comp->data;

		// Insert the divider between this port and the clock signal
		Netlist_ValCat_FreeList(sig_data->port_in->source);
		sig_data->port_in->source = Netlist_ValCat_MakeList_FullPort(clkdiv_data->port_out);
		Netlist_PortTargets_SetSource(sig_data->port_in);

		clkdiv_data->port_in->source = Netlist_ValCat_MakeList_FullPort(Implem->netlist.port_clock);
		Netlist_PortTargets_SetSource(clkdiv_data->port_in);

		// Update the target clock
		Techno_SynthTarget_SetFrequency(synth, synth->clk_freq/(double)divider);
	}

	else if(strcmp(cmd, "inst")==0) {
		// Parameters = model_name inst_name [other instance names]
		char* model_name = Command_PopParam_namealloc(cmd_data);
		if(model_name==NULL) {
			printf("Error: Missing model name for command '%s'. Try 'help'.\n", cmd);
			return -1;
		}
		implem_t* modImplem = augh_modimplem_get(Implem->env.globals, model_name);
		if(modImplem==NULL) {
			printf("Error: Component model '%s' was not found.\n", model_name);
			return -1;
		}

		// All other parameters are instance names
		unsigned errors_nb = 0;
		unsigned inst_nb = 0;
		do {
			char* inst_name = Command_PopParam_namealloc(cmd_data);
			if(inst_name==NULL) break;
			// Ensure that no component with that name already exists
			netlist_comp_t* comp = Netlist_Comp_GetChild(Implem->netlist.top, inst_name);
			if(comp!=NULL) {
				printf("Error: Component instance '%s' already exists.\n", inst_name);
				return -1;
			}
			// Create the component (model TOP), with no content
			comp = Netlist_Comp_Dup_flag(modImplem->netlist.top, false);
			comp->name = inst_name;
			// For the netlist module: identify the component as extern
			comp->flags |= NETLIST_COMP_ISEXTERN;
			assert(comp->model==NETLIST_COMP_TOP);
			netlist_top_t* comptop_data = comp->data;
			comptop_data->extern_model_name = model_name;
			// Add the new component to the Implem
			Netlist_Comp_SetChild(Implem->netlist.top, comp);
			inst_nb++;
		} while(1);

		if(errors_nb>0) return errors_nb;
		if(inst_nb==0) {
			printf("Error: No instance names were given for model '%s'. Try 'help'.\n", model_name);
			return -1;
		}

		// Update the instance count all component models
		ImpMod_Count_Update(Implem->env.globals);
	}

	else if(strcmp(cmd, "link")==0) {
		// Parameters = inst1.access inst2.access
		const char* str_access1 = Command_PopParam_namealloc(cmd_data);
		const char* str_access2 = Command_PopParam_namealloc(cmd_data);
		if(str_access2==NULL) {
			printf("Error: Missing parameters for command '%s'. Try 'help'.\n", cmd);
			return -1;
		}
		// Get the component and access
		netlist_access_t* access1 = ImpMod_GetAccess_User(Implem, str_access1, true);
		if(access1==NULL) return -1;
		netlist_access_t* access2 = ImpMod_GetAccess_User(Implem, str_access2, true);
		if(access2==NULL) return -1;
		// Launch the function that does the connection and who instantiates interface components if needed
		return Hier_ConnectAccesses(Implem, access1, access2);
	}

	else if(strcmp(cmd, "fromboard")==0) {
		// Ensure a board is actually targeted
		board_fpga_t* board = Implem->synth_target->board_fpga;
		if(board==NULL) {
			printf("Error: A target board must be set for command '%s'.\n", cmd);
			return -1;
		}
		// Parameters = access-name [rename]
		char* access_name = Command_PopParam_namealloc(cmd_data);
		char* rename      = Command_PopParam_namealloc(cmd_data);
		if(access_name==NULL) {
			printf("Error: Missing parameter for command '%s'. Try 'help'.\n", cmd);
			return -1;
		}
		// Get the access
		netlist_access_t* access = Netlist_Comp_GetAccess(board->connect, access_name);
		if(access==NULL && strcmp(access_name, "stdin")==0)  access = board->default_stdin;
		if(access==NULL && strcmp(access_name, "stdout")==0) access = board->default_stdout;
		if(access==NULL) {
			printf("Error: Access '%s' not found in the targeted board.\n", access_name);
			return -1;
		}
		// Build the name of the new access
		char* newaccess_name = access_name;
		if(rename!=NULL) newaccess_name = rename;
		// Ensure there is no access with the target name
		if(Netlist_Comp_GetAccess(Implem->netlist.top, newaccess_name)!=NULL) {
			printf("Error: An access named '%s' already exists, can't import from the board model.\n", newaccess_name);
			return -1;
		}
		// Duplicate the access in the new component...
		netlist_access_t* newaccess = Netlist_Access_Dup(access, Implem->netlist.top, newaccess_name);
		if(newaccess==NULL) {
			printf("Error: Failed creation of new access '%s' from board model.\n", newaccess_name);
			return -1;
		}
		// Link the two access structures
		avl_pp_add_overwrite(&Implem->synth_target->impacc2boardacc, newaccess, access);
	}

	// The other commands require that the hierarchy exists

	#if 0
	if(Implem->H==NULL) {
		printf("Error : the hierarchy is not built.\n");
		return -1;
	}
	#endif

	else {
		printf("Error 'build' : Unknown command '%s'.\n", cmd);
		return -1;
	}

	return 0;
}



//=====================================================================
// Init
//=====================================================================

// FIXME TODO add builtins to set FIFO depth

void Hier_Build_Init() {
	avl_pp_init(&HBUILD_CB_TREEALL);

	// Declare builtin functions and their callbacks

	builtinfunc_t* builtin = NULL;

	// Creation of data interfaces

	builtin = Hier_BuiltinFunc_New();
	builtin->func_name = namealloc("augh_port_in");
	builtin->call_is_act = true;
	builtin->alone_in_state = true;
	builtin->type = HIER_BUILTIN_IF;
	builtin->func_vex = Hier_BuiltinFunc_PortInCreate_vex;
	Hier_BuiltinFunc_Add(builtin);

	builtin = Hier_BuiltinFunc_New();
	builtin->func_name = namealloc("augh_port_out");
	builtin->call_is_act = true;
	builtin->alone_in_state = true;
	builtin->type = HIER_BUILTIN_IF;
	builtin->func_vex = Hier_BuiltinFunc_PortOutCreate_vex;
	Hier_BuiltinFunc_Add(builtin);

	builtin = Hier_BuiltinFunc_New();
	builtin->func_name = namealloc("augh_access_fifo_in");
	builtin->call_is_act = true;
	builtin->alone_in_state = true;
	builtin->type = HIER_BUILTIN_IF;
	builtin->func_vex = Hier_BuiltinFunc_AccFifoInCreate_vex;
	Hier_BuiltinFunc_Add(builtin);

	builtin = Hier_BuiltinFunc_New();
	builtin->func_name = namealloc("augh_access_fifo_out");
	builtin->call_is_act = true;
	builtin->alone_in_state = true;
	builtin->type = HIER_BUILTIN_IF;
	builtin->func_vex = Hier_BuiltinFunc_AccFifoOutCreate_vex;
	Hier_BuiltinFunc_Add(builtin);

	builtin = Hier_BuiltinFunc_New();
	builtin->func_name = namealloc("augh_access_uart_rx");
	builtin->call_is_act = true;
	builtin->alone_in_state = true;
	builtin->type = HIER_BUILTIN_IF;
	builtin->func_vex = Hier_BuiltinFunc_AccUartRxCreate_vex;
	Hier_BuiltinFunc_Add(builtin);

	builtin = Hier_BuiltinFunc_New();
	builtin->func_name = namealloc("augh_access_uart_tx");
	builtin->call_is_act = true;
	builtin->alone_in_state = true;
	builtin->type = HIER_BUILTIN_IF;
	builtin->func_vex = Hier_BuiltinFunc_AccUartTxCreate_vex;
	Hier_BuiltinFunc_Add(builtin);

	builtin = Hier_BuiltinFunc_New();
	builtin->func_name = namealloc("augh_uart_create");
	builtin->call_is_act = true;
	builtin->alone_in_state = true;
	builtin->type = HIER_BUILTIN_IF;
	builtin->func_vex = Hier_BuiltinFunc_UartCreate_vex;
	Hier_BuiltinFunc_Add(builtin);

	// Data transfers

	builtin = Hier_BuiltinFunc_New();
	builtin->func_name = namealloc("augh_read");
	builtin->call_is_act = true;
	builtin->alone_in_state = true;
	builtin->type = HIER_BUILTIN_TRANSFER;
	builtin->func_vex = Hier_BuiltinFunc_FifoRead_vex;
	Hier_BuiltinFunc_Add(builtin);

	builtin = Hier_BuiltinFunc_New();
	builtin->func_name = namealloc("augh_write");
	builtin->call_is_act = true;
	builtin->alone_in_state = true;
	builtin->type = HIER_BUILTIN_TRANSFER;
	builtin->func_vex = Hier_BuiltinFunc_FifoWrite_vex;
	Hier_BuiltinFunc_Add(builtin);

	builtin = Hier_BuiltinFunc_New();
	builtin->func_name = namealloc("augh_read_vector");
	builtin->call_is_act = true;
	builtin->alone_in_state = true;
	builtin->type = HIER_BUILTIN_TRANSFER;
	builtin->func_vex = Hier_BuiltinFunc_FifoVectorRead_vex;
	Hier_BuiltinFunc_Add(builtin);

	builtin = Hier_BuiltinFunc_New();
	builtin->func_name = namealloc("augh_write_vector");
	builtin->call_is_act = true;
	builtin->alone_in_state = true;
	builtin->type = HIER_BUILTIN_TRANSFER;
	builtin->func_vex = Hier_BuiltinFunc_FifoVectorWrite_vex;
	Hier_BuiltinFunc_Add(builtin);

	// Non-blocking data transfers

	builtin = Hier_BuiltinFunc_New();
	builtin->func_name = namealloc("augh_tryread");
	builtin->call_is_act = false;
	builtin->alone_in_state = false;
	builtin->type = HIER_BUILTIN_TRANSFER;
	builtin->func_vex = Hier_BuiltinFunc_FifoTryRead_vex;
	Hier_BuiltinFunc_Add(builtin);

	builtin = Hier_BuiltinFunc_New();
	builtin->func_name = namealloc("augh_trywrite");
	builtin->call_is_act = false;
	builtin->alone_in_state = false;
	builtin->type = HIER_BUILTIN_TRANSFER;
	builtin->func_vex = Hier_BuiltinFunc_FifoTryWrite_vex;
	Hier_BuiltinFunc_Add(builtin);

	builtin = Hier_BuiltinFunc_New();
	builtin->func_name = namealloc("aughin_setready");
	builtin->call_is_act = true;
	builtin->alone_in_state = true;
	builtin->type = HIER_BUILTIN_TRANSFER;
	builtin->func_vex = Hier_BuiltinFunc_FifoInSetReady_vex;
	Hier_BuiltinFunc_Add(builtin);

	builtin = Hier_BuiltinFunc_New();
	builtin->func_name = namealloc("aughout_setready");
	builtin->call_is_act = true;
	builtin->alone_in_state = true;
	builtin->type = HIER_BUILTIN_TRANSFER;
	builtin->func_vex = Hier_BuiltinFunc_FifoOutSetReady_vex;
	Hier_BuiltinFunc_Add(builtin);

	builtin = Hier_BuiltinFunc_New();
	builtin->func_name = namealloc("aughin_setreadyval");
	builtin->call_is_act = true;
	builtin->alone_in_state = true;
	builtin->type = HIER_BUILTIN_TRANSFER;
	builtin->func_vex = Hier_BuiltinFunc_FifoInSetReady_vex;
	Hier_BuiltinFunc_Add(builtin);

	builtin = Hier_BuiltinFunc_New();
	builtin->func_name = namealloc("aughout_setreadyval");
	builtin->call_is_act = true;
	builtin->alone_in_state = true;
	builtin->type = HIER_BUILTIN_TRANSFER;
	builtin->func_vex = Hier_BuiltinFunc_FifoOutSetReady_vex;
	Hier_BuiltinFunc_Add(builtin);

	builtin = Hier_BuiltinFunc_New();
	builtin->func_name = namealloc("aughin_spydata");
	builtin->call_is_act = true;
	builtin->alone_in_state = true;
	builtin->type = HIER_BUILTIN_TRANSFER;
	builtin->func_vex = Hier_BuiltinFunc_FifoSpyData_vex;
	Hier_BuiltinFunc_Add(builtin);

	builtin = Hier_BuiltinFunc_New();
	builtin->func_name = namealloc("aughout_setdata");
	builtin->call_is_act = true;
	builtin->alone_in_state = true;
	builtin->type = HIER_BUILTIN_TRANSFER;
	builtin->func_vex = Hier_BuiltinFunc_FifoSetData_vex;
	Hier_BuiltinFunc_Add(builtin);

	builtin = Hier_BuiltinFunc_New();
	builtin->func_name = namealloc("aughin_spyready");
	builtin->call_is_act = false;
	builtin->alone_in_state = false;
	builtin->type = HIER_BUILTIN_TRANSFER;
	builtin->func_vex = Hier_BuiltinFunc_FifoInSpyReady_vex;
	Hier_BuiltinFunc_Add(builtin);

	builtin = Hier_BuiltinFunc_New();
	builtin->func_name = namealloc("aughout_spyready");
	builtin->call_is_act = false;
	builtin->alone_in_state = false;
	builtin->type = HIER_BUILTIN_TRANSFER;
	builtin->func_vex = Hier_BuiltinFunc_FifoOutSpyReady_vex;
	Hier_BuiltinFunc_Add(builtin);

	// The builtin functions replaced last

	builtin = Hier_BuiltinFunc_New();
	builtin->func_name = namealloc("cysleep");
	builtin->call_is_act = true;
	builtin->alone_in_state = true;
	builtin->func_vex = Hier_BuiltinFunc_CycleSleep_vex;
	Hier_BuiltinFunc_Add(builtin);

	builtin = Hier_BuiltinFunc_New();
	builtin->func_name = namealloc("nsleep");
	builtin->call_is_act = true;
	builtin->alone_in_state = true;
	builtin->func_vex = Hier_BuiltinFunc_NanoSleep_vex;
	Hier_BuiltinFunc_Add(builtin);

	builtin = Hier_BuiltinFunc_New();
	builtin->func_name = namealloc("usleep");
	builtin->call_is_act = true;
	builtin->alone_in_state = true;
	builtin->func_vex = Hier_BuiltinFunc_MicroSleep_vex;
	Hier_BuiltinFunc_Add(builtin);

	builtin = Hier_BuiltinFunc_New();
	builtin->func_name = namealloc("sleep");
	builtin->call_is_act = true;
	builtin->alone_in_state = true;
	builtin->func_vex = Hier_BuiltinFunc_Sleep_vex;
	Hier_BuiltinFunc_Add(builtin);

	// Declare callback functions to connect accesses between components

	// WIRE accesses
	Hier_ConnectAccesses_AddCB_SymProdRead(
		Hier_ConnectAccesses_Wire_Prod2Read,
		NETLIST_ACCESS_WIRE_IN, NETLIST_ACCESS_WIRE_OUT,
		NETLIST_ACCESS_WIRE_OUT, NETLIST_ACCESS_WIRE_IN
	);
	// FIFO accesses
	Hier_ConnectAccesses_AddCB_SymProdRead(
		Hier_ConnectAccesses_Fifo_Prod2Read,
		NETLIST_ACCESS_FIFO_IN, NETLIST_ACCESS_FIFO_OUT,
		NETLIST_ACCESS_FIFO_OUT, NETLIST_ACCESS_FIFO_IN
	);

	// Accesses UART <-> FIFO
	Hier_ConnectAccesses_AddCB_SymProdRead(
		Hier_ConnectAccesses_UartRx_Prod2Read,
		NETLIST_ACCESS_UART_RX, NETLIST_ACCESS_UART_TX,
		NETLIST_ACCESS_FIFO_OUT, NETLIST_ACCESS_FIFO_IN
	);
	Hier_ConnectAccesses_AddCB_SymProdRead(
		Hier_ConnectAccesses_UartTx_Prod2Read,
		NETLIST_ACCESS_FIFO_IN, NETLIST_ACCESS_FIFO_OUT,
		NETLIST_ACCESS_UART_TX, NETLIST_ACCESS_UART_RX
	);

	// Memory accesses
	Hier_ConnectAccesses_AddCB_SymProdRead(
		Hier_ConnectAccesses_MemRA_Prod2Read,
		NETLIST_ACCESS_MEM_RA, NETLIST_ACCESS_MEM_RA_EXT,
		NETLIST_ACCESS_MEM_RA_EXT, NETLIST_ACCESS_MEM_RA
	);
	Hier_ConnectAccesses_AddCB_SymProdRead(
		Hier_ConnectAccesses_MemWA_Prod2Read,
		NETLIST_ACCESS_MEM_WA, NETLIST_ACCESS_MEM_WA_EXT,
		NETLIST_ACCESS_MEM_WA_EXT, NETLIST_ACCESS_MEM_WA
	);

	// DiffClock accesses
	Hier_ConnectAccesses_AddCB_SymProdRead(
		Hier_ConnectAccesses_DiffClock_Prod2Read,
		NETLIST_ACCESS_DIFFCLOCK, NULL,
		NULL, NETLIST_ACCESS_DIFFCLOCK
	);

}



//=====================================================================
// Replacing function calls
//=====================================================================

// The created temporary registers
static avl_p_tree_t tree_tmpregs_all;
// The temporary registers used in the STATE being processed
static avl_p_tree_t tree_tmpregs_state;

void Hier_Load_FuncCall_ReplaceArgs(implem_t* Implem, hier_action_t* action, hvex_t* Expr, hier_node_t* nodeProc) {
	assert(Expr->model==HVEX_FUNCTION);
	hier_proc_t* proc_data = &nodeProc->NODE.PROCESS;
	hier_node_t* nodeState = action->node;

	// Create the assignments of arguments
	chain_list* cur_scan_args = proc_data->build.list_args;
	hvex_t* cur_vexParam = Expr->operands;

	while(cur_scan_args!=NULL && cur_vexParam!=NULL) {
		hvex_t* vexArg = cur_scan_args->DATA;

		assert(vexArg->model==HVEX_VECTOR);

		vexArg = hvex_dup(vexArg);
		hvex_t* vexParam = hvex_dup(cur_vexParam);
		if(vexParam->width != vexArg->width) {
			vexParam = hvex_new_resize(vexParam, vexArg->width);
		}

		// Create a State/Action to assign arg=param
		hier_build_bbstact_t bbstact;
		Hier_Build_StAct(&bbstact);
		bbstact.action->expr = hvex_asg_make(vexArg, NULL, vexParam, NULL);
		// Insert this before the current State, inside the same BB
		Hier_Lists_AddNode(Implem->H, bbstact.state);
		Hier_InsertLevelBeforeNode(nodeState, bbstact.state);

		// FIXME Add source line information

		// Next arg/param
		cur_scan_args = cur_scan_args->NEXT;
		cur_vexParam = cur_vexParam->next;
	}  // Scan parameters and arguments

	if(cur_scan_args!=NULL || cur_vexParam!=NULL) {
		errprintf("Number of arguments mismatch for function '%s' at: \n", proc_data->name_orig);
		LineList_Print_be(action->source_lines, NULL, "\n");
		abort();
	}
}

// This function can set the action->expr to NULL in case a procedure call is replaced
void Hier_Load_FuncCall_ReplaceRecurs(implem_t* Implem, hier_action_t* action, hvex_t* Expr) {

	// First, process operands, in case there are nested VEX calls
	hvex_foreach(Expr->operands, VexOp) Hier_Load_FuncCall_ReplaceRecurs(Implem, action, VexOp);

	// Here process the local node
	if(Expr->model!=HVEX_FUNCTION) return;
	char* name = hvex_func_getname(Expr);
	hier_node_t* nodeState = action->node;

	// Get the PROCESS that corresponds to the function
	// Only handle functions who have a body (skip declared-only functions)
	hier_node_t* nodeProc = Hier_GetProc(Implem->H, name);
	if(nodeProc==NULL) return;
	hier_proc_t* proc_data = &nodeProc->NODE.PROCESS;
	if(proc_data->user.only_declared==true) return;

	// Create the assignments of arguments
	Hier_Load_FuncCall_ReplaceArgs(Implem, action, Expr, nodeProc);

	// Create a CALL node
	hier_node_t* nodeCall = Hier_Node_NewType(HIERARCHY_CALL);
	Hier_Lists_AddNode(Implem->H, nodeCall);
	Hier_CallProc_Link(nodeCall, nodeProc);

	// Insert the CALL node before the current state, splitting the BB
	Hier_InsertLevelBeforeNode_SplitBB(Implem->H, nodeState, nodeCall);

	// When there is no return value, the Action is only the function call
	// So it is a procedure call
	if(proc_data->build.vex_ret==NULL) {
		assert(action->expr==Expr);
		// Remove the Action
		hvex_free(action->expr);
		action->expr = NULL;
		return;
	}

	// Small optim: if the Expr is already the expr of the assignment of the Action,
	// don't create an intermediate return reg
	if(Expr==hvex_asg_get_expr(action->expr)) {
		hvex_t* vex_ret = hvex_dup(proc_data->build.vex_ret);
		vex_ret = hvex_new_resize(vex_ret, Expr->width);
		hvex_replace_free(Expr, vex_ret);
		return;
	}

	// Create the return VEX expression
	unsigned needed_width = Expr->left + 1;
	// Find an existing temp reg with enough width
	hvex_t* vex_tmpreg = NULL;
	avl_p_foreach(&tree_tmpregs_all, scanReg) {
		hvex_t* vex_local = scanReg->data;
		if(vex_local->width < needed_width) continue;
		if(vex_tmpreg==NULL || vex_local->width < vex_tmpreg->width) vex_tmpreg = vex_local;
	}
	// If no compatible temp reg was found, create a new one
	if(vex_tmpreg==NULL) {
		char* newname = Map_Netlist_MakeInstanceName_Prefix(Implem, "augh_build_callret");
		netlist_comp_t* comp = Netlist_Comp_Reg_New(newname, needed_width);
		Netlist_Comp_SetChild(Implem->netlist.top, comp);
		vex_tmpreg = hvex_newvec(newname, needed_width-1, 0);
	}
	else {
		// Remove the tmpreg from the tree of all ones
		avl_p_rem_key(&tree_tmpregs_all, vex_tmpreg);
	}
	// Add tmpreg to the tree of those used at the current state
	avl_p_add_overwrite(&tree_tmpregs_state, vex_tmpreg, vex_tmpreg);
	// Work on a duplicate
	vex_tmpreg = hvex_dup(vex_tmpreg);
	// Adapt indexes, in case the Reg was reused
	vex_tmpreg->width = proc_data->build.vex_ret->width;
	vex_tmpreg->left  = proc_data->build.vex_ret->width - 1;
	vex_tmpreg->right = 0;

	// Create the State/Action for assignment of return value
	hier_build_bbstact_t bbstact;
	Hier_Build_StAct(&bbstact);
	bbstact.action->expr = hvex_asg_make(
		hvex_dup(vex_tmpreg), NULL,
		hvex_dup(proc_data->build.vex_ret), NULL
	);
	// Insert this before the current State, inside the same BB
	Hier_Lists_AddNode(Implem->H, bbstact.state);
	Hier_InsertLevelBeforeNode(nodeState, bbstact.state);
	// FIXME Add source lines

	// Replace the current Expr
	vex_tmpreg->width = Expr->width;
	vex_tmpreg->left  = Expr->left;
	vex_tmpreg->right = Expr->right;

	hvex_replace_free(Expr, vex_tmpreg);
}

// Create the CALL nodes from VEX function calls that are inside ASG
// And verify that no function call remains
int Hier_Load_FuncCalls_Replace(implem_t* Implem) {
	unsigned error_code = 0;

	// The created temporary registers
	avl_p_init(&tree_tmpregs_all);
	// The temporary registers used in the State being processed
	avl_p_init(&tree_tmpregs_state);

	// List all State nodes to process (needed because new States will be added)
	chain_list* list_states = NULL;
	avl_p_foreach(&Implem->H->NODES[HIERARCHY_STATE], scan) list_states = addchain(list_states, scan->data);

	foreach(list_states, scanState) {
		hier_node_t* nodeState = scanState->DATA;
		hier_state_t* state_data = &nodeState->NODE.STATE;

		// Launch recursive scan of the VEX of all Actions
		chain_list* list_act_del = NULL;
		foreach(state_data->actions, scanAction) {
			hier_action_t* action = scanAction->DATA;
			Hier_Load_FuncCall_ReplaceRecurs(Implem, action, action->expr);
			if(action->expr==NULL) list_act_del = addchain(list_act_del, action);
		}
		foreach(list_act_del, scanAction) {
			hier_action_t* action = scanAction->DATA;
			Hier_Action_FreeFull(action);
		}
		freechain(list_act_del);

		// Put temporary registers back in the global tree
		if(avl_p_isempty(&tree_tmpregs_state)==false) {
			avl_p_foreach(&tree_tmpregs_state, scan) avl_p_add_overwrite(&tree_tmpregs_all, scan->data, scan->data);
			avl_p_reset(&tree_tmpregs_state);
		}

	}  // Scan all STATE nodes

	// Clean
	freechain(list_states);
	avl_p_foreach(&tree_tmpregs_all, scan) hvex_free(scan->data);
	avl_p_reset(&tree_tmpregs_all);

	return error_code;
}



//=====================================================================
// Main load function
//=====================================================================

#include "../ughparser/ughparser.h"

int Hier_Load(implem_t* Implem, const char* name) {
	unsigned error_code = 0;

	int z = Hier_Load_UghParser(Implem, name);
	if(z!=0) {
		error_code = __LINE__;
		goto ERROR_CASE;
	}

	#if 0  // Print, for debug
	Hier_Print(Implem->H);
	exit(0);
	#endif

	// Set the status flag
	Implem->synth_state.alloc = 1;

	// Detect user annotations
	Hier_GetAnnotations(Implem);

	// Remove the BREAK at the end of the bodies of the cases
	// Keep information in case_data->build when necessary
	avl_p_foreach(&Implem->H->NODES[HIERARCHY_SWITCH], scanSwitch) {
		hier_node_t* nodeSwitch = scanSwitch->data;
		//hier_switch_t* switch_data = &nodeSwitch->NODE.SWITCH;
		foreach(nodeSwitch->CHILDREN, scanChild) {
			hier_node_t* nodeCase = scanChild->DATA;
			hier_case_t* case_data = &nodeCase->NODE.CASE;
			// Scan the nodes of the body, search a BREAK
			if(case_data->body==NULL) continue;

			hier_node_t* nodeBreak = Hier_Level_GetLastNode(case_data->body);
			if(nodeBreak->TYPE!=HIERARCHY_JUMP && nodeBreak->TYPE!=HIERARCHY_RETURN) continue;

			// Cut the link with the next Case
			if(case_data->build.case_after!=NULL) {
				case_data->build.case_after->NODE.CASE.build.case_before = NULL;
				case_data->build.case_after = NULL;
			}

			// Remove the BREAK node
			assert(nodeBreak->NODE.JUMP.target!=NULL);
			if(nodeBreak->NODE.JUMP.target==nodeSwitch->NEXT) {
				Hier_RemoveLevel_Free(Implem->H, nodeBreak);
			}

		}  // Scan all Cases
	}  // Scan all Switch

	// Compute branch probabilities of all CASE nodes
	avl_p_foreach(&Implem->H->NODES[HIERARCHY_SWITCH], scanSwitch) {
		hier_node_t* nodeSwitch = scanSwitch->data;
		// Get the branch probability of the non-annotated branches
		double sum_prob_annotated = 0;
		unsigned non_annotated_nb = 0;
		foreach(nodeSwitch->CHILDREN, scanCase) {
			hier_node_t* nodeCase = scanCase->DATA;
			hier_case_t* case_data = &nodeCase->NODE.CASE;
			if(case_data->user.branch_prob_given==true) sum_prob_annotated += case_data->user.branch_prob;
			else non_annotated_nb++;
		}
		if(non_annotated_nb==0) continue;
		double prob_non_annotated = (1 - sum_prob_annotated) / non_annotated_nb;
		// Set annotations to non-annotated Case nodes
		foreach(nodeSwitch->CHILDREN, scanCase) {
			hier_node_t* nodeCase = scanCase->DATA;
			hier_case_t* case_data = &nodeCase->NODE.CASE;
			if(case_data->user.branch_prob_given==true) continue;
			case_data->user.branch_prob_given = true;
			case_data->user.branch_prob = prob_non_annotated;
		}
	}

	// Merge Cases with body with their next Case
	// Duplicate the bodies of the Cases that have no BREAK at the end
	avl_p_foreach(&Implem->H->NODES[HIERARCHY_SWITCH], scanSwitch) {
		hier_node_t* nodeSwitch = scanSwitch->data;
		hier_switch_t* switch_data = &nodeSwitch->NODE.SWITCH;
		if(switch_data->info.was_if!=0) continue;

		for(bool done=true; done==true; ) {
			done = false;

			// Cases can be removed, so work on a copy of the list
			chain_list* list_cases = dupchain(nodeSwitch->CHILDREN);

			foreach(list_cases, scanChild) {
				hier_node_t* nodeCase = scanChild->DATA;
				hier_case_t* case_data = &nodeCase->NODE.CASE;
				hier_node_t* nextCase = case_data->build.case_after;
				if(nextCase==NULL) continue;
				// Process only if the next case does not also continue in another Case
				hier_case_t* nextcase_data = &nextCase->NODE.CASE;
				if(nextcase_data->build.case_after!=NULL) continue;
				// Flag
				done = true;

				// Cut the link
				nextcase_data->build.case_before = NULL;
				case_data->build.case_after = NULL;

				// If the body of the current Case is empty, merge the test with the next Case
				if(case_data->body==NULL) {
					if(case_data->is_default!=0) {
						case_data->is_default = 0;
						// Delete test values in next Case
						avl_p_foreach(&nextcase_data->values, scanVal) Hier_TestInfo_FreeFull(scanVal->data);
						avl_p_reset(&nextcase_data->values);
						// Next Case is now the default
						nextcase_data->is_default = 1;
						switch_data->info.default_case = nextCase;
					}
					else if(nextcase_data->is_default==0) {
						// Transfer all test values
						avl_p_foreach(&case_data->values, scanVal) avl_p_add_overwrite(&nextcase_data->values, scanVal->data, scanVal->data);
						avl_p_reset(&case_data->values);
					}
					// Note: if the next case is already default, nothing to do about the tests

					// Merge branch probabilities
					nextcase_data->user.branch_prob += case_data->user.branch_prob;

					// Handle the case_before
					if(case_data->build.case_before!=NULL) {
						case_data->build.case_before->NODE.CASE.build.case_after = nextCase;
						nextcase_data->build.case_before = case_data->build.case_before;
					}

					// And delete the current Case
					Hier_ReplaceNode_Free(Implem->H, nodeCase, NULL);
					continue;
				}

				// Append the body of the next case to the present case
				if(nextcase_data->body==NULL) continue;
				if(Hier_Level_HasDubiousJumpLabel_Recurs(nextcase_data->body)==false) {
					// Append the body of the next case to the present case
					hier_node_t* newbody = Hier_Level_Dup(nextcase_data->body);
					Hier_Lists_AddLevel(Implem->H, newbody);
					Hier_InsertLevelAfterLevel(case_data->body, newbody);
				}
				else {
					// Create a JUMP node at end of the present Case body
					hier_node_t* nodeJump = Hier_Node_NewType(HIERARCHY_JUMP);
					nodeJump->source_lines = LineList_Dup(nextCase->source_lines);
					Hier_Lists_AddLevel(Implem->H, nodeJump);
					Hier_InsertLevelAfterLevel(case_data->body, nodeJump);
					// Create a Label at the beginning of the next Case
					hier_node_t* nodeLabel = Hier_Node_NewType(HIERARCHY_LABEL);
					nodeLabel->source_lines = LineList_Dup(nextCase->source_lines);
					Hier_Lists_AddLevel(Implem->H, nodeLabel);
					Hier_InsertLevelBeforeNode(nextcase_data->body, nodeLabel);
					// Connect the Jump to the Label
					Hier_JumpLabel_Link(nodeJump, nodeLabel);
				}

			}  // Scan the Cases

			freechain(list_cases);

		}  // Loop while something is done
	}  // Scan the switch nodes

	// All STATE nodes must be in BB
	avl_p_foreach(&Implem->H->NODES[HIERARCHY_STATE], scanState) {
		hier_node_t* nodeState = scanState->data;
		// Check existing parent BB
		hier_node_t* nodeBB = Hier_GetParentNode(nodeState);
		if(nodeBB!=NULL && nodeBB->TYPE==HIERARCHY_BB) continue;
		// Create a new BB
		nodeBB = Hier_Node_NewType(HIERARCHY_BB);
		Hier_ReplaceNode(nodeState, nodeBB);
		// Correctly link BB and State
		nodeBB->NODE.BB.body = nodeState;
		nodeState->PARENT = nodeBB;
		nodeBB->CHILDREN = addchain(nodeBB->CHILDREN, nodeState);
		// Add to the Hier lists of nodes
		Hier_Lists_AddNode(Implem->H, nodeBB);
	}

	#if 0  // For debug: print Hier
	printf("\nDEBUG %s:%u : Hier as read\n\n", __FILE__, __LINE__);
	Hier_Print(Implem->H);
	printf("\n");
	exit(EXIT_FAILURE);
	#endif

	// In case we are targeting an FPGA board
	Hier_InheritBoardInterfaces_FromReg(Implem);

	// All remaining function calls
	z = Hier_Load_FuncCalls_Replace(Implem);
	if(z!=0) {
		errprintf("Replacing remaining function calls failed.\n");
		error_code = __LINE__;
		goto ERROR_CASE;
	}

	#if 0  // For debug: print Hier
	printf("\nDEBUG %s:%u : Hier after replacement of CALL nodes\n\n", __FILE__, __LINE__);
	Hier_Print(Implem->H);
	printf("\n");
	exit(EXIT_FAILURE);
	#endif

	// Here, find the top-level processes
	z = Build_DetectTopLevels(Implem);
	if(z!=0)  {
		printf("Error: Detection of top-level components failed (code %i)\n", z);
		error_code = __LINE__;
		goto ERROR_CASE;
	}

	ERROR_CASE:

	// FIXME In case there are several top-levels: Free them all?
	// Beware: other modImplems may have been created by explicitely loading C files
	if(error_code!=0) {
		if(Implem->H!=NULL) { Hier_Free(Implem->H); Implem->H = NULL; }
		Map_Netlist_Implem_Free(Implem);
		Map_Netlist_Implem_Init(Implem);
		avl_pi_reset(&Implem->symbols_flags);
		return error_code;
	}

	return 0;
}


