
#ifndef _MAP_H_
#define _MAP_H_

#include "auto.h"
#include "addops.h"  // For op_usage_t*
#include "../netlist/netlist.h"
#include "../netlist/netlist_comps.h"  // For netlist_fsm_state_t



// The structure that lists all component types
// It lists all those present and available at the current cycle

typedef struct map_comptype_t {
	avl_p_tree_t all;
	// Note: One may think additional trees of avail and used components would be useful
	// But even without using this very structure, mapping time was not very longer
} map_comptype_t;

// Other structures for mapper

typedef struct map_netlist_hwusage_t {

	// The components who are used (only computing components, not MUX nor storage)
	avl_p_tree_t comp_used;

	// MEM: the accesses that are used
	avl_p_tree_t mem_accesses_used;

	// MUX : The input that is used (the "access" structure)
	// To avoid scanning all inputs
	// Key = component pointer, data = access pointer
	avl_pp_tree_t mux_in_used;

	// Value of the FSM output ports
	// Key = FSM port, data = string value
	avl_pp_tree_t fsm_out_value;

} map_netlist_hwusage_t;

// Structure representing a "datapath" :
// The hardware elements used at a clock cycle.
typedef struct map_netlist_datapath_t {
	// The expression this datapaths provides/executes (allocated VEX)
	hvex_dico_elt_t* Expr;
	// Where to get the result (NULL for ASG operation)
	chain_list* valcat;

	map_netlist_hwusage_t hwusage;

	// Sub-datapaths the current one depends on.
	chain_list* sub_datapaths;

} map_netlist_datapath_t;

// This structure contains all useful data for mapping.
// It is used to pass the data to all functions of this mapping process.
typedef struct map_netlist_data_t {
	implem_t*     Implem;

	// Link the Hierarchy nodes and the FSM states
	avl_pp_tree_t node2state;
	avl_pp_tree_t state2node;

	// The ID of a Call, in the context of the destination Proc
	avl_pi_tree_t call2id;

	// The dictionary of expressions
	hvex_dico_t expr_dictionary;
	// Link between HierAction and VexAlloc_Ref
	// This way, operator sharing done in VexAlloc is kept
	avl_pp_tree_t tree_act2ref;
	// Link between allocated expressions and the possible datapaths.
	// Key  = an allocated VEX expression (vex_alloc_elt_t*)
	// Data = a chain_list* of datapath structures
	avl_pp_tree_t expr_datapaths;

	// This structure contains all useful data for the mapping of a state.
	struct {

		// The FSM state being processed
		netlist_fsm_state_t* state;

		// HW usage for the current state
		map_netlist_hwusage_t hwusage;

		// Contains all expressions already mapped. Data = corresponding datapath.
		avl_pp_tree_t expr2datapath;

		// Contains all datapaths used at the current trans.
		avl_p_tree_t all_datapaths;

	} cur_state;

	// Components sorted by type. Indexed by model name.
	// FIXME replace by an index by model pointer
	avl_pp_tree_t comptype;

	// To display errors when mapping actions
	hier_action_t* cur_action;

} map_netlist_data_t;



void Map_Init_Hvex_Callbacks();

void Map_Netlist_Implem_Init(implem_t* Implem);
void Map_Netlist_Implem_Free(implem_t* Implem);

void Map_Netlist_CreateSigClock_check(implem_t* Implem);
void Map_Netlist_CreateSigReset_check(implem_t* Implem);
void Map_Netlist_CreateSigStart_check(implem_t* Implem);

void Map_Netlist_CreateFsm_check(implem_t* Implem);

void Map_Netlist_CreatePortClock(implem_t* Implem);
void Map_Netlist_CreatePortReset(implem_t* Implem);
void Map_Netlist_CreatePortStart(implem_t* Implem);

void Map_Netlist_CheckCreateSystemPorts(implem_t* Implem);
void Map_Netlist_CheckConnectSystemPorts(implem_t* Implem);

void Map_Netlist_MapData_Reset(map_netlist_data_t* map_data);

hier_node_t* Map_FsmState2Node(map_netlist_data_t* data, netlist_fsm_state_t* state);
netlist_fsm_state_t* Map_Node2FsmState(map_netlist_data_t* data, hier_node_t* node);

int  Map_Netlist_AddComponents_InitFromVpn(implem_t* Implem);
int  Map_Netlist_CompleteComponents(implem_t* Implem);
int  Map_Netlist_RemoveComponents(implem_t* Implem);

void Map_Netlist_Simplify(implem_t* Implem);

int  Map_Netlist(implem_t* Implem);

void Map_Netlist_AddSigFsmIn(implem_t* Implem, char* sig_name, char* fsmin_name, unsigned width);
void Map_Netlist_AddSigFsmOut(implem_t* Implem, char* sig_name, char* fsmin_name, unsigned width);

char* Map_Netlist_MakeInstanceName_Prefix_AvoidSyms(implem_t* Implem, char* prefix, avl_p_tree_t* tree_avoid_names);
char* Map_Netlist_MakeInstanceName_TryThis_AvoidSyms(implem_t* Implem, char* name, avl_p_tree_t* tree_avoid_names);

static inline char* Map_Netlist_MakeInstanceName_Prefix(implem_t* Implem, char* prefix) {
	return Map_Netlist_MakeInstanceName_Prefix_AvoidSyms(Implem, prefix, NULL);
}
static inline char* Map_Netlist_MakeInstanceName_TryThis(implem_t* Implem, char* name) {
	return Map_Netlist_MakeInstanceName_TryThis_AvoidSyms(Implem, name, NULL);
}

static inline char* Map_Netlist_MakeInstanceName_AvoidSyms(implem_t* Implem, netlist_comp_model_t* model, avl_p_tree_t* tree_avoid_names) {
	return Map_Netlist_MakeInstanceName_Prefix_AvoidSyms(Implem, model->name, tree_avoid_names);
}
static inline char* Map_Netlist_MakeInstanceName(implem_t* Implem, netlist_comp_model_t* model) {
	return Map_Netlist_MakeInstanceName_Prefix_AvoidSyms(Implem, model->name, NULL);
}

netlist_comp_t* Map_Netlist_GetComponent(implem_t* Implem, const char* name);
netlist_comp_t* Map_Netlist_GetComp_verbose(implem_t* Implem, const char* name, netlist_comp_model_t* model);

netlist_comp_t* Map_Netlist_AddOneOperator(implem_t* Implem, netlist_comp_model_t* model, op_usage_t* usage);
int Map_Netlist_AddSomeOperators(implem_t* Implem, netlist_comp_model_t* model, unsigned number);

void Map_Netlist_GenVHDL(implem_t* Implem);

int Map_Netlist_Dup(implem_t* newImplem);

// Utility functions for mapping

map_comptype_t* Map_CompType_GetAdd(map_netlist_data_t* map_data, char* modelname);
map_comptype_t* Map_CompType_AddComp(map_netlist_data_t* map_data, netlist_comp_t* comp);

map_netlist_datapath_t* Map_Netlist_Datapath_New();
void Map_Netlist_Datapath_Free(map_netlist_datapath_t* datapath);

void Map_Netlist_Datapath_BuildNew_Op_error(
	map_netlist_data_t* map_data, map_netlist_datapath_t* datapath,
	hvex_model_t* op_model, netlist_comp_model_t* comp_model, unsigned fromline
);

netlist_port_t* Map_Netlist_GetSourcePort_Check(netlist_port_t* port);
netlist_comp_t* Map_Netlist_GetSourceMux(netlist_port_t* port);

netlist_access_t* Map_Netlist_Mux_GetAddInput_SetState_internal(
	map_netlist_data_t* map_data, map_netlist_datapath_t* datapath,
	netlist_comp_t* mux, netlist_access_t* muxIn
);
netlist_access_t* Map_Netlist_Mux_GetAddInput_SetState(
	map_netlist_data_t* map_data, map_netlist_datapath_t* datapath,
	netlist_comp_t* mux, chain_list* valcat
);

chain_list* Map_Netlist_ValCat_DupCropToRef(chain_list* valcat, hvex_dico_ref_t* Expr);
chain_list* Map_Netlist_ValCat_DupResizeToRef(chain_list* valcat, hvex_dico_ref_t* Expr);
chain_list* Map_Netlist_ValCat_DupResize(chain_list* valcat, hvex_dico_ref_t* Expr, unsigned width);

void Map_Netlist_Port_MuxGetAddInput_SetState_FromExpr(
	map_netlist_data_t* map_data, map_netlist_datapath_t* datapath, netlist_port_t* port,
	hvex_dico_ref_t* Expr, chain_list* valcat
);

void Map_Netlist_Datapath_AddToState(map_netlist_data_t* map_data, map_netlist_datapath_t* datapath);

map_netlist_datapath_t* Map_Netlist_Datapath_Expr_BuildNew(map_netlist_data_t* map_data, hvex_dico_elt_t* Expr);
map_netlist_datapath_t* Map_Netlist_Datapath_Expr(map_netlist_data_t* map_data, hvex_dico_elt_t* Expr);

// typedefs to help using HVEX callbacks

typedef map_netlist_datapath_t* (*map_hvex_cb_type)(map_netlist_data_t* map_data, hvex_dico_elt_t* Expr);
typedef void* (*map_hvex_cbvoid_type)(void* map_data, void* Expr);

// Command interpreter

int Map_Command(implem_t* Implem, command_t* cmd_data);



#endif  // _MAP_H_

