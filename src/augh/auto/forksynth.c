
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>   // For command-line parameters parsing
#include <time.h>    // To display the scheduling time
#include <signal.h>

#include <fcntl.h>  // to use the pipes with child processes
#include <syslog.h>
#include <sys/wait.h>  // For function wait(), to get exit status of child processes

#include "auto.h"
#include "hierarchy.h"
#include "command.h"
#include "techno.h"
#include "schedule.h"
#include "core.h"
#include "addops.h"

#include "forksynth.h"



//======================================================================
// Parameters, structures
//======================================================================

// For forking and estimating the results of a synthesis
// FIXME This structure is used ONLY in the context of the Forksynth module
typedef struct {
	int want_time;
	int want_resources;
	int pipe[2];
	FILE* pipe_file[2];
	int result_time;
	ptype_list* result_resources;
	// The standard outputs of the child process
	char* stdout_filename;
	int   stdout_append;
	char* stderr_filename;
	int   stderr_append;
} fork_params_t;

// FIXME this structure should not be shared
static fork_params_t fork_params;

void ForkSynth_Init() {
	memset(&fork_params, 0, sizeof(fork_params));
}

static struct {
	int resource;
	int degrees;  // freedom degrees
	int time;
	int silent;
	// Optional commands the child process can apply
	char* commands;
	// An optional freedom degree the child process can apply
	freedom_degree_t* freedom_degree;
	chain_list*       list_freedom_degrees;
} options;

static void ForkSynth_InitOptions_Empty() {
	memset(&options, 0, sizeof(options));
}
static void ForkSynth_InitOptions_Default() {
	memset(&options, 0, sizeof(options));
	options.resource = 1;
	options.degrees = 1;
	options.time = 1;
	options.silent = 1;
}


// Debug parameters
static int debug_child_pause = 0;



//======================================================================
// Child process finishes the synthesis
//======================================================================

static void TestSynth_child_finish(implem_t *Implem) {

	// Open the pipe as buffered
	close(fork_params.pipe[0]);
	fork_params.pipe_file[1] = fdopen(fork_params.pipe[1], "w");

	if(Implem->synth_state.schedule==0) {
		Hier_Update_All(Implem);
		Schedule(Implem, SCHED_ALGO_LIST);
		Hier_Update_All(Implem);
	}

	// Build the freedom degrees

	int degrees_valid = 0;

	if(options.degrees!=0) {
		FreedomDegrees_Build(Implem);
		degrees_valid = 1;
	}

	if(Implem->synth_state.postproc==0) {
		augh_postprocess(Implem);
	}

	// Compute the execution time

	int time_valid = 0;

	if(options.time!=0) {
		Hier_Timing_Update(Implem, false);
		Hier_Timing_Show(Implem);
		time_valid = 1;
	}

	// Evaluate the resource cost

	int resource_valid = 0;

	if(options.resource!=0) {

		if(Implem->CORE.resources_opers!=NULL) Techno_NameOpRes_Free(Implem->CORE.resources_opers);
		Implem->CORE.resources_opers = Techno_Netlist_EvalSize_WithTypes(Implem->synth_target, Implem->netlist.top);
		if(Implem->CORE.resources_total!=NULL) freeptype(Implem->CORE.resources_total);
		Implem->CORE.resources_total = Techno_NameOpRes_GetTotal(Implem->CORE.resources_opers);

		resource_valid = 1;
	}

	// Update the costs and scores of the freedom degrees

	if(degrees_valid!=0) {
		if(resource_valid!=0) {
			FreedomDegrees_SetFinalScore(Implem);
		}
		else degrees_valid = 0;
	}

	// Send the results

	if(options.resource!=0) {
		if(resource_valid!=0) {
			printf("DEBUG %s:%d : Sending resource usage.\n", __FILE__, __LINE__);
			fprintf(fork_params.pipe_file[1], "resources\n");
			Techno_NameOpRes_Print(Implem->CORE.resources_opers);
			Techno_NameOpRes_fprint(fork_params.pipe_file[1], Implem->CORE.resources_opers);
		}
		else {
			printf("DEBUG %s:%d : Resource usage is unknown but was asked.\n", __FILE__, __LINE__);
			fprintf(fork_params.pipe_file[1], "resources-unknown\n");
		}
	}

	if(options.time!=0) {
		if(time_valid!=0) {
			printf("DEBUG %s:%d : Sending time information.\n", __FILE__, __LINE__);
			Hier_Timing_Show(Implem);
			fprintf(fork_params.pipe_file[1], "time ");
			minmax_fprint_raw(fork_params.pipe_file[1], &Implem->time.execution_cycles);
			fprintf(fork_params.pipe_file[1], "\n");
		}
		else {
			printf("DEBUG %s:%d : Time information is unknown, but was asked.\n", __FILE__, __LINE__);
			fprintf(fork_params.pipe_file[1], "time-unknown\n");
		}
	}

	if(options.degrees!=0) {
		if(degrees_valid!=0) {
			printf("DEBUG %s:%d : Sending freedom degrees.\n", __FILE__, __LINE__);
			fprintf(fork_params.pipe_file[1], "degrees\n");
			FreedomDegrees_fprintf(fork_params.pipe_file[1], Implem, NULL);
		}
		else {
			printf("DEBUG %s:%d : Freedom degrees are unknown, but were asked.\n", __FILE__, __LINE__);
			fprintf(fork_params.pipe_file[1], "degrees-unknown\n");
		}
	}

	// Better for debug ?
	fflush(NULL);

	// Close the file
	fclose(fork_params.pipe_file[1]);
}

static int TestSynth_parent_wait(implem_t *Implem, forksynth_carac_t* dest) {
	// Open the pipe as buffered
	close(fork_params.pipe[1]);
	fork_params.pipe_file[0] = fdopen(fork_params.pipe[0], "r");

	int z;

	#if 0  // DEBUG only dump on the screen what is received (each char)
	printf("DEBUG %s:%d : Dumping what is sent by the child process.\n", __FILE__, __LINE__);
	do {
		int c = fgetc(fork_params.pipe_file[0]);
		if(c==EOF) break;
		putchar(c);
	} while(1);
	exit(EXIT_FAILURE);
	#endif

	// Read the resources used
	if(options.resource!=0) {
		char buffer[500];
		z = fscanf(fork_params.pipe_file[0], "%s", buffer);
		if(z!=1)
			return __LINE__;
		if(strcmp(buffer, "resources")!=0)
			return __LINE__;

		z = Techno_NameOpRes_fscanf(fork_params.pipe_file[0], &dest->res_oper);
		if(z!=0)
			return __LINE__;
		dest->res_total = Techno_NameOpRes_GetTotal(dest->res_oper);
	}

	// Read the execution time
	if(options.time!=0) {
		char buffer[500];
		z = fscanf(fork_params.pipe_file[0], "%s", buffer);
		if(z!=1)
			return __LINE__;
		if(strcmp(buffer, "time")!=0)
			return __LINE__;
		z = minmax_fscanf_raw(fork_params.pipe_file[0], &dest->time);
		if(z!=0)
			return __LINE__;
	}

	// Read the freedom degrees
	if(options.degrees!=0) {
		char buffer[500];
		z = fscanf(fork_params.pipe_file[0], "%s", buffer);
		if(z!=1)
			return __LINE__;
		if(strcmp(buffer, "degrees")!=0)
			return __LINE__;
		z = FreedomDegrees_fscanf(fork_params.pipe_file[0], Implem, &dest->degrees);
		if(z!=0)
			return __LINE__;
	}

	// Read all trailing characters. Paranoia.
	while( fgetc(fork_params.pipe_file[0])!=EOF ) ;

	// Close the file
	fclose(fork_params.pipe_file[0]);

	return 0;
}

static void TestSynth_open_pipe() {
	// Ignore when the child exits
	struct sigaction sigact;
	memset(&sigact, 0, sizeof(sigact));
	sigact.sa_handler = SIG_IGN;
	sigaction(SIGPIPE, &sigact, NULL);
	// Create the pipe
	pipe(fork_params.pipe);
}

// Close file descriptors, reopen standard I/O
static void TestSynth_process_silent() {
	close(0); close(1); close(2);
	int z;
	// Open stdin
	int i = open("/dev/null", O_RDWR);
	// Open stdout
	z = -1;
	if(fork_params.stdout_filename!=NULL) {
		int flags = O_RDWR | O_CREAT;
		if(fork_params.stdout_append!=0) flags |= O_APPEND;
		z = open(fork_params.stdout_filename, flags);
		if(z<0) syslog(LOG_ERR, "Warning : Could not open descriptor '%s' for stdout.\n", fork_params.stdout_filename);
	}
	if(z<0) {
		z = dup(i);
		if(z<0) { syslog(LOG_ERR, "Could not duplicate descriptor for stdout.\n"); exit(EXIT_FAILURE); }
	}
	// Open stderr. Try to duplicate stdout if filenames match.
	z = -1;
	if(fork_params.stderr_filename==fork_params.stdout_filename) {
		z = dup(1);
		if(z<0) syslog(LOG_ERR, "Warning : Could not duplicate stdout for stderr.\n");
	}
	else if(fork_params.stderr_filename!=NULL) {
		int flags = O_RDWR | O_CREAT;
		if(fork_params.stderr_append!=0) flags |= O_APPEND;
        z = open(fork_params.stderr_filename, flags);
		if(z<0) syslog(LOG_ERR, "Warning : Could not open descriptor '%s' for stderr.\n", fork_params.stderr_filename);
	}
	if(z<0) {
		z = dup(i);
		if(z<0) { syslog(LOG_ERR, "Could not duplicate descriptor for stderr.\n"); exit(EXIT_FAILURE); }
	}
}



//======================================================================
// Silently return the resources and time after a full synthesis
//======================================================================

void ForkSynth_SetOptions(const char* params) {
	// Set the parameters
	if(params==NULL) {
		// Default
		ForkSynth_InitOptions_Default();
	}
	else {
		ForkSynth_InitOptions_Empty();
		// Parse the parameters
		for(char const * ptr=params; *ptr!=0; ptr++) {
			if(*ptr=='d') options.degrees = 1;
			if(*ptr=='r') options.resource = 1;
			if(*ptr=='t') options.time = 1;
			if(*ptr=='s') options.silent = 1;
			if(*ptr=='v') options.silent = 0;
		}
	}
}

static int ForkSynth_GetCarac_internal(implem_t* Implem, forksynth_carac_t* carac) {

	ForkSynth_InitCarac(carac);

	// Ensure coherence of options
	if(options.degrees!=0) {
		options.resource = 1;
		options.time = 1;
	}

	// Create the communication channel between the parent and the child
	TestSynth_open_pipe();

	// Flush all open output streams
	fflush(NULL);

	// Create the child process
	pid_t pid = fork();

	// DEBUG : the child pauses to let user attach GDB
	if(debug_child_pause!=0) {
		if(pid==0) {  // This is the child
			printf("DEBUG : child process PID %d waiting.\n", getpid());
			for(int i=1; i>0; i=i) sleep(1);
		}
		else {
			printf("DEBUG : child process PID %d waiting.\n", pid);
		}
	}

	if(pid==0) {  // This is the child

		// If silent, close the standard inputs
		if(options.silent!=0) {
			TestSynth_process_silent();
		}
		// Apply the transformations
		if(options.commands!=NULL) AughCmd_Exec_str(Implem, options.commands);
		// Apply the transformations
		if(options.freedom_degree!=NULL) {
			int z = Core_FreedomDegree_Apply(Implem, options.freedom_degree, 0);
			if(z!=0) exit(EXIT_FAILURE);
			Hier_Update_SimpStruct(Implem);
		}
		if(options.list_freedom_degrees!=NULL) {
			int z = Core_FreedomDegree_ApplyList(Implem, options.list_freedom_degrees, NULL, 0);
			if(z!=0) exit(EXIT_FAILURE);
			Hier_Update_SimpStruct(Implem);
		}

		// Launch a fake synthesis
		TestSynth_child_finish(Implem);

		// And exit
		_exit(EXIT_SUCCESS);
	}

	// Wait until the child has finished.
	int z = TestSynth_parent_wait(Implem, carac);

	int recv_status = 0;
	pid_t recv_pid = wait(&recv_status);

	if(recv_pid!=pid) {
		errprintf("Got status from child process %i, expected %i (status value %i).\n", recv_pid, pid, recv_status);
		abort();
	}
	if(recv_status!=EXIT_SUCCESS) {
		errprintf("Child process status is %i.\n", recv_status);
		return recv_status;
	}

	return z;
}

int ForkSynth_GetCarac(implem_t* Implem, forksynth_carac_t* carac) {
	return ForkSynth_GetCarac_internal(Implem, carac);
}

int ForkSynth_GetCarac_FreedomDegree(implem_t* Implem, forksynth_carac_t* carac, freedom_degree_t* degree) {
	options.freedom_degree = degree;
	int z = ForkSynth_GetCarac_internal(Implem, carac);
	options.freedom_degree = NULL;
	return z;
}
int ForkSynth_GetCarac_ListFreedomDegrees(implem_t* Implem, forksynth_carac_t* carac, chain_list* list) {
	options.list_freedom_degrees = list;
	int z = ForkSynth_GetCarac_internal(Implem, carac);
	options.list_freedom_degrees = NULL;
	return z;
}

int ForkSynth_GetAndSetCarac(implem_t* Implem) {
	forksynth_carac_t carac;
	int z = ForkSynth_GetCarac_internal(Implem, &carac);
	if(z!=0) return z;
	ForkSynth_SetCarac(Implem, &carac);
	return 0;
}


// Set the data to an Implem structure
void ForkSynth_SetCarac(implem_t* Implem, forksynth_carac_t* carac) {
	if(options.resource!=0) {
		if(Implem->CORE.resources_opers!=NULL) Techno_NameOpRes_Free(Implem->CORE.resources_opers);
		Implem->CORE.resources_opers = carac->res_oper;
		if(Implem->CORE.resources_total!=NULL) freeptype(Implem->CORE.resources_total);
		Implem->CORE.resources_total = carac->res_total;
	}
	if(options.time!=0) {
		Implem->CORE.execution_time = carac->time;
	}
	if(options.degrees!=0) {
		if(Implem->CORE.freedom_degrees!=NULL) freedom_degree_freelist(Implem->CORE.freedom_degrees);
		Implem->CORE.freedom_degrees = carac->degrees;
	}
}
// Free the data
void ForkSynth_FreeCarac(forksynth_carac_t* carac) {
	Techno_NameOpRes_Free(carac->res_oper);
	freeptype(carac->res_total);
	freedom_degree_freelist(carac->degrees);
}

void ForkSynth_InitCarac(forksynth_carac_t* carac) {
	carac->res_oper = NULL;
	carac->res_total = NULL;
	carac->degrees = NULL;
	minmax_set_cst(&carac->time, 0);
}



//======================================================================
// Command-line interpreter
//======================================================================

int TestSynth_Command(implem_t* Implem, command_t* cmd_data) {
	const char* cmd = Command_PopParam(cmd_data);
	if(cmd==NULL) {
		printf("Error : No command received. Try 'help'.\n");
		return -1;
	}

	if(strcmp(cmd, "help")==0) {
		printf(
			"ForkSynth : This module does a fork(), "
			"the child process applies some transformations on its implementation, "
			"finishes the synthesis, displays its characteristics, and dies.\n"
			"\n"
			"Possible commands :\n"
			"  help            Display this help.\n"
			"  launch <type> <commands>\n"
			"    <type>        A set of characters, beginning with.\n"
			"                  'd' : Freedom degrees are asked [not by default].\n"
			"                  'r' : Resource usage is asked [not by default].\n"
			"                  't' : Execution time is asked [not by default].\n"
			"                  's' : Silent execution [not by default].\n"
			"                  'v' : Verbose execution [default].\n"
			"    <commands>    The list of transformations to apply.\n"
			"                  The syntax of the commands is the same as for the present command interpreter.\n"
			"Set parameters :\n"
			"\n"
			"  redirect <stdout|stderr|both> set <filename>\n"
			"                                append <filename>\n"
			"                                unset\n"
			"    The child process' standard or error output is written or appended\n"
			"    to the specified file, or /dev/null by default.\n"
			"\n"
			"  debug-child-pause [<bool>]   Pause at child process creation. Init=no, default=true.\n"
			"                               It lets time to connect GDB for debug.\n"
		);
		return 0;
	}

	else if(strcmp(cmd, "redirect")==0) {
		// Get the standard file
		int file_is_stdout = 0;
		int file_is_stderr = 0;
		const char* standard_file = Command_PopParam(cmd_data);
		if(standard_file==NULL) {
			printf("Error 'testsynth', command '%s' : Missing parameter. Try 'help'.\n", cmd);
			return -1;
		}
		if(strcmp(standard_file, "stdout")==0) file_is_stdout = 1;
		else if(strcmp(standard_file, "stderr")==0) file_is_stderr = 1;
		else if(strcmp(standard_file, "both")==0) { file_is_stdout = 1; file_is_stderr = 1; }
		else {
			printf("Error 'testsynth', command '%s' : Wrong parameter '%s'. Try 'help'.\n", cmd, standard_file);
			return -1;
		}
		// Get the action
		const char* action = Command_PopParam(cmd_data);
		if(strcmp(action, "unset")==0) {
			if(file_is_stdout!=0) fork_params.stdout_filename = NULL;
			if(file_is_stderr!=0) fork_params.stderr_filename = NULL;
		}
		else if(strcmp(action, "set")==0 || strcmp(action, "append")==0) {
			char* filename = Command_PopParam_stralloc(cmd_data);
			if(filename==NULL) {
				printf("Error 'testsynth', command '%s %s' : A file name must be given. Try 'help'.\n", cmd, action);
				return -1;
			}
			if(file_is_stdout!=0) fork_params.stdout_filename = filename;
			if(file_is_stderr!=0) fork_params.stderr_filename = filename;
			if(strcmp(action, "set")==0) {
				if(file_is_stdout!=0) fork_params.stdout_append = 0;
				if(file_is_stderr!=0) fork_params.stderr_append = 0;
			}
			else {
				if(file_is_stdout!=0) fork_params.stdout_append = 1;
				if(file_is_stderr!=0) fork_params.stderr_append = 1;
			}
		}
		else {
			printf("Error 'testsynth', command '%s' : Unknown parameter '%s'.\n", cmd, action);
			return -1;
		}
	}
	else if(strcmp(cmd, "debug-child-pause")==0) {
		int z = Command_PopParam_YesNoDef_Verbose(cmd_data, true, cmd);
		if(z<0) return -1;
		debug_child_pause = z;
	}

	else if(strcmp(cmd, "launch")==0) {
		// Here a TestSynth process is launched.

		// Set the parameters
		const char* params = Command_PopParam(cmd_data);
		ForkSynth_SetOptions(params);

		// Launch the process
		forksynth_carac_t carac;
		int z = ForkSynth_GetCarac(Implem, &carac);
		if(z!=0) {
			printf("Error : The process returned an error (code %d).\n", z);
			return -1;
		}

		// Display the data, only if the child does not.
		if(options.silent!=0) {
			Techno_NameOpRes_Print(carac.res_oper);
			minmax_print(&carac.time, "Execution time ", "\n");
		}
		// Free the data
		ForkSynth_FreeCarac(&carac);
	}

	else {
		printf("Error 'ForkSynth' : Unknown command '%s'.\n", cmd);
		return -1;
	}

	return 0;
}


