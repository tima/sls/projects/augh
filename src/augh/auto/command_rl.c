
/*
This is a dummy source file, needed to isolate the call to readline()
from the rest of the application.
This is because the readline header defines things that can conflict
with AUGH code.
For example a macro called 'RETURN' conflicted with things inherited from UGH.
*/

#include <stdio.h>
#include <readline/readline.h>
#include <readline/history.h>


// Call to readline to get a full line
char* command_rl(char* prompt) {
	return readline(prompt);
}

// Add a line in the readline history
void command_rl_hist(char* command) {
	add_history(command);
}


