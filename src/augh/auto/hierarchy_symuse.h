
#ifndef _HIERARCHY_SYMUSE_H_
#define _HIERARCHY_SYMUSE_H_

#include <stdint.h>

#include "../chain.h"
#include "../indent.h"
#include "auto.h"
#include "core.h"



/* Explanations about register usage flags

Here is how flags about symbol usage should be interpreted, on a few exemples.
We are interested only in the register "a" on these examples.

Example 1:

state1 -W-  a = <trucs>;
state2 --L  <nothing-that-involves-a>;
state3 R--  b = a;

Example 2:

state1 -W-  a = <trucs>;
state2 --L  <nothing-that-involves-a>;
state3 R-L  b = a;
state2 --L  <nothing-that-involves-a>;
state3 R--  c = a;

Example 3:

state1 -W-  a = <trucs>;
state2 --L  <nothing-that-involves-a>;
state3 RW-  a = a + 1;
state2 --L  <nothing-that-involves-a>;
state3 R--  b = a;

Conclusion :

The flags LIVE cover the states BETWEEN a Write and the furthest next Read.
But they are not set on "frontier" states where the symbol is read or written.

If you want them to be set, use the parameters live_frontier_r and live_frontier_w
of the function Hier_SymUse_FillHier_opt().

*/

// Flags allowing to keep track of register usage inside a basic block

// Fags to each bit of symbol in States
#define BB_SYMUSE_IS_R      0x01
#define BB_SYMUSE_IS_W      0x02
#define BB_SYMUSE_IS_LIVE   0x04
#define BB_SYMUSE_IS_LA     0x08  // Live after the Write

// Summary flags for BB, levels, other nodes
#define BB_SYMUSE_R_FIRST   0x10
#define BB_SYMUSE_W_FIRST   0x20

// In case a user wants to do something personal...
#define BB_SYMUSE_USER1     0x40
#define BB_SYMUSE_USER2     0x80

typedef struct bb_symuse_t {
	// Remainder of the symbol name and width
	char*    name;
	unsigned width;

	// In this array, there is one cell per symbol bit
	// index = index in the symbol, data = usage flags
	// Note: this array can be NULL. In this case, the summary field below is used.
	uint8_t* bits;

	// This is the summary (an OR operation on all cells)
	// Warning: this flag can contain simultaneously R_FIRST and W_FIRST
	uint8_t allbits;

} bb_symuse_t;



// Basic functions

void Hier_SymUse_Free(bb_symuse_t* elt);
void Hier_SymUse_Clear(bb_symuse_t* symuse);

bb_symuse_t* Hier_SymUse_NewInit(char* name, unsigned width);
bb_symuse_t* Hier_SymUse_GetAdd(avl_pp_tree_t* tree, char* name, unsigned width);
bb_symuse_t* Hier_SymUse_Get(avl_pp_tree_t* tree, char* name);
void         Hier_SymUse_Rem(avl_pp_tree_t* tree, char* name);

static inline bb_symuse_t* Hier_Node_SymUse_GetAdd(hier_node_t* node, char* name, unsigned width) {
	return Hier_SymUse_GetAdd(&node->symuse, name, width);
}
static inline bb_symuse_t* Hier_Node_SymUse_Get(hier_node_t* node, char* name) {
	return Hier_SymUse_Get(&node->symuse, name);
}
static inline void Hier_Node_SymUse_Rem(hier_node_t* node, char* name) {
	return Hier_SymUse_Rem(&node->symuse, name);
}

void Hier_SymUse_ClearNode(hier_node_t* node);
void Hier_SymUse_ClearImplem(implem_t* Implem);

void Hier_SymUse_FPrint_Bit(FILE* F, uint8_t val);
static inline void Hier_SymUse_Print_Bit(uint8_t val) {
	Hier_SymUse_FPrint_Bit(stdout, val);
}

void Hier_SymUse_FPrint_Sym(FILE* F, bb_symuse_t* symuse);
static inline void Hier_SymUse_Print_Sym(bb_symuse_t* symuse) {
	Hier_SymUse_FPrint_Sym(stdout, symuse);
}

void Hier_SymUse_FPrint_Implem(FILE* F, implem_t* Implem);
static inline void Hier_SymUse_Print_Implem(implem_t* Implem) {
	Hier_SymUse_FPrint_Implem(stdout, Implem);
}

void Hier_SymUse_FPrint_BB(FILE* F, hier_node_t* nodeBB);
static inline void Hier_SymUse_Print_BB(hier_node_t* nodeBB) {
	Hier_SymUse_FPrint_BB(stdout, nodeBB);
}


// Adding flag R & W

void Hier_SymUse_Vex_SetFlag(implem_t* Implem, avl_pp_tree_t* tree, hvex_t* Expr, uint8_t flag);
static inline void Hier_SymUse_Vex_SetFlag_R(implem_t* Implem, avl_pp_tree_t* tree, hvex_t* Expr) {
	Hier_SymUse_Vex_SetFlag(Implem, tree, Expr, BB_SYMUSE_IS_R);
}
static inline void Hier_SymUse_Vex_SetFlag_W(implem_t* Implem, avl_pp_tree_t* tree, hvex_t* Expr) {
	Hier_SymUse_Vex_SetFlag(Implem, tree, Expr, BB_SYMUSE_IS_W);
}

void Hier_SymUse_Action_SetRW(implem_t* Implem, avl_pp_tree_t* tree, hier_action_t* action);
void Hier_SymUse_State_SetRW(implem_t* Implem, hier_node_t* node);
void Hier_SymUse_State_RedoRW(implem_t* Implem, hier_node_t* node);


// Adding LIVE flag

void Hier_SymUse_FillHier(implem_t* Implem);
void Hier_SymUse_State_UpdLive(implem_t* Implem, hier_node_t* node);

// To re-generate all flags if an Action have been modified
static inline void Hier_SymUse_State_Upd(implem_t* Implem, hier_node_t* node) {
	// Remove and re-set all flags R and W
	Hier_SymUse_State_RedoRW(Implem, node);
	// Update the flags LIVE, LA
	Hier_SymUse_State_UpdLive(Implem, node);
}


// Simplification of the Hier graph

int Hier_SymUse_RemUnusedAct(implem_t* Implem);



#endif  // _HIERARCHY_SYMUSE_H_

