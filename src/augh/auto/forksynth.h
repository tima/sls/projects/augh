

#ifndef _FORKSYNTH_H_
#define _FORKSYNTH_H_

#include "auto.h"


//======================================================================
// Declarations of structures
//======================================================================

typedef struct {
	// Field DATA = name of the resource (string "dff", "lut6")
	// Field TYPE = amount used
	ptype_list*  res_total;
	// Field DATA_FROM = name of the component type (string "add", "mux", "fsm")
	// Field TYPE      = The number of components
	// Field DATA_TO   = The total resources used: a ptype_list
	bitype_list* res_oper;
	// The execution time
	minmax_t     time;
	// The freedom degrees
	chain_list*  degrees;
} forksynth_carac_t;


//======================================================================
// Declarations of functions
//======================================================================

void ForkSynth_Init();

void ForkSynth_SetOptions(const char* params);

void ForkSynth_SetCarac(implem_t* Implem, forksynth_carac_t* carac);
int  ForkSynth_GetCarac(implem_t* Implem, forksynth_carac_t* carac);
int  ForkSynth_GetAndSetCarac(implem_t* Implem);
int  ForkSynth_GetCarac_FreedomDegree(implem_t* Implem, forksynth_carac_t* carac, freedom_degree_t* degree);
int  ForkSynth_GetCarac_ListFreedomDegrees(implem_t* Implem, forksynth_carac_t* carac, chain_list* list);

void ForkSynth_InitCarac(forksynth_carac_t* carac);
void ForkSynth_FreeCarac(forksynth_carac_t* carac);

int TestSynth_Command(implem_t* Implem, command_t* cmd_data);


#endif  // _FORKSYNTH_H_

