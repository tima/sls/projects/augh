
#ifndef _COMMAND_H_
#define _COMMAND_H_

#include "stdbool.h"

#include "auto.h"


// Data types

typedef struct command_t  command_t;
struct command_t {

	// Flags to indicate exit, etc
	int control;

	// Optional plugin-specific data storage
	void* data;

	// The arbitrary data from which the next word is fetched
	// e.g. for augh historical command interpreter, this is a char*
	// For TCL, python etc this would a be a structure containing an array of TCL objects, other data structures...
	void* data_command;
	// The callback to get the next word
	// Can be from augh command interpreter, TCL, Python, etc
	const char* (*callback_nextword)(command_t* cmd);
	// The callback to get the full line buffer, editable
	char* (*callback_editbuf)(command_t* cmd);

	// Result of the command: a chain_list* of stralloc'd strings
	// FIXME should avoid stralloc(), may lead to excessive permanent storage in dictionary ?
	chain_list* list_res;
	// Callback to print all results
	void (*callback_result_finish)(command_t* cmd);

};

// Type definition for callback functions
typedef int (*augh_cmd_interp_t)(implem_t* Implem, const char* filename);

// Constants

#define COMMAND_CTL_DIE     1
#define COMMAND_CTL_EXIT    2
#define COMMAND_CTL_MANUAL  3

// Getting commands

int Command_CmpStr_YesNo(const char* str);

int Command_PopParam_YesNoDef(command_t* cmd_data, int def);
int Command_PopParam_YesNoDef_Verbose(command_t* cmd_data, int def, const char* from_cmd);

int Command_PopParam_WholeLine_stralloc_verbose(command_t* cmd_data, const char* cmd, char** pline);

static inline const char* Command_PopParam(command_t* cmd_data) {
	return cmd_data->callback_nextword(cmd_data);
}
// FIXME This should return type "const char *", namealloc too
static inline char* Command_PopParam_namealloc(command_t* cmd_data) {
	return namealloc( cmd_data->callback_nextword(cmd_data) );
}
static inline char* Command_PopParam_stralloc(command_t* cmd_data) {
	return stralloc( cmd_data->callback_nextword(cmd_data) );
}

static inline int Command_PopParam_YesNo(command_t* cmd_data) {
	return Command_PopParam_YesNoDef(cmd_data, -1);
}
static inline int Command_PopParam_YesNo_Verbose(command_t* cmd_data, const char* from_cmd) {
	return Command_PopParam_YesNoDef_Verbose(cmd_data, -1, from_cmd);
}

// Some callback functions

const char* AughCmd_callback_nextword(command_t* cmd_data);
char* AughCmd_callback_editbuf(command_t* cmd_data);

// Setting result

static inline void Command_Result_AddStatic(command_t* cmd_data, const char* str) {
	cmd_data->list_res = addchain(cmd_data->list_res, (char*)str);
}

static inline void Command_Result_AddCopy(command_t* cmd_data, const char* str) {
	cmd_data->list_res = addchain(cmd_data->list_res, stralloc(str));
}

// Main functions

int AughCmd_Exec(implem_t* Implem, command_t* cmd_data);
int AughCmd_Exec_str(implem_t* Implem, char* commands);

int AughCmd_Interactive(implem_t* Implem);
int AughCmd_Script(implem_t* Implem, const char* filename);

bool AughCmd_Loadrc(const char* path, implem_t* Implem);

void AughCmd_Decl_Interp(const char* ext, augh_cmd_interp_t interp);

void AughCmd_Init();


#endif  // _COMMAND_H_

