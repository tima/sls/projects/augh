
// This module performs operator mapping and netlist generation.
// The input Hierarchy graph is not modified.

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#include "auto.h"
#include "linelist.h"
#include "addops.h"
#include "schedule.h"
#include "map.h"
#include "techno.h"
#include "techno_delay.h"
#include "../hvex/hvex_misc.h"

#include "../netlist/netlist.h"
#include "../netlist/netlist_comps.h"
#include "../netlist/netlist_access.h"
#include "../netlist/netlist_vhdl.h"
#include "../netlist/netlist_cmd.h"
#include "../netlist/netlist_simp.h"


// Filename to output FSM states and associates Actions
// FIXME all is dumped also for mapping of a DupImplem, furthermode there is filename pb
static char* filename_dump_states = NULL;

static unsigned fsmstates_addcycles = 0;
static unsigned fsmstates_setcycles = 0;  // Disabled if zero
static bool     fsmstates_dupcycles = false;

// FIXME this is set to false because extremely preliminary
static bool do_timing_analysis = false;



// Initialization of the main netlist field of an Implem structure
void Map_Netlist_Implem_Init(implem_t* Implem) {
	memset(&Implem->netlist, 0, sizeof(Implem->netlist));

	// Create the top-level component
	char* name = namealloc(Implem->env.entity);
	netlist_comp_t* top = Netlist_Comp_Top_New(name);
	Implem->netlist.top = top;

	// Add control signals and ports and link them
	Map_Netlist_CreateSigClock_check(Implem);
	Map_Netlist_CreateSigReset_check(Implem);
}
void Map_Netlist_Implem_Free(implem_t* Implem) {
	if(Implem->netlist.top==NULL) return;

	// Recursively free the entire circuit
	Netlist_Comp_Free(Implem->netlist.top);

	// Free the mapping data
	if(Implem->netlist.mapping!=NULL) {
		Map_Netlist_MapData_Reset(Implem->netlist.mapping);
		free(Implem->netlist.mapping);
	}

	// And aggressively clear the rest :)
	memset(&Implem->netlist, 0, sizeof(Implem->netlist));
}

void Map_Netlist_CreateSigClock_check(implem_t* Implem) {
	if(Implem->netlist.sig_clock!=NULL) return;
	char* name = namealloc("sig_clock");
	Implem->netlist.sig_clock = Netlist_Comp_Sig_New(name, 1);
	Netlist_Comp_SetChild(Implem->netlist.top, Implem->netlist.sig_clock);
	Implem_SymFlag_Add(Implem, name, SYM_PROTECT_NODEL);
}
void Map_Netlist_CreateSigReset_check(implem_t* Implem) {
	if(Implem->netlist.sig_reset!=NULL) return;
	char* name = namealloc("sig_reset");
	Implem->netlist.sig_reset = Netlist_Comp_Sig_New(name, 1);
	Netlist_Comp_SetChild(Implem->netlist.top, Implem->netlist.sig_reset);
	Implem_SymFlag_Add(Implem, name, SYM_PROTECT_NODEL);
}
void Map_Netlist_CreateSigStart_check(implem_t* Implem) {
	if(Implem->netlist.sig_start!=NULL) return;
	char* name = namealloc("sig_start");
	Implem->netlist.sig_start = Netlist_Comp_Sig_New(name, 1);
	Netlist_Comp_SetChild(Implem->netlist.top, Implem->netlist.sig_start);
	Implem_SymFlag_Add(Implem, name, SYM_PROTECT_NODEL);
}

void Map_Netlist_CreateFsm_check(implem_t* Implem) {
	if(Implem->netlist.fsm!=NULL) return;
	char* name = Map_Netlist_MakeInstanceName(Implem, NETLIST_COMP_FSM);
	netlist_comp_t* fsm = Netlist_Comp_Fsm_New(name);
	Netlist_Comp_SetChild(Implem->netlist.top, fsm);
	Implem->netlist.fsm = fsm;
}

void Map_Netlist_CreatePortClock(implem_t* Implem) {
	Map_Netlist_CreateSigClock_check(Implem);
	// Create the port
	char* name = namealloc("clock");
	Implem->netlist.port_clock = Netlist_Comp_AddPortAccess_Clock(Implem->netlist.top, name);
	// Connect the port
	if(Implem->netlist.sig_clock!=NULL) {
		netlist_signal_t* sig_data = Implem->netlist.sig_clock->data;
		if(sig_data->port_in->source!=NULL) {
			netlist_val_cat_t* valcat = sig_data->port_in->source->DATA;
			if(valcat->source!=NULL) {
				printf("WARNING: Overwriting clock source from comp '%s' type '%s' port '%s' to top-level port '%s'\n",
					valcat->source->component->name,
					valcat->source->component->model->name,
					valcat->source->name,
					Implem->netlist.port_clock->name
				);
			}
			Netlist_ValCat_FreeList(sig_data->port_in->source);
		}
		sig_data->port_in->source = Netlist_ValCat_MakeList_FullPort(Implem->netlist.port_clock);
	}
}
void Map_Netlist_CreatePortReset(implem_t* Implem) {
	Map_Netlist_CreateSigReset_check(Implem);
	// Create the port
	char* name = namealloc("reset");
	Implem->netlist.port_reset = Netlist_Comp_AddPortAccess_Reset(Implem->netlist.top, name);
	// Connect the port
	if(Implem->netlist.sig_reset!=NULL) {
		netlist_signal_t* sig_data = Implem->netlist.sig_reset->data;
		if(sig_data->port_in->source!=NULL) {
			netlist_val_cat_t* valcat = sig_data->port_in->source->DATA;
			if(valcat->source!=NULL) {
				printf("WARNING: Overwriting reset source from comp '%s' type '%s' port '%s' to top-level port '%s'\n",
					valcat->source->component->name,
					valcat->source->component->model->name,
					valcat->source->name,
					Implem->netlist.port_reset->name
				);
			}
			Netlist_ValCat_FreeList(sig_data->port_in->source);
		}
		sig_data->port_in->source = Netlist_ValCat_MakeList_FullPort(Implem->netlist.port_reset);
	}
}
void Map_Netlist_CreatePortStart(implem_t* Implem) {
	Map_Netlist_CreateSigStart_check(Implem);
	// Create the port
	char* name = namealloc("start");
	Implem->netlist.port_start = Netlist_Comp_AddPortAccess_Start(Implem->netlist.top, name);
	// Connect the port
	if(Implem->netlist.sig_start!=NULL) {
		netlist_signal_t* sig_data = Implem->netlist.sig_start->data;
		if(sig_data->port_in->source!=NULL) {
			netlist_val_cat_t* valcat = sig_data->port_in->source->DATA;
			if(valcat->source!=NULL) {
				printf("WARNING: Overwriting start source from comp '%s' type '%s' port '%s' to top-level port '%s'\n",
					valcat->source->component->name,
					valcat->source->component->model->name,
					valcat->source->name,
					Implem->netlist.port_start->name
				);
			}
			Netlist_ValCat_FreeList(sig_data->port_in->source);
		}
		sig_data->port_in->source = Netlist_ValCat_MakeList_FullPort(Implem->netlist.port_start);
	}
}

void Map_Netlist_CheckCreateSystemPorts(implem_t* Implem) {

	// Add missing clock and reset sources
	//   (Optional because these signals can be created with special components/interfaces)
	netlist_signal_t* sig_data;

	if(Implem->netlist.sig_clock!=NULL) {
		// Note: no clock means the design is a combinatorial circuit
		sig_data = Implem->netlist.sig_clock->data;
		if(sig_data->port_in->source==NULL) Map_Netlist_CreatePortClock(Implem);
	}

	if(Implem->netlist.sig_reset!=NULL) {
		sig_data = Implem->netlist.sig_reset->data;
		if(sig_data->port_in->source==NULL) {
			if(Implem->synth_target->board_fpga!=NULL) {
				// If a board is targeted then the board does not provide reset functionality
				// Do not create a top-level reset port

				// FIXME the reset sig is not deleted but is should? Netlist-simp removes it anyway.
				//   Or it's not removed when it's connected to something...

				// Default action
				if(Implem->synth_target->reset_active_state=='0') {
					sig_data->port_in->source = Netlist_ValCat_MakeList_Literal("1");
				}
				else {
					sig_data->port_in->source = Netlist_ValCat_MakeList_Literal("0");
				}

			}
			else {
				// Here we are not targeting a board, so create a top-level reset port
				Map_Netlist_CreatePortReset(Implem);
			}
		}
	}

	if(Implem->netlist.sig_start!=NULL) {
		sig_data = Implem->netlist.sig_start->data;
		if(sig_data->port_in->source==NULL) Map_Netlist_CreatePortStart(Implem);
	}

}

void Map_Netlist_CheckConnectSystemPorts(implem_t* Implem) {

	// Add missing clock and reset sources
	//   (Optional because these signals can be created with special components/interfaces)
	Map_Netlist_CheckCreateSystemPorts(Implem);

	printf("Connecting the system ports (clock, reset, start)...\n");

	avl_pp_foreach(&Implem->netlist.top->children, scanChild) {
		netlist_comp_t* comp = scanChild->data;
		avl_pp_foreach(&comp->ports, scanPort) {
			netlist_port_t* port = scanPort->data;
			if(port->access==NULL) continue;
			if(port->access->model==NETLIST_ACCESS_CLOCK) {
				if(port->source==NULL) {
					if(Implem->netlist.sig_clock==NULL) {
						errprintf("The component '%s' model '%s' has a clock input but the top-level doesn't provide a clock.\n", comp->name, comp->model->name);
						abort();
					}
					port->source = Netlist_ValCat_MakeList_Sig(Implem->netlist.sig_clock);
					Netlist_Access_Clock_SetFrequency(port->access, Implem->synth_target->clk_freq);
				}
			}
			if(port->access->model==NETLIST_ACCESS_RESET) {
				if(port->source==NULL) {
					if(Implem->netlist.sig_reset!=NULL) {
						port->source = Netlist_ValCat_MakeList_Sig(Implem->netlist.sig_reset);
						Netlist_Access_Reset_SetActiveState(port->access, Implem->synth_target->reset_active_state);
					}
					else {
						port->source = Netlist_ValCat_MakeList_Literal("0");
						Netlist_Access_Reset_SetActiveState(port->access, '1');
					}
				}
			}
			if(port->access->model==NETLIST_ACCESS_START) {
				if(port->source==NULL) {
					if(Implem->netlist.sig_start!=NULL) {
						port->source = Netlist_ValCat_MakeList_Sig(Implem->netlist.sig_start);
					}
					else {
						port->source = Netlist_ValCat_MakeList_Literal("1");
					}
				}
			}
		}  // Scan ports
	}  // Scan components

}

// Mostly used by the elaboration core
netlist_comp_t* Map_Netlist_AddOneOperator(implem_t* Implem, netlist_comp_model_t* model, op_usage_t* usage) {
	if(model==NETLIST_COMP_ADD) {
		char* ins_name = Map_Netlist_MakeInstanceName(Implem, model);
		netlist_comp_t* comp = Netlist_Add_New(ins_name, usage->out_max_left+1, true, true);
		Netlist_Asb_SetDefaultCI(comp, '0');
		Netlist_Comp_SetChild(Implem->netlist.top, comp);
		return comp;
	}
	else if(model==NETLIST_COMP_SUB) {
		char* ins_name = Map_Netlist_MakeInstanceName(Implem, model);
		unsigned needed_width = GetMax(usage->out_max_left+1, usage->op_max_width);  // Needed because comparisons have output width=1 but larger operands
		netlist_comp_t* comp = Netlist_Sub_New(ins_name, needed_width, true, true);
		Netlist_Asb_SetDefaultCI(comp, '0');
		Netlist_Asb_EnableSign(comp, false);
		Netlist_Comp_SetChild(Implem->netlist.top, comp);
		return comp;
	}
	else if(model==NETLIST_COMP_MUL) {
		char* ins_name = Map_Netlist_MakeInstanceName(Implem, model);
		netlist_comp_t* comp = Netlist_Mul_New(ins_name, usage->op_max_width, usage->op_max_width, usage->out_max_left+1, true);
		Netlist_Comp_SetChild(Implem->netlist.top, comp);
		return comp;
	}
	else if(model==NETLIST_COMP_DIVQR) {
		unsigned width_num = usage->op_max_widthes->DATA;
		unsigned width_den = usage->op_max_widthes->NEXT->DATA;
		char* ins_name = Map_Netlist_MakeInstanceName(Implem, model);
		netlist_comp_t* comp = Netlist_DivQR_New(ins_name, width_num, width_den);
		Netlist_Comp_SetChild(Implem->netlist.top, comp);
		return comp;
	}
	else if(model==NETLIST_COMP_SHL) {
		unsigned width_data  = usage->op_max_widthes->DATA;
		unsigned width_shift = usage->op_max_widthes->NEXT->DATA;
		unsigned width_comp  = GetMax(usage->out_max_left+1, width_data);
		char* ins_name = Map_Netlist_MakeInstanceName(Implem, model);
		netlist_comp_t* comp = Netlist_ShL_New(ins_name, width_comp, width_shift);
		Netlist_Comp_SetChild(Implem->netlist.top, comp);
	}
	else if(model==NETLIST_COMP_SHR) {
		unsigned width_data  = usage->op_max_widthes->DATA;
		unsigned width_shift = usage->op_max_widthes->NEXT->DATA;
		unsigned width_comp  = GetMax(usage->out_max_left+1, width_data);
		char* ins_name = Map_Netlist_MakeInstanceName(Implem, model);
		netlist_comp_t* comp = Netlist_ShR_New(ins_name, width_comp, width_shift);
		Netlist_Comp_SetChild(Implem->netlist.top, comp);
	}
	else if(model==NETLIST_COMP_ROTL) {
		unsigned width_data = usage->op_max_widthes->DATA;
		unsigned width_shift = usage->op_max_widthes->NEXT->DATA;
		char* ins_name = Map_Netlist_MakeInstanceName(Implem, model);
		netlist_comp_t* comp = Netlist_RotL_New(ins_name, width_data, width_shift);
		Netlist_Comp_SetChild(Implem->netlist.top, comp);
	}
	else if(model==NETLIST_COMP_ROTR) {
		unsigned width_data = usage->op_max_widthes->DATA;
		unsigned width_shift = usage->op_max_widthes->NEXT->DATA;
		char* ins_name = Map_Netlist_MakeInstanceName(Implem, model);
		netlist_comp_t* comp = Netlist_RotR_New(ins_name, width_data, width_shift);
		Netlist_Comp_SetChild(Implem->netlist.top, comp);
	}
	// Operator with generic definitions and callbacks
	else if(model->func_comp_new != NULL) {
		char* ins_name = Map_Netlist_MakeInstanceName(Implem, model);
		netlist_comp_t* comp = model->func_comp_new(ins_name, model);
		Netlist_Comp_SetChild(Implem->netlist.top, comp);
	}
	// Unknown operator
	else {
		printf("INTERNAL ERROR %s:%d : Operators '%s' are not handled here.\n", __FILE__, __LINE__, model->name);
	}
	return NULL;
}
int Map_Netlist_AddSomeOperators(implem_t* Implem, netlist_comp_model_t* model, unsigned number) {

	if(model==NULL) {
		printf("INTERNAL ERROR %s:%d : No component model given.\n", __FILE__, __LINE__);
		return __LINE__;
	}

	if(model==NETLIST_COMP_ADD) {
		op_usage_t usage;
		OpUsageStats_Get_Implem(Implem, model->name, &usage);
		if(usage.trans_nb==0) goto NO_USAGE_STATS;
		for(int i=0; i<number; i++) {
			char* ins_name = Map_Netlist_MakeInstanceName(Implem, model);
			netlist_comp_t* comp = Netlist_Add_New(ins_name, usage.out_max_left+1, true, true);
			Netlist_Asb_SetDefaultCI(comp, '0');
			Netlist_Comp_SetChild(Implem->netlist.top, comp);
		}
		OpUsageStats_Reset(&usage);
	}
	else if(model==NETLIST_COMP_SUB) {
		op_usage_t usage;
		OpUsageStats_Get_Implem(Implem, model->name, &usage);
		if(usage.trans_nb==0) goto NO_USAGE_STATS;
		unsigned needed_width = GetMax(usage.out_max_left+1, usage.op_max_width);  // Needed because comparisons have output width=1 but larger operands
		for(int i=0; i<number; i++) {
			char* ins_name = Map_Netlist_MakeInstanceName(Implem, model);
			netlist_comp_t* comp = Netlist_Sub_New(ins_name, needed_width, true, true);
			Netlist_Asb_SetDefaultCI(comp, '0');
			Netlist_Asb_EnableSign(comp, false);
			Netlist_Comp_SetChild(Implem->netlist.top, comp);
		}
		OpUsageStats_Reset(&usage);
	}
	else if(model==NETLIST_COMP_MUL) {
		op_usage_t usage;
		OpUsageStats_Get_Implem(Implem, model->name, &usage);
		if(usage.trans_nb==0) goto NO_USAGE_STATS;
		for(int i=0; i<number; i++) {
			char* ins_name = Map_Netlist_MakeInstanceName(Implem, model);
			netlist_comp_t* comp = Netlist_Mul_New(ins_name, usage.op_max_width, usage.op_max_of_min_width, usage.out_max_left+1, true);
			Netlist_Comp_SetChild(Implem->netlist.top, comp);
		}
		OpUsageStats_Reset(&usage);
	}
	else if(model==NETLIST_COMP_DIVQR) {
		op_usage_t usage;
		OpUsageStats_Get_Implem(Implem, model->name, &usage);
		if(usage.trans_nb==0) goto NO_USAGE_STATS;
		unsigned width_num = usage.op_max_widthes->DATA;
		unsigned width_den = usage.op_max_widthes->NEXT->DATA;
		for(int i=0; i<number; i++) {
			char* ins_name = Map_Netlist_MakeInstanceName(Implem, model);
			netlist_comp_t* comp = Netlist_DivQR_New(ins_name, width_num, width_den);
			Netlist_Comp_SetChild(Implem->netlist.top, comp);
		}
		OpUsageStats_Reset(&usage);
	}
	else if(model==NETLIST_COMP_SHL) {
		op_usage_t usage;
		OpUsageStats_Get_Implem(Implem, model->name, &usage);
		if(usage.trans_nb==0) goto NO_USAGE_STATS;
		//unsigned width_data = usage.op_max_widthes->DATA;
		unsigned width_data  = usage.op_max_widthes->DATA;
		unsigned width_shift = usage.op_max_widthes->NEXT->DATA;
		unsigned width_comp  = GetMax(usage.out_max_left+1, width_data);
		OpUsageStats_Reset(&usage);
		for(int i=0; i<number; i++) {
			char* ins_name = Map_Netlist_MakeInstanceName(Implem, model);
			netlist_comp_t* comp = Netlist_ShL_New(ins_name, width_comp, width_shift);
			Netlist_Comp_SetChild(Implem->netlist.top, comp);
		}
	}
	else if(model==NETLIST_COMP_SHR) {
		op_usage_t usage;
		OpUsageStats_Get_Implem(Implem, model->name, &usage);
		if(usage.trans_nb==0) goto NO_USAGE_STATS;
		unsigned width_data  = usage.op_max_widthes->DATA;
		unsigned width_shift = usage.op_max_widthes->NEXT->DATA;
		unsigned width_comp  = GetMax(usage.out_max_left+1, width_data);
		OpUsageStats_Reset(&usage);
		for(int i=0; i<number; i++) {
			char* ins_name = Map_Netlist_MakeInstanceName(Implem, model);
			netlist_comp_t* comp = Netlist_ShR_New(ins_name, width_comp, width_shift);
			Netlist_Comp_SetChild(Implem->netlist.top, comp);
		}
	}
	else if(model==NETLIST_COMP_ROTL) {
		op_usage_t usage;
		OpUsageStats_Get_Implem(Implem, model->name, &usage);
		if(usage.trans_nb==0) goto NO_USAGE_STATS;
		unsigned width_data = usage.op_max_widthes->DATA;
		unsigned width_shift = usage.op_max_widthes->NEXT->DATA;
		OpUsageStats_Reset(&usage);
		for(int i=0; i<number; i++) {
			char* ins_name = Map_Netlist_MakeInstanceName(Implem, model);
			netlist_comp_t* comp = Netlist_RotL_New(ins_name, width_data, width_shift);
			Netlist_Comp_SetChild(Implem->netlist.top, comp);
		}
	}
	else if(model==NETLIST_COMP_ROTR) {
		op_usage_t usage;
		OpUsageStats_Get_Implem(Implem, model->name, &usage);
		if(usage.trans_nb==0) goto NO_USAGE_STATS;
		unsigned width_data = usage.op_max_widthes->DATA;
		unsigned width_shift = usage.op_max_widthes->NEXT->DATA;
		OpUsageStats_Reset(&usage);
		for(int i=0; i<number; i++) {
			char* ins_name = Map_Netlist_MakeInstanceName(Implem, model);
			netlist_comp_t* comp = Netlist_RotR_New(ins_name, width_data, width_shift);
			Netlist_Comp_SetChild(Implem->netlist.top, comp);
		}
	}
	// Operator with generic definitions and callbacks
	else if(model->func_comp_new != NULL) {
		for(int i=0; i<number; i++) {
			char* ins_name = Map_Netlist_MakeInstanceName(Implem, model);
			netlist_comp_t* comp = model->func_comp_new(ins_name, model);
			Netlist_Comp_SetChild(Implem->netlist.top, comp);
		}
	}
	// Unknown operator
	else {
		printf("INTERNAL ERROR %s:%d : Operators '%s' are not handled here.\n", __FILE__, __LINE__, model->name);
		return __LINE__;
	}

	return 0;

	NO_USAGE_STATS:

	printf("Error : Can't add components of model '%s' because no usage occurrence found.\n", model->name);

	return __LINE__;
}

// Add and update all components and storage elements in the netlist
int Map_Netlist_CompleteComponents(implem_t* Implem) {
	if(Implem->H==NULL) return 0;

	// Detect the needed and available resources

	hwres_heap_t* heap_need = NULL;
	if(sched_use_vexalloc_share==true) {
		heap_need = hwres_heap_getneed_vexalloc(Implem);
	}
	else {
		heap_need = hwres_heap_getneed(Implem);
	}

	hwres_heap_t* heap_avail = hwres_heap_getavail(Implem);

	#if 0  // For debug, because sometimes some components are missing

	printf("\nDEBUG %s:%d : Check needed resources\n\n", __FILE__, __LINE__);

	hwres_heap_print(heap_need, NULL);

	printf("\nDEBUG %s:%d : Check avail resources\n\n", __FILE__, __LINE__);

	hwres_heap_print(heap_avail, NULL);

	printf("\nDEBUG %s:%d : End check needed/avail resources\n\n", __FILE__, __LINE__);

	#endif

	// Ensure the memories have enough ports
	avl_pp_foreach(&heap_need->type_mem, scanElt) {
		hw_res_t* hwelt = scanElt->data;
		char *VarName = hwelt->name;

		// Get the component if it already exists
		netlist_comp_t* comp = Map_Netlist_GetComponent(Implem, VarName);
		netlist_access_t* topacc = Netlist_Comp_GetAccess(Implem->netlist.top, VarName);
		if(comp==NULL) {
			if(topacc==NULL) errprintfa("MEM Component or access name '%s' not found.\n", VarName);
			// Here the symbol is a top-level access
			// Note: for now, it's not possible to add accesses
			if(topacc->model==NETLIST_ACCESS_MEM_RA_EXT) {
				if(hwelt->ports_r > 1) {
					errprintfa("%u mem RA EXT accesses are needed for mem '%s' but only 1 can be provided.\n", hwelt->ports_r, VarName);
				}
				if(hwelt->ports_w > 0) {
					errprintfa("%u mem WA EXT accesses are needed for mem '%s' but none can be provided.\n", hwelt->ports_w, VarName);
				}
			}
			else if(topacc->model==NETLIST_ACCESS_MEM_WA_EXT) {
				if(hwelt->ports_w > 1) {
					errprintfa("%u mem WA EXT accesses are needed for mem '%s' but only 1 can be provided.\n", hwelt->ports_w, VarName);
				}
				if(hwelt->ports_r > 0) {
					errprintfa("%u mem RA EXT accesses are needed for mem '%s' but none can be provided.\n", hwelt->ports_r, VarName);
				}
			}
			else {
				errprintfa("Incompatible top-levle access model '%s' for mem '%s'.\n", topacc->model->name, VarName);
			}
			// It's OK for the current mem symbol
			continue;
		}

		// Set the parameters to the component
		if(comp->model==NETLIST_COMP_MEMORY) {
			netlist_memory_t* comp_mem = comp->data;

			#if 0  // For debug
			dbgprintf("Mem '%s' has-ra %u has-wa %u need-ra %u need-wa %u\n",
				comp->name, comp_mem->ports_ra_nb, comp_mem->ports_wa_nb, hwelt->ports_r, hwelt->ports_w
			);
			#endif

			if(comp_mem->direct_en==true) {
				while(hwelt->ports_r_ifdirect > comp_mem->ports_ra_nb) Netlist_Mem_AddAccess_RA(comp);
				while(hwelt->ports_w_ifdirect > comp_mem->ports_wa_nb) Netlist_Mem_AddAccess_WA(comp);
			}
			else {
				while(hwelt->ports_r > comp_mem->ports_ra_nb) Netlist_Mem_AddAccess_RA(comp);
				while(hwelt->ports_w > comp_mem->ports_wa_nb) Netlist_Mem_AddAccess_WA(comp);
			}
		}

		else if(comp->model==NETLIST_COMP_PINGPONG_IN || comp->model==NETLIST_COMP_PINGPONG_OUT) {
			netlist_pingpong_t* pp_data = comp->data;
			if(pp_data->is_direct==true) continue;
			while(hwelt->ports_r > pp_data->ports_ra_nb) {
				dbgprintf("Adding access RA to comp '%s' type '%s'\n", comp->name, comp->model->name);
				Netlist_PingPong_AddAccess_RA(comp);
			}
			while(hwelt->ports_w > pp_data->ports_wa_nb) Netlist_PingPong_AddAccess_WA(comp);
		}
		else {
			abort();
		}
	}

	printf("Adding operators...");

	avl_pp_foreach(&heap_need->type_ops, avl_node) {
		hw_res_t* need_elt = avl_node->data;
		hw_res_t* avail_elt = hwres_heap_op_find(heap_avail, need_elt->name);

		// Compute the nb of missing operators
		int missing_nb = 0;
		if(avail_elt!=NULL) missing_nb = need_elt->number - avail_elt->number;
		else missing_nb = need_elt->number;
		if(missing_nb<=0) continue;

		printf("  %s(%d)", need_elt->name, missing_nb);

		// Add the missing operators
		Map_Netlist_AddSomeOperators(Implem, Netlist_CompModel_Get(need_elt->name), missing_nb);
	}

	printf("\n");

	// Clean the heaps of resources
	hwres_heap_free(heap_need);
	hwres_heap_free(heap_avail);

	return 0;
}

// Remove unused components
// Note: This function should not be used if Mux are already created.
int Map_Netlist_RemoveComponents(implem_t* Implem) {
	if(Implem->H==NULL) return 0;

	// Detect the needed and available resources

	hwres_heap_t* heap_need = NULL;
	if(sched_use_vexalloc_share==true) {
		heap_need = hwres_heap_getneed_vexalloc(Implem);
	}
	else {
		heap_need = hwres_heap_getneed(Implem);
	}

	hwres_heap_t* heap_avail = hwres_heap_getavail(Implem);

	printf("Removing components...\n");

	avl_pp_foreach(&heap_avail->type_reg, scanElt) {
		hw_res_t* avail_elt = scanElt->data;
		if(Implem_SymFlag_Chk(Implem, avail_elt->name, SYM_PROTECT_NODEL)==true) continue;
		hw_res_t* need_elt = hwres_heap_reg_find(heap_need, avail_elt->name);
		if(need_elt!=NULL) continue;
		netlist_comp_t* comp = Map_Netlist_GetComponent(Implem, avail_elt->name);
		if(comp==NULL) continue;  // The resource could be a port
		printf("  Removing: %s '%s'\n", comp->model->name, comp->name);
		Netlist_Comp_Free(comp);
	}

	avl_pp_foreach(&heap_avail->type_mem, scanElt) {
		hw_res_t* avail_elt = scanElt->data;
		if(Implem_SymFlag_Chk(Implem, avail_elt->name, SYM_PROTECT_NODEL)==true) continue;
		hw_res_t* need_elt = hwres_heap_mem_find(heap_need, avail_elt->name);

		netlist_comp_t* comp = Map_Netlist_GetComponent(Implem, avail_elt->name);
		netlist_access_t* topacc = Netlist_Comp_GetAccess(Implem->netlist.top, avail_elt->name);
		if(comp==NULL) {
			if(topacc==NULL) abort();
			continue;
		}

		if(need_elt==NULL) {
			printf("  Removing: %s '%s'\n", comp->model->name, comp->name);
			Netlist_Comp_Free(comp);
			continue;
		}

		if(comp->model==NETLIST_COMP_PINGPONG_IN || comp->model==NETLIST_COMP_PINGPONG_OUT){
			netlist_pingpong_t* comp_pp = comp->data;

			int excess_ra_nb = 0, excess_wa_nb = 0;
			if(comp_pp->is_direct==true) {
				excess_ra_nb = avail_elt->ports_r - need_elt->ports_r_ifdirect;
				excess_wa_nb = avail_elt->ports_w - need_elt->ports_w_ifdirect;
			}
			else {
				excess_ra_nb = avail_elt->ports_r - need_elt->ports_r;
				excess_wa_nb = avail_elt->ports_w - need_elt->ports_w;
			}

			if(excess_ra_nb>0 || excess_wa_nb>0) {
				printf("  Removing in %s '%s':", comp->model->name, comp->name);
			}

			if(excess_ra_nb>0) printf(" ra %u/%u", excess_ra_nb, avail_elt->ports_r);
			for(int i=0; i<excess_ra_nb; i++) {
				Netlist_PingPong_Remove_RA(avl_p_deepest_data(&comp_pp->access_ports_ra));
			}
			if(excess_wa_nb>0) printf(" wa %u/%u", excess_wa_nb, avail_elt->ports_w);
			for(int i=0; i<excess_wa_nb; i++) {
				Netlist_PingPong_Remove_WA(avl_p_deepest_data(&comp_pp->access_ports_wa));
			}

			if(excess_ra_nb>0 || excess_wa_nb>0) {
				printf("\n");
			}
		}

		else if(comp->model==NETLIST_COMP_MEMORY){
			netlist_memory_t* comp_mem = comp->data;

			int excess_ra_nb = 0, excess_wa_nb = 0;
			if(comp_mem->direct_en==true) {
				excess_ra_nb = avail_elt->ports_r - need_elt->ports_r_ifdirect;
				excess_wa_nb = avail_elt->ports_w - need_elt->ports_w_ifdirect;
			}
			else {
				excess_ra_nb = avail_elt->ports_r - need_elt->ports_r;
				excess_wa_nb = avail_elt->ports_w - need_elt->ports_w;
			}

			if(excess_ra_nb>0 || excess_wa_nb>0) {
				printf("  Removing in %s '%s':", comp->model->name, comp->name);
			}

			if(excess_ra_nb>0) printf(" ra %u/%u", excess_ra_nb, avail_elt->ports_r);
			for(int i=0; i<excess_ra_nb; i++) {
				Netlist_Access_MemRA_Remove(avl_p_deepest_data(&comp_mem->access_ports_ra));
			}
			if(excess_wa_nb>0) printf(" wa %u/%u", excess_wa_nb, avail_elt->ports_w);
			for(int i=0; i<excess_wa_nb; i++) {
				Netlist_Access_MemWA_Remove(avl_p_deepest_data(&comp_mem->access_ports_wa));
			}

			if(excess_ra_nb>0 || excess_wa_nb>0) {
				printf("\n");
			}
		}

	}

	avl_pp_foreach(&heap_avail->type_ops, scanElt) {
		hw_res_t* avail_elt = scanElt->data;
		hw_res_t* need_elt = hwres_heap_op_find(heap_need, avail_elt->name);

		#if 0  // For debug
		if(need_elt==NULL) {
			printf("  Info operator type '%s' is not needed. There are %u available.\n", avail_elt->name, avail_elt->number);
		}
		else {
			printf("  Info operator type '%s' : need %u, avail %u\n", avail_elt->name, need_elt->number, avail_elt->number);
		}
		if(need_elt!=NULL && need_elt->number==0) {
			printf("  WARNING %s:%u : Structure for needed operator type '%s' exists but contains zero.\n", __FILE__, __LINE__, avail_elt->name);
		}
		#endif

		// Compute the nb to remove
		int excess_nb = 0;
		if(need_elt!=NULL) excess_nb = avail_elt->number - need_elt->number;
		else excess_nb = avail_elt->number;
		assert(excess_nb>=0);
		if(excess_nb<=0) continue;

		printf("  Removing: operator %s %u/%u\n", avail_elt->name, excess_nb, avail_elt->number);

		// Get the operators to remove
		chain_list* list_ops = NULL;
		avl_pp_foreach(&Implem->netlist.top->children, scanComp) {
			netlist_comp_t* comp = scanComp->data;
			if(comp->model->name!=avail_elt->name) continue;
			list_ops = addchain(list_ops, comp);
			excess_nb--;
			if(excess_nb<=0) break;
		}

		if(ChainList_Count(list_ops) < excess_nb) {
			printf("  WARNING Operator '%s' : try to remove %u, only got %u\n", avail_elt->name, excess_nb, ChainList_Count(list_ops));
		}

		// Remove the operators
		foreach(list_ops, scanComp) {
			netlist_comp_t* comp = scanComp->DATA;
			Netlist_Comp_Free(comp);
		}
		freechain(list_ops);
	}

	// Clean the heaps of resources
	hwres_heap_free(heap_need);
	hwres_heap_free(heap_avail);

	return 0;
}


// Mostly for debug
void Map_Netlist_PrintTargetPorts(implem_t* Implem, netlist_port_t* target) {
	chain_list* list = NULL;

	// Scan all components
	avl_pp_foreach(&Implem->netlist.top->children, scanChild) {
		netlist_comp_t* child = scanChild->data;
		avl_pp_foreach(&child->ports, scanPort) {
			netlist_port_t* port = scanPort->data;
			if(port->direction!=NETLIST_PORT_DIR_IN) continue;
			foreach(port->source, scanSource) {
				netlist_val_cat_t* valcat = scanSource->DATA;
				if(valcat->source!=target) continue;
				list = addchain(list, port);
				break;
			}
		}
	}
	// Scan the ports of the top-level entity
	avl_pp_foreach(&Implem->netlist.top->ports, scanPort) {
		netlist_port_t* port = scanPort->data;
		if(port->direction!=NETLIST_PORT_DIR_OUT) continue;
		foreach(port->source, scanSource) {
			netlist_val_cat_t* valcat = scanSource->DATA;
			if(valcat->source!=target) continue;
			list = addchain(list, port);
			break;
		}
	}

	unsigned nb = ChainList_Count(list);
	printf("Number of target ports : %u.\n", nb);
	if(nb>0) {
		printf("List of target ports :");
		foreach(list, scan) {
			netlist_port_t* port = scan->DATA;
			if(scan!=list) printf(",");
			printf(" %s '%s' port '%s'", port->component->model->name, port->component->name, port->name);
		}
		printf("\n");
		freechain(list);
	}
}
void Map_Netlist_PrintTargetPorts_user(implem_t* Implem, char* comp_name, char* port_name) {
	comp_name = namealloc(comp_name);
	port_name = namealloc(port_name);
	netlist_comp_t* comp = Map_Netlist_GetComponent(Implem, comp_name);
	if(comp==NULL) {
		printf("Unknown component.\n");
		return;
	}
	netlist_port_t* port = Netlist_Comp_GetPort(comp, port_name);
	if(port==NULL) {
		printf("Unknown port.\n");
		return;
	}
	Map_Netlist_PrintTargetPorts(Implem, port);
}
void Map_Netlist_PrintComp_FromModel_user(implem_t* Implem, char* model_name) {
	model_name = namealloc(model_name);
	unsigned nb = 0;
	avl_pp_foreach(&Implem->netlist.top->children, scan) {
		netlist_comp_t* comp = scan->data;
		if(comp->model->name!=model_name) continue;
		printf(" %s", comp->name);
	}
	if(nb>0) printf(" (total = %u)\n", nb);
}
void Map_Netlist_PrintComp_All(implem_t* Implem) {
	unsigned nb = 0;
	avl_pp_foreach(&Implem->netlist.top->children, scan) {
		netlist_comp_t* comp = scan->data;
		printf("Model '%s' name '%s'\n", comp->model->name, comp->name);
	}
	if(nb>0) printf("Total = %u\n", nb);
}



//=================================================
// Miscellaneous functions for mapping
//=================================================

void Map_Netlist_AddSigFsmIn(implem_t* Implem, char* sig_name, char* fsmin_name, unsigned width) {

	// Create the Signal

	netlist_comp_t* sig = Netlist_Comp_Sig_New(sig_name, width);
	netlist_signal_t* comp_sig = sig->data;
	Netlist_Comp_SetChild(Implem->netlist.top, sig);

	// Add the FSM input

	netlist_port_t* port_fsm = Netlist_Comp_Fsm_AddInput(Implem->netlist.fsm, width);

	// Link the two

	port_fsm->source = Netlist_ValCat_MakeList_FullPort(comp_sig->port_out);
}
void Map_Netlist_AddSigFsmOut(implem_t* Implem, char* sig_name, char* fsmin_name, unsigned width) {

	// Create the Signal

	netlist_comp_t* sig = Netlist_Comp_Sig_New(sig_name, width);
	netlist_signal_t* comp_sig = sig->data;
	Netlist_Comp_SetChild(Implem->netlist.top, sig);

	// Add the FSM output

	netlist_port_t* port_fsm = Netlist_Comp_Fsm_AddOutput(Implem->netlist.fsm, width);

	char* alloc_value = stralloc_repeat('-', width);
	Netlist_Comp_Fsm_SetDefaultValue(port_fsm, alloc_value);

	// Link the two

	comp_sig->port_in->source = Netlist_ValCat_MakeList_FullPort(port_fsm);
}

void Map_Netlist_AddMuxIn(implem_t* Implem, netlist_comp_t* comp) {

	// Create the MUX input

	netlist_access_t* access = Netlist_Mux_AddInput(comp);
	netlist_mux_access_t* access_mux = access->data;

	// Add the FSM output

	netlist_port_t* port_fsm = Netlist_Comp_Fsm_AddOutput(Implem->netlist.fsm, 1);

	char* alloc_value = stralloc_repeat('0', 1);
	Netlist_Comp_Fsm_SetDefaultValue(port_fsm, alloc_value);

	// Link the two

	access_mux->port_en->source = Netlist_ValCat_MakeList_FullPort(port_fsm);
}

// Note: The given prefix must not be empty.
char* Map_Netlist_MakeInstanceName_Prefix_AvoidSyms(implem_t* Implem, char* prefix, avl_p_tree_t* tree_avoid_names) {
	// Create buffer
	char buffer[strlen(prefix) + 20];
	sprintf(buffer, "%s_", prefix);
	char* ptrindex = buffer + strlen(buffer);
	// Start searching valid names
	char* alloc_name = NULL;
	do {
		sprintf(ptrindex, "%d", Implem->netlist.comp_name_index);
		alloc_name = namealloc(buffer);
		Implem->netlist.comp_name_index ++;
		if( Netlist_Comp_GetChild(Implem->netlist.top, alloc_name)!=NULL ) continue;
		if( Netlist_Comp_GetPort(Implem->netlist.top, alloc_name)!=NULL ) continue;
		if(tree_avoid_names!=NULL) {
			if(avl_p_isthere(tree_avoid_names, alloc_name)==true) continue;
		}
		break;
	} while(1);
	return alloc_name;
}
// If the given is not already reserved, use it
char* Map_Netlist_MakeInstanceName_TryThis_AvoidSyms(implem_t* Implem, char* name, avl_p_tree_t* tree_avoid_names) {
	// Check whether this name is already reserved
	if( Netlist_Comp_GetChild(Implem->netlist.top, name)!=NULL ) goto SEARCHOTHER;
	if( Netlist_Comp_GetPort(Implem->netlist.top, name)!=NULL ) goto SEARCHOTHER;
	if(tree_avoid_names!=NULL) {
		if(avl_p_isthere(tree_avoid_names, name)==true) goto SEARCHOTHER;
	}
	return name;
	// Search other names
	SEARCHOTHER:
	return Map_Netlist_MakeInstanceName_Prefix_AvoidSyms(Implem, name, tree_avoid_names);
}

netlist_comp_t* Map_Netlist_GetComponent(implem_t* Implem, const char* name) {
	return Netlist_Comp_GetChild(Implem->netlist.top, name);
}

netlist_comp_t* Map_Netlist_GetComp_verbose(implem_t* Implem, const char* name, netlist_comp_model_t* model) {
	netlist_comp_t* comp = Map_Netlist_GetComponent(Implem, name);
	if(comp==NULL) {
		printf("Error : Component named '%s' not found.\n", name);
		return NULL;
	}
	if(model!=NULL && comp->model!=model) {
		printf("Error : The component named '%s' is model '%s', expected '%s'.\n", name, comp->model->name, model->name);
		return NULL;
	}
	return comp;
}



//================================================
// Basic allocation and manipulation of nodes
// For the dependency graph
//================================================

#define POOL_prefix      pool_map_comptype
#define POOL_data_t      map_comptype_t
#define POOL_ALLOC_SIZE  1000

#define POOL_INSTANCE             POOL_COMPTYPE
#define POOL_INSTANCE_ATTRIBUTES  static

#include "../pool.h"

// Allocation wrappers

static map_comptype_t* map_comptype_new() {
	map_comptype_t* elt = pool_map_comptype_pop(&POOL_COMPTYPE);
	avl_p_init(&elt->all);
	return elt;
}
static void map_comptype_free(map_comptype_t* elt) {
	avl_p_reset(&elt->all);
	pool_map_comptype_push(&POOL_COMPTYPE, elt);
}

// Manipulation functions

map_comptype_t* Map_CompType_GetAdd(map_netlist_data_t* map_data, char* modelname) {
	avl_pp_node_t* pp_node = NULL;
	bool b = avl_pp_add(&map_data->comptype, modelname, &pp_node);
	if(b==true) pp_node->data = map_comptype_new();
	return pp_node->data;
}

map_comptype_t* Map_CompType_AddComp(map_netlist_data_t* map_data, netlist_comp_t* comp) {
	map_comptype_t* comptype = Map_CompType_GetAdd(map_data, comp->model->name);
	avl_p_add_overwrite(&comptype->all, comp, comp);
	return comptype;
}



//=================================================
// Main data structures for mapping
//=================================================

static void Map_Netlist_HwUsage_Init(map_netlist_hwusage_t* hwusage) {
	avl_p_init(&hwusage->comp_used);
	avl_p_init(&hwusage->mem_accesses_used);
	avl_pp_init(&hwusage->mux_in_used);
	avl_pp_init(&hwusage->fsm_out_value);
}
static void Map_Netlist_HwUsage_Reset(map_netlist_hwusage_t* hwusage) {
	avl_p_reset(&hwusage->comp_used);
	avl_p_reset(&hwusage->mem_accesses_used);
	avl_pp_reset(&hwusage->mux_in_used);
	avl_pp_reset(&hwusage->fsm_out_value);
}


map_netlist_datapath_t* Map_Netlist_Datapath_New() {
	map_netlist_datapath_t* datapath = calloc(1, sizeof(*datapath));
	Map_Netlist_HwUsage_Init(&datapath->hwusage);
	return datapath;
}
void Map_Netlist_Datapath_Free(map_netlist_datapath_t* datapath) {
	Netlist_ValCat_FreeList(datapath->valcat);
	Map_Netlist_HwUsage_Reset(&datapath->hwusage);
	freechain(datapath->sub_datapaths);
	free(datapath);
}



static void Map_Netlist_MapData_Init(map_netlist_data_t* map_data) {

	avl_pp_init(&map_data->node2state);
	avl_pp_init(&map_data->state2node);
	avl_pi_init(&map_data->call2id);

	VexAlloc_Init(&map_data->expr_dictionary);
	avl_pp_init(&map_data->tree_act2ref);
	avl_pp_init(&map_data->expr_datapaths);

	// "Current State" section
	Map_Netlist_HwUsage_Init(&map_data->cur_state.hwusage);
	avl_pp_init(&map_data->cur_state.expr2datapath);
	avl_p_init(&map_data->cur_state.all_datapaths);

	// CompType tree
	avl_pp_init(&map_data->comptype);

}

static void Map_Netlist_MapData_ResetCurState(map_netlist_data_t* map_data) {
	Map_Netlist_HwUsage_Reset(&map_data->cur_state.hwusage);
	avl_pp_reset(&map_data->cur_state.expr2datapath);
	avl_p_reset(&map_data->cur_state.all_datapaths);
}

void Map_Netlist_MapData_Reset(map_netlist_data_t* map_data) {

	avl_pp_reset(&map_data->node2state);
	avl_pp_reset(&map_data->state2node);
	avl_pi_reset(&map_data->call2id);

	VexAlloc_Clear(&map_data->expr_dictionary);
	avl_pp_foreach(&map_data->expr_datapaths, scanExpr) {
		chain_list* list = scanExpr->data;
		foreach(list, scanDP) Map_Netlist_Datapath_Free(scanDP->DATA);
	}
	avl_pp_reset(&map_data->tree_act2ref);
	avl_pp_reset(&map_data->expr_datapaths);

	Map_Netlist_MapData_ResetCurState(map_data);

	// The CompType
	avl_pp_foreach(&map_data->comptype, scan) map_comptype_free(scan->data);
	avl_pp_reset(&map_data->comptype);

}



hier_node_t* Map_FsmState2Node(map_netlist_data_t* data, netlist_fsm_state_t* state) {
	hier_node_t* node = NULL;
	avl_pp_find_data(&data->state2node, state, (void**)&node);
	return node;
}
netlist_fsm_state_t* Map_Node2FsmState(map_netlist_data_t* data, hier_node_t* node) {
	netlist_fsm_state_t* state = NULL;
	avl_pp_find_data(&data->node2state, node, (void**)&state);
	return state;
}



//======================================================================
// Build the next state expression for FSM states
//======================================================================

// These functions are recursive.

static netlist_fsm_rule_t* Map_Netlist_BuildRule_FromNode(map_netlist_data_t* data, hier_node_t* node, unsigned level);
static netlist_fsm_rule_t* Map_Netlist_BuildRule_FromLoopTest(map_netlist_data_t* data, hier_node_t* node, unsigned level);
static netlist_fsm_rule_t* Map_Netlist_BuildRule_Return(map_netlist_data_t* data, hier_node_t* nodeProc, unsigned level);

static void Map_Netlist_BuildRule_CheckHugeDepth(unsigned level) {
	if(level > 100) {
		errprintf("Infinite loop when building state-to-state rules.\n");
		abort();
	}
}

// The input node is supposed to be the last of a level.
static netlist_fsm_rule_t* Map_Netlist_BuildRule_AfterNode(map_netlist_data_t* data, hier_node_t* node, unsigned level) {

	Map_Netlist_BuildRule_CheckHugeDepth(level);

	if(node->NEXT!=NULL) return Map_Netlist_BuildRule_FromNode(data, node->NEXT, level+1);

	// Find the first node of the level
	hier_node_t* first_node = node;
	while(first_node->PREV!=NULL) first_node = first_node->PREV;

	hier_node_t* parent_node = first_node->PARENT;
	if(parent_node==NULL) {
		// Note: This situation can't happen if the Hier is properly simplified.
		// Just in case mapping is called on a non-simplified Hier, return to the main function.
		printf("Warning: Mapping launched on an unclean graph. Inserting jump to init state.\n");
		return Map_Netlist_BuildRule_FromNode(data, data->Implem->H->PROCESS, level+1);
	}

	if(parent_node->TYPE==HIERARCHY_BB) {
		return Map_Netlist_BuildRule_AfterNode(data, parent_node, level+1);
	}

	if(parent_node->TYPE==HIERARCHY_CASE) {
		return Map_Netlist_BuildRule_AfterNode(data, parent_node, level+1);
	}

	if(parent_node->TYPE==HIERARCHY_SWITCH) {
		return Map_Netlist_BuildRule_AfterNode(data, parent_node, level+1);
	}

	if(parent_node->TYPE==HIERARCHY_LOOP) {
		hier_loop_t* parent_loop = &parent_node->NODE.LOOP;

		if(first_node==parent_loop->body_after) return Map_Netlist_BuildRule_FromLoopTest(data, parent_node, level+1);

		if(first_node==parent_loop->body_before) {
			if(parent_loop->body_after!=NULL) return Map_Netlist_BuildRule_FromNode(data, parent_loop->body_after, level+1);
			return Map_Netlist_BuildRule_FromLoopTest(data, parent_node, level+1);
		}

		printf("INTERNAL ERROR %s:%d : This state should not be reachable.\n", __FILE__, __LINE__);
		abort();
	}

	printf("INTERNAL ERROR %s:%d : This state should not be reachable.\n", __FILE__, __LINE__);
	abort();

	return NULL;
}

static netlist_fsm_rule_t* Map_Netlist_BuildRule_FromLoopTest(map_netlist_data_t* data, hier_node_t* node, unsigned level) {

	Map_Netlist_BuildRule_CheckHugeDepth(level);

	hier_loop_t* node_loop = &node->NODE.LOOP;

	// If a state is associated to this loop, get it
	netlist_fsm_state_t* state = Map_Node2FsmState(data, node);

	if(node_loop->testinfo->cond!=NULL) {
		netlist_fsm_rule_t* rule = Netlist_FsmRule_New();
		rule->condition = hvex_dup(node_loop->testinfo->cond);
		assert(rule->condition!=NULL);

		if(node_loop->body_before!=NULL) {
			rule->rule_true = Map_Netlist_BuildRule_FromNode(data, node_loop->body_before, level+1);
		}
		else if(node_loop->body_after!=NULL) {
			rule->rule_true = Map_Netlist_BuildRule_FromNode(data, node_loop->body_after, level+1);
		}
		else {
			if(state==NULL) {
				printf("INTERNAL ERROR %s:%d : This state should not be reachable.\n", __FILE__, __LINE__);
				assert(data==NULL);
			}
			rule->rule_true = Netlist_FsmRule_New();
			rule->rule_true->state = state;
		}

		rule->rule_false = Map_Netlist_BuildRule_AfterNode(data, node, level+1);
		return rule;
	}
	else if(node_loop->body_before!=NULL) {
		return Map_Netlist_BuildRule_FromNode(data, node_loop->body_before, level+1);
	}
	else if(node_loop->body_after!=NULL) {
		return Map_Netlist_BuildRule_FromNode(data, node_loop->body_after, level+1);
	}

	// The loop is empty and infinite (dead end)

	// If a state is associated to this loop, use it
	if(state==NULL) {
		printf("INTERNAL ERROR %s:%d : This state should not be reachable.\n", __FILE__, __LINE__);
		assert(data==NULL);
	}

	netlist_fsm_rule_t* rule = Netlist_FsmRule_New();
	rule->state = state;
	return rule;
}

static netlist_fsm_rule_t* Map_Netlist_BuildRule_Return(map_netlist_data_t* data, hier_node_t* nodeProc, unsigned level) {

	Map_Netlist_BuildRule_CheckHugeDepth(level);

	assert(nodeProc->TYPE==HIERARCHY_PROCESS);
	hier_proc_t* proc_data = &nodeProc->NODE.PROCESS;

	// Handle when the function is the main process (it has no function equivalent in the FSM)
	// There is no call ID to handle
	if(nodeProc==data->Implem->H->PROCESS) {
		return Map_Netlist_BuildRule_FromNode(data, data->Implem->H->PROCESS, level+1);
	}
	// Get the fsmfunc
	netlist_fsm_func_t* fsmfunc = Netlist_Comp_Fsm_Func_Get(data->Implem->netlist.fsm, proc_data->name_orig);
	assert(fsmfunc!=NULL);

	// Here we have to build the whole list of call possibilities
	netlist_fsm_rule_t* rule = Netlist_FsmRule_New();
	foreach(proc_data->calls, scan) {
		hier_node_t* nodeCall = scan->DATA;
		// Get the call ID
		avl_pi_node_t* pi_node = NULL;
		avl_pi_find(&data->call2id, nodeCall, &pi_node);
		assert(pi_node!=NULL);
		int id = pi_node->data;
		// Create the bitype_list* element
		netlist_fsm_rule_t* ruletarget = Map_Netlist_BuildRule_AfterNode(data, nodeCall, level+1);
		rule->func_rets = addbitype(rule->func_rets, id, fsmfunc, ruletarget);
	}

	return rule;
}

static netlist_fsm_rule_t* Map_Netlist_BuildRule_FromNode(map_netlist_data_t* data, hier_node_t* node, unsigned level) {

	Map_Netlist_BuildRule_CheckHugeDepth(level);

	if(node->TYPE==HIERARCHY_PROCESS) {
		return Map_Netlist_BuildRule_AfterNode(data, node, level+1);
	}

	if(node->TYPE==HIERARCHY_BB) {
		return Map_Netlist_BuildRule_FromNode(data, node->NODE.BB.body, level+1);
	}

	if(node->TYPE==HIERARCHY_STATE) {
		netlist_fsm_state_t* state = Map_Node2FsmState(data, node);
		assert(state!=NULL);
		netlist_fsm_rule_t* rule = Netlist_FsmRule_New();
		rule->state = state;
		return rule;
	}

	if(node->TYPE==HIERARCHY_LOOP) {
		// If it's an infinite loop, there is a State associated
		netlist_fsm_state_t* state = Map_Node2FsmState(data, node);
		if(state!=NULL) {
			netlist_fsm_rule_t* rule = Netlist_FsmRule_New();
			rule->state = state;
			return rule;
		}
		hier_loop_t* node_loop = &node->NODE.LOOP;
		if(node_loop->body_after!=NULL) return Map_Netlist_BuildRule_FromNode(data, node_loop->body_after, level+1);
		return Map_Netlist_BuildRule_FromLoopTest(data, node, level+1);
	}

	if(node->TYPE==HIERARCHY_JUMP) {
		hier_jump_t* jump_data = &node->NODE.JUMP;
		return Map_Netlist_BuildRule_FromNode(data, jump_data->target, level+1);
	}

	if(node->TYPE==HIERARCHY_LABEL) {
		return Map_Netlist_BuildRule_AfterNode(data, node, level+1);
	}

	if(node->TYPE==HIERARCHY_CALL) {
		hier_jump_t* call_data = &node->NODE.JUMP;
		// Get the call ID
		avl_pi_node_t* pi_node = NULL;
		avl_pi_find(&data->call2id, node, &pi_node);
		assert(pi_node!=NULL);
		int id = pi_node->data;
		// Get the fsmfunc
		hier_node_t* nodeProc = call_data->target;
		assert(nodeProc->TYPE==HIERARCHY_PROCESS);
		hier_proc_t* proc_data = &nodeProc->NODE.PROCESS;
		netlist_fsm_func_t* fsmfunc = Netlist_Comp_Fsm_Func_Get(data->Implem->netlist.fsm, proc_data->name_orig);
		assert(fsmfunc!=NULL);
		// Create the bitype_list* element
		// FIXME Here there is crash when nodeProc->NEXT = NULL (empty function, not simplified)
		netlist_fsm_rule_t* ruletarget = Map_Netlist_BuildRule_FromNode(data, nodeProc->NEXT, level+1);
		netlist_fsm_rule_t* rule = Netlist_FsmRule_New();
		rule->func_call = addbitype(NULL, id, fsmfunc, ruletarget);
		return rule;
	}

	if(node->TYPE==HIERARCHY_RETURN) {
		hier_jump_t* ret_data = &node->NODE.JUMP;
		return Map_Netlist_BuildRule_Return(data, ret_data->target, level+1);
	}

	if(node->TYPE==HIERARCHY_SWITCH) {
		//hier_switch_t* node_switch = &node->NODE.SWITCH;

		netlist_fsm_rule_t* rule_top = NULL;
		netlist_fsm_rule_t* rule_prev = NULL;

		hier_node_t* default_case = NULL;
		foreach(node->CHILDREN, scan) {
			hier_node_t* nodeCase = scan->DATA;
			hier_case_t* node_case = &nodeCase->NODE.CASE;

			if(node_case->is_default!=0) {
				default_case = nodeCase;
				continue;
			}

			netlist_fsm_rule_t* rule = Netlist_FsmRule_New();
			rule->condition = Hier_Case_OrConds(nodeCase);
			assert(rule->condition!=NULL);
			rule->rule_true = Map_Netlist_BuildRule_FromNode(data, nodeCase, level+1);

			if(rule_prev!=NULL) rule_prev->rule_false = rule;
			else rule_top = rule;
			rule_prev = rule;
		}

		if(default_case!=NULL) {
			netlist_fsm_rule_t* rule = Map_Netlist_BuildRule_FromNode(data, default_case, level+1);
			if(rule_prev!=NULL) rule_prev->rule_false = rule;
			else rule_top = rule;
		}
		else {
			if(rule_prev!=NULL) {
				rule_prev->rule_false = Map_Netlist_BuildRule_AfterNode(data, node, level+1);
			}
		}

		if(rule_top==NULL) {
			return Map_Netlist_BuildRule_AfterNode(data, node, level+1);
		}

		return rule_top;
	}

	if(node->TYPE==HIERARCHY_CASE) {
		hier_case_t* node_case = &node->NODE.CASE;
		if(node_case->body!=NULL) {
			return Map_Netlist_BuildRule_FromNode(data, node_case->body, level+1);
		}
		return Map_Netlist_BuildRule_AfterNode(data, node->PARENT, level+1);
	}

	printf("INTERNAL ERROR %s:%d : This state should not be reachable (node type %d).\n", __FILE__, __LINE__, node->TYPE);
	abort();

	return NULL;
}

static netlist_fsm_rule_t* Map_Netlist_BuildRule(map_netlist_data_t* data, netlist_fsm_state_t* state) {

	// Get the Hier node corresponding to this state
	hier_node_t* node = Map_FsmState2Node(data, state);

	// Special nodes that can loop on themselves

	if(node->TYPE==HIERARCHY_LOOP) {
		return Map_Netlist_BuildRule_FromLoopTest(data, node, 0);
	}

	// Consider the graph that follows
	return Map_Netlist_BuildRule_AfterNode(data, node, 0);
}



//======================================================================
// Build the datapaths for all expressions
//======================================================================

// The choices that make the process simpler and faster :
// Only single-operation operators are used.
//   In particular, no ALU, LU... only ADD, SUB, and independent logic operators.
// This makes the process faster.
// Optionally, a post-mapping operation could merge MUX and Logic operators.

static void Map_HwUsage_Print(indent_t* indent, map_netlist_data_t* map_data, map_netlist_hwusage_t* hwusage) {
	bool first = true;

	indentprintf(indent, "Comps ..");
	first = true;
	avl_p_foreach(&hwusage->comp_used, scan) {
		netlist_comp_t* comp = scan->data;
		if(first==true) first = false; else putchar(',');
		printf(" %s '%s'", comp->model->name, comp->name);
	}
	putchar('\n');

	// FIXME other data items could be printed
}
static void Map_Datapath_Print(indent_t* indent, map_netlist_data_t* map_data, map_netlist_datapath_t* dp) {
	indent_t indent1;
	if(indent==NULL) indent1 = Indent_Make(' ', 2, 1);
	else indent1 = Indent_Make_IncLevel(indent);

	indentprintf(indent, "Datapath ptr %p Expr ptr %p\n", dp, dp->Expr);

	indentprintf(&indent1, "Expr .... ");
	VexAlloc_Print_Elt_be(dp->Expr, NULL, "\n");

	indentprintf(&indent1, "Sub-DP ..");
	foreach(dp->sub_datapaths, scan) printf(" %p", scan->DATA);
	putchar('\n');

	Map_HwUsage_Print(&indent1, map_data, &dp->hwusage);
}

// Display error context
void Map_Netlist_Datapath_BuildNew_Op_error(
	map_netlist_data_t* map_data, map_netlist_datapath_t* datapath,
	hvex_model_t* op_model, netlist_comp_model_t* comp_model, unsigned fromline
) {
	printf("\n");
	printf("INTERNAL ERROR (line %u) when mapping an operation model '%s' to component type '%s'.\n",
		fromline, op_model->name, comp_model->name
	);
	printf("The Action is : "); HierAct_DebugPrint(map_data->cur_action);
	printf("The datapath wants output bits %d,%d:%d\n",
		datapath->Expr->left, datapath->Expr->right, datapath->Expr->width
	);

	printf("List of components in netlist:\n");
	map_comptype_t* comptype = Map_CompType_GetAdd(map_data, comp_model->name);
	avl_p_foreach(&comptype->all, scanComp) {
		netlist_comp_t* comp = scanComp->data;
		printf("  ptr %p name %s ", comp, comp->name);
		bool is_used = avl_p_isthere(&map_data->cur_state.hwusage.comp_used, comp);
		printf("%s\n", is_used==true ? "used" : "unused");
	}

	printf("Expressions already mapped in current cycle:\n");
	indent_t indent = Indent_Make(' ', 2, 1);
	avl_p_foreach(&map_data->cur_state.all_datapaths, scanDP) {
		map_netlist_datapath_t* dp = scanDP->data;
		hvex_dico_elt_t* elt = dp->Expr;
		if(elt->model!=op_model) continue;
		Map_Datapath_Print(&indent, map_data, dp);
	}

	abort();
}


netlist_port_t* Map_Netlist_GetSourcePort_Check(netlist_port_t* port) {
	assert(port->source!=NULL);
	assert(port->source->NEXT==NULL);
	netlist_val_cat_t* valcat = port->source->DATA;
	assert(valcat->source!=NULL);
	netlist_port_t* port_src = valcat->source;
	return port_src;
}
netlist_comp_t* Map_Netlist_GetSourceMux(netlist_port_t* port) {
	netlist_port_t* port_src = Map_Netlist_GetSourcePort_Check(port);
	netlist_comp_t* compMux = port_src->component;
	//assert(compMux->comp_name==NETLIST_COMP_MUX);
	// For debug
	if(compMux->model!=NETLIST_COMP_MUX) {
		printf("INTERNAL ERROR %s:%d : comp %s type %s port %s : wanted a mux before, got '%s' type '%s'\n", __FILE__, __LINE__,
			port_src->component->name, port_src->component->model->name, port_src->name,
			compMux->name, compMux->model->name
		);
		abort();
	}
	return compMux;
}

netlist_access_t* Map_Netlist_Mux_GetAddInput_SetState_internal(
	map_netlist_data_t* map_data, map_netlist_datapath_t* datapath,
	netlist_comp_t* mux, netlist_access_t* muxIn
) {
	netlist_mux_access_t* muxIn_data = muxIn->data;

	avl_pp_add_overwrite(&datapath->hwusage.mux_in_used, mux, muxIn);

	// Get the FSM port
	netlist_comp_t* fsm = map_data->Implem->netlist.fsm;
	netlist_port_t* port_fsm = NULL;
	if(muxIn_data->port_en->source!=NULL) {
		assert(muxIn_data->port_en->source->NEXT==NULL);
		netlist_val_cat_t* valcat_elt = muxIn_data->port_en->source->DATA;
		port_fsm = valcat_elt->source;
		assert(port_fsm!=NULL);
	}
	else {
		// Add an output port, with default value = zero
		port_fsm = Netlist_Comp_Fsm_AddOutput(fsm, 1);
		char* alloc_value = stralloc_repeat('0', 1);
		Netlist_Comp_Fsm_SetDefaultValue(port_fsm, alloc_value);
		// Link the two ports
		muxIn_data->port_en->source = Netlist_ValCat_MakeList_FullPort(port_fsm);
	}

	// Add the current state to the FSM output
	char* alloc_value = stralloc_repeat('1', 1);

	// Add this to the current hardware usage
	avl_pp_add_overwrite(&datapath->hwusage.fsm_out_value, port_fsm, alloc_value);

	// Note: the actual FSM Action is added to the FSM states in the state mapping function
	// This ensures correct mapping when re-using datapaths

	return muxIn;
}

// WARNING for this function, the valcat width MUST be the same than the MUX
netlist_access_t* Map_Netlist_Mux_GetAddInput_SetState(
	map_netlist_data_t* map_data, map_netlist_datapath_t* datapath,
	netlist_comp_t* mux, chain_list* valcat
) {
	netlist_access_t* muxIn = Netlist_Mux_GetAddInput(mux, valcat);
	return Map_Netlist_Mux_GetAddInput_SetState_internal(map_data, datapath, mux, muxIn);
}

#if 0  // Disabled because unused
// If padding=0 it means sign extension
static void Map_Netlist_Port_MuxGetAddInput_SetState_WidthExtend(
	map_netlist_data_t* map_data, map_netlist_datapath_t* datapath, netlist_port_t* port,
	chain_list* valcat, unsigned valcat_width, char padding
) {
	netlist_comp_t* mux = Map_Netlist_GetSourceMux(port);
	//assert(Netlist_ValCat_ListWidth(valcat)==valcat_width);  // Debug
	if(valcat_width==port->width) {
		Map_Netlist_Mux_GetAddInput_SetState(map_data, datapath, mux, valcat);
	}
	else {
		int padding_size = port->width - valcat_width;
		assert(padding_size>0);
		chain_list* local_valcat = Netlist_ValCat_DupList(valcat);
		local_valcat = Netlist_ValCat_AddBitsLeft(local_valcat, padding, padding_size);

		Map_Netlist_Mux_GetAddInput_SetState(map_data, datapath, mux, local_valcat);

		Netlist_ValCat_FreeList(local_valcat);
	}
}
#endif

// In these functions the ValCat must correspond exactly to the full Expr->ref.elt
chain_list* Map_Netlist_ValCat_DupCropToRef(chain_list* valcat, hvex_dico_ref_t* Expr) {
	// For debug
	//assert(Netlist_ValCat_ListWidth(valcat)==Expr->ref.elt->width);

	valcat = Netlist_ValCat_DupList(valcat);

	valcat = Netlist_ValCat_Crop(
		valcat,
		Expr->ref.elt->left - Expr->ref.left,
		Expr->ref.right - Expr->ref.elt->right
	);

	// For debug
	//assert(Netlist_ValCat_ListWidth(valcat)==Expr->ref.width);

	return valcat;
}
chain_list* Map_Netlist_ValCat_DupResizeToRef(chain_list* valcat, hvex_dico_ref_t* Expr) {
	valcat = Map_Netlist_ValCat_DupCropToRef(valcat, Expr);
	valcat = Netlist_ValCat_AddBitsLeft(valcat, Expr->extend, Expr->width - Expr->ref.width);

	// For debug
	//assert(Netlist_ValCat_ListWidth(valcat)==Expr->width);

	return valcat;
}
chain_list* Map_Netlist_ValCat_DupResize(chain_list* valcat, hvex_dico_ref_t* Expr, unsigned width) {
	valcat = Map_Netlist_ValCat_DupCropToRef(valcat, Expr);

	if(width<Expr->ref.width) {
		// Crop
		valcat = Netlist_ValCat_Crop(valcat, Expr->ref.width - width, 0);
	}
	else {
		// Extend
		valcat = Netlist_ValCat_AddBitsLeft(valcat, Expr->extend, width - Expr->ref.width);
	}

	// For debug
	//assert(Netlist_ValCat_ListWidth(valcat)==Expr->width);

	return valcat;
}

// The valcat is the expr of a datapath already built from Expr->ref.elt
//   So the width of the ValCat expression is assumed to be Expr->ref.elt->width
// Expr can be NULL: in this case the ValCat is evaluated and unsigned extended.
void Map_Netlist_Port_MuxGetAddInput_SetState_FromExpr(
	map_netlist_data_t* map_data, map_netlist_datapath_t* datapath, netlist_port_t* port,
	hvex_dico_ref_t* Expr, chain_list* valcat
) {
	// For debug
	//assert(Netlist_ValCat_ListWidth(valcat)==Expr->ref.elt->width);

	chain_list* dup_valcat = Map_Netlist_ValCat_DupResize(valcat, Expr, port->width);

	netlist_comp_t* mux = Map_Netlist_GetSourceMux(port);
	Map_Netlist_Mux_GetAddInput_SetState(map_data, datapath, mux, dup_valcat);

	Netlist_ValCat_FreeList(dup_valcat);
}

void Map_Netlist_Datapath_AddToState(map_netlist_data_t* map_data, map_netlist_datapath_t* datapath) {
	#if 0  // For debug, print what is done
	dbgprintf("AddToState ENTER Datapath %p", datapath);
	VexAlloc_Print_Elt_be(datapath->Expr, "  -  ", "\n");
	printf("  Status: Expr2DP %u DP %u\n", avl_pp_count(&map_data->cur_state.expr2datapath), avl_p_count(&map_data->cur_state.all_datapaths));
	#endif

	bool b;
	b = avl_p_isthere(&map_data->cur_state.all_datapaths, datapath);
	if(b==true) {

		//dbgprintf("AddToState ALREADY THERE\n");

		#ifndef NDEBUG
		if(avl_pp_isthere(&map_data->cur_state.expr2datapath, datapath->Expr)==false) {
			errprintf("AddToState Datapath %p not in Expr2Datapath\n", datapath);
			VexAlloc_Print_Elt_be(datapath->Expr, "  -  ", "\n");
			abort();
		}
		#endif

		return;
	}

	#ifndef NDEBUG
	if(avl_pp_isthere(&map_data->cur_state.expr2datapath, datapath->Expr)==true) {
		avl_pp_node_t* node = NULL;
		avl_pp_find(&map_data->cur_state.expr2datapath, datapath->Expr, &node);
		map_netlist_datapath_t* prev_dp = node->data;
		errprintf("AddToState Datapath %p", datapath);
		VexAlloc_Print_Elt_be(datapath->Expr, "  -  ", "\n");
		printf("  Expr already present, datapath %p", prev_dp);
		VexAlloc_Print_Elt_be(prev_dp->Expr, "  -  ", "\n");
		abort();
	}
	#endif

	//dbgprintf("AddToState RECURS\n");

	foreach(datapath->sub_datapaths, scanDatapath) Map_Netlist_Datapath_AddToState(map_data, scanDatapath->DATA);

	map_netlist_hwusage_t* src_hwusage = &datapath->hwusage;
	map_netlist_hwusage_t* dest_hwusage = &map_data->cur_state.hwusage;

	avl_p_foreach(&src_hwusage->comp_used, scan) {
		assert(avl_p_isthere(&dest_hwusage->comp_used, scan->data)==false);
		avl_p_add_overwrite(&dest_hwusage->comp_used, scan->data, scan->data);
	}
	avl_p_foreach(&src_hwusage->mem_accesses_used, scan) {
		assert(avl_p_isthere(&dest_hwusage->mem_accesses_used, scan->data)==false);
		avl_p_add_overwrite(&dest_hwusage->mem_accesses_used, scan->data, scan->data);
	}
	avl_pp_foreach(&src_hwusage->mux_in_used, scan) {
		#ifndef NDEBUG
		avl_pp_node_t* node = NULL;
		if(avl_pp_find(&dest_hwusage->mux_in_used, scan->key, &node)==true) if(node->data!=scan->data) abort();
		#endif
		avl_pp_add_overwrite(&dest_hwusage->mux_in_used, scan->key, scan->data);
	}
	avl_pp_foreach(&src_hwusage->fsm_out_value, scan) {
		assert(avl_pp_isthere(&dest_hwusage->fsm_out_value, scan->key)==false);
		avl_pp_add_overwrite(&dest_hwusage->fsm_out_value, scan->key, scan->data);
		// Fixme here implement a function Netlist_Comp_Fsm_GetAction to assert none is used for the output
		Netlist_Comp_Fsm_AddAction(map_data->Implem->netlist.fsm, map_data->cur_state.state, scan->key, scan->data);
	}

	#if 0  // For debug, print what is done
	dbgprintf("AddToState FINISH Expr %p Datapath %p", datapath->Expr, datapath);
	VexAlloc_Print_Elt_be(datapath->Expr, "  -  ", "\n");
	#endif

	avl_pp_add_overwrite(&map_data->cur_state.expr2datapath, datapath->Expr, datapath);
	avl_p_add_overwrite(&map_data->cur_state.all_datapaths, datapath, datapath);
}

#define MAP_DP_AVAIL_PART   0x01  // Partially available, partially selected
#define MAP_DP_AVAIL_FULL   0x02  // Fully available, nothing selected
#define MAP_DP_SELECT_FULL  0x04  // Already completely selected

// Return 0 if can't be selected because some HW is used for something else
// Otherwise, return the above flags
static int Map_Netlist_Datapath_CheckAvail(map_netlist_data_t* map_data, map_netlist_datapath_t* datapath) {
	// Detect if already fully selected
	bool b = avl_p_isthere(&map_data->cur_state.all_datapaths, datapath);
	if(b==true) {
		assert(avl_pp_isthere(&map_data->cur_state.expr2datapath, datapath->Expr)==true);
		return MAP_DP_SELECT_FULL;
	}
	else {
		// Here, if the Expr is already present in another datapath, return NOT POSSIBLE
		// FIXME if there is no used hardware (like: "00" & var), could return AVAIL
		b = avl_pp_isthere(&map_data->cur_state.expr2datapath, datapath->Expr);
		if(b==true) return 0;
	}

	// Check all sub-datapathes
	unsigned subs_nb = 0;
	int subs_flags = 0;
	foreach(datapath->sub_datapaths, scanDatapath) {
		subs_nb++;
		int z = Map_Netlist_Datapath_CheckAvail(map_data, scanDatapath->DATA);
		if(z==0) return 0;
		subs_flags |= z;
	}
	// Fix what has been received
	if(subs_nb == 0) subs_flags = MAP_DP_AVAIL_FULL;
	else if(subs_nb > 1) {
		if((subs_flags & MAP_DP_AVAIL_PART) != 0)  subs_flags = MAP_DP_AVAIL_PART;
		else if((subs_flags & MAP_DP_AVAIL_FULL) != 0 && (subs_flags & MAP_DP_SELECT_FULL) != 0) subs_flags = MAP_DP_AVAIL_PART;
	}

	map_netlist_hwusage_t* src_hwusage = &datapath->hwusage;
	map_netlist_hwusage_t* dest_hwusage = &map_data->cur_state.hwusage;

	// FIXME here operator sharing is not done... for similar operations
	avl_p_foreach(&src_hwusage->comp_used, scan) {
		bool b = avl_p_isthere(&dest_hwusage->comp_used, scan->data);
		if(b==true) return 0;
	}
	avl_p_foreach(&src_hwusage->mem_accesses_used, scan) {
		bool b = avl_p_isthere(&dest_hwusage->mem_accesses_used, scan->data);
		if(b==true) return 0;
	}
	avl_pp_foreach(&src_hwusage->mux_in_used, scan) {
		void* data;
		bool b = avl_pp_find_data(&dest_hwusage->mux_in_used, scan->key, &data);
		if(b==true) {
			if(data!=scan->data) return 0;
		}
	}
	avl_pp_foreach(&src_hwusage->fsm_out_value, scan) {
		void* data;
		bool b = avl_pp_find_data(&dest_hwusage->fsm_out_value, scan->key, &data);
		if(b==true) {
			if(data!=scan->data) return 0;
		}
	}

	return subs_flags;
}



//======================================================================
// Build the datapaths of expressions: main function
//======================================================================

// This is the "head" function that calls the callbacks of the HVEX models
map_netlist_datapath_t* Map_Netlist_Datapath_Expr_BuildNew(map_netlist_data_t* map_data, hvex_dico_elt_t* Expr) {

	#if 0  // For debug
	dbgprintf("Building new datapath for Expr %p:\n", Expr);
	VexAlloc_Print_Elt_be(Expr, "  ", "\n");
	#endif

	//dbgprintf("Model %s\n", Expr->model->name);

	// Get the model-specific callback
	map_hvex_cb_type cb = (map_hvex_cb_type)Expr->model->cb_map;
	if(cb==NULL) {
		errprintfa("No map callback is declared for HVEX model '%s'\n", Expr->model->name);
	}

	// Launch the calllback function
	map_netlist_datapath_t* datapath = cb(map_data, Expr);

	assert(datapath!=NULL);
	assert(datapath->Expr==Expr);

	#if 0
	dbgprintf("Generated ValCat : ");
	Netlist_ValCat_DebugPrintList(datapath->valcat);
	#endif

	if(datapath->valcat!=NULL) {
		assert(Netlist_ValCat_ListWidth(datapath->valcat)==datapath->Expr->width);
	}

	return datapath;
}

map_netlist_datapath_t* Map_Netlist_Datapath_Expr(map_netlist_data_t* map_data, hvex_dico_elt_t* Expr) {
	map_netlist_datapath_t* datapath = NULL;
	bool b;

	//dbgprintf("Processing Expr ptr %p\n", Expr);

	// If the expression is already used in the current state, simply reuse the datapath
	b = avl_pp_find_data(&map_data->cur_state.expr2datapath, Expr, (void**)&datapath);
	if(b==true) {
		return datapath;
	}

	// Get the list of compatible datapaths
	avl_pp_node_t* expr_node = NULL;
	b = avl_pp_add(&map_data->expr_datapaths, Expr, &expr_node);
	if(b==true) expr_node->data = NULL;

	// To reuse existing compatible datapathes, care must be taken not to select possible datapaths
	// when their sub-datapathes could be shared
	// This can cause mapping failure because of "missing" operators, for shared operators
	// But it also makes circuits unnecessarily big, for non-shared operators

	// What's currently done: scan compatible datapaths, and for each, determine whether:
	//   - some parts are already selected at the current cycle
	//   - some parts are not selected at the current cycle but a compatible datapath is
	//   - some parts are invalidated by what is already built
	// Selection is done this way:
	//   only determine whether some parts are already selected at the current cycle
	//   if no compatible datapath is even partially selected at the current cycle,
	//   then select the first available one
	// FIXME Handle when one datapath is partially selected, and all others are fully available
	//   And select that partially selected datapath
	//   Warning: all partially selected datapathes must be selected from only one other datapath
	//     otherwise there could be conflicts when setting FSM outputs...
	// FIXME And when that works, implement selection of sub-datapathes that best suit what has to be mapped at the current cycle
	// Example: Before launching mapping of a clock cycle
	//   scan all compatible datapathes for all vex subexpressions to map
	//   Use a counter for each sub-datapath: count the number of times this datapath can be shared
	//   Then, when needing to select the best datapathes, use this counter

	// Try to get a compatible datapath, that is available, and that best shares sub-datapathes
	#if 1
	map_netlist_datapath_t* first_avail_dp = NULL;
	bool problem_found = false;
	foreach((chain_list*)expr_node->data, scanDatapath) {
		map_netlist_datapath_t* local_datapath = scanDatapath->DATA;
		int z = Map_Netlist_Datapath_CheckAvail(map_data, local_datapath);
		if(z==0) continue;
		//if(z==0) { problem_found = true; break; }  // FIXME With this line, NOTHING is gained...
		if((z & MAP_DP_AVAIL_PART) != 0) { problem_found = true; break; }
		// Note: Here, the returned value may be SELECT_FULL simply if all subexpressions are selected
		// So, don't check whether it's SELECT_FULL or AVAIL_FULL
		first_avail_dp = local_datapath;
	}
	if(problem_found==false && first_avail_dp!=NULL) {
		datapath = first_avail_dp;
		#if 0  // For debug, print what is done
		dbgprintf("REUSE Datapath %p flags %u", datapath, Map_Netlist_Datapath_CheckAvail(map_data, datapath));
		VexAlloc_Print_Elt_be(datapath->Expr, "  -  ", "\n");
		#endif
	}
	#endif

	if(datapath==NULL) {
		// Build a new datapath
		datapath = Map_Netlist_Datapath_Expr_BuildNew(map_data, Expr);
		assert(datapath!=NULL);
		// Add this datapath in the list for the current Expr
		expr_node->data = addchain(expr_node->data, datapath);
	}

	assert(datapath!=NULL);

	// Flag this new datapath as used in the current state
	Map_Netlist_Datapath_AddToState(map_data, datapath);

	//dbgprintf("After Datapath %p\n", datapath);

	return datapath;
}

static int Map_Netlist_Datapath_State(map_netlist_data_t* map_data, netlist_fsm_state_t* state) {
	// For debug: Fully link all components to be able to follow connections
	//Netlist_PortTargets_CompBuild(map_data->Implem->netlist.top);

	hier_node_t* node = Map_FsmState2Node(map_data, state);

	if(node->TYPE==HIERARCHY_STATE) {
		hier_state_t* trans_data = &node->NODE.STATE;
		if(trans_data->actions==NULL) return 0;

		// Initialize the hardware and datapath usage for this cycle
		Map_Netlist_MapData_ResetCurState(map_data);
		map_data->cur_state.state = state;

		// For debug
		//dbgprintf("Mapping new state...\n");

		// Process all Actions
		foreach(trans_data->actions, scanAction) {
			hier_action_t* action = scanAction->DATA;
			map_data->cur_action = action;

			#if 0  // For debug
			dbgprintf("State %p Mapping Action...\n", map_data->cur_state.state);
			#endif
			#if 0  // For debug
			dbgprintf("State %p Mapping Action : ", map_data->cur_state.state);
			hvex_print_bn(action->expr);
			#endif

			// Get the corresponding expression in the dictionary
			hvex_dico_ref_t* ExprAlloc = NULL;
			avl_pp_find_data(&map_data->tree_act2ref, action, (void**)&ExprAlloc);
			assert(ExprAlloc!=NULL);
			// Process this expression
			Map_Netlist_Datapath_Expr(map_data, ExprAlloc->ref.elt);

			map_data->cur_action = NULL;
		}

		// Clean
		Map_Netlist_MapData_ResetCurState(map_data);
	}

	else if(node->TYPE==HIERARCHY_LOOP) {
		#ifndef NDEBUG
		hier_loop_t* loop_data = &node->NODE.LOOP;
		assert(loop_data->body_after==NULL && loop_data->body_before==NULL);
		#endif
		// This corresponds to a wait, the condition is assumed to be a port.
	}

	else {
		abort();
	}

	return 0;
}



//======================================================================
// Simplification
//======================================================================

// FIXME Why is there a duplicate Map_Netlist_Simplify_flags() function just after?
void Map_Netlist_Simplify(implem_t* Implem) {

	// Initialize the lists of all targets of all component ports
	Netlist_PortTargets_CompFree(Implem->netlist.top);
	Netlist_PortTargets_CompBuild(Implem->netlist.top);

	// The data structures for the simplification process
	netlist_simp_data_t simpdata_i;
	netlist_simp_data_t* simpdata = &simpdata_i;
	Netlist_Simp_Init(simpdata);

	// Initialize the todo list with all components
	avl_pp_foreach(&Implem->netlist.top->children, scanComp) {
		netlist_comp_t* comp = scanComp->data;
		avl_p_add_overwrite(&simpdata->comps_todo, comp, comp);
	}

	// Protect the top-level component
	Netlist_Simp_Protect_AddFlag(simpdata,
		Implem->netlist.top,
		NETLIST_SIMP_PROTECT_NOSIMP | NETLIST_SIMP_PROTECT_NODEL
	);
	// Protect the Signals for clock, reset, start (if any)
	if(Implem->netlist.sig_clock!=NULL) {
		Netlist_Simp_Protect_AddFlag(simpdata,
			Implem->netlist.sig_clock,
			NETLIST_SIMP_PROTECT_NODEL
		);
	}
	if(Implem->netlist.sig_reset!=NULL) {
		Netlist_Simp_Protect_AddFlag(simpdata,
			Implem->netlist.sig_reset,
			NETLIST_SIMP_PROTECT_NODEL
		);
	}
	if(Implem->netlist.sig_start!=NULL) {
		Netlist_Simp_Protect_AddFlag(simpdata,
			Implem->netlist.sig_start,
			NETLIST_SIMP_PROTECT_NODEL
		);
	}

	// Protect the FSM
	Netlist_Simp_Protect_AddFlag(simpdata,
		Implem->netlist.fsm,
		NETLIST_SIMP_PROTECT_NODEL
	);

	// Launch
	Netlist_Simp(simpdata);

	// Process deleted components
	foreach(simpdata->comps_protect_unlinked, scanComp) {
		netlist_comp_t* comp = scanComp->DATA;
		// Unlink from the Implem
		if(comp==Implem->netlist.sig_clock) {
			Implem->netlist.sig_clock = NULL;
		}
		else if(comp==Implem->netlist.sig_reset) {
			Implem->netlist.sig_reset = NULL;
		}
		else if(comp==Implem->netlist.sig_start) {
			Implem->netlist.sig_start = NULL;
		}
		else if(comp==Implem->netlist.fsm) {
			Implem->netlist.fsm = NULL;
		}
		else {
			abort();
		}
		// Remove the component
		Netlist_Comp_Free(comp);
	}

	// Clean
	Netlist_Simp_Clear(simpdata);
}

static void Map_Netlist_Simplify_flags(map_netlist_data_t* map_data, bool nofsm) {
	implem_t* Implem = map_data->Implem;

	// Initialize the lists of all targets of all component ports
	Netlist_PortTargets_CompFree(Implem->netlist.top);
	Netlist_PortTargets_CompBuild(Implem->netlist.top);

	// The data structures for the simplification process
	netlist_simp_data_t simpdata_i;
	netlist_simp_data_t* simpdata = &simpdata_i;
	Netlist_Simp_Init(simpdata);

	// Initialize the todo list with all components
	avl_pp_foreach(&Implem->netlist.top->children, scanComp) {
		netlist_comp_t* comp = scanComp->data;
		avl_p_add_overwrite(&simpdata->comps_todo, comp, comp);
	}

	// Protect the top-level component
	Netlist_Simp_Protect_AddFlag(simpdata,
		Implem->netlist.top,
		NETLIST_SIMP_PROTECT_NOSIMP | NETLIST_SIMP_PROTECT_NODEL
	);
	// Protect the Signals for clock, reset, start (if any)
	if(Implem->netlist.sig_clock!=NULL) {
		Netlist_Simp_Protect_AddFlag(simpdata,
			Implem->netlist.sig_clock,
			NETLIST_SIMP_PROTECT_NODEL
		);
	}
	if(Implem->netlist.sig_reset!=NULL) {
		Netlist_Simp_Protect_AddFlag(simpdata,
			Implem->netlist.sig_reset,
			NETLIST_SIMP_PROTECT_NODEL
		);
	}
	if(Implem->netlist.sig_start!=NULL) {
		Netlist_Simp_Protect_AddFlag(simpdata,
			Implem->netlist.sig_start,
			NETLIST_SIMP_PROTECT_NODEL
		);
	}

	// Protect the FSM
	if(nofsm==true) {
		Netlist_Simp_Protect_AddFlag(simpdata,
			Implem->netlist.fsm,
			NETLIST_SIMP_PROTECT_NOSIMP | NETLIST_SIMP_PROTECT_NODEL
		);
	}
	else {
		Netlist_Simp_Protect_AddFlag(simpdata,
			Implem->netlist.fsm,
			NETLIST_SIMP_PROTECT_NODEL
		);
	}

	// Launch
	Netlist_Simp(simpdata);

	// Process deleted components
	foreach(simpdata->comps_protect_unlinked, scanComp) {
		netlist_comp_t* comp = scanComp->DATA;
		// Unlink from the Implem
		if(comp==Implem->netlist.sig_clock) {
			Implem->netlist.sig_clock = NULL;
		}
		else if(comp==Implem->netlist.sig_reset) {
			Implem->netlist.sig_reset = NULL;
		}
		else if(comp==Implem->netlist.sig_start) {
			Implem->netlist.sig_start = NULL;
		}
		else if(comp==Implem->netlist.fsm) {
			Implem->netlist.fsm = NULL;
		}
		else {
			abort();
		}
		// Remove the component
		Netlist_Comp_Free(comp);
	}

	// Clean
	Netlist_Simp_Clear(simpdata);
}



//======================================================================
// Miscellaneous mapping functions
//======================================================================

static void Map_Netlist_ExtendRight(implem_t* Implem) {
	unsigned count = 0;

	avl_p_foreach(&Implem->H->NODES[HIERARCHY_STATE], scanState) {
		hier_node_t* nodeTrans = scanState->data;
		hier_state_t* node_trans = &nodeTrans->NODE.STATE;
		foreach(node_trans->actions, scanAction) {
			hier_action_t* action = scanAction->DATA;

			// Only for destination Register: remove unuseful left and right concatenations
			hvex_t* vex_dest = hvex_asg_get_dest(action->expr);
			char* destname = hvex_vec_getname(vex_dest);
			if(vex_dest->right==0) continue;

			// Get the dest component
			netlist_comp_t* comp = Netlist_Comp_GetChild(Implem->netlist.top, destname);
			netlist_port_t* port = Netlist_Comp_GetPort(Implem->netlist.top, destname);

			if(comp!=NULL) {
				if(comp->model==NETLIST_COMP_REGISTER) {
					// Concat some bits at right of expression
					hvex_t* vex_expr = hvex_asg_get_expr(action->expr);
					hvex_catright(vex_expr, hvex_newvec(destname, vex_dest->right-1, 0));
					// Fix indexes of destination
					vex_dest->width += vex_dest->right;
					vex_dest->right = 0;
					// Count stats
					count ++;
				}
				else {
					printf("Warning: Found right index > 0 in assignment to non-register component '%s' model '%s'.\n",
						comp->name, comp->model->name
					);
				}
			}
			else if(port!=NULL) {
				printf("Warning: Found right index > 0 in assignment to top-level port '%s'.\n", destname);
			}
			else {
				abort();
			}

		}  // Scan Actions
	}  // Scan States

	if(count > 0) {
		printf("Note: Right index of destination fixed in %u Actions.\n", count);
	}
}

// This function fills the dictionary.
// It is necessary for all elements to be dimensioned large enough
static void Map_Netlist_BuildAllExpr(map_netlist_data_t* map_data) {
	implem_t* Implem = map_data->Implem;

	// Insert ALL expressions of the design into the dictionary
	avl_p_foreach(&Implem->H->NODES[HIERARCHY_STATE], scanNode) {
		hier_node_t* node = scanNode->data;
		hier_state_t* state_data = &node->NODE.STATE;
		foreach(state_data->actions, scanAction) {
			hier_action_t* action = scanAction->DATA;
			hvex_dico_ref_t* ref = VexAlloc_Add(&map_data->expr_dictionary, action->expr);
			avl_pp_add_overwrite(&map_data->tree_act2ref, action, ref);
		}
	}
	avl_p_foreach(&Implem->H->NODES[HIERARCHY_LOOP], scanNode) {
		hier_node_t* node = scanNode->data;
		hier_loop_t* loop_data = &node->NODE.LOOP;
		VexAlloc_Add(&map_data->expr_dictionary, loop_data->testinfo->cond);
	}
	avl_p_foreach(&Implem->H->NODES[HIERARCHY_CASE], scanNode) {
		hier_node_t* node = scanNode->data;
		hier_case_t* case_data = &node->NODE.CASE;
		if(case_data->is_default!=0) continue;
		avl_p_foreach(&case_data->values, scanval) {
			hier_testinfo_t* testinfo = scanval->data;
			VexAlloc_Add(&map_data->expr_dictionary, testinfo->cond);
		}
	}

	// Apply more extensive operator sharing
	VexAlloc_Share_Expensive(&map_data->expr_dictionary);

	#if 0  // For debug: Print all operations of a particular type
	dbgprintf("All literals in dictionary:\n");
	avl_pp_foreach(&map_data->expr_dictionary.elt_lit, scan) {
		vex_alloc_elt_t* elt = scan->data;
		printf("  [refs:%u] ", ChainList_Count(elt->refs));
		VexAlloc_Print_Elt_be(elt, NULL, "\n");
	}
	#endif

	#if 0  // For debug: Print all operations of a particular type
	dbgprintf("Some oper in dictionary:\n");
	avl_ip_node_t* avl_node;
	chain_list* list_expr = NULL;
	bool b = avl_ip_find(&map_data->expr_dictionary.elt_oper, VEX_EQ, &avl_node);
	if(b==true) list_expr = avl_node->data;
	dbgprintf("Elements = %u\n", ChainList_Count(list_expr));
	b = avl_ip_find(&map_data->expr_dictionary.elt_oper, VEX_NE, &avl_node);
	if(b==true) list_expr = avl_node->data;
	dbgprintf("Elements = %u\n", ChainList_Count(list_expr));
	#endif

	#if 0  // For debug: Print all functions of a particular name
	dbgprintf("Some func in dictionary:\n");
	avl_pp_node_t* avl_node;
	chain_list* list_expr = NULL;
	bool b = avl_pp_find(&map_data->expr_dictionary.elt_func, namealloc(HWRES_FUNC_SHL), &avl_node);
	if(b==true) list_expr = avl_node->data;
	dbgprintf("Elements = %u\n", ChainList_Count(list_expr));
	foreach(list_expr, scan) {
		vex_alloc_elt_t* elt = scan->DATA;
		printf("  [refs:%u] ", ChainList_Count(elt->refs));
		VexAlloc_Print_Elt_be(elt, NULL, "\n");
	}
	#endif

}

static void Map_Netlist_ConnectSigBranchCond(map_netlist_data_t* map_data, netlist_fsm_rule_t* rule) {
	if(rule==NULL) return;
	// Process the sub-rules
	Map_Netlist_ConnectSigBranchCond(map_data, rule->rule_true);
	Map_Netlist_ConnectSigBranchCond(map_data, rule->rule_false);
	// Process the current condition
	chain_list* list = hvex_SearchSym_AddList(NULL, rule->condition, NULL);
	foreach(list, scan) {
		hvex_t* Expr = scan->DATA;
		if(Expr->model==HVEX_LITERAL) continue;
		assert(Expr->model==HVEX_VECTOR);
		char* name = hvex_vec_getname(Expr);

		netlist_port_t* src_port = NULL;

		// Case where the source is a component
		netlist_comp_t* comp = Map_Netlist_GetComponent(map_data->Implem, name);
		if(comp!=NULL) {
			if(comp->model==NETLIST_COMP_SIGNAL) {
				netlist_signal_t* sig_data = comp->data;
				src_port = sig_data->port_out;
			}
			else {
				errprintfa("Inappropriate component %s '%s'.\n", comp->model->name, comp->name);
			}
		}

		// Case where the symbol is a port of the top-level entity
		if(src_port==NULL) {
			src_port = Netlist_Comp_GetPort(map_data->Implem->netlist.top, name);
		}

		if(src_port==NULL) {
			errprintfa("The symbol '%s' was not found.\n", name);
		}

		if(Netlist_PortTargets_CheckEmpty(src_port)==true) {
			netlist_port_t* fsm_port = Netlist_Comp_Fsm_AddInput(map_data->Implem->netlist.fsm, 1);
			fsm_port->source = Netlist_ValCat_MakeList_FullPort(src_port);
			Netlist_PortTargets_SetSource(fsm_port);
		}
	}  // Loop on all symbols
	freechain(list);
}

static int Map_Netlist_DumpFsmStates(map_netlist_data_t* map_data, char* filename) {
	FILE* F = fopen(filename, "wb");
	if(F==NULL) {
		printf("Error: Couldn't open the file '%s' to dump FSM sates and Actions\n", filename);
		return -1;
	}

	Netlist_Comp_Fsm_SetStateIdx(map_data->Implem->netlist.fsm);
	netlist_fsm_t* comp_fsm = map_data->Implem->netlist.fsm->data;

	foreach(comp_fsm->states, scanState) {
		netlist_fsm_state_t* state = scanState->DATA;
		fprintf(F, "==== STATE %u ====================================\n", state->index);
		hier_node_t* node = Map_FsmState2Node(map_data, state);
		if(node==NULL) {
			fprintf(F, "No Hier node associated. Simplifications? Retiming?\n\n");
			continue;
		}
		fprintf(F, "Node type %s lines ", Hier_NodeType2str(node->TYPE));
		LineList_Print_fbe(F, node->source_lines, NULL, "\n");
		if(node->TYPE==HIERARCHY_STATE) {
			hier_state_t* state_data = &node->NODE.STATE;
			foreach(state_data->actions, scanAction) {
				hier_action_t* action = scanAction->DATA;
				HierAct_FPrint(F, action);
			}
		}
		fprintf(F, "\n");
	}

	fclose(F);

	return 0;
}

// Create multiplexers for input ports of components and top-level output ports
static void Map_Netlist_CreateMux(map_netlist_data_t* map_data) {
	implem_t* Implem = map_data->Implem;
	unsigned added_mux = 0;

	chain_list* list_comp = NULL;
	avl_pp_foreach(&Implem->netlist.top->children, scanComp) {
		netlist_comp_t* comp = scanComp->data;
		list_comp = addchain(list_comp, comp);
	}
	foreach(list_comp, scanComp) {
		netlist_comp_t* comp = scanComp->DATA;
		avl_pp_foreach(&comp->ports, scanPort) {
			netlist_port_t* port = scanPort->data;
			if(port->direction!=NETLIST_PORT_DIR_IN) continue;
			if(port->source!=NULL) continue;
			// Create a new MUX
			char* mux_name = Map_Netlist_MakeInstanceName(Implem, NETLIST_COMP_MUX);
			netlist_comp_t* mux = Netlist_Mux_New(mux_name, port->width);
			Netlist_Comp_SetChild(Implem->netlist.top, mux);
			Map_CompType_AddComp(map_data, mux);
			added_mux++;
			// Connect the MUX
			netlist_mux_t* mux_data = mux->data;
			port->source = Netlist_ValCat_MakeList_FullPort(mux_data->port_out);
		}
	}
	freechain(list_comp);

	avl_pp_foreach(&Implem->netlist.top->ports, scanPort) {
		netlist_port_t* port = scanPort->data;
		if(port->direction!=NETLIST_PORT_DIR_OUT) continue;
		if(port->source!=NULL) continue;
		// Create a new MUX
		char* mux_name = Map_Netlist_MakeInstanceName(Implem, NETLIST_COMP_MUX);
		netlist_comp_t* mux = Netlist_Mux_New(mux_name, port->width);
		Netlist_Comp_SetChild(Implem->netlist.top, mux);
		Map_CompType_AddComp(map_data, mux);
		added_mux++;
		// Connect the MUX
		netlist_mux_t* mux_data = mux->data;
		port->source = Netlist_ValCat_MakeList_FullPort(mux_data->port_out);
	}

	dbgprintf("%d multiplexers were added.\n", added_mux);

	dbgprintf("Setting default MUX output values...\n");

	// Default output value to special ports.
	// The port Enable for registers and memory accesses, read/write for FIFOs

	// FROM=port, TO=string
	bitype_list* list_default_values = NULL;

	avl_pp_foreach(&Implem->netlist.top->children, scanComp) {
		netlist_comp_t* comp = scanComp->data;
		if(comp->model==NETLIST_COMP_REGISTER) {
			netlist_register_t* reg_data = comp->data;
			list_default_values = addbitype(list_default_values, 0, reg_data->port_en, namealloc("0"));
		}
		else if(comp->model==NETLIST_COMP_MEMORY) {
			netlist_memory_t* mem_data = comp->data;
			avl_p_foreach(&mem_data->access_ports_ra, scanPort) {
				netlist_access_t* access = scanPort->data;
				netlist_access_mem_addr_t* memAcc_data = access->data;
				if(memAcc_data->port_e!=NULL) {
					list_default_values = addbitype(list_default_values, 0, memAcc_data->port_e, namealloc("0"));
				}
			}
			avl_p_foreach(&mem_data->access_ports_wa, scanPort) {
				netlist_access_t* access = scanPort->data;
				netlist_access_mem_addr_t* memAcc_data = access->data;
				list_default_values = addbitype(list_default_values, 0, memAcc_data->port_e, namealloc("0"));
			}
			avl_ip_foreach(&mem_data->access_ports_d, scanPort) {
				netlist_access_t* access = scanPort->data;
				netlist_access_mem_direct_t* memAcc_data = access->data;
				list_default_values = addbitype(list_default_values, 0, memAcc_data->port_we, namealloc("0"));
			}
		}
		else if(comp->model==NETLIST_COMP_PINGPONG_IN || comp->model==NETLIST_COMP_PINGPONG_OUT) {
			netlist_pingpong_t* pp_data = comp->data;
			// The SWITCH command
			list_default_values = addbitype(list_default_values, 0, pp_data->port_switch, namealloc("0"));
			// The data ports
			avl_p_foreach(&pp_data->access_ports_ra, scanPort) {
				netlist_access_t* access = scanPort->data;
				netlist_access_mem_addr_t* memAcc_data = access->data;
				if(memAcc_data->port_e!=NULL) {
					list_default_values = addbitype(list_default_values, 0, memAcc_data->port_e, namealloc("0"));
				}
			}
			avl_p_foreach(&pp_data->access_ports_wa, scanPort) {
				netlist_access_t* access = scanPort->data;
				netlist_access_mem_addr_t* memAcc_data = access->data;
				list_default_values = addbitype(list_default_values, 0, memAcc_data->port_e, namealloc("0"));
			}
			avl_ip_foreach(&pp_data->access_ports_d, scanPort) {
				netlist_access_t* access = scanPort->data;
				netlist_access_mem_direct_t* memAcc_data = access->data;
				list_default_values = addbitype(list_default_values, 0, memAcc_data->port_we, namealloc("0"));
			}
		}
		else {
			avl_pp_foreach(&comp->accesses, scanAccess) {
				netlist_access_t* access = scanAccess->data;
				if(access->model==NETLIST_ACCESS_FIFO_IN || access->model==NETLIST_ACCESS_FIFO_OUT) {
					netlist_access_fifo_t* fifo_data = access->data;
					list_default_values = addbitype(list_default_values, 0, fifo_data->port_ack, namealloc("0"));
				}
			}
		}
	}

	avl_pp_foreach(&Implem->netlist.top->accesses, scanAccess) {
		netlist_access_t* access = scanAccess->data;
		if(access->model==NETLIST_ACCESS_FIFO_IN || access->model==NETLIST_ACCESS_FIFO_OUT) {
			netlist_access_fifo_t* fifo_data = access->data;
			list_default_values = addbitype(list_default_values, 0, fifo_data->port_rdy, namealloc("0"));
		}
		else if(access->model==NETLIST_ACCESS_MEM_WA_EXT) {
			netlist_access_mem_addr_t* memwa_data = access->data;
			list_default_values = addbitype(list_default_values, 0, memwa_data->port_e, namealloc("0"));
		}
	}

	foreach(list_default_values, scan) {
		netlist_port_t* port = scan->DATA_FROM;
		char* value = scan->DATA_TO;
		// Handle special cases
		if(port->source==NULL) continue;
		if(port->source->NEXT!=NULL) continue;
		netlist_val_cat_t* valcat = port->source->DATA;
		if(valcat->source==NULL) continue;
		netlist_port_t* port_src = valcat->source;
		if(port->width!=port_src->width) continue;

		// Handle one level of signals
		if(port_src->component->model==NETLIST_COMP_SIGNAL) {
			netlist_signal_t* sig_data = port_src->component->data;
			port = sig_data->port_in;
			if(port->source==NULL) continue;
			if(port->source->NEXT!=NULL) continue;
			valcat = port->source->DATA;
			if(valcat->source==NULL) continue;
			port_src = valcat->source;
			if(port->width!=port_src->width) continue;
		}

		if(port_src->component->model!=NETLIST_COMP_MUX) continue;

		// Process
		Netlist_Mux_SetDefaultValue(port_src->component, value);
	}

	freebitype(list_default_values);
}

// Create all FSM states
static void Map_Netlist_CreateFsmStates(map_netlist_data_t* map_data) {
	implem_t* Implem = map_data->Implem;
	netlist_fsm_t* comp_fsm = Implem->netlist.fsm->data;

	// Activate retiming
	// FIXME The fanout value is arbitrary
	if(fsmstates_dupcycles==false) {
		comp_fsm->retime_en = true;
		comp_fsm->rtm_counter_bitsnb = 5;
		comp_fsm->rtm_counter_fanout = 32;
	}

	avl_p_foreach(&Implem->H->NODES_ALL, scanNode) {
		hier_node_t* node = scanNode->data;
		netlist_fsm_state_t* state = NULL;

		if(node->TYPE==HIERARCHY_STATE) {
			state = Netlist_Comp_Fsm_AddState(Implem->netlist.fsm);
		}
		else if(node->TYPE==HIERARCHY_LOOP) {
			hier_loop_t* node_loop = &node->NODE.LOOP;
			if(node_loop->body_after==NULL && node_loop->body_before==NULL) {
				state = Netlist_Comp_Fsm_AddState(Implem->netlist.fsm);
			}
		}

		if(state!=NULL) {
			avl_pp_add_overwrite(&map_data->node2state, node, state);
			avl_pp_add_overwrite(&map_data->state2node, state, node);
		}
	}

	// Find the initial state.
	hier_node_t* node = Implem->H->PROCESS;
	do {

		if(node->TYPE==HIERARCHY_PROCESS) {
			node = node->NEXT;
			continue;
		}
		else if(node->TYPE==HIERARCHY_STATE) {
			netlist_fsm_state_t* state = Map_Node2FsmState(map_data, node);
			assert(state!=NULL);
			comp_fsm->init_state = state;
			break;
		}
		else if(node->TYPE==HIERARCHY_BB) {
			node = node->NODE.BB.body;
			continue;
		}
		else if(node->TYPE==HIERARCHY_LOOP) {
			netlist_fsm_state_t* state = Map_Node2FsmState(map_data, node);
			if(state!=NULL) {
				comp_fsm->init_state = state;
				break;
			}
			hier_loop_t* node_loop = &node->NODE.LOOP;
			if(node_loop->body_after!=NULL) {
				node = node_loop->body_after;
				continue;
			}
			if(node_loop->testinfo->cond==NULL && node_loop->body_before!=NULL) {
				node = node_loop->body_before;
				continue;
			}
			if(node_loop->testinfo->cond!=NULL) {
				netlist_fsm_state_t* state = Netlist_Comp_Fsm_AddState(Implem->netlist.fsm);
				avl_pp_add_overwrite(&map_data->node2state, node, state);
				avl_pp_add_overwrite(&map_data->state2node, state, node);
				comp_fsm->init_state = state;
				break;
			}
			// This state should not be reached
			abort();
		}
		else if(node->TYPE==HIERARCHY_LABEL) {
			node = node->NEXT;
			continue;
		}
		else {
			errprintf("No appropriate initial State found.\n");
			abort();
		}

	} while(1);

	if(comp_fsm->init_state==NULL) {
		printf("INTERNAL ERROR %s:%d : The FSM has no initial state.\n", __FILE__, __LINE__);
		abort();
	}

	dbgprintf("%d FSM states were created.\n", comp_fsm->states_nb);

	// Compute the next state, inside BBs
	foreach(comp_fsm->states, scanState) {
		netlist_fsm_state_t* state = scanState->DATA;
		state->rules = Map_Netlist_BuildRule(map_data, state);
	}

	// Check
	unsigned count_error = 0;
	foreach(comp_fsm->states, scanState) {
		netlist_fsm_state_t* state = scanState->DATA;
		if(state->rules==NULL) count_error ++;
	}
	if(count_error>0) {
		printf("WARNING %s:%u : %d states have no next state rule.\n", __FILE__, __LINE__, count_error);
	}
}

static void Map_Netlist_FsmRetiming(map_netlist_data_t* map_data) {
	implem_t* Implem = map_data->Implem;
	netlist_fsm_t* comp_fsm = Implem->netlist.fsm->data;

	dbgprintf("Setting flags for multi-cycle states...\n");

	void get_fsmout_set_lastcycle_recurs(netlist_port_t* orig_port, netlist_port_t* port) {
		if(port->source==NULL) {
			dbgprintf("WARNING: Port '%s' of comp '%s' model '%s' has no source.\n",
				port->name, port->component->name, port->component->model->name
			);
			return;
		}
		if(port->source->NEXT!=NULL) {
			dbgprintf("WARNING: Port '%s' of comp '%s' model '%s' has a non-trivial source.\n",
				port->name, port->component->name, port->component->model->name
			);
			return;
		}
		netlist_val_cat_t* valcat = port->source->DATA;
		if(valcat->literal!=NULL) {
			dbgprintf("WARNING: Port '%s' of comp '%s' model '%s' has a literal source.\n",
				port->name, port->component->name, port->component->model->name
			);
			return;
		}
		netlist_port_t* port_source = valcat->source;
		if(port_source->component->model==NETLIST_COMP_FSM) {
			#if 0
			dbgprintf("Setting LAST_CYCLE flag on FSM output '%s' for port '%s' of comp '%s' model '%s'\n",
				port_source->name, orig_port->name, orig_port->component->name, orig_port->component->model->name
			);
			#endif
			netlist_fsm_outval_t* outval = Netlist_Comp_Fsm_GetOutVal(port_source);
			outval->is_buffered = false;
			foreach(outval->actions, scanAction) {
				netlist_fsm_action_t* action = scanAction->DATA;
				action->only_last_cycle = true;
			}
		}
		else if(port_source->component->model==NETLIST_COMP_SIGNAL) {
			netlist_signal_t* signal_data = port_source->component->data;
			get_fsmout_set_lastcycle_recurs(orig_port, signal_data->port_in);
		}
		else if(port_source->component->model==NETLIST_COMP_MUX) {
			avl_pp_foreach(&port_source->component->accesses, scanAccess) {
				netlist_access_t* access = scanAccess->data;
				netlist_mux_access_t* muxin_data = access->data;
				get_fsmout_set_lastcycle_recurs(orig_port, muxin_data->port_en);
			}
		}
		else {
			// Note: here we may be in a case where two components are hard-connected
			#if 0
			dbgprintf("WARNING: Unhandled source port '%s' of comp '%s' model '%s'\n",
				port_source->name, port_source->component->name, port_source->component->model->name
			);
			#endif
		}
	}
	void get_fsmout_set_lastcycle(netlist_port_t* port) {
		get_fsmout_set_lastcycle_recurs(port, port);
	}

	avl_pp_foreach(&Implem->netlist.top->children, scanComp) {
		netlist_comp_t* comp = scanComp->data;
		if(comp->model==NETLIST_COMP_REGISTER) {
			netlist_register_t* reg_data = comp->data;
			if(reg_data->port_en!=NULL) get_fsmout_set_lastcycle(reg_data->port_en);
		}
		else if(comp->model==NETLIST_COMP_MEMORY) {
			netlist_memory_t* mem_data = comp->data;
			avl_p_foreach(&mem_data->access_ports_ra, scanPort) {
				netlist_access_t* access = scanPort->data;
				netlist_access_mem_addr_t* memAcc_data = access->data;
				if(memAcc_data->port_e!=NULL) get_fsmout_set_lastcycle(memAcc_data->port_e);
			}
			avl_p_foreach(&mem_data->access_ports_wa, scanPort) {
				netlist_access_t* access = scanPort->data;
				netlist_access_mem_addr_t* memAcc_data = access->data;
				get_fsmout_set_lastcycle(memAcc_data->port_e);
			}
			avl_ip_foreach(&mem_data->access_ports_d, scanPort) {
				netlist_access_t* access = scanPort->data;
				netlist_access_mem_direct_t* memAcc_data = access->data;
				if(memAcc_data->port_we!=NULL) get_fsmout_set_lastcycle(memAcc_data->port_we);
			}
		}
		else if(comp->model==NETLIST_COMP_PINGPONG_IN || comp->model==NETLIST_COMP_PINGPONG_OUT) {
			netlist_pingpong_t* pp_data = comp->data;
			// The SWITCH command
			get_fsmout_set_lastcycle(pp_data->port_switch);
			// The data ports
			avl_p_foreach(&pp_data->access_ports_ra, scanPort) {
				netlist_access_t* access = scanPort->data;
				netlist_access_mem_addr_t* memAcc_data = access->data;
				if(memAcc_data->port_e!=NULL) get_fsmout_set_lastcycle(memAcc_data->port_e);
			}
			avl_p_foreach(&pp_data->access_ports_wa, scanPort) {
				netlist_access_t* access = scanPort->data;
				netlist_access_mem_addr_t* memAcc_data = access->data;
				get_fsmout_set_lastcycle(memAcc_data->port_e);
			}
			avl_ip_foreach(&pp_data->access_ports_d, scanPort) {
				netlist_access_t* access = scanPort->data;
				netlist_access_mem_direct_t* memAcc_data = access->data;
				if(memAcc_data->port_we!=NULL) get_fsmout_set_lastcycle(memAcc_data->port_we);
			}
		}
		else {
			avl_pp_foreach(&comp->accesses, scanAccess) {
				netlist_access_t* access = scanAccess->data;
				if(access->model==NETLIST_ACCESS_FIFO_IN || access->model==NETLIST_ACCESS_FIFO_OUT) {
					netlist_access_fifo_t* fifo_data = access->data;
					get_fsmout_set_lastcycle(fifo_data->port_ack);
				}
			}
		}
	}

	avl_pp_foreach(&Implem->netlist.top->accesses, scanAccess) {
		netlist_access_t* access = scanAccess->data;
		if(access->model==NETLIST_ACCESS_FIFO_IN || access->model==NETLIST_ACCESS_FIFO_OUT) {
			netlist_access_fifo_t* fifo_data = access->data;
			get_fsmout_set_lastcycle(fifo_data->port_rdy);
		}
	}

	#if 0  // Debug : Show the design size
	dbgprintf("Resource usage :\n");
	bitype_list* list_res = Techno_Netlist_EvalSize(Implem->netlist.top);
	Techno_NameOpRes_Print(list_res);
	Techno_NameOpRes_Free(list_res);
	#endif

	dbgprintf("Setting multi-cycle durations...\n");
	dbgprintf("FSM states: add %u set %u dup %s\n", fsmstates_addcycles, fsmstates_setcycles, fsmstates_dupcycles==true ? "yes" : "no");

	avl_p_foreach(&Implem->H->NODES[HIERARCHY_STATE], scanState) {
		hier_node_t* nodeState = scanState->data;
		hier_state_t* state_data = &nodeState->NODE.STATE;

		netlist_fsm_state_t* fsmstate = Map_Node2FsmState(map_data, nodeState);
		if(fsmstate==NULL) continue;  // paranoia

		fsmstate->cycles_nb = state_data->time.nb_clk;
		if(fsmstate->cycles_nb<1) fsmstate->cycles_nb = 1;

		// Apply changes according to retiming style
		fsmstate->cycles_nb += fsmstates_addcycles;
		if(fsmstates_setcycles>0) fsmstate->cycles_nb = fsmstates_setcycles;

	}  // Scan all State nodes

	// Duplicate multi-cycle states
	if(objective_simu==false && fsmstates_dupcycles==true) {
		dbgprintf("Duplicating multi-cycle states...\n");

		chain_list* fsmstates_to_dup = NULL;

		// Because new states will be added, we have to work on a local list of states
		foreach(comp_fsm->states, scanState) {
			netlist_fsm_state_t* state = scanState->DATA;
			if(state->cycles_nb<=1) continue;
			fsmstates_to_dup = addchain(fsmstates_to_dup, state);
		}

		foreach(fsmstates_to_dup, scanState) {
			netlist_fsm_state_t* state = scanState->DATA;

			//dbgprintf("Duplicating an FSM state, number of cycles is %u\n", state->cycles_nb);
			// Create N-1 additional states (before the original one), duplicate FSM actions
			netlist_fsm_state_t* arrstate[state->cycles_nb];
			for(unsigned i=0; i<state->cycles_nb-1; i++) {
				netlist_fsm_state_t* newstate = Netlist_Comp_Fsm_AddState(Implem->netlist.fsm);
				foreach(state->actions, scanAction) {
					netlist_fsm_action_t* action = scanAction->DATA;
					// Skip actions that must be executed only at the last cycle
					if(action->only_last_cycle==true) continue;
					Netlist_Comp_Fsm_AddAction(Implem->netlist.fsm, newstate, action->outval->port, action->value);
				}
				arrstate[i] = newstate;
			}
			arrstate[state->cycles_nb-1] = state;

			// Change destination of the branch rules in ALL states of the FSM
			// when the target state is the current state, set the first of the new states
			void fsm_change_rule(netlist_fsm_rule_t* rule) {
				if(rule==NULL) return;
				if(rule->state==state) rule->state = arrstate[0];
				fsm_change_rule(rule->rule_true);
				fsm_change_rule(rule->rule_false);
			}
			foreach(comp_fsm->states, scanState) {
				netlist_fsm_state_t* other_state = scanState->DATA;
				fsm_change_rule(other_state->rules);
			}

			// Create the branch rules for the new states
			for(unsigned i=0; i<state->cycles_nb-1; i++) {
				netlist_fsm_rule_t* rule = Netlist_FsmRule_New();
				rule->state = arrstate[i+1];
				arrstate[i]->rules = rule;
			}

			// Reset to 1 the number of cycles for this State
			state->cycles_nb = 1;
		}

		freechain(fsmstates_to_dup);
	}  // End duplication of FSM states

}



//======================================================================
// Rewrite FSM rules
//======================================================================

// Return an existing FSM input port, or NULL if none is found
static netlist_port_t* Map_Netlist_FsmPort_FromSigName(implem_t* Implem, char* name) {
	netlist_comp_t* comp = Map_Netlist_GetComponent(Implem, name);
	netlist_port_t* top_port = Netlist_Comp_GetPort(Implem->netlist.top, name);

	if(top_port!=NULL && top_port->direction==NETLIST_PORT_DIR_IN) {
		// There may be wired conditions etc that read that port
		// So scan the targets in search for an existing link to the FSM
		if(Netlist_PortTargets_CheckEmpty(top_port)==false) {
			avl_p_foreach(top_port->target_ports, scan) {
				netlist_port_t* target_port = scan->data;
				if(target_port->component==Implem->netlist.fsm) return target_port;
			}
		}
		// Here there was no direct to the FSM
		// The situation where there are several targets is not handled
		if(Netlist_PortTargets_GetNb(top_port) != 1) return NULL;
		// Continue by checking the components, assuming there is only one target component
		comp = Netlist_PortTargets_GetFirst(top_port)->component;
	}

	assert(comp!=NULL);

	do {
		netlist_port_t* port_out = NULL;

		if(comp->model==NETLIST_COMP_SIGNAL) {
			netlist_register_t* sig_data = comp->data;
			port_out = sig_data->port_out;
		}

		// Traverse registers to handle double buffering
		if(comp->model==NETLIST_COMP_REGISTER) {
			netlist_register_t* reg_data = comp->data;
			port_out = reg_data->port_out;
		}
		assert(port_out!=NULL);

		// There may be wired conditions etc that read that port
		// So scan the targets in search for an existing link to the FSM
		if(Netlist_PortTargets_CheckEmpty(port_out)==false) {
			avl_p_foreach(port_out->target_ports, scan) {
				netlist_port_t* target_port = scan->data;
				if(target_port->component==Implem->netlist.fsm) return target_port;
			}
		}
		// Here there was no direct to the FSM
		// The situation where there are several targets is not handled
		if(Netlist_PortTargets_GetNb(port_out) != 1) return NULL;
		// Continue by checking the components, assuming there is only one target component
		comp = Netlist_PortTargets_GetFirst(port_out)->component;

	}while(1);

	return NULL;
}

// Recursive scan of the VEX expr
static void Map_Netlist_FsmRule_ReplaceVex(implem_t* Implem, hvex_t* Expr) {
	if(Expr==NULL) return;
	// Scan the operands
	hvex_foreach(Expr->operands, VexOp) Map_Netlist_FsmRule_ReplaceVex(Implem, VexOp);
	// Process the local name
	if(Expr->model==HVEX_VECTOR) {
		char* name = hvex_vec_getname(Expr);
		assert(name!=NULL);
		netlist_port_t* port_fsm = Map_Netlist_FsmPort_FromSigName(Implem, name);
		assert(port_fsm!=NULL);
		hvex_vec_setname(Expr, port_fsm->name);
	}
}

// Recursive scan of the rules
static void Map_Netlist_FsmRule_Replace(implem_t* Implem, netlist_fsm_rule_t* rule) {
	if(rule==NULL) return;
	// Process the local condition
	if(rule->condition!=NULL) {
		chain_list* list = hvex_SearchVector_AddList(NULL, rule->condition, NULL);
		foreach(list, scan) {
			hvex_t* Expr = scan->DATA;
			char* name = hvex_vec_getname(Expr);
			assert(name!=NULL);
			netlist_port_t* port_fsm = Map_Netlist_FsmPort_FromSigName(Implem, name);
			assert(port_fsm!=NULL);
			hvex_vec_setname(Expr, port_fsm->name);
		}
		freechain(list);
		// Process the branches
		Map_Netlist_FsmRule_Replace(Implem, rule->rule_true);
		Map_Netlist_FsmRule_Replace(Implem, rule->rule_false);
		return;
	}
	// The calls/returns
	foreach(rule->func_call, scan) Map_Netlist_FsmRule_Replace(Implem, scan->DATA_TO);
	foreach(rule->func_rets, scan) Map_Netlist_FsmRule_Replace(Implem, scan->DATA_TO);
}

// Replace design signals in FSM rules by actual FSM ports
static void Map_Netlist_FsmFixNames(map_netlist_data_t* map_data) {
	implem_t* Implem = map_data->Implem;
	netlist_fsm_t* comp_fsm = Implem->netlist.fsm->data;

	foreach(comp_fsm->states, scanState) {
		netlist_fsm_state_t* state = scanState->DATA;
		Map_Netlist_FsmRule_Replace(Implem, state->rules);
	}

}



//======================================================================
// Main mapping function
//======================================================================

// For debug only
// Activate this if generated VHDL has wrong syntax
bool debug_all_checks = false;

int Map_Netlist(implem_t* Implem) {
	if(Implem->H==NULL) return 0;

	// Create the FSM
	Map_Netlist_CreateFsm_check(Implem);

	// Add missing clock and reset signals, ports
	Map_Netlist_CheckConnectSystemPorts(Implem);

	// For debug
	//Hier_PrintTree(Implem->H);

	map_netlist_data_t* map_data = malloc(sizeof(map_netlist_data_t));
	Map_Netlist_MapData_Init(map_data);
	map_data->Implem = Implem;
	Implem->netlist.mapping = map_data;

	// In each State, merge all ASG with same dest symbol
	Hier_MergeSameSymAsg(Implem->H);

	// Fix the right index of assignments to registers
	Map_Netlist_ExtendRight(Implem);

	// Add all existing components to the CompType tree
	avl_pp_foreach(&Implem->netlist.top->children, scan) Map_CompType_AddComp(map_data, scan->data);

	// WARNING: The hierarchy is supposed to be already fully simplified and optimized.
	// But a few checks here might not be too much.

	// Ensure perfect integrity of the entire Implem
	#ifndef NDEBUG
	Implem_CheckIntegrity(Implem);
	// Check that all assignations have correct width
	// (the expression can be shorter than the destination, but not larger)
	avl_p_foreach(&Implem->H->NODES[HIERARCHY_STATE], scanState) {
		hier_node_t* node = scanState->data;
		hier_state_t* state_data = &node->NODE.STATE;
		foreach(state_data->actions, scanAction) {
			hier_action_t* action = scanAction->DATA;
			hvex_t* vex_dest = hvex_asg_get_dest(action->expr);
			hvex_t* vex_expr = hvex_asg_get_expr(action->expr);
			assert(vex_expr->width <= vex_dest->width);
		}
	}
	#endif

	// Create the structures in FSM for function calls
	avl_p_foreach(&Implem->H->NODES[HIERARCHY_PROCESS], scan) {
		hier_node_t* node = scan->data;
		hier_proc_t* proc_data = &node->NODE.PROCESS;
		if(proc_data->calls==NULL) continue;
		netlist_fsm_func_t* fsmfunc = Netlist_Comp_Fsm_Func_New(Implem->netlist.fsm, proc_data->name_orig);
		assert(fsmfunc!=NULL);
		// Create the call IDs
		foreach(proc_data->calls, scanCall) {
			hier_node_t* nodeCall = scanCall->DATA;
			int id = fsmfunc->calls_nb ++;
			avl_pi_add_overwrite(&map_data->call2id, nodeCall, id);
		}
	}

	// Create all FSM states
	Map_Netlist_CreateFsmStates(map_data);

	// Create one multiplexer for each input of all components
	// And of all top-level entity output ports
	Map_Netlist_CreateMux(map_data);

	#if 0  // Debug : Print all component names
	dbgprintf("All component names :");
	avl_pp_foreach(&Implem->netlist.top->children, scanComp) {
		netlist_comp_t* comp = scanComp->data;
		printf(" %s", comp->name);
	}
	printf("\n");
	#endif

	#if 0  // Debug : Show the design size
	dbgprintf("Resource usage :\n");
	bitype_list* list_res = Techno_Netlist_EvalSize(Implem->netlist.top);
	Techno_NameOpRes_Print(list_res);
	Techno_NameOpRes_Free(list_res);
	#endif

	// Find Actions with wired condition, that target top-level ports or signals, and where the Mux has a default value
	avl_p_foreach(&Implem->H->NODES[HIERARCHY_STATE], scanState) {
		hier_node_t* node = scanState->data;
		hier_state_t* state_data = &node->NODE.STATE;
		foreach(state_data->actions, scanAction) {
			hier_action_t* action = scanAction->DATA;
			if(action->expr->model!=HVEX_ASG_EN) continue;

			// Only process Actions with wired condition
			hvex_t* vex_cond = hvex_asg_get_cond(action->expr);
			assert(vex_cond!=NULL);

			// Find the port that is the Action target
			char* name = hvex_asg_get_destname(action->expr);
			assert(name!=NULL);
			netlist_port_t* port = NULL;
			netlist_comp_t* comp = Netlist_Comp_GetChild(Implem->netlist.top, name);
			if(comp!=NULL) {
				if(comp->model!=NETLIST_COMP_SIGNAL) continue;
				netlist_signal_t* sig_data = comp->data;
				port = sig_data->port_in;
			}
			if(port==NULL) {
				port = Netlist_Comp_GetPort(Implem->netlist.top, name);
			}
			if(port==NULL) {
				printf("INTERNAL ERROR %s:%u : Dest symbol '%s' not found.\n", __FILE__, __LINE__, name);
				abort();
			}
			// Find the source MUX, check if it has a default value
			netlist_comp_t* mux = Map_Netlist_GetSourceMux(port);
			netlist_mux_t* mux_data = mux->data;
			if(mux_data->default_value==NULL) continue;

			// Here, modify the vex Expr and remove the vex condition
			hvex_t* tmp_vexlit = hvex_newlit(mux_data->default_value);
			hvex_t* vex_expr = hvex_asg_get_expr(action->expr);
			hvex_t* new_expr = hvex_new_move(vex_expr);
			hvex_resize(new_expr, mux_data->width);
			hvex_unlinkop(vex_cond);
			new_expr = hvex_newmodel_opn(HVEX_MUXB, new_expr->width, vex_cond, tmp_vexlit, new_expr, NULL);
			hvex_replace_free(vex_expr, new_expr);

			action->expr->model = HVEX_ASG;
		}
	}

	// Initialize the dictionary of expressions
	Map_Netlist_BuildAllExpr(map_data);

	dbgprintf("Mapping all states...\n");

	netlist_fsm_t* comp_fsm = Implem->netlist.fsm->data;

	// Map the states
	foreach(comp_fsm->states, scanState) {
		netlist_fsm_state_t* state = scanState->DATA;
		Map_Netlist_Datapath_State(map_data, state);
	}
	// Map the branch conditions
	foreach(comp_fsm->states, scanState) {
		netlist_fsm_state_t* state = scanState->DATA;
		Map_Netlist_ConnectSigBranchCond(map_data, state->rules);
	}

	if(debug_all_checks==true){
		dbgprintf("Checking Netlist...\n");
		Netlist_Debug_CheckIntegrity(Implem->netlist.top);
	}

	if(filename_dump_states!=NULL) {
		Map_Netlist_DumpFsmStates(map_data, filename_dump_states);
	}

	dbgprintf("Getting usage of all output ports...\n");

	// Analyze the output ports that are actually read somewhere
	Netlist_PortTargets_CompBuild(Implem->netlist.top);

	dbgprintf("Simplifying all components but FSM...\n");

	Map_Netlist_Simplify_flags(map_data, true);

	dbgprintf("Processing retiming steps...\n");

	Map_Netlist_FsmRetiming(map_data);

	dbgprintf("Replacing design signals in FSM rules by actual FSM ports...\n");

	Map_Netlist_FsmFixNames(map_data);

	dbgprintf("Simplifying all components including FSM...\n");

	Map_Netlist_Simplify_flags(map_data, false);

	if(debug_all_checks==true) {
		dbgprintf("Checking Netlist...\n");
		Netlist_Debug_CheckIntegrity(Implem->netlist.top);
	}

	if(do_timing_analysis==true) {
		dbgprintf("Precise timing analysis...\n");
		Techno_Delay(Implem);
		Hier_Timing_Update(Implem, true);
		// FIXME Set back delays from estimations. This is not very efficient
		Hier_Timing_Update(Implem, false);
	}

	if(filename_dump_states!=NULL) {
		char buf[strlen(filename_dump_states) + 10];
		sprintf(buf, "%s.dup", filename_dump_states);
		Map_Netlist_DumpFsmStates(map_data, buf);
	}

	// Clean
	//Map_Netlist_MapData_Reset(map_data);

	return 0;
}



//======================================================================
// VHDL generation
//======================================================================

// Insert signals before all in and out component ports.
// This is to generate VHDL.
// Component instantiation implies having clean signal names to connect each port.
// FIXME this function should be in the netlist module, and it should work only on a copy of the netlist.
static void Map_Netlist_InsertSig(implem_t* Implem, netlist_vhdl_t* params) {

	Netlist_PortTargets_CompClear(Implem->netlist.top);
	Netlist_PortTargets_CompBuild(Implem->netlist.top);

	// Process the outputs first
	// Because that way, a signal is created only once, instead of first creating
	// one signal per destination component, then another for the output.

	// A tree to list all ValCat elements that refer to output ports (comp out of top-level in)
	// key = source port, data = a bitype_list*
	// List usage: DATA_FROM = valcat*, DATA_TO = dest port (owner of the valcat element)
	avl_pp_tree_t valcat_uses_out;
	avl_pp_init(&valcat_uses_out);

	avl_pp_foreach(&Implem->netlist.top->children, scanComp) {
		netlist_comp_t* comp = scanComp->data;
		avl_pp_foreach(&comp->ports, scanPort) {
			netlist_port_t* port = scanPort->data;
			if(port->direction!=NETLIST_PORT_DIR_IN) continue;
			if(port->source==NULL) {
				printf("ERROR: The input port '%s' of comp '%s' model '%s' has no source.\n", port->name, comp->name, comp->model->name);
				abort();
			}
			foreach(port->source, scanValCat) {
				netlist_val_cat_t* valcat = scanValCat->DATA;
				if(valcat->source==NULL) continue;
				// Add in the usage lists
				avl_pp_node_t* node = NULL;
				bool b = avl_pp_add(&valcat_uses_out, valcat->source, &node);
				if(b==true) node->data = NULL;
				node->data = addbitype(node->data, 0, valcat, port);
			}
		}
	}
	avl_pp_foreach(&Implem->netlist.top->ports, scanPort) {
		netlist_port_t* port = scanPort->data;
		if(port->direction!=NETLIST_PORT_DIR_OUT) continue;
		foreach(port->source, scanValCat) {
			netlist_val_cat_t* valcat = scanValCat->DATA;
			if(valcat->source==NULL) continue;
			// Add in the usage lists
			avl_pp_node_t* node = NULL;
			bool b = avl_pp_add(&valcat_uses_out, valcat->source, &node);
			if(b==true) node->data = NULL;
			node->data = addbitype(node->data, 0, valcat, port);
		}
	}

	// Insert intermediate signals for all component outputs

	// Use a temporary list of ports. Because new components are added.
	chain_list* ports_to_process = NULL;
	avl_pp_foreach(&Implem->netlist.top->children, scanComp) {
		netlist_comp_t* comp = scanComp->data;
		if(comp->model==NETLIST_COMP_SIGNAL) continue;
		if(params->inline_comps==true && comp->model->inlvhdl_en==true) continue;
		avl_pp_foreach(&comp->ports, scanPort) {
			netlist_port_t* port = scanPort->data;
			if(port->direction!=NETLIST_PORT_DIR_OUT) continue;
			// Skip ports that are already connected to a SIGNAL, or to a top-level port
			//   only if the full width is used
			// FIXME Partial destination is OK as long as there is no repeat -> but need to generate the range in VHDL
			avl_pp_node_t* node = NULL;
			bool b = avl_pp_find(&valcat_uses_out, port, &node);
			if(b==true) {
				bitype_list* list_targets = node->data;
				if(ChainBiType_Count(list_targets)==1) {
					netlist_port_t* target_port = list_targets->DATA_TO;
					if(
						(target_port->component==Implem->netlist.top || target_port->component->model==NETLIST_COMP_SIGNAL) &&
						Netlist_Port_IsSourceFullPort(target_port, port)==true
					){
						// The destination uses the full port, the symbol can be reused
						continue;
					}
				}
			}
			// A signal will be inserted for this port
			ports_to_process = addchain(ports_to_process, port);
		}
	}

	// Actually create the signals
	foreach(ports_to_process, scanPort) {
		netlist_port_t* port = scanPort->DATA;
		// Insert a new signal.
		char* new_name = Map_Netlist_MakeInstanceName(Implem, NETLIST_COMP_SIGNAL);
		netlist_comp_t* newSig = Netlist_Comp_Sig_New(new_name, port->width);
		Netlist_Comp_SetChild(Implem->netlist.top, newSig);
		// Update the connections
		netlist_signal_t* sig_data = newSig->data;
		sig_data->port_in->source = Netlist_ValCat_MakeList_FullPort(port);
		avl_pp_node_t* node = NULL;
		bool b = avl_pp_find(&valcat_uses_out, port, &node);
		if(b==true) {
			foreach((chain_list*)node->data, scanValCat) {
				netlist_val_cat_t* valcat = scanValCat->DATA;
				valcat->source = sig_data->port_out;
			}
		}
	}
	freechain(ports_to_process);
	ports_to_process = NULL;

	avl_pp_foreach(&valcat_uses_out, node) freebitype(node->data);
	avl_pp_reset(&valcat_uses_out);

	// Insert intermediate signals for all component inputs

	// Use a temporary list of ports. Because new components are added.
	avl_pp_foreach(&Implem->netlist.top->children, scanComp) {
		netlist_comp_t* comp = scanComp->data;
		if(comp->model==NETLIST_COMP_SIGNAL) continue;
		if(params->inline_comps==true && comp->model->inlvhdl_en==true) continue;
		avl_pp_foreach(&comp->ports, scanPort) {
			netlist_port_t* port = scanPort->data;
			if(port->direction!=NETLIST_PORT_DIR_IN) continue;
			ports_to_process = addchain(ports_to_process, port);
		}
	}

	foreach(ports_to_process, scanPort) {
		netlist_port_t* port = scanPort->DATA;
		if(port->direction!=NETLIST_PORT_DIR_IN) continue;
		assert(port->source!=NULL);

		// Check if inserting a signal is needed
		bool can_skip = true;

		#if 0
		// Only one element, must not be a repeated bit
		can_skip = false;
		if(port->source->NEXT==NULL) {
			netlist_val_cat_t* valcat = port->source->DATA;
			if(valcat->width==1 || valcat->left!=valcat->right) {
				if(valcat->literal!=NULL) can_skip = true;
				if(valcat->source!=NULL) {
					netlist_port_t* other_port = valcat->source;
					if(other_port->component==Implem->netlist.top) can_skip = true;
					if(other_port->component->model==NETLIST_COMP_SIGNAL) can_skip = true;
				}
			}
		}
		#endif

		#if 1
		// Only one element
		can_skip = false;
		if(port->source->NEXT==NULL) {
			netlist_val_cat_t* valcat = port->source->DATA;
			if(valcat->width==1 || valcat->left!=valcat->right) {
				if(valcat->literal!=NULL) can_skip = true;
				if(valcat->source!=NULL) {
					netlist_port_t* other_port = valcat->source;
					if(other_port->component==Implem->netlist.top) can_skip = true;
					if(other_port->component->model==NETLIST_COMP_SIGNAL) can_skip = true;
					if(params->inline_comps==true && other_port->component->model->inlvhdl_en) can_skip = true;
				}
			}
			else {
				if(valcat->literal!=NULL) can_skip = true;
			}
		}
		#endif

		#if 0
		// This code is disabled because GHDL refuses component instantiation with
		// this kind of port assignation : port => literal & signal
		// Ditto with these inputs : port => (31 downto 0 => signal)
		can_skip = true;
		foreach(port->source, scan) {
			netlist_val_cat_t* valcat = scan->DATA;
			if(valcat->literal!=NULL) continue;
			if(valcat->source!=NULL) {
				netlist_port_t* other_port = valcat->source;
				if(other_port->component==Implem->netlist.top) continue;
				if(other_port->component->comp_name==NETLIST_COMP_SIGNAL) continue;
				if(params->inline_comps==true && other_port->component->model->inlvhdl_en) continue;
				can_skip = false;
			}
		}
		#endif

		if(can_skip==true) continue;

		// Insert an intermediate signal
		char* new_name = Map_Netlist_MakeInstanceName(Implem, NETLIST_COMP_SIGNAL);
		netlist_comp_t* newSig = Netlist_Comp_Sig_New(new_name, port->width);
		Netlist_Comp_SetChild(Implem->netlist.top, newSig);
		// Update the connections
		netlist_signal_t* sig_data = newSig->data;
		sig_data->port_in->source = port->source;
		port->source = Netlist_ValCat_MakeList_FullPort(sig_data->port_out);
	}
	freechain(ports_to_process);

	// Rebuild targets of ports because not everything could be updated
	Netlist_PortTargets_CompClear(Implem->netlist.top);
	Netlist_PortTargets_CompBuild(Implem->netlist.top);

}

// Rename all components, to avoid collisions
static void Map_Netlist_RenameComps(implem_t* Implem, netlist_vhdl_t* params) {
	// Reset
	avl_p_reset(&params->global_ids);
	avl_pp_reset(&params->comp_rename);

	// Insert all top-level port names
	// They can't be same than component types and instance names
	avl_pp_foreach(&Implem->netlist.top->ports, scan) {
		netlist_port_t* port = scan->data;
		avl_p_add_overwrite(&params->global_ids, port->name, port->name);
	}

	// This is a local recursive function
	// To scan all components
	void scan_rename_comp(netlist_comp_t* comp) {
		// Get the rename data
		bitype_list* comp_ren = Netlist_Comp_Rename(comp, params);

		// For debug
		//dbgprintf("Rename comp '%s' => '%s' inst '%s'\n", comp->name, (char*)comp_ren->DATA_FROM, (char*)comp_ren->DATA_TO);

		// Save the new names
		avl_p_add_overwrite(&params->global_ids, comp_ren->DATA_FROM, comp_ren->DATA_FROM);
		avl_p_add_overwrite(&params->global_ids, comp_ren->DATA_TO, comp_ren->DATA_TO);
		avl_pp_add_overwrite(&params->comp_rename, comp, comp_ren);

		// Recursive scan of all other components
		avl_pp_foreach(&comp->children, scan) scan_rename_comp(scan->data);
	}

	scan_rename_comp(Implem->netlist.top);
}

// For getcwd()
#include <unistd.h>

// For mkdir()
#include <sys/stat.h>
#include <errno.h>

void Map_Netlist_GenVHDL(implem_t* Implem) {

	dbgprintf("Simplifying component connectivity expressions...\n");
	Netlist_Comp_Simplify(Implem->netlist.top);

	dbgprintf("Inserting intermediate signals...\n");

	if(debug_all_checks==true) {
		dbgprintf("Checking Netlist...\n");
		Netlist_Debug_CheckIntegrity(Implem->netlist.top);
	}

	// Generation parameters
	netlist_vhdl_t params;
	Netlist_GenParams_Init(&params);
	params.vhdl_prefix = Implem->env.vhdl_prefix;

	Map_Netlist_InsertSig(Implem, &params);

	if(debug_all_checks==true) {
		dbgprintf("Checking Netlist...\n");
		Netlist_Debug_CheckIntegrity(Implem->netlist.top);
	}

	dbgprintf("Generating VHDL in directory '%s'...\n", Implem->env.vhdl_dir);

	char* oldwd = getcwd(NULL, 0);
	if(oldwd==NULL) {
		errprintf("Can't get the current working directory.\n");
		printf("Reason: %s\n", strerror(errno));
		abort();
	}

	char pathbuf[50 + strlen(Implem->env.vhdl_dir)];
	sprintf(pathbuf, "mkdir -p '%s'", Implem->env.vhdl_dir);
	int z = system(pathbuf);
	if(z!=0) {
		errprintf("Can't create the vhdl directory '%s' (code %i).\n", Implem->env.vhdl_dir, z);
		abort();
	}

	// Doesn't work because only one directory level can be created
	// And it would fail if the directory already exists
	#if 0
	if(mkdir(Implem->env.vhdl_dir,  S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH)!=0) {
		errprintf("Can't create the vhdl directory '%s'.\n", Implem->env.vhdl_dir);
		printf("Reason: %s\n", strerror(errno));
		abort();
	}
	#endif

	if(chdir(Implem->env.vhdl_dir)!=0) {
		errprintf("Can't change to vhdl directory '%s'.\n", Implem->env.vhdl_dir);
		printf("Reason: %s\n", strerror(errno));
		abort();
	}

	// Rename components
	Map_Netlist_RenameComps(Implem, &params);

	// Actual VHDL generation
	Netlist_GenVHDL(Implem->netlist.top, &params);

	// Clear
	Netlist_GenParams_Clear(&params);

	chdir(oldwd);
	free(oldwd);
}



//======================================================================
// Duplication of the netlist allocation
//======================================================================

int Map_Netlist_Dup(implem_t* newImplem) {
	implem_t* Implem = newImplem->DUP.father;

	// To store links between old and new pointers
	avl_pp_tree_t tree_old2new_loc;
	avl_pp_init(&tree_old2new_loc);

	// The duplication itself
	newImplem->netlist.top = Netlist_Comp_Dup_Lnk(&tree_old2new_loc, Implem->netlist.top);

	// Insert the links between old and new pointers into the main structures
	avl_pp_foreach(&tree_old2new_loc, scan) {
		Implem_Dup_LinkPointers(newImplem, scan->key, scan->data);
	}
	avl_pp_reset(&tree_old2new_loc);

	// Link the special netlist elements
	newImplem->netlist.fsm = Implem_Dup_Old2New(newImplem, Implem->netlist.fsm);

	newImplem->netlist.sig_clock = Implem_Dup_Old2New(newImplem, Implem->netlist.sig_clock);
	newImplem->netlist.sig_reset = Implem_Dup_Old2New(newImplem, Implem->netlist.sig_reset);
	newImplem->netlist.sig_start = Implem_Dup_Old2New(newImplem, Implem->netlist.sig_start);

	newImplem->netlist.port_clock = Implem_Dup_Old2New(newImplem, Implem->netlist.port_clock);
	newImplem->netlist.port_reset = Implem_Dup_Old2New(newImplem, Implem->netlist.port_reset);
	newImplem->netlist.port_start = Implem_Dup_Old2New(newImplem, Implem->netlist.port_start);

	// Copy other fields
	newImplem->netlist.comp_name_index = Implem->netlist.comp_name_index;

	return 0;
}



//======================================================================
// Command interpreter
//======================================================================

#include "command.h"

int Map_Command(implem_t* Implem, command_t* cmd_data) {
	const char* cmd = Command_PopParam(cmd_data);
	if(cmd==NULL) {
		printf("Error 'map' : No command received. Try 'help'.\n");
		return -1;
	}

	if(strcmp(cmd, "help")==0) {
		printf(
			"Available commands:\n"
			"  help                 Display this help\n"
			"  dump-states-en <filename>\n"
			"                       Enable generation of a post-mapping text file\n"
			"                       describing the Actions executed at each FSM state\n"
			"  dump-states-dis      Disable generation of post-mapping file\n"
			"  fsmretime [auto] [dup] [none] [+<nb>] [[=]<nb>]\n"
			"                       Tamper with FSM retiming.\n"
			"                         The default is to create clock cycle counters in the FSM.\n"
			"                       [none] forces all states to last exactly one clock cycle.\n"
			"                       [dup] will duplicate clock cycles instead of using a cycle counter.\n"
			"                       [+<nb>] will add <nb> cycles to the duration of all states.\n"
			"                       [[=]<nb>] will force all states to last exactly <nb> cycles.\n"
			"  simp-obfuscate [bool]\n"
			"                       Allow netlist simplifications to obfuscate functionality\n"
			"                       Init=true, default=true\n"
			"  postmap-timing [bool]\n"
			"                       Perform post-mapping timing analysis on netlist (WORK IN PROGRESS)\n"
			"                       Init=false, default=true\n"
		);
		return 0;
	}

	// Commands that only set parameters

	if(strcmp(cmd, "dump-states-en")==0) {
		char* filename_dump_states = Command_PopParam_stralloc(cmd_data);
		if(filename_dump_states==NULL) {
			printf("Error : Missing parameter for command '%s'.\n", cmd);
			return -1;
		}
	}
	else if(strcmp(cmd, "dump-states-dis")==0) {
		filename_dump_states = NULL;
	}
	else if(strcmp(cmd, "postmap-timing")==0) {
		int z = Command_PopParam_YesNoDef_Verbose(cmd_data, true, cmd);
		if(z<0) return -1;
		do_timing_analysis = z;
		return 0;
	}

	else if(strcmp(cmd, "fsmretime")==0) {
		unsigned nb_params = 0;
		do {
			const char* param = Command_PopParam_stralloc(cmd_data);
			if(param==NULL) break;
			nb_params++;
			if(strcmp(param, "auto")==0) {
				fsmstates_dupcycles = false;
				fsmstates_addcycles = 0;
				fsmstates_setcycles = 0;
			}
			else if(strcmp(param, "dup")==0) {
				fsmstates_dupcycles = true;
			}
			else if(strcmp(param, "none")==0) {
				fsmstates_dupcycles = false;
				fsmstates_setcycles = 1;
			}
			else {
				if(param[0]=='+') {
					int val = atoi(param + 1);
					if(val<0) {
						printf("Error: Wrong parameter '%s' for command '%s'.\n", param, cmd);
						return -1;
					}
					fsmstates_addcycles = val;
				}
				else {
					int val = 0;
					if(param[0]=='=') val = atoi(param + 1);
					else val = atoi(param);
					if(val<1) {
						printf("Error : Wrong parameter '%s' for command '%s'.\n", param, cmd);
						return -1;
					}
					fsmstates_setcycles = val;
				}
			}

		}while(1);
		if(nb_params==0) {
			printf("Error: Missing parameter for command '%s'.\n", cmd);
			return -1;
		}
	}

	else {
		printf("Error 'map' : Unknown command '%s'.\n", cmd);
		return -1;
	}

	return 0;
}


