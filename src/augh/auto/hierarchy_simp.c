
// Contents of this file: various simplification processes for the Hier graph

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <ctype.h>
#include <assert.h>
#include <math.h>
#include <inttypes.h>

#include "auto.h"
#include "hierarchy.h"
#include "hierarchy_symuse.h"
#include "schedule.h"
#include "linelist.h"
#include "techno.h"
#include "map.h"
#include "../hvex/hvex_misc.h"
#include "../netlist/netlist_simp.h"

extern bool asgpropag_en;
extern bool asgpropag_notsimple_en;
extern bool circularuse_en;
extern bool autoinline_en;
extern bool inserttempregs_inbb_en;



//=====================================================================
// Action propagation - Forward
//=====================================================================

// The reference node (original assignation)
typedef struct hier_asgprop_data_t {
	// To avoid passing too many parameters to functions
	implem_t* Implem;

	// Main data
	hier_action_t* action;
	hvex_t*        dest_vex;   // The destination VEX, to have indexes
	char*          dest_name;

	// If the expr is not a simple selection of bits, we can still consider propagating sign extension
	bool expr_is_simple;

	// If the left part of the Expr is a sign extension, or a literal,
	// propagate a sign extension, or the literal, instead of a vector => this will enable more simplifications
	// These Vex must be freed if not NULL
	hvex_t* destvex_signext;
	hvex_t* destvex_leftlit;

	// This point to the Vex to look for overlap. It is NOT allocated there.
	hvex_t* search_destvex;

	// The other registers read in the reference Action.
	// They must not be written to bypass the Action.
	// data = a VEX node that stores max LEFT and RIGHT indexes
	avl_pp_tree_t  read_regs;

	// Options
	bool list_read_occur;

} hier_asgprop_data_t;

static void Hier_AsgProp_DataInit(hier_asgprop_data_t* data) {
	memset(data, 0, sizeof(*data));
	avl_pp_init(&data->read_regs);
}
static void Hier_AsgProp_DataClear(hier_asgprop_data_t* data) {
	if(data->destvex_signext!=NULL) { hvex_free(data->destvex_signext); data->destvex_signext = NULL; }
	if(data->destvex_leftlit!=NULL) { hvex_free(data->destvex_leftlit); data->destvex_leftlit = NULL; }
	data->search_destvex = NULL;
	avl_pp_foreach(&data->read_regs, scan) hvex_free(scan->data);
	avl_pp_reset(&data->read_regs);
	data->list_read_occur = false;
}

// The results
typedef struct hier_asgprop_result_t {

	// The original assignation can be removed if the main symbol is not read,
	//  or if it is written AND bypassed in all occurrences.
	bool dest_read;
	bool dest_written;
	bool dest_written_full;
	// Indicates a symbol read in the ref Action was written
	bool expr_written;

	// Forbids deletion of the original Action
	bool must_keep_act;
	// to stop scanning a Hier level
	bool level_done;
	// indicates an early stop in the level. Not fully processed. Implies must_keep_act.
	bool early_stop;

	// Read occurrences of the main symbol, that can be replaced
	// Field DATA_FROM = ptr to the Action, to later simplify it
	//       DATA_TO   = ptr to the occurrence of the symbol, to replace
	bitype_list* read_occur;

} hier_asgprop_result_t;

static void Hier_AsgProp_ResultInit(hier_asgprop_result_t* result) {
	result->read_occur = NULL;
}
static void Hier_AsgProp_ResultClear(hier_asgprop_result_t* result) {
	result->dest_read     = false;
	result->dest_written  = false;
	result->dest_written_full = false;
	result->expr_written  = false;
	result->must_keep_act = false;
	result->level_done    = false;
	result->early_stop    = false;
	TestDoNull(result->read_occur, freebitype);
}

// Declaration
static void Hier_AsgProp_Level(hier_node_t* node, hier_asgprop_data_t* data, hier_asgprop_result_t* result);

static void Hier_AsgProp_Node(hier_node_t* node, hier_asgprop_data_t* data, hier_asgprop_result_t* result) {
	if(node==NULL) return;  // Paranoia

	// For debug
	//printf(" #%d ", node->TYPE);

	// Process the node

	switch(node->TYPE) {

		case HIERARCHY_STATE : {
			hier_state_t* state_data = &node->NODE.STATE;

			// Handle when the symbol is read
			foreach(state_data->actions, scanAction) {
				hier_action_t* action = scanAction->DATA;

				if(hvex_model_is_asg(action->expr->model)==false) {
					result->must_keep_act = true;
					result->level_done = true;
					result->early_stop = true;
					break;
				}

				// Find all Read occurrences
				chain_list* list_occur = NULL;
				list_occur = hvex_SearchVectorIdx_AddList(list_occur, hvex_asg_get_expr(action->expr), data->search_destvex);
				list_occur = hvex_SearchVectorIdx_AddList(list_occur, hvex_asg_get_addr(action->expr), data->search_destvex);
				list_occur = hvex_SearchVectorIdx_AddList(list_occur, hvex_asg_get_cond(action->expr), data->search_destvex);

				if(list_occur!=NULL) {
					// Check if the current Action has the flag NOPROPAG_TO
					if((action->flags & HIER_ACT_NOPROPAG_TO) != 0) {
						result->must_keep_act = true;
					}
					else {
						// Save the occurrences
						result->dest_read = true;
						if(data->list_read_occur==true) {
							foreach(list_occur, scanOccur) result->read_occur = addbitype(result->read_occur, 0, action, scanOccur->DATA);
						}
					}
					freechain(list_occur);
				}

			}  // Scan all Actions

			// Note : all branch conditions are supposed to be SIGNALS
			// These signals are assigned in regular Actions,
			// so there is no use trying to scan the branch conditions of the next nodes.

			// Check the destination symbol
			foreach(state_data->actions, scanAction) {
				hier_action_t* action = scanAction->DATA;

				if(hvex_model_is_asg(action->expr->model)==false) {
					// In case function calls remain
					assert(action->expr->model==HVEX_FUNCTION);
					// Worst case decision
					result->must_keep_act = true;
					result->level_done = true;
					result->early_stop = true;
					break;
				}

				// Get the Written VEX
				hvex_t* Expr = action->expr->operands;
				char* name = hvex_vec_getname(Expr);

				// Check if this symbol was read in the reference Action
				hvex_t* vex_read = NULL;
				avl_pp_find_data(&data->read_regs, name, (void**)&vex_read);
				if(vex_read!=NULL) {
					if(hvex_check_overlap(Expr, vex_read)==false) {
						// The indexes do not match, do nothing
					}
					else {
						result->expr_written = true;
						result->must_keep_act = true;
						result->level_done = true;
						result->early_stop = true;
					}
				}

				// Note: When only wanting to do propagation of sign extension
				// these checks still apply correctly

				// Check if this symbol is the reference destination symbol
				if(name==data->dest_name) {
					// Check the indexes.
					if(hvex_check_overlap(Expr, data->dest_vex)==false) {
						// Write outside the indexes of the reference Action
						// Nothing happens.
					}
					else if(hvex_check_inclusion(Expr, data->dest_vex)==true) {
						// Complete overwrite. The reference Action can even be deleted.
						result->dest_written = true;
						result->dest_written_full = true;
						result->level_done = true;
					}
					else {
						// Only partially overwrite what is written in the reference Action
						result->dest_written = true;
						result->must_keep_act = true;
						result->level_done = true;
						result->early_stop = true;
					}
				}

			}  // Scan all Actions

			break;
		}  // STATE

		case HIERARCHY_BB : {
			hier_bb_t* node_bb = &node->NODE.BB;
			Hier_AsgProp_Level(node_bb->body, data, result);
			break;
		}

		case HIERARCHY_LOOP : {
			hier_loop_t* node_loop = &node->NODE.LOOP;

			#if 0  // Activate this to track bugs
			result->must_keep_act = true;
			result->level_done = true;
			result->early_stop = true;
			break;
			#endif

			// Get what happens in the branches

			hier_asgprop_result_t result_after;
			Hier_AsgProp_ResultInit(&result_after);
			Hier_AsgProp_ResultClear(&result_after);

			hier_asgprop_result_t result_before;
			Hier_AsgProp_ResultInit(&result_before);
			Hier_AsgProp_ResultClear(&result_before);

			// Scan the bodies

			if(node_loop->body_after!=NULL) {
				Hier_AsgProp_Level(node_loop->body_after, data, &result_after);
			}
			if(node_loop->body_before!=NULL) {
				Hier_AsgProp_Level(node_loop->body_before, data, &result_before);
			}

			if(result_after.must_keep_act==true || result_before.must_keep_act==true) {
				result->must_keep_act = true;
			}

			if(result_after.early_stop==true || result_before.early_stop==true) {
				result->must_keep_act = true;
				result->level_done = true;
				result->early_stop = true;

				Hier_AsgProp_ResultClear(&result_after);
				Hier_AsgProp_ResultClear(&result_before);

				break;
			}

			// Set the flags

			// Situation where the ref symbol is not written
			if(result_after.dest_written==false && result_before.dest_written==false) {
				// Set flag
				if(result_after.dest_read==true || result_before.dest_read==true) result->dest_read = true;
				// Save data
				if(data->list_read_occur==true) {
					result->read_occur = ChainBiType_Concat(result_after.read_occur, result->read_occur);
					result_after.read_occur = NULL;
					result->read_occur = ChainBiType_Concat(result_before.read_occur, result->read_occur);
					result_before.read_occur = NULL;
				}
			}

			// Situation where, in the BodyAfter, the ref symbol is written AND not read
			// What happens in the BodyBefore is of no interest.
			else if(result_after.dest_written_full==true && result_after.dest_read==false) {
				result->dest_written = true;
				result->dest_written_full = true;
				result->level_done = true;
			}

			// All other cases: worst case decision.
			else {
				result->must_keep_act = true;
				result->level_done = true;
				result->early_stop = true;
			}

			Hier_AsgProp_ResultClear(&result_after);
			Hier_AsgProp_ResultClear(&result_before);

			break;
		}  // LOOP

		case HIERARCHY_SWITCH : {
			//hier_switch_t* switch_data = &node->NODE.SWITCH;

			#if 0  // Activate this to track bugs
			result->must_keep_act = true;
			result->level_done = true;
			result->early_stop = true;
			break;
			#endif

			// Note: the list of Cases of a Switch is considered exhaustive.
			// If there is no Default case, AUGH assumes one Case WILL be taken.

			// Initialization of flags for scanning of cases
			bool is_read = false;      // read in at least one case
			bool is_written = false;   // written in at least one case
			bool written_all = true;   // written full in ALL cases
			bitype_list* all_read_occur = NULL;

			hier_asgprop_result_t result_case;
			Hier_AsgProp_ResultInit(&result_case);

			// Scan all Cases
			foreach(node->CHILDREN, scanChild) {
				hier_node_t* childNode = scanChild->DATA;
				hier_case_t* node_case = &childNode->NODE.CASE;

				Hier_AsgProp_ResultClear(&result_case);
				Hier_AsgProp_Level(node_case->body, data, &result_case);

				// Set flags

				// FIXME Here we assume there is no dirty Jump node in the case body

				if(result_case.dest_read==true) is_read = true;
				all_read_occur = ChainBiType_Concat(all_read_occur, result_case.read_occur);
				result_case.read_occur = NULL;

				if(result_case.dest_written==true) {
					is_written = true;
					if(result_case.dest_written_full==false) written_all = false;
				}
				else written_all = false;

				if(result_case.must_keep_act==true) result->must_keep_act = true;
				if(result_case.early_stop==true) { result->level_done = true; result->early_stop = true; }

			}  // Scan all children

			Hier_AsgProp_ResultClear(&result_case);

			if(is_read==true) result->dest_read = true;
			if(data->list_read_occur==true) {
				result->read_occur = ChainBiType_Concat(all_read_occur, result->read_occur);
				all_read_occur = NULL;
			}
			TestDoNull(all_read_occur, freebitype);

			if(is_written==true) {
				result->dest_written = true;
				if(written_all==true) result->dest_written_full = true;
			}

			if(is_written==true) {
				if(written_all==true) {
					result->level_done = true;
				}
				else {
					result->must_keep_act = true;
					result->level_done = true;
					result->early_stop = true;
				}
			}

			break;
		}  // SWITCH

		// Unhandled node type: worst case decision
		default : {
			result->must_keep_act = true;
			result->level_done = true;
			result->early_stop = true;
			break;
		}

	}

}

static void Hier_AsgProp_Level(hier_node_t* node, hier_asgprop_data_t* data, hier_asgprop_result_t* result) {
	for( ; node!=NULL; node=node->NEXT) {
		Hier_AsgProp_Node(node, data, result);
		if(result->level_done==true) break;
	}
}

// Check if a Vex is a simple selection of bits, from registers
static bool checkvex_issimple(implem_t* Implem, hvex_t* vex) {
	if(vex->model==HVEX_LITERAL) return true;
	if(vex->model==HVEX_VECTOR) {
		// Only registers, not ports nor signals, mem...
		char* name = hvex_vec_getname(vex);
		netlist_comp_t* loc_comp = Netlist_Comp_GetChild(Implem->netlist.top, name);
		if(loc_comp==NULL) return false;
		if(loc_comp->model!=NETLIST_COMP_REGISTER) return false;
		return true;
	}
	if(vex->model==HVEX_CONCAT || vex->model==HVEX_RESIZE || vex->model==HVEX_REPEAT) {
		hvex_foreach(vex->operands, vexop) {
			bool b = checkvex_issimple(Implem, vexop);
			if(b==false) return false;
		}
		return true;
	}
	// For any other operation, the Vex is not a simple selection of bits
	return false;
}
// A utility function to find the read symbols
static int list_read_regs(hier_asgprop_data_t* data, hvex_t* Expr) {
	if(Expr->model==HVEX_FUNCTION) return __LINE__;

	if(Expr->model==HVEX_VECTOR) {
		char* name = hvex_vec_getname(Expr);
		// Only accept registers
		netlist_comp_t* comp = Map_Netlist_GetComponent(data->Implem, name);
		if(comp==NULL) return __LINE__;
		if(comp->model!=NETLIST_COMP_REGISTER) return __LINE__;
		// Get/Add the node
		avl_pp_node_t* avl_node = NULL;
		bool b = avl_pp_add(&data->read_regs, name, &avl_node);
		if(b==true) avl_node->data = hvex_dup(Expr);
		else {
			hvex_t* vex = avl_node->data;
			if(Expr->left > vex->left) vex->left = Expr->left;
			if(Expr->right < vex->right) vex->right = Expr->right;
			vex->width = vex->left - vex->right + 1;
		}
		return 0;
	}

	hvex_foreach(Expr->operands, VexOp) {
		int z = list_read_regs(data, VexOp);
		if(z!=0) return z;
	}

	return 0;
}

static int Hier_AsgProp_Action(implem_t* Implem, hier_action_t* action, hier_asgprop_data_t* data, hier_asgprop_result_t* result) {
	data->Implem = Implem;

	// Skip if there is the flag NOPROPAG_FROM
	if((action->flags & HIER_ACT_NOPROPAG_FROM) != 0) return __LINE__;

	// Only handle unconditional register ASG
	if(action->expr->model!=HVEX_ASG) return __LINE__;

	hvex_t* ref_vex_dest  = hvex_asg_get_dest(action->expr);
	char*   ref_name_dest = hvex_vec_getname(ref_vex_dest);

	// Skip protected symbols
	if(Implem_SymFlag_Chk(Implem, ref_name_dest, SYM_PROTECT_NOPROPAG)==true) return __LINE__;

	// Skip Actions with timed dependencies
	if(avl_pi_isempty(&action->deps_timed)==false) return __LINE__;

	// Only handle registers
	netlist_comp_t* comp = Map_Netlist_GetComponent(Implem, ref_name_dest);
	if(comp==NULL) return __LINE__;
	if(comp->model!=NETLIST_COMP_REGISTER) return __LINE__;

	// Check that the EXPR is simple
	hvex_t* ref_vex_expr = hvex_asg_get_expr(action->expr);

	data->expr_is_simple = checkvex_issimple(Implem, ref_vex_expr);
	if(data->expr_is_simple==false) {
		if(asgpropag_notsimple_en==false) return __LINE__;
	}

	hvex_t* destvex_signext = NULL;
	hvex_t* destvex_leftlit = NULL;

	// If the Expr is not simple, check if it is a sign extension or if it has a literal at the left
	if(data->expr_is_simple==false) {
		hvex_t* leftop = ref_vex_expr;
		// Get the leftmost operand, enter inside the CONCAT models if any
		while(leftop->model==HVEX_CONCAT) leftop = leftop->operands;
		if(leftop->right!=0) return __LINE__;
		// Check the leftmost operand
		if(leftop->model==HVEX_LITERAL) {
			assert(leftop->width < ref_vex_dest->width);
			destvex_leftlit = hvex_newvec(ref_name_dest, ref_vex_dest->left, ref_vex_dest->left - leftop->width + 1);
		}
		else if(leftop->model==HVEX_RESIZE) {
			// Only handle signed extension because an unsigned extension is supposed to have already been replaced by a LITERAL
			if(hvex_is_signed(leftop->operands)==false) return __LINE__;
			unsigned extwidth = leftop->width - leftop->operands->width;
			destvex_signext = hvex_newvec(ref_name_dest, ref_vex_dest->left, ref_vex_dest->left - extwidth + 1);
		}
		else return __LINE__;
	}

	// From here the expression either is simple, or it is not but it has a signed extension

	// Reset the data/result structures
	Hier_AsgProp_DataClear(data);
	Hier_AsgProp_ResultClear(result);

	// The data structure to apply constant propagation
	data->action = action;
	data->dest_vex = ref_vex_dest;
	data->dest_name = ref_name_dest;

	data->destvex_signext = destvex_signext;
	data->destvex_leftlit = destvex_leftlit;

	// If the expression is simple, must check that no read symbol is written in the current State
	// Otherwise, we are only propagating sign extension, so the read symbols are not a problem
	if(data->expr_is_simple==true) {
		// List the symbols read in this Action
		int z = list_read_regs(data, ref_vex_expr);
		if(z!=0) return __LINE__;

		// Check that the read symbols are not written in the current State
		bool samestate_accepted = true;
		hier_node_t* nodeState = data->action->node;
		hier_state_t* state_data = &nodeState->NODE.STATE;
		foreach(state_data->actions, scanAction) {
			hier_action_t* other_action = scanAction->DATA;
			if(other_action==data->action) continue;

			// Get the destination VEX
			hvex_t* other_dest_vex  = hvex_asg_get_dest(other_action->expr);
			char*   other_dest_name = hvex_vec_getname(other_dest_vex);

			// Check whether the indexes overlap
			hvex_t* vex_read = NULL;
			avl_pp_find_data(&data->read_regs, other_dest_name, (void**)&vex_read);
			if(vex_read==NULL) continue;
			if(hvex_check_overlap(other_dest_vex, vex_read)==false) continue;

			// Here, the indexes overlap, bypassing is not possible
			samestate_accepted = false;
			break;
		}
		if(samestate_accepted==false) return __LINE__;

		// Set the Action dest as Vector to look for overlap
		data->search_destvex = ref_vex_dest;
	}
	else {
		if(data->destvex_signext!=NULL) data->search_destvex = data->destvex_signext;
		if(data->destvex_leftlit!=NULL) data->search_destvex = data->destvex_leftlit;
	}

	assert(data->search_destvex!=NULL);

	// Set the commands
	data->list_read_occur = true;

	// Process the next nodes.
	// When the end is reached, go in the upper levels and process the next nodes.
	hier_node_t* cur_process_node = action->node;

	// If something follows, apply the processing.
	if(cur_process_node->NEXT!=NULL) {
		Hier_AsgProp_Level(cur_process_node->NEXT, data, result);
	}

	// Process the upper nodes
	do {

		if(result->level_done==true) break;

		// Get the parent node
		hier_node_t* parent_node = Hier_GetParentNode(cur_process_node);
		if(parent_node==NULL) {
			// We are in the main level where the process starts
			result->must_keep_act = true;
			result->level_done = true;
			result->early_stop = true;
			break;
		}

		if(parent_node->TYPE==HIERARCHY_BB) {
			cur_process_node = parent_node->NEXT;
		}
		else {
			result->must_keep_act = true;
			result->level_done = true;
			result->early_stop = true;
			break;
		}

		// Process the node
		Hier_AsgProp_Level(cur_process_node, data, result);

	} while(1);

	if(result->level_done==false) result->must_keep_act = true;

	return 0;
}

// These parameters are for debug use only
static bool regasgprop_nodel = false;
static unsigned regasgprop_max = 0;

// Main call function to launch Action propagation
int Hier_RegAsgProp_Simple_OnePass(implem_t* Implem) {
	unsigned count = 0;

	//printf("DEBUG %s:%u : Launching Action bypassing...\n", __FILE__, __LINE__);

	// List of Actions to simplify at the end
	avl_p_tree_t acts_simp;
	avl_p_init(&acts_simp);

	hier_asgprop_data_t data;
	Hier_AsgProp_DataInit(&data);

	hier_asgprop_result_t result;
	Hier_AsgProp_ResultInit(&result);

	avl_p_foreach(&Implem->H->NODES[HIERARCHY_STATE], scanState) {
		hier_node_t* nodeTrans = scanState->data;
		hier_state_t* node_trans = &nodeTrans->NODE.STATE;

		// Work on a duplicate list of Actions
		// This is because Actions are removed from the graph
		chain_list* list_actions = dupchain(node_trans->actions);

		// Process all Actions of this State
		foreach(list_actions, scanAction) {
			hier_action_t* action = scanAction->DATA;

			// Inside this call, the structures are initialized when necessary
			int z = Hier_AsgProp_Action(Implem, action, &data, &result);
			if(z!=0) continue;

			// This avoids a case of infinite replacement with identical expressions
			if(result.must_keep_act==true) {
				bool b = avl_pp_isthere(&data.read_regs, data.dest_name);
				if(b==true) continue;
			}

			// Replace !
			if(result.read_occur!=NULL) {

				if(actsimp_verbose==true) {
					printf("Info: Propagating from Action: ");
					HierAct_DebugPrint(action);
					#if 0
					printf("  Read symbols :");
					avl_pp_foreach(&data.read_regs, scanReg) {
						printf(" ");
						viewvexexprbound(scanReg->data);
					}
					printf("\n");
					#endif
				}

				// Replace all occurrences
				foreach(result.read_occur, scanOccur) {
					// Save the Action to simplify
					hier_action_t* other_action = scanOccur->DATA_FROM;
					avl_p_add_overwrite(&acts_simp, other_action, other_action);

					hvex_t* Expr = scanOccur->DATA_TO;

					if(data.expr_is_simple==true) {
						// Replace the occurrences by the original expression
						hvex_t* ref_expr = hvex_asg_get_expr(data.action->expr);
						hvex_ReplaceSubExpr_FromPrevExpr(Expr, data.dest_vex, ref_expr);
					}
					else if(data.destvex_signext!=NULL) {
						// Create the replacement expression: it is a RESIZE / REPEAT of one bit
						hvex_t* vex_replace = hvex_newmodel_op1(HVEX_REPEAT, data.destvex_signext->width,
							hvex_newvec(data.dest_name, data.destvex_signext->right - 1, data.destvex_signext->right - 1)
						);
						// Perform replacement
						hvex_ReplaceSubExpr_FromPrevExpr(Expr, data.destvex_signext, vex_replace);
						// Clean
						hvex_free(vex_replace);
					}
					else if(data.destvex_leftlit!=NULL) {
						// Create the replacement expression: select the LITERAL part of the original expression
						hvex_t* vex_replace = hvex_dup(hvex_asg_get_expr(data.action->expr));
						hvex_croprel(vex_replace, 0, data.destvex_leftlit->right - data.dest_vex->right);
						// Perform replacement
						hvex_ReplaceSubExpr_FromPrevExpr(Expr, data.destvex_leftlit, vex_replace);
						// Clean
						hvex_free(vex_replace);
					}
					else abort();

					// Count, for stats and to know something was modified
					count++;
					// Early exit for debug
					if(regasgprop_max>0 && count>=regasgprop_max) break;
				}

			}  // End replace occurrences

			if(data.expr_is_simple==true && result.must_keep_act==false && regasgprop_nodel==false) {
				if(actsimp_verbose==true) {
					printf("Info: Removing propagated Action: ");
					HierAct_DebugPrint(action);
				}
				avl_p_rem_key(&acts_simp, action);
				Hier_Action_FreeFull(action);
				count ++;
			}

			// Early exit for debug
			if(regasgprop_max>0 && count>=regasgprop_max) break;

		}  // Scan the Actions

		// clean
		freechain(list_actions);

		// Early exit for debug
		if(regasgprop_max>0 && count>=regasgprop_max) break;

	}  // Scan the states

	// Simplify the modified Actions
	avl_p_foreach(&acts_simp, scanAction) {
		hier_action_t* action = scanAction->data;
		hvex_simp(action->expr);
	}
	avl_p_reset(&acts_simp);

	Hier_AsgProp_DataClear(&data);
	Hier_AsgProp_ResultClear(&result);

	// Reset the early exit condition
	regasgprop_max = 0;

	return count;
}



//=====================================================================
// Action propagation - Backward
//=====================================================================

// Main call function to launch Action propagation
int Hier_RegAsgProp_SimpleFromLoop_OnePass(implem_t* Implem) {
	unsigned count = 0;

	// Process all States
	avl_p_foreach(&Implem->H->NODES[HIERARCHY_LOOP], scanLoop) {
		hier_node_t* nodeLoop = scanLoop->data;
		hier_loop_t* loop_data = &nodeLoop->NODE.LOOP;
		// Skip infinite loops
		if(loop_data->testinfo->cond==NULL) continue;

		// Check: the body_before is NULL
		if(loop_data->body_before!=NULL) continue;
		// Check: the body_after is only one BB
		if(loop_data->body_after==NULL) continue;
		if(loop_data->body_after->TYPE!=HIERARCHY_BB) continue;
		if(loop_data->body_after->NEXT!=NULL) continue;

		// Check: the BB of body_after has only one State
		hier_node_t* nodeBB = loop_data->body_after;
		hier_bb_t* bb_data = &nodeBB->NODE.BB;
		hier_node_t* loopState = bb_data->body;
		if(loopState==NULL) continue;
		if(loopState->NEXT!=NULL) continue;

		// Check: the node after the loop is a BB
		if(nodeLoop->NEXT==NULL) continue;
		if(nodeLoop->NEXT->TYPE!=HIERARCHY_BB) continue;
		hier_node_t* nodeBB_after = nodeLoop->NEXT;
		hier_node_t* nextState = nodeBB_after->NODE.BB.body;
		if(nextState==NULL) continue;
		hier_state_t* nextstate_data = &nextState->NODE.STATE;

		// Get all symbols read and written in the loop
		avl_pp_tree_t treevex_loop_read;
		avl_pp_init(&treevex_loop_read);

		avl_pp_tree_t treeact_loop_write;
		avl_pp_init(&treeact_loop_write);

		// Process all Actions of the State in the Loop
		hier_state_t* loopstate_data = &loopState->NODE.STATE;
		foreach(loopstate_data->actions, scanAction) {
			hier_action_t* action = scanAction->DATA;
			hvex_GetAllSymbols_treevex(action->expr, &treevex_loop_read, NULL);
			char* name = hvex_asg_get_destname(action->expr);
			assert(name!=NULL);
			avl_pp_add_overwrite(&treeact_loop_write, name, action);
		}

		// Now we focus on the next State

		// Work on a copy of the list of Actions, because some Actions are removed from this State
		chain_list* list_actions = dupchain(nextstate_data->actions);

		// Process all Actions of the next State
		foreach(list_actions, scanNextAction) {
			hier_action_t* next_action = scanNextAction->DATA;

			// Skip if there is the flag NOPROPAG_TO
			if((next_action->flags & HIER_ACT_NOPROPAG_TO) != 0) continue;

			// No wired condition is allowed
			if(hvex_asg_get_cond(next_action->expr)!=NULL) continue;

			hvex_t* vex_dest = hvex_asg_get_dest(next_action->expr);
			char*   name_dest = hvex_vec_getname(vex_dest);

			// Only handle registers
			netlist_comp_t* comp = Map_Netlist_GetComponent(Implem, name_dest);
			if(comp==NULL) continue;
			if(comp->model!=NETLIST_COMP_REGISTER) continue;

			// Skip protected symbols
			if(Implem_SymFlag_ChkAnyProtect(Implem, name_dest)==true) continue;

			// The dest symbol must not be written, nor read, in the Loop
			// FIXME There are situations where this could be allowed
			if(hvex_treevex_get(&treevex_loop_read, name_dest)!=NULL) continue;
			if(hvex_treevex_get(&treeact_loop_write, name_dest)!=NULL) continue;

			// Check symbols read in the present Action
			// For that, gat all symbols read in this State except the present Action

			avl_pp_tree_t treevex_next_read;
			avl_pp_init(&treevex_next_read);

			// Process all Actions in the next State, skip the present Action
			foreach(nextstate_data->actions, scanLocAction) {
				hier_action_t* loc_action = scanLocAction->DATA;
				if(loc_action==next_action) continue;
				hvex_GetAllSymbols_treevex(loc_action->expr, &treevex_next_read, NULL);
			}

			// If the dest symbol is read in the present State, and if indexes overlap, don't move if
			bool is_read_here = false;
			hvex_t* loc_read = hvex_treevex_get(&treevex_next_read, name_dest);
			if(loc_read!=NULL) {
				if(hvex_check_overlap(vex_dest, loc_read)==true) is_read_here = true;
			}

			hvex_treevex_reset(&treevex_next_read);

			if(is_read_here==true) continue;

			// Read symbols: authorize only registers
			// If read in another Action of this next State, don't move
			// If it's the symbol written here, ensure indexes don't overlap
			// Note: if written in the Loop, no problem

			bool scan_aborted = false;
			chain_list* list_vex_replace = NULL;

			// Local function to check the symbols that are read in the present Action
			void check_read(hvex_t* Expr) {
				if(Expr==NULL) return;
				if(Expr->model==HVEX_LITERAL) return;
				// Check variables
				if(Expr->model==HVEX_VECTOR) {
					char* name = hvex_vec_getname(Expr);
					// No protected symbols
					if(Implem_SymFlag_ChkAnyProtect(Implem, name)==true) { scan_aborted=true; return; }
					// If it's the symbol written here, check indexes
					if(name==name_dest) {
						// If completely outside what is written in the original Action, skip
						if(hvex_check_overlap(Expr, vex_dest)==true) scan_aborted = true;
						return;
					}
					// Only registers
					netlist_comp_t* loc_comp = Map_Netlist_GetComponent(Implem, name);
					if(loc_comp==NULL) { scan_aborted = true; return; }
					if(loc_comp->model!=NETLIST_COMP_REGISTER) { scan_aborted = true; return; }
					// Check if written in the Loop
					hier_action_t* loop_action = NULL;
					avl_pp_find_data(&treeact_loop_write, name, (void**)&loop_action);
					if(loop_action!=NULL) {
						hvex_t* loopvex_dest = hvex_asg_get_dest(loop_action->expr);
						if(hvex_check_overlap(Expr, loopvex_dest)==true) {
							// Skip if there is the flag NOPROPAG_FROM
							if((next_action->flags & HIER_ACT_NOPROPAG_FROM) != 0) { scan_aborted = true; return; }
							// Save this occurrence in the list to be replaced later
							list_vex_replace = addchain(list_vex_replace, Expr);
						}
					}
					return;
				}
				// Check operations that are only gathering of bits
				if(Expr->model==HVEX_CONCAT || Expr->model==HVEX_REPEAT || Expr->model==HVEX_RESIZE) {
					// Recursively scan operands
					hvex_foreach(Expr->operands, VexOp) {
						check_read(VexOp);
						if(scan_aborted==true) return;
					}
					return;
				}
				// Other operation models are rejected
				scan_aborted = true;
				return;
			}

			// Launch the search for Read occurrences
			check_read(hvex_asg_get_expr(next_action->expr));
			check_read(hvex_asg_get_addr(next_action->expr));
			check_read(hvex_asg_get_cond(next_action->expr));

			// If nothing from the original Action is read from, nothing to do
			if(scan_aborted==true) {
				freechain(list_vex_replace);
				continue;
			}

			// For now, only move Actions for which there is something to replace
			// Because it seems that it's best to keep the basic blocks as-is...
			if(list_vex_replace==NULL) continue;

			// This Action can be removed from its State and put in the reference State
			if(actsimp_verbose==true) {
				printf("Info: Moving Action backwards inside Loop: ");
				HierAct_Print(next_action);
			}

			Hier_Action_Unlink(next_action);
			Hier_Action_Link(loopState, next_action);

			foreach(list_vex_replace, scanVec) {
				hvex_t* Expr = scanVec->DATA;
				char* name = hvex_vec_getname(Expr);
				// Check if written in the Loop
				hier_action_t* loop_action = NULL;
				avl_pp_find_data(&treeact_loop_write, name, (void**)&loop_action);
				if(loop_action!=NULL) {
					hvex_t* loopvex_dest = hvex_asg_get_dest(loop_action->expr);
					hvex_ReplaceSubExpr_FromPrevExpr(Expr, loopvex_dest, hvex_asg_get_expr(loop_action->expr));
				}
			}
			freechain(list_vex_replace);

			// Increment the number of things done
			count++;

		}  // Scan Actions in the State after the Loop

		// Clean
		freechain(list_actions);

		hvex_treevex_reset(&treevex_loop_read);
		avl_pp_reset(&treeact_loop_write);

	}  // Scan Loops

	//dbgprintf("Actions moved: %u\n", count);

	return count;
}

/* Note about Action propagation to handle special situations

Example of interesting situation:

  do { stdin_u32 = fifo_data; } while(fifo_ack==0);
  myreg1 = stdin_u32;
  do { stdin_u32 = fifo_data; } while(fifo_ack==0);
  myreg2 = stdin_u32;

=> Due to the loops, the Action myreg1 = stdin_u32 is not handled by normal forward propagation
We want to obtain that:

	do { myreg1 = fifo_data; } while(fifo_ack==0);
	do { myreg2 = fifo_data; } while(fifo_ack==0);

Generalization:

  ref state .. reg1 = <expr>
  then ....... <reg1 only read, nothing that involves reg2>
  then ....... reg2 = <something that only involves reg1>

=> The most interesting situation
In the ref State, add an Action: reg2 = <expr>
(with the proper indexes of course)
And remove the last Action: reg2 = ...

*/

// Action propagation, backwards into loop - Version with SymUse
int Hier_RegAsgProp_SimpleFromLoop_SymUse_OnePass(implem_t* Implem) {
	unsigned count = 0;

	Hier_SymUse_FillHier(Implem);

	// Process all States
	avl_p_foreach(&Implem->H->NODES[HIERARCHY_LOOP], scanLoop) {
		hier_node_t* nodeLoop = scanLoop->data;
		hier_loop_t* loop_data = &nodeLoop->NODE.LOOP;
		// Skip infinite loops
		if(loop_data->testinfo->cond==NULL) continue;

		// Check: the body_before is NULL
		if(loop_data->body_before!=NULL) continue;
		// Check: the body_after is only one BB
		if(loop_data->body_after==NULL) continue;
		if(loop_data->body_after->TYPE!=HIERARCHY_BB) continue;
		if(loop_data->body_after->NEXT!=NULL) continue;

		// Check: the BB of body_after has only one State
		hier_node_t* nodeBB = loop_data->body_after;
		hier_bb_t* bb_data = &nodeBB->NODE.BB;
		if(bb_data->body==NULL) continue;
		if(bb_data->body->NEXT!=NULL) continue;

		// Check: the node after the loop is a BB
		if(nodeLoop->NEXT==NULL) continue;
		if(nodeLoop->NEXT->TYPE!=HIERARCHY_BB) continue;
		hier_node_t* nodeBB_after = nodeLoop->NEXT;

		// Now we focus on the State in the BB in body_after
		hier_node_t* nodeState = bb_data->body;
		hier_state_t* state_data = &nodeState->NODE.STATE;

		// Work on a copy of the list of Actions, because some Actions are added to this State
		chain_list* list_actions = dupchain(state_data->actions);

		// Process all Actions of this State
		foreach(list_actions, scanAction) {
			hier_action_t* action = scanAction->DATA;

			// Skip if there is the flag NOPROPAG_FROM
			if((action->flags & HIER_ACT_NOPROPAG_FROM) != 0) continue;

			// No wired condition is allowed
			if(hvex_asg_get_cond(action->expr)!=NULL) continue;

			hvex_t* vex_dest = hvex_asg_get_dest(action->expr);
			char*   name_dest = hvex_vec_getname(vex_dest);

			// Only handle registers
			netlist_comp_t* comp = Map_Netlist_GetComponent(Implem, name_dest);
			if(comp==NULL) continue;
			if(comp->model!=NETLIST_COMP_REGISTER) continue;

			// Skip protected symbols
			if(Implem_SymFlag_ChkAnyProtect(Implem, name_dest)==true) continue;

			// Here the scan begins
			// scan the BB after the Loop

			// Flag the other symbols written at this State
			avl_pp_tree_t symuse_scan;
			avl_pp_init(&symuse_scan);
			foreach(state_data->actions, scanAction_here) {
				hier_action_t* action_here = scanAction_here->DATA;
				if(action_here==action) continue;
				hvex_t* Expr_here = hvex_asg_get_dest(action_here->expr);
				if(Expr_here==NULL) continue;
				Hier_SymUse_Vex_SetFlag_W(Implem, &symuse_scan, Expr_here);
			}

			bool dest_is_written = false;

			hier_bb_t* bb_after_data = &nodeBB_after->NODE.BB;
			foreach(bb_after_data->body, nodeState_after) {
				hier_state_t* state_after_data = &nodeState_after->NODE.STATE;

				// List the Read symbols
				// FIXME Actually, the dest symbol of the Action we want to move
				//   can be read in the same Action but this is not handled yet
				foreach(state_after_data->actions, scanAction_after) {
					hier_action_t* action_after = scanAction_after->DATA;
					// Get all occurrences of VECTOR models
					chain_list* list_read = NULL;
					list_read = hvex_SearchVector_AddList(list_read, hvex_asg_get_expr(action_after->expr), NULL);
					list_read = hvex_SearchVector_AddList(list_read, hvex_asg_get_addr(action_after->expr), NULL);
					list_read = hvex_SearchVector_AddList(list_read, hvex_asg_get_cond(action_after->expr), NULL);
					// Add Read flags in the State symuse structure
					foreach(list_read, scan) {
						hvex_t* Expr = scan->DATA;
						Hier_SymUse_Vex_SetFlag_R(Implem, &symuse_scan, Expr);
					}
					freechain(list_read);
				}

				// Work on a copy of the list of Actions
				chain_list* list_actions_after = dupchain(state_after_data->actions);

				// Check the Written symbols
				foreach(list_actions_after, scanAction_after) {
					hier_action_t* action_after = scanAction_after->DATA;

					// Get the Written VEX
					hvex_t* Expr = hvex_asg_get_dest(action_after->expr);
					if(Expr==NULL) continue;
					char* name = hvex_vec_getname(Expr);
					if(name==name_dest) {
						// Check the indexes
						// If completely outside what is written in the original Action, it's fine
						if(Expr->left < vex_dest->right || Expr->right > vex_dest->left) {}
						else { dest_is_written = true; continue; }
					}

					// Skip if there is the flag NOPROPAG_TO
					if((action_after->flags & HIER_ACT_NOPROPAG_TO) != 0) continue;

					// Wired conditions are forbidden
					if(hvex_asg_get_cond(action_after->expr)!=NULL) continue;

					// Only registers
					netlist_comp_t* comp = Map_Netlist_GetComponent(Implem, name);
					if(comp==NULL) continue;
					if(comp->model!=NETLIST_COMP_REGISTER) continue;
					// Skip protected symbols
					if(Implem_SymFlag_ChkAnyProtect(Implem, name)==true) continue;

					bb_symuse_t* symuse = Hier_SymUse_GetAdd(&symuse_scan, name, ((netlist_register_t*)comp->data)->width);
					// The dest symbol must NOT have been read nor written since the ref Action
					// Add the W flags on the fly
					bool was_accessed = false;
					for(unsigned i=Expr->right; i<=Expr->left; i++) {
						if(symuse->bits[i]!=0) was_accessed = true;
						symuse->bits[i] |= BB_SYMUSE_IS_W;
					}
					if(was_accessed==true) continue;

					bool is_read = false;
					bool scan_aborted = false;

					// Local function to check
					// - if the ref symbol is read
					// - if another symbol is read
					void check_read(hvex_t* Expr) {
						if(Expr==NULL) return;
						if(Expr->model==HVEX_LITERAL) return;
						// Check variables
						if(Expr->model==HVEX_VECTOR) {
							char* name = hvex_vec_getname(Expr);
							// If this is not yet in symuse_scan, it is not handled
							bb_symuse_t* symuse_read = Hier_SymUse_Get(&symuse_scan, name);
							if(symuse_read==NULL) { scan_aborted = true; return; }
							// No protected symbols
							if(Implem_SymFlag_ChkAnyProtect(Implem, name)==true) { scan_aborted=true; return; }
							// Check that this symbol was never written
							for(unsigned i=Expr->right; i<=Expr->left; i++) {
								if( (symuse_read->bits[i] & BB_SYMUSE_IS_W) != 0 ) { scan_aborted=true; return; }
							}
							// If it's the ref dest symbol, check indexes
							if(name==name_dest) {
								// If completely outside what is written in the original Action, skip
								if(Expr->left < vex_dest->right || Expr->right > vex_dest->left) {}
								else is_read = true;
							}
							return;
						}
						// Check operations that are only gathering of bits
						if(Expr->model==HVEX_CONCAT || Expr->model==HVEX_REPEAT || Expr->model==HVEX_RESIZE) {
							// Recursively scan operands
							hvex_foreach(Expr->operands, VexOp) {
								check_read(VexOp);
								if(scan_aborted==true) return;
							}
							return;
						}
						// Other operation models are rejected
						scan_aborted = true;
						return;
					}

					// Launch the search for Read occurrences
					check_read(hvex_asg_get_expr(action_after->expr));
					check_read(hvex_asg_get_addr(action_after->expr));
					check_read(hvex_asg_get_cond(action_after->expr));

					// If nothing from the original Action is read from, nothing to do
					if(scan_aborted==true || is_read==false) continue;

					// This Action can be removed from its State and put in the reference State
					if(actsimp_verbose==true) {
						printf("Info: Moving Action backwards inside Loop: ");
						HierAct_Print(action_after);
					}

					Hier_Action_Unlink(action_after);
					Hier_Action_Link(nodeState, action_after);

					// Get all occurrences of the original symbol and replace
					chain_list* list_occur = hvex_SearchSym_AddList(NULL, action_after->expr, name_dest);
					assert(list_occur!=NULL);
					// Replace the symbol
					foreach(list_occur, scanOccur) {
						hvex_t* Expr = scanOccur->DATA;
						hvex_ReplaceSubExpr_FromPrevExpr(Expr, vex_dest, hvex_asg_get_expr(action->expr));
					}
					freechain(list_occur);

					// Increment the number of things done
					count++;

					// Update the SymUse flags

					// Update the flags R and W in the affected nodes
					Hier_SymUse_State_RedoRW(Implem, nodeState);
					Hier_SymUse_State_RedoRW(Implem, nodeState_after);
					// Update the flags LIVE, LA in the affected nodes
					Hier_SymUse_State_UpdLive(Implem, nodeState);
					Hier_SymUse_State_UpdLive(Implem, nodeState_after);

				}  // Scan all Actions
				freechain(list_actions_after);

				if(dest_is_written==true) break;

			}  // Scan all States

			avl_pp_foreach(&symuse_scan, scan) Hier_SymUse_Free(scan->data);
			avl_pp_reset(&symuse_scan);

			if(dest_is_written==true) continue;

		}  // Scan Actions in the reference State
		freechain(list_actions);

	}  // Scan Loops

	//dbgprintf("Actions moved: %u\n", count);

	return count;
}



//=====================================================================
// Action propagation - Workaround for temp var in array rotation
//=====================================================================

/* Explanation
 * We want to eliminate the variable tmp (a register) in this kind of code:
 *
 * State 1: tmp  = reg0;
 *          reg0 = reg1;
 *          reg1 = reg2;
 * State 2: reg2 = tmp;
 *
 * The normal ASG propagation process works only with one Action, and in forward direction.
 * Furthermore, there may be operations involving shared operators in the Actions of State 1.
 * The only possible transformation is to move the Action of State 2 backwards in State 1:
 *
 * State 1: tmp  = reg0;
 *          reg0 = reg1;
 *          reg1 = reg2;
 *          reg2 = reg0;  // This Action has moved
 *
 * The original assignation of the temporary variable is kept just in case it's read somewhere else.
 *
 * Forbidden situations (we want to move Actions 21 and 22 to State 1):
 *
 * State 1: act 11: tmp  = A + B;
 *          act 12: reg0 = stuff;     // reg0 MUST be COMPLETELY overwritten after
 * State 2: act 21: reg0 = tmp;       // tmp MUST be read with EXACTLY the same range as in previous State
 *          act 22: reg1 = tmp;       // tmp MUST be read with EXACTLY the same range as in previous State
 *          act 23: reg3 = D + tmp;   // No problem because the previous ASG of tmp is kept
 *          act 24: reg4 = E + reg0;  // FORBIDDEN because dependcy conflict with Action 21 about reg0: WaR
 */

int Hier_RegAsgProp_ArrayRotStyle_OnePass(implem_t* Implem) {
	unsigned count = 0;

	// Process all States
	avl_p_foreach(&Implem->H->NODES[HIERARCHY_STATE], scanState) {
		hier_node_t* nodeState = scanState->data;
		hier_node_t* nextState = nodeState->NEXT;
		// Only consider States that have another State right after
		if(nextState==NULL) continue;
		if(nextState->TYPE!=HIERARCHY_STATE) continue;  // Paranoia

		hier_state_t* state_data = &nodeState->NODE.STATE;
		hier_state_t* nextstate_data = &nextState->NODE.STATE;

		// All Actions to move to the current State
		chain_list* list_new_actions = NULL;

		// Scan Actions
		foreach(state_data->actions, scanRefAction) {
			hier_action_t* ref_action = scanRefAction->DATA;
			// Be paranoid: No Action flags
			if(ref_action->flags != 0) continue;

			// Only handle unconditional register ASG
			if(ref_action->expr->model!=HVEX_ASG) continue;
			// Skip Actions with timed dependencies
			if(avl_pi_isempty(&ref_action->deps_timed)==false) continue;

			hvex_t* ref_vex_dest  = hvex_asg_get_dest(ref_action->expr);
			char*   ref_name_dest = hvex_vec_getname(ref_vex_dest);

			// Skip protected symbols
			if(Implem_SymFlag_Chk(Implem, ref_name_dest, SYM_PROTECT_NOPROPAG)==true) continue;
			// Only consider destination Registers
			netlist_comp_t* comp = Map_Netlist_GetComponent(Implem, ref_name_dest);
			if(comp==NULL) continue;
			if(comp->model!=NETLIST_COMP_REGISTER) continue;

			// This will contain the list of Actions candidates to be moved to the previous State
			chain_list* list_cand_actions = NULL;

			// Scan Actions of the next State, get those that have Expr = ref dest
			foreach(nextstate_data->actions, scanNextAction) {
				hier_action_t* next_action = scanNextAction->DATA;
				// Be paranoid: No Action flags
				if(next_action->flags != 0) continue;

				// Only handle unconditional register ASG
				if(next_action->expr->model!=HVEX_ASG) continue;
				// Skip Actions with timed dependencies
				if(avl_pi_isempty(&next_action->deps_timed)==false) continue;

				hvex_t* next_vex_dest  = hvex_asg_get_dest(next_action->expr);
				char*   next_name_dest = hvex_vec_getname(next_vex_dest);

				// The destination must be a different symbol, but with same width
				if(next_name_dest == ref_name_dest) continue;
				if(next_vex_dest->width != ref_vex_dest->width) continue;

				// Skip protected symbols
				if(Implem_SymFlag_Chk(Implem, next_name_dest, SYM_PROTECT_NOPROPAG)==true) continue;
				// Only consider destination Registers
				netlist_comp_t* next_comp = Map_Netlist_GetComponent(Implem, next_name_dest);
				if(next_comp==NULL) continue;
				if(next_comp->model!=NETLIST_COMP_REGISTER) continue;

				// The Expression of this ASG must be identical to the ref dest Vex (except the sign)
				hvex_t* next_vex_expr = hvex_asg_get_expr(next_action->expr);
				if(next_vex_expr->model!=HVEX_VECTOR) continue;
				char* next_name_expr = hvex_vec_getname(next_vex_expr);
				if(next_name_expr!=ref_name_dest) continue;
				if(hvex_check_sameidx(next_vex_expr, ref_vex_dest)==false) continue;

				// The symbol written in this Action must not be written in the previous State
				bool is_written_before = false;
				foreach(state_data->actions, scanPrevAction) {
					hier_action_t* prev_action = scanPrevAction->DATA;
					if(hvex_model_is_asg(prev_action->expr->model)==false) { is_written_before = true; break; }
					hvex_t* prev_vex_dest  = hvex_asg_get_dest(prev_action->expr);
					char*   prev_name_dest = hvex_vec_getname(prev_vex_dest);
					if(prev_name_dest != next_name_dest) continue;
					if(hvex_check_overlap(prev_vex_dest, next_vex_dest)==true) { is_written_before = true; break; }
				}
				if(is_written_before==true) continue;

				// Here we have found an appropriate Action in the other State.
				// For now, just store it in the list of candidates
				list_cand_actions = addchain(list_cand_actions, next_action);

			}  // Scan Actions of the next State

			if(list_cand_actions==NULL) continue;

			// This is the list of Actions that are NOT moved from nextState
			chain_list* list_nocand_actions = NULL;
			// Fill that list
			foreach(nextstate_data->actions, scanNextAction) {
				hier_action_t* next_action = scanNextAction->DATA;
				if(ChainList_IsDataHere(list_cand_actions, next_action)==true) continue;
				list_nocand_actions = addchain(list_nocand_actions, next_action);
			}

			chain_list* list_cand_pruned = NULL;

			// Scan candidate Actions, search occurrences of the dest Vex in the non-candidate Actions
			// Search occurrences of the dest symbols of the candidate Actions
			foreach(list_cand_actions, scanCandAction) {
				hier_action_t* cand_action = scanCandAction->DATA;
				hvex_t* cand_vex_dest = hvex_asg_get_dest(cand_action->expr);

				// This list will contain hvex_t elements with occurrences that overlap with the ref dest Vex
				// (only from Actions that are NOT candidates to be moved to the previous State)
				chain_list* vex_other_reads = NULL;

				// Scan non-candidate Actions
				foreach(list_nocand_actions, scanNoCandAction) {
					hier_action_t* nocand_action = scanNoCandAction->DATA;
					vex_other_reads = hvex_SearchVectorIdx_AddList(vex_other_reads, hvex_asg_get_addr(nocand_action->expr), cand_vex_dest);
					vex_other_reads = hvex_SearchVectorIdx_AddList(vex_other_reads, hvex_asg_get_expr(nocand_action->expr), cand_vex_dest);
					vex_other_reads = hvex_SearchVectorIdx_AddList(vex_other_reads, hvex_asg_get_cond(nocand_action->expr), cand_vex_dest);
					if(vex_other_reads!=NULL) break;
				}

				// Note: if the candidate Action is invalid, it's not needed to add it to the non-candidate list
				// Because the read Vector of the candidate is already known to be exclusively the ref dest vector
				if(vex_other_reads!=NULL) {
					freechain(vex_other_reads);
					continue;
				}

				// Here the candidate Action is still valid, keep it
				list_cand_pruned = addchain(list_cand_pruned, cand_action);
			}  // Scan all candidate Actions

			// Clean temp lists, keep the pruned list of candidates
			freechain(list_cand_actions);
			freechain(list_nocand_actions);
			list_cand_actions = list_cand_pruned;

			// Process all pruned candidate Actions
			foreach(list_cand_actions, scanCandAction) {
				hier_action_t* cand_action = scanCandAction->DATA;
				// Replace the Expr by a duplicate of the Expr of the original Action
				hvex_replace_free(
					hvex_asg_get_expr(cand_action->expr),
					hvex_dup(hvex_asg_get_expr(ref_action->expr))
				);
				// Unlink the Action from the current nextState, and save it in the list
				Hier_Action_Unlink(cand_action);
				list_new_actions = addchain(list_new_actions, cand_action);
			}
			freechain(list_cand_actions);

		}  // Scan all Actions

		#if 0  // For debug
		if(list_new_actions!=NULL) {
			dbgprintf("Propag Actions:\n");
			foreach(list_new_actions, scanNewActions) {
				hier_action_t* new_action = scanNewActions->DATA;
				printf("  "); HierAct_DebugPrint(new_action);
			}
		}
		#endif

		// Link next Actions to the current State
		foreach(list_new_actions, scanNewActions) {
			hier_action_t* new_action = scanNewActions->DATA;
			Hier_Action_Link(nodeState, new_action);
			// Increment the number of things done
			count ++;
		}
		freechain(list_new_actions);

	}  // Scan all States

	return count;
}



//=====================================================================
// Action removal in case of simple State-to-State write after write
//=====================================================================

/* Explanation
 * We want to eliminate the variable tmp (a register) in this kind of code:
 *
 * State 1 : tmp = stuff1;
 * State 2 : tmp = stuff2;
 *
 * The assignation in State 1 can be safely removed if the written range in State 2 includes State 1,
 * and if the dest is not read in State 2
*/

int Hier_ActionRemove_SimpleWaW_OnePass(implem_t* Implem) {
	unsigned count = 0;

	// This tree contains the range of all symbols that are read in next State
	avl_pp_tree_t next_treevex_read;
	avl_pp_init(&next_treevex_read);

	// Process all States
	avl_p_foreach(&Implem->H->NODES[HIERARCHY_STATE], scanState) {
		hier_node_t* nodeState = scanState->data;
		hier_node_t* nextState = nodeState->NEXT;
		// Only consider States that have another State right after
		if(nextState==NULL) continue;
		if(nextState->TYPE!=HIERARCHY_STATE) continue;  // Paranoia

		hier_state_t* state_data = &nodeState->NODE.STATE;
		hier_state_t* nextstate_data = &nextState->NODE.STATE;

		chain_list* del_actions = NULL;

		// Get all read symbols on the next State
		hvex_treevex_reset(&next_treevex_read);
		foreach(nextstate_data->actions, scanNextAction) {
			hier_action_t* next_action = scanNextAction->DATA;
			hvex_GetAllSymbols_treevex(next_action->expr, &next_treevex_read, NULL);
		}

		// Scan Actions
		foreach(state_data->actions, scanRefAction) {
			hier_action_t* ref_action = scanRefAction->DATA;

			// Note: ref Actions can have a condition

			// Skip Actions with timed dependencies
			if(avl_pi_isempty(&ref_action->deps_timed)==false) continue;

			hvex_t* ref_vex_dest  = hvex_asg_get_dest(ref_action->expr);
			char*   ref_name_dest = hvex_vec_getname(ref_vex_dest);

			// Skip protected symbols
			if(Implem_SymFlag_Chk(Implem, ref_name_dest, SYM_PROTECT_NOPROPAG)==true) continue;
			// Only consider destination Registers
			netlist_comp_t* comp = Map_Netlist_GetComponent(Implem, ref_name_dest);
			if(comp==NULL) continue;
			if(comp->model!=NETLIST_COMP_REGISTER) continue;

			// The symbol must not be read in the next State
			hvex_t* vex_read = NULL;
			avl_pp_find_data(&next_treevex_read, ref_name_dest, (void**)&vex_read);
			if(vex_read!=NULL) {
				if(hvex_check_overlap(ref_vex_dest, vex_read)==true) continue;
			}

			// Scan Actions of the next State in search for a similar destination
			foreach(nextstate_data->actions, scanNextAction) {
				hier_action_t* next_action = scanNextAction->DATA;

				// Only handle unconditional register ASG
				if(next_action->expr->model!=HVEX_ASG) continue;
				// Skip Actions with timed dependencies
				if(avl_pi_isempty(&next_action->deps_timed)==false) continue;

				hvex_t* next_vex_dest  = hvex_asg_get_dest(next_action->expr);
				char*   next_name_dest = hvex_vec_getname(next_vex_dest);

				// The destination must be the same symbol, and include the previous Write range
				if(next_name_dest != ref_name_dest) continue;
				if(hvex_check_inclusion(next_vex_dest, ref_vex_dest)==false) continue;

				// Here a case of WaW is found.
				// Save the ref Action in the list so it is later removed.
				del_actions = addchain(del_actions, ref_action);

				// Note: Exit the loop now because an Action was removed
				break;

			}  // Scan Actions of the next State

		}  // Scan all Actions

		#if 0  // For debug
		if(del_actions!=NULL) {
			dbgprintf("Remove Actions:\n");
			foreach(del_actions, scanDelActions) {
				hier_action_t* del_action = scanDelActions->DATA;
				printf("  "); HierAct_DebugPrint(del_action);
			}
		}
		#endif

		// Link next Actions to the current State
		foreach(del_actions, scanDelActions) {
			hier_action_t* del_action = scanDelActions->DATA;
			Hier_Action_FreeFull(del_action);
			// Increment the number of things done
			count ++;
		}
		freechain(del_actions);

	}  // Scan all States

	// Clean
	hvex_treevex_reset(&next_treevex_read);

	return count;
}



//=====================================================================
// Simplification functions for Actions
//=====================================================================

// Remove unused symbols, crop constant bits

// Utility function: Remove a component that is not read (and/or not written to)
// Important: Existing hard-connections must have already been checked
static void Hier_RemoveUnusedSym_OneComp_DoRem(implem_t* Implem, netlist_comp_t* comp, hvex_t* vex_read, hvex_t* vex_written) {
	if(actsimp_verbose==true) {
		printf("INFO : Comp '%s' model '%s' is never read. Deleting.\n", comp->name, comp->model->name);
		if(vex_written!=NULL) printf("  Note: Also deleting all Actions that write to it.\n");
	}

	// Remove all Actions that write to the component
	if(vex_written!=NULL) {
		chain_list* list = Hier_GetAct_SymIsDest(Implem->H, comp->name);
		foreach(list, scanAct) Hier_Action_FreeFull(scanAct->DATA);
		freechain(list);
	}

	// Remove the component
	Netlist_Comp_Free(comp);
}

// Utility function: Remove a component that is only read
// Important: Existing hard-connections must have already been checked
static void Hier_ReplaceReadSym_OneComp_DoRem(implem_t* Implem, netlist_comp_t* comp, char* lit_replace) {
	if(actsimp_verbose==true) {
		printf("INFO : Comp '%s' model '%s' is only read. Replacing.\n", comp->name, comp->model->name);
	}

	// Create the replacement Vex
	hvex_t* vex_lit = hvex_newlit(lit_replace);

	// Replace occurrences
	chain_list* list_uses = Hier_GetSymbolOccur(Implem->H, comp->name);
	foreach(list_uses, scanOccur) {
		hvex_t* vex = scanOccur->DATA;
		if(hvex_is_father_hvex(vex)==true && hvex_asg_get_dest(vex->father)==vex) abort();
		assert(vex->left <= vex_lit->left);
		hvex_replace_resize_free(vex, hvex_dup(vex_lit));
	}
	freechain(list_uses);

	// Clean
	hvex_free(vex_lit);

	// Remove the component
	Netlist_Comp_Free(comp);
}

// Print the usage of this symbol
static void Hier_RemoveUnusedSym_OneComp_PrintUsage(implem_t* Implem, netlist_comp_t* comp) {
	printf("Info: Usage of symbol '%s':\n", comp->name);
	unsigned node_count = 0;
	avl_p_foreach(&Implem->H->NODES_ALL, scan) {
		hier_node_t* node = scan->data;
		avl_p_tree_t node_read;
		avl_p_init(&node_read);
		Hier_Node_GetSymbols_tree(node, &node_read, NULL);
		if(avl_p_isthere(&node_read, comp->name)==true) {
			node_count++;
			printf("  In node type %s, lines ", Hier_NodeType2str(node->TYPE));
			LineList_Print_be(node->source_lines, NULL, "\n");
		}
		avl_p_reset(&node_read);
	}
	if(node_count>3) printf("  %u nodes found.\n", node_count);
}

// Only for debug. The content can be completely disabled with the preprocessor.
static inline void Hier_RemoveUnusedSym_PrintRanges(
	implem_t* Implem, netlist_comp_t* comp, unsigned comp_width, hvex_t* vex_read, hvex_t* vex_written
) {
	#if 0
	dbgprintf("Comp '%s' model '%s' with data width %u is ", comp->name, comp->model->name, comp_width);
	if(vex_read!=NULL) printf("read in range [%u-%u], ", vex_read->left, vex_read->right);
	else printf("not read, ");
	if(vex_written!=NULL) printf("written in range [%u-%u]\n", vex_written->left, vex_written->right);
	else printf("not written\n");
	#endif
}

// Modify the expressions and Actions in Hierarchy. For Sig and Reg, but also works on Mem.
static unsigned Hier_RemoveUnusedSym_ModifyHier(implem_t* Implem, netlist_comp_t* comp, hvex_t* vex_keep) {
	unsigned count = 0;

	// First, only handle the read occurrences
	// Because when removing/cropping Write occurrences, some VEX nodes may be removed
	// Only then, Writes can be processed

	if(vex_keep->right > 0) {
		chain_list* list_uses = Hier_GetSymbolOccur(Implem->H, comp->name);
		foreach(list_uses, scanOccur) {
			hvex_t* vex = scanOccur->DATA;
			if(hvex_is_father_hvex(vex)==true && hvex_asg_get_dest(vex->father)==vex) continue;
			assert(vex->right >= vex_keep->right);
			vex->right -= vex_keep->right;
			vex->left -= vex_keep->right;
			count ++;
		}
		freechain(list_uses);
	}

	// Process writes

	chain_list* list = Hier_GetAct_SymIsDest(Implem->H, comp->name);
	foreach(list, scanAct) {
		hier_action_t* action = scanAct->DATA;
		hvex_t* vex_dest = hvex_asg_get_dest(action->expr);
		assert(vex_dest!=NULL);
		// Remove the Action if the written range is out of the range to keep
		if(hvex_check_overlap(vex_dest, vex_keep)==false) {
			//dbgprintf("Removing Action : "); HierAct_DebugPrint(action);
			Hier_Action_FreeFull(action);
			count++;
			continue;
		}
		// Crop the expression
		hvex_t* vex_expr = hvex_asg_get_expr(action->expr);
		assert(vex_expr!=NULL);
		// Compute crop width
		unsigned crop_left = 0;
		unsigned crop_right = 0;
		if(vex_dest->left > vex_keep->left)   crop_left = vex_dest->left - vex_keep->left;
		if(vex_dest->right < vex_keep->right) crop_right = vex_keep->right - vex_dest->right;
		if(crop_left==0 && crop_right==0 && vex_keep->right==0) continue;
		// Crop the expression
		hvex_croprel(vex_expr, crop_left, crop_right);
		// Crop and shift the destination
		vex_dest->left  -= crop_left;
		vex_dest->right += crop_right;
		vex_dest->width = vex_dest->left - vex_dest->right + 1;
		vex_dest->left  -= vex_keep->right;
		vex_dest->right -= vex_keep->right;
		count++;
	}
	freechain(list);

	return count;
}

static int Hier_RemoveUnusedSym_Sig(implem_t* Implem, netlist_comp_t* comp, hvex_t* vex_read, hvex_t* vex_written) {
	unsigned count = 0;
	netlist_signal_t* sig_data = comp->data;

	// Handle when the symbol is not read (and/or not written to)
	// FIXMEEEEE Handle when the component is hard-connected to something that reads it
	//   Note: maybe currently such components always have the flag NODEL?
	if(vex_read==NULL) {
		Hier_RemoveUnusedSym_OneComp_DoRem(Implem, comp, vex_read, vex_written);
		return 1;
	}

	// Handle when the symbol is read but not written to
	// FIXMEEEEE Handle when the component is hard-connected to something that writes to it
	//   Note: maybe currently such components always have the flag NODEL?
	if(vex_written==NULL) {
		printf("WARNING: Signal '%s' is read but not written.\n", comp->name);
		Hier_RemoveUnusedSym_OneComp_PrintUsage(Implem, comp);
		return -1;
	}

	// Here the component is read from and kept.
	// Detect unused index ranges and crop the component.
	unsigned comp_width = comp_width = sig_data->width;
	// Debug: print some details
	Hier_RemoveUnusedSym_PrintRanges(Implem, comp, comp_width, vex_read, vex_written);

	// If the component is not read full width, crop it
	if(vex_read->left+1 < comp_width || vex_read->right > 0) {
		if(actsimp_verbose==true) {
			printf("Info: Signal '%s' with data width %u is only read in range [%u-%u]. Cropping.\n",
				comp->name, comp_width, vex_read->left, vex_read->right
			);
		}
		// Crop the component
		Netlist_Comp_Sig_Crop(comp, comp_width-1 - vex_read->left, vex_read->right);
		// Modify the Actions in Hier
		count += Hier_RemoveUnusedSym_ModifyHier(Implem, comp, vex_read);
		return count + 1;
	}

	// FIXME Search for constant bits in expr of ASG operations where the sym is dest

	return count;
}

static int Hier_RemoveUnusedSym_Reg(implem_t* Implem, netlist_comp_t* comp, hvex_t* vex_read, hvex_t* vex_written) {
	unsigned count = 0;
	netlist_register_t* reg_data = comp->data;

	// Handle when the symbol is not read (and/or not written to)
	// FIXMEEEEE Handle when the component is hard-connected to something that reads it
	//   Note: maybe currently such components always have the flag NODEL?
	if(vex_read==NULL) {
		Hier_RemoveUnusedSym_OneComp_DoRem(Implem, comp, vex_read, vex_written);
		return 1;
	}

	// Handle when the symbol is read but not written to
	// FIXMEEEEE Handle when the component is hard-connected to something that writes to it
	//   Note: maybe currently such components always have the flag NODEL?
	if(vex_written==NULL) {
		if(reg_data->init_value!=NULL) {
			Hier_ReplaceReadSym_OneComp_DoRem(Implem, comp, reg_data->init_value);
			return count + 1;
		}
		printf("WARNING: Register '%s' is read but not written and has no initial value.\n", comp->name);
		Hier_RemoveUnusedSym_OneComp_PrintUsage(Implem, comp);
		return -1;
	}

	// Here the component is read and written. Keep it.
	// Detect unused index ranges and crop the component.
	unsigned comp_width = reg_data->width;
	// Debug: print some details
	Hier_RemoveUnusedSym_PrintRanges(Implem, comp, comp_width, vex_read, vex_written);

	// If the component is not read full width, crop it
	if(vex_read->left+1 < comp_width || vex_read->right > 0) {
		if(actsimp_verbose==true) {
			printf("Info: Register '%s' with data width %u is only read in range [%u-%u]. Cropping.\n",
				comp->name, comp_width, vex_read->left, vex_read->right
			);
		}
		// Crop the component
		Netlist_Comp_Reg_Crop(comp, comp_width-1 - vex_read->left, vex_read->right);
		// Modify the Actions in Hier
		count += Hier_RemoveUnusedSym_ModifyHier(Implem, comp, vex_read);
		return count + 1;
	}

	// FIXME: Handle the case where the REG is not written and has init value => detect sign extension
	//   Or, ensure that kind of transformation is already handled in another simplification function

	// FIXME Search for constant bits in expr of ASG operations where the sym is dest
	// FIXME Also detect constant bits in init values for all component models

	return count;
}

static int Hier_RemoveUnusedSym_Mem(implem_t* Implem, netlist_comp_t* comp, hvex_t* vex_read, hvex_t* vex_written) {
	unsigned count = 0;
	netlist_memory_t* mem_data = comp->data;

	// Handle when the symbol is not read (and/or not written to)
	// FIXMEEEEE Handle when the component is hard-connected to something that reads it
	//   Note: maybe currently such components always have the flag NODEL?
	if(vex_read==NULL) {
		Hier_RemoveUnusedSym_OneComp_DoRem(Implem, comp, vex_read, vex_written);
		return 1;
	}

	// Handle when the symbol is read but not written to
	// FIXMEEEEE Handle when the component is hard-connected to something that writes to it
	//   Note: maybe currently such components always have the flag NODEL?
	if(vex_written==NULL) {
		bool is_serious = true;
		if(mem_data->init_vex!=NULL) is_serious = false;
		else {
			printf("WARNING: Memory '%s' is read but not written and has no initial values.\n", comp->name);
		}
		if(is_serious==true) {
			Hier_RemoveUnusedSym_OneComp_PrintUsage(Implem, comp);
			return -1;
		}
	}

	// Here the component is read from and kept.
	// Detect unused index ranges and crop the component.
	unsigned comp_width = mem_data->data_width;
	// Debug: print some details
	Hier_RemoveUnusedSym_PrintRanges(Implem, comp, comp_width, vex_read, vex_written);

	// Handle ROM with (un)signed extended init values
	if(vex_written==NULL) {
		// Here we have a ROM. Check whether all init values are unsigned/signed extension.
		assert(mem_data->init_vex!=NULL);
		// Note: Negative values mean uninitialized
		int common_sign_ext = -1;
		int common_unsign_ext = -1;
		for(unsigned i = 0; i<mem_data->cells_nb; i++) {
			hvex_t* vex = mem_data->init_vex[i];
			if(vex==NULL) continue;
			unsigned v;
			v = hvex_shrinku_getcrop(vex, '0');
			if(common_unsign_ext < 0 || v < common_unsign_ext) common_unsign_ext = v;
			v = hvex_shrinks_getcrop(vex);
			if(common_sign_ext < 0 || v < common_sign_ext) common_sign_ext = v;
			if(common_unsign_ext==0 && common_sign_ext==0) break;
		}
		// Little check for paranoia
		if(common_unsign_ext < 0 || common_sign_ext < 0) abort();  // All cells of the array are NULL? FIXME do something
		// Set parameters to crop all Actions
		bool ext_is_signed = false;
		unsigned crop_width = common_unsign_ext;
		if(common_sign_ext > common_unsign_ext) { ext_is_signed = true; crop_width = common_sign_ext; }
		unsigned max_left = mem_data->data_width-1 - crop_width;
		// Test if crop needed
		if(crop_width > 0 && vex_read->left > max_left) {
			// A bit of verbosity
			if(actsimp_verbose==true) {
				printf("INFO : ROM '%s' of width %u : Setting max index to %u because of %s extention of init values.\n",
					comp->name, mem_data->data_width, max_left, ext_is_signed==true ? "signed" : "unsigned"
				);
			}
			// Now, replace all occurrences.
			// Note: It's a ROM so we know we will only find read occurrences.
			chain_list* list_uses = Hier_GetSymbolOccur(Implem->H, comp->name);
			foreach(list_uses, scanOccur) {
				hvex_t* vex = scanOccur->DATA;
				if(vex->left <= max_left) continue;
				if(vex->right > max_left) {
					// Replace by only the extension
					if(ext_is_signed==true) {
						hvex_t* newvex = hvex_new_move(vex);
						// Change indexes
						newvex->left = max_left;
						newvex->right = max_left;
						newvex->width = 1;
						// Set the appropriate sign extension flag
						newvex->pad = 0;
						// Convert the original VEX into a RESIZE
						vex->model = HVEX_RESIZE;
						hvex_addop_head(vex, newvex);
					}
					else {
						hvex_clearinside(vex);
						hvex_lit_setrepeat(vex, '0', vex->width);
					}
				}
				else {
					// Crop the original expression and resize it
					hvex_t* newvex = hvex_new_move(vex);
					// Change indexes
					newvex->left = max_left;
					newvex->width = newvex->left - newvex->right + 1;
					// Set the appropriate sign extension flag
					if(ext_is_signed==true) newvex->pad = 0;
					else newvex->pad = '0';
					// Convert the original VEX into a RESIZE
					vex->model = HVEX_RESIZE;
					hvex_addop_head(vex, newvex);
				}
				// Stats
				count ++;
			}
			// IMPORTANT: Do not simplify the modified VEX because there could be other occurrences in operands,
			//   also because it might change access indexes of other symbols that will be processed after the current one
			freechain(list_uses);

			// Now, crop the mem component
			Netlist_Comp_Mem_CropData(comp, crop_width, 0);

			return count + 1;
		}
	}

	// If the component is not read full width, crop it
	if(vex_read->left+1 < comp_width || vex_read->right > 0) {
		if(actsimp_verbose==true) {
			printf("Info: Memory '%s' with data width %u is only read in range [%u-%u]. Cropping.\n",
				comp->name, comp_width, vex_read->left, vex_read->right
			);
		}
		// Crop the component
		Netlist_Comp_Mem_CropData(comp, comp_width-1 - vex_read->left, vex_read->right);
		// Modify the Actions in Hier
		count += Hier_RemoveUnusedSym_ModifyHier(Implem, comp, vex_read);
		return count + 1;
	}

	// FIXME Search for constant bits in expr of ASG operations where the sym is dest
	// FIXME Also detect accessed address range

	return count;
}

int Hier_RemoveUnusedSym_OnePass(implem_t* Implem) {
	unsigned count = 0;
	unsigned errors_nb = 0;

	// Only for debug
	//actsimp_verbose = true;

	avl_pp_tree_t all_read_symbols;
	avl_pp_tree_t all_written_symbols;

	avl_pp_init(&all_read_symbols);
	avl_pp_init(&all_written_symbols);

	// Get the usage of all symbols
	Hier_GetSymbols_treevex(Implem->H, &all_read_symbols, &all_written_symbols);

	// List all suitable symbols
	avl_pp_foreach(&Implem->netlist.top->children, scanComp) {
		netlist_comp_t* comp = scanComp->data;
		// Skip protected symbols
		if(Implem_SymFlag_Chk(Implem, comp->name, SYM_PROTECT_NODEL)==true) continue;

		hvex_t* vex_read = NULL;
		hvex_t* vex_written = NULL;

		avl_pp_find_data(&all_read_symbols, comp->name, (void**)&vex_read);
		avl_pp_find_data(&all_written_symbols, comp->name, (void**)&vex_written);

		int z = 0;
		if(comp->model==NETLIST_COMP_SIGNAL) {
			z = Hier_RemoveUnusedSym_Sig(Implem, comp, vex_read, vex_written);
		}
		else if(comp->model==NETLIST_COMP_REGISTER) {
			z = Hier_RemoveUnusedSym_Reg(Implem, comp, vex_read, vex_written);
		}
		else if(comp->model==NETLIST_COMP_MEMORY) {
			z = Hier_RemoveUnusedSym_Mem(Implem, comp, vex_read, vex_written);
		}
		if(z < 0) errors_nb ++; else count += z;

		// Very important: If Actions were removed or resized, some VEX nodes may have been deleted.
		// So we need to re-scan the Hier.
		if(z!=0) {
			// Clear the trees
			avl_pp_foreach(&all_read_symbols, scan) hvex_free(scan->data);
			avl_pp_foreach(&all_written_symbols, scan) hvex_free(scan->data);
			avl_pp_reset(&all_read_symbols);
			avl_pp_reset(&all_written_symbols);
			// Get the usage of all symbols
			Hier_GetSymbols_treevex(Implem->H, &all_read_symbols, &all_written_symbols);
		}

	}  // Scan all netlist components

	// Clear trees of VEX
	avl_pp_foreach(&all_read_symbols, scan) hvex_free(scan->data);
	avl_pp_foreach(&all_written_symbols, scan) hvex_free(scan->data);
	avl_pp_reset(&all_read_symbols);
	avl_pp_reset(&all_written_symbols);

	if(errors_nb > 0) {
		errprintfa("Simplification errors found: %u. Stopping.\n", errors_nb);
	}

	return count;
}


// Remove Actions when
// - Dest = Expr
// - Cond = 0

static int Hier_RemoveVoidActions(implem_t* Implem) {
	unsigned count = 0;

	chain_list* acts_to_delete = NULL;

	avl_p_foreach(&Implem->H->NODES[HIERARCHY_STATE], scanState) {
		hier_node_t* nodeTrans = scanState->data;
		hier_state_t* node_trans = &nodeTrans->NODE.STATE;

		foreach(node_trans->actions, scanAction) {
			hier_action_t* action = scanAction->DATA;

			// If the wired condition is '0', remove the Action
			hvex_t* vex_cond = hvex_asg_get_cond(action->expr);
			if(vex_cond!=NULL) {
				if(vex_cond->model==HVEX_LITERAL) {
					char* lit = hvex_lit_getval(vex_cond);
					if(lit[vex_cond->width-1]!='1') {
						acts_to_delete = addchain(acts_to_delete, action);
						continue;
					}
				}
			}

			// If Dest = Expr, remove the Action
			hvex_t* dest_dup = hvex_DupFullDest(action->expr);
			assert(dest_dup!=NULL);
			hvex_t* vex_expr = hvex_asg_get_expr(action->expr);
			assert(vex_expr!=NULL);
			int z = hvex_cmp(dest_dup, vex_expr);  // FIXME Should take into accound commutativity of operands, etc
			hvex_free(dest_dup);

			if(z==0) {
				acts_to_delete = addchain(acts_to_delete, action);
				continue;
			}

			// FIXME This makes simulation of many applications fail, reason still unknown
			// Maybe some other process of AUGH would put several Actions in a State, that write to the same Symbol?
			#if 0

			// Only for destination Register: remove unuseful left and right concatenations
			hvex_t* vex_dest = hvex_asg_get_dest(action->expr);
			char* destname = hvex_vec_getname(vex_dest);
			bool can_try_simp = true;
			// Get the dest component
			netlist_comp_t* comp = Netlist_Comp_GetChild(Implem->netlist.top, destname);
			if(comp==NULL) can_try_simp = false;
			else {
				if(comp->model!=NETLIST_COMP_REGISTER) can_try_simp = false;
			}
			// Forbid wired conditions
			// FIXME This is an attempt at avoiding a potential corner case
			//   where the scheduler (or something else) would put 2 Actions in the same State, with different conditions
			if(hvex_asg_get_cond(action->expr)!=NULL) can_try_simp = false;
			// Launch simp if appropriate
			if(can_try_simp==true) {
				if(vex_expr->model==HVEX_CONCAT) {
					hvex_t* opleft = vex_expr->operands;
					hvex_t* opright = hvex_last(vex_expr->operands);
					if(opleft!=opright) {
						unsigned crop_left = 0;
						unsigned crop_right = 0;
						// Check left padding
						if(opleft->model==HVEX_VECTOR) {
							if(hvex_vec_getname(opleft)==destname) {
								if(opleft->left==vex_dest->left) crop_left = opleft->width;
							}
						}
						// Check right padding
						if(opright->model==HVEX_VECTOR) {
							if(hvex_vec_getname(opright)==destname) {
								if(opright->right==vex_dest->right) crop_right = opright->width;
							}
						}
						// Perform crop
						if(crop_left > 0 || crop_right > 0) {
							hvex_croprel(vex_dest, crop_left, crop_right);
							hvex_croprel(vex_expr, crop_left, crop_right);
							count ++;
						}
					}
				}  // Expr is model CONCAT
			}  // Try to crop left and right

			#endif

		}  // Scan the Actions
	}  // Scan the states

	// Remove the unused Actions
	foreach(acts_to_delete, scanAct) {
		hier_action_t* action = scanAct->DATA;
		//printf("DEBUG %s:%d : Removing Action : ", __FILE__, __LINE__); HierAct_DebugPrint(action);
		Hier_Action_FreeFull(action);
		count ++;
	}
	freechain(acts_to_delete);

	return count;
}


// Remove registers/memories read only in Actions that write to themselves
// FIXME handle groups of reg/mem that write only to themselves

static int Hier_RemoveSym_CircularUse(implem_t* Implem) {
	unsigned count = 0;

	// These trees contain a chain_list*
	//   The list contains pointers to Actions
	avl_pp_tree_t sym_acts_r;
	avl_pp_tree_t sym_acts_w;

	avl_pp_init(&sym_acts_r);
	avl_pp_init(&sym_acts_w);

	void add_contents(avl_pp_tree_t* dest, avl_p_tree_t* syms, hier_action_t* action) {
		avl_p_foreach(syms, scanSym) {
			char* sym = scanSym->data;
			avl_pp_node_t* avl_node = NULL;
			bool b = avl_pp_add(dest, sym, &avl_node);
			if(b==true) avl_node->data = NULL;
			avl_node->data = addchain(avl_node->data, action);
		}
	}

	// Temporary trees to scan Actions
	avl_p_tree_t act_read_symbols;
	avl_p_tree_t act_written_symbols;
	avl_p_init(&act_read_symbols);
	avl_p_init(&act_written_symbols);

	// Get the usage of all symbols
	avl_p_foreach(&Implem->H->NODES[HIERARCHY_STATE], scan) {
		hier_node_t* node = scan->data;
		hier_state_t* state_data = &node->NODE.STATE;

		foreach(state_data->actions, scanAction) {
			hier_action_t* action = scanAction->DATA;
			hvex_GetSymbols_tree(action->expr, &act_read_symbols, &act_written_symbols);
			// Add the content of these trees to the global trees
			add_contents(&sym_acts_r, &act_read_symbols, action);
			add_contents(&sym_acts_w, &act_written_symbols, action);
			// Clear the temporary trees
			avl_p_reset(&act_read_symbols);
			avl_p_reset(&act_written_symbols);
		}
	}  // Scan all Actions

	chain_list* comp_to_delete = NULL;
	chain_list* acts_to_delete = NULL;

	// Scan all register and memory symbols
	avl_pp_foreach(&Implem->netlist.top->children, scanComp) {
		netlist_comp_t* comp = scanComp->data;
		if(comp->model!=NETLIST_COMP_REGISTER && comp->model!=NETLIST_COMP_MEMORY) continue;

		// Skip protected symbols
		if(Implem_SymFlag_Chk(Implem, comp->name, SYM_PROTECT_NODEL)==true) continue;
		if(Implem_SymFlag_Chk(Implem, comp->name, SYM_PROTECT_NOCIRC)==true) continue;

		// Get the Actions where it is read
		chain_list* list_acts_read = NULL;
		chain_list* list_acts_written = NULL;
		bool b;
		b = avl_pp_find_data(&sym_acts_r, comp->name, (void**)&list_acts_read);
		if(b==false) continue;
		b = avl_pp_find_data(&sym_acts_w, comp->name, (void**)&list_acts_written);
		if(b==false) continue;

		// Load the Write Actions in a tree (faster search)
		avl_p_tree_t tree_acts_w;
		avl_p_init(&tree_acts_w);
		foreach(list_acts_written, scan) avl_p_add_overwrite(&tree_acts_w, scan->DATA, scan->DATA);

		// Check if the set of Acts where the symbol is read
		//   is included in the Acts where it is written
		bool is_included = true;
		foreach(list_acts_read, scanRead) {
			b = avl_p_isthere(&tree_acts_w, scanRead->DATA);
			if(b==false) { is_included = false; break; }
		}

		// Conclusion
		if(is_included==true) {
			// List this symbol as to be deleted
			//   Ditto for all Actions it is written to.
			if(actsimp_verbose==true) {
				printf("Info: Circular usage of '%s'. Deleting.\n", comp->name);
			}
			comp_to_delete = addchain(comp_to_delete, comp);
			acts_to_delete = append(dupchain(list_acts_written), acts_to_delete);
			count++;
		}
		else {
			// FIXME: Try to build a group of symbols that would present circular usage

			// List the destination symbols of the Actions where the considered symbols are written
			//   If there are signals: fail.
			//   If there are new signals: add to the list and do the check again
			//     else: the considered symbols present circular usage.
		}

		avl_p_reset(&tree_acts_w);
	}

	// Clear
	avl_pp_foreach(&sym_acts_r, node) freechain(node->data);
	avl_pp_reset(&sym_acts_r);
	avl_pp_foreach(&sym_acts_w, node) freechain(node->data);
	avl_pp_reset(&sym_acts_w);

	// Display
	if(actsimp_verbose==true && comp_to_delete!=NULL) {
		printf("Info: Removing %u Actions due to circular usage of symbols:\n", ChainList_Count(acts_to_delete));
		bool is_first = true;
		foreach(comp_to_delete, scanComp) {
			netlist_comp_t* comp = scanComp->DATA;
			if(is_first==true) is_first = false; else putchar(',');
			printf(" %s model %s", comp->name, comp->model->name);
		}
		printf("\n");
	}

	// Remove symbol declarations
	foreach(comp_to_delete, scanComp) {
		netlist_comp_t* comp = scanComp->DATA;
		Netlist_Comp_Free(comp);
	}
	freechain(comp_to_delete);

	// Remove Actions
	foreach(acts_to_delete, scanAction) {
		hier_action_t* action = scanAction->DATA;
		Hier_Action_FreeFull(action);
	}
	freechain(acts_to_delete);

	return count;
}


// Insert temporary registers to remove data dependencies inside BBs

static int Hier_InsertTempRegs_InBB(implem_t* Implem) {
	unsigned done_nb = 0;

	// Analyze global register usage
	Hier_SymUse_FillHier(Implem);

	// Store all registers available in the Implem
	// Key = name, data = width
	avl_pi_tree_t tree_allregs;
	avl_pi_init(&tree_allregs);

	avl_pp_foreach(&Implem->netlist.top->children, scanReg) {
		netlist_comp_t* comp = scanReg->data;
		if(comp->model!=NETLIST_COMP_REGISTER) continue;
		netlist_register_t* reg_data = comp->data;
		// Skip protected symbols
		if(Implem_SymFlag_ChkAnyProtect(Implem, comp->name)==true ) continue;
		avl_pi_add_overwrite(&tree_allregs, comp->name, reg_data->width);
	}

	// All registers that can be freely used as temp reg in the scanned BB
	avl_pi_tree_t tree_freeregs;
	avl_pi_init(&tree_freeregs);

	// All registers used the scanned BB
	avl_pi_tree_t tree_usedregs;
	avl_pi_init(&tree_usedregs);

	// Process all BBs
	avl_p_foreach(&Implem->H->NODES[HIERARCHY_BB], scanBB) {
		hier_node_t* nodeBB = scanBB->data;
		hier_bb_t* bb_data = &nodeBB->NODE.BB;

		// Launch the process only if the BB is big enough
		if(bb_data->body==NULL || bb_data->body->NEXT==NULL) continue;

		// Find all registers NOT used nor live anywhere in the BB
		avl_pi_reset(&tree_freeregs);
		avl_pi_reset(&tree_usedregs);

		avl_pi_dup(&tree_freeregs, &tree_allregs);
		// The symbols used in the BB can't be used as temp reg
		foreach(bb_data->body, nodeState) {
			avl_pp_foreach(&nodeState->symuse, scanSym) {
				char* name = scanSym->key;
				bb_symuse_t* symuse = scanSym->data;
				unsigned width = symuse->width;
				avl_pi_add_overwrite(&tree_usedregs, name, width);
				avl_pi_rem_key(&tree_freeregs, name);
			}
		}

		//dbgprintf("Entering new BB...\n");

		// Now the heavy job begins

		avl_pi_foreach(&tree_usedregs, scanUsed) {
			char* name = scanUsed->key;
			if(Implem_SymFlag_ChkAnyProtect(Implem, name)==true ) continue;
			unsigned width = scanUsed->data;

			// Scan all States, starting from the first
			//   Look for symbols written in this State, but NOT flagged as live
			//   It means we have found the start of a life scope of a symbol

			// Scan the next nodes, find where the flag LIVE is no longer set
			//   It is the end of the life scope.

			// Note: if the symbol is live straight at the beginning of the BB,
			// no need to find the beginning of the first life scope

			bool beg_first_scope_found = false;
			bool end_first_scope_found = false;
			hier_node_t* nodeState = bb_data->body;

			for( ; nodeState!=NULL; nodeState=nodeState->NEXT) {
				bb_symuse_t* symuse = Hier_Node_SymUse_Get(nodeState, name);
				if(symuse==NULL) {
					assert(beg_first_scope_found==false);
					continue;
				}
				if( (symuse->allbits & BB_SYMUSE_IS_LIVE) != 0 ) beg_first_scope_found = true;
				else {
					if(beg_first_scope_found==false) {
						if( (symuse->allbits & BB_SYMUSE_IS_W)!=0 && (symuse->allbits & BB_SYMUSE_IS_LA)!=0 ) beg_first_scope_found = true;
					}
					else {
						assert( (symuse->allbits & BB_SYMUSE_IS_R)!=0 );
						end_first_scope_found = true;
						break;
					}
				}
			}

			// Check if the end of the first life scope was correctly found
			// If not, go check the next symbol
			if(end_first_scope_found==false) continue;

			// Find all next life scopes and rename the original symbol
			do {

				// Find all next life scope inside the BB, ensure it ends inside the BB
				// Inside this new life scope, replace the original symbol by a new register
				// Note: always use a temp register only one time inside a BB
				//   But a temp BB can be used in several BBs
				// Note: It may be interesting to identify ALL life scopes of all registers,
				//   in order to reveal circular usage simplification opportunities

				// Find the beginning of the next life scope
				bool beg_scope_found = false;
				for( ; nodeState!=NULL; nodeState=nodeState->NEXT) {
					bb_symuse_t* symuse = Hier_Node_SymUse_Get(nodeState, name);
					if(symuse==NULL) continue;
					assert( (symuse->allbits & BB_SYMUSE_IS_LIVE)==0 );
					if( (symuse->allbits & BB_SYMUSE_IS_W)!=0 && (symuse->allbits & BB_SYMUSE_IS_LA)!=0 ) {
						beg_scope_found = true;
						break;
					}
				}
				if(beg_scope_found==false) break;

				// Get the VEX occurrences of the original symbol... WRITE side only!
				chain_list* list_occur = NULL;
				hier_state_t* state_data = &nodeState->NODE.STATE;
				foreach(state_data->actions, scanAction) {
					hier_action_t* action = scanAction->DATA;
					hvex_t* Expr = hvex_asg_get_dest(action->expr);
					if(Expr!=NULL) list_occur = hvex_SearchSym_AddList(list_occur, Expr, name);
				}
				assert(list_occur!=NULL);

				// Scan the rest of the life scope
				bool end_scope_found = false;
				for(nodeState=nodeState->NEXT; nodeState!=NULL; nodeState=nodeState->NEXT) {
					bb_symuse_t* symuse = Hier_Node_SymUse_Get(nodeState, name);
					assert(symuse!=NULL);
					if( (symuse->allbits & BB_SYMUSE_IS_LIVE)!=0 ) {
						// Add all occurrences, R and W
						list_occur = Hier_Node_AddSymbolOccur_opt(list_occur, nodeState, name, false, false);
						continue;
					}
					// Add occurrences, only R
					assert( (symuse->allbits & BB_SYMUSE_IS_R) !=0 );
					hier_state_t* state_data = &nodeState->NODE.STATE;
					foreach(state_data->actions, scanAction) {
						hier_action_t* action = scanAction->DATA;
						list_occur = hvex_SearchSym_AddList(list_occur, hvex_asg_get_expr(action->expr), name);
						list_occur = hvex_SearchSym_AddList(list_occur, hvex_asg_get_addr(action->expr), name);
						list_occur = hvex_SearchSym_AddList(list_occur, hvex_asg_get_cond(action->expr), name);
					}
					end_scope_found = true;
					break;
				}
				if(end_scope_found==false) {
					freechain(list_occur);
					break;
				}

				// Now we can replace the occurrences that were found

				// Get an available register, large enough, or create a new one
				char* replace_name = NULL;
				avl_pi_foreach(&tree_freeregs, scanFree) {
					unsigned freewidth = scanFree->data;
					if(freewidth<width) continue;
					replace_name = scanFree->key;
					avl_pi_rem_key(&tree_freeregs, replace_name);
					break;
				}
				if(replace_name==NULL) {
					replace_name = Map_Netlist_MakeInstanceName(Implem, NETLIST_COMP_REGISTER);

					if(actsimp_verbose==true) {
						printf("Info: Inserting a new temporary register '%s'\n", replace_name);
					}

					netlist_comp_t* comp = Netlist_Comp_Reg_New(replace_name, width);
					Netlist_Comp_SetChild(Implem->netlist.top, comp);
					avl_pi_add_overwrite(&tree_allregs, replace_name, width);
				}

				if(actsimp_verbose==true) {
					printf("Info: Replacing one life scope of symbol '%s' by '%s'\n", name, replace_name);
				}

				// Replace the occurrences
				foreach(list_occur, scanOccur) {
					hvex_t* Expr = scanOccur->DATA;
					assert(Expr->model==HVEX_VECTOR);
					hvex_vec_setname(Expr, replace_name);
					done_nb++;
				}
				freechain(list_occur);

			} while(1);  // Scan life scopes and replace

		}  // Scan all used symbols that are not protected

	}  // Scan all BBs

	// Clean
	avl_pi_reset(&tree_allregs);
	avl_pi_reset(&tree_freeregs);
	avl_pi_reset(&tree_usedregs);

	// Return the number of things done ?
	return done_nb;
}


// Simplify all internal VEX expressions

int Hier_NodeLocal_SimplifyExpr(hier_node_t* node) {
	unsigned count = 0;

	switch(node->TYPE) {
		case HIERARCHY_STATE : {
			hier_state_t* state_data = &node->NODE.STATE;
			foreach(state_data->actions, scanAction) {
				hier_action_t* action = scanAction->DATA;
				count += hvex_simp(action->expr);
			}
			break;
		}
		case HIERARCHY_LOOP : {
			hier_loop_t* loop_data = &node->NODE.LOOP;
			if(loop_data->testinfo->cond!=NULL) count += hvex_simp(loop_data->testinfo->cond);
			break;
		}
		case HIERARCHY_CASE : {
			hier_case_t* case_data = &node->NODE.CASE;
			avl_p_foreach(&case_data->values, scanval) {
				hier_testinfo_t* testinfo = scanval->data;
				if(testinfo->cond!=NULL) count += hvex_simp(testinfo->cond);
			}
			break;
		}
		default : break;
	}

	return count;
}
int Hier_Level_SimplifyExpr(hier_node_t* node) {
	unsigned count = 0;
	for( ; node!=NULL; node=node->NEXT) {
		count += Hier_NodeLocal_SimplifyExpr(node);
		foreach(node->CHILDREN, scanChild) count += Hier_Level_SimplifyExpr(scanChild->DATA);
	}
	return count;
}
int Hier_SimplifyExpr(hier_t* H) {
	unsigned count = 0;
	avl_p_foreach(&H->NODES_ALL, scan) count += Hier_NodeLocal_SimplifyExpr(scan->data);
	return count;
}


// The main function to simplify Actions

int Hier_Simp_Actions(implem_t* Implem) {
	unsigned count = 0;

	unsigned iter_nb;
	for(iter_nb=1; ; iter_nb++) {
		bool redo = false;

		#if 1  // Display, for debug
		dbgprintf("Launching iteration %d...\n", iter_nb);
		//getchar();
		#endif

		int z;

		#if 1  // Hier_RemoveUnusedSym_OnePass
		z = Hier_RemoveUnusedSym_OnePass(Implem);
		count += z;
		if(z!=0) redo = true;
		#endif

		#if 1  // Hier_RemoveVoidActions
		z = Hier_RemoveVoidActions(Implem);
		count += z;
		if(z!=0) redo = true;
		#endif

		#if 1  // Hier_RemoveSym_CircularUse
		if(circularuse_en==true) {
			z = Hier_RemoveSym_CircularUse(Implem);
			count += z;
			if(z!=0) redo = true;
		}
		#endif

		#if 1  // Hier_RegAsgProp_Simple_OnePass
		if(asgpropag_en==true) {
			z = Hier_RegAsgProp_Simple_OnePass(Implem);
			count += z;
			if(z!=0) redo = true;
		}
		#endif

		#if 1  // Hier_RegAsgProp_SimpleFromLoop_OnePass
		if(asgpropag_en==true) {
			z = Hier_RegAsgProp_SimpleFromLoop_OnePass(Implem);
			count += z;
			if(z!=0) redo = true;
		}
		#endif

		#if 1  // Hier_RegAsgProp_SimpleFromLoop_SymUse_OnePass
		if(asgpropag_en==true && asgpropag_symuse_en==true) {
			z = Hier_RegAsgProp_SimpleFromLoop_SymUse_OnePass(Implem);
			count += z;
			if(z!=0) redo = true;
		}
		#endif

		#if 1  // Hier_ActionRemove_SimpleWaW_OnePass
		if(asgpropag_en==true) {
			z = Hier_ActionRemove_SimpleWaW_OnePass(Implem);
			count += z;
			if(z!=0) redo = true;
		}
		#endif

		#if 1  // Hier_RegAsgProp_ArrayRotStyle_OnePass
		if(asgpropag_en==true) {
			z = Hier_RegAsgProp_ArrayRotStyle_OnePass(Implem);
			count += z;
			if(z!=0) redo = true;
		}
		#endif

		// FIXME This does not work for many applications
		#if 0  // Hier_SymUse_RemUnusedAct
		if(asgpropag_symuse_en==true) {
			z = Hier_SymUse_RemUnusedAct(Implem);
			count += z;
			if(z!=0) redo = true;
		}
		#endif

  	#if 1  // Hier_InsertTempRegs_InBB
		if(inserttempregs_inbb_en==true) {
			z = Hier_InsertTempRegs_InBB(Implem);
			count += z;
			if(z!=0) redo = true;
		}
		#endif

		if(redo==false) break;
	}

	#ifndef NDEBUG
	dbgprintf("In %u iter : %u simplifications were done.\n", iter_nb, count);
	//printf("Press [return] to continue."); getchar();
	Hier_Check_Integrity(Implem->H);
	#endif

	return count;
}



//=====================================================================
// Update control
//=====================================================================

// Declarations for recursivity
static void Hier_GetBBs_BeforeNode_tree_rec(hier_node_t* node, avl_p_tree_t* tree, avl_p_tree_t* tree_scanned);
static void Hier_GetBBs_BeforeLoopTest_tree_rec(hier_node_t* node, avl_p_tree_t* tree, avl_p_tree_t* tree_scanned);

static void Hier_GetBBs_FromNode_tree_rec(hier_node_t* node, avl_p_tree_t* tree, avl_p_tree_t* tree_scanned) {
	bool b = avl_p_add_overwrite(tree_scanned, node, node);
	if(b==false) return;

	switch(node->TYPE) {

		case HIERARCHY_PROCESS: {
			hier_proc_t* proc_data = &node->NODE.PROCESS;
			foreach(proc_data->calls, scanCall) {
				// Note: use BeforeNode() instead of FromNode() is intentional
				Hier_GetBBs_BeforeNode_tree_rec(scanCall->DATA, tree, tree_scanned);
			}
			break;
		}

		case HIERARCHY_BB: {
			avl_p_add_overwrite(tree, node, node);
			break;
		}

		case HIERARCHY_STATE: {
			abort();
			break;
		}

		case HIERARCHY_LOOP: {
			// Code after an infinite loop is unreachable
			// (unless there is a specific JUMP that leads to it)
			hier_loop_t* loop_data = &node->NODE.LOOP;
			if(loop_data->testinfo->cond==NULL) return;
			// Scan the loop
			Hier_GetBBs_BeforeLoopTest_tree_rec(node, tree, tree_scanned);
			break;
		}

		case HIERARCHY_SWITCH: {
			foreach(node->CHILDREN, scan) {
				Hier_GetBBs_FromNode_tree_rec(scan->DATA, tree, tree_scanned);
			}
			break;
		}

		case HIERARCHY_CASE: {
			hier_case_t* case_data = &node->NODE.CASE;
			if(case_data->body==NULL) {
				Hier_GetBBs_BeforeNode_tree_rec(node->PARENT, tree, tree_scanned);
			}
			else {
				hier_node_t* lastnode = Hier_Level_GetLastNode(case_data->body);
				Hier_GetBBs_FromNode_tree_rec(lastnode, tree, tree_scanned);
			}
			break;
		}

		case HIERARCHY_JUMP: {
			Hier_GetBBs_BeforeNode_tree_rec(node, tree, tree_scanned);
			break;
		}

		case HIERARCHY_LABEL: {
			hier_label_t* label_data = &node->NODE.LABEL;
			foreach(label_data->jumps, scan) {
				Hier_GetBBs_FromNode_tree_rec(node, tree, tree_scanned);
			}
			Hier_GetBBs_BeforeNode_tree_rec(node, tree, tree_scanned);
			break;
		}

		case HIERARCHY_CALL: {
			hier_jump_t* call_data = &node->NODE.JUMP;
			hier_proc_t* proc_data = &call_data->target->NODE.PROCESS;
			foreach(proc_data->returns, scan) {
				Hier_GetBBs_FromNode_tree_rec(scan->DATA, tree, tree_scanned);
			}
			if(call_data->target->NEXT!=NULL) {
				hier_node_t* lastnode = Hier_Level_GetLastNode(call_data->target->NEXT);
				Hier_GetBBs_FromNode_tree_rec(lastnode, tree, tree_scanned);
			}
			break;
		}

		case HIERARCHY_RETURN: {
			Hier_GetBBs_BeforeNode_tree_rec(node, tree, tree_scanned);
			break;
		}

	}  // Switch on the node type

}
static void Hier_GetBBs_BeforeLoopTest_tree_rec(hier_node_t* node, avl_p_tree_t* tree, avl_p_tree_t* tree_scanned) {
	assert(node->TYPE==HIERARCHY_LOOP);
	hier_loop_t* loop_data = &node->NODE.LOOP;

	if(loop_data->body_after!=NULL) {
		hier_node_t* lastnode = Hier_Level_GetLastNode(loop_data->body_after);
		Hier_GetBBs_FromNode_tree_rec(lastnode, tree, tree_scanned);
	}
	else {
		if(loop_data->body_before!=NULL) {
			hier_node_t* lastnode = Hier_Level_GetLastNode(loop_data->body_before);
			Hier_GetBBs_FromNode_tree_rec(lastnode, tree, tree_scanned);
		}
		Hier_GetBBs_BeforeNode_tree_rec(node, tree, tree_scanned);
	}

}
static void Hier_GetBBs_BeforeNode_tree_rec(hier_node_t* node, avl_p_tree_t* tree, avl_p_tree_t* tree_scanned) {

	if(node->PREV!=NULL) {
		hier_node_t* prevnode = node->PREV;
		// Nodes after JUMP and RETURN nodes are unreachable
		// They can be reached if it's a LABEL, via the JUMP that lead to it
		if(prevnode->TYPE==HIERARCHY_JUMP || prevnode->TYPE==HIERARCHY_RETURN) return;
		// Launch scan from the prev node
		Hier_GetBBs_FromNode_tree_rec(prevnode, tree, tree_scanned);
		return;
	}

	// Here the node has no PREV node

	if(node->PARENT==NULL) {
		assert(node->TYPE!=HIERARCHY_PROCESS);
		return;
	}

	// Here the node has a PARENT
	hier_node_t* parent_node = node->PARENT;

	if(parent_node->TYPE==HIERARCHY_LOOP) {
		hier_loop_t* loop_data = &node->NODE.LOOP;
		if(node==loop_data->body_after) {
			if(loop_data->body_before!=NULL) {
				hier_node_t* lastnode = Hier_Level_GetLastNode(loop_data->body_before);
				Hier_GetBBs_FromNode_tree_rec(lastnode, tree, tree_scanned);
			}
			else {
				hier_node_t* lastnode = Hier_Level_GetLastNode(loop_data->body_after);
				Hier_GetBBs_FromNode_tree_rec(lastnode, tree, tree_scanned);
			}
			Hier_GetBBs_BeforeNode_tree_rec(parent_node, tree, tree_scanned);
		}
		else if(node==loop_data->body_before) {
			if(loop_data->body_after!=NULL) {
				hier_node_t* lastnode = Hier_Level_GetLastNode(loop_data->body_after);
				Hier_GetBBs_FromNode_tree_rec(lastnode, tree, tree_scanned);
			}
			else {
				hier_node_t* lastnode = Hier_Level_GetLastNode(loop_data->body_before);
				Hier_GetBBs_FromNode_tree_rec(lastnode, tree, tree_scanned);
				// Also scan what is before the loop
				Hier_GetBBs_BeforeNode_tree_rec(parent_node, tree, tree_scanned);
			}
		}
		else {
			abort();
		}
	}
	else if(parent_node->TYPE==HIERARCHY_SWITCH) {
		Hier_GetBBs_BeforeNode_tree_rec(parent_node, tree, tree_scanned);
	}
	else if(parent_node->TYPE==HIERARCHY_CASE) {
		Hier_GetBBs_BeforeNode_tree_rec(parent_node, tree, tree_scanned);
	}
	else {
		abort();
	}

}

void Hier_GetBBs_BeforeNode_tree(hier_node_t* node, avl_p_tree_t* tree_bbs) {
	avl_p_tree_t tree_flags;
	avl_p_init(&tree_flags);

	Hier_GetBBs_BeforeNode_tree_rec(node, tree_bbs, &tree_flags);

	avl_p_reset(&tree_flags);
}
void Hier_FlagTestActs_BeforeNode_tree(hier_node_t* node, bool is_looptest, avl_p_tree_t* tree_names) {
	avl_p_tree_t tree_bbs;
	avl_p_tree_t tree_scanned;

	avl_p_init(&tree_bbs);
	avl_p_init(&tree_scanned);

	// Get all previous BBs
	if(is_looptest==true) {
		Hier_GetBBs_BeforeLoopTest_tree_rec(node, &tree_bbs, &tree_scanned);
	}
	else {
		Hier_GetBBs_BeforeNode_tree_rec(node, &tree_bbs, &tree_scanned);
	}

	// Scan the BBs, only the last State
	avl_p_foreach(&tree_bbs, scanBB) {
		hier_node_t* nodeBB = scanBB->data;
		hier_bb_t* bb_data = &nodeBB->NODE.BB;
		assert(bb_data->body!=NULL);
		// Only process the last State
		hier_node_t* nodeState = Hier_Level_GetLastNode(bb_data->body);
		assert(nodeState->TYPE==HIERARCHY_STATE);
		hier_state_t* state_data = &nodeState->NODE.STATE;
		// Scan the Actions in the last State only
		foreach(state_data->actions, scanAction) {
			hier_action_t* action = scanAction->DATA;
			if(action->expr->model!=HVEX_ASG) continue;
			char* namedest = hvex_asg_get_destname(action->expr);
			if(avl_p_isthere(tree_names, namedest)==true) {
				// Set the flags
				HierAct_FlagSet(action, HIER_ACT_END | HIER_ACT_TEST);
			}
		}  // Scan the Actions
	}  // Scan the BBs

	avl_p_reset(&tree_bbs);
	avl_p_reset(&tree_scanned);
}

void Hier_Update_Control(implem_t* Implem) {

	// Process all loops
	Hier_Loops_DetectControl(Implem);

	// Detect the actual test of the SWITCH nodes
	Hier_Switch_UpdControl_All(Implem);

	// Scan all Actions, remove flags HIER_ACT_END and HIER_ACT_TEST
	avl_p_foreach(&Implem->H->NODES[HIERARCHY_STATE], scanState) {
		hier_node_t* node = scanState->data;
		hier_state_t* state_data = &node->NODE.STATE;
		foreach(state_data->actions, scanAction) {
			hier_action_t* action = scanAction->DATA;
			HierAct_FlagClear(action, HIER_ACT_END | HIER_ACT_TEST);
		}
	}

	// Now we can set the flag END to the test Actions

	// Note: we can't just flag all Actions in the Hier whose dest sym is used in a test
	// Because some stray Actions, unused, may remain, and may also not be at the last State of the BB
	// And they must not be flagged, that can create dependency loops when scheduling the BB
	// So, we actually find all Actions that participate in the tests and we flag onlt these.

	avl_p_tree_t tree_names;
	avl_p_init(&tree_names);

	avl_p_foreach(&Implem->H->NODES[HIERARCHY_SWITCH], scanSwitch) {
		hier_node_t* nodeSwitch = scanSwitch->data;
		foreach(nodeSwitch->CHILDREN, scanCase) {
			hier_node_t* nodeCase = scanCase->DATA;
			hier_case_t* case_data = &nodeCase->NODE.CASE;
			// Scan all values associated to the Case
			avl_p_foreach(&case_data->values, scanval) {
				hier_testinfo_t* testinfo = scanval->data;
				hvex_GetSymbols_tree(testinfo->cond, &tree_names, NULL);
			}
		}  // Scan all Case nodes
		// Search all Actions that write these symbols and flag them
		Hier_FlagTestActs_BeforeNode_tree(nodeSwitch, false, &tree_names);
		avl_p_reset(&tree_names);
	}  // Scan all Swith nodes

	avl_p_foreach(&Implem->H->NODES[HIERARCHY_LOOP], scanLoop) {
		hier_node_t* nodeLoop = scanLoop->data;
		hier_loop_t* loop_data = &nodeLoop->NODE.LOOP;
		// Get all symbols used in the test
		hvex_GetSymbols_tree(loop_data->testinfo->cond, &tree_names, NULL);
		// Search all Actions that write these symbols and flag them
		Hier_FlagTestActs_BeforeNode_tree(nodeLoop, true, &tree_names);
		avl_p_reset(&tree_names);
	}  // Scan all LOOP nodes

	avl_p_reset(&tree_names);
}



//=====================================================================
// Simplification, optimization of Hier nodes
//=====================================================================

typedef struct hier_node_simp_t {
	unsigned cutlink;
	unsigned deadnodes_del;
	unsigned call_empty;
	unsigned call_inline;
	unsigned jump_del;
	unsigned label_del;
	unsigned label_merge;
	unsigned state_empty_del;
	unsigned bb_empty_del;
	unsigned bb_merge;
	unsigned loop_noiter;
	unsigned for_iter_0;
	unsigned switch_empty;
	unsigned switch_is_case;
	unsigned switch_is_def;
	unsigned switch_case_del;
	unsigned misc;
} hier_node_simp_t;

// Declaration
static void Hier_Simp_Level(implem_t* Implem, hier_node_t* node, hier_node_simp_t* result);

static hier_node_t* Hier_Simp_NodeLoop(implem_t* Implem, hier_node_t* node, hier_node_simp_t* result) {
	hier_node_t* node_next = node->NEXT;
	hier_t* H = Implem->H;

	hier_loop_t* loop_data = &node->NODE.LOOP;

	// Simplify the bodies
	Hier_Simp_Level(Implem, loop_data->body_after, result);
	Hier_Simp_Level(Implem, loop_data->body_before, result);
	Hier_Loop_UpdControl(Implem, node);

	if(hier_loop_chknever(loop_data->flags)) {

		#if 0
		printf("DEBUG: Replacing a loop that never iterates\n");
		LineList_Print_be(node->source_lines, "  Source lines: ", "\n");
		#endif

		// Add to the results
		result->loop_noiter++;
		// Extract the Body
		hier_node_t* body = loop_data->body_after;
		// Unlink all bodies instead of deleting, in case there are jumps leading inside
		while(node->CHILDREN!=NULL) Hier_ReplaceChild(node, node->CHILDREN->DATA, NULL);
		// Do the actual replacement
		Hier_ReplaceNode_Free(H, node, body);
		return body;
	}

	// If number of iterations is zero
	if(hier_for_chkiternb(loop_data->flags) && loop_data->iter.iterations_nb==0) {

		#if 0
		printf("DEBUG: Replacing a FOR loop with 0 iterations\n");
		LineList_Print_be(node->source_lines, "  Source lines: ", "\n");
		#endif

		// Add to the results
		result->for_iter_0++;
		// Extract the Body
		hier_node_t* body = loop_data->body_after;
		// Unlink all bodies instead of deleting, in case there are jumps leading inside
		while(node->CHILDREN!=NULL) Hier_ReplaceChild(node, node->CHILDREN->DATA, NULL);
		// Do the actual replacement
		Hier_ReplaceNode_Free(H, node, body);
		return body;
	}

	// If infinite loop
	if(hier_loop_chkalways(loop_data->flags) || loop_data->testinfo->cond==NULL || Hier_TestInfo_GetVal(loop_data->testinfo)==true) {
		// Merge the bodies, or just move body_before in place of body_after
		if(loop_data->body_before!=NULL) {
			hier_node_t* body_before = loop_data->body_before;
			if(loop_data->body_after==NULL) {
				loop_data->body_after = body_before;
				loop_data->body_before = NULL;
			}
			else {
				Hier_ReplaceChild(node, loop_data->body_before, NULL);
				Hier_InsertLevelAfterLevel(loop_data->body_after, body_before);
			}
			result->misc++;
		}
	}

	// If post-loading style, convert to ONEBODY style
	if(
		hier_for_chkonebody(loop_data->flags)==false &&
		hier_for_chkiternb(loop_data->flags) &&
		loop_data->iter.iterations_nb > 1
	) {

		// Create one BB that will contain one State: the INC and TEST Actions
		hier_build_bbstact_t bbstact;
		Hier_Build_BBState(&bbstact);
		// Set source lines
		Hier_Nodes_SetLines(bbstact.bb, node->source_lines, true, true);
		// Move the INC and TEST Actions there
		Hier_Action_Unlink(loop_data->iter.inc_act);
		Hier_Action_Link(bbstact.state, loop_data->iter.inc_act);
		Hier_Action_Unlink(loop_data->testinfo->sig_asg);
		Hier_Action_Link(bbstact.state, loop_data->testinfo->sig_asg);

		// Modify the TEST Action
		unsigned val_test = loop_data->iter.val_init + (loop_data->iter.iterations_nb - 1) * loop_data->iter.val_step;
		hvex_t* newtest = hvex_newmodel_op2(HVEX_NE, 1,
			hvex_dup(loop_data->iter.vex_iterator),
			hvex_newlit_int(val_test, loop_data->iter.vex_iterator->width, false)
		);
		hvex_replace_free(hvex_asg_get_expr(loop_data->testinfo->sig_asg->expr), newtest);
		if(loop_data->testinfo->oper_not==true) hvex_not(loop_data->testinfo->cond);

		// Remove the body_after, replace by the body_before
		Hier_RemoveLevel_Free(H, loop_data->body_after);
		loop_data->body_after = loop_data->body_before;
		loop_data->body_before = NULL;

		// Append the new BB to the body_after
		Hier_Lists_AddLevel(H, bbstact.bb);
		Hier_InsertLevelAfterLevel(loop_data->body_after, bbstact.bb);

		// Simplify the body_after
		Hier_Simp_Level(Implem, loop_data->body_after, result);

		// Update control, again
		Hier_Loop_UpdControl(Implem, node);

		return node_next;
	}

	return node_next;
}

static hier_node_t* Hier_Simp_NodeSwitch(implem_t* Implem, hier_node_t* node, hier_node_simp_t* result) {
	hier_node_t* node_next = node->NEXT;
	hier_t* H = Implem->H;

	hier_switch_t* switch_data = & node->NODE.SWITCH;

	// Simplify the cases
	foreach(node->CHILDREN, scanChild) {
		hier_node_t* child = scanChild->DATA;
		hier_case_t* case_data = &child->NODE.CASE;
		Hier_Simp_Level(Implem, case_data->body, result);
	}

	// If the condition of one CASE is known to be true, delete all other cases
	// On the fly, check if all bodies are empty (to delete the switch)
	//   and if all tests are false (replace the switch by the default case)

	hier_node_t* case_always_taken = NULL;
	bool all_tests_false = true;  // To detect when the DEFAULT case is taken
	bool all_cases_empty = true;  // To detect when the entire SWITCH is empty

	foreach(node->CHILDREN, scanChild) {
		hier_node_t* nodeCase = scanChild->DATA;
		hier_case_t* case_data = &nodeCase->NODE.CASE;
		if(case_data->body!=NULL) all_cases_empty = false;
		if(case_data->is_default!=0) continue;

		// Scan all values associated to the Case
		avl_p_foreach(&case_data->values, scanval) {
			hier_testinfo_t* testinfo = scanval->data;
			// Check whether the test result is known
			int val = Hier_TestInfo_GetVal(testinfo);
			if(val==true) {
				all_tests_false = false;
				case_always_taken = nodeCase;
				break;
			}
			else if(val==false) {
				// Nothing to do here
			}
			else {
				// The test result is uncertain
				all_tests_false = false;
			}
		}  // Scan all values of the Case

		if(case_always_taken!=NULL) break;

	}  // Loop on all cases

	hier_node_t* case_replace_switch = NULL;
	bool remove_switch = false;

	if(case_always_taken!=NULL) {
		hier_case_t* case_data = &case_always_taken->NODE.CASE;
		if(case_data->body!=NULL) case_replace_switch = case_always_taken;
		else remove_switch = true;
	}
	else if(all_tests_false==true) {
		if(switch_data->info.default_case!=NULL) {
			hier_case_t* case_data = &switch_data->info.default_case->NODE.CASE;
			if(case_data->body!=NULL) case_replace_switch = switch_data->info.default_case;
			else remove_switch = true;
		}
		else remove_switch = true;
	}
	else if(all_cases_empty==true) {
		remove_switch = true;
	}

	if(remove_switch==true) {

		#if 0
		printf("DEBUG: Switch removed\n");
		LineList_Print_be(node->source_lines, "  Switch lines .. ", "\n");
		#endif

		// Unlink all bodies instead of deleting, in case there are jumps leading inside
		while(node->CHILDREN!=NULL) {
			hier_node_t* nodeCase = node->CHILDREN->DATA;
			if(nodeCase->NODE.CASE.body!=NULL) Hier_ReplaceChild(nodeCase, nodeCase->NODE.CASE.body, NULL);
			Hier_ReplaceNode_Free(H, nodeCase, NULL);
		}
		// Remove the node
		Hier_ReplaceNode_Free(H, node, NULL);
		result->switch_empty++;
		return node_next;
	}

	if(case_replace_switch!=NULL) {
		hier_case_t* case_data = &case_replace_switch->NODE.CASE;

		#if 0
		printf("DEBUG: Switch replaced by Case\n");
		LineList_Print_be(node->source_lines, "  Switch lines .. ", "\n");
		//LineList_Print_be(case_replace_switch->source_lines, "  Case lines .... ", "\n");
		printf("  Case values ... ");
		bool is_first = true;
		avl_p_foreach(&case_data->values, scan) {
			hier_testinfo_t* testinfo = scan->data;
			if(is_first==true) is_first = false; else printf(", ");
			viewvexexprbound( Hier_SwitchCase_GetTestValue(case_replace_switch, testinfo) );
		}
		printf("\n");
		#endif

		// Extract the Body
		hier_node_t* replace_body = case_data->body;
		assert(replace_body!=NULL);
		// Unlink all bodies instead of deleting, in case there are jumps leading inside
		while(node->CHILDREN!=NULL) {
			hier_node_t* nodeCase = node->CHILDREN->DATA;
			if(nodeCase->NODE.CASE.body!=NULL) Hier_ReplaceChild(nodeCase, nodeCase->NODE.CASE.body, NULL);
			Hier_ReplaceNode_Free(H, nodeCase, NULL);
		}
		// Do the actual replacement
		Hier_ReplaceNode_Free(H, node, replace_body);
		result->switch_is_case++;
		return replace_body;
	}

	// FIXME this is not generic enough
	//   We shoud merge all Cases with similar body (regardless if it involves the DEFAULT)
	//   Note: don't forget to merge user annotations

	#if 0
	// If no DEFAULT case is defined, remove cases with empty bodies
	if(switch_data->info.default_case==NULL) {
		chain_list* cases_to_delete = NULL;

		foreach(node->CHILDREN, scanChild) {
			hier_node_t* nodeCase = scanChild->DATA;
			hier_case_t* case_data = &nodeCase->NODE.CASE;
			if(case_data->body!=NULL) continue;
			cases_to_delete = addchain(cases_to_delete, nodeCase);
		}  // Loop on all cases

		foreach(cases_to_delete, scanCase) {
			hier_node_t* nodeCase = scanCase->DATA;
			hier_case_t* case_data = &nodeCase->NODE.CASE;
			if(case_data->is_default!=0) switch_data->info.default_case = NULL;
			Hier_ReplaceNode_Free(H, nodeCase, NULL);
			result->switch_case_del++;
		}
		freechain(cases_to_delete);

		// Paranoia if the SWITCH is empty
		if(node->CHILDREN==NULL) {
			Hier_ReplaceNode_Free(H, node, NULL);
			result->switch_empty++;
			return node_next;
		}

	}  // If no DEFAULT case is defined, remove cases with empty bodies
	#endif

	return node_next;
}

static hier_node_t* Hier_Simp_NodeLabel(implem_t* Implem, hier_node_t* node, hier_node_simp_t* result) {
	hier_node_t* node_next = node->NEXT;
	hier_t* H = Implem->H;

	hier_label_t* label_data = &node->NODE.LABEL;

	if(label_data->name==NULL && label_data->jumps==NULL) {
		// This node can be deleted
		if(nodesimp_verbose==true) {
			printf("Info: Removing unused Label node (ptr %p)\n", node);
		}
		Hier_ReplaceNode_Free(H, node, NULL);
		result->label_del++;
	}

	else if(node->PREV!=NULL && node->PREV->TYPE==HIERARCHY_LABEL) {
		hier_label_t* prevlabel_data = &node->PREV->NODE.LABEL;
		if(label_data->name==NULL || prevlabel_data->name==NULL) {
			if(nodesimp_verbose==true) {
				printf("Info: Merging Label node ptr %p into node ptr %p\n", node->PREV, node);
			}
			// Move the jumps of the prev Label into the current Label
			while(prevlabel_data->jumps!=NULL) {
				hier_node_t* nodeJump = prevlabel_data->jumps->DATA;
				prevlabel_data->jumps = ChainList_Remove(prevlabel_data->jumps, nodeJump);
				Hier_JumpLabel_Link(nodeJump, node);
			}
			if(prevlabel_data->name!=NULL) label_data->name = prevlabel_data->name;
			Hier_ReplaceNode_Free(H, node->PREV, NULL);
			result->label_merge++;
		}
	}

	return node_next;
}

static hier_node_t* Hier_Simp_NodeJump(implem_t* Implem, hier_node_t* node, hier_node_simp_t* result) {
	hier_node_t* node_next = node->NEXT;
	hier_t* H = Implem->H;

	hier_jump_t* jump_data = &node->NODE.JUMP;
	hier_node_t* nodeLabel = jump_data->target;

	if(nodeLabel==node->NEXT) {
		if(nodesimp_verbose==true) {
			printf("Info: Removing Jump node (ptr %p) whose destination Label (ptr %p) is right after\n", node, nodeLabel);
		}
		// This can happen with RETURN nodes when inlining functions
		// Keep code hierarchy as given in input C
		Hier_ReplaceNode_Free(H, node, NULL);
		result->jump_del++;
		return node_next;
	}

	// FIXME Without that appli chstone/jpeg fails!
	return node_next;

	// If the destination Label is only reachable via the current Jump
	// (the prev node is a Jump, Return or an infinite loop)
	// and ONLY if the Level from that Label ends by a Jump/Return/InfLoop, then enqueue that Level after the current Jump

	// Check if the dest Label is only reachable via this JUMP
	hier_label_t* label_data = &nodeLabel->NODE.LABEL;
	bool canlink = true;
	if(ChainList_Count(label_data->jumps)!=1) canlink = false;
	// If there is a direct Parent, no re-link possible
	if(canlink==true && nodeLabel->PARENT!=NULL) canlink = false;
	// If there is a Prev node, ensure it is a break in the control flow: Jump/Return/Infloop
	if(canlink==true && nodeLabel->PREV!=NULL) {
		if(nodeLabel->PREV->TYPE==HIERARCHY_JUMP) {}
		else if(nodeLabel->PREV->TYPE==HIERARCHY_RETURN) {}
		else if(nodeLabel->PREV->TYPE==HIERARCHY_LOOP) {
			hier_loop_t* loop_data = &nodeLabel->PREV->NODE.LOOP;
			if(hier_loop_chkalways(loop_data->flags) || loop_data->testinfo->cond==NULL || Hier_TestInfo_GetVal(loop_data->testinfo)==true) {
				// Here the loop is an infinite loop, it's OK
			}
			else canlink = false;
		}
		else {
			canlink = false;
		}
	}

	// Find another break in the control flow after the Label: Jump/Return/Infloop
	// FIXME We have to ensure that our Jump is not reachable throught one of the children of the Level
	hier_node_t* afterNode = NULL;
	if(canlink==true) {
		for(afterNode=nodeLabel->NEXT; afterNode!=NULL; afterNode=afterNode->NEXT) {
			if(afterNode->TYPE==HIERARCHY_JUMP) {
				// Forbid cases where the Label was before the Jump
				if(afterNode==node) canlink = false;
				break;
			}
			else if(afterNode->TYPE==HIERARCHY_RETURN) break;
			else if(afterNode->TYPE==HIERARCHY_LOOP) {
				hier_loop_t* loop_data = &afterNode->NODE.LOOP;
				if(hier_loop_chkalways(loop_data->flags) || loop_data->testinfo->cond==NULL || Hier_TestInfo_GetVal(loop_data->testinfo)==true) {
					// Here the loop is an infinite loop, it's OK
					break;
				}
			}
		}
		// If the node is not NULL, it is indeed a break in the control flow
		if(afterNode!=NULL) afterNode = afterNode->NEXT;
		else canlink = false;
	}

	// Perform re-link
	if(canlink==true) {
		if(nodesimp_verbose==true) {
			printf("Info: Removing Jump node (ptr %p) and replacing by its destination Label (ptr %p)\n", node, nodeLabel);
		}
		// If there is an afterNode, cut the link just before
		if(afterNode!=NULL) Hier_CutLinkBefore(afterNode);
		// If the Label has previous/parent stuff, cut and replace by the afterNode
		if(nodeLabel->PREV!=NULL) {
			hier_node_t* prevNode = nodeLabel->PREV;
			Hier_CutLinkAfter(prevNode);
			if(afterNode!=NULL) Hier_Nodes_Link(prevNode, afterNode);
		}
		else if(nodeLabel->PARENT!=NULL) {
			hier_node_t* parentNode = nodeLabel->PARENT;
			Hier_ReplaceChild(parentNode, nodeLabel, afterNode);
		}
		// Ensure there is nothing after the Jump
		Hier_CutLinkAfter(node);
		// Here the Label is completely disconnected, with its Level
		// Remove the JUMP, replace by the Label
		Hier_Nodes_Link(node, nodeLabel);
		Hier_ReplaceNode_Free(Implem->H, node, NULL);
		result->jump_del++;
		return nodeLabel;
	}

	// The Level should be finished (or reached via a different path)
	return NULL;
}

static hier_node_t* Hier_Simp_NodeCall(implem_t* Implem, hier_node_t* node, hier_node_simp_t* result) {
	hier_node_t* node_next = node->NEXT;
	//hier_t* H = Implem->H;

	hier_jump_t* call_data = &node->NODE.JUMP;
	hier_node_t* nodeProc = call_data->target;
	hier_proc_t* proc_data = &nodeProc->NODE.PROCESS;

	// Remove the CALL node if the target PROCESS has no body
	if(nodeProc->NEXT==NULL || nodeProc->NEXT->TYPE==HIERARCHY_RETURN) {
		if(nodesimp_verbose==true) {
			printf("Info: Removing Call to empty Process '%s'\n", proc_data->name_orig);
		}
		Hier_ReplaceNode_Free(Implem->H, node, NULL);
		result->call_empty ++;
		return node_next;
	}

	// Inline the PROCESS if there is only one CALL
	if(autoinline_en==true && proc_data->user.no_autoinline==false && ChainList_Count(proc_data->calls)==1) {
		if(nodesimp_verbose==true) {
			printf("Info: Inlining Call to Process '%s' because it's the only Call to it\n", proc_data->name_orig);
		}

		// Create a LABEL, it is the destination of all Return nodes. Link it just after the Call.
		hier_node_t* nodeLabel = Hier_Node_NewType(HIERARCHY_LABEL);
		Hier_Lists_AddNode(Implem->H, nodeLabel);
		Hier_InsertLevelAfterNode(node, nodeLabel);

		if(nodesimp_verbose==true) {
			printf("  Info: Creating Label node (ptr %p) as destination for all Return nodes\n", nodeLabel);
		}

		#if 0  // For debug: give the Label a meaningful name
		char buf[strlen(proc_data->name_orig) + 50];
		sprintf(buf, "augh_retfromproc_%s", proc_data->name_orig);
		nodeLabel->NODE.LABEL.name = namealloc(buf);
		#endif

		// Replace all RETURN nodes by a JUMP to the new LABEL
		while(proc_data->returns!=NULL) {
			hier_node_t* nodeRet = proc_data->returns->DATA;
			hier_node_t* nodeJump = Hier_Node_NewType(HIERARCHY_JUMP);
			nodeJump->NODE.JUMP.type = HIER_JUMP_RET;
			Hier_Lists_AddNode(Implem->H, nodeJump);
			Hier_JumpLabel_Link(nodeJump, nodeLabel);
			Hier_ReplaceNode_Free(Implem->H, nodeRet, nodeJump);
		}

		// Extract the Proc body
		hier_node_t* body = nodeProc->NEXT;
		Hier_CutLinkBefore(body);

		// Replace the Call node by the Proc body
		Hier_ReplaceNode_Free(Implem->H, node, body);
		// Remove the PROCESS node
		Hier_ReplaceNode_Free(Implem->H, nodeProc, NULL);

		// Increment things done
		result->call_inline ++;

		// Continue simplification at the beginning of the process body
		return body;
	}

	return node_next;
}

static hier_node_t* Hier_Simp_NodeState(implem_t* Implem, hier_node_t* node, hier_node_simp_t* result) {
	hier_node_t* node_next = node->NEXT;
	hier_t* H = Implem->H;

	hier_state_t* state_data = &node->NODE.STATE;

	if(state_data->actions==NULL) {
		Hier_ReplaceNode_Free(H, node, NULL);
		result->state_empty_del++;
	}

	return node_next;
}

static hier_node_t* Hier_Simp_NodeBB(implem_t* Implem, hier_node_t* node, hier_node_simp_t* result) {
	hier_node_t* node_next = node->NEXT;
	hier_t* H = Implem->H;

	hier_bb_t* bb_data = &node->NODE.BB;

	Hier_Simp_Level(Implem, bb_data->body, result);

	if(bb_data->body==NULL) {
		Hier_ReplaceNode_Free(H, node, NULL);
		result->bb_empty_del++;
		return node_next;
	}

	if(node_next!=NULL && node_next->TYPE==HIERARCHY_BB) {
		hier_node_t* body_next = node_next->NODE.BB.body;
		if(body_next==NULL) return node_next;
		// Extract the body of the next node
		Hier_ReplaceChild(node_next, body_next, NULL);
		// Concat the two bodies
		Hier_InsertLevelAfterLevel(bb_data->body, body_next);
		// Add the timings
		time_casefactor_add_once(&node->timing, &node_next->timing.once);
		// Free the next node. Its body is already unlinked so it is safe.
		Hier_ReplaceNode_Free(H, node_next, NULL);
		// Add in the results
		result->bb_merge++;
		return node;
	}

	return node_next;
}

// Return : the next node of the level to process.
// The changes are already applied.
static hier_node_t* Hier_Simp_Node(implem_t* Implem, hier_node_t* node, hier_node_simp_t* result) {
	if(node==NULL) return NULL;
	hier_t* H = Implem->H;
	assert(avl_p_isthere(&H->NODES_ALL, node)==true);

	//printf("DEBUG %s:%d : node %p, type %d\n", __FILE__, __LINE__, node, node->TYPE);

	switch(node->TYPE) {

		case HIERARCHY_BB : {
			return Hier_Simp_NodeBB(Implem, node, result);
		}

		case HIERARCHY_STATE : {
			return Hier_Simp_NodeState(Implem, node, result);
		}

		case HIERARCHY_LOOP : {
			return Hier_Simp_NodeLoop(Implem, node, result);
		}

		case HIERARCHY_SWITCH : {
			return Hier_Simp_NodeSwitch(Implem, node, result);
		}

		case HIERARCHY_LABEL : {
			return Hier_Simp_NodeLabel(Implem, node, result);
		}

		case HIERARCHY_JUMP : {
			return Hier_Simp_NodeJump(Implem, node, result);
		}

		case HIERARCHY_CALL : {
			return Hier_Simp_NodeCall(Implem, node, result);
		}

		case HIERARCHY_RETURN : {
			// The Level should be finished (or reached via a different path)
			return NULL;
		}

		default : {
			break;
		}

	}  // Switch on the node type

	#if 0  // Debug: Check integrity of the Hier graph
	Hier_Check_Integrity(Implem->H);
	#endif

	return node->NEXT;
}

static void Hier_Simp_Level(implem_t* Implem, hier_node_t* node, hier_node_simp_t* result) {
	while(node!=NULL) {
		node = Hier_Simp_Node(Implem, node, result);
	}
}

int Hier_Simp_Nodes(implem_t* Implem) {
	hier_node_simp_t result;
	memset(&result, 0, sizeof(result));

	// Remove dead code
	result.deadnodes_del += Hier_RemoveUnusedNodes(Implem->H);

	#if 0  // Debug: Check integrity of the Hier graph
	Hier_Check_Integrity(Implem->H);
	#endif

	// List the PROCESS nodes (in case some are deleted while being simplified)
	chain_list* list_nodes = NULL;
	avl_p_foreach(&Implem->H->NODES[HIERARCHY_PROCESS], scan) list_nodes = addchain(list_nodes, scan->data);
	avl_p_foreach(&Implem->H->NODES[HIERARCHY_LABEL], scan) {
		hier_node_t* node = scan->data;
		if(node->PARENT==NULL && node->PREV==NULL) list_nodes = addchain(list_nodes, node);
	}
	// Scan all PROCESS nodes, simplify their body
	foreach(list_nodes, scan) {
		hier_node_t* node = scan->DATA;
		// Check if already removed
		if(avl_p_isthere(&Implem->H->NODES_ALL, node)==false) continue;
		// Launch simplification
		Hier_Simp_Level(Implem, node, &result);
	}
	freechain(list_nodes);

	unsigned total_done = 0;
	total_done += result.cutlink;
	total_done += result.deadnodes_del;
	total_done += result.call_empty;
	total_done += result.call_inline;
	total_done += result.jump_del;
	total_done += result.label_del;
	total_done += result.label_merge;
	total_done += result.state_empty_del;
	total_done += result.bb_empty_del;
	total_done += result.bb_merge;
	total_done += result.loop_noiter;
	total_done += result.for_iter_0;
	total_done += result.switch_empty;
	total_done += result.switch_is_def;
	total_done += result.switch_is_case;
	total_done += result.switch_case_del;
	total_done += result.misc;

	if(total_done>0) {
		printf("INFO Simplifications done :");
		if(result.cutlink!=0)          printf(" CutLink=%u", result.cutlink);
		if(result.deadnodes_del!=0)    printf(" DeadNodes=%u", result.deadnodes_del);
		if(result.call_empty!=0)       printf(" CallEmpty=%u", result.call_empty);
		if(result.call_inline!=0)      printf(" CallInline=%u", result.call_inline);
		if(result.jump_del!=0)         printf(" JumpDel=%u", result.jump_del);
		if(result.label_del!=0)        printf(" LabelDel=%u", result.label_del);
		if(result.label_merge!=0)      printf(" LabelMerge=%u", result.label_merge);
		if(result.state_empty_del!=0)  printf(" StateEmpty=%u", result.state_empty_del);
		if(result.bb_empty_del!=0)     printf(" BbEmpty=%u", result.bb_empty_del);
		if(result.bb_merge!=0)         printf(" BbMerge=%u", result.bb_merge);
		if(result.loop_noiter!=0)      printf(" LoopNoIter=%u", result.loop_noiter);
		if(result.for_iter_0!=0)       printf(" ForIter0=%u", result.for_iter_0);
		if(result.switch_empty!=0)     printf(" SwitchEmpty=%u", result.switch_empty);
		if(result.switch_is_def!=0)    printf(" SwitchIsDefault=%u", result.switch_is_def);
		if(result.switch_is_case!=0)   printf(" SwitchIsCase=%u", result.switch_is_case);
		if(result.switch_case_del!=0)  printf(" SwitchCaseDel=%u", result.switch_case_del);
		if(result.misc!=0)             printf(" Misc=%u", result.misc);
		printf("\n");
	}

	// For debug
	//Hier_CheckAccessToNodes(H);

	return total_done;
}



//=====================================================================
// Main simplification functions
//=====================================================================

void Hier_Update_SimpStruct(implem_t* Implem) {
	// FIXME this should be inside a loop
	Hier_SimplifyExpr(Implem->H);
	Hier_Simp_Actions(Implem);
	Hier_Simp_Nodes(Implem);

	Hier_SimplifyExpr(Implem->H);
	Hier_Update_Control(Implem);
}

int Hier_Update_All(implem_t* Implem) {
	unsigned count = 0;

	do {
		unsigned local_count = 0;
		int z;

		#ifndef NDEBUG  // Debug: Check integrity of the Hier graph
		Hier_Check_Integrity(Implem->H);
		#endif

		// And the real work
		local_count += Hier_SimplifyExpr(Implem->H);

		z = Hier_Simp_Actions(Implem);
		if(z>0) local_count += z;

		Hier_Update_Control(Implem);
		z = Hier_Simp_Nodes(Implem);
		if(z>0) local_count += z;

		count += local_count;
		if(local_count == 0) break;
	} while(1);

	Hier_Timing_Update(Implem, false);

	return count;
}

void Hier_Update_NoAct(implem_t* Implem) {

	do {

		#ifndef NDEBUG  // Debug: Check integrity of the Hier graph
		Hier_Check_Integrity(Implem->H);
		#endif

		bool redo = false;
		int z;

		Hier_Update_Control(Implem);
		z = Hier_Simp_Nodes(Implem);
		if(z>0) redo = true;

		if(redo==false) break;
	}while(1);

	Hier_Timing_Update(Implem, false);
}

int Hier_Simp_RemoveUnusedLabels_opt(implem_t* Implem, const char* name, bool only_unnamed) {
	chain_list* list_nodes = NULL;
	unsigned count = 0;
	// List the nodes to delete
	avl_p_foreach(&Implem->H->NODES[HIERARCHY_LABEL], scan) {
		hier_node_t* node = scan->data;
		hier_label_t* label_data = &node->NODE.LABEL;
		if(label_data->jumps!=NULL) continue;
		// Only named labels may be kept
		if(only_unnamed==true && label_data->name!=NULL) continue;
		if(only_unnamed==false && name!=NULL && label_data->name==name) continue;
		// This node will be deleted
		list_nodes = addchain(list_nodes, node);
	}
	// Remove the nodes
	foreach(list_nodes, scan) {
		hier_node_t* node = scan->DATA;
		Hier_ReplaceNode_Free(Implem->H, node, NULL);
		count ++;
	}
	// Clean
	freechain(list_nodes);
	return count;
}


