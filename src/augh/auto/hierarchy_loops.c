
// Contents of this file: manipulation of loops and particularly FOR loops

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <assert.h>
#include <inttypes.h>
#include <math.h>

#include "auto.h"
#include "hierarchy.h"
#include "hierarchy_symuse.h"
#include "schedule.h"
#include "linelist.h"
#include "techno.h"
#include "map.h"
#include "../hvex/hvex_misc.h"

extern bool overbigloop_unroll_en;



//=====================================================================
// Detection of FOR loops
//=====================================================================

// FIXME handle when the VEX (value start, end, or step) is too long for an int64_t, however rare that case would occur
// FIXME the SIGNED/UNSIGNED tests are not handled. This could cause overflows.

int Hier_For_DetectIterator_PostLoadStyle(hier_t* H, hier_node_t* node, hvex_model_t* test_op) {
	hier_loop_t* node_loop = &node->NODE.LOOP;

	// Separately handle the EQ operation
	if(test_op==HVEX_EQ) {
		if(node_loop->iter.val_init == node_loop->iter.val_end) {
			node_loop->iter.iterations_nb = 1;
			node_loop->iter.testfalse_nb = 1;
			node_loop->iter.inc_nb = 1;
			node_loop->flags |= HIER_FOR_ITER_NB;
		}
		else node_loop->flags |= HIER_LOOP_NEVER;
		return 0;
	}

	int64_t iter_span = node_loop->iter.val_end - node_loop->iter.val_init;
	int64_t iter_step = node_loop->iter.val_step;

	// Convert to positive values to calculate the number of iterations
	uint64_t iter_span_abs = iter_span;
	uint64_t iter_step_abs = iter_step;
	if(iter_span < 0) iter_span_abs = -iter_span;
	if(iter_step < 0) iter_step_abs = -iter_step;

	// Separately handle the NE operation
	if(test_op==HVEX_NE) {
		// Separately handle the case where the loop does not iterate
		if(node_loop->iter.val_init == node_loop->iter.val_end) {
			node_loop->flags |= HIER_FOR_OVERFLOW;
			return 0;
		}
		// From here the end value is different than the init value
		// Detect obvious case when the iterator can overflow: check signedness
		if((iter_span > 0) != (iter_step > 0)) {
			node_loop->flags |= HIER_FOR_OVERFLOW;
			return 0;
		}
		// Examples:
		//   for(i=0; i!=8; i++)  => 8 iterations = end-init
		//   for(i=0; i!=8; i+=2) => i=0,2,4,6 => 4 iterations = (end-init)/step
		// Detect when the iterator can miss the end value (note: here we know step != 0)
		if(iter_span_abs % iter_step_abs != 0) {
			node_loop->flags |= HIER_FOR_OVERFLOW;
			return 0;
		}
		// Calculate the number of iterations
		node_loop->iter.iterations_nb = iter_span_abs / iter_step_abs;
		node_loop->flags |= HIER_FOR_ITER_NB;
		return 0;
	}

	// From here the operation is an inequality

	// FIXME TODO Some cases are not handled. Here mainly the 'right' cases are handled.
	//   Missing detection of cases of infinite loop and cases of overflow
	//   Handle signedness of iterator and literal values
	#if 0
	if(IsOperLower(node_loop->iter.test_operation)!=0) {
		if(iter_span<0 && iter_step>0) { loop_uses_overflow=1; goto END_ITER_NB; }
		if(iter_span>0 && iter_step<0) { loop_uses_overflow=1; goto END_ITER_NB; }
		if(iter_span==0) {
			if(IsOperIneqOrEq(node_loop->iter.test_operation)==0) { loop_not_executed=1; goto END_ITER_NB; }
			// Here the comparison accepts equality
			if(iter_step<0) { loop_uses_overflow=1; goto END_ITER_NB; }
		}
	}
	if(IsOperUpper(node_loop->iter.test_operation)!=0) {
		if(iter_span<0 && iter_step>0) { loop_uses_overflow=1; goto END_ITER_NB; }
		if(iter_span>0 && iter_step<0) { loop_uses_overflow=1; goto END_ITER_NB; }
		if(iter_span==0) {
			if(IsOperIneqOrEq(node_loop->iter.test_operation)==0) { loop_not_executed=1; goto END_ITER_NB; }
			// Here the comparison accepts equality
			if(iter_step<0) { loop_uses_overflow=1; goto END_ITER_NB; }
		}
	}
	#endif

	if(hvex_model_is_ineq_or_eq(test_op)==true) iter_span_abs++;
	node_loop->iter.iterations_nb = iter_span_abs / iter_step_abs;
	if(node_loop->iter.iterations_nb*iter_step_abs < iter_span_abs) node_loop->iter.iterations_nb++;

	node_loop->iter.testfalse_nb = node_loop->iter.iterations_nb;
	node_loop->iter.inc_nb = node_loop->iter.iterations_nb;

	node_loop->flags |= HIER_FOR_ITER_NB;

	return 0;
}

int Hier_For_DetectIterator_OneBodyStyle(hier_t* H, hier_node_t* node, hvex_model_t* test_op) {
	hier_loop_t* node_loop = &node->NODE.LOOP;

	// FIXME For now, only handle when the INC is done after the test (same State)
	if(hier_loop_chkflag(node_loop->flags, HIER_FOR_TESTFIRST)==false) return __LINE__;

	// Separately handle the EQ operation
	if(test_op==HVEX_EQ) {
		if(node_loop->iter.val_init == node_loop->iter.val_end) {
			node_loop->iter.iterations_nb = 1;
			node_loop->iter.testfalse_nb = 1;
			node_loop->iter.inc_nb = 2;
			node_loop->flags |= HIER_FOR_ITER_NB;
		}
		else node_loop->flags |= HIER_LOOP_NEVER;
		return 0;
	}

	int64_t iter_span = node_loop->iter.val_end - node_loop->iter.val_init;
	int64_t iter_step = node_loop->iter.val_step;

	// Convert to positive values to calculate the number of iterations
	uint64_t iter_span_abs = iter_span;
	uint64_t iter_step_abs = iter_step;
	if(iter_span < 0) iter_span_abs = -iter_span;
	if(iter_step < 0) iter_step_abs = -iter_step;

	// Separately handle the NE operation
	if(test_op==HVEX_NE) {
		// Separately handle the case where the loop does not iterate
		if(node_loop->iter.val_init == node_loop->iter.val_end) {
			node_loop->flags |= HIER_FOR_OVERFLOW;
			return 0;
		}
		// From here the end value is different than init value
		// Detect obvious case when the iterator can overflow: check signedness
		if((iter_span > 0) != (iter_step > 0)) {
			node_loop->flags |= HIER_FOR_OVERFLOW;
			return 0;
		}
		// Examples (assume the test is done just before the inc):
		//   for(i=0; i!=8; i++)  => i=0,1,2,3,4,5,6,7,8 => 9 iterations = end-init+1
		//   for(i=0; i!=8; i+=2) => i=0,2,4,6,8 => 5 iterations = (end-init+step)/step
		// Detect when the iterator can miss the end value (note: here we know step != 0)
		if(iter_span_abs % iter_step_abs != 0) {
			node_loop->flags |= HIER_FOR_OVERFLOW;
			return 0;
		}
		// Calculate the number of iterations
		node_loop->iter.iterations_nb = (iter_span_abs + iter_step_abs) / iter_step_abs;
		node_loop->iter.testfalse_nb = node_loop->iter.iterations_nb - 1;
		node_loop->iter.inc_nb = node_loop->iter.iterations_nb;
		node_loop->flags |= HIER_FOR_ITER_NB;
		return 0;
	}

	// From here the operation is an inequality

	// FIXME TODO Some cases are not handled. Here mainly the 'right' cases are handled.
	//   Missing detection of cases of infinite loop and cases of overflow
	//   Handle signedness of iterator and literal values
	#if 0
	if(IsOperLower(node_loop->iter.test_operation)!=0) {
		if(iter_span<0 && iter_step>0) { loop_uses_overflow=1; goto END_ITER_NB; }
		if(iter_span>0 && iter_step<0) { loop_uses_overflow=1; goto END_ITER_NB; }
		if(iter_span==0) {
			if(IsOperIneqOrEq(node_loop->iter.test_operation)==0) { loop_not_executed=1; goto END_ITER_NB; }
			// Here the comparison accepts equality
			if(iter_step<0) { loop_uses_overflow=1; goto END_ITER_NB; }
		}
	}
	if(IsOperUpper(node_loop->iter.test_operation)!=0) {
		if(iter_span<0 && iter_step>0) { loop_uses_overflow=1; goto END_ITER_NB; }
		if(iter_span>0 && iter_step<0) { loop_uses_overflow=1; goto END_ITER_NB; }
		if(iter_span==0) {
			if(IsOperIneqOrEq(node_loop->iter.test_operation)==0) { loop_not_executed=1; goto END_ITER_NB; }
			// Here the comparison accepts equality
			if(iter_step<0) { loop_uses_overflow=1; goto END_ITER_NB; }
		}
	}
	#endif

	// Similar to NE operation. Take care when the last iterator value is not a multiple of the step.
	// Examples (assume the test is done just before the inc):
	//   for(i=0; i<8; i++)   => same as NE operation: iterations = (end-init+step)/step
	//   for(i=0; i<=8; i++)  => i=0,1,2,3,4,5,6,7,8,9 => 10 iterations = end-init+1+1
	//   for(i=0; i<=8; i+=2) => i=0,2,4,6,8,10 => 6 iterations
	//   for(i=0; i<=2; i+=8) => i=0 => 1 iterations=

	if(hvex_model_is_ineq_or_eq(test_op)==true) iter_span_abs++;

	node_loop->iter.testfalse_nb = (iter_span_abs + iter_step_abs - 1) / iter_step_abs;
	node_loop->iter.iterations_nb = node_loop->iter.testfalse_nb + 1;
	node_loop->iter.inc_nb = node_loop->iter.iterations_nb;

	node_loop->flags |= HIER_FOR_ITER_NB;

	return 0;
}

// Detect the iterator of the FOR loops.
// The test info is assumed to be already available, it is not changed.
int Hier_For_DetectIterator(hier_t* H, hier_node_t* node) {
	hier_loop_t* node_loop = &node->NODE.LOOP;
	hier_testinfo_t* testinfo = node_loop->testinfo;

	// Free the iterator data
	TestDoNull(node_loop->iter.vex_iterator, hvex_free);
	TestDoNull(node_loop->iter.vex_end, hvex_free);
	node_loop->flags = 0;
	memset(&node_loop->iter, 0, sizeof(node_loop->iter));

	// Skip known infinite loops
	if(testinfo->cond==NULL) return 0;

	// Various checks
	if(testinfo->is_simple==false) return __LINE__;
	if(testinfo->sym_is_sig==false) return __LINE__;
	if(testinfo->sig_asg==NULL) return __LINE__;
	if(testinfo->asg_is_literal==true) return __LINE__;

	if(node_loop->body_after==NULL && node_loop->body_before==NULL) {
		//printf("WARNING %s:%d : Empty loop.\n", __FILE__, __LINE__);
		//LineList_Print_be(node->source_lines, "  Source lines : ", "\n");
		return __LINE__;
	}

	// No dubious jumps/labels
	if(Hier_Level_HasDubiousJumpLabel_Recurs(node_loop->body_after)==true)  { node_loop->flags |= HIER_FOR_HASJUMPS; return __LINE__; }
	if(Hier_Level_HasDubiousJumpLabel_Recurs(node_loop->body_before)==true) { node_loop->flags |= HIER_FOR_HASJUMPS; return __LINE__; }

	// Detect the 2 FOR loop styles:
	// - Post-loading style, with only the test ASG in body_after and the rest in body_before
	// - one-body style, with everything in body_after and no body_before

	hier_node_t* body_where_inc = NULL;

	// Handle the post-loading style
	if(node_loop->body_after!=NULL && node_loop->body_before!=NULL) {
		// Ensure the test Action is the only Action of the loop body
		hier_node_t* nodeBB = Hier_IsLevelOneType_GetSkipLabels(node_loop->body_after, HIERARCHY_BB);
		if(nodeBB==NULL) return __LINE__;
		chain_list* list = Hier_BB_GetAllAct(nodeBB);
		if(list==NULL) return __LINE__;
		hier_action_t* action = list->DATA;
		if(ChainList_Count(list)>1) { freechain(list); return __LINE__; }
		freechain(list);
		if(action!=testinfo->sig_asg) return __LINE__;
		// The INC Action must be in the body_before
		body_where_inc = node_loop->body_before;
	}
	// Handle the compact style, with everything in the body_after
	else if(node_loop->body_after!=NULL && node_loop->body_before==NULL) {
		// The INC Action must be in the body_before
		body_where_inc = node_loop->body_after;
		node_loop->flags |= HIER_FOR_ONEBODY;
	}
	else {
		return __LINE__;
	}

	// Get the test operation
	hvex_t* test_expr = hvex_asg_get_expr(testinfo->sig_asg->expr);

	// We only handle comparison  operations for the test: < > <= >= != =
	hvex_model_t* test_op = test_expr->model;
	if(hvex_model_is_ineq(test_op)==false && test_op!=HVEX_EQ && test_op!=HVEX_NE) return __LINE__;

	// Handle when there is a NOT in the Signal test
	if(testinfo->oper_not==true) {
		assert(test_op->model_not != NULL);
		test_op = test_op->model_not;
	}

	// Get the iterator. There must be only one Reg operand for the test:
	// Example : reg1 < expr
	if(test_expr->operands->model==HVEX_VECTOR && test_expr->operands->next->model!=HVEX_VECTOR) {
		node_loop->iter.vex_iterator = hvex_dup(test_expr->operands);
		node_loop->iter.vex_end      = hvex_dup(test_expr->operands->next);
	}
	else if(test_expr->operands->model!=HVEX_VECTOR && test_expr->operands->next->model==HVEX_VECTOR) {
		node_loop->iter.vex_iterator = hvex_dup(test_expr->operands->next);
		node_loop->iter.vex_end      = hvex_dup(test_expr->operands);
		// Reverse the test operation
		if(hvex_model_is_ineq(test_op)==true) test_op = hvex_model_reverse_ineq(test_op);
		assert(test_op!=NULL);
	}
	else {
		// We can't detect an iterator from the test operation
		return __LINE__;
	}

	// Get the iterator name
	node_loop->iter.iterator_name = hvex_vec_getname(node_loop->iter.vex_iterator);

	// Get the end expression
	if(node_loop->iter.vex_end->model==HVEX_LITERAL) {
		int vexval = hvex_lit_evalint(node_loop->iter.vex_end);  // FIXME check range
		node_loop->iter.val_end = vexval;
		node_loop->flags |= HIER_FOR_END_LIT;
	}

	// Get the initial value by searching backwards in the States
	node_loop->iter.init_act = Hier_GetAsg_BeforeNode(H, node, node_loop->iter.iterator_name);
	if(node_loop->iter.init_act!=NULL) {
		// Forbid wired conditions
		if(hvex_asg_get_cond(node_loop->iter.init_act->expr)!=NULL) node_loop->iter.init_act = NULL;
	}
	// Try to get the Init literal value
	if(node_loop->iter.init_act!=NULL) {
		hvex_t* dest_expr = hvex_asg_get_dest(node_loop->iter.init_act->expr);
		hvex_t* src_expr = hvex_asg_get_expr(node_loop->iter.init_act->expr);
		// Check indexes
		if(hvex_check_inclusion(dest_expr, node_loop->iter.vex_iterator)==true) {
			if(hvex_check_sameidx(dest_expr, node_loop->iter.vex_iterator)==true) {
				// Directly get the literal value
				if(src_expr->model==HVEX_LITERAL) {
					int vexval = hvex_lit_evalint(src_expr);  // FIXME check range
					node_loop->iter.val_init = vexval;
					node_loop->flags |= HIER_FOR_INIT_LIT;
				}
			}
			else {
				// Work on a copy of the expression
				hvex_t* dupexpr = hvex_dup(src_expr);
				unsigned crop_left  = dest_expr->left - node_loop->iter.vex_iterator->left;
				unsigned crop_right = node_loop->iter.vex_iterator->right - dest_expr->right;
				hvex_croprel(dupexpr, crop_left, crop_right);
				// Try to get the literal value
				if(dupexpr->model==HVEX_LITERAL) {
					int vexval = hvex_lit_evalint(dupexpr);  // FIXME check range
					node_loop->iter.val_init = vexval;
					node_loop->flags |= HIER_FOR_INIT_LIT;
				}
				// Clean
				hvex_free(dupexpr);
			}
		}
		else {
			// Forget that init Act, indexes are wrong
			node_loop->iter.init_act = NULL;
		}
	}  // Init Act found

	// Get the increment Action, ensure it is written only once.
	node_loop->iter.inc_act = NULL;
	chain_list* list_asg_actions = Hier_Node_GetAct_SymIsDest(node, node_loop->iter.iterator_name);
	if(ChainList_Count(list_asg_actions)==1) {
		node_loop->iter.inc_act = list_asg_actions->DATA;
	}
	if(list_asg_actions!=NULL) freechain(list_asg_actions);
	if(node_loop->iter.inc_act==NULL) return __LINE__;
	// Forbid wired conditions
	if(hvex_asg_get_cond(node_loop->iter.inc_act->expr)!=NULL) {
		node_loop->iter.inc_act = NULL;
		return __LINE__;
	}

	// Ensure the INC Action is not in a nested condition, and that it is in the expected Loop body.
	// Get the parent BB
	hier_node_t* scanNode = node_loop->iter.inc_act->node;
	while(scanNode->PREV!=NULL) scanNode = scanNode->PREV;
	scanNode = scanNode->PARENT;
	if(scanNode==NULL) return __LINE__;
	if(scanNode->TYPE!=HIERARCHY_BB) return __LINE__;
	// Traverse the PREV nodes, check that the last PREV node is the body
	while(scanNode->PREV!=NULL) scanNode = scanNode->PREV;
	if(scanNode!=body_where_inc) return __LINE__;

	// Get the Increment operation: only handle ADD and SUB.
	hvex_t* incExpr = hvex_asg_get_expr(node_loop->iter.inc_act->expr);
	hvex_model_t* inc_op = incExpr->model;
	if(inc_op!=HVEX_ADD && inc_op!=HVEX_SUB) return __LINE__;
	// Check indexes
	if(incExpr->left+1 != node_loop->iter.vex_iterator->width || incExpr->right != 0) return __LINE__;

	// Get the step value, ADD style.
	if(hvex_cmp(incExpr->operands, node_loop->iter.vex_iterator)!=0) return __LINE__;
	if(incExpr->operands->next->model!=HVEX_LITERAL) return __LINE__;
	int vexval = hvex_lit_evalint(incExpr->operands->next);  // FIXME Missing literal range check
	node_loop->iter.val_step = vexval;
	if(inc_op==HVEX_SUB) node_loop->iter.val_step = -node_loop->iter.val_step;  // FIXME Missing literal range check

	// The iterator and increment for FOR structure is recognized
	node_loop->flags |= HIER_FOR_INC_LIT;

	//dbgprintf("Found iterator: "); hvex_print_bn(node_loop->iter.vex_iterator);

	// In case the critical values are not known...
	if(hier_for_chkinitlit(node_loop->flags)==0 || hier_for_chkendlit(node_loop->flags)==0) {
		return __LINE__;
	}

	// Check whether the test is done after or before the INC Action
	// Just check if the test Action is in the same State as the INC Action
	if(node_loop->iter.inc_act->node == node_loop->testinfo->sig_asg->node) node_loop->flags |= HIER_FOR_TESTFIRST;
	else {
		// For one-body style, ensure that the INC Action is in the State just before the TEST Action
		// And that there is no other Action in the State of the TEST Action
		// FIXME There may be other Actions in between and with the INC or TEST Actions, as long as they don't read the iterator
		if(hier_loop_chkflag(node_loop->flags, HIER_FOR_ONEBODY)!=0) {
			if(node_loop->iter.inc_act->node->NEXT != node_loop->testinfo->sig_asg->node) return __LINE__;
			if(ChainList_Count(node_loop->testinfo->sig_asg->node->NODE.STATE.actions)!=1) return __LINE__;
		}
	}

	// Compute the number of iterations

	// FIXME for now, signed inequalities are not handled
	if(hvex_model_is_ineq_signed(test_op)==true) return __LINE__;

	// Separately handle when the step is zero
	// No need to check the flags ONEBODY and TESTFIRST here
	if(node_loop->iter.val_step==0) {

		if(node_loop->iter.val_init < node_loop->iter.val_end) {
			if(hvex_model_is_ineq_lower(test_op)==true) node_loop->flags |= HIER_LOOP_ALWAYS;
			else node_loop->flags |= HIER_LOOP_NEVER;
		}
		else if(node_loop->iter.val_init > node_loop->iter.val_end) {
			if(hvex_model_is_ineq_upper(test_op)==true) node_loop->flags |= HIER_LOOP_ALWAYS;
			else node_loop->flags |= HIER_LOOP_NEVER;
		}
		else {  // Hande when init and end values are equal
			if(hvex_model_is_ineq_or_eq(test_op)==true) node_loop->flags |= HIER_LOOP_ALWAYS;
			else node_loop->flags |= HIER_LOOP_NEVER;
		}

		if(test_op==HVEX_EQ) {
			if(node_loop->iter.val_init == node_loop->iter.val_end) node_loop->flags |= HIER_LOOP_ALWAYS;
			else node_loop->flags |= HIER_LOOP_NEVER;
		}
		else if(test_op==HVEX_NE) {
			if(node_loop->iter.val_init != node_loop->iter.val_end) node_loop->flags |= HIER_LOOP_ALWAYS;
			else node_loop->flags |= HIER_LOOP_NEVER;
		}

		goto END_ITER_NB;
	}

	int z = 0;
	if(hier_loop_chkflag(node_loop->flags, HIER_FOR_ONEBODY)==0) {
		z = Hier_For_DetectIterator_PostLoadStyle(H, node, test_op);
	}
	else {
		z = Hier_For_DetectIterator_OneBodyStyle(H, node, test_op);
	}
	if(z!=0) return z;

	END_ITER_NB:

	if(hier_loop_chknever(node_loop->flags)==true) {
		node_loop->iter.testfalse_nb = 0;
		if(hier_loop_chkflag(node_loop->flags, HIER_FOR_ONEBODY)==0) {
			node_loop->iter.iterations_nb = 1;
			node_loop->iter.inc_nb = 1;
		}
		else {
			node_loop->iter.iterations_nb = 0;
			node_loop->iter.inc_nb = 0;
		}
		return __LINE__;
	}

	if(hier_loop_chkalways(node_loop->flags)==true) return __LINE__;

	if(hier_for_chkoverflow(node_loop->flags)==true) {
		printf("WARNING %s : The loop uses overflow to terminate.\n", __func__);
		node_loop->iter.iterations_nb = -1;
		return __LINE__;
	}

	// Only here we can consider the loop is indeed a clean FOR loop
	node_loop->flags |= HIER_FOR_OK;

	#if 0
	dbgprintf("Iterator = "); hvex_print_bn(node_loop->iter.vex_iterator);
	printf(" start=%"PRId64" end=%"PRId64" step=%"PRId64" iterations=%"PRId64"\n",
		node_loop->iter.val_init, node_loop->iter.val_end, node_loop->iter.val_step, node_loop->iter.iterations_nb
	);
	#endif

	return 0;
}

void Hier_Loop_UpdControl(implem_t* Implem, hier_node_t* node) {
	hier_loop_t* loop_data = &node->NODE.LOOP;

	loop_data->flags = 0;
	loop_data->iter.error_code = 0;

	// Infinite loop
	if(loop_data->testinfo->cond==NULL) {
		loop_data->flags |= HIER_LOOP_ALWAYS;
		return;
	}

	Hier_TestInfo_Loop(Implem, node);
	if(loop_data->testinfo->is_simple==false) return;

	// Check whether the test result is known
	int val = Hier_TestInfo_GetVal(loop_data->testinfo);
	// This loop never loops
	if(val==false) {
		loop_data->flags |= HIER_LOOP_NEVER;
		return;
	}
	// Infinite loop
	if(val==true){
		loop_data->flags |= HIER_LOOP_ALWAYS;
		TestDoNull(loop_data->testinfo->cond, hvex_free);
		return;
	}

	// Checking whether this loop is a FOR loop

	// Note: if this returns zero, it is a FOR loop. Otherwise it is an error code.
	loop_data->iter.error_code = Hier_For_DetectIterator(Implem->H, node);
}

int Hier_Loops_DetectControl(implem_t* Implem) {
	unsigned count_never = 0;
	unsigned count_always = 0;
	unsigned count_for = 0;
	unsigned count_all = 0;

	if(actsimp_verbose==true) {
		printf("INFO: Getting test information of the loops...\n");
	}

	avl_p_foreach(&Implem->H->NODES[HIERARCHY_LOOP], scanNode) {
		hier_node_t* node = scanNode->data;
		count_all++;

		Hier_Loop_UpdControl(Implem, node);

		hier_loop_t* loop_data = &node->NODE.LOOP;
		if(hier_loop_chknever(loop_data->flags)) count_never ++;
		if(hier_loop_chkalways(loop_data->flags)) count_always ++;
		if(hier_loop_chkflags_and(loop_data->flags, HIER_FOR_OK | HIER_FOR_ITER_NB)) count_for ++;
	}

	if(actsimp_verbose==true) {
		printf("INFO: Among %u loops, %u never loop, %u are infinite, %u are clean FOR loops.\n",
			count_all, count_never, count_always, count_for
		);
	}

	return 0;
}



//=====================================================================
// Unrolling FOR loops
//=====================================================================

// Modify a Loop to partially unroll it (in-place unroll)
int Hier_For_PartialUnroll(implem_t* Implem, hier_node_t* node, int factor, int flags) {
	if(node->TYPE!=HIERARCHY_LOOP) return __LINE__;
	hier_loop_t* node_loop = &node->NODE.LOOP;

	int unroll_style_literal = 1;

	if(hier_for_chkiternb(node_loop->flags)) {
		// Check the iteration number
		if(hier_for_chkinitlit(node_loop->flags)==0) return __LINE__;
		if(hier_for_chkendlit(node_loop->flags)==0) return __LINE__;
		if(node_loop->iter.val_step!=1) return __LINE__;
		if(node_loop->iter.iterations_nb<4) return __LINE__;
		// Check the factor value
		if(!uint32_ispower2(factor) || factor<2) return __LINE__;
		if(factor >= node_loop->iter.iterations_nb) return __LINE__;  // This is a full unroll, the loop would disappear.
		if(node_loop->iter.val_init % factor != 0) return __LINE__;
		if(node_loop->iter.iterations_nb % factor != 0) return __LINE__;
	}
	else {
		// Check the iteration number
		if(node_loop->iter.iterator_name==NULL) return __LINE__;
		if(node_loop->iter.inc_act==NULL) return __LINE__;
		if(node_loop->user.iter_nb_power2_given==false) return __LINE__;
		if(node_loop->user.iter_nb_power2<2) return __LINE__;
		// Check the factor value
		if(!uint32_ispower2(factor) || factor<2) return __LINE__;
		if(factor > node_loop->user.iter_nb_power2) return __LINE__;
		if(node_loop->user.iter_nb_power2 % factor != 0) return __LINE__;
		// Set the style flags for the rest of the process
		unroll_style_literal = 0;
	}

	// The iterator will be replaced by, for example: iterator(7 downto 3) & "011"
	// Find the width of the value to append to the iterator.
	int value_width = uint_bitsnb(factor-1);

	// Select the right loop body according to the loop style
	hier_node_t* loop_body = NULL;
	if(hier_for_chkonebody(node_loop->flags)==false) loop_body = node_loop->body_before;
	else loop_body = node_loop->body_after;

	// Paranoia
	if(loop_body==NULL) return __LINE__;

	// Duplicate the body_before child.
	// The body_after is not duplicated because it is assumed to contain only the Test action.

	hier_node_t* bodies[factor];
	bodies[0] = loop_body;

	// Duplicate the body_before
	for(unsigned i=1; i<factor; i++) {
		bodies[i] = Hier_Level_Dup(loop_body);
		Hier_Lists_AddLevel(Implem->H, bodies[i]);
	}

	// Detect the last test Action (only for one-body style)
	if(hier_for_chkonebody(node_loop->flags)==true) {
		chain_list* list = Hier_Level_GetAct_SymIsDest(bodies[factor-1], node_loop->testinfo->sym_name);
		assert(ChainList_Count(list)==1);
		node_loop->testinfo->sig_asg = list->DATA;
		freechain(list);
	}

	// Remove the test Actions in the first iterations (only for one-body style)
	if(hier_for_chkonebody(node_loop->flags)==true) {
		for(unsigned i=0; i<factor-1; i++) {
			chain_list* list = Hier_Level_GetAct_SymIsDest(bodies[i], node_loop->testinfo->sym_name);
			foreach(list, scanAct) {
				hier_action_t* action = list->DATA;
				Hier_Action_FreeFull(action);
			}
			freechain(list);
		}
	}

	// Detect the last increment Action
	chain_list* list = Hier_Level_GetAct_SymIsDest(bodies[factor-1], node_loop->iter.iterator_name);
	assert(ChainList_Count(list)==1);
	node_loop->iter.inc_act = list->DATA;
	freechain(list);

	// Remove the increment Actions in the first iterations
	for(unsigned i=0; i<factor-1; i++) {
		// Get the action where the symbol is written
		chain_list* list = Hier_Level_GetAct_SymIsDest(bodies[i], node_loop->iter.iterator_name);
		assert(ChainList_Count(list)==1);
		hier_action_t* action = list->DATA;
		freechain(list);
		// Remove the action
		Hier_Action_FreeFull(action);
	}

	// Replace the occurrences of the iterator
	for(unsigned i=0; i<factor; i++) {
		// Replace the iterator in the bodies
		hvex_t* vex_replace = hvex_newlit_int(i, value_width, 0);
		hvex_t* vex_iter_part = hvex_dup(node_loop->iter.vex_iterator);
		vex_iter_part->width = value_width;
		vex_iter_part->left = vex_iter_part->left + value_width - 1;

		// List all the occurrences of the iterator in the body
		//printf("DEBUG %s : Searching for occurrences of the iterator name '%s'.\n", __func__, GetVexAtomValue(node_loop->iter.iterator));
		chain_list* list = Hier_Level_GetSymbolOccur(bodies[i], node_loop->iter.iterator_name);

		// Replace occurrences
		//printf("DEBUG %s : found %u occurrences of the iterator.\n", __func__, ChainList_Count(list));
		foreach(list, scan) {
			hvex_t* oldvex = scan->DATA;
			// Don't change TEST and INC Actions
			hvex_t* topvex = hvex_top_parent(oldvex);
			if(topvex==node_loop->testinfo->sig_asg->expr) continue;
			if(topvex==node_loop->iter.inc_act->expr) continue;
			// Perform replacement
			hvex_ReplaceSubExpr_FromPrevExpr(oldvex, vex_iter_part, vex_replace);
		}  // Scan all occurrences of the iterator

		// Clean
		freechain(list);
		hvex_free(vex_replace);
		hvex_free(vex_iter_part);
	}

	// Then link the new bodies.
	// Remind: the end of the initial BODY in already disconnected.

	// Link the bodies together, they form a new level
	for(unsigned i=0; i<factor-1; i++) {
		hier_node_t* lastnode = Hier_Level_GetLastNode(bodies[i]);
		hier_node_t* nextnode = bodies[i+1];
		lastnode->NEXT = nextnode;
		nextnode->PREV = lastnode;
	}

	// Change the INC Action => modify the step
	hvex_t* oldvex_expr = hvex_asg_get_expr(node_loop->iter.inc_act->expr);

	if(unroll_style_literal==0) {
		// FIXME The way the step expression is obtained is ugly and very fragile
		// Assume the INC Action is an ADD of the form: iterator + step
		if(oldvex_expr->model!=HVEX_ADD) {
			errprintfa("Increment operation model '%s' is not handled. Expected an addition.\n", oldvex_expr->model->name);
		}
		hvex_t* oldvex_step = NULL;
		if(hvex_cmp(oldvex_expr->operands, node_loop->iter.vex_iterator) == 0) oldvex_step = oldvex_expr->operands->next;
		else if(hvex_cmp(oldvex_expr->operands->next, node_loop->iter.vex_iterator) == 0) oldvex_step = oldvex_expr->operands;
		else {
			errprintfa("Increment not recognized.\n");
		}

		hvex_t* vex_step = hvex_newmodel_op2(HVEX_CONCAT, oldvex_step->width + value_width,
			hvex_dup(oldvex_step), hvex_newlit_repeat('0', value_width)
		);
		if(vex_step->width > node_loop->iter.vex_iterator->width) {
			hvex_croprel(vex_step, vex_step->width - node_loop->iter.vex_iterator->width, 0);
		}

		hvex_t* vex_expr = hvex_newmodel_op2(HVEX_ADD, node_loop->iter.vex_iterator->width,
			hvex_dup(node_loop->iter.vex_iterator), vex_step
		);

		hvex_replace_free(oldvex_expr, vex_expr);

		// Change the annotation about number of iterations, if any
		node_loop->user.iter_nb_power2 /= factor;
	}
	else {
		hvex_t* vex_expr = hvex_newmodel_op2(HVEX_ADD, node_loop->iter.vex_iterator->width,
			hvex_dup(node_loop->iter.vex_iterator),
			hvex_newlit_int(node_loop->iter.val_step * factor, node_loop->iter.vex_iterator->width, false)
		);
		hvex_replace_free(oldvex_expr, vex_expr);
	}

	// Simplify all VEX expressions in the new body.
	// In particular, the new literal concatenated with the iterator, can often be merged with other literals.
	// If not done, scalar replacement of arrays and rom inlining can be impossible.
	Hier_Level_SimplifyExpr(bodies[0]);

	// And update the control flow
	Hier_Update_Control(Implem);

	return 0;
}

// Return a fully unrolled version of the FOR loop
hier_node_t* Hier_For_GetFullUnroll(hier_t* H, hier_node_t* node, int flags) {
	if(node->TYPE!=HIERARCHY_LOOP) return NULL;
	hier_loop_t* node_loop = &node->NODE.LOOP;
	if(hier_for_chkok(node_loop->flags)==0) return NULL;

	if(hier_for_chkiternb(node_loop->flags)==0) return NULL;
	if(node_loop->iter.iterations_nb<1) return NULL;

	// Select the right loop body according to the loop style
	hier_node_t* loop_body = NULL;
	if(hier_for_chkonebody(node_loop->flags)==false) loop_body = node_loop->body_before;
	else loop_body = node_loop->body_after;

	// Paranoia
	if(loop_body==NULL) return NULL;

	// The list of new bodies
	hier_node_t* bodies[node_loop->iter.iterations_nb];

	// Create the new bodies
	for(unsigned i=0; i<node_loop->iter.iterations_nb; i++) {
		bodies[i] = Hier_Level_Dup(loop_body);
	}

	// Remove all test Actions (only for one-body style)
	if(hier_for_chkonebody(node_loop->flags)==true) {
		for(unsigned i=0; i<node_loop->iter.iterations_nb; i++) {
			chain_list* list = Hier_Level_GetAct_SymIsDest(bodies[i], node_loop->testinfo->sym_name);
			foreach(list, scanAct) {
				hier_action_t* action = list->DATA;
				Hier_Action_FreeFull(action);
			}
			freechain(list);
		}
	}

	// Remove all increment Actions
	// Or, keep them but add Action flags NOPROPAG
	for(unsigned i=0; i<node_loop->iter.iterations_nb; i++) {
		// Get the action where the iterator is written
		chain_list* list = Hier_Level_GetAct_SymIsDest(bodies[i], node_loop->iter.iterator_name);
		assert(ChainList_Count(list)==1);
		hier_action_t* action = list->DATA;
		freechain(list);
		if( (flags & HIER_LOOPUNROLL_KEEPITER) != 0 ) {
			// Add flags NOPROPAG
			action->flags |= HIER_ACT_NOPROPAG_FROM | HIER_ACT_NOPROPAG_TO;
		}
		else {
			// Remove the action
			Hier_Action_FreeFull(action);
		}
	}

	// Replace the iterator in the bodies
	if( (flags & HIER_LOOPUNROLL_KEEPITER) == 0 ) {
		for(unsigned i=0; i<node_loop->iter.iterations_nb; i++) {
			// List all the occurrences of the iterator in the body
			chain_list* list = Hier_Level_GetSymbolOccur(bodies[i], node_loop->iter.iterator_name);
			if(list==NULL) continue;
			// Replace the occurrences, while taking care of indexes
			hvex_t* vex_replace = hvex_newlit_int(
				node_loop->iter.val_init + i*node_loop->iter.val_step,
				node_loop->iter.vex_iterator->width, false
			);
			foreach(list, scan) {
				hvex_t* oldvex = scan->DATA;
				hvex_ReplaceSubExpr_FromPrevExpr(oldvex, node_loop->iter.vex_iterator, vex_replace);
			}
			hvex_free(vex_replace);
			freechain(list);
		}
	}

	// Link the new bodies
	for(unsigned i=0; i<node_loop->iter.iterations_nb-1; i++) {
		Hier_InsertLevelAfterLevel(bodies[i], bodies[i+1]);
	}

	// Add the last assignation of the iterator
	if( (flags & HIER_LOOPUNROLL_KEEPITER) == 0 ) {
		hier_build_bbstact_t bbstact;
		Hier_Build_BBStAct(&bbstact);

		// Set the Action Expr
		bbstact.action->expr = hvex_asg_make(
			hvex_dup(node_loop->iter.vex_iterator),
			NULL,
			hvex_newlit_int(
				node_loop->iter.val_init + node_loop->iter.inc_nb * node_loop->iter.val_step,
				node_loop->iter.vex_iterator->width, false
			),
			NULL
		);

		// Set source lines
		Hier_Nodes_SetLines(bbstact.bb, node->source_lines, true, true);

		// Link at the end of the new body
		Hier_InsertLevelAfterLevel(bodies[node_loop->iter.iterations_nb-1], bbstact.bb);

		#if 0  // For debug
		printf("\n");
		Hier_PrintNode(bodies[1]);
		printf("\n");
		#endif

		// Simplify all VEX expressions.
		Hier_Level_SimplifyExpr(bodies[0]);
	}

	#if 0  // For debug
	printf("\n");
	Hier_PrintNode(bodies[1]);
	printf("\n");
	#endif

	return bodies[0];
}

int Hier_For_ReplaceByFullUnroll(implem_t* Implem, hier_node_t* node, int flags) {
	// Generate the whole unrolled loop
	hier_node_t* new_level = Hier_For_GetFullUnroll(Implem->H, node, flags);
	if(new_level==NULL) return __LINE__;

	if( (flags & HIER_LOOPUNROLL_KEEPITER) != 0 ) {
		// Add flags NOPROPAG on the init Action
		hier_loop_t* node_loop = &node->NODE.LOOP;
		node_loop->iter.init_act->flags |= HIER_ACT_NOPROPAG_FROM | HIER_ACT_NOPROPAG_TO;
	}

	// Replace the node by the unrolled body
	Hier_ReplaceNode_Free(Implem->H, node, new_level);

	#if 0  // For debug
	printf("\n");
	Hier_Print(Implem->H);
	printf("\n");
	#endif

	Hier_Update_Control(Implem);
	return 0;
}


void Hier_For_PrintDetails(implem_t* Implem, hier_node_t* node, indent_t* indent) {
	hier_loop_t* node_loop = &node->NODE.LOOP;

	// List the lines of this FOR loop
	indentprint(indent);
	LineList_Print_be(node->source_lines, "Source lines : ", "\n");

	// Dsplay the timings

	if(node_loop->body_after!=NULL) {
		minmax_t t = Hier_Timing_GetLevelTime(node_loop->body_after);
		indentprint(indent);
		minmax_print(&t, "Body_after, execution time, internal : ", " (clock cycles)\n");
	}
	if(node_loop->body_before!=NULL) {
		minmax_t t = Hier_Timing_GetLevelTime(node_loop->body_before);
		indentprint(indent);
		minmax_print(&t, "Body_before, execution time, internal : ", " (clock cycles)\n");
	}

	indentprint(indent);
	minmax_print(&node->timing.global, "Execution time, global   : ", " (clock cycles)");
	printf(" (Factor : %g)\n", node->timing.factor);

	if(hier_for_chkok(node_loop->flags)) {
		if(hier_for_chkiternb(node_loop->flags)) {
			indentprintf(indent, "Start=%"PRId64" End=%"PRId64" Step=%"PRId64" Iterations=%"PRId64"\n",
				node_loop->iter.val_init, node_loop->iter.val_end, node_loop->iter.val_step, node_loop->iter.iterations_nb
			);
		}
		else {
			indentprint(indent);
			if(node_loop->iter.init_act!=NULL) {
				if(hier_for_chkinitlit(node_loop->flags)) printf("Start value : %"PRId64"\n", node_loop->iter.val_init);
				else { printf("Start VEX : "); hvex_print_bn(node_loop->iter.init_act->expr->operands->next); }
			}
			else printf("Start Action not found.\n");
			indentprint(indent);
			if(node_loop->iter.vex_end!=NULL) {
				if(hier_for_chkendlit(node_loop->flags)) printf("End value : %"PRId64"\n", node_loop->iter.val_end);
				else { printf("End VEX : "); hvex_print_bn(node_loop->iter.vex_end); }
			}
			else printf("End Action not found.\n");
			indentprintf(indent, "Step value : %"PRId64"\n", node_loop->iter.val_step);
		}
	}
	else {
		indentprintf(indent, "FOR iteration structure not recognized (error code %i).\n", node_loop->iter.error_code);
	}
	if(node_loop->user.iter_nb_given==true) {
		indentprintf(indent, "User-given iteration number : %g\n", node_loop->user.iter_nb);
	}
	if(node_loop->user.iter_nb_power2_given==true) {
		indentprintf(indent, "User-given power2 factor : %d\n", node_loop->user.iter_nb_power2);
	}
}



//=====================================================================
// Detection of feasible transformations
//=====================================================================

chain_list* Hier_For_FreedomDegrees_Get(implem_t* Implem) {
	chain_list* list_degrees = NULL;
	hier_t* H = Implem->H;
	if(H==NULL) return NULL;

	// Get global parameters...
	bool skip_part_unroll = ChainNum_IsDataHere(Implem->CORE.freedom_degrees_skipped_ops, FD_SKIP_UNROLL_PART);
	bool skip_full_unroll = ChainNum_IsDataHere(Implem->CORE.freedom_degrees_skipped_ops, FD_SKIP_UNROLL_FULL);

	avl_p_foreach(&H->NODES[HIERARCHY_LOOP], scan) {
		hier_node_t* node = scan->data;
		hier_loop_t* node_loop = &node->NODE.LOOP;
		if(hier_for_chkok(node_loop->flags)==0) continue;

		// Check the content of the bodies
		int reject = 0;

		chain_list* list_children = NULL;
		if(node_loop->body_after!=NULL) list_children = addchain(list_children, node_loop->body_after);
		if(node_loop->body_before!=NULL) list_children = addchain(list_children, node_loop->body_before);

		foreach(list_children, scanChild) {
			hier_node_t* child = scanChild->DATA;
			if(Hier_Level_IsThereTypeInside(child, HIERARCHY_SWITCH)==true) { reject = 1; break; }
			// The presence of the loops is trickier...
			chain_list* list = Hier_Level_ListInsideType(child, HIERARCHY_LOOP);
			if(list!=NULL) {
				foreach(list, scan) {
					hier_node_t* otherNode = scan->DATA;
					hier_loop_t* otherNode_loop = &otherNode->NODE.LOOP;
					if(hier_for_chkok(otherNode_loop->flags)) {
						if(hier_for_chkiternb(otherNode_loop->flags)) { reject = 1; break; }
						else if(otherNode_loop->user.iter_nb_power2_given==true) { reject = 1; break; }
					}
				}
				freechain(list);
				if(reject!=0) break;
			}
		}  // Scan the children
		freechain(list_children);

		if(reject!=0) continue;

		/* Resource cost estimation
				There is the code for 2 estimations.
				1) A full unroll costs the size of the FSM, a partial unroll is that cost of the full unroll divided by the ratio.
				2) The cost is the number of new FSM states, for FF and LUTs (because the FSM is one-hot)
		*/

		// Add a full unroll
		bool full_unroll_possible = true;
		if(skip_full_unroll==true) full_unroll_possible = false;
		if(node_loop->core.full_unroll_impossible!=0) full_unroll_possible = false;
		if(hier_for_chkiternb(node_loop->flags)==0) full_unroll_possible = false;

		// An arbitrary safeguard to avoid making FSM size explode
		if(full_unroll_possible==true && overbigloop_unroll_en==false) {
			if(node->timing.once.min > 500) {
				full_unroll_possible = false;
			}
		}

		if(full_unroll_possible==true) {
			freedom_degree_t* new_degree = freedom_degree_new();
			new_degree->Implem = Implem;

			new_degree->type = FD_LOOP_UNROLL;
			new_degree->target.hier.node = node;
			new_degree->target.hier.d.unroll_factor = 0;

			// Set the resource cost
			new_degree->resource_cost = NULL;

			hier_node_t* dupNode = NULL;
			implem_t* dupImplem = Implem->CORE.dup_implem;
			if(dupImplem!=NULL) {
				// Get the mirror LOOP node
				dupNode = Implem_Dup_Old2New(dupImplem, node);
				if(dupNode==NULL) {
					lineprintf("WARNING", "Could not get dupNode in the dupImplem\n");
				}
				else if(dupNode->TYPE!=HIERARCHY_LOOP) {
					lineprintf("WARNING", "Skipping non-Loop dupNode\n");
					dupNode = NULL;
				}
			}

			if(dupNode==NULL) {
				// Coarse estimation of the nb of new states
				unsigned nb_states = 0;

				void get_states_recurs(hier_node_t* node, bool do_level) {
					while(node!=NULL) {
						// Get the FSM state associatesd to this node
						if(node->TYPE==HIERARCHY_STATE) nb_states++;
						else if(node->TYPE==HIERARCHY_LOOP) {
							hier_loop_t* loop_data = &node->NODE.LOOP;
							if(loop_data->body_after==NULL && loop_data->body_before==NULL) nb_states++;
						}
						// Launch recursively
						foreach(node->CHILDREN, scanChild) get_states_recurs(scanChild->DATA, true);
						// Exit condition
						if(do_level==false) return;
						node = node->NEXT;
					}
				}

				// Only get the States associated to the main loop body
				if(hier_for_chkonebody(node_loop->flags)==false) get_states_recurs(node_loop->body_before, true);
				else get_states_recurs(node_loop->body_after, true);
				assert(nb_states>0);

				unsigned new_states = (node_loop->iter.iterations_nb - 1) * nb_states;
				new_degree->resource_cost = NULL;

				// Call the techno lib to get the estimated resource cost
				techno_t* techno = Implem->synth_target->techno;
				if(techno != NULL) {
					if(techno->sizeeval_fsmstates != NULL) {
						new_degree->resource_cost = techno->sizeeval_fsmstates(Implem, new_states);
					}
				}

			}
			else {
				// Get the number of states from the duplicated Implem
				hier_loop_t* duploop_data = &dupNode->NODE.LOOP;

				// Get the FSM states of the body, count the Actions
				map_netlist_data_t* dupMap = dupImplem->netlist.mapping;
				unsigned dup_nb_states = 0;
				unsigned dup_nb_actions_total = 0;

				void get_states_recurs(hier_node_t* node, bool do_level) {
					while(node!=NULL) {
						// Get the FSM state associatesd to this node
						netlist_fsm_state_t* state = NULL;
						avl_pp_find_data(&dupMap->node2state, node, (void**)&state);
						if(state!=NULL) {
							dup_nb_states ++;
							dup_nb_actions_total += ChainList_Count(state->actions);
						}
						// Launch recursively
						foreach(node->CHILDREN, scanChild) get_states_recurs(scanChild->DATA, true);
						// Exit condition
						if(do_level==false) return;
						node = node->NEXT;
					}
				}

				// Only get the FSM states associated to the main loop body
				if(hier_for_chkonebody(duploop_data->flags)==false) get_states_recurs(duploop_data->body_before, true);
				else get_states_recurs(duploop_data->body_after, true);
				assert(dup_nb_states>0);

				//printf("DEBUG %s:%u : states=%u, actions=%u\n", __FILE__, __LINE__, dup_nb_states, dup_nb_actions_total);

				unsigned new_states = (node_loop->iter.iterations_nb - 1) * dup_nb_states;
				new_degree->resource_cost = NULL;

				// Call the techno lib to get the estimated resource cost
				techno_t* techno = Implem->synth_target->techno;
				if(techno != NULL) {
					if(techno->sizeeval_fsmstatesacts != NULL) {
						new_degree->resource_cost = techno->sizeeval_fsmstatesacts(Implem, new_states, dup_nb_actions_total);
					}
				}

			}

			// Set the time gain
			if(node->timing.once.avg<=0) new_degree->time_gain = 1 * node->timing.factor;
			else {

				#if 0
				new_degree->time_gain = GetMax(sqrt(node->timing.once.avg), 1) * node->timing.factor;
				#endif

				#if 1  // Warning this code makes many bench fail!

				// Generate the unrolled version
				hier_node_t* unrolled = Hier_For_GetFullUnroll(H, node, 0);
				assert(unrolled!=NULL);

				// Simplify the BBs
				if(unrolled->TYPE==HIERARCHY_BB) {
					while(unrolled->NEXT!=NULL && unrolled->NEXT->TYPE==HIERARCHY_BB) {
						// Merge the states in BB after in the first BB
						hier_node_t* next_bb = unrolled->NEXT;
						hier_node_t* next_body = next_bb->NODE.BB.body;
						if(next_body!=NULL) {
							Hier_ReplaceChild(next_bb, next_body, NULL);
							Hier_InsertLevelAfterLevel( Hier_Level_GetLastNode(unrolled->NODE.BB.body), next_body);
						}
						Hier_UnlinkNode(next_bb);
						Hier_FreeNode(next_bb);
					}
				}

				bool newbody_is_bb = true;
				if(unrolled->TYPE!=HIERARCHY_BB || unrolled->NEXT!=NULL) newbody_is_bb = false;

				if(newbody_is_bb==true) {
					int nb_clk = Schedule_OneBB_OnlyNbClk(unrolled, Implem->SCHED.hwres_available);
					// FIXME take into account the init state
					// if the init of the loop is the only Act in its State
					new_degree->time_gain = ((double)nb_clk - node->timing.once.avg) * node->timing.factor;
				}
				else {
					new_degree->time_gain = GetMax(sqrt(node->timing.once.avg), 1) * node->timing.factor;
				}

				// Default: At least one cycle is saved
				if(new_degree->time_gain <= 1) new_degree->time_gain = 1 * node->timing.factor;

				// Clean
				Hier_FreeNodeLevel(unrolled);
				#endif

			}

			// Add the freedom degree
			list_degrees = addchain(list_degrees, new_degree);
		}

		// Add a partial unroll, factor 2
		bool part_unroll_possible = true;
		if(skip_part_unroll==true) part_unroll_possible = false;
		if(node_loop->core.part_unroll_impossible!=0) part_unroll_possible = false;
		if(hier_for_chkiternb(node_loop->flags)) {
			if(node_loop->iter.iterations_nb<4) part_unroll_possible = false;
			if(node_loop->iter.val_init % 2 != 0) part_unroll_possible = false;
			if(node_loop->iter.iterations_nb % 2 != 0) part_unroll_possible = false;
		}
		else {
			if(node_loop->user.iter_nb_power2_given==false) part_unroll_possible = false;
			if(node_loop->user.iter_nb_power2<2) part_unroll_possible = false;
		}

		// An arbitrary safeguard to avoid making FSM size explode
		if(part_unroll_possible==true && overbigloop_unroll_en==false) {
			if(node->timing.once.min / node_loop->iter.iterations_nb > 500) {
				part_unroll_possible = false;
			}
		}

		if(part_unroll_possible==true) {
			freedom_degree_t* new_degree = freedom_degree_new();
			new_degree->Implem = Implem;

			new_degree->type = FD_LOOP_UNROLL;
			new_degree->target.hier.node = node;
			new_degree->target.hier.d.unroll_factor = 2;

			// Set the resource cost
			new_degree->resource_cost = NULL;

			// Get the number of states... FIXME this is NOT the right computing
			minmax_t t = Hier_Timing_GetLevelTime(node_loop->body_before);
			unsigned new_states = t.avg;
			new_degree->resource_cost = NULL;

			// Call the techno lib to get the estimated resource cost
			techno_t* techno = Implem->synth_target->techno;
			if(techno != NULL) {
				if(techno->sizeeval_fsmstates != NULL) {
					new_degree->resource_cost = techno->sizeeval_fsmstates(Implem, new_states);
				}
			}

			// Set the time gain
			// FIXME Generate the new body, re-schedule it, and use it for the weight
			if(node->timing.once.avg<=0) new_degree->time_gain = 1 * node->timing.factor;
			else {
				new_degree->time_gain = GetMax(sqrt(node->timing.once.avg), 1) * node->timing.factor * 2.0 / node_loop->iter.iterations_nb;
			}
			if(new_degree->time_gain <= 1) new_degree->time_gain = 1 * node->timing.factor;

			// Add the freedom degree
			list_degrees = addchain(list_degrees, new_degree);
		}

	}  // Scan the FOR nodes

	return list_degrees;
}



//=====================================================================
// Command interpreter
//=====================================================================

#include "command.h"
#include <ctype.h>

int Hier_Loops_Command(implem_t* Implem, command_t* cmd_data) {
	const char* cmd = Command_PopParam(cmd_data);
	if(cmd==NULL) {
		printf("Error (loops): No command received. Try 'help'.\n");
		return -1;
	}

	if(strcmp(cmd, "help")==0) {
		printf(
			"Actions with loops:\n"
			"  Usage: help             Display this help section.\n"
			"  Usage: list             List the loops.\n"
			"  Usage: list-labels      List the labels of the loops that have one.\n"
			"  Usage: find|findall <search> <action>\n"
			"    Search one or several loops and apply an action.\n"
			"    Possible search parameters:\n"
			"      line <line>          Search by declaration line.\n"
			"      idx <index>          Search by index in the list of FOR loops.\n"
			"      label <name>         Search by label name.\n"
			"    Possible action parameters:\n"
			"      show | disp          Print the full node tree.\n"
			"      skel                 Print the skeleton of the node.\n"
			"      unroll-seq [-keep-iter] full\n"
			"                           Fully unroll the nodes.\n"
			"                           Optionally, force keeping the iterator.\n"
			"      unroll-seq <factor>  Unroll the nodes by the specified factor.\n"
			"      no-auto-unroll       Forbid automatic unrolling.\n"
		);
		return 0;
	}

	if(Implem->H==NULL) {
		printf("Error: The hierarchy is not built (or unknown command '%s').\n", cmd);
		return -1;
	}

	if(strcmp(cmd, "list")==0) {
		printf("There are %u loops.\n", avl_p_count(&Implem->H->NODES[HIERARCHY_LOOP]));
		int index = 0;
		avl_p_foreach(&Implem->H->NODES[HIERARCHY_LOOP], scan) {
			hier_node_t* node = scan->data;
			printf("Loop index %d :\n", index);
			// List the lines of this FOR loop
			indent_t indent = Indent_Make(' ', 2, 1);
			Hier_For_PrintDetails(Implem, node, &indent);
			index++;
		}
	}

	else if(strcmp(cmd, "list-labels")==0) {
		// Store the names in a tree to de-duplicate
		avl_p_tree_t tree_labels;
		avl_p_init(&tree_labels);
		// Scan the nodes
		avl_p_foreach(&Implem->H->NODES[HIERARCHY_LOOP], scan) {
			hier_node_t* node = scan->data;
			hier_loop_t* loop_data = &node->NODE.LOOP;
			if(loop_data->label_name==NULL) continue;
			avl_p_add_overwrite(&tree_labels, loop_data->label_name, loop_data->label_name);
		}
		// Set the results of the command
		avl_p_foreach(&tree_labels, scan) {
			Command_Result_AddStatic(cmd_data, (char*)scan->data);
		}
		// Clean
		avl_p_reset(&tree_labels);
	}

	else if(strcmp(cmd, "find")==0 || strcmp(cmd, "findall")==0) {
		bool multiple = strcmp(cmd, "findall")==0 ? true : false;
		// The nodes found
		chain_list* list_nodes = NULL;
		// Search action
		const char* cmd_search = Command_PopParam(cmd_data);
		if(cmd_search==NULL) {
			printf("Error : Missing parameters. Try 'help'.\n");
			return -1;
		}

		if(strcmp(cmd_search, "line")==0) {
			const char* str_line = Command_PopParam(cmd_data);
			if(str_line==NULL) {
				printf("Error : Missing the line value.\n");
				return -1;
			}
			int line = atoi(str_line);
			avl_p_foreach(&Implem->H->NODES[HIERARCHY_LOOP], scanNode) {
				hier_node_t* nodeLoop = scanNode->data;
				bool b = LineList_IsThereNameLine(nodeLoop->source_lines, NULL, line);
				if(b==false) continue;
				list_nodes = addchain(list_nodes, nodeLoop);
				if(multiple==false) break;
			}
		}
		else if(strcmp(cmd_search, "idx")==0) {
			const char* str_index = Command_PopParam(cmd_data);
			if(str_index==NULL) {
				printf("Error : Missing the index value.\n");
				return -1;
			}
			int index = atoi(str_index);
			hier_node_t* node = avl_p_GetIdx(&Implem->H->NODES[HIERARCHY_LOOP], index);
			if(node!=NULL) list_nodes = addchain(list_nodes, node);
		}
		else if(strcmp(cmd_search, "label")==0) {
			char* label_name = Command_PopParam_stralloc(cmd_data);
			if(label_name==NULL) {
				printf("Error : Missing label name.\n");
				return -1;
			}
			// Scan all loop nodes
			avl_p_foreach(&Implem->H->NODES[HIERARCHY_LOOP], scanNode) {
				hier_node_t* nodeLoop = scanNode->data;
				hier_loop_t* loop_data = &nodeLoop->NODE.LOOP;
				if(loop_data->label_name!=label_name) continue;
				list_nodes = addchain(list_nodes, nodeLoop);
				if(multiple==false) break;
			}
		}
		else {
			printf("Error : Unknown search command '%s'.\n", cmd_search);
			return -1;
		}

		if(list_nodes==NULL) {
			printf("No matching loop node was found.\n");
			return -1;
		}
		if(multiple==true) {
			printf("Loops found: %u\n", ChainList_Count(list_nodes));
		}

		// From here, don't use return. Must use goto ENDFIND and set the ret to error value.
		unsigned ret = 0;

		// Get the action associated
		const char* cmd_action = Command_PopParam(cmd_data);
		if(cmd_action==NULL) {
			printf("Error : Missing action.\n");
			ret = -1;
			goto ENDFIND;
		}

		if(strcmp(cmd_action, "show")==0 || strcmp(cmd_action, "disp")==0) {
			printf("\n");
			foreach(list_nodes, scan) {
				hier_node_t* node = scan->DATA;
				Hier_PrintNode(node);
				printf("\n");
			}
		}
		else if(strcmp(cmd_action, "skel")==0) {
			printf("\n");
			foreach(list_nodes, scan) {
				hier_node_t* node = scan->DATA;
				Hier_PrintNodeSkel(node);
				printf("\n");
			}
		}
		else if(strcmp(cmd_action, "disp-time")==0) {
			if(multiple==true) printf("\n");
			foreach(list_nodes, scan) {
				hier_node_t* node = scan->DATA;
				minmax_print(&node->timing.once, "  Execution time, once   : ", " (clock cycles)\n");
				minmax_print(&node->timing.global, "  Execution time, global : ", " (clock cycles)\n");
				if(multiple==true) printf("\n");
			}
		}
		else if(strcmp(cmd_action, "unroll-seq")==0) {
			int flags = 0;
			char const * unroll_factor = NULL;
			// Get the optional parameter -keep-iter
			char const * unroll_opt = Command_PopParam(cmd_data);
			if(unroll_opt!=NULL) {
				if(strcmp(unroll_opt, "-keep-iter")==0) {
					flags |= HIER_LOOPUNROLL_KEEPITER;
				}
				else unroll_factor = unroll_opt;
			}
			// Get the unroll factor
			if(unroll_factor==NULL) unroll_factor = Command_PopParam(cmd_data);
			if(unroll_factor==NULL) {
				printf("Error : Missing unroll factor.\n");
				ret = -1;
				goto ENDFIND;
			}
			if(strcmp(unroll_factor, "full")==0) {
				// Full unroll.
				foreach(list_nodes, scan) {
					hier_node_t* node = scan->DATA;
					int z = Hier_For_ReplaceByFullUnroll(Implem, node, flags);
					if(z!=0) {
						printf("ERROR : Failed to unroll a loop (code %d).\n", z);
						ret = -1;
						goto ENDFIND;
					}
				}
				Hier_Timing_Update(Implem, false);
			}
			else if(isdigit(unroll_factor[0])!=0) {
				// Partial unroll.
				int unroll_value = atoi(unroll_factor);
				if(unroll_value<1) {
					printf("Error : Invalid unroll factor '%s'.\n", unroll_factor);
					ret = -1;
					goto ENDFIND;
				}
				foreach(list_nodes, scan) {
					hier_node_t* node = scan->DATA;
					int z = Hier_For_PartialUnroll(Implem, node, unroll_value, flags);
					if(z!=0) {
						printf("ERROR : Failed to unroll a loop (code %d).\n", z);
						ret = z;
						goto ENDFIND;
					}
				}
				Hier_Timing_Update(Implem, false);
			}
			else {
				printf("Error : Invalid unroll factor '%s'.\n", unroll_factor);
				ret = -1;
				goto ENDFIND;
			}
		}
		else if(strcmp(cmd_action, "no-auto-unroll")==0) {
			foreach(list_nodes, scan) {
				hier_node_t* node = scan->DATA;
				hier_loop_t* loop_data = &node->NODE.LOOP;
				loop_data->core.part_unroll_impossible = 1;
				loop_data->core.full_unroll_impossible = 1;
			}
		}
		else {
			printf("Error : Unknown action '%s'.\n", cmd_action);
			ret = -1;
			goto ENDFIND;
		}

		ENDFIND:

		// Clean
		freechain(list_nodes);

		return ret;
	}

	else {
		printf("Error (loops) : Unknown command '%s'.\n", cmd);
		return -1;
	}

	return 0;
}


