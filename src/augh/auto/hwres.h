
#ifndef _HWRES_H_
#define _HWRES_H_

#include "../hvex/hvex_dico.h"

typedef struct hw_res_t         hw_res_t;
typedef struct hwres_heap_t     hwres_heap_t;
typedef struct hwres_scanvex_t  hwres_scanvex_t;

#include "../indent.h"
#include "auto.h"


//=========================================================
// Definitions of structures
//=========================================================

/* Definition of the hardware resource structure
FIXME this comment section must be completely adapted to the new structures and processes...

Operators such as AND, ADD, ALU, MUL etc can be used only one time in a clock cycle.
Their number in the architecture is known and their availability at any clock cycle is easy to track.

Operators such as memory banks are a bit trickier.
Accessing a cell in a memory bank does not mean the memory becomes unavailable to other ACT in the clock cycle.
What actually means is the number of read and write ports and what they are assigned to (the VEX expression).

So :
For some operators we only have to track if they are used or not, and in which expression, to share its result.
For memory banks, we have to track each read and write port, and in which expression they are used to.
For registers, anybody can read it. There is no limit to the number of 'read ports'.

The goal is of course to build a generic structure.
An operator could be defined by :
- A VEX identifier : operation AND, ADD, MUL etc, or the name of a register or array.
- The number of read ports
  - Each read has the accessed address used (used during scheduling).
- The number of write ports
  It is not necesary to track what is written and to which address.

Exemple :
  An operator :
    The VEX is a Operation node, without operands.
    The number of read ports is UNLIMITED. The number of write ports is 1.
  A register :
    The VEX is the name of the register (atom node).
    The number of read ports is UNLIMITED. The number of write ports is 1.
  A memory bank :
    The VEX is the name of the memory bank (Operation node, Array_Index type).
    The number of read ports is NR. The number of write ports is NW.

FIXMEEEEEEEEEEEEE For the ACTs, we need to count the OPERATIONS, but we have lists of available OPERATORS.
	We need to build a selection function, that associates an operation to an operator !

*/

typedef enum hwres_type_t {
	HWRES_TYPE_REG,
	HWRES_TYPE_MEM,
	HWRES_TYPE_OP,
} hwres_type_t;

struct hw_res_t {
	hwres_type_t   type;

	char*          name;           // Netlist component name (reg & mem) or model name (operator)
	int            number;         // The number of instances, if not a register/memory
	// For memory resource, number of ports available/needed
	int            ports_r;
	int            ports_w;
	// For memory resource, indicates whether direct cell access is available
	bool           direct_avail;
	bool           direct_need;  // FIXME TO USE

	// When computing the resources needed for an ACT, the needed results of all operations/accesses have to be listed.
	// Then when checking if an Act can be scheduled at a clock cycle, we check if some of these
	// are already provided by previous assignations.
	chain_list*    vexes_r;        // The VEX expressions needed for read
	chain_list*    vexes_w;        // The VEX expressions needed for write
	// For memory resource, number of ports needed if direct accesses are available
	int            ports_r_ifdirect;
	int            ports_w_ifdirect;
};

// A heap of resource elements of different kinds
struct hwres_heap_t {
	avl_pp_tree_t  type_reg;  // Indexed by name
	avl_pp_tree_t  type_mem;  // Indexed by name
	avl_pp_tree_t  type_ops;  // Indexed by type
};

// A data structure used for recursive scan of Vex / VexAlloc expressions
struct hwres_scanvex_t {
	// Contains the already scanned VEX (used only for VEX-based scan)
	chain_list* scanned_vex;
	// To flag scanned nodes (used only for VEXALLOC-based scan)
	avl_p_tree_t scanned_vexalloc;

	// Contains the needed operators.
	hwres_heap_t* heap_needed;
	// In case of error, this is in the error message.
	bitype_list* source_lines;
};



//=========================================================
// Functions
//=========================================================

hw_res_t* hwres_new();
void      hwres_free(hw_res_t* ptr_hwres);

void hwres_print(hw_res_t* ptr_hwres);

hwres_heap_t* hwres_heap_new();
void          hwres_heap_free(hwres_heap_t* heap);
hwres_heap_t* hwres_heap_dup(hwres_heap_t* heap);
void          hwres_heap_print(hwres_heap_t* heap, indent_t* indent);


static inline hw_res_t* hwres_heap_reg_find(hwres_heap_t* heap, char* name) {
	avl_pp_node_t* avl_node = NULL;
	bool b = avl_pp_find(&heap->type_reg, name, &avl_node);
	if(b==true) return (hw_res_t*)avl_node->data;
	return NULL;
}
static inline hw_res_t* hwres_heap_reg_getadd(hwres_heap_t* heap, char* name) {
	avl_pp_node_t* avl_node = NULL;
	bool b = avl_pp_add(&heap->type_reg, name, &avl_node);
	if(b==true) {
		hw_res_t* elt = hwres_new();
		avl_node->data = elt;
		// Initialize
		elt->type = HWRES_TYPE_REG;
		elt->name = name;
	}
	return (hw_res_t*)avl_node->data;
}

static inline hw_res_t* hwres_heap_mem_find(hwres_heap_t* heap, char* name) {
	avl_pp_node_t* avl_node = NULL;
	bool b = avl_pp_find(&heap->type_mem, name, &avl_node);
	if(b==true) return (hw_res_t*)avl_node->data;
	return NULL;
}
static inline hw_res_t* hwres_heap_mem_getadd(hwres_heap_t* heap, char* name) {
	avl_pp_node_t* avl_node = NULL;
	bool b = avl_pp_add(&heap->type_mem, name, &avl_node);
	if(b==true) {
		hw_res_t* elt = hwres_new();
		avl_node->data = elt;
		// Initialize
		elt->type = HWRES_TYPE_MEM;
		elt->name = name;
	}
	return (hw_res_t*)avl_node->data;
}

static inline hw_res_t* hwres_heap_op_find(hwres_heap_t* heap, char* modelname) {
	avl_pp_node_t* avl_node = NULL;
	bool b = avl_pp_find(&heap->type_ops, modelname, &avl_node);
	if(b==true) return (hw_res_t*)avl_node->data;
	return NULL;
}
static inline hw_res_t* hwres_heap_op_getadd(hwres_heap_t* heap, char* modelname) {
	avl_pp_node_t* avl_node = NULL;
	bool b = avl_pp_add(&heap->type_ops, modelname, &avl_node);
	if(b==true) {
		hw_res_t* elt = hwres_new();
		avl_node->data = elt;
		// Initialize
		elt->type = HWRES_TYPE_OP;
		elt->name = modelname;
	}
	return (hw_res_t*)avl_node->data;
}

void hwres_heap_remove_vex(hwres_heap_t* heap);
void hwres_heap_remove_empty(hwres_heap_t* heap);
void hwres_heap_remove_ops(hwres_heap_t* heap);

bool hwres_heap_isempty(hwres_heap_t* heap);
bool hwres_heap_isempty_noreg(hwres_heap_t* heap);
int  hwres_heap_cmp(hwres_heap_t* heap0, hwres_heap_t* heap1);

void hwres_heap_add_heap_needed(hwres_heap_t* heap, hwres_heap_t* src);
void hwres_heap_add_heap_avail(hwres_heap_t* heap, hwres_heap_t* src);

void hwres_heap_getmax_need(hwres_heap_t* dest, hwres_heap_t* src);
void hwres_heap_sub_shareneed(hwres_heap_t* heap0, hwres_heap_t* heap1);
int  hwres_heap_sub_fromavail(hwres_heap_t* heap_avail, hwres_heap_t* heap_need, bool noerr, bool subsrc);

void hwres_heap_add_heap_avail_forFD(hwres_heap_t* heap, hwres_heap_t* src);
bool hwres_heap_check_avail_forFD(hwres_heap_t* heap_avail, hwres_heap_t* heap_need);
bool hwres_heap_check_need_included_forFD(hwres_heap_t* heap1, hwres_heap_t* heap0);
void hwres_heap_sub_fromavail_forFD(hwres_heap_t* heap_avail, hwres_heap_t* heap_need);
int  hwres_heap_cmp_forFD(hwres_heap_t* heap0, hwres_heap_t* heap1);

static inline bool hwres_heap_isthere_memr(hwres_heap_t* heap) {
	avl_pp_foreach(&heap->type_mem, avl_node) {
		hw_res_t* elt = (hw_res_t*)avl_node->data;
		if(elt->ports_r>0) return true;
		if(elt->vexes_r!=NULL) return true;
	}
	return false;
}
static inline bool hwres_heap_isthere_memw(hwres_heap_t* heap) {
	avl_pp_foreach(&heap->type_mem, avl_node) {
		hw_res_t* elt = (hw_res_t*)avl_node->data;
		if(elt->ports_w>0) return true;
	}
	return false;
}


ptype_list* hwres_heap_to_op_names(hwres_heap_t* heap);

// This may be used in callbacks for user-defined components
void hwres_heap_makeadd_needed_op_vex(hwres_heap_t* heap, char* type, hvex_t* vex);

// This may be used in callbacks for user-defined components
void hwres_scanvex_init(hwres_scanvex_t* hwres_data);
void hwres_scanvex_clear(hwres_scanvex_t* hwres_data);
void hwres_scanvex_recurs(hvex_t *Expr, hwres_scanvex_t* hwres_data);

hwres_heap_t* hwres_heap_getneed_vex(hvex_t* Expr);
hwres_heap_t* hwres_heap_getneed_hierstate(hier_node_t* node);
hwres_heap_t* hwres_heap_getneed_actwithinline(hier_action_t* action);

hwres_heap_t* hwres_heap_getneed_node(hier_node_t* node);
hwres_heap_t* hwres_heap_getneed_level(hier_node_t* node);

hwres_heap_t* hwres_heap_getneed(implem_t* Implem);

// This may be used in callbacks for user-defined components
void hwres_scanvexalloc_recurs(hvex_dico_ref_t* ref, hwres_scanvex_t* data);

hwres_heap_t* hwres_heap_getneed_vexalloc_ref(hvex_dico_ref_t* ref);
hwres_heap_t* hwres_heap_getneed_hierstate_vexalloc(hier_node_t* node);
hwres_heap_t* hwres_heap_getneed_actwithinline_vexalloc(hier_action_t* action, avl_pp_tree_t* tree_act2ref);

hwres_heap_t* hwres_heap_getneed_node_vexalloc(hier_node_t* node);
hwres_heap_t* hwres_heap_getneed_level_vexalloc(hier_node_t* node);

hwres_heap_t* hwres_heap_getneed_vexalloc(implem_t* Implem);

// These may be used in callbacks for user-defined components
void hwres_heap_add_avail_reg(hwres_heap_t* heap, char* name, int num_r, int num_w);
void hwres_heap_add_avail_mem(hwres_heap_t* heap, char* name, int num_r, int num_w, bool direct);
void hwres_heap_add_avail_op(hwres_heap_t* heap, char* name, int number);

hwres_heap_t* hwres_heap_getavail(implem_t* Implem);

int  Resources_Available_Detect(implem_t* Implem);
void Resources_Available_View(implem_t* Implem, indent_t* indent);


#endif  // _HWRES_H_

