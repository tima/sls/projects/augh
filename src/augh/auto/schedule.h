
#ifndef _SCHEDULE_H_
#define _SCHEDULE_H_

#include <stdbool.h>
#include <stdint.h>

#include "../chain.h"
#include "auto.h"
#include "hwres.h"



//======================================================
// Declaration of structures
//======================================================

/* This 'scheduling' structure is filled before the scheduling process.
The 'rank' represents the number of states to execute the Actions that follow.
	It is the number of dependency 'needed states' after.
	So the rank of each Action is the highest rank of the next Actions, +1
	Note: it is not an actual number of states, but a number of dependency links...
		WaR allows a scheduling on the same cycle.
The list of previous and next Actions does not include those that must be scheduled
	on the same state due to mutual WaR dependency.
	This is to make bypassing Actions possible.
*/

typedef struct act_dep_t    act_dep_t;
typedef struct state_dep_t  state_dep_t;
typedef struct schedule_t   schedule_t;


#include "hierarchy.h"


// Scheduling algorithms
#define SCHED_ALGO_LIST  1
#define SCHED_ALGO_ASAP  2

// Flags for data dependencies between Actions
#define SCHED_DEP_WAR  0x0004  // Write After Read
#define SCHED_DEP_RAW  0x0002  // Read  After Write
#define SCHED_DEP_WAW  0x0001  // Write After Write


struct act_dep_t {
	hier_action_t*  action;          // The Hier Action this structure refers to.

	state_dep_t*    state_orig;      // The state this Action was originally scheduled to
	state_dep_t*    state_sched;     // The state this Action is re-scheduled to
	int             rank;            // The clock cycle index according to ASAP scheduling

	// Dependencies of each symbol.
	// Indexed by name. Contains elements of type sched_sym_dep_t*
	struct {
		// The timed dependencies to other ACTs. Usage similar to the Hier Action tree.
		// The Hier Action tree is copied into this one for scheduling.
		avl_pi_tree_t   timed;
		// The data dependencies to other ACTs. The data contains the dependency flags.
		avl_pi_tree_t   before;
		avl_pi_tree_t   after;
	} deps;

	// The hardware resources occupied by this ACT
	hwres_heap_t*   needed_hwres;

	// Scheduler flag to track schedulability of Actions
	int             flag_sched;
};

struct state_dep_t {
	chain_list*     acts;         // The list of Actions: pointers to the dependency structure.
	hier_node_t*    state;        // The Hier State node.
	int             date;         // The original state index in the basic block.
	// After scheduling
	hwres_heap_t*   hwres_remain;
	chain_list*     acts_unscheduled;
	// Chained list
	schedule_t*     ref_sched;
	state_dep_t*    PREV;
	state_dep_t*    NEXT;
};

// One entire basic block
struct schedule_t {
	// Algorithm: if no resources, it's ASAP algorithm
	int                 algo;
	bool                nores;
	// Input basic block
	hier_node_t*        node_bb;
	chain_list*         act_dep_list;     // The Actions, with dependency links.
	state_dep_t*        state_dep_first;  // The first state (linked list).
	state_dep_t*        state_dep_last;   // The last state (linked list).
	int                 state_nb_orig;    // Original number of states with actions
	// After scheduling
	state_dep_t*        state_sched_first;
	state_dep_t*        state_sched_last;
	unsigned            state_sched_nb;
	// These lists contain pointers to the dependency array.
	chain_list*         begin_depacts;    // Actions with no dependence before
	chain_list*         finish_depacts;   // Actions with no dependence after
};



//======================================================
// Parameters
//======================================================

extern bool sched_use_vexalloc_share;



//======================================================
// Functions
//======================================================

void Sched_free(schedule_t* sched);

void Sched_ImplemFree(implem_t* Implem);
void Sched_ImplemInit(implem_t* Implem);

int Schedule(implem_t* Implem, int algo);
int Schedule_OneBB_OnlyNbClk(hier_node_t* node, hwres_heap_t* avail_resources);

chain_list* Sched_FreedomDegrees_Build_Ops(implem_t* Implem);
chain_list* Sched_FreedomDegrees_Build_ScalarReplace(implem_t* Implem);
chain_list* Sched_FreedomDegrees_Build_PPDirect(implem_t* Implem);

double Sched_GetScheduledLatency_ForFD(implem_t* Implem, hier_node_t* nodeBB, hwres_heap_t* avail_resources);

// User commands

#include "command.h"

int Schedule_Command(implem_t* Implem, command_t* cmd_data);



#endif  // _SCHEDULE_H_

