
// This file contains the mapper-specific callbacks for the HVEX models

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#include "auto.h"
#include "map.h"
#include "../hvex/hvex_misc.h"

#include "../netlist/netlist.h"
#include "../netlist/netlist_comps.h"
#include "../netlist/netlist_access.h"



// FIXME Recognize the operations that use the same operators and operands.
//   Example : "c = a-b" and "a > b"
// FIXME the width check should take the sign into account
//   Add one bit to the unsigned input IF the two inputs have different signedness.

static map_netlist_datapath_t* Map_Hvex_CB_Literal(map_netlist_data_t* map_data, hvex_dico_elt_t* Expr) {
	//dbgprintf("Literal %s\n", Expr->data.lit.value);

	map_netlist_datapath_t* datapath = Map_Netlist_Datapath_New();
	datapath->Expr = Expr;

	netlist_val_cat_t* valcat = Netlist_ValCat_New();
	valcat->literal = Expr->data;
	valcat->width = Expr->width;
	valcat->left = Expr->left;
	valcat->right = Expr->right;

	datapath->valcat = addchain(NULL, valcat);

	return datapath;
}

static map_netlist_datapath_t* Map_Hvex_CB_Vector(map_netlist_data_t* map_data, hvex_dico_elt_t* Expr) {
	char* name = Expr->data;
	netlist_port_t* target_port = NULL;

	//dbgprintf("Vector name '%s'\n", name);

	map_netlist_datapath_t* datapath = Map_Netlist_Datapath_New();
	datapath->Expr = Expr;

	// First, search a component with the right name
	netlist_comp_t* comp = Map_Netlist_GetComponent(map_data->Implem, name);
	if(comp!=NULL) {
		if(comp->model==NETLIST_COMP_SIGNAL) {
			netlist_signal_t* compSig_data = comp->data;
			target_port = compSig_data->port_out;
		}
		else if(comp->model==NETLIST_COMP_REGISTER) {
			netlist_register_t* compReg_data = comp->data;
			target_port = compReg_data->port_out;
		}
		else {
			printf("FATAL ERROR: Symbol '%s' was not found\n", name);
			abort();
		}
	}

	// Second, search a port with the right name
	if(target_port==NULL) {
		target_port = Netlist_Comp_GetPort(map_data->Implem->netlist.top, name);
	}
	assert(target_port!=NULL);

	// Build the "ValCat" element
	netlist_val_cat_t* valcat = Netlist_ValCat_New();
	valcat->source = target_port;
	valcat->width = Expr->width;
	valcat->left = Expr->left;
	valcat->right = Expr->right;

	datapath->valcat = addchain(NULL, valcat);

	return datapath;
}

static map_netlist_datapath_t* Map_Hvex_CB_Index(map_netlist_data_t* map_data, hvex_dico_elt_t* Expr) {
	hvex_dico_ref_t* Expr_Name = Expr->args->DATA;
	hvex_dico_ref_t* Expr_Address = Expr->args->NEXT->DATA;

	char* name = Expr_Name->ref.elt->data;
	//dbgprintf("Mapping INDEX oper to symbol '%s'.\n", name);

	map_netlist_datapath_t* datapath = Map_Netlist_Datapath_New();
	datapath->Expr = Expr;

	netlist_comp_t* comp = Map_Netlist_GetComponent(map_data->Implem, name);
	netlist_access_t* topaccess = Netlist_Comp_GetAccess(map_data->Implem->netlist.top, name);
	netlist_port_t* port_data = NULL;

	if(comp!=NULL) {

		if(comp->model==NETLIST_COMP_MEMORY) {
			netlist_memory_t* compMem_data = comp->data;

			// If the address is a literal and the component has direct access.
			// Use direct the access port.
			if(Expr_Address->ref.elt->model==HVEX_LITERAL && compMem_data->direct_en==true) {

				unsigned index = VexAlloc_Lit2Uint(Expr_Address->ref.elt);
				netlist_access_t* access = Netlist_Mem_GetDirectAccess(comp, index);
				if(access==NULL) {
					errprintfa("No direct access at index %u was found in mem '%s'.\n", index, comp->name);
				}
				netlist_access_mem_direct_t* memD_data = access->data;
				port_data = memD_data->port_rd;

			}  // Direct access

			// Use an access by address
			else {
				map_netlist_datapath_t* Datapath_Address = Map_Netlist_Datapath_Expr(map_data, Expr_Address->ref.elt);
				datapath->sub_datapaths = addchain(datapath->sub_datapaths, Datapath_Address);

				chain_list* dup_addr_valcat = Map_Netlist_ValCat_DupResize(
					Datapath_Address->valcat, Expr_Address, compMem_data->addr_width
				);

				netlist_access_t* available_access = NULL;
				netlist_access_t* matching_access = NULL;

				// Get an available access, that also has the required address datapath built
				// FIXME need to also check the Value datapath and consider MUX size
				avl_p_foreach(&compMem_data->access_ports_ra, scanRA) {
					netlist_access_t* local_access = scanRA->data;
					bool b = avl_p_isthere(&map_data->cur_state.hwusage.mem_accesses_used, local_access);
					if(b==true) {
						// Compare the already used address to what is needed.
						netlist_access_mem_addr_t* memRA_data = local_access->data;
						netlist_comp_t* muxAddr = Map_Netlist_GetSourceMux(memRA_data->port_a);
						netlist_access_t* muxIn = NULL;
						avl_pp_find_data(&map_data->cur_state.hwusage.mux_in_used, muxAddr, (void**)&muxIn);
						assert(muxIn!=NULL);
						netlist_mux_access_t* muxIn_data = muxIn->data;
						bool b_valcat = Netlist_ValCat_EqList(muxIn_data->port_data->source, dup_addr_valcat);
						if(b_valcat==true) {
							matching_access = local_access;
							break;
						}
					}
					else available_access = local_access;
				}

				assert(available_access!=NULL || matching_access!=NULL);

				if(matching_access!=NULL) {
					netlist_access_mem_addr_t* memRA_data = matching_access->data;
					port_data = memRA_data->port_d;
					// Flag the access as used
					avl_p_add_overwrite(&datapath->hwusage.mem_accesses_used, matching_access, matching_access);
				}
				else {
					netlist_access_mem_addr_t* memRA_data = available_access->data;

					// Get an input of the Address MUX.
					Map_Netlist_Port_MuxGetAddInput_SetState_FromExpr(
						map_data, datapath, memRA_data->port_a,
						Expr_Address, Datapath_Address->valcat
					);

					port_data = memRA_data->port_d;

					// Flag the access as used
					avl_p_add_overwrite(&datapath->hwusage.mem_accesses_used, available_access, available_access);
				}

				Netlist_ValCat_FreeList(dup_addr_valcat);
			}  // Access by address

			if(port_data==NULL) {
				printf("INTERNAL ERROR %s:%u : Missing R port for mem '%s'\n", __FILE__, __LINE__, name);
				printf("  avail ports: %u\n", compMem_data->ports_ra_nb);
				abort();
			}

		}

		else if(comp->model==NETLIST_COMP_PINGPONG_IN || comp->model==NETLIST_COMP_PINGPONG_OUT) {
			netlist_pingpong_t* compPP_data = comp->data;

			// If the address is a literal and the component has direct access.
			// Use direct the access port.
			if(Expr_Address->ref.elt->model==HVEX_LITERAL && compPP_data->is_direct==true) {

				unsigned index = VexAlloc_Lit2Uint(Expr_Address->ref.elt);
				netlist_access_t* access = Netlist_PingPong_GetDirectAccess(comp, index);
				assert(access!=NULL);
				netlist_access_mem_direct_t* memD_data = access->data;
				port_data = memD_data->port_rd;

			}  // Direct access

			else {

				map_netlist_datapath_t* Datapath_Address = Map_Netlist_Datapath_Expr(map_data, Expr_Address->ref.elt);
				datapath->sub_datapaths = addchain(datapath->sub_datapaths, Datapath_Address);

				chain_list* dup_addr_valcat = Map_Netlist_ValCat_DupResize(
					Datapath_Address->valcat, Expr_Address, compPP_data->addr_width
				);

				netlist_access_t* available_access = NULL;
				netlist_access_t* matching_access = NULL;

				// Get an available access, that also has the required address datapath built
				// FIXME need to also check the Value datapath and consider MUX size
				avl_p_foreach(&compPP_data->access_ports_ra, scanRA) {
					netlist_access_t* local_access = scanRA->data;
					bool b = avl_p_isthere(&map_data->cur_state.hwusage.mem_accesses_used, local_access);
					if(b==true) {
						// Compare the already used address to what is needed.
						netlist_access_mem_addr_t* memRA_data = local_access->data;
						netlist_comp_t* muxAddr = Map_Netlist_GetSourceMux(memRA_data->port_a);
						netlist_access_t* muxIn = NULL;
						#ifndef NDEBUG
						bool b_mux =
						#endif
						avl_pp_find_data(&map_data->cur_state.hwusage.mux_in_used, muxAddr, (void**)&muxIn);
						assert(b_mux==true);
						netlist_mux_access_t* muxIn_data = muxIn->data;
						bool b_valcat = Netlist_ValCat_EqList(muxIn_data->port_data->source, dup_addr_valcat);
						if(b_valcat==true) {
							matching_access = local_access;
							break;
						}
					}
					else available_access = local_access;
				}

				assert(available_access!=NULL || matching_access!=NULL);

				if(matching_access!=NULL) {
					netlist_access_mem_addr_t* memRA_data = matching_access->data;
					port_data = memRA_data->port_d;
					// Flag the access as used
					avl_p_add_overwrite(&datapath->hwusage.mem_accesses_used, matching_access, matching_access);
				}
				else {
					netlist_access_mem_addr_t* memRA_data = available_access->data;

					// Get an input of the Address MUX.
					Map_Netlist_Port_MuxGetAddInput_SetState_FromExpr(
						map_data, datapath, memRA_data->port_a,
						Expr_Address, Datapath_Address->valcat
					);

					port_data = memRA_data->port_d;

					// Flag the access as used
					avl_p_add_overwrite(&datapath->hwusage.mem_accesses_used, available_access, available_access);
				}

				Netlist_ValCat_FreeList(dup_addr_valcat);
			}

			if(port_data==NULL) {
				printf("INTERNAL ERROR %s:%u : Missing R port for mem '%s'\n", __FILE__, __LINE__, name);
				printf("  avail ports: %u\n", compPP_data->ports_ra_nb);
			}

		} // end case PINGPONG

		else {
			printf("ERROR: Model VEX_INDEX can't be mapped on component '%s' type '%s'.\n", comp->name, comp->model->name);
			abort();
		}

	}

	else if(topaccess!=NULL) {

		if(topaccess->model==NETLIST_ACCESS_MEM_RA_EXT) {
			netlist_access_mem_addr_t* memRA_data = topaccess->data;

			// Ensure the access is free. If not, this should have been cached.
			assert(avl_p_isthere(&map_data->cur_state.hwusage.mem_accesses_used, topaccess)==false);

			map_netlist_datapath_t* Datapath_Address = Map_Netlist_Datapath_Expr(map_data, Expr_Address->ref.elt);
			datapath->sub_datapaths = addchain(datapath->sub_datapaths, Datapath_Address);

			chain_list* dup_addr_valcat = Map_Netlist_ValCat_DupResize(
				Datapath_Address->valcat, Expr_Address, memRA_data->port_a->width
			);

			// Get an input of the Address MUX.
			Map_Netlist_Port_MuxGetAddInput_SetState_FromExpr(
				map_data, datapath, memRA_data->port_a,
				Expr_Address, Datapath_Address->valcat
			);
			Netlist_ValCat_FreeList(dup_addr_valcat);

			port_data = memRA_data->port_d;

			// Flag the access as used
			avl_p_add_overwrite(&datapath->hwusage.mem_accesses_used, topaccess, topaccess);
		}

		else {
			errprintfa("Model VEX_INDEX: Incompatible top-level access '%s' model '%s'\n", name, topaccess->model->name);
		}

	}

	else {
		errprintfa("Model HVEX_INDEX on symbol '%s': no corresponding component nor top-level access.\n", name);
	}

	if(port_data==NULL) {
		printf("  Cur expr:\n");
		VexAlloc_Print_Elt_be(Expr, "    ", "\n");
		printf("  Mapped expr in cur state:\n");
		avl_pp_foreach(&map_data->cur_state.expr2datapath, scan) {
			hvex_dico_elt_t* elt = scan->key;
			if(elt->model!=Expr->model) continue;
			char* this_name = elt->data;
			if(this_name!=name) continue;
			VexAlloc_Print_Elt_be(elt, "    ", "\n");
		}
		printf("  All similar expr in dictionary:\n");
		avl_pp_node_t* avl_node;
		chain_list* list_elt = NULL;
		bool b = avl_pp_find(&map_data->expr_dictionary.elt_others, Expr->model->name, &avl_node);
		if(b==true) list_elt = avl_node->data;
		foreach(list_elt, scan) {
			hvex_dico_elt_t* elt = scan->DATA;
			char* this_name = elt->data;
			if(this_name!=name) continue;
			VexAlloc_Print_Elt_be(elt, "    ", "\n");
		}
		abort();
		//datapath->valcat = Netlist_ValCat_MakeList_RepeatBit('0', Expr->width);
	}

	// Build the "ValCat" element
	netlist_val_cat_t* valcat = Netlist_ValCat_Make_FullPort(port_data);
	valcat->width = Expr->width;
	valcat->left = Expr->left;
	valcat->right = Expr->right;

	datapath->valcat = addchain(NULL, valcat);

	return datapath;
}

// All types of assignments
static map_netlist_datapath_t* Map_Hvex_CB_Asg(map_netlist_data_t* map_data, hvex_dico_elt_t* Expr) {
	assert(Expr->model==HVEX_ASG || Expr->model==HVEX_ASG_EN || Expr->model==HVEX_ASG_ADDR || Expr->model==HVEX_ASG_ADDR_EN);

	map_netlist_datapath_t* datapath = Map_Netlist_Datapath_New();
	datapath->Expr = Expr;

	// Get the destination component and the input MUX
	hvex_dico_ref_t* Expr_Dest = Expr->args->DATA;
	char* name = Expr_Dest->ref.elt->data;

	//dbgprintf("Mapping ASG oper %d to symbol '%s'.\n", Expr->data.oper.oper, name);

	// Get the expression to assign
	hvex_dico_ref_t* Expr_Value = Expr->args->NEXT->DATA;
	map_netlist_datapath_t* Datapath_Value = Map_Netlist_Datapath_Expr(map_data, Expr_Value->ref.elt);
	datapath->sub_datapaths = addchain(datapath->sub_datapaths, Datapath_Value);

	// Get the address, if applicable
	hvex_dico_ref_t* Expr_Address = NULL;
	if(Expr->model==HVEX_ASG_ADDR || Expr->model==HVEX_ASG_ADDR_EN) {
		Expr_Address = Expr->args->NEXT->NEXT->DATA;
	}

	// Get the wired condition, if applicable
	hvex_dico_ref_t* Expr_Enable = NULL;
	if(Expr->model==HVEX_ASG_EN) Expr_Enable = Expr->args->NEXT->NEXT->DATA;
	else if(Expr->model==HVEX_ASG_ADDR_EN) {
		Expr_Enable = Expr->args->NEXT->NEXT->NEXT->DATA;
	}

	// Handle write to components: memory, register, signal
	// Handle write to top-level interface: port, access

	netlist_comp_t* target_comp = Map_Netlist_GetComponent(map_data->Implem, name);
	netlist_access_t* topaccess = Netlist_Comp_GetAccess(map_data->Implem->netlist.top, name);
	netlist_port_t* target_port = Netlist_Comp_GetPort(map_data->Implem->netlist.top, name);

	// Register, signal or top-level port
	if(Expr_Address==NULL) {
		assert(target_comp!=NULL || target_port!=NULL);

		// Check if the symbol is a component (i.e. not a port)
		if(target_comp!=NULL) {
			if(target_comp->model==NETLIST_COMP_SIGNAL) {
				netlist_signal_t* comp_sig = target_comp->data;

				Map_Netlist_Port_MuxGetAddInput_SetState_FromExpr(
					map_data, datapath, comp_sig->port_in,
					Expr_Value, Datapath_Value->valcat
				);

			}
			else if(target_comp->model==NETLIST_COMP_REGISTER) {
				netlist_register_t* compReg_data = target_comp->data;

				// Get the right input of the Data MUX.
				Map_Netlist_Port_MuxGetAddInput_SetState_FromExpr(
					map_data, datapath, compReg_data->port_in,
					Expr_Value, Datapath_Value->valcat
				);

				// Get the right input of the Enable MUX.
				chain_list* valcat_en = NULL;
				if(Expr_Enable==NULL) {
					valcat_en = Netlist_ValCat_MakeList_Literal("1");
				}
				else {
					map_netlist_datapath_t* Datapath_Enable = Map_Netlist_Datapath_Expr(map_data, Expr_Enable->ref.elt);
					datapath->sub_datapaths = addchain(datapath->sub_datapaths, Datapath_Enable);
					valcat_en = Map_Netlist_ValCat_DupCropToRef(Datapath_Enable->valcat, Expr_Enable);
				}
				netlist_comp_t* compMuxEn = Map_Netlist_GetSourceMux(compReg_data->port_en);
				Map_Netlist_Mux_GetAddInput_SetState(map_data, datapath, compMuxEn, valcat_en);
				Netlist_ValCat_FreeList(valcat_en);
			}
			// Error
			else {
				abort();
			}
		}

		// Check if the symbol is a port of the entity
		else if(target_port!=NULL) {
			assert(target_port->direction==NETLIST_PORT_DIR_OUT);

			Map_Netlist_Port_MuxGetAddInput_SetState_FromExpr(
				map_data, datapath, target_port,
				Expr_Value, Datapath_Value->valcat
			);

		}

		// Error
		else {
			abort();
		}

	}  // End case for register, signal, port.

	// Memory
	else {
		assert(target_comp!=NULL || topaccess!=NULL);

		if(topaccess!=NULL && topaccess->model == NETLIST_ACCESS_MEM_WA_EXT) {
			if(avl_p_isthere(&map_data->cur_state.hwusage.mem_accesses_used, topaccess)==true) abort();

			map_netlist_datapath_t* Datapath_Address = Map_Netlist_Datapath_Expr(map_data, Expr_Address->ref.elt);
			datapath->sub_datapaths = addchain(datapath->sub_datapaths, Datapath_Address);

			netlist_access_mem_addr_t* memWA_data = topaccess->data;

			// Handle Data MUX.
			Map_Netlist_Port_MuxGetAddInput_SetState_FromExpr(
				map_data, datapath, memWA_data->port_d,
				Expr_Value, Datapath_Value->valcat
			);

			// Handle Address MUX.
			Map_Netlist_Port_MuxGetAddInput_SetState_FromExpr(
				map_data, datapath, memWA_data->port_a,
				Expr_Address, Datapath_Address->valcat
			);

			// Handle Enable MUX.
			netlist_comp_t* compMuxEn = Map_Netlist_GetSourceMux(memWA_data->port_e);
			chain_list* valcat_en = NULL;
			if(Expr_Enable==NULL) {
				valcat_en = Netlist_ValCat_MakeList_Literal("1");
			}
			else {
				map_netlist_datapath_t* Datapath_Enable = Map_Netlist_Datapath_Expr(map_data, Expr_Enable->ref.elt);
				datapath->sub_datapaths = addchain(datapath->sub_datapaths, Datapath_Enable);
				valcat_en = Map_Netlist_ValCat_DupCropToRef(Datapath_Enable->valcat, Expr_Enable);
			}
			Map_Netlist_Mux_GetAddInput_SetState(map_data, datapath, compMuxEn, valcat_en);
			Netlist_ValCat_FreeList(valcat_en);

			// Flag the access as used. Note: for now, it's unuseful because there is only one access
			avl_p_add_overwrite(&datapath->hwusage.mem_accesses_used, access, access);

		}  // End case memory

		else if (target_comp!=NULL && target_comp->model == NETLIST_COMP_MEMORY) {
			netlist_memory_t* compMem_data = target_comp->data;

			// If the address is a literal and the component has direct access,
			// use direct access ports.
			if(Expr_Address->ref.elt->model==HVEX_LITERAL && compMem_data->direct_en==true) {

				unsigned index = VexAlloc_Lit2Uint(Expr_Address->ref.elt);
				netlist_access_t* access = Netlist_Mem_GetDirectAccess(target_comp, index);
				assert(access!=NULL);
				netlist_access_mem_direct_t* memWD_data = access->data;

				// Get an input of the Data MUX.
				Map_Netlist_Port_MuxGetAddInput_SetState_FromExpr(
					map_data, datapath, memWD_data->port_wd,
					Expr_Value, Datapath_Value->valcat
				);

				// Get an input of the Enable MUX.
				netlist_comp_t* compMuxEn = Map_Netlist_GetSourceMux(memWD_data->port_we);
				chain_list* valcat_en = NULL;
				if(Expr_Enable==NULL) {
					valcat_en = Netlist_ValCat_MakeList_Literal("1");
				}
				else {
					map_netlist_datapath_t* Datapath_Enable = Map_Netlist_Datapath_Expr(map_data, Expr_Enable->ref.elt);
					datapath->sub_datapaths = addchain(datapath->sub_datapaths, Datapath_Enable);
					valcat_en = Map_Netlist_ValCat_DupCropToRef(Datapath_Enable->valcat, Expr_Enable);
				}
				Map_Netlist_Mux_GetAddInput_SetState(map_data, datapath, compMuxEn, valcat_en);
				Netlist_ValCat_FreeList(valcat_en);

			}  // Direct access

			// Use an access by address
			else {
				map_netlist_datapath_t* Datapath_Address = Map_Netlist_Datapath_Expr(map_data, Expr_Address->ref.elt);
				datapath->sub_datapaths = addchain(datapath->sub_datapaths, Datapath_Address);

				netlist_access_t* access = NULL;

				// Get an available access
				avl_p_foreach(&compMem_data->access_ports_wa, scanWA) {
					netlist_access_t* local_access = scanWA->data;
					bool b = avl_p_isthere(&map_data->cur_state.hwusage.mem_accesses_used, local_access);
					if(b==true) continue;
					access = local_access;
					break;
				}
				assert(access!=NULL);
				netlist_access_mem_addr_t* memWA_data = access->data;

				// Handle Data MUX.
				Map_Netlist_Port_MuxGetAddInput_SetState_FromExpr(
					map_data, datapath, memWA_data->port_d,
					Expr_Value, Datapath_Value->valcat
				);

				// Handle Address MUX.
				Map_Netlist_Port_MuxGetAddInput_SetState_FromExpr(
					map_data, datapath, memWA_data->port_a,
					Expr_Address, Datapath_Address->valcat
				);

				// Handle Enable MUX.
				netlist_comp_t* compMuxEn = Map_Netlist_GetSourceMux(memWA_data->port_e);
				chain_list* valcat_en = NULL;
				if(Expr_Enable==NULL) {
					valcat_en = Netlist_ValCat_MakeList_Literal("1");
				}
				else {
					map_netlist_datapath_t* Datapath_Enable = Map_Netlist_Datapath_Expr(map_data, Expr_Enable->ref.elt);
					datapath->sub_datapaths = addchain(datapath->sub_datapaths, Datapath_Enable);
					valcat_en = Map_Netlist_ValCat_DupCropToRef(Datapath_Enable->valcat, Expr_Enable);
				}
				Map_Netlist_Mux_GetAddInput_SetState(map_data, datapath, compMuxEn, valcat_en);
				Netlist_ValCat_FreeList(valcat_en);

				// Flag the access as used
				avl_p_add_overwrite(&datapath->hwusage.mem_accesses_used, access, access);
			}  // Access by address

		}  // End case memory

		// Ping-Pong buffer
		else if (target_comp!=NULL &&
			(target_comp->model == NETLIST_COMP_PINGPONG_IN || target_comp->model == NETLIST_COMP_PINGPONG_OUT)
		){
			netlist_pingpong_t* compPP_data = target_comp->data;

			// If the address is a literal and the component has direct access,
			// use direct access ports.
			if(Expr_Address->ref.elt->model==HVEX_LITERAL && compPP_data->is_direct==true) {

				unsigned index = VexAlloc_Lit2Uint(Expr_Address->ref.elt);
				netlist_access_t* access = Netlist_PingPong_GetDirectAccess(target_comp, index);
				assert(access!=NULL);
				netlist_access_mem_direct_t* memWD_data = access->data;

				// Get an input of the Data MUX.
				Map_Netlist_Port_MuxGetAddInput_SetState_FromExpr(
					map_data, datapath, memWD_data->port_wd,
					Expr_Value, Datapath_Value->valcat
				);

				// Get an input of the Enable MUX.
				netlist_comp_t* compMuxEn = Map_Netlist_GetSourceMux(memWD_data->port_we);
				chain_list* valcat_en = NULL;
				if(Expr_Enable==NULL) {
					valcat_en = Netlist_ValCat_MakeList_Literal("1");
				}
				else {
					map_netlist_datapath_t* Datapath_Enable = Map_Netlist_Datapath_Expr(map_data, Expr_Enable->ref.elt);
					datapath->sub_datapaths = addchain(datapath->sub_datapaths, Datapath_Enable);
					valcat_en = Map_Netlist_ValCat_DupCropToRef(Datapath_Enable->valcat, Expr_Enable);
				}
				Map_Netlist_Mux_GetAddInput_SetState(map_data, datapath, compMuxEn, valcat_en);
				Netlist_ValCat_FreeList(valcat_en);

			}  // Direct access

			// Use an access by address
			else {
				map_netlist_datapath_t* Datapath_Address = Map_Netlist_Datapath_Expr(map_data, Expr_Address->ref.elt);
				datapath->sub_datapaths = addchain(datapath->sub_datapaths, Datapath_Address);

				netlist_access_t* access = NULL;

				// Get an available access
				avl_p_foreach(&compPP_data->access_ports_wa, scanWA) {
					netlist_access_t* local_access = scanWA->data;
					bool b = avl_p_isthere(&map_data->cur_state.hwusage.mem_accesses_used, local_access);
					if(b==true) continue;
					access = local_access;
					break;
				}
				assert(access!=NULL);
				netlist_access_mem_addr_t* memWA_data = access->data;

				// Handle Data MUX.
				Map_Netlist_Port_MuxGetAddInput_SetState_FromExpr(
					map_data, datapath, memWA_data->port_d,
					Expr_Value, Datapath_Value->valcat
				);

				// Handle Address MUX.
				Map_Netlist_Port_MuxGetAddInput_SetState_FromExpr(
					map_data, datapath, memWA_data->port_a,
					Expr_Address, Datapath_Address->valcat
				);

				// Handle Enable MUX.
				netlist_comp_t* compMuxEn = Map_Netlist_GetSourceMux(memWA_data->port_e);
				chain_list* valcat_en = NULL;
				if(Expr_Enable==NULL) {
					valcat_en = Netlist_ValCat_MakeList_Literal("1");
				}
				else {
					map_netlist_datapath_t* Datapath_Enable = Map_Netlist_Datapath_Expr(map_data, Expr_Enable->ref.elt);
					datapath->sub_datapaths = addchain(datapath->sub_datapaths, Datapath_Enable);
					valcat_en = Map_Netlist_ValCat_DupCropToRef(Datapath_Enable->valcat, Expr_Enable);
				}
				Map_Netlist_Mux_GetAddInput_SetState(map_data, datapath, compMuxEn, valcat_en);
				Netlist_ValCat_FreeList(valcat_en);

				// Flag the access as used
				avl_p_add_overwrite(&datapath->hwusage.mem_accesses_used, access, access);
			}

		}  // End case Ping-Pong

		else {
			abort();
		}

	}  // End of case memory

	return datapath;
}

static map_netlist_datapath_t* Map_Hvex_CB_Concat(map_netlist_data_t* map_data, hvex_dico_elt_t* Expr) {
	map_netlist_datapath_t* datapath = Map_Netlist_Datapath_New();
	datapath->Expr = Expr;

	foreach(Expr->args, scanArg) {
		hvex_dico_ref_t* Expr_operand = scanArg->DATA;

		map_netlist_datapath_t* Datapath_operand = Map_Netlist_Datapath_Expr(map_data, Expr_operand->ref.elt);
		datapath->sub_datapaths = addchain(datapath->sub_datapaths, Datapath_operand);

		chain_list* valcat_dup = Map_Netlist_ValCat_DupResizeToRef(Datapath_operand->valcat, Expr_operand);
		datapath->valcat = append(datapath->valcat, valcat_dup);
	}

	return datapath;
}

static map_netlist_datapath_t* Map_Hvex_CB_Repeat(map_netlist_data_t* map_data, hvex_dico_elt_t* Expr) {
	hvex_dico_ref_t* Expr_operand = Expr->args->DATA;

	map_netlist_datapath_t* datapath = Map_Netlist_Datapath_New();
	datapath->Expr = Expr;

	map_netlist_datapath_t* Datapath_operand = Map_Netlist_Datapath_Expr(map_data, Expr_operand->ref.elt);
	datapath->sub_datapaths = addchain(datapath->sub_datapaths, Datapath_operand);

	chain_list* valcat_ref = Map_Netlist_ValCat_DupResizeToRef(Datapath_operand->valcat, Expr_operand);
	assert(Netlist_ValCat_ListWidth(valcat_ref)==1);

	for(unsigned i=0; i<Expr->width; i++) {
		chain_list* valcat_dup = Netlist_ValCat_DupList(valcat_ref);
		datapath->valcat = append(datapath->valcat, valcat_dup);
	}
	Netlist_ValCat_FreeList(valcat_ref);

	datapath->valcat = Netlist_ValCat_Simplify(datapath->valcat);

	return datapath;
}

static map_netlist_datapath_t* Map_Hvex_CB_Resize(map_netlist_data_t* map_data, hvex_dico_elt_t* Expr) {
	hvex_dico_ref_t* Expr_operand = Expr->args->DATA;

	map_netlist_datapath_t* datapath = Map_Netlist_Datapath_New();
	datapath->Expr = Expr;

	map_netlist_datapath_t* Datapath_operand = Map_Netlist_Datapath_Expr(map_data, Expr_operand->ref.elt);
	datapath->sub_datapaths = addchain(datapath->sub_datapaths, Datapath_operand);

	datapath->valcat = Map_Netlist_ValCat_DupResize(Datapath_operand->valcat, Expr_operand, Expr->width);

	return datapath;
}

// An internal function used by callbacks ADD, SUB, NEG

static void Map_Netlist_Datapath_BuildNew_Asb_BuildOut(
	map_netlist_data_t* map_data, map_netlist_datapath_t* datapath, netlist_comp_t* comp
) {
	netlist_asb_t* comp_asb = comp->data;
	hvex_dico_elt_t* Expr = datapath->Expr;

	if(Expr->left <= comp_asb->width-1) {
		// Here we are reading only the sum output
		datapath->valcat = Netlist_ValCat_MakeList_FullPort(comp_asb->port_out);
		netlist_val_cat_t* valcat_elt = datapath->valcat->DATA;
		valcat_elt->width = Expr->width;
		valcat_elt->left = Expr->left;
		valcat_elt->right = Expr->right;
	}
	else if(Expr->right == comp_asb->width) {
		// Here we are reading only the CO output
		assert(Expr->width == 1);
		datapath->valcat = Netlist_ValCat_MakeList_FullPort(comp_asb->port_co);
	}
	else {
		// Here we are reading both the CO and the sum outputs
		assert(Expr->left == comp_asb->width);
		assert(comp_asb->port_co != NULL);

		netlist_val_cat_t* valcat_out = Netlist_ValCat_Make_FullPort(comp_asb->port_out);
		valcat_out->width = Expr->width - 1;
		valcat_out->left = Expr->left - 1;
		valcat_out->right = Expr->right;

		netlist_val_cat_t* valcat_co = Netlist_ValCat_Make_FullPort(comp_asb->port_co);

		datapath->valcat = addchain(NULL, valcat_out);
		datapath->valcat = addchain(datapath->valcat, valcat_co);
	}
}

static void Map_Netlist_Datapath_BuildNew_Asb(
	map_netlist_data_t* map_data, map_netlist_datapath_t* datapath, netlist_comp_t* comp,
	chain_list* valcat_A, hvex_dico_ref_t *Expr_A,
	chain_list* valcat_B, hvex_dico_ref_t *Expr_B,
	chain_list* valcat_Cin, hvex_dico_ref_t *Expr_Cin
) {
	netlist_asb_t* comp_asb = comp->data;

	// Do sign extension for each input, and get the MUX inputs

	// Input A

	Map_Netlist_Port_MuxGetAddInput_SetState_FromExpr(
		map_data, datapath, comp_asb->port_inA, Expr_A, valcat_A
	);

	// Input B

	Map_Netlist_Port_MuxGetAddInput_SetState_FromExpr(
		map_data, datapath, comp_asb->port_inB, Expr_B, valcat_B
	);

	// The carry input

	if(valcat_Cin!=NULL) {
		assert(comp_asb->port_ci!=NULL);
		Map_Netlist_Port_MuxGetAddInput_SetState_FromExpr(
			map_data, datapath, comp_asb->port_ci, Expr_Cin, valcat_Cin
		);
	}
	else if(comp_asb->port_ci!=NULL) {
		valcat_Cin = Netlist_ValCat_MakeList_Literal(stralloc("0"));
		netlist_comp_t* mux = Map_Netlist_GetSourceMux(comp_asb->port_ci);
		Map_Netlist_Mux_GetAddInput_SetState(map_data, datapath, mux, valcat_Cin);
		Netlist_ValCat_FreeList(valcat_Cin);
	}

	// Build the output ValCat expression
	Map_Netlist_Datapath_BuildNew_Asb_BuildOut(map_data, datapath, comp);
}

static void Map_Netlist_Datapath_BuildNew_Asb_NoShare(
	map_netlist_data_t* map_data, map_netlist_datapath_t* datapath, netlist_comp_t* comp,
	chain_list* valcat_A, hvex_dico_ref_t *Expr_A,
	chain_list* valcat_B, hvex_dico_ref_t *Expr_B,
	chain_list* valcat_Cin, hvex_dico_ref_t *Expr_Cin
) {
	netlist_asb_t* comp_asb = comp->data;

	// Do sign extension for each input, and get the MUX inputs

	// Input A

	assert(comp_asb->port_inA->source==NULL);
	comp_asb->port_inA->source = Map_Netlist_ValCat_DupResize(valcat_A, Expr_A, comp_asb->port_inA->width);

	// Input B

	assert(comp_asb->port_inB->source==NULL);
	comp_asb->port_inB->source = Map_Netlist_ValCat_DupResize(valcat_B, Expr_B, comp_asb->port_inB->width);

	// The carry input

	if(valcat_Cin!=NULL) {
		assert(comp_asb->port_ci!=NULL);
		assert(comp_asb->port_ci->source==NULL);
		comp_asb->port_ci->source = Map_Netlist_ValCat_DupResize(valcat_Cin, Expr_Cin, comp_asb->port_ci->width);
	}
	else if(comp_asb->port_ci!=NULL) {
		assert(comp_asb->port_ci->source==NULL);
		comp_asb->port_ci->source = Netlist_ValCat_MakeList_Literal(stralloc("0"));
	}

	// Build the output ValCat expression
	Map_Netlist_Datapath_BuildNew_Asb_BuildOut(map_data, datapath, comp);
}

static map_netlist_datapath_t* Map_Hvex_CB_Add(map_netlist_data_t* map_data, hvex_dico_elt_t* Expr) {
	assert(Expr->args_nb==2);

	map_netlist_datapath_t* datapath = Map_Netlist_Datapath_New();
	datapath->Expr = Expr;

	hvex_dico_ref_t* Expr_A = Expr->args->DATA;
	map_netlist_datapath_t* Datapath_A = Map_Netlist_Datapath_Expr(map_data, Expr_A->ref.elt);
	datapath->sub_datapaths = addchain(datapath->sub_datapaths, Datapath_A);

	hvex_dico_ref_t* Expr_B = Expr->args->NEXT->DATA;
	map_netlist_datapath_t* Datapath_B = Map_Netlist_Datapath_Expr(map_data, Expr_B->ref.elt);
	datapath->sub_datapaths = addchain(datapath->sub_datapaths, Datapath_B);

	unsigned needed_width = GetMax(Expr_A->width, Expr_B->width);
	// FIXME This could be optimized when operands are shorter, and unsigned
	needed_width = GetMax(needed_width, Expr->left+1);

	// Get an available operator, with enough width.
	netlist_comp_t* comp = NULL;

	if( (NETLIST_COMP_ADD->flags & NETLIST_COMPMOD_FLAG_CANSHARE)==0 ) {
		// Create a new component
		char* name = Map_Netlist_MakeInstanceName(map_data->Implem, NETLIST_COMP_ADD);
		comp = Netlist_Add_New(name, needed_width, true, true);
		Netlist_Asb_SetDefaultCI(comp, '0');
		Netlist_Comp_SetChild(map_data->Implem->netlist.top, comp);
		Map_CompType_AddComp(map_data, comp);
	}
	else {
		// Find a suitable component
		map_comptype_t* comptype = Map_CompType_GetAdd(map_data, NETLIST_COMP_ADD->name);
		avl_p_foreach(&comptype->all, scan) {
			netlist_comp_t* comp1 = scan->data;
			bool is_used = avl_p_isthere(&map_data->cur_state.hwusage.comp_used, comp1);
			if(is_used==true) continue;

			netlist_asb_t* comp_asb = comp1->data;
			if(comp_asb->width < needed_width) continue;

			comp = comp1;
			break;
		}
	}

	if(comp==NULL) {
		Map_Netlist_Datapath_BuildNew_Op_error(map_data, datapath, Expr->model, NETLIST_COMP_ADD, __LINE__);
	}

	// Flag the component as used
	avl_p_add_overwrite(&datapath->hwusage.comp_used, comp, comp);

	// Use the mapping function associated to the ASB component.

	if( (NETLIST_COMP_ADD->flags & NETLIST_COMPMOD_FLAG_CANSHARE)==0 ) {
		Map_Netlist_Datapath_BuildNew_Asb_NoShare(
			map_data, datapath, comp,
			Datapath_A->valcat, Expr_A,
			Datapath_B->valcat, Expr_B,
			NULL, NULL
		);
	}
	else {
		Map_Netlist_Datapath_BuildNew_Asb(
			map_data, datapath, comp,
			Datapath_A->valcat, Expr_A,
			Datapath_B->valcat, Expr_B,
			NULL, NULL
		);
	}

	return datapath;
}

static map_netlist_datapath_t* Map_Hvex_CB_Sub(map_netlist_data_t* map_data, hvex_dico_elt_t* Expr) {
	map_netlist_datapath_t* datapath = Map_Netlist_Datapath_New();
	datapath->Expr = Expr;

	// Add some padding for input MSB if needed.
	// The output width can be one bit larger (the carry out will be used).

	// Get the expressions to assign

	hvex_dico_ref_t* Expr_A = Expr->args->DATA;
	map_netlist_datapath_t* Datapath_A = Map_Netlist_Datapath_Expr(map_data, Expr_A->ref.elt);
	datapath->sub_datapaths = addchain(datapath->sub_datapaths, Datapath_A);

	hvex_dico_ref_t* Expr_B = Expr->args->NEXT->DATA;
	map_netlist_datapath_t* Datapath_B = Map_Netlist_Datapath_Expr(map_data, Expr_B->ref.elt);
	datapath->sub_datapaths = addchain(datapath->sub_datapaths, Datapath_B);

	unsigned needed_width = GetMax(Expr_A->width, Expr_B->width);
	// FIXME This could be optimized when operands are shorter, and unsigned  (sign extend from the CO)
	needed_width = GetMax(needed_width, Expr->left+1);

	// Get an available operator, with enough width.
	netlist_comp_t* comp = NULL;

	if( (NETLIST_COMP_SUB->flags & NETLIST_COMPMOD_FLAG_CANSHARE)==0 ) {
		// Create a new component
		char* name = Map_Netlist_MakeInstanceName(map_data->Implem, NETLIST_COMP_SUB);
		comp = Netlist_Sub_New(name, needed_width, true, true);
		Netlist_Asb_SetDefaultCI(comp, '0');
		Netlist_Comp_SetChild(map_data->Implem->netlist.top, comp);
		Map_CompType_AddComp(map_data, comp);
	}
	else {
		// Get an existing component
		map_comptype_t* comptype = Map_CompType_GetAdd(map_data, NETLIST_COMP_SUB->name);
		avl_p_foreach(&comptype->all, scan) {
			netlist_comp_t* comp1 = scan->data;
			bool is_used = avl_p_isthere(&map_data->cur_state.hwusage.comp_used, comp1);
			if(is_used==true) continue;

			netlist_asb_t* comp_asb = comp1->data;
			if(comp_asb->width < needed_width) continue;

			comp = comp1;
			break;
		}
	}

	if(comp==NULL) {
		Map_Netlist_Datapath_BuildNew_Op_error(map_data, datapath, Expr->model, NETLIST_COMP_SUB, __LINE__);
	}

	// Flag the component as used
	avl_p_add_overwrite(&datapath->hwusage.comp_used, comp, comp);

	// Use the mapping function associated to the ASB component.

	if( (NETLIST_COMP_SUB->flags & NETLIST_COMPMOD_FLAG_CANSHARE)==0 ) {
		Map_Netlist_Datapath_BuildNew_Asb_NoShare(
			map_data, datapath, comp,
			Datapath_A->valcat, Expr_A,
			Datapath_B->valcat, Expr_B,
			NULL, NULL
		);
	}
	else {
		Map_Netlist_Datapath_BuildNew_Asb(
			map_data, datapath, comp,
			Datapath_A->valcat, Expr_A,
			Datapath_B->valcat, Expr_B,
			NULL, NULL
		);
	}

	return datapath;
}

static map_netlist_datapath_t* Map_Hvex_CB_Neg(map_netlist_data_t* map_data, hvex_dico_elt_t* Expr) {
	map_netlist_datapath_t* datapath = Map_Netlist_Datapath_New();
	datapath->Expr = Expr;

	// Add some padding for input MSB if needed.
	// The output width can be one bit larger (the carry out will be used).

	// Get the expressions to assign

	// Create a dummy expression in the dictionary
	hvex_t* vex_A = hvex_newlit_bit('0');
	hvex_dico_ref_t* Expr_A = VexAlloc_Add(&map_data->expr_dictionary, vex_A);
	map_netlist_datapath_t* Datapath_A = Map_Netlist_Datapath_Expr(map_data, Expr_A->ref.elt);
	// Note: Not needed to add this datapath in dependencies because it is unconditionally available and "shareable"
	hvex_free(vex_A);

	hvex_dico_ref_t* Expr_B = Expr->args->DATA;
	map_netlist_datapath_t* Datapath_B = Map_Netlist_Datapath_Expr(map_data, Expr_B->ref.elt);
	datapath->sub_datapaths = addchain(datapath->sub_datapaths, Datapath_B);

	unsigned needed_width = Expr_B->width;
	// FIXME This could be optimized when operand is shorter, and unsigned (sign extend from the CO)
	needed_width = GetMax(needed_width, Expr->left+1);

	// Get an available operator, with enough width.
	netlist_comp_t* comp = NULL;

	if( (NETLIST_COMP_SUB->flags & NETLIST_COMPMOD_FLAG_CANSHARE)==0 ) {
		// Create a new component
		char* name = Map_Netlist_MakeInstanceName(map_data->Implem, NETLIST_COMP_SUB);
		comp = Netlist_Sub_New(name, needed_width, true, true);
		Netlist_Asb_SetDefaultCI(comp, '0');
		Netlist_Comp_SetChild(map_data->Implem->netlist.top, comp);
		Map_CompType_AddComp(map_data, comp);
	}
	else {
		// Get an existing component
		map_comptype_t* comptype = Map_CompType_GetAdd(map_data, NETLIST_COMP_SUB->name);
		avl_p_foreach(&comptype->all, scan) {
			netlist_comp_t* comp1 = scan->data;
			bool is_used = avl_p_isthere(&map_data->cur_state.hwusage.comp_used, comp1);
			if(is_used==true) continue;

			netlist_asb_t* comp_asb = comp1->data;
			if(comp_asb->width < needed_width) continue;

			comp = comp1;
			break;
		}
	}

	if(comp==NULL) {
		Map_Netlist_Datapath_BuildNew_Op_error(map_data, datapath, Expr->model, NETLIST_COMP_SUB, __LINE__);
	}

	// Flag the component as used
	avl_p_add_overwrite(&datapath->hwusage.comp_used, comp, comp);

	// Use the mapping function associated to the ASB component.

	if( (NETLIST_COMP_SUB->flags & NETLIST_COMPMOD_FLAG_CANSHARE)==0 ) {
		Map_Netlist_Datapath_BuildNew_Asb_NoShare(
			map_data, datapath, comp,
			Datapath_A->valcat, Expr_A,
			Datapath_B->valcat, Expr_B,
			NULL, NULL
		);
	}
	else {
		Map_Netlist_Datapath_BuildNew_Asb(
			map_data, datapath, comp,
			Datapath_A->valcat, Expr_A,
			Datapath_B->valcat, Expr_B,
			NULL, NULL
		);
	}

	return datapath;
}

// Works for signed and unsigned multiplications.
// Generates only signed multiplications.
static map_netlist_datapath_t* Map_Hvex_CB_Mul(map_netlist_data_t* map_data, hvex_dico_elt_t* Expr) {

	// Note : The inputs may have different widthes.
	// The MUL inputs are selected to best fit the widthes.

	assert(Expr->args_nb==2);

	map_netlist_datapath_t* datapath = Map_Netlist_Datapath_New();
	datapath->Expr = Expr;

	// Get the expressions to assign

	hvex_dico_ref_t* Expr_wide  = datapath->Expr->args->DATA;
	hvex_dico_ref_t* Expr_short = datapath->Expr->args->NEXT->DATA;

	map_netlist_datapath_t* Datapath_wide  = Map_Netlist_Datapath_Expr(map_data, Expr_wide->ref.elt);
	map_netlist_datapath_t* Datapath_short = Map_Netlist_Datapath_Expr(map_data, Expr_short->ref.elt);
	datapath->sub_datapaths = addchain(datapath->sub_datapaths, Datapath_wide);
	datapath->sub_datapaths = addchain(datapath->sub_datapaths, Datapath_short);

	unsigned width_wide  = Expr_wide->width;
	unsigned width_short = Expr_short->width;

	// Take into account 1-bit sign extension, for unsigned expressions
	// Only if needed
	if(width_wide < datapath->Expr->left+1 && Expr_wide->extend!=0) {
		if(VexAllocRef_GetLeftBit_Char(Expr_wide)!='0') width_wide++;
	}
	if(width_short < datapath->Expr->left+1 && Expr_short->extend!=0) {
		if(VexAllocRef_GetLeftBit_Char(Expr_short)!='0') width_short++;
	}

	if(width_wide < width_short) {
		Swap(Expr_wide, Expr_short);
		Swap(Datapath_wide, Datapath_short);
		Swap(width_wide, width_short);
	}

	// Get an available operator, with enough width.
	netlist_comp_t* comp = NULL;

	if( (NETLIST_COMP_MUL->flags & NETLIST_COMPMOD_FLAG_CANSHARE)==0 ) {
		// Create a new component
		char* name = Map_Netlist_MakeInstanceName(map_data->Implem, NETLIST_COMP_MUL);
		comp = Netlist_Mul_New(name, width_wide, width_short, Expr->left+1, true);
		Netlist_Comp_SetChild(map_data->Implem->netlist.top, comp);
		Map_CompType_AddComp(map_data, comp);
	}
	else {
		// Get a suitable component
		map_comptype_t* comptype = Map_CompType_GetAdd(map_data, NETLIST_COMP_MUL->name);
		avl_p_foreach(&comptype->all, scan) {
			netlist_comp_t* comp1 = scan->data;
			bool is_used = avl_p_isthere(&map_data->cur_state.hwusage.comp_used, comp1);
			if(is_used==true) continue;

			netlist_mul_t* comp_mul = comp1->data;
			if(comp_mul->is_signed==false) continue;
			if(datapath->Expr->left > comp_mul->width_out-1) continue;

			// Get the input ports, short and wide
			netlist_port_t* port_wide = comp_mul->port_inA;
			netlist_port_t* port_short = comp_mul->port_inB;
			if(port_wide->width < port_short->width) Swap(port_wide, port_short);

			if(width_short > port_short->width) continue;
			if(width_wide > port_wide->width) continue;

			comp = comp1;
			break;
		}
	}

	if(comp==NULL) {
		Map_Netlist_Datapath_BuildNew_Op_error(
			map_data, datapath, datapath->Expr->model, NETLIST_COMP_MUL, __LINE__
		);
	}

	// Flag the component as used
	avl_p_add_overwrite(&datapath->hwusage.comp_used, comp, comp);

	netlist_mul_t* comp_mul = comp->data;
	// Get the smallest and the biggest inputs
	netlist_port_t* port_wide = comp_mul->port_inA;
	netlist_port_t* port_short = comp_mul->port_inB;
	if(port_wide->width < port_short->width) {
		netlist_port_t* port_tmp = port_wide;
		port_wide = port_short;
		port_short = port_tmp;
	}
	assert(width_short <= port_short->width);
	assert(width_wide <= port_wide->width);

	// Do sign extension for each input, and get the MUX inputs.

	if( (NETLIST_COMP_MUL->flags & NETLIST_COMPMOD_FLAG_CANSHARE)==0 ) {
		assert(comp_mul->port_inA->source==NULL);
		assert(comp_mul->port_inB->source==NULL);
		comp_mul->port_inA->source = Map_Netlist_ValCat_DupResize(Datapath_wide->valcat, Expr_wide, comp_mul->port_inA->width);
		comp_mul->port_inB->source = Map_Netlist_ValCat_DupResize(Datapath_short->valcat, Expr_short, comp_mul->port_inB->width);
	}
	else {
		Map_Netlist_Port_MuxGetAddInput_SetState_FromExpr(
			map_data, datapath, port_wide,
			Expr_wide, Datapath_wide->valcat
		);

		Map_Netlist_Port_MuxGetAddInput_SetState_FromExpr(
			map_data, datapath, port_short,
			Expr_short, Datapath_short->valcat
		);
	}

	// Build the output ValCat expression

	netlist_val_cat_t* valcat_out = Netlist_ValCat_Make_FullPort(comp_mul->port_out);
	valcat_out->width = datapath->Expr->width;
	valcat_out->left = datapath->Expr->left;
	valcat_out->right = datapath->Expr->right;

	datapath->valcat = addchain(NULL, valcat_out);

	return datapath;
}

// Works for DIVQ and DIVR
static map_netlist_datapath_t* Map_Hvex_CB_DivQR(map_netlist_data_t* map_data, hvex_dico_elt_t* Expr) {
	assert(Expr->args_nb==2);
	assert(Expr->model==HVEX_DIVQ || Expr->model==HVEX_DIVR);

	map_netlist_datapath_t* datapath = Map_Netlist_Datapath_New();
	datapath->Expr = Expr;

	// Get the expressions to assign

	hvex_dico_ref_t* Expr_num = datapath->Expr->args->DATA;
	hvex_dico_ref_t* Expr_den = datapath->Expr->args->NEXT->DATA;

	map_netlist_datapath_t* Datapath_num = Map_Netlist_Datapath_Expr(map_data, Expr_num->ref.elt);
	map_netlist_datapath_t* Datapath_den = Map_Netlist_Datapath_Expr(map_data, Expr_den->ref.elt);
	datapath->sub_datapaths = addchain(datapath->sub_datapaths, Datapath_num);
	datapath->sub_datapaths = addchain(datapath->sub_datapaths, Datapath_den);

	unsigned width_num = Expr_num->width;
	unsigned width_den = Expr_den->width;

	// Get an available operator, with enough width.
	netlist_comp_t* comp = NULL;

	if( (NETLIST_COMP_DIVQR->flags & NETLIST_COMPMOD_FLAG_CANSHARE)==0 ) {
		// Create a new component
		char* name = Map_Netlist_MakeInstanceName(map_data->Implem, NETLIST_COMP_DIVQR);
		comp = Netlist_DivQR_New(name, width_num, width_den);
		Netlist_Comp_SetChild(map_data->Implem->netlist.top, comp);
		Map_CompType_AddComp(map_data, comp);
	}
	else {
		// Get a suitable component
		map_comptype_t* comptype = Map_CompType_GetAdd(map_data, NETLIST_COMP_DIVQR->name);
		avl_p_foreach(&comptype->all, scan) {
			netlist_comp_t* comp1 = scan->data;
			bool is_used = avl_p_isthere(&map_data->cur_state.hwusage.comp_used, comp1);
			if(is_used==true) continue;

			netlist_divqr_t* comp_div = comp1->data;

			if(Expr->model==HVEX_DIVQ) {
				if(Expr->left > comp_div->port_out_quo->width) continue;
			}
			else {
				if(Expr->left > comp_div->port_out_rem->width) continue;
			}

			if(width_num > comp_div->port_in_num->width) continue;
			if(width_den > comp_div->port_in_den->width) continue;

			comp = comp1;
			break;
		}
	}

	if(comp==NULL) {
		Map_Netlist_Datapath_BuildNew_Op_error(
			map_data, datapath, datapath->Expr->model, NETLIST_COMP_DIVQR, __LINE__
		);
	}

	// Flag the component as used
	avl_p_add_overwrite(&datapath->hwusage.comp_used, comp, comp);

	netlist_divqr_t* comp_div = comp->data;

	// Do sign extension for each input, and get the MUX inputs.

	if( (NETLIST_COMP_DIVQR->flags & NETLIST_COMPMOD_FLAG_CANSHARE)==0 ) {
		assert(comp_div->port_in_num->source==NULL);
		assert(comp_div->port_in_den->source==NULL);
		comp_div->port_in_num->source = Map_Netlist_ValCat_DupResize(Datapath_num->valcat, Expr_num, comp_div->port_in_num->width);
		comp_div->port_in_den->source = Map_Netlist_ValCat_DupResize(Datapath_den->valcat, Expr_den, comp_div->port_in_den->width);
	}
	else {
		Map_Netlist_Port_MuxGetAddInput_SetState_FromExpr(
			map_data, datapath, comp_div->port_in_num,
			Expr_num, Datapath_num->valcat
		);

		Map_Netlist_Port_MuxGetAddInput_SetState_FromExpr(
			map_data, datapath, comp_div->port_in_den,
			Expr_den, Datapath_den->valcat
		);
	}

	// Build the output ValCat expression

	netlist_port_t* port_out = NULL;
	if(Expr->model==HVEX_DIVQ) port_out = comp_div->port_out_quo;
	else port_out = comp_div->port_out_rem;

	netlist_val_cat_t* valcat_out = Netlist_ValCat_Make_FullPort(port_out);
	valcat_out->width = datapath->Expr->width;
	valcat_out->left = datapath->Expr->left;
	valcat_out->right = datapath->Expr->right;

	datapath->valcat = addchain(NULL, valcat_out);

	return datapath;
}

static map_netlist_datapath_t* Map_Hvex_CB_AddCin(map_netlist_data_t* map_data, hvex_dico_elt_t* Expr) {
	assert(Expr->args_nb==3);

	map_netlist_datapath_t* datapath = Map_Netlist_Datapath_New();
	datapath->Expr = Expr;

	hvex_dico_ref_t* Expr_A = Expr->args->DATA;
	map_netlist_datapath_t* Datapath_A = Map_Netlist_Datapath_Expr(map_data, Expr_A->ref.elt);
	datapath->sub_datapaths = addchain(datapath->sub_datapaths, Datapath_A);

	hvex_dico_ref_t* Expr_B = Expr->args->NEXT->DATA;
	map_netlist_datapath_t* Datapath_B = Map_Netlist_Datapath_Expr(map_data, Expr_B->ref.elt);
	datapath->sub_datapaths = addchain(datapath->sub_datapaths, Datapath_B);

	hvex_dico_ref_t* Expr_Cin = Expr->args->NEXT->NEXT->DATA;
	map_netlist_datapath_t* Datapath_Cin = Map_Netlist_Datapath_Expr(map_data, Expr_Cin->ref.elt);
	datapath->sub_datapaths = addchain(datapath->sub_datapaths, Datapath_Cin);

	unsigned needed_width = GetMax(Expr_A->width, Expr_B->width);
	// FIXME This could be optimized when operands are shorter, and unsigned
	needed_width = GetMax(needed_width, Expr->left+1);

	// Get an available operator, with enough width.
	netlist_comp_t* comp = NULL;

	if( (NETLIST_COMP_ADD->flags & NETLIST_COMPMOD_FLAG_CANSHARE)==0 ) {
		// Create a new component
		char* name = Map_Netlist_MakeInstanceName(map_data->Implem, NETLIST_COMP_ADD);
		comp = Netlist_Add_New(name, needed_width, true, true);
		Netlist_Asb_SetDefaultCI(comp, '0');
		Netlist_Comp_SetChild(map_data->Implem->netlist.top, comp);
		Map_CompType_AddComp(map_data, comp);
	}
	else {
		// Find a suitable component
		map_comptype_t* comptype = Map_CompType_GetAdd(map_data, NETLIST_COMP_ADD->name);
		avl_p_foreach(&comptype->all, scan) {
			netlist_comp_t* comp1 = scan->data;
			bool is_used = avl_p_isthere(&map_data->cur_state.hwusage.comp_used, comp1);
			if(is_used==true) continue;

			netlist_asb_t* comp_asb = comp1->data;
			if(comp_asb->width < needed_width) continue;

			comp = comp1;
			break;
		}
	}

	if(comp==NULL) {
		Map_Netlist_Datapath_BuildNew_Op_error(map_data, datapath, Expr->model, NETLIST_COMP_ADD, __LINE__);
	}

	// Flag the component as used
	avl_p_add_overwrite(&datapath->hwusage.comp_used, comp, comp);

	// Use the mapping function associated to the ASB component.

	if( (NETLIST_COMP_ADD->flags & NETLIST_COMPMOD_FLAG_CANSHARE)==0 ) {
		Map_Netlist_Datapath_BuildNew_Asb_NoShare(
			map_data, datapath, comp,
			Datapath_A->valcat, Expr_A,
			Datapath_B->valcat, Expr_B,
			Datapath_Cin->valcat, Expr_Cin
		);
	}
	else {
		Map_Netlist_Datapath_BuildNew_Asb(
			map_data, datapath, comp,
			Datapath_A->valcat, Expr_A,
			Datapath_B->valcat, Expr_B,
			Datapath_Cin->valcat, Expr_Cin
		);
	}

	return datapath;
}

// Internal function for inequalities. Build the 1-bit result, handle signedness and equality
static void Map_Netlist_Datapath_BuildNew_SubCmp(
	map_netlist_data_t* map_data, map_netlist_datapath_t* datapath,
	hvex_dico_ref_t* Expr_A, hvex_dico_ref_t* Expr_B,
	bool is_signed, bool is_gt, bool with_eq
) {

	// Input A
	map_netlist_datapath_t* Datapath_A = Map_Netlist_Datapath_Expr(map_data, Expr_A->ref.elt);
	datapath->sub_datapaths = addchain(datapath->sub_datapaths, Datapath_A);

	// Input B
	map_netlist_datapath_t* Datapath_B = Map_Netlist_Datapath_Expr(map_data, Expr_B->ref.elt);
	datapath->sub_datapaths = addchain(datapath->sub_datapaths, Datapath_B);

	// Get the required width
	unsigned required_width = GetMax(Expr_A->width, Expr_B->width);

	// Get an available operator SUB, with enough width.
	netlist_comp_t* comp = NULL;

	if( (NETLIST_COMP_SUB->flags & NETLIST_COMPMOD_FLAG_CANSHARE)==0 ) {
		// Create a new component
		char* name = Map_Netlist_MakeInstanceName(map_data->Implem, NETLIST_COMP_SUB);
		comp = Netlist_Sub_New(name, required_width, true, true);
		Netlist_Asb_SetDefaultCI(comp, '0');
		Netlist_Asb_EnableSign(comp, false);
		Netlist_Comp_SetChild(map_data->Implem->netlist.top, comp);
		Map_CompType_AddComp(map_data, comp);
	}
	else {
		// Get an existing component
		map_comptype_t* comptype = Map_CompType_GetAdd(map_data, NETLIST_COMP_SUB->name);
		avl_p_foreach(&comptype->all, scan) {
			netlist_comp_t* comp1 = scan->data;
			bool is_used = avl_p_isthere(&map_data->cur_state.hwusage.comp_used, comp1);
			if(is_used==true) continue;

			netlist_asb_t* comp_asb = comp1->data;
			if(comp_asb->sign_extend==NULL) continue;
			if(required_width>comp_asb->width) continue;

			comp = comp1;
			break;
		}
	}

	if(comp==NULL) {
		Map_Netlist_Datapath_BuildNew_Op_error(
			map_data, datapath, datapath->Expr->model, NETLIST_COMP_SUB, __LINE__
		);
	}

	// Flag the component as used
	avl_p_add_overwrite(&datapath->hwusage.comp_used, comp, comp);

	// Do sign extension for each input, and get the MUX inputs
	netlist_asb_t* comp_asb = comp->data;

	// Prepare the Sign input
	chain_list* valcat_sign = NULL;
	if(is_signed==true) valcat_sign = Netlist_ValCat_MakeList_RepeatBit('1', 1);
	else                valcat_sign = Netlist_ValCat_MakeList_RepeatBit('0', 1);

	if( (NETLIST_COMP_SUB->flags & NETLIST_COMPMOD_FLAG_CANSHARE)==0 ) {
		// Input A
		assert(comp_asb->port_inA->source==NULL);
		comp_asb->port_inA->source = Map_Netlist_ValCat_DupResize(Datapath_A->valcat, Expr_A, comp_asb->port_inA->width);
		// Input B
		assert(comp_asb->port_inB->source==NULL);
		comp_asb->port_inB->source = Map_Netlist_ValCat_DupResize(Datapath_B->valcat, Expr_B, comp_asb->port_inB->width);
		// The Sign input
		assert(comp_asb->sign_extend->port_sign->source==NULL);
		comp_asb->sign_extend->port_sign->source = valcat_sign;
	}
	else {
		// Input A
		// FIXME Sign extension here should not be always done... inputs can have their own signedness
		Map_Netlist_Port_MuxGetAddInput_SetState_FromExpr(
			map_data, datapath, comp_asb->port_inA,
			Expr_A, Datapath_A->valcat
		);
		// Input B
		// FIXME Sign extension here should not be always done... inputs can have their own signedness
		Map_Netlist_Port_MuxGetAddInput_SetState_FromExpr(
			map_data, datapath, comp_asb->port_inB,
			Expr_B, Datapath_B->valcat
		);
		// The Sign input
		netlist_comp_t* compMuxSign = Map_Netlist_GetSourceMux(comp_asb->sign_extend->port_sign);
		Map_Netlist_Mux_GetAddInput_SetState(map_data, datapath, compMuxSign, valcat_sign);
		Netlist_ValCat_FreeList(valcat_sign);
	}

	// Build the output signal
	if(is_gt==true) {
		if(with_eq==true) datapath->valcat = Netlist_ValCat_MakeList_FullPort(comp_asb->sign_extend->port_ge);
		else              datapath->valcat = Netlist_ValCat_MakeList_FullPort(comp_asb->sign_extend->port_gt);
	}
	else {  // LT
		if(with_eq==true) datapath->valcat = Netlist_ValCat_MakeList_FullPort(comp_asb->sign_extend->port_le);
		else              datapath->valcat = Netlist_ValCat_MakeList_FullPort(comp_asb->sign_extend->port_lt);
	}
}

// The callback for all inequalities
static map_netlist_datapath_t* Map_Hvex_CB_SubCmp(map_netlist_data_t* map_data, hvex_dico_elt_t* Expr) {
	map_netlist_datapath_t* datapath = Map_Netlist_Datapath_New();
	datapath->Expr = Expr;

	if(Expr->model==HVEX_GT) {
		Map_Netlist_Datapath_BuildNew_SubCmp(
			map_data, datapath,
			Expr->args->DATA, Expr->args->NEXT->DATA,
			false, true, false  // signed, gt, with_eq
		);
	}
	else if(Expr->model==HVEX_GE) {
		Map_Netlist_Datapath_BuildNew_SubCmp(
			map_data, datapath,
			Expr->args->DATA, Expr->args->NEXT->DATA,
			false, true, true  // signed, gt, with_eq
		);
	}
	else if(Expr->model==HVEX_LT) {
		Map_Netlist_Datapath_BuildNew_SubCmp(
			map_data, datapath,
			Expr->args->DATA, Expr->args->NEXT->DATA,
			false, false, false  // signed, gt, with_eq
		);
	}
	else if(Expr->model==HVEX_LE) {
		Map_Netlist_Datapath_BuildNew_SubCmp(
			map_data, datapath,
			Expr->args->DATA, Expr->args->NEXT->DATA,
			false, false, true  // signed, gt, with_eq
		);
	}
	else if(Expr->model==HVEX_GTS) {
		Map_Netlist_Datapath_BuildNew_SubCmp(
			map_data, datapath,
			Expr->args->DATA, Expr->args->NEXT->DATA,
			true, true, false  // signed, gt, with_eq
		);
	}
	else if(Expr->model==HVEX_GES) {
		Map_Netlist_Datapath_BuildNew_SubCmp(
			map_data, datapath,
			Expr->args->DATA, Expr->args->NEXT->DATA,
			true, true, true  // signed, gt, with_eq
		);
	}
	else if(Expr->model==HVEX_LTS) {
		Map_Netlist_Datapath_BuildNew_SubCmp(
			map_data, datapath,
			Expr->args->DATA, Expr->args->NEXT->DATA,
			true, false, false  // signed, gt, with_eq
		);
	}
	else if(Expr->model==HVEX_LES) {
		Map_Netlist_Datapath_BuildNew_SubCmp(
			map_data, datapath,
			Expr->args->DATA, Expr->args->NEXT->DATA,
			true, false, true  // signed, gt, with_eq
		);
	}
	else {
		abort();
	}

	return datapath;
}

// Internal function for all logic operations
static void Map_Netlist_Datapath_BuildNew_Logic(
	map_netlist_data_t* map_data, map_netlist_datapath_t* datapath,
	hvex_dico_elt_t* Expr, netlist_comp_model_t* model
) {
	// Create an operator

	char* name = Map_Netlist_MakeInstanceName(map_data->Implem, model);
	netlist_comp_t* comp = Netlist_Logic_New(model, name, Expr->width);
	Netlist_Comp_SetChild(map_data->Implem->netlist.top, comp);
	Map_CompType_AddComp(map_data, comp);

	netlist_logic_t* comp_data = comp->data;
	foreach(Expr->args, scanArg) {
		hvex_dico_ref_t* Expr_operand = scanArg->DATA;
		// Create the datapath
		map_netlist_datapath_t* Datapath_operand = Map_Netlist_Datapath_Expr(map_data, Expr_operand->ref.elt);
		datapath->sub_datapaths = addchain(datapath->sub_datapaths, Datapath_operand);
		// Add an input
		netlist_port_t* port = Netlist_Logic_AddInput(comp);
		port->source = Map_Netlist_ValCat_DupResize(Datapath_operand->valcat, Expr_operand, port->width);
	}  // Scan all operands

	// Build the output "ValCat" expression
	datapath->valcat = Netlist_ValCat_MakeList_FullPort(comp_data->port_out);

	// Flag the component as used
	avl_p_add_overwrite(&datapath->hwusage.comp_used, comp, comp);
}

// The callback for all logic operations
static map_netlist_datapath_t* Map_Hvex_CB_Logic(map_netlist_data_t* map_data, hvex_dico_elt_t* Expr) {
	map_netlist_datapath_t* datapath = Map_Netlist_Datapath_New();
	datapath->Expr = Expr;

	if(Expr->model==HVEX_AND) {
		Map_Netlist_Datapath_BuildNew_Logic(map_data, datapath, Expr, NETLIST_COMP_AND);
	}
	else if(Expr->model==HVEX_NAND) {
		Map_Netlist_Datapath_BuildNew_Logic(map_data, datapath, Expr, NETLIST_COMP_NAND);
	}
	else if(Expr->model==HVEX_OR) {
		Map_Netlist_Datapath_BuildNew_Logic(map_data, datapath, Expr, NETLIST_COMP_OR);
	}
	else if(Expr->model==HVEX_NOR) {
		Map_Netlist_Datapath_BuildNew_Logic(map_data, datapath, Expr, NETLIST_COMP_NOR);
	}
	else if(Expr->model==HVEX_XOR) {
		Map_Netlist_Datapath_BuildNew_Logic(map_data, datapath, Expr, NETLIST_COMP_XOR);
	}
	else if(Expr->model==HVEX_NXOR) {
		Map_Netlist_Datapath_BuildNew_Logic(map_data, datapath, Expr, NETLIST_COMP_NXOR);
	}
	else if(Expr->model==HVEX_NOT) {
		Map_Netlist_Datapath_BuildNew_Logic(map_data, datapath, Expr, NETLIST_COMP_NOT);
	}
	else {
		abort();
	}

	return datapath;
}

// The callback for binary MUX
static map_netlist_datapath_t* Map_Hvex_CB_MuxB(map_netlist_data_t* map_data, hvex_dico_elt_t* Expr) {
	map_netlist_datapath_t* datapath = Map_Netlist_Datapath_New();
	datapath->Expr = Expr;

	// Create an operator
	char* name = Map_Netlist_MakeInstanceName(map_data->Implem, NETLIST_COMP_MUXBIN);
	hvex_dico_ref_t* Expr_sel = Expr->args->DATA;
	unsigned width_sel = Expr_sel->width;
	unsigned width_data = Expr->width;
	unsigned operands_nb = ((unsigned)1) << width_sel;

	netlist_comp_t* comp = Netlist_MuxBin_New(name, width_sel, width_data, operands_nb);
	Netlist_Comp_SetChild(map_data->Implem->netlist.top, comp);
	Map_CompType_AddComp(map_data, comp);

	netlist_muxbin_t* muxbin_data = comp->data;

	// Build the datapath for the selection input
	map_netlist_datapath_t* Datapath_sel = Map_Netlist_Datapath_Expr(map_data, Expr_sel->ref.elt);
	datapath->sub_datapaths = addchain(datapath->sub_datapaths, Datapath_sel);
	// Assign the selection input
	muxbin_data->port_sel->source = Map_Netlist_ValCat_DupResize(Datapath_sel->valcat, Expr_sel, width_sel);

	unsigned op_idx = 0;
	foreach(Expr->args->NEXT, scanArg) {
		hvex_dico_ref_t* Expr_operand = scanArg->DATA;
		// Create the datapath
		map_netlist_datapath_t* Datapath_operand = Map_Netlist_Datapath_Expr(map_data, Expr_operand->ref.elt);
		datapath->sub_datapaths = addchain(datapath->sub_datapaths, Datapath_operand);
		// Add an input
		muxbin_data->ports_datain[op_idx]->source = Map_Netlist_ValCat_DupResize(Datapath_operand->valcat, Expr_operand, width_data);
		op_idx++;
	}  // Scan all operands

	// Build the output "ValCat" expression
	datapath->valcat = Netlist_ValCat_MakeList_FullPort(muxbin_data->port_out);

	// Flag the component as used
	avl_p_add_overwrite(&datapath->hwusage.comp_used, comp, comp);

	return datapath;
}

// The callback for decoded MUX
static map_netlist_datapath_t* Map_Hvex_CB_MuxDec(map_netlist_data_t* map_data, hvex_dico_elt_t* Expr) {
	map_netlist_datapath_t* datapath = Map_Netlist_Datapath_New();
	datapath->Expr = Expr;

	// Create an operator
	char* name = Map_Netlist_MakeInstanceName(map_data->Implem, NETLIST_COMP_MUX);
	netlist_comp_t* comp = Netlist_Mux_New(name, Expr->width);
	Netlist_Mux_SetDefaultValue(comp, stralloc_repeat('0', Expr->width));
	Netlist_Comp_SetChild(map_data->Implem->netlist.top, comp);
	Map_CompType_AddComp(map_data, comp);

	foreach(Expr->args, scanArg) {
		hvex_dico_ref_t* Expr_operand = scanArg->DATA;
		assert(Expr_operand->ref.elt->model==HVEX_MUXDECIN);
		hvex_dico_ref_t* Expr_opcond = Expr_operand->ref.elt->args->DATA;
		hvex_dico_ref_t* Expr_opdata = Expr_operand->ref.elt->args->NEXT->DATA;
		// Create the datapath for condition
		map_netlist_datapath_t* Datapath_opcond = Map_Netlist_Datapath_Expr(map_data, Expr_opcond->ref.elt);
		datapath->sub_datapaths = addchain(datapath->sub_datapaths, Datapath_opcond);
		// Create the datapath for data input
		map_netlist_datapath_t* Datapath_opdata = Map_Netlist_Datapath_Expr(map_data, Expr_opdata->ref.elt);
		datapath->sub_datapaths = addchain(datapath->sub_datapaths, Datapath_opdata);
		// Add a MUX input
		netlist_access_t* muxIn = Netlist_Mux_AddInput(comp);
		netlist_mux_access_t* muxIn_data = muxIn->data;
		muxIn_data->port_en->source = Map_Netlist_ValCat_DupResize(Datapath_opcond->valcat, Expr_opcond, 1);
		muxIn_data->port_data->source = Map_Netlist_ValCat_DupResize(Datapath_opdata->valcat, Expr_opdata, Expr->width);
	}  // Scan all operands

	// Build the output "ValCat" expression
	netlist_mux_t* mux_data = comp->data;
	datapath->valcat = Netlist_ValCat_MakeList_FullPort(mux_data->port_out);

	// Flag the component as used
	avl_p_add_overwrite(&datapath->hwusage.comp_used, comp, comp);

	return datapath;
}

// Internal function for comparisons EQ and NE
// Return the created component. The calling function then chooses the output, NE or EQ.
static netlist_comp_t* Map_Netlist_Datapath_BuildNew_Cmp(map_netlist_data_t* map_data, map_netlist_datapath_t* datapath) {
	// Find the max width of the operands
	unsigned width = 0;

	// DATA_FROM = Expr, DATA_TO = datapath
	bitype_list* input_dps = NULL;

	foreach(datapath->Expr->args, scanArg) {
		hvex_dico_ref_t* Expr_operand = scanArg->DATA;
		// Create the datapath
		map_netlist_datapath_t* Datapath_operand = Map_Netlist_Datapath_Expr(map_data, Expr_operand->ref.elt);
		datapath->sub_datapaths = addchain(datapath->sub_datapaths, Datapath_operand);
		// Save the generated datapath
		input_dps = addbitype(input_dps, 0, Expr_operand, Datapath_operand);
		// Get the width
		// FIXME here we should apply a sign extension if needed
		width = GetMax(width, Expr_operand->width);
	}  // Scan all operands

	// Create an operator
	char* name = Map_Netlist_MakeInstanceName(map_data->Implem, NETLIST_COMP_CMP);
	netlist_comp_t* comp = Netlist_Cmp_New_Raw(name, width);
	Netlist_Comp_SetChild(map_data->Implem->netlist.top, comp);
	Map_CompType_AddComp(map_data, comp);

	//netlist_cmp_t* comp_data = comp->data;
	foreach(input_dps, scanInDp) {
		hvex_dico_ref_t* Expr_operand = scanInDp->DATA_FROM;
		map_netlist_datapath_t* Datapath_operand = scanInDp->DATA_TO;
		// Add an input
		netlist_port_t* port = Netlist_Cmp_AddInput(comp);
		port->source = Map_Netlist_ValCat_DupResize(Datapath_operand->valcat, Expr_operand, width);
	}  // Scan all operands

	freebitype(input_dps);
	return comp;
}

static map_netlist_datapath_t* Map_Hvex_CB_Eq(map_netlist_data_t* map_data, hvex_dico_elt_t* Expr) {
	map_netlist_datapath_t* datapath = Map_Netlist_Datapath_New();
	datapath->Expr = Expr;

	netlist_comp_t* comp = Map_Netlist_Datapath_BuildNew_Cmp(map_data, datapath);
	netlist_cmp_t* comp_data = comp->data;

	// Build the output "ValCat" expression
	datapath->valcat = Netlist_ValCat_MakeList_FullPort(comp_data->port_eq);

	// Flag the component as used
	avl_p_add_overwrite(&datapath->hwusage.comp_used, comp, comp);

	return datapath;
}

static map_netlist_datapath_t* Map_Hvex_CB_Ne(map_netlist_data_t* map_data, hvex_dico_elt_t* Expr) {
	map_netlist_datapath_t* datapath = Map_Netlist_Datapath_New();
	datapath->Expr = Expr;

	netlist_comp_t* comp = Map_Netlist_Datapath_BuildNew_Cmp(map_data, datapath);
	netlist_cmp_t* comp_data = comp->data;

	// Build the output "ValCat" expression
	datapath->valcat = Netlist_ValCat_MakeList_FullPort(comp_data->port_ne);

	// Flag the component as used
	avl_p_add_overwrite(&datapath->hwusage.comp_used, comp, comp);

	return datapath;
}

// Internal function for shifts
static void Map_Netlist_Datapath_BuildNew_Shift(
	map_netlist_data_t* map_data, map_netlist_datapath_t* datapath, netlist_comp_t* comp,
	map_netlist_datapath_t* datapath_data, hvex_dico_ref_t* Expr_data,
	map_netlist_datapath_t* datapath_shift, hvex_dico_ref_t* Expr_shift,
	char padding_val
) {

	netlist_shift_t* shift_data = comp->data;

	// The padding input
	chain_list* valcat_pad = NULL;
	if(padding_val==0) {
		chain_list* valcat_tmp = Map_Netlist_ValCat_DupResize(datapath_data->valcat, Expr_data, shift_data->port_in->width);
		valcat_pad = Netlist_ValCat_GetLeftBit(valcat_tmp);
		Netlist_ValCat_FreeList(valcat_tmp);
	}
	else {
		valcat_pad = Netlist_ValCat_MakeList_RepeatBit(padding_val, 1);
	}

	if( (comp->model->flags & NETLIST_COMPMOD_FLAG_CANSHARE)==0 ) {
		assert(shift_data->port_in->source==NULL);
		assert(shift_data->port_shift->source==NULL);
		assert(shift_data->port_padding->source==NULL);
		shift_data->port_in->source = Map_Netlist_ValCat_DupResize(datapath_data->valcat, Expr_data, shift_data->port_in->width);
		shift_data->port_shift->source = Map_Netlist_ValCat_DupResize(datapath_shift->valcat, Expr_shift, shift_data->port_shift->width);
		shift_data->port_padding->source = valcat_pad;
	}
	else {
		// The Data input
		Map_Netlist_Port_MuxGetAddInput_SetState_FromExpr(
			map_data, datapath, shift_data->port_in,
			Expr_data, datapath_data->valcat
		);

		// The Shift input
		Map_Netlist_Port_MuxGetAddInput_SetState_FromExpr(
			map_data, datapath, shift_data->port_shift,
			Expr_shift, datapath_shift->valcat
		);

		netlist_comp_t* mux = Map_Netlist_GetSourceMux(shift_data->port_padding);
		Map_Netlist_Mux_GetAddInput_SetState(
			map_data, datapath, mux, valcat_pad
		);

		Netlist_ValCat_FreeList(valcat_pad);
	}

	// The output value

	datapath->valcat = Netlist_ValCat_MakeList_FullPort(shift_data->port_out);
	netlist_val_cat_t* valcat_elt = datapath->valcat->DATA;
	valcat_elt->width = datapath->Expr->width;
	valcat_elt->left = datapath->Expr->left;
	valcat_elt->right = datapath->Expr->right;
}

static map_netlist_datapath_t* Map_Hvex_CB_Shl(map_netlist_data_t* map_data, hvex_dico_elt_t* Expr) {
	map_netlist_datapath_t* datapath = Map_Netlist_Datapath_New();
	datapath->Expr = Expr;

	hvex_dico_ref_t* Expr_data = Expr->args->DATA;
	map_netlist_datapath_t* Datapath_data = Map_Netlist_Datapath_Expr(map_data, Expr_data->ref.elt);
	datapath->sub_datapaths = addchain(datapath->sub_datapaths, Datapath_data);

	hvex_dico_ref_t* Expr_shift = Expr->args->NEXT->DATA;
	map_netlist_datapath_t* Datapath_shift = Map_Netlist_Datapath_Expr(map_data, Expr_shift->ref.elt);
	datapath->sub_datapaths = addchain(datapath->sub_datapaths, Datapath_shift);

	// Get an available operator, with enough width.
	netlist_comp_t* comp = NULL;

	if( (NETLIST_COMP_SHL->flags & NETLIST_COMPMOD_FLAG_CANSHARE)==0 ) {
		// Create a new component
		char* name = Map_Netlist_MakeInstanceName(map_data->Implem, NETLIST_COMP_SHL);
		comp = Netlist_ShL_New(name, GetMax(Expr->left+1, Expr_data->width), Expr_shift->width);
		Netlist_Comp_SetChild(map_data->Implem->netlist.top, comp);
		Map_CompType_AddComp(map_data, comp);
	}
	else {
		// Get a suitable component
		map_comptype_t* comptype = Map_CompType_GetAdd(map_data, NETLIST_COMP_SHL->name);
		avl_p_foreach(&comptype->all, scan) {
			netlist_comp_t* comp1 = scan->data;
			bool is_used = avl_p_isthere(&map_data->cur_state.hwusage.comp_used, comp1);
			if(is_used==true) continue;

			netlist_shift_t* comp_data = comp1->data;
			if(Expr->left > comp_data->width_data-1) continue;
			if(Expr_shift->width > comp_data->width_shift) continue;

			comp = comp1;
			break;
		}
	}

	if(comp==NULL) {
		Map_Netlist_Datapath_BuildNew_Op_error(map_data, datapath, Expr->model, NETLIST_COMP_SHL, __LINE__);
	}

	// Flag the component as used
	avl_p_add_overwrite(&datapath->hwusage.comp_used, comp, comp);

	// Use the mapping function associated to shifters
	Map_Netlist_Datapath_BuildNew_Shift(
		map_data, datapath, comp,
		Datapath_data, Expr_data,
		Datapath_shift, Expr_shift,
		'0'
	);

	return datapath;
}

static map_netlist_datapath_t* Map_Hvex_CB_Shr(map_netlist_data_t* map_data, hvex_dico_elt_t* Expr) {
	map_netlist_datapath_t* datapath = Map_Netlist_Datapath_New();
	datapath->Expr = Expr;

	hvex_dico_ref_t* Expr_data = Expr->args->DATA;
	map_netlist_datapath_t* Datapath_data = Map_Netlist_Datapath_Expr(map_data, Expr_data->ref.elt);
	datapath->sub_datapaths = addchain(datapath->sub_datapaths, Datapath_data);

	hvex_dico_ref_t* Expr_shift = Expr->args->NEXT->DATA;
	map_netlist_datapath_t* Datapath_shift = Map_Netlist_Datapath_Expr(map_data, Expr_shift->ref.elt);
	datapath->sub_datapaths = addchain(datapath->sub_datapaths, Datapath_shift);

	// Get an available operator, with enough width.
	netlist_comp_t* comp = NULL;

	if( (NETLIST_COMP_SHR->flags & NETLIST_COMPMOD_FLAG_CANSHARE)==0 ) {
		// Create a new component
		char* name = Map_Netlist_MakeInstanceName(map_data->Implem, NETLIST_COMP_SHR);
		comp = Netlist_ShR_New(name, GetMax(Expr->left+1, Expr_data->width), Expr_shift->width);
		Netlist_Comp_SetChild(map_data->Implem->netlist.top, comp);
		Map_CompType_AddComp(map_data, comp);
	}
	else {
		// Get a suitable component
		map_comptype_t* comptype = Map_CompType_GetAdd(map_data, NETLIST_COMP_SHR->name);
		avl_p_foreach(&comptype->all, scan) {
			netlist_comp_t* comp1 = scan->data;
			bool is_used = avl_p_isthere(&map_data->cur_state.hwusage.comp_used, comp1);
			if(is_used==true) continue;

			netlist_shift_t* comp_data = comp1->data;
			if(Expr->left > comp_data->width_data-1) continue;
			if(Expr_shift->width > comp_data->width_shift) continue;

			comp = comp1;
			break;
		}
	}

	if(comp==NULL) {
		Map_Netlist_Datapath_BuildNew_Op_error(map_data, datapath, Expr->model, NETLIST_COMP_SHR, __LINE__);
	}

	// Flag the component as used
	avl_p_add_overwrite(&datapath->hwusage.comp_used, comp, comp);

	// Use the mapping function associated to shifters
	Map_Netlist_Datapath_BuildNew_Shift(
		map_data, datapath, comp,
		Datapath_data, Expr_data,
		Datapath_shift, Expr_shift,
		Expr_data->extend
	);

	return datapath;
}



// Set the mapper-specific callbacks in the HVEX models
void Map_Init_Hvex_Callbacks() {
	unsigned errors_nb = 0;

	#define addcb_check(model, cb) do { \
		if(model==NULL) { \
			lineprintf("INTERNAL ERROR" , #model " = NULL\n"); \
			errors_nb++; \
		} \
		else model->cb_map = (map_hvex_cbvoid_type)cb; \
	} while(0)

	addcb_check(HVEX_LITERAL, Map_Hvex_CB_Literal);
	addcb_check(HVEX_VECTOR,  Map_Hvex_CB_Vector);
	addcb_check(HVEX_INDEX,   Map_Hvex_CB_Index);

	addcb_check(HVEX_ASG,         Map_Hvex_CB_Asg);
	addcb_check(HVEX_ASG_EN,      Map_Hvex_CB_Asg);
	addcb_check(HVEX_ASG_ADDR,    Map_Hvex_CB_Asg);
	addcb_check(HVEX_ASG_ADDR_EN, Map_Hvex_CB_Asg);

	addcb_check(HVEX_CONCAT, Map_Hvex_CB_Concat);
	addcb_check(HVEX_REPEAT, Map_Hvex_CB_Repeat);
	addcb_check(HVEX_RESIZE, Map_Hvex_CB_Resize);

	addcb_check(HVEX_ADD, Map_Hvex_CB_Add);
	addcb_check(HVEX_SUB, Map_Hvex_CB_Sub);
	addcb_check(HVEX_NEG, Map_Hvex_CB_Neg);
	addcb_check(HVEX_MUL, Map_Hvex_CB_Mul);

	addcb_check(HVEX_DIVQ, Map_Hvex_CB_DivQR);
	addcb_check(HVEX_DIVR, Map_Hvex_CB_DivQR);

	addcb_check(HVEX_ADDCIN, Map_Hvex_CB_AddCin);

	addcb_check(HVEX_GT,  Map_Hvex_CB_SubCmp);
	addcb_check(HVEX_GE,  Map_Hvex_CB_SubCmp);
	addcb_check(HVEX_LT,  Map_Hvex_CB_SubCmp);
	addcb_check(HVEX_LE,  Map_Hvex_CB_SubCmp);
	addcb_check(HVEX_GTS, Map_Hvex_CB_SubCmp);
	addcb_check(HVEX_GES, Map_Hvex_CB_SubCmp);
	addcb_check(HVEX_LTS, Map_Hvex_CB_SubCmp);
	addcb_check(HVEX_LES, Map_Hvex_CB_SubCmp);

	addcb_check(HVEX_AND,  Map_Hvex_CB_Logic);
	addcb_check(HVEX_NAND, Map_Hvex_CB_Logic);
	addcb_check(HVEX_OR,   Map_Hvex_CB_Logic);
	addcb_check(HVEX_NOR,  Map_Hvex_CB_Logic);
	addcb_check(HVEX_XOR,  Map_Hvex_CB_Logic);
	addcb_check(HVEX_NXOR, Map_Hvex_CB_Logic);
	addcb_check(HVEX_NOT,  Map_Hvex_CB_Logic);

	addcb_check(HVEX_MUXB,   Map_Hvex_CB_MuxB);
	addcb_check(HVEX_MUXDEC, Map_Hvex_CB_MuxDec);

	addcb_check(HVEX_EQ, Map_Hvex_CB_Eq);
	addcb_check(HVEX_NE, Map_Hvex_CB_Ne);

	addcb_check(HVEX_SHL, Map_Hvex_CB_Shl);
	addcb_check(HVEX_SHR, Map_Hvex_CB_Shr);

	if(errors_nb > 0) {
		abort();
	}
}


