
#ifndef _TECHNO_H_
#define _TECHNO_H_

#include <stdio.h>
#include <stdbool.h>
#include <assert.h>

#include "../indent.h"
#include "../netlist/netlist.h"



//=====================================================
// Data structures
//=====================================================

typedef struct techno_timing_t  techno_timing_t;
typedef struct techno_t         techno_t;

typedef struct synth_target_t   synth_target_t;

typedef struct fpga_pkg_model_t fpga_pkg_model_t;
typedef struct fpga_model_t     fpga_model_t;

typedef struct board_fpga_t     board_fpga_t;
typedef struct board_t          board_t;

#include "auto.h"


struct techno_t {
	// The board name
	char* name;
	char* description;  // Optional
	// Reference to the plugin that owns this techno model
	void* plugin;

	// The list of speed grades available
	avl_pp_tree_t timings;
	// Optional: The default speed grade to use
	techno_timing_t* default_timing;

	// The list of packages available
	avl_pp_tree_t packages;

	// Techno-specific access functions
	// FIXMEEEE Missing checks that these functions are actually defined. Ditto for size eval of components.
	void        (*fprint_timing)(FILE* F, techno_timing_t* timing, indent_t* indent);
	ptype_list* (*getres)(fpga_model_t* model);
	fpga_model_t* (*chipmodel_fromcode)(const char* code);
	void        (*reswritenames)(FILE* F, const char* beg, const char* sep, const char* end);
	void        (*reswriteusage)(FILE* F, ptype_list* list, const char* beg, const char* sep, const char* end);
	bool        (*isresnamevalid)(const char* name);
	double      (*vexcoarseevaldelay)(implem_t* Implem, hvex_t* Expr);
	ptype_list* (*sizeeval_mem2reg)(implem_t* Implem, const char* name);
	ptype_list* (*sizeeval_addreadports)(implem_t* Implem, const char* name, unsigned ports_add_nb);
	ptype_list* (*sizeeval_compfromoutwidth)(implem_t* Implem, const char* type, unsigned width);
	ptype_list* (*sizeeval_muxdirect)(implem_t* Implem, unsigned width, unsigned inputs);
	ptype_list* (*sizeeval_fsmout1)(implem_t* Implem, unsigned actions_nb);
	ptype_list* (*sizeeval_fsmstates)(implem_t* Implem, unsigned states_nb);
	ptype_list* (*sizeeval_fsmstatesacts)(implem_t* Implem, unsigned states_nb, unsigned actions_nb);
	chain_list* (*listmem_readbuf)(implem_t* Implem);

	// Manipulation of private data
	void  (*privatedata_init)(synth_target_t*);
	void  (*privatedata_initchip)(synth_target_t*);
	void  (*privatedata_free)(synth_target_t*);

	// Manipulation of pin attributes
	// FIXME: this may not be useful because it can be done with the command interpreter
	void* (*portattr_dup)(void*);
	void  (*portattr_free)(void*);

	// Generation of synthesis project files
	int (*synth_gen_prj)(implem_t* Implem);
	// Launch synthesis
	int (*synth_launch)(implem_t* Implem);

	// Command interpreter
	int   (*command)(implem_t* Implem, command_t* cmd_data);
};

struct techno_timing_t {
	// The ID of the timing model
	char* name;
	char* description;  // Optional
	// The techno it pertains to
	techno_t* techno;
	// The techno-specific data
	void* data;
};

// This structure represents the HW target of a synthesis operation
// It can be an entire FPGA of a bigger board
// Or a certain partition of an FPGA
struct synth_target_t {

	// The technology this target it is based on
	techno_t* techno;
	// The timing model used
	techno_timing_t* timing;
	// The selected chip package
	fpga_pkg_model_t* package;

	// The targeted hardware resources
	ptype_list* resources;
	// The targeted frequency
	double clk_freq;
	double clk_period_ns;
	// The reset active state: '0' or '1' (default: '1')
	char reset_active_state;

	// The FPGA model to use.
	// Can be NULL if a custom target is defined.
	fpga_model_t* model;
	// Flag needed to apply a default chip usage ratio
	bool usage_is_default;
	double model_ratio;  // Ratio of model resources used. From 0 to 1.

	// Reference to the board this FPGA is soldered on.
	// Can be NULL if there is no such context. In this case,
	// The 3 next fields descibe the characteristics of the targeted
	// hardware and platform.
	board_fpga_t* board_fpga;

	// Top-level access assignation
	// Key = Implem access, data = board_fpga access
	avl_pp_tree_t impacc2boardacc;

	// To manually set the FPGA pins
	// Key = netlist_port_t*, data = void* (techno specific)
	// FIXME not used yet
	avl_pp_tree_t port2pin;

	// The list of Implem that are being synthesized on this set of resources
	avl_p_tree_t* implems;

	// Techno-specific data
	void* techno_data;
};

struct fpga_model_t {
	// The name of the FPGA in the techno
	char* name;
	char* description;  // Optional
	// Reference to the plugin that owns this FPGA model (NULL if AUGH)
	void* plugin;
	// The techno it is based on
	techno_t* techno;
	// The available speed grades
	chain_list* timings;
	// The available packages
	chain_list* packages;
	// Its hardware resources (techno-specific structure)
	void* resources;
};

struct fpga_pkg_model_t {
	// The name of the package model in the techno
	char* name;
	char* description;  // Optional
	// Some private data
	void* data;
};

struct board_fpga_t {
	// The name of the FPGA on the board
	char* name;
	char* description;  // Optional
	// Reference to the board this FPGA is soldered on
	board_t* board;

	// The corresponding FPGA model
	fpga_model_t* model;
	// The technology speed grade
	techno_timing_t* speed;
	// The FPGA package
	fpga_pkg_model_t* package;

	// Indicate the available connectivity
	// The access structures of this component represent the external connectivity
	//   with board connectors and features (like clock) and with other board FPGAs
	// The child components can represent the synth_target of the Implem...
	// If there is none, assume there is only one Implem and it targets the entire FPGA
	netlist_comp_t* connect;

	// Some default ports when targeting this board
	netlist_access_t* default_clock;
	netlist_access_t* default_reset;
	netlist_access_t* default_stdin;
	netlist_access_t* default_stdout;

	// How to get the FPGA pins
	// Key = netlist_port_t*, data = void* (techno specific)
	avl_pp_tree_t port2pin;

	// Other optional data

	// ID of the FPGA in the board JTAG chain. Assume the first in the chain is 1. <=0 if not applicable.
	int jtag_id;
	// To program the board with the program xc3sprog (NULL if not applicable)
	char* xc3sprog_cable;

	// Techno-specific data
	void* techdata;
};

struct board_t {
	// The board name
	char* name;
	char* description;  // Optional
	// Reference to the plugin that owns this component model (NULL if AUGH)
	void* plugin;

	// A board can have multiple FPGA
	// ... possibly from different vendors, so declared by different plugins
	// Indexed by name
	avl_pp_tree_t fpgas;

	// Indicate the available connectifity involving:
	// - board connectors
	// - board features (like clock generator, usb2uart interface, etc)
	// - board FPGAs with themselves
	// This top-level component contains one component per FPGA of the board
	//   (for all, the netlist comp model type = TOP)
	//   (and the component name is the board_fpga name)
	netlist_comp_t* connect;
};

// Allocation wrappers

techno_t* Techno_New();
void Techno_Free(techno_t* techno);

techno_timing_t* Techno_Timing_New();
void Techno_Timing_Free(techno_timing_t* timing);

fpga_model_t* FPGA_Model_New();
void FPGA_Model_Free(fpga_model_t* model);

fpga_pkg_model_t* FPGA_PkgModel_New();
void FPGA_PkgModel_Free(fpga_pkg_model_t* model);

board_t* Board_New();
void Board_Free(board_t* board);

board_fpga_t* Board_FPGA_New();
void Board_FPGA_Free(board_fpga_t* fpga);

synth_target_t* Synth_Target_New();
void Synth_Target_Free(synth_target_t* target);



//=====================================================
// Managing all available HW target stuff
//=====================================================

bool Techno_DeclareTechno(techno_t* techno);
bool Techno_DeclareFPGA(fpga_model_t* fpga);
bool Techno_DeclareBoard(board_t* board);

techno_t* Techno_GetTechno(const char* name);
techno_timing_t* Techno_GetSpeed(techno_t* techno, const char* name);
fpga_pkg_model_t* Techno_GetPackage(techno_t* techno, const char* name);
fpga_model_t* Techno_GetKnownFPGA(const char* name);
fpga_model_t* Techno_GetFPGA(const char* name);
fpga_pkg_model_t* Techno_FPGA_GetPkg(fpga_model_t* fpga, const char* name);
techno_timing_t* Techno_FPGA_GetSpeed(fpga_model_t* fpga, const char* name);
board_t* Techno_GetBoard(const char* name);



//=====================================================
// Functions
//=====================================================

void AughTechno_Init();

void Techno_SynthTarget_Reset(synth_target_t* synth);
void Techno_SynthTarget_SetTechno(synth_target_t* synth, techno_t* techno);
void Techno_SynthTarget_SetTechnoSpeed(synth_target_t* synth, techno_timing_t* timing);
void Techno_SynthTarget_SetChipModel(synth_target_t* synth, fpga_model_t* model);

void Techno_SetBoardFpga(implem_t* Implem, board_fpga_t* board_fpga);

int  Techno_SetBoardClock_FromName(implem_t* Implem, const char* name);
int  Techno_SetBoardClock_Access(implem_t* Implem, netlist_access_t* access);
void Techno_SetBoardClock_AccessClock(implem_t* Implem, netlist_access_t* access);
void Techno_SetBoardClock_AccessDiffClock(implem_t* Implem, netlist_access_t* access);

void Techno_SynthTarget_SetFrequency(synth_target_t* synth, double frequency);

int   Techno_DispTechnoData_user(const char* name, indent_t* indent);
void  Techno_DispAllTechnoData(indent_t* indent);
void  Techno_DispAllTechno(indent_t* indent);

ptype_list*  Techno_GetChipResources(const char* name);
char*        Techno_GetChipTechno(const char* name);
int          Techno_DispChipData_user(const char* name, indent_t* indent);
void         Techno_DispAllChips(indent_t* indent);
void         Techno_DispAllChipsData(indent_t* indent);

// Evaluation of component size
static inline ptype_list* Techno_EvalSize_Comp(synth_target_t* synth, netlist_comp_t* comp) {
	assert(comp->model->func_eval_size!=NULL);
	return comp->model->func_eval_size(synth, comp);
}

ptype_list*  Techno_EvalSize_addrecurs(ptype_list* list, synth_target_t* synth, netlist_comp_t* comp);
bitype_list* Techno_Netlist_EvalSize_WithTypes(synth_target_t* synth, netlist_comp_t* comp);

void         Techno_Netlist_Size_EvalPrint(synth_target_t* synth, netlist_comp_t* comp);

// Latency estimation

double Techno_Eval_Latency_VexAsg(implem_t* Implem, hvex_t* Expr);
double Techno_Eval_Latency_HierState(implem_t* Implem, hier_node_t* node);
double Techno_Eval_Latency_HierMax(implem_t* Implem);

// Manipulation of hardware resources, by name

ptype_list* Techno_NameRes_AddMult(ptype_list* dest, ptype_list* src, int nb);
void        Techno_NameRes_MultCst(ptype_list* dest, int nb);
void        Techno_NameRes_Print(ptype_list* list, const char* beg, const char* end);
int         Techno_NameRes_Sub_raw(ptype_list* dest, ptype_list* src, bool minzero, bool noerr);
bool        Techno_NameRes_CheckInclude(ptype_list* dest, ptype_list* src);
ptype_list* Techno_NameRes_Add_One(ptype_list* dest, char* name, int val);
ptype_list* Techno_NameRes_Add(ptype_list* dest, ptype_list* src);
ptype_list* Techno_NameRes_SetMax_One(ptype_list* dest, char* name, int val);
ptype_list* Techno_NameRes_SetMax(ptype_list* dest, ptype_list* src);

bool Techno_NameRes_CheckInclude(ptype_list* dest, ptype_list* src);

bitype_list* Techno_NameOpRes_Add_One(bitype_list* dest, char* name, unsigned nb, ptype_list* res);
bitype_list* Techno_NameOpRes_Add(bitype_list* dest, bitype_list* src);
ptype_list*  Techno_NameOpRes_GetTotal(bitype_list* list);
void         Techno_NameOpRes_Free(bitype_list* list);

void Techno_NameOpRes_FsPrint(FILE* F, bitype_list* list, char* indent_str);
void Techno_NameOpRes_FiPrint(FILE* F, bitype_list* list, indent_t* indent);
static inline void Techno_NameOpRes_Print(bitype_list* list) {
	Techno_NameOpRes_FsPrint(stdout, list, NULL);
}

void Techno_NameRes_fprintf(FILE* F, ptype_list* list);
int  Techno_NameRes_fscanf(FILE* F, ptype_list** res_list);

void Techno_NameOpRes_fprint(FILE* F, bitype_list* list);
int  Techno_NameOpRes_fscanf(FILE* F, bitype_list** list);

// Special evaluations

ptype_list* Techno_Netlist_Eval_Mem2Reg(implem_t* Implem, const char* name);
ptype_list* Techno_Netlist_Eval_AddReadPorts(implem_t* Implem, const char* name, unsigned ports_add_nb);
ptype_list* Techno_Netlist_Get_NameRes_FromOutWidth(implem_t* Implem, char* type, unsigned width);
ptype_list* Techno_EvalRes_Mux_Direct(implem_t* Implem, unsigned width, unsigned inputs);
ptype_list* Techno_EvalRes_FsmOut_OneBit_OneHot(implem_t* Implem, unsigned actions_nb);

// Command interpreter

int Techno_Command(implem_t* Implem, command_t* cmd_data);


#endif  // _TECHNO_H_

