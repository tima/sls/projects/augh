
#ifndef _IMPLEM_MODELS_H_
#define _IMPLEM_MODELS_H_


void ImpMod_Count_Update(augh_globals_t* globals);
void ImpMod_ComputeSizes(augh_globals_t* globals);
void ImpMod_ComputeTimes(augh_globals_t* globals);

netlist_access_t* ImpMod_GetAccess_User(implem_t* Implem, const char* str, bool verbose);

implem_t* ImpMod_Load(augh_globals_t* globals, const char* modname, const char* filename);

int ImpMod_Command(implem_t* Implem, command_t* cmd_data);


#endif  // _IMPLEM_MODELS_H_

