
// Contents of this file: manipulation of switch nodes

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <assert.h>
#include <inttypes.h>
#include <math.h>

#include "auto.h"
#include "hierarchy.h"
#include "hierarchy_symuse.h"
#include "schedule.h"
#include "linelist.h"
#include "techno.h"
#include "map.h"
#include "../hvex/hvex_misc.h"



//=====================================================================
// Misc functions
//=====================================================================

void Hier_Switch_UpdControl(implem_t* Implem, hier_node_t* nodeSwitch) {
	#ifndef NDEBUG
	hier_switch_t* switch_data = &nodeSwitch->NODE.SWITCH;
	#endif

	foreach(nodeSwitch->CHILDREN, scanChild) {
		hier_node_t* nodeCase = scanChild->DATA;
		hier_case_t* case_data = &nodeCase->NODE.CASE;
		// Skip the Default case
		if(case_data->is_default!=0) continue;

		// Scan all values associated to the Case
		avl_p_foreach(&case_data->values, scanval) {
			hier_testinfo_t* testinfo = scanval->data;
			// Find the signal name and actual Action
			testinfo->is_simple = false;
			#ifndef NDEBUG
			int z =
			#endif
			Hier_TestInfo_Simple(Implem, testinfo, nodeSwitch);
			#ifndef NDEBUG
			if(switch_data->info.was_if==false) {
				assert(z==0);
				assert(testinfo->sym_is_sig==true);
			}
			#endif
		}  // Case values

	}  // Scan Cases
}
// Detect the actual test of all SWITCH nodes
void Hier_Switch_UpdControl_All(implem_t* Implem) {
	avl_p_foreach(&Implem->H->NODES[HIERARCHY_SWITCH], scanSwitch) {
		hier_node_t* nodeSwitch = scanSwitch->data;
		Hier_Switch_UpdControl(Implem, nodeSwitch);
	}
}


// Return the VEX_ATOM node that contains the tested value
hvex_t* Hier_SwitchCase_GetTestValue(hier_node_t* node, hier_testinfo_t* testinfo) {
	assert(testinfo->cond!=NULL);

	if(testinfo->is_simple==false) return NULL;
	if(testinfo->sym_is_sig==false) return NULL;
	if(testinfo->sig_asg==NULL) return NULL;

	hier_action_t* action = testinfo->sig_asg;

	hvex_t* vex_val = action->expr->operands->next;
	if(vex_val->model!=HVEX_LITERAL) {
		//printf("ERROR %s : The VEX for the Case value is not a literal.\n", __func__);
		return NULL;
	}

	return vex_val;
}


// These functions xxxTest replace the test signals by the actual expression in the test Actions

// Get the expression of a test in the State right before a Switch node
hvex_t* Hier_Switch_GetSigAsgExpr(hier_node_t* nodeSwitch, char* name) {

	// Get the previous State right before the Switch
	hier_node_t* nodeBB = nodeSwitch->PREV;
	if(nodeBB==NULL) return NULL;
	if(nodeBB->TYPE!=HIERARCHY_BB) return NULL;
	hier_bb_t* bb_data = &nodeBB->NODE.BB;
	if(bb_data->body==NULL) return NULL;
	hier_node_t* nodeState = Hier_Level_GetLastNode(bb_data->body);
	if(nodeState->TYPE!=HIERARCHY_STATE) return NULL;
	hier_state_t* state_data = &nodeState->NODE.STATE;

	foreach(state_data->actions, scanAction) {
		hier_action_t* action = scanAction->DATA;
		hvex_t* Expr = action->expr;
		if(Expr->model!=HVEX_ASG) continue;
		char* destname = hvex_asg_get_destname(Expr);
		if(destname!=name) continue;
		return hvex_asg_get_expr(Expr);
	}

	return NULL;
}
hvex_t* Hier_Case_BuildFullTestExpr(hier_node_t* nodeCase, hier_testinfo_t* testinfo) {
	if(testinfo->is_simple==true) {
		hvex_t* cond = hvex_dup( hvex_asg_get_expr(testinfo->sig_asg->expr) );
		if(testinfo->oper_not==true) hvex_not(cond);
		return cond;
	}

	// Get all symbols involved in the test
	avl_p_tree_t tree_names;
	avl_p_init(&tree_names);

	hvex_GetSymbols_tree(testinfo->cond, &tree_names, NULL);

	avl_pp_tree_t tree_expr;
	avl_pp_init(&tree_expr);

	avl_p_foreach(&tree_names, scanName) {
		char* name = scanName->data;
		hvex_t* Expr = Hier_Switch_GetSigAsgExpr(nodeCase->PARENT, name);
		if(Expr==NULL) continue;
		avl_pp_add_overwrite(&tree_expr, name, Expr);
	}

	avl_p_reset(&tree_names);

	// Utility function
	void scanvex_replace(hvex_t* Expr) {
		hvex_foreach(Expr->operands, VexOp) scanvex_replace(VexOp);
		if(Expr->model!=HVEX_VECTOR) return;
		char* name = hvex_vec_getname(Expr);
		hvex_t* newvex = NULL;
		avl_pp_find_data(&tree_expr, name, (void**)&newvex);
		if(newvex==NULL) return;
		hvex_clearinside(Expr);
		hvex_replace_free(Expr, hvex_dup(newvex));
	}

	hvex_t* cond = hvex_dup(testinfo->cond);
	scanvex_replace(cond);

	avl_pp_reset(&tree_expr);

	return cond;
}

// Utility function to build a particular condition expression
// The Switch must not be only a DEFAULT case
hvex_t* Hier_Switch_NorTests(hier_node_t* nodeSwitch) {
	hvex_t* vex_nor = hvex_newmodel(HVEX_NOR, 1);
	foreach(nodeSwitch->CHILDREN, scanCase) {
		hier_node_t* nodeCase = scanCase->DATA;
		hier_case_t* case_data = & nodeCase->NODE.CASE;
		if(case_data->is_default!=0) continue;
		avl_p_foreach(&case_data->values, scanval) {
			hier_testinfo_t* testinfo = scanval->data;
			hvex_t* cond = Hier_Case_BuildFullTestExpr(nodeCase, testinfo);
			hvex_addop_head(vex_nor, cond);
		}
	}
	return hvex_new_simp(vex_nor);
}
// The Case node must not be a default case
hvex_t* Hier_Case_OrTests(hier_node_t* nodeCase) {
	hier_case_t* case_data = &nodeCase->NODE.CASE;
	unsigned nb_tests = avl_p_count(&case_data->values);

	hvex_t* vex_or = NULL;

	if(nb_tests>1) {
		vex_or = hvex_newmodel(HVEX_OR, 1);
		avl_p_foreach(&case_data->values, scanval) {
			hier_testinfo_t* testinfo = scanval->data;
			hvex_t* cond = Hier_Case_BuildFullTestExpr(nodeCase, testinfo);
			hvex_addop_head(vex_or, cond);
		}
		hvex_simp(vex_or);
	}
	else {
		hier_testinfo_t* testinfo = avl_p_root_data(&case_data->values);
		vex_or = Hier_Case_BuildFullTestExpr(nodeCase, testinfo);
	}

	return vex_or;
}
// Build the VEX condition to take the Case
hvex_t* Hier_Case_FullTest(hier_node_t* nodeCase) {
	hier_case_t* case_data = &nodeCase->NODE.CASE;
	if(case_data->is_default==0) return Hier_Case_OrTests(nodeCase);
	return Hier_Switch_NorTests(nodeCase->PARENT);
}

// These functions xxxCond keep the test signals

// Utility function to build a particular condition expression
// The Switch must not be only a DEFAULT case
hvex_t* Hier_Switch_NorConds(hier_node_t* nodeSwitch) {
	hvex_t* vex_nor = hvex_newmodel(HVEX_NOR, 1);
	foreach(nodeSwitch->CHILDREN, scanCase) {
		hier_node_t* nodeCase = scanCase->DATA;
		hier_case_t* case_data = & nodeCase->NODE.CASE;
		if(case_data->is_default!=0) continue;
		avl_p_foreach(&case_data->values, scanval) {
			hier_testinfo_t* testinfo = scanval->data;
			hvex_addop_head(vex_nor, hvex_dup(testinfo->cond));
		}
	}
	return hvex_new_simp(vex_nor);
}
// The Case node must not be a default case
hvex_t* Hier_Case_OrConds(hier_node_t* nodeCase) {
	hier_case_t* case_data = &nodeCase->NODE.CASE;
	unsigned nb_tests = avl_p_count(&case_data->values);

	hvex_t* vex_or = NULL;

	if(nb_tests>1) {
		vex_or = hvex_newmodel(HVEX_OR, 1);
		avl_p_foreach(&case_data->values, scanval) {
			hier_testinfo_t* testinfo = scanval->data;
			hvex_addop_head(vex_or, hvex_dup(testinfo->cond));
		}
		hvex_simp(vex_or);
	}
	else {
		hier_testinfo_t* testinfo = avl_p_root_data(&case_data->values);
		vex_or = hvex_dup(testinfo->cond);
	}

	return vex_or;
}
// Build the VEX condition to take the Case
hvex_t* Hier_Case_FullCond(hier_node_t* nodeCase) {
	hier_case_t* case_data = &nodeCase->NODE.CASE;
	if(case_data->is_default==0) return Hier_Case_OrConds(nodeCase);
	return Hier_Switch_NorConds(nodeCase->PARENT);
}



//=====================================================================
// Wiring conditions
//=====================================================================

bool wiring_append_prefer_mux = true;

hier_wire_style_t Hier_WireStyle_Str2ID(const char* str) {
	if(strcmp(str, "seq-auto")==0)    return HIER_WIRE_STYLE_SEQ_AUTO;
	if(strcmp(str, "seq-mux")==0)     return HIER_WIRE_STYLE_SEQ_MUX;
	if(strcmp(str, "seq-asgcond")==0) return HIER_WIRE_STYLE_SEQ_ASGCOND;
	if(strcmp(str, "one-cycle")==0)   return HIER_WIRE_STYLE_ONE_CYCLE;
	return HIER_WIRE_STYLE_NONE;
}
char* Hier_WireStyle_ID2Str(hier_wire_style_t style) {
	if(style==HIER_WIRE_STYLE_SEQ_AUTO)    return "seq-auto";
	if(style==HIER_WIRE_STYLE_SEQ_MUX)     return "seq-mux";
	if(style==HIER_WIRE_STYLE_SEQ_ASGCOND) return "seq-asgcond";
	if(style==HIER_WIRE_STYLE_ONE_CYCLE)   return "one-cycle";
	return NULL;
}

#if 0  // A complex attempt at wiring generic SWITCH...
// Disable switch wiring because it seems this causes segfaults at postprocessing, at "Simplification" step...

// This structure allows to return the final BB,
// and optional new registers (if the switch test symbol is overwritten).
typedef struct {
	chain_list*  list_newcomp;  // List of components that have been created (not linked in main netlist)
	hier_node_t* resulting_BB;
} wire_switch_t;

// List the freedom degrees
static int Hier_Switch_GetWired(hier_t* H, hier_node_t* node, wire_switch_t* result) {
	hier_switch_t* node_switch = &node->NODE.SWITCH;

	// Checks !
	int not_possible = 0;
	int all_empty = 1;
	foreach(node->CHILDREN, scanChild) {
		hier_node_t* child = scanChild->DATA;
		hier_case_t* node_case = &child->NODE.CASE;
		if(node_case->body==NULL) continue;
		if(node_case->body->TYPE==HIERARCHY_JUMP) {
			if(node_case->body->NODE.JUMP.info.is_break!=0) continue;
			printf("DEBUG %s:%d : Case %"PRId64" contains a non-break jump... Cannot cable.\n", __FILE__, __LINE__, node_case->info.value);
			not_possible = 1;
			break;
		}
		if(node_case->body->TYPE!=HIERARCHY_BB) {
			printf("DEBUG %s:%d : Case %"PRId64" does not contain a basic block... Cannot cable.\n", __FILE__, __LINE__, node_case->info.value);
			not_possible = 1;
			break;
		}
		if(node_case->body->NEXT==NULL ||
		   node_case->body->NEXT->TYPE!=HIERARCHY_JUMP ||
		   node_case->body->NEXT->NODE.JUMP.info.is_break==0
		) {
			printf("DEBUG %s:%d : Case %"PRId64" body is not followed by a break... Cannot cable.\n", __FILE__, __LINE__, node_case->info.value);
			not_possible = 1;
			break;
		}
		// Don't handle bodies that overlap several cases.
		// But allow a body to be associated several case values.
		if(node_case->body!=NULL) {
			all_empty = 0;
			if(node_case->info.case_after!=NULL) {
				printf("DEBUG %s:%d : Case %"PRId64" is not empty and followed by another Case... Cannot cable.\n", __FILE__, __LINE__, node_case->info.value);
				not_possible = 1;
				break;
			}
		}
	}
	if(not_possible!=0) return __LINE__;
	if(all_empty!=0) return 0;

	// The first and last States in the final basic block.
	hier_node_t* newnode_last = NULL;
	hier_node_t* newnode_first = NULL;

	// The switch tested symbol (register or array cell)
	// This separate variable is needed when the symbol changes : if the original symbol is overwritten !
	vexexpr* switch_test_sym = node_switch->info.switch_test;
	char* switch_test_name = getvexarrayname(switch_test_sym);

	unsigned clk_idx = 0;
	do {

		// Add the content of the Bodies

		// List all destinations that are referenced in several cases, build a big Action for them.
		// Add the other assignations in separate Actions, with the suitable cabled conditions (mux style).

		// Destination list (bitype_list*) :
		// Field DATA_FROM : list of sources (bitype_list*)
		//   Field DATA_FROM : the list of case nodes (chain_list*)
		//   Field DATA_TO : the assigned VEX expression (dup'ed in case modif needed)
		// Field DATA_TO   : The destination VEX (dup'ed in case modif needed)
		bitype_list* list_dest = NULL;

		foreach(node->CHILDREN, scanChild) {
			hier_node_t* child = scanChild->DATA;
			hier_case_t* node_case = &child->NODE.CASE;
			hier_node_t* child_body = node_case->body;
			if(child_body==NULL) continue;
			if(child_body->TYPE!=HIERARCHY_BB) continue;  // If not BB, it is JUMP
			// Find the nth Trans that has actions
			hier_node_t* found_trans = NULL;
			hier_node_t* child_trans = child_body->NODE.BB.BODY;
			unsigned cur_clk_idx = 0;
			while(child_trans!=NULL) {
				hier_state_t* cur_trans = &child_trans->NODE.STATE;
				if(cur_clk_idx<clk_idx) {
					if(cur_trans->actions!=NULL) cur_clk_idx++;
					child_trans = child_trans->NEXT;
					continue;
				}
				if(cur_trans->actions==NULL) {
					child_trans = child_trans->NEXT;
					continue;
				}
				// Here a Transwith Actions is found for this clock cycle.
				found_trans = child_trans;
				break;
			}
			if(found_trans==NULL) continue;

			// Here we have a State with Actions to add to our new body

			hier_state_t* found_trans_data = &found_trans->NODE.STATE;
			foreach(found_trans_data->actions, scanAction) {
				hier_action_t* action = scanAction->DATA;

				// FIXMEEEEE This is never freed, MEMORY LEAK
				vexexpr* action_dest = VexAsg_DupFullDest(action->expr);
				vexexpr* action_expr = VexAsg_GetExpr(action->expr);

				// Add the destination if it does not already exist in the list
				bitype_list* elt_dest = NULL;
				foreach(list_dest, scanDest) {
					if(Vex_Compare_SameOper(scanDest->DATA_TO, action_dest)!=0) continue;
					elt_dest = scanDest;
					break;
				}
				if(elt_dest==NULL && IsVexNodeAtom(action_dest)!=0) {
					// For register assignations, the widthes can change. Handle that and adapt the VEX widthes.
					// To do that, concat bits of the destination register to fill what is missing for the smaller expressions.
					char* destName = getvexarrayname(action_dest);
					foreach(list_dest, scanDest) {
						vexexpr* vex_dest = scanDest->DATA_TO;
						if(IsVexNodeAtom(vex_dest)==0) continue;
						char* scanName = getvexarrayname(vex_dest);
						if(scanName==destName) { elt_dest=scanDest; break; }
					}
				}
				if(elt_dest==NULL) {
					list_dest = addbitype(list_dest, 0, NULL, dupvexexpr(action_dest));
					elt_dest = list_dest;
				}

				// Search the Source list element, create one if needed.
				bitype_list* elt_src = NULL;

				// Handle the case of registers separately.
				// This is because the width can change, so a resize is necessary.
				if(IsVexNodeAtom(action_dest)!=0) {
					vexexpr* vex_dest_elt = elt_dest->DATA_TO;
					char* destName = getvexarrayname(vex_dest_elt);
					// Find the width limits ('downto' style)
					unsigned max_left = action_dest->LEFT;
					unsigned min_right = action_dest->RIGHT;
					if(vex_dest_elt->LEFT>max_left) max_left = vex_dest_elt->LEFT;
					if(vex_dest_elt->RIGHT<min_right) min_right = vex_dest_elt->RIGHT;
					unsigned new_width = max_left - min_right + 1;

					// Here, add the missing bits in the shorter sides of the Vex expressions

					// Resize all previously listed Expressions
					foreach((bitype_list*)elt_dest->DATA_FROM, scanSrc) {
						vexexpr* vex_expr = scanSrc->DATA_TO;
						if(IsVexNodeOper(vex_expr)==0 || GetVexOperValue(vex_expr)!=VEX_CONCAT) {
							vexexpr* new_node = createvexoper(VEX_CONCAT, new_width);
							new_node->OPERAND = addchain(new_node->OPERAND, vex_expr);
							scanSrc->DATA_TO = new_node;
							vex_expr = new_node;
						}
						if(vex_dest_elt->LEFT<max_left) {
							vexexpr* missing_vex = createvexatomvec(destName, max_left, vex_dest_elt->LEFT+1);
							vex_expr->OPERAND = addchain(vex_expr->OPERAND, missing_vex);
						}
						if(vex_dest_elt->RIGHT<min_right) {
							vexexpr* missing_vex = createvexatomvec(destName, vex_dest_elt->RIGHT-1, min_right);
							vex_expr->OPERAND = append(vex_expr->OPERAND, addchain(NULL, missing_vex));
						}
						scanSrc->DATA_TO = Vex_Simplify(vex_expr);
					}

					// Resize the destination Vex
					vex_dest_elt->LEFT = max_left;
					vex_dest_elt->RIGHT = min_right;
					vex_dest_elt->WIDTH = vex_dest_elt->LEFT - vex_dest_elt->RIGHT + 1;

					// Resize the new Expression
					vexexpr* vex_expr = dupvexexpr(action_expr);
					if(IsVexNodeOper(vex_expr)==0 || GetVexOperValue(vex_expr)!=VEX_CONCAT) {
						vexexpr* new_node = createvexoper(VEX_CONCAT, new_width);
						new_node->OPERAND = addchain(new_node->OPERAND, vex_expr);
						vex_expr = new_node;
					}
					if(action_dest->LEFT<max_left) {
						vexexpr* missing_vex = createvexatomvec(destName, max_left, action_dest->LEFT+1);
						vex_expr->OPERAND = addchain(vex_expr->OPERAND, missing_vex);
					}
					if(action_dest->RIGHT<min_right) {
						vexexpr* missing_vex = createvexatomvec(destName, action_dest->RIGHT-1, min_right);
						vex_expr->OPERAND = append(vex_expr->OPERAND, addchain(NULL, missing_vex));
					}
					vex_expr = Vex_Simplify(vex_expr);

					// Search if the Expression already exists,
					// Create a node in the list of sources if not.
					foreach((bitype_list*)elt_dest->DATA_FROM, scanSrc) {
						if(Vex_Compare_SameOper(scanSrc->DATA_TO, action_expr)!=0) continue;
						elt_src = scanSrc;
						break;
					}
					if(elt_src==NULL) {
						elt_dest->DATA_FROM = addbitype(elt_dest->DATA_FROM, 0, NULL, vex_expr);
						elt_src = elt_dest->DATA_FROM;
					}
					else {
						freevexexpr(vex_expr);
					}
				}

				// This is for cases other than register assignation (already done above).
				if(elt_src==NULL) {
					foreach((bitype_list*)elt_dest->DATA_FROM, scanSrc) {
						if(Vex_Compare_SameOper(scanSrc->DATA_TO, action_expr)!=0) continue;
						elt_src = scanSrc;
						break;
					}
				}
				if(elt_src==NULL) {
					elt_dest->DATA_FROM = addbitype(elt_dest->DATA_FROM, 0, NULL, dupvexexpr(action_expr));
					elt_src = elt_dest->DATA_FROM;
				}

				// Add the Cases.
				hier_node_t* add_child_cur = child;
				do {
					elt_src->DATA_FROM = addchain(elt_src->DATA_FROM, add_child_cur);
					add_child_cur = add_child_cur->NODE.CASE.info.case_before;
				} while(add_child_cur!=0);

			}  // Add the Actions of one state

		}  // Scan the child cases

		// If no Action was found, the basic block is complete.
		if(list_dest==NULL) break;

		// Here we have the list of assignations for this clock cycle.

		// Create a new Hierarchy node for this State
		hier_node_t* newnode = Hier_Node_NewType(HIERARCHY_STATE);
		// Enqueue in the shared list
		if(newnode_last!=NULL) {
			newnode_last->NEXT = newnode;
			newnode->PREV = newnode_last;
		}
		else newnode_first = newnode;
		newnode_last = newnode;

		// Create the new list of Actions and add to the new State

		int switch_test_sym_overwritten = 0;

		foreach(list_dest, scanDest) {
			// Check if this element was merged with another...
			if(scanDest->DATA_FROM==NULL || scanDest->DATA_TO==NULL) continue;

			// This will contain the new VEX_EXPR of the new Action
			chain_list* operands_for_or = NULL;

			foreach((bitype_list*)scanDest->DATA_FROM, scanSrc) {
				vexexpr* vex_src = scanSrc->DATA_TO;

				// If the sources do not include the DEFAULT case, the test is about the listed cases :
				//   srcVex AND (val==Case0 or val==Case1 ...)
				// If the sources include the DEFAULT case, the test is about all other cases.
				//   srcVex AND NOT(val==otherCase0 or val==otherCase1 ...)
				int default_found = 0;
				foreach((chain_list*)scanSrc->DATA_FROM, scanCase) {
					hier_node_t* nodeCase = scanCase->DATA;
					if(nodeCase->NODE.CASE.info.is_default!=0) { default_found=1; break; }
				}

				// Build the 1-bit VEX condition
				vexexpr* vex_test = NULL;
				if(default_found==0) {
					vex_test = createvexoper(VEX_OR, 1);
					foreach((chain_list*)scanSrc->DATA_FROM, scanCase) {
						hier_node_t* nodeCase = scanCase->DATA;
						vexexpr* vex_elt = createvexoper(VEX_EQ, 1);
						vex_elt->OPERAND = addchain(vex_elt->OPERAND, createvexatomlong(nodeCase->NODE.CASE.info.value, switch_test_sym->WIDTH, 1));
						vex_elt->OPERAND = addchain(vex_elt->OPERAND, dupvexexpr(switch_test_sym));
						vex_test->OPERAND = addchain(vex_test->OPERAND, vex_elt);
					}
				}
				else {
					vex_test = createvexoper(VEX_OR, 1);
					foreach(node->CHILDREN, scanCase) {
						hier_node_t* nodeCase = scanCase->DATA;
						if(ChainList_IsDataHere(scanSrc->DATA_FROM, nodeCase)==true) continue;
						vexexpr* vex_elt = createvexoper(VEX_EQ, 1);
						vex_elt->OPERAND = addchain(vex_elt->OPERAND, createvexatomlong(nodeCase->NODE.CASE.info.value, switch_test_sym->WIDTH, 1));
						vex_elt->OPERAND = addchain(vex_elt->OPERAND, dupvexexpr(switch_test_sym));
						vex_test->OPERAND = addchain(vex_test->OPERAND, vex_elt);
					}
					vex_test = createvexunaryexpr(VEX_NOT, 1, vex_test);
				}

				// Build the VEX expression : srcVex AND condVex
				vexexpr* vex_and = createvexoper(VEX_AND, vex_src->WIDTH);
				if(vex_src->WIDTH>1) {
					vexexpr* vex_concat = createvexoper(VEX_CONCAT, vex_src->WIDTH);
					for(unsigned i=0; i<vex_src->WIDTH; i++) vex_concat->OPERAND = addchain(vex_concat->OPERAND, dupvexexpr(vex_test));
					freevexexpr(vex_test);
					vex_and->OPERAND = addchain(vex_and->OPERAND, vex_concat);
				}
				else {
					vex_and->OPERAND = addchain(vex_and->OPERAND, vex_test);
				}
				vex_and->OPERAND = addchain(vex_and->OPERAND, dupvexexpr(vex_src));

				// Add this VEX chunk to the final VEX expression :
				operands_for_or = addchain(operands_for_or, vex_and);

			}  // Scan the different source VEXes

			// This is the new VEX_EXPR of the new Action
			vexexpr* full_vex_src = createvexoper(VEX_OR, switch_test_sym->WIDTH);
			if(operands_for_or->NEXT==NULL) {
				full_vex_src->OPERAND = operands_for_or->DATA;
				freechain(operands_for_or);
			}
			else {
				full_vex_src->OPERAND = operands_for_or;
			}

			// Create the new Action on the new State
			hier_action_t* action = Hier_Action_New();
			Hier_Action_Link(newnode, action);
			vexexpr* newact_dest = scanDest->DATA_TO;
			vexexpr* newact_expr = Vex_Simplify(full_vex_src);
			if(IsVexNodeAtom(newact_dest)!=0) {
				action->expr = VexAsg_Make(newact_dest, NULL, newact_expr, NULL);
			}
			else {
				action->expr = VexAsg_Make(newact_dest->OPERAND->DATA, newact_dest->OPERAND->NEXT->DATA, newact_expr, NULL);
			}
			freevexexpr(newact_expr);

			// Detect if the tested symbol is owerwritten
			if(getvexarrayname(scanDest->DATA_TO)==switch_test_name) switch_test_sym_overwritten = 1;

		}  // Scan the destinations VEXes

		// If the tested symbol is owerwritten, save it in a temporary register.
		if(switch_test_sym_overwritten!=0) {

			// Create a new register
			char* newreg_name = Map_Netlist_MakeInstanceName(Implem, switch_test_name);
			netlist_comp_t* newreg = Netlist_Comp_Reg_New(newreg_name, switch_test_sym->WIDTH);

			vexexpr* VexAtom = createvexatomvec(newreg_name, switch_test_sym->WIDTH-1, 0);
			ClearVexNodeSigned(VexAtom);

			// Add an Action
			hier_action_t* action = Hier_Action_New();
			Hier_Action_Link(newnode, action);
			action->expr = VexAsg_Make(dupvexexpr(VexAtom), NULL, switch_test_sym, NULL);

			// Replace the switch test symbol
			// FIXME there is a memory leak with this VexAtom
			result->list_newcomp = addchain(result->list_newcomp, dupvexexpr(newreg));
			switch_test_sym = dupvexexpr(VexAtom);
			switch_test_name = newreg_name;
		}

		// Clean the list of destinations
		foreach(list_dest, scanDest) {
			foreach((bitype_list*)scanDest->DATA_FROM, scanSrc) {
				freechain(scanSrc->DATA_FROM);
				if(scanSrc->DATA_TO!=NULL) freevexexpr(scanSrc->DATA_TO);
			}
			freebitype(scanDest->DATA_FROM);
			if(scanDest->DATA_TO!=NULL) freevexexpr(scanDest->DATA_TO);
		}
		freebitype(list_dest);

		// Increment the clock cycle value
		clk_idx++;

	}while(1);  // Loop on the destination clock cycles

	// The BB can be empty if the cases contained States but no Actions.
	if(newnode_first==NULL) return 0;

	// Create the final node : one Basic BLock
	hier_node_t* newHierBB = Hier_Node_NewType(HIERARCHY_BB);
	// Link the nodes together
	newHierBB->NODE.BB.BODY = newnode_first;
	newnode_first->PARENT = newHierBB;
	newHierBB->CHILDREN = addchain(newHierBB->CHILDREN, newnode_first);

	// Save the results
	result->resulting_BB = newHierBB;

	return 0;
}

int Hier_Switch_ReplaceByWired(implem_t* Implem, hier_node_t* node) {
	//hier_switch_t* node_switch = &node->NODE.SWITCH;

	// Generate the cabled version
	wire_switch_t wire_data;
	memset(&wire_data, 0, sizeof(wire_data));
	int z = Hier_Switch_GetWired(Implem->H, node, &wire_data);
	if(z!=0) return z;

	// If new registers are needed, create them. Ensure the names are still unique.
	foreach(wire_data.list_newcomp, list) {
		netlist_comp_t* comp = list->DATA;
		// Add the new VEX declaration.
		if(Map_Netlist_GetComponent(Implem, comp->name)==NULL) {
			Netlist_Comp_SetChild(Implem->netlist.top, comp);
		}
		else {
			// In case previous transformations have independently created this register... rare case, though.
			char* prev_name = comp->name;

			// Rename the new register
			char* new_name = Map_Netlist_MakeInstanceName(Implem, prev_name);
			comp->name = new_name;

			// Now, replace all occurrences of the first name by the new one.
			chain_list* list_occur = Hier_Level_GetSymbolOccur(wire_data.resulting_BB, prev_name);
			foreach(list_occur, scanOccur) {
				vexexpr** pExpr = scanOccur->DATA;
				vexexpr* Expr = *pExpr;
				SetVexAtomValue(Expr, new_name);
			}
			freechain(list_occur);
		}
	}
	freechain(wire_data.list_newcomp);

	// Replace the node by the wired version
	Hier_ReplaceNode_Free(Implem->H, node, wire_data.resulting_BB);

	Hier_Update_Structures(Implem);

	return 0;
}

#endif

// FIXME need to check if the tested symbols are not overwritten inside...
// In this case, several solutions:
// - Scan data dependencies, if possible to schedule all in one cycle, do it
// - Save the test result before the SWITCH, and wire the cases at will.

// FIXME when new wiring styles are activated, need to update the CORE about selection of FD:
//   need to disable FDs about same node!

/* The branch bodies are simply appended to each other. Handle ASG condition and MUXB styles.
 *
 * The test is incorporated into the assignments.
 * The number of Cases can be arbitrary.
 * The tested values do not need to cover all possibilities of the tested expression.
 * The destination variables can be present in several cases.
 * Actions with wired conditions are always added a new wired condition.
 *
 * Example:
 *
 * 	case 0 :  a = <expr0>; break;
 * 	case 5 :  k = <expr5k>; l = <expr5l>; break;
 * 	default : l = <exprdl>; break;
 *
 * After conversion:
 *
 * a = muxb(test0, a, <expr0>);
 * k = muxb(test5, k, <expr5k>);
 * l = muxb(test5, l, <expr5l>);
 * l = muxb(test0 NOR test5, l, <exprdl>);
 *
 * With prefer_mux = false, the conditions are wired as ASG conditions.
 */

static hier_node_t* Hier_Switch_Wire_AppendCases(implem_t* Implem, hier_node_t* nodeSwitch, bool prefer_mux) {
	//hier_switch_t* switch_data = &nodeSwitch->NODE.SWITCH;

	// Checks
	foreach(nodeSwitch->CHILDREN, scanCase) {
		hier_node_t* nodeCase = scanCase->DATA;
		hier_case_t* case_data = & nodeCase->NODE.CASE;
		// Check the tests
		avl_p_foreach(&case_data->values, scanval) {
			hier_testinfo_t* testinfo = scanval->data;
			if(testinfo->is_simple==false) return NULL;
		}
		// Check the body
		if(case_data->body==NULL) continue;
		// The body must be only basic blocks (and/or Labels, which are skipped)
		foreach(case_data->body, nodeBody) {
			if(nodeBody->TYPE==HIERARCHY_LABEL) {
				// Forbid Labels that are destination of Jumps
				if(nodeBody->NODE.LABEL.jumps!=NULL) return NULL;
				continue;
			}
			if(nodeBody->TYPE!=HIERARCHY_BB) return NULL;
			// Check the Actions of the States of the BB
			foreach(nodeBody->NODE.BB.body, nodeState) {
				hier_state_t* state_data = &nodeState->NODE.STATE;
				foreach(state_data->actions, scanAction) {
					hier_action_t* action = scanAction->DATA;
					if(HierAct_FlagCheck(action, HIER_ACT_INL)==true) continue;
					// Only accept assignments to registers/memories
					char* name = hvex_asg_get_destname(action->expr);
					netlist_comp_t* comp = Map_Netlist_GetComponent(Implem, name);
					if(comp==NULL) return NULL;
					if(comp->model==NETLIST_COMP_SIGNAL) return NULL;
				}
			}  // Scan the States of the BB
		}  // Scan the nodes of the Case body
	}  // Scan the cases

	// Wire the branches

	hier_node_t* first_state = NULL;
	hier_node_t* prev_state = NULL;

	foreach(nodeSwitch->CHILDREN, scanCase) {
		hier_node_t* nodeCase = scanCase->DATA;
		hier_case_t* case_data = & nodeCase->NODE.CASE;
		if(case_data->body==NULL) continue;

		// Create the condition that enables the Actions of this Case
		hvex_t* cond = Hier_Case_FullTest(nodeCase);

		// The body must be only basic blocks (and/or Labels, which are skipped)
		foreach(case_data->body, nodeBody) {
			if(nodeBody->TYPE==HIERARCHY_LABEL) continue;
			assert(nodeBody->TYPE==HIERARCHY_BB);
			// Here the node is a BB, process it
			foreach(nodeBody->NODE.BB.body, scanState) {
				// Duplicate the State
				hier_node_t* newState = Hier_Node_Dup_State(scanState);
				// Enqueue in the shared list
				if(prev_state!=NULL) Hier_Nodes_Link(prev_state, newState);
				else first_state = newState;
				prev_state = newState;

				// Modify the actions
				hier_state_t* newstate_data = &newState->NODE.STATE;
				foreach(newstate_data->actions, scanAction) {
					hier_action_t* newact = scanAction->DATA;
					if(HierAct_FlagCheck(newact, HIER_ACT_INL)==true) continue;

					// If there is a wired condition, add the present cond
					if(hvex_asg_get_cond(newact->expr)!=NULL) {
						hvex_asg_andcond(newact->expr, hvex_dup(cond));
						continue;
					}

					if(prefer_mux==false) {
						hvex_asg_andcond(newact->expr, hvex_dup(cond));
					}
					else {
						hvex_t* vex_expr = hvex_asg_get_expr(newact->expr);
						hvex_t* vex_dest = hvex_DupFullDest(newact->expr);
						hvex_t* newexpr = hvex_newmodel_opn(HVEX_MUXB, vex_expr->width, hvex_dup(cond), vex_dest, hvex_dup(vex_expr), NULL);
						hvex_replace_free(vex_expr, newexpr);
					}

				}  // Modify Actions
			}  // Scan States of the original BB
		}  // Scan the nodes of the Case body

		// Clean
		hvex_free(cond);
	}

	if(first_state==NULL) return NULL;

	// Create the node : one Basic BLock
	hier_node_t* newHierBB = Hier_Node_NewType(HIERARCHY_BB);
	// Link the nodes together
	newHierBB->NODE.BB.body = first_state;
	first_state->PARENT = newHierBB;
	newHierBB->CHILDREN = addchain(newHierBB->CHILDREN, first_state);

	return newHierBB;
}

// Generate a one-cycle BB.
// All cases must be max 1 State long.
// Wired conditions are not allowed.
// FIXME Detect when symbols used in the tests are overwritten in the test State
// FIXME Handle inlined Actions
hier_node_t* Hier_Switch_GetWired_OneCycle(implem_t* Implem, hier_node_t* nodeSwitch) {
	//hier_switch_t* switch_data = &nodeSwitch->NODE.SWITCH;

	int error_line = 0;

	// Checks. Scan the cases
	foreach(nodeSwitch->CHILDREN, scanCase) {
		hier_node_t* nodeCase = scanCase->DATA;
		hier_case_t* case_data = & nodeCase->NODE.CASE;
		if(case_data->body==NULL) continue;
		// Check the body: it must be a single basic block with a single State
		hier_node_t* nodeBB = Hier_IsLevelOneType_GetSkipLabels(case_data->body, HIERARCHY_BB);
		if(nodeBB==NULL) { error_line = __LINE__; goto EXIT_POINT1; }
		hier_bb_t* bb_data = &nodeBB->NODE.BB;
		// There must be only one cycle
		if(bb_data->body->TYPE!=HIERARCHY_STATE) { error_line = __LINE__; goto EXIT_POINT1; }
		if(bb_data->body->NEXT!=NULL) { error_line = __LINE__; goto EXIT_POINT1; }
		// Check the Actions
		hier_state_t* trans_data = &bb_data->body->NODE.STATE;
		foreach(trans_data->actions, scanAction) {
			hier_action_t* action = scanAction->DATA;
			// Accept only ASG_VAR actions.
			// FIXME If signals are rejected, ports should be too, and only reg and mem should be handled
			char* name = hvex_asg_get_destname(action->expr);
			netlist_comp_t* comp = Map_Netlist_GetComponent(Implem, name);
			if(comp!=NULL && comp->model==NETLIST_COMP_SIGNAL) { error_line = __LINE__; goto EXIT_POINT1; }
			// And no wired condition
			if( hvex_asg_get_cond(action->expr)!=NULL ) { error_line = __LINE__; goto EXIT_POINT1; }
		}
	}  // Scan the cases

	EXIT_POINT1:

	if(error_line!=0) {
		#if 0  // For debug
		dbgprintf("Can't wire a SWITCH for reason %u.\n", error_line);
		LineList_Print_be(nodeSwitch->source_lines, "  Source lines: ", "\n");
		#endif
		return NULL;
	}

	// The only State in the final basic block.
	hier_node_t* newnode = Hier_Node_NewType(HIERARCHY_STATE);

	// List all Actions
	chain_list* list_actions = NULL;
	foreach(nodeSwitch->CHILDREN, scanCase) {
		hier_node_t* nodeCase = scanCase->DATA;
		hier_case_t* case_data = & nodeCase->NODE.CASE;
		if(case_data->body==NULL) continue;
		hier_node_t* nodeBB = Hier_IsLevelOneType_GetSkipLabels(case_data->body, HIERARCHY_BB);
		assert(nodeBB!=NULL);
		hier_bb_t* bb_data = &nodeBB->NODE.BB;
		hier_state_t* trans_data = &bb_data->body->NODE.STATE;
		foreach(trans_data->actions, scanAction) {
			hier_action_t* action = scanAction->DATA;
			list_actions = addchain(list_actions, action);
		}
	}  // Scan the cases

	// Now, for each destination symbol, find all cases where they are assigned, and build the final expression
	// For memories, only merge if the destination VEX are strictly the same.
	//   FIXME we should build a logic operation on the address datapath

	/* Example:

		if(test) a = <expr_true>;
		else     a = <expr_false>;

	Is converted to:

		a = (test AND <expr_true>) OR ((NOT(test) AND <expr_false>)

	*/

	while(list_actions!=NULL) {
		// Get the first action, it is the reference
		hier_action_t* ref_action = list_actions->DATA;
		hvex_t* ref_action_dest = hvex_asg_get_dest(ref_action->expr);
		hvex_t* ref_action_addr = hvex_asg_get_addr(ref_action->expr);
		char* ref_action_destname = hvex_vec_getname(ref_action_dest);
		// List all actions with a similar destination symbol (and address if memory)
		chain_list* selected_actions = addchain(NULL, ref_action);
		foreach(list_actions->NEXT, scan) {
			hier_action_t* action = scan->DATA;
			char* action_destname = hvex_asg_get_destname(action->expr);
			if(action_destname!=ref_action_destname) continue;
			if(ref_action_addr!=NULL) {
				hvex_t* action_addr = hvex_asg_get_addr(action->expr);
				if(hvex_cmp(action_addr, ref_action_addr)!=0) continue;  // FIXME Handle commutativity of operands
			}
			selected_actions = addchain(selected_actions, action);
		}
		// Remove the selected Actions from the list of Actions
		foreach(selected_actions, scan) {
			list_actions = ChainList_Remove(list_actions, scan->DATA);
		}

		// Build the new Action

		// Find the indexes max for left and right (not for memory)
		unsigned new_dest_left = ref_action_dest->left;
		unsigned new_dest_right = ref_action_dest->right;
		foreach(selected_actions, scan) {
			hier_action_t* action = scan->DATA;
			hvex_t* action_dest = hvex_asg_get_dest(action->expr);
			new_dest_right = GetMin(new_dest_right, action_dest->right);
			new_dest_left = GetMax(new_dest_left, action_dest->left);
		}

		// Build the new destination VEX
		hvex_t* new_dest = hvex_newvec(ref_action_destname, new_dest_left, new_dest_right);

		// Build the new expression
		// Scan the actions, extend them on the left and right with the destination symbol, and build the MUXDEC operation
		// Detect the DEFAULT case action, if found it replaces the last ... MUXDECIN (NOT(<tests>), dest)

		// The Action of the default case is saved here and processed after
		hier_action_t* action_default = NULL;
		// This stores the different modified expressions
		// After, they are merged into an OR expression
		chain_list* list_muxdec_operands = NULL;
		// This stores the CASE nodes where there is an Action
		avl_p_tree_t tree_covered_cases;
		avl_p_init(&tree_covered_cases);

		// Scan all selected Actions, only process those in non-default Case
		foreach(selected_actions, scanAct) {
			hier_action_t* action = scanAct->DATA;
			hier_node_t* nodeTrans = action->node;
			hier_node_t* nodeBB = Hier_GetParentNode(nodeTrans);
			hier_node_t* nodeCase = Hier_GetParentNode(nodeBB);
			hier_case_t* case_data = & nodeCase->NODE.CASE;
			// Flag the CASE as covered
			avl_p_add_overwrite(&tree_covered_cases, nodeCase, nodeCase);
			if(case_data->is_default!=0) { action_default = action; continue; }
			// Optionally, extend the expression with the destination symbol
			hvex_t* this_dest = hvex_asg_get_dest(action->expr);
			hvex_t* this_expr = hvex_dup(hvex_asg_get_expr(action->expr));
			this_expr = hvex_Resize_CatSym_FromVex(this_expr, this_dest, new_dest, ref_action_destname, ref_action_addr);
			// Build the test bit
			hvex_t* cond = Hier_Case_OrTests(nodeCase);
			// Build the MUXDECIN operand
			hvex_t* this_muxdecin = hvex_newmodel_op2(HVEX_MUXDECIN, this_expr->width, cond, this_expr);
			// Add to the list of MUXDEC operands
			list_muxdec_operands = addchain(list_muxdec_operands, this_muxdecin);
		}  // Scan the Actions

		// Build the condition for the DEFAULT case
		if(action_default!=NULL) {
			hvex_t* this_dest = hvex_asg_get_dest(action_default->expr);
			hvex_t* this_expr = hvex_dup(hvex_asg_get_expr(action_default->expr));
			this_expr = hvex_Resize_CatSym_FromVex(this_expr, this_dest, new_dest, ref_action_destname, ref_action_addr);
			// Build the condition for the DEFAULT case: NOT(test_case1 OR test_case2 OR ...)
			hvex_t* cond_default = Hier_Switch_NorTests(nodeSwitch);
			// Build the MUXDECIN operand
			hvex_t* this_muxdecin = hvex_newmodel_op2(HVEX_MUXDECIN, this_expr->width, cond_default, this_expr);
			// Add to the list of MUXDEC operands
			list_muxdec_operands = addchain(list_muxdec_operands, this_muxdecin);
		}

		// At least one Action should be found
		assert(list_muxdec_operands!=NULL);

		// If there are Cases with no Action, another OR operand with the reference symbol must be added
		bool cases_no_act = false;
		foreach(nodeSwitch->CHILDREN, scanCase) {
			hier_node_t* nodeCase = scanCase->DATA;
			if(avl_p_isthere(&tree_covered_cases, nodeCase)==true) continue;
			cases_no_act = true;
			break;
		}
		if(cases_no_act==true) {
			// Create the condition that leads to non-covered CASE nodes
			// Note: we are guaranteed that the Switch is not only a Default Case,
			//   because there is at least one covered Case and one non-covered Case
			// Example: 3 cases + default, only case2 is covered:
			//   cond = case1 or case2 or not( case1 or case2 or case3 )
			hvex_t* cond_or = hvex_newmodel(HVEX_OR, 1);
			foreach(nodeSwitch->CHILDREN, scanCase) {
				hier_node_t* nodeCase = scanCase->DATA;
				if(avl_p_isthere(&tree_covered_cases, nodeCase)==true) continue;
				hier_case_t* case_data = & nodeCase->NODE.CASE;
				if(case_data->is_default!=0) {
					hvex_t* cond = Hier_Switch_NorTests(nodeSwitch);
					hvex_addop_head(cond_or, cond);
				}
				else {
					avl_p_foreach(&case_data->values, scanval) {
						hier_testinfo_t* testinfo = scanval->data;
						hvex_t* cond = Hier_Case_BuildFullTestExpr(nodeCase, testinfo);
						hvex_addop_head(cond_or, cond);
					}
				}
			}
			hvex_simp(cond_or);
			// In all non-covered Cases, re-assign the destination symbol
			// Build the MUXDEC operand
			hvex_t* this_muxdecin = hvex_newmodel_op2(HVEX_MUXDECIN, new_dest->width, cond_or,
				hvex_ReadSym(ref_action_destname, hvex_dup(ref_action_addr), new_dest->left, new_dest->right)
			);
			// Add to the list of MUXDEC operands
			list_muxdec_operands = addchain(list_muxdec_operands, this_muxdecin);
		}

		// clean
		avl_p_reset(&tree_covered_cases);

		// Build the new expression VEX
		hvex_t* new_expr = hvex_newmodel(HVEX_MUXDEC, new_dest->width);
		foreach(list_muxdec_operands, scanDecIn) hvex_addop_head(new_expr, scanDecIn->DATA);
		freechain(list_muxdec_operands);

		// Build the new Action
		hier_action_t* newact = Hier_Action_New();
		Hier_Action_Link(newnode, newact);
		newact->expr = hvex_asg_make(new_dest, hvex_dup(hvex_asg_get_addr(ref_action->expr)), hvex_new_simp(new_expr), NULL);

		// Clean the mess
		freechain(selected_actions);

	}  // Infinite loop that scans the destinations of the Actions

	// Create the final node : one Basic BLock
	hier_node_t* newHierBB = Hier_Node_NewType(HIERARCHY_BB);
	// Link the nodes together
	newHierBB->NODE.BB.body = newnode;
	newnode->PARENT = newHierBB;
	newHierBB->CHILDREN = addchain(newHierBB->CHILDREN, newnode);

	// Return the new basic block
	return newHierBB;
}

int Hier_Switch_ReplaceByWired_SelectStyle(implem_t* Implem, hier_node_t* node, hier_wire_style_t style) {
	//hier_switch_t* switch_data = &node->NODE.SWITCH;

	// Generate the wired version
	hier_node_t* new_level = NULL;
	switch(style) {
		case HIER_WIRE_STYLE_SEQ_AUTO :
			new_level = Hier_Switch_Wire_AppendCases(Implem, node, wiring_append_prefer_mux);
			break;
		case HIER_WIRE_STYLE_SEQ_MUX :
			new_level = Hier_Switch_Wire_AppendCases(Implem, node, true);
			break;
		case HIER_WIRE_STYLE_SEQ_ASGCOND :
			new_level = Hier_Switch_Wire_AppendCases(Implem, node, false);
			break;
		case HIER_WIRE_STYLE_ONE_CYCLE :
			new_level = Hier_Switch_GetWired_OneCycle(Implem, node);
			break;
		default : return __LINE__;
	}
	if(new_level==NULL) {
		printf("DEBUG %s:%d : style %s failed to wire\n", __FILE__, __LINE__, Hier_WireStyle_ID2Str(style));
		printf("\n\n");
		Hier_PrintNode(node);
		printf("\n\n");
		return __LINE__;
	}

	// Note: the actual test Action(s) will be deleted in following simplification passes

	// For debug
	#if 0
	printf("\n");
	Hier_PrintNode(node);
	printf("\n");
	Hier_PrintNode(new_level);
	printf("\n");
	#endif

	// Replace the node by the wired version
	Hier_ReplaceNode_Free(Implem->H, node, new_level);

	Hier_Update_Control(Implem);

	return 0;
}



//=====================================================================
// Detection of feasible transformations
//=====================================================================

chain_list* Hier_Switch_FreedomDegrees_Get(implem_t* Implem) {
	chain_list* list_degrees = NULL;
	hier_t* H = Implem->H;
	if(H==NULL) return NULL;

	// List the IF nodes that have only one ACT in the branches
	avl_p_foreach(&H->NODES[HIERARCHY_SWITCH], scan) {
		hier_node_t* nodeSwitch = scan->data;
		hier_switch_t* switch_data = &nodeSwitch->NODE.SWITCH;
		// Skip the nodes already flagged as impossible
		if(switch_data->core.wire_impossible!=0) continue;

		// Check the number of Trans in the branches
		unsigned reject_line = 0;
		foreach(nodeSwitch->CHILDREN, scanCase) {
			hier_node_t* nodeCase = scanCase->DATA;
			hier_case_t* case_data = & nodeCase->NODE.CASE;
			if(case_data->body==NULL) continue;
			// Check the body: it must be a single basic block with a single State
			hier_node_t* nodeBB = Hier_IsLevelOneType_GetSkipLabels(case_data->body, HIERARCHY_BB);
			if(nodeBB==NULL) { reject_line = __LINE__; break; }
			// FIXME: An arbitrary limit
			if(case_data->body->timing.once.avg > 8) { reject_line = __LINE__; break; }
		}
		if(reject_line!=0) {
			#if 0  // For debug
			dbgprintf("Can't wire a SWITCH for reason %u.\n", reject_line);
			LineList_Print_be(nodeSwitch->source_lines, "  Source lines: ", "\n");
			#endif
			continue;
		}

		// Now test the different wiring styles

		// FIXME Building and freeing all these versions is not efficient
		// At least this new body could be saved in the freedom degree structure...
		// And it could be used to launch precise estimations (operators, scheduling...)

		// The ONE-CYCLE style
		bool b = ChainNum_IsDataHere(Implem->CORE.freedom_degrees_skipped_ops, FD_SKIP_WIRE_ONECYCLE);
		if(b==false) {
			// Try to get a wired version of this node
			hier_node_t* newnode = Hier_Switch_GetWired_OneCycle(Implem, nodeSwitch);
			if(newnode!=NULL) {

				// Build a new freedom degree
				freedom_degree_t* degree = freedom_degree_new();
				degree->Implem = Implem;

				degree->type = FD_COND_WIRE;
				degree->target.hier.d.wire_style = HIER_WIRE_STYLE_ONE_CYCLE;
				degree->target.hier.node = nodeSwitch;

				// Set the weights
				// Consider one cycle is saved
				degree->time_gain = 1 * nodeSwitch->timing.factor;
				degree->resource_cost = FD_HwCost_Level_Missing(Implem, newnode);

				// Free this new body
				Hier_FreeNodeLevel(newnode);

				list_degrees = addchain(list_degrees, degree);

				// If this style works, it is the only one considered
				continue;
			}
		}

		// Old good MUX/SEQ style
		b = ChainNum_IsDataHere(Implem->CORE.freedom_degrees_skipped_ops, FD_SKIP_WIRE_SEQ_AUTO);
		if(b==false) {
			// Try to get a wired version of this node
			hier_node_t* newnode = Hier_Switch_Wire_AppendCases(Implem, nodeSwitch, wiring_append_prefer_mux);
			if(newnode!=NULL) {

				// Add a freedom degree
				freedom_degree_t* degree = freedom_degree_new();
				degree->Implem = Implem;

				degree->type = FD_COND_WIRE;
				degree->target.hier.d.wire_style = HIER_WIRE_STYLE_SEQ_AUTO;
				degree->target.hier.node = nodeSwitch;

				// Set the weights
				// Consider one cycle is saved
				degree->time_gain = 1 * nodeSwitch->timing.factor;
				degree->resource_cost = FD_HwCost_Level_Missing(Implem, newnode);

				// Free this new body
				Hier_FreeNodeLevel(newnode);

				list_degrees = addchain(list_degrees, degree);
			}
		}


		#if 0

		// Try to get a wired version of this node
		// This process also internally performs lots of other tests
		wire_switch_t wire_data;
		memset(&wire_data, 0, sizeof(wire_data));
		int z = Hier_Switch_GetWired(H, node, &wire_data);
		if(z!=0) continue;



		// FIXME Study the hardware cost : the logic to select data (actually, decoded MUX expressed with OR/AND)
		//   And the needed R/W ports for the memories
		//   And the extra operators
		// With the new HWRES processes, evaluating this HW cost should not be too difficult

		// FIXME we should do some estimations based on this result !
		// Execution time : is average execution time < max exec time of the cases ?
		//   Only then is the wiring transformation interesting.

		foreach(wire_data.list_newcomp, list) {
			netlist_comp_t* comp = list->DATA;
			Netlist_Comp_Free(comp);
		}
		freechain(wire_data.list_newcomp);

		// Delete this new body
		// FIXME this is bad. At least this new body could be saved in the freedom degree structure...
		Hier_FreeNodeLevel(wire_data.resulting_BB);

		freedom_degree_t* degree = freedom_degree_new();
		degree->Implem = Implem;

		degree->type = FREEDOM_DEGREE_COND_WIRE;
		degree->target.hier.node = node;
		list_degrees = addchain(list_degrees, degree);

		#endif

	}  // Scan the IF nodes

	return list_degrees;
}



//=====================================================================
// Command interpreter
//=====================================================================

#include "command.h"

int Hier_Switch_Command(implem_t* Implem, command_t* cmd_data) {
	const char* cmd = Command_PopParam(cmd_data);
	if(cmd==NULL) {
		printf("Error (switch): No command received. Try 'help'.\n");
		return -1;
	}

	if(strcmp(cmd, "help")==0) {
		printf(
			"Actions on SWITCH nodes:\n"
			"  Usage : help             Display this help section.\n"
			"  Usage : list             List the SWITCH nodes.\n"
			"  Usage : list-labels      List the labels of the SWITCH nodes that have one.\n"
			"  Usage : wire-prefer-mux  For wiring style seq-auto, prefer MUX style.\n"
			"  Usage : wire-prefer-asgcond\n"
			"                           For wiring style seq-auto, prefer ASGCOND style.\n"
			"  Usage : find|findall <search> <action>\n"
			"    Search one or several SWITCH nodes and apply an action.\n"
			"    Possible search parameters :\n"
			"      line <line>          Search by declaration line.\n"
			"      idx <index>          Search by index in the list of SWITCH nodes.\n"
			"      label <name>         Search by label name.\n"
			"    Possible action parameters :\n"
			"      show | disp          Print the full node tree.\n"
			"      skel                 Print the skeleton of the node.\n"
			"      wire <style>         Perform condition wiring.\n"
			"        Possible styles: seq-auto, seq-mux, seq-asgcond, one-cycle\n"
			"      no-auto-wire         Forbid automatic wiring.\n"
		);
		return 0;
	}

	else if(strcmp(cmd, "wire-prefer-mux")==0)     { wiring_append_prefer_mux = true;  return 0; }
	else if(strcmp(cmd, "wire-prefer-asgcond")==0) { wiring_append_prefer_mux = false; return 0; }

	if(Implem->H==NULL) {
		printf("Error: The hierarchy is not built (or unknown command '%s').\n", cmd);
		return -1;
	}

	if(strcmp(cmd, "list")==0) {
		printf("There are %u SWITCH nodes.\n", avl_p_count(&Implem->H->NODES[HIERARCHY_SWITCH]));
		int index = 0;
		avl_p_foreach(&Implem->H->NODES[HIERARCHY_SWITCH], scan) {
			hier_node_t* node = scan->data;
			printf("SWITCH node index %d :\n", index);
			LineList_Print_be(node->source_lines, "  Source lines : ", "\n");
			// Display the timings
			printf("  Number of Cases : %u\n", ChainList_Count(node->CHILDREN));
			minmax_print(&node->timing.global, "  Execution time, global : ", " (clock cycles)\n");
			index++;
		}
	}

	else if(strcmp(cmd, "list-labels")==0) {
		// Store the names in a tree to de-duplicate
		avl_p_tree_t tree_labels;
		avl_p_init(&tree_labels);
		// Scan the nodes
		avl_p_foreach(&Implem->H->NODES[HIERARCHY_SWITCH], scan) {
			hier_node_t* node = scan->data;
			hier_switch_t* switch_data = &node->NODE.SWITCH;
			if(switch_data->label_name==NULL) continue;
			avl_p_add_overwrite(&tree_labels, switch_data->label_name, switch_data->label_name);
		}
		// Set the results of the command
		avl_p_foreach(&tree_labels, scan) {
			Command_Result_AddStatic(cmd_data, (char*)scan->data);
		}
		// Clean
		avl_p_reset(&tree_labels);
	}

	else if(strcmp(cmd, "find")==0 || strcmp(cmd, "findall")==0) {
		bool multiple = strcmp(cmd, "findall")==0 ? true : false;

		// The nodes found
		chain_list* list_nodes = NULL;
		// Search action
		const char* cmd_search = Command_PopParam(cmd_data);
		if(cmd_search==NULL) {
			printf("Error: Missing parameters. Try 'help'.\n");
			return -1;
		}

		if(strcmp(cmd_search, "line")==0) {
			const char* str_line = Command_PopParam(cmd_data);
			if(str_line==NULL) {
				printf("Error: Missing the line value.\n");
				return -1;
			}
			int line = atoi(str_line);
			avl_p_foreach(&Implem->H->NODES[HIERARCHY_SWITCH], scanNode) {
				hier_node_t* node = scanNode->data;
				bool b = LineList_IsThereNameLine(node->source_lines, NULL, line);
				if(b==false) continue;
				list_nodes = addchain(list_nodes, node);
				if(multiple==false) break;
			}
		}
		else if(strcmp(cmd_search, "idx")==0) {
			const char* str_index = Command_PopParam(cmd_data);
			if(str_index==NULL) {
				printf("Error: Missing the index value.\n");
				return -1;
			}
			int index = atoi(str_index);
			hier_node_t* node = avl_p_GetIdx(&Implem->H->NODES[HIERARCHY_SWITCH], index);
			if(node!=NULL) list_nodes = addchain(list_nodes, node);
		}
		else if(strcmp(cmd_search, "label")==0) {
			char* label_name = Command_PopParam_stralloc(cmd_data);
			if(label_name==NULL) {
				printf("Error: Missing label name.\n");
				return -1;
			}
			// Scan all loop nodes
			avl_p_foreach(&Implem->H->NODES[HIERARCHY_SWITCH], scanNode) {
				hier_node_t* node = scanNode->data;
				hier_switch_t* switch_data = &node->NODE.SWITCH;
				if(switch_data->label_name!=label_name) continue;
				list_nodes = addchain(list_nodes, node);
				if(multiple==false) break;
			}
		}
		else {
			printf("Error: Unknown search command '%s'.\n", cmd_search);
			return -1;
		}

		if(list_nodes==NULL) {
			printf("Error: No matching switch node was found.\n");
			return -1;
		}
		if(multiple==true) {
			printf("Nodes found: %u\n", ChainList_Count(list_nodes));
		}

		// From here, don't use return. Must use goto ENDFIND and set the ret to error value.
		unsigned ret = 0;

		// Get the action associated
		const char* cmd_action = Command_PopParam(cmd_data);
		if(cmd_action==NULL) {
			printf("Error: Missing action.\n");
			ret = -1;
			goto ENDFIND;
		}

		if(strcmp(cmd_action, "show")==0 || strcmp(cmd_action, "disp")==0) {
			printf("\n");
			foreach(list_nodes, scan) {
				hier_node_t* node = scan->DATA;
				Hier_PrintNode(node);
				printf("\n");
			}
		}
		else if(strcmp(cmd_action, "skel")==0) {
			printf("\n");
			foreach(list_nodes, scan) {
				hier_node_t* node = scan->DATA;
				Hier_PrintNodeSkel(node);
				printf("\n");
			}
		}
		else if(strcmp(cmd_action, "disp-time")==0) {
			if(multiple==true) printf("\n");
			foreach(list_nodes, scan) {
				hier_node_t* node = scan->DATA;
				minmax_print(&node->timing.once,   "  Execution time, once   : ", " (clock cycles)\n");
				minmax_print(&node->timing.global, "  Execution time, global : ", " (clock cycles)\n");
				if(multiple==true) printf("\n");
			}
		}
		else if(strcmp(cmd_action, "wire")==0) {
			// Get the style
			const char* wire_style = Command_PopParam(cmd_data);
			if(wire_style==NULL) {
				printf("Error: Missing wiring style.\n");
				ret = -1;
				goto ENDFIND;
			}
			hier_wire_style_t style_id = Hier_WireStyle_Str2ID(wire_style);
			if(style_id==HIER_WIRE_STYLE_NONE) {
				printf("Error: Unknown wiring style '%s'.\n", wire_style);
				ret = -1;
				goto ENDFIND;
			}

			foreach(list_nodes, scan) {
				hier_node_t* node = scan->DATA;
				int z = Hier_Switch_ReplaceByWired_SelectStyle(Implem, node, style_id);
				if(z!=0) {
					printf("ERROR: Failed to wire a Switch (code %d).\n", z);
					return z;
				}
			}
			Hier_Timing_Update(Implem, false);
		}
		else if(strcmp(cmd_action, "no-auto-wire")==0) {
			foreach(list_nodes, scan) {
				hier_node_t* node = scan->DATA;
				hier_switch_t* switch_data = &node->NODE.SWITCH;
				switch_data->core.wire_impossible = 1;
			}
		}
		else {
			printf("Error: Unknown action '%s'.\n", cmd_action);
			ret = -1;
			goto ENDFIND;
		}

		ENDFIND:

		// Clean
		freechain(list_nodes);

		return ret;
	}

	else {
		printf("Error (switch): Unknown command '%s'.\n", cmd);
		return -1;
	}

	return 0;
}


