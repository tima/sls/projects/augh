
/*
This file contains various functions to manipulate the internal representation.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <assert.h>
#include <inttypes.h>

#include "auto.h"
#include "hierarchy.h"
#include "hierarchy_symuse.h"
#include "schedule.h"
#include "linelist.h"
#include "techno.h"
#include "map.h"
#include "../hvex/hvex_misc.h"



// Shared config variables
bool asgpropag_en = true;
bool asgpropag_notsimple_en = false;
bool circularuse_en = true;
bool autoinline_en = true;
bool inserttempregs_inbb_en = false;

bool overbigloop_unroll_en = false;

bool asgpropag_symuse_en = false;
bool actsimp_verbose = false;
bool nodesimp_verbose = false;



//=====================================================================
// Misc exploration
//=====================================================================

char* Hier_NodeType2str(hier_type_t type) {
	switch(type) {
		case HIERARCHY_PROCESS : return "PROC";
		case HIERARCHY_BB      : return "BB";
		case HIERARCHY_STATE   : return "STATE";
		case HIERARCHY_LOOP    : return "LOOP";
		case HIERARCHY_SWITCH  : return "SWITCH";
		case HIERARCHY_CASE    : return "CASE";
		case HIERARCHY_JUMP    : return "JUMP";
		case HIERARCHY_LABEL   : return "LABEL";
		case HIERARCHY_CALL    : return "CALL";
		case HIERARCHY_RETURN  : return "RETURN";
		default : break;
	}
	return "UNKNOWN";
}

hier_node_t* Hier_GetParentNode(hier_node_t *node) {
	while(node!=NULL) {
		if(node->PARENT!=NULL) return node->PARENT;
		node = node->PREV;
	}
	return NULL;
}
hier_node_t* Hier_GetTopNode(hier_node_t *node) {
	if(node==NULL) return NULL;
	do {
		while(node->PREV!=NULL) node = node->PREV;
		if(node->PARENT!=NULL) node = node->PARENT;
		else break;
	}while(1);
	return node;
}

// Return a list that identifies the node inside the hierarchy.
// The list is of type ptype_list, the TYPE is the type of the parent node (or LEVEL),
//   And the DATA is an int that identifies the child node (or the index of a node in a LEVEL).
// The target node is reached when the TYPE matches, and the child ID is NONE.
ptype_list* HierID_Get(hier_node_t *node) {
	ptype_list* list = NULL;

	// The current node
	list = addptype(list, node->TYPE, HIER_CHILD_NONE);

	// The parent nodes
	do {
		switch(node->TYPE) {
			case HIERARCHY_CASE:
				// This node is never indexed in a LEVEL topology
				break;
			default: {
				// Get the index of the node in the current level
				long index = 0;
				while(node->PREV!=NULL) { index++; node=node->PREV; }
				// Add the level ID node
				list = addptype(list, HIERARCHY_LEVEL, (void*)index);
			}
		}

		// Look for the parent node
		hier_node_t* parent_node = node->PARENT;
		if(parent_node==NULL) break;

		// Add the ID for the parent node
		switch(parent_node->TYPE) {
			case HIERARCHY_BB:
				list = addptype(list, parent_node->TYPE, (void*)HIER_CHILD_BB_BODY);
				break;
			case HIERARCHY_LOOP:
				if(parent_node->NODE.LOOP.body_before==node) list = addptype(list, parent_node->TYPE, (void*)HIER_CHILD_LOOP_BEFORE);
				if(parent_node->NODE.LOOP.body_after==node) list = addptype(list, parent_node->TYPE, (void*)HIER_CHILD_LOOP_AFTER);
				break;
			case HIERARCHY_SWITCH:
				// Unfinished code
				printf("INTERNAL ERROR %s:%d : Unfinished code.\n", __FILE__, __LINE__);
				abort();
				//list = addptype(list, parent_node->TYPE, (void*)node->NODE.CASE.info.value);
				break;
			case HIERARCHY_CASE:
				list = addptype(list, parent_node->TYPE, (void*)HIER_CHILD_CASE_BODY);
				break;
			default : {
				printf("ERROR %s:%d : Invalid parent node type %d.\n", __FILE__, __LINE__, parent_node->TYPE);
				freeptype(list);
				list = NULL;
				break;
			}
		}

		// Handle the upper level
		node = parent_node;
	}while(1);

	return list;
}

hier_node_t* HierID_GetNode(hier_node_t* node, ptype_list* list) {
	do {
		// Integrity checks
		if(list==NULL) return NULL;
		if(node==NULL) return NULL;

		// Check the received node type
		hier_type_t node_type = list->TYPE;
		if(node_type!=HIERARCHY_LEVEL && node->TYPE!=node_type) return NULL;

		// Check if the destination is reached
		hier_child_id_t child_type = (long)list->DATA;
		if(child_type==HIER_CHILD_NONE) break;

		// Get the correct child
		switch(node_type) {
			case HIERARCHY_BB:
				if(child_type==HIER_CHILD_BB_BODY) node = node->NODE.BB.body;
				else return NULL;
				break;
			case HIERARCHY_LOOP:
				if(child_type==HIER_CHILD_LOOP_BEFORE) node = node->NODE.LOOP.body_before;
				else if(child_type==HIER_CHILD_LOOP_AFTER) node = node->NODE.LOOP.body_after;
				else return NULL;
				break;
			case HIERARCHY_SWITCH: {
				char* case_value = list->DATA;
				int found = 0;
				foreach(node->CHILDREN, scanChild) {
					hier_node_t* child_node = scanChild->DATA;
					avl_p_foreach(&child_node->NODE.CASE.values, scanval) {
						hier_testinfo_t* testinfo = scanval->data;
						hvex_t* vex_val = Hier_SwitchCase_GetTestValue(child_node, testinfo);
						assert(vex_val!=NULL);
						// Note: direct comparison of string pointers because they are stored in a dictionary
						if(hvex_lit_getval(vex_val)==case_value) { found = 1; node = child_node; break; }
					}
					if(found!=0) break;
				}
				if(found==0) return NULL;
				break;
			}
			case HIERARCHY_CASE:
				if(child_type==HIER_CHILD_CASE_BODY) node = node->NODE.CASE.body;
				else return NULL;
				break;
			default : {
				printf("ERROR %s:%d : Invalid child node type %d.\n", __FILE__, __LINE__, child_type);
				return NULL;
				break;
			}
		}

		// Next iteration
		list = list->NEXT;

	}while(1);

	return node;
}

void HierID_Free(ptype_list* list) {
	freeptype(list);
}

bool Hier_IsLevelOnlyType(hier_node_t* node, hier_type_t type) {
	if(node==NULL) return false;
	if(node->TYPE!=type) return false;
	if(node->NEXT!=NULL) return false;
	return true;
}
hier_node_t* Hier_IsLevelOneType_GetSkipLabels(hier_node_t* node, hier_type_t type) {
	hier_node_t* thenode = NULL;
	for( ; node!=NULL; node=node->NEXT) {
		if(node->TYPE==HIERARCHY_LABEL) {
			hier_label_t* label_data = &node->NODE.LABEL;
			if(label_data->jumps!=NULL) return NULL;
			continue;
		}
		if(node->TYPE!=HIERARCHY_BB) return NULL;
		if(thenode!=NULL) return NULL;
		thenode = node;
	}
	return thenode;
}
bool Hier_Level_IsEmpty_SkipLabels(hier_node_t* node) {
	for( ; node!=NULL; node=node->NEXT) {
		if(node->TYPE==HIERARCHY_LABEL) {
			hier_label_t* label_data = &node->NODE.LABEL;
			if(label_data->jumps!=NULL) return false;
			continue;
		}
		return false;
	}
	return true;
}
bool Hier_Level_HasDubiousJumpLabel_Recurs(hier_node_t* node) {
	for( ; node!=NULL; node=node->NEXT) {
		foreach(node->CHILDREN, scan) {
			bool b = Hier_Level_HasDubiousJumpLabel_Recurs(scan->DATA);
			if(b==true) return true;
		}
		if(node->TYPE==HIERARCHY_JUMP) return true;
		else if(node->TYPE==HIERARCHY_CALL) return true;
		else if(node->TYPE==HIERARCHY_RETURN) return true;
		else if(node->TYPE==HIERARCHY_LABEL) {
			hier_label_t* label_data = &node->NODE.LABEL;
			if(label_data->jumps!=NULL) return true;
		}
	}
	return false;
}
void Hier_Level_UpdateJumpTarget_Recurs(hier_node_t* node) {
	for( ; node!=NULL; node=node->NEXT) {
		foreach(node->CHILDREN, scan) {
			Hier_Level_UpdateJumpTarget_Recurs(scan->DATA);
		}
		if(node->TYPE!=HIERARCHY_JUMP) continue;
		hier_jump_t* jump_data = &node->NODE.JUMP;
		assert(jump_data->target->TYPE==HIERARCHY_LABEL);
		hier_label_t* label_data = &node->NODE.LABEL;
		label_data->jumps = ChainList_Add_NoDup(label_data->jumps, node);
	}
}

hier_node_t* Hier_GetLabelUnique_Verbose(hier_t* H, const char* name) {
	hier_node_t* thenode = NULL;
	avl_p_foreach(&H->NODES[HIERARCHY_LABEL], scan) {
		hier_node_t* node = scan->data;
		hier_label_t* label_data = &node->NODE.LABEL;
		if(label_data->name!=name) continue;
		if(thenode!=NULL) {
			printf("Error: Label '%s' was found in more than one node.\n", name);
			return NULL;
		}
		thenode = node;
	}
	if(thenode==NULL) {
		printf("Error: Label '%s' was not found.\n", name);
		return NULL;
	}
	return thenode;
}

bool Hier_Node_IsThereTypeInside(hier_node_t* node, hier_type_t type) {
	if(node->TYPE==type) return true;
	foreach(node->CHILDREN, scan) {
		if(Hier_Level_IsThereTypeInside(scan->DATA, type)==true) return true;
	}
	return false;
}
bool Hier_Level_IsThereTypeInside(hier_node_t* node, hier_type_t type) {
	for( ; node!=NULL; node=node->NEXT) {
		if(Hier_Node_IsThereTypeInside(node, type)==true) return true;
	}
	return false;
}

chain_list* Hier_Node_ListInsideType(hier_node_t* node, hier_type_t type) {
	chain_list* list = NULL;
	if(node->TYPE==type) list = addchain(list, node);
	foreach(node->CHILDREN, scan) {
		chain_list* local_list = Hier_Level_ListInsideType(scan->DATA, type);
		list = append(local_list, list);
	}
	return list;
}
chain_list* Hier_Level_ListInsideType(hier_node_t* node, hier_type_t type) {
	chain_list* list = NULL;
	for( ; node!=NULL; node=node->NEXT) {
		chain_list* local_list = Hier_Node_ListInsideType(node, type);
		list = append(local_list, list);
	}
	return list;
}

hier_node_t* Hier_Level_GetLastNode(hier_node_t* node) {
	if(node==NULL) return NULL;
	while(node->NEXT!=NULL) node = node->NEXT;
	return node;
}

unsigned Hier_Level_GetNbOfNodes(hier_node_t* node) {
	unsigned count = 0;
	for( ; node!=NULL; node=node->NEXT) count++;
	return count;
}

chain_list* Hier_Node_AddAct_SymIsDest(chain_list* list, hier_node_t* node, char* name) {

	if(node->TYPE==HIERARCHY_STATE) {
		hier_state_t* trans_data = &node->NODE.STATE;
		foreach(trans_data->actions, scanAction) {
			hier_action_t* action = scanAction->DATA;
			if(hvex_asg_get_destname(action->expr)==name) list = addchain(list, action);
		}
	}
	foreach(node->CHILDREN, scanChild) {
		list = Hier_Level_AddAct_SymIsDest(list, scanChild->DATA, name);
	}

	return list;
}
chain_list* Hier_Level_AddAct_SymIsDest(chain_list* list, hier_node_t* node, char* name) {
	for( ; node!=NULL; node=node->NEXT) {
		list = Hier_Node_AddAct_SymIsDest(list, node, name);
	}
	return list;
}
chain_list* Hier_GetAct_SymIsDest(hier_t* H, char* name) {
	chain_list* list = NULL;
	avl_p_foreach(&H->NODES[HIERARCHY_STATE], scan) {
		hier_node_t* node = scan->data;
		hier_state_t* trans_data = &node->NODE.STATE;
		foreach(trans_data->actions, scanAction) {
			hier_action_t* action = scanAction->DATA;
			if(hvex_asg_get_destname(action->expr)==name) list = addchain(list, action);
		}
	}  // Nodes
	return list;
}

chain_list* Hier_Node_AddSymbolOccur_opt(chain_list* list, hier_node_t* node, char* name, bool doLevel, bool doRecurs) {

	do {

		if(node->TYPE==HIERARCHY_STATE) {
			hier_state_t* trans_data = &node->NODE.STATE;
			foreach(trans_data->actions, scanAction) {
				hier_action_t* action = scanAction->DATA;
				list = hvex_SearchSym_AddList(list, action->expr, name);
			}
		}

		if(doRecurs==true) {
			foreach(node->CHILDREN, scan) {
				list = Hier_Node_AddSymbolOccur_opt(list, scan->DATA, name, true, true);
			}
		}

		if(doLevel==false) break;
		node = node->NEXT;

	} while(node!=NULL);

	return list;
}
chain_list* Hier_GetSymbolOccur(hier_t* H, char* name) {
	chain_list* list = NULL;
	avl_p_foreach(&H->NODES_ALL, scan) {
		hier_node_t* node = scan->data;
		list = Hier_Node_AddSymbolOccur_opt(list, node, name, false, false);
	}
	return list;
}

chain_list* Hier_AddArrayOccur_opt(chain_list* list, hier_node_t* node, char* name, bool doLevel, bool doRecurs) {

	do {

		if(node->TYPE==HIERARCHY_STATE) {
			hier_state_t* trans_data = &node->NODE.STATE;
			foreach(trans_data->actions, scanAction) {
				hier_action_t* action = scanAction->DATA;
				list = hvex_SearchArray_AddList(list, action->expr, name);
			}
		}

		if(doRecurs==true) {
			foreach(node->CHILDREN, scan) {
				list = Hier_AddArrayOccur_opt(list, scan->DATA, name, true, true);
			}
		}

		if(doLevel==false) break;
		node = node->NEXT;

	} while(node!=NULL);

	return list;
}
chain_list* Hier_GetArrayOccur(hier_t* H, char* name) {
	chain_list* list = NULL;
	avl_p_foreach(&H->NODES_ALL, scan) {
		hier_node_t* node = scan->data;
		list = Hier_AddArrayOccur_opt(list, node, name, false, false);
	}
	return list;
}

chain_list* Hier_BB_GetAllAct(hier_node_t* node) {
	chain_list* list = NULL;

	for(node=node->NODE.BB.body; node!=NULL; node=node->NEXT) {
		hier_state_t* trans_data = &node->NODE.STATE;
		foreach(trans_data->actions, scanAction) {
			list = addchain(list, scanAction->DATA);
		}
	}

	return list;
}

chain_list* Hier_GetNodes_MatchOneLine_list(chain_list* list, char* name, int line) {
	chain_list* results = NULL;

	foreach(list, scan) {
		hier_node_t* node = scan->DATA;
		bool b = LineList_IsThereNameLine(node->source_lines, name, line);
		if(b==true) results = addchain(results, node);
	}  // List of nodes

	return results;
}
chain_list* Hier_GetNodes_MatchOneLine_tree(avl_p_tree_t* tree, char* name, int line) {
	chain_list* results = NULL;

	avl_p_foreach(tree, scan) {
		hier_node_t* node = scan->data;
		bool b = LineList_IsThereNameLine(node->source_lines, name, line);
		if(b==true) results = addchain(results, node);
	}  // List of nodes

	return results;
}

chain_list* Hier_GetNodes_MatchLines_list(chain_list* list, bitype_list* lines) {
	chain_list* results = NULL;

	foreach(list, scan) {
		hier_node_t* node = scan->DATA;
		bool b = LineList_IsIncluded(node->source_lines, lines);
		if(b==true) results = addchain(results, node);
	}  // List of nodes

	return results;
}
chain_list* Hier_GetNodes_MatchLines_tree(avl_p_tree_t* tree, bitype_list* lines) {
	chain_list* results = NULL;

	avl_p_foreach(tree, scan) {
		hier_node_t* node = scan->data;
		bool b = LineList_IsIncluded(node->source_lines, lines);
		if(b==true) results = addchain(results, node);
	}  // List of nodes

	return results;
}

// Store the symbol names in the trees. The trees can be NULL.
void Hier_Node_GetSymbols_tree(hier_node_t* node, avl_p_tree_t* tree_read, avl_p_tree_t* tree_write) {
	switch(node->TYPE) {
		case HIERARCHY_STATE : {
			hier_state_t* state_data = &node->NODE.STATE;
			foreach(state_data->actions, scanAction) {
				hier_action_t* action = scanAction->DATA;
				hvex_GetSymbols_tree(action->expr, tree_read, tree_write);
			}
			break;
		}
		case HIERARCHY_LOOP : {
			hier_loop_t* loop_data = &node->NODE.LOOP;
			hvex_GetSymbols_tree(loop_data->testinfo->cond, tree_read, tree_write);
			break;
		}
		case HIERARCHY_CASE : {
			hier_case_t* case_data = &node->NODE.CASE;
			avl_p_foreach(&case_data->values, scanval) {
				hier_testinfo_t* testinfo = scanval->data;
				hvex_GetSymbols_tree(testinfo->cond, tree_read, tree_write);
			}
			break;
		}
		default : break;
	}
}
void Hier_GetSymbols_tree(hier_t* H, avl_p_tree_t* tree_read, avl_p_tree_t* tree_write) {
	avl_p_foreach(&H->NODES_ALL, scan) {
		hier_node_t* node = scan->data;
		Hier_Node_GetSymbols_tree(node, tree_read, tree_write);
	}
}

// Store a VEX per symbol in the trees. The trees can be NULL.
void Hier_Node_GetSymbols_treevex(hier_node_t* node, avl_pp_tree_t* tree_read, avl_pp_tree_t* tree_write) {
	switch(node->TYPE) {
		case HIERARCHY_STATE : {
			hier_state_t* state_data = &node->NODE.STATE;
			foreach(state_data->actions, scanAction) {
				hier_action_t* action = scanAction->DATA;
				hvex_GetAllSymbols_treevex(action->expr, tree_read, tree_write);
			}
			break;
		}
		case HIERARCHY_LOOP : {
			hier_loop_t* loop_data = &node->NODE.LOOP;
			hvex_GetAllSymbols_treevex(loop_data->testinfo->cond, tree_read, tree_write);
			break;
		}
		case HIERARCHY_CASE : {
			hier_case_t* case_data = &node->NODE.CASE;
			avl_p_foreach(&case_data->values, scanval) {
				hier_testinfo_t* testinfo = scanval->data;
				hvex_GetAllSymbols_treevex(testinfo->cond, tree_read, tree_write);
			}
			break;
		}
		default : break;
	}
}
void Hier_GetSymbols_treevex(hier_t* H, avl_pp_tree_t* tree_read, avl_pp_tree_t* tree_write) {
	avl_p_foreach(&H->NODES_ALL, scan) {
		hier_node_t* node = scan->data;
		Hier_Node_GetSymbols_treevex(node, tree_read, tree_write);
	}
}

// Key = symbol name, data = chain_list* of vexexpr**
void Hier_AllSymbols_GetVexOccur_treelist(hier_t* H, avl_pp_tree_t* tree) {
	avl_p_foreach(&H->NODES[HIERARCHY_STATE], scanNode) {
		hier_node_t* node = scanNode->data;
		hier_state_t* state_data = &node->NODE.STATE;
		// Scan the actions
		foreach(state_data->actions, scanAct) {
			hier_action_t* action = scanAct->DATA;
			hvex_SearchAllSyms_tree(action->expr, tree, true, true);
		}
	}

	avl_p_foreach(&H->NODES[HIERARCHY_LOOP], scanNode) {
		hier_node_t* node = scanNode->data;
		hier_loop_t* loop_data = &node->NODE.LOOP;
		// Scan the loop condition
		hvex_SearchAllSyms_tree(loop_data->testinfo->cond, tree, true, true);
	}

	avl_p_foreach(&H->NODES[HIERARCHY_CASE], scanNode) {
		hier_node_t* node = scanNode->data;
		hier_case_t* case_data = &node->NODE.CASE;
		// Scan the case values
		avl_p_foreach(&case_data->values, scanVal) {
			hier_testinfo_t* testinfo = scanVal->data;
			hvex_SearchAllSyms_tree(testinfo->cond, tree, true, true);
		}
	}
}

void Hier_Nodes_SetLines(hier_node_t* node, bitype_list* lines, bool do_recurs, bool do_level) {
	// Scan the level
	for( ; node!=NULL; node=node->NEXT) {

		// Scan the children
		if(do_recurs==true) {
			foreach(node->CHILDREN, scan) Hier_Nodes_SetLines(scan->DATA, lines, true, true);
		}

		// Set lines to the current node
		TestDo(node->source_lines, LineList_Free);
		node->source_lines = LineList_Dup(lines);

		// Also process Actions
		if(node->TYPE==HIERARCHY_STATE) {
			hier_state_t* state_data = &node->NODE.STATE;
			foreach(state_data->actions, scanAction) {
				hier_action_t* action = scanAction->DATA;
				TestDo(action->source_lines, LineList_Free);
				action->source_lines = LineList_Dup(lines);
			}
		}

		if(do_level==false) break;
	}
}

// About the FATHER field of VEX expressions
hier_action_t* Hier_Action_GetFromVex(hvex_t* Expr) {
	while(hvex_is_father_hvex(Expr)==true) Expr = Expr->father;
	return (hier_action_t*)Expr->father;
}
// FIXME This function may now be obsolete/unuseful because functionality is handled in HVEX lib
void Hier_Action_SetExprFather(hier_action_t* action) {
	if(action->expr==NULL) return;
	action->expr->father = (void*)action;
	action->expr->father_type = HVEX_FATHER_NONE;
}
void Hier_AllActions_SetExprFather(hier_t* H) {
	avl_p_foreach(&H->NODES[HIERARCHY_STATE], scanNode) {
		hier_node_t* node = scanNode->data;
		hier_state_t* state_data = &node->NODE.STATE;
		foreach(state_data->actions, scanAction) Hier_Action_SetExprFather(scanAction->DATA);
	}
}
void Hier_Level_SetExprFather(hier_node_t* node) {
	for( ; node!=NULL; node=node->NEXT) {
		if(node->TYPE==HIERARCHY_STATE) {
			hier_state_t* state_data = &node->NODE.STATE;
			foreach(state_data->actions, scanAction) Hier_Action_SetExprFather(scanAction->DATA);
		}
		foreach(node->CHILDREN, scanChild) Hier_Level_SetExprFather(scanChild->DATA);
	}
}

chain_list* Hier_ListActions_AddInline(chain_list* list_actions) {
	avl_p_tree_t act_to_scan;
	avl_p_init(&act_to_scan);

	foreach(list_actions, scan) avl_p_add_overwrite(&act_to_scan, scan->DATA, scan->DATA);

	do {
		bool redo = false;
		avl_p_foreach(&act_to_scan, scanAction) {
			hier_action_t* other_action = scanAction->data;
			foreach(other_action->acts_inline, scanInlAction) {
				hier_action_t* inl_action = scanInlAction->DATA;
				bool b = avl_p_add_overwrite(&act_to_scan, inl_action, inl_action);
				if(b==true) redo = true;
			}
		}
		if(redo==false) break;
	}while(1);

	freechain(list_actions);
	list_actions = NULL;
	avl_p_foreach(&act_to_scan, scan) list_actions = addchain(list_actions, scan->data);

	avl_p_reset(&act_to_scan);

	return list_actions;
}



//===================================================
// Low-level node manipulations
//===================================================

void Hier_Lists_AddNode(hier_t* H, hier_node_t* node) {
	foreach(node->CHILDREN, scan) Hier_Lists_AddLevel(H, scan->DATA);
	avl_p_add_keep(&H->NODES_ALL, node, node);
	avl_p_add_keep(&H->NODES[node->TYPE], node, node);
}
void Hier_Lists_AddLevel(hier_t* H, hier_node_t* node) {
	for( ; node!=NULL; node=node->NEXT) Hier_Lists_AddNode(H, node);
}
void Hier_Lists_RemoveNode(hier_t* H, hier_node_t* node) {
	foreach(node->CHILDREN, scan) Hier_Lists_RemoveLevel(H, scan->DATA);
	switch(node->TYPE) {
		case HIERARCHY_PROCESS :
			foreach(node->NODE.PROCESS.calls, scan) {
				hier_node_t* nodeCall = scan->DATA;
				nodeCall->NODE.JUMP.target = NULL;
			}
			foreach(node->NODE.PROCESS.returns, scan) {
				hier_node_t* nodeRet = scan->DATA;
				nodeRet->NODE.JUMP.target = NULL;
			}
			if(node==H->PROCESS) H->PROCESS = NULL;
			break;
		case HIERARCHY_STATE :
			break;
		case HIERARCHY_BB :
			break;
		case HIERARCHY_LOOP :
			break;
		case HIERARCHY_SWITCH :
			break;
		case HIERARCHY_LABEL :
			foreach(node->NODE.LABEL.jumps, scan) {
				hier_node_t* nodeJump = scan->DATA;
				nodeJump->NODE.JUMP.target = NULL;
			}
			break;
		case HIERARCHY_JUMP :
			if(node->NODE.JUMP.target!=NULL) {
				hier_node_t* nodeLabel = node->NODE.JUMP.target;
				nodeLabel->NODE.LABEL.jumps = ChainList_Remove(nodeLabel->NODE.LABEL.jumps, node);
			}
			break;
		case HIERARCHY_CALL :
			if(node->NODE.JUMP.target!=NULL) {
				hier_node_t* nodeProc = node->NODE.JUMP.target;
				nodeProc->NODE.PROCESS.calls = ChainList_Remove(nodeProc->NODE.PROCESS.calls, node);
			}
			break;
		case HIERARCHY_RETURN :
			if(node->NODE.JUMP.target!=NULL) {
				hier_node_t* nodeProc = node->NODE.JUMP.target;
				nodeProc->NODE.PROCESS.returns = ChainList_Remove(nodeProc->NODE.PROCESS.returns, node);
			}
			break;
		default : break;
	}
	avl_p_rem_key(&H->NODES_ALL, node);
	avl_p_rem_key(&H->NODES[node->TYPE], node);
}
void Hier_Lists_RemoveLevel(hier_t* H, hier_node_t* node) {
	for( ; node!=NULL; node=node->NEXT) Hier_Lists_RemoveNode(H, node);
}

void Hier_ReplaceChild(hier_node_t* parent, hier_node_t* oldnode, hier_node_t* newnode) {
	switch(parent->TYPE) {
		case HIERARCHY_BB :
			assert(parent->NODE.BB.body==oldnode);
			parent->NODE.BB.body = newnode;
			break;
		case HIERARCHY_CASE :
			assert(parent->NODE.CASE.body==oldnode);
			parent->NODE.CASE.body = newnode;
			break;
		case HIERARCHY_SWITCH : {
			if(newnode!=NULL && ( newnode->NEXT!=NULL || newnode->TYPE!=HIERARCHY_CASE) ) {
				printf("INTERNAL ERROR %s:%d : Wrong replacement of CASE node.\n", __FILE__, __LINE__);
				abort();
			}
			hier_switch_t* switch_data = &parent->NODE.SWITCH;
			assert( (switch_data->info.default_case==oldnode) == (oldnode->NODE.CASE.is_default!=0) );
			if(switch_data->info.default_case==oldnode) {
				switch_data->info.default_case = newnode;
				if(newnode!=NULL) newnode->NODE.CASE.is_default = 1;
			}
			break;
		}
		case HIERARCHY_LOOP :
			if(parent->NODE.LOOP.body_before==oldnode) parent->NODE.LOOP.body_before = newnode;
			else if(parent->NODE.LOOP.body_after==oldnode) parent->NODE.LOOP.body_after = newnode;
			else {
				abort();
			}
			break;
		default :
			printf("INTERNAL ERROR %s:%d : Attempting to replace a child in a node of type %d.\n", __FILE__, __LINE__, parent->TYPE);
			abort();
			break;
	}
	// Re-link the parent and child
	oldnode->PARENT = NULL;
	if(newnode!=NULL) newnode->PARENT = parent;
	parent->CHILDREN = ChainList_Remove(parent->CHILDREN, oldnode);
	parent->CHILDREN = ChainList_Add_NoNull(parent->CHILDREN, newnode);
}
void Hier_UnlinkNode(hier_node_t* node) {
	if(node->PARENT!=NULL) Hier_ReplaceChild(node->PARENT, node, node->NEXT);
	if(node->NEXT!=NULL) node->NEXT->PREV = node->PREV;
	if(node->PREV!=NULL) node->PREV->NEXT = node->NEXT;
	node->PARENT = NULL;
	node->NEXT = NULL;
	node->PREV = NULL;
}
void Hier_CutLinkBefore(hier_node_t* node) {
	if(node->PARENT!=NULL) {
		Hier_ReplaceChild(node->PARENT, node, NULL);
		node->PARENT = NULL;
	}
	if(node->PREV!=NULL) {
		node->PREV->NEXT = NULL;
		node->PREV = NULL;
	}
}
void Hier_CutLinkAfter(hier_node_t* node) {
	if(node->NEXT!=NULL) {
		node->NEXT->PREV = NULL;
		node->NEXT = NULL;
	}
}

// The specified node only is replaced by a new level
// The old node and the new level must be completely unrelated.
void Hier_ReplaceNode(hier_node_t* oldnode, hier_node_t* newnode) {
	if(newnode==NULL) {
		Hier_UnlinkNode(oldnode);
		return;
	}
	hier_node_t* new_last = Hier_Level_GetLastNode(newnode);

	hier_node_t* old_prev = oldnode->PREV;
	if(old_prev!=NULL) old_prev->NEXT = newnode;
	newnode->PREV = old_prev;
	oldnode->PREV = NULL;

	hier_node_t* old_next = oldnode->NEXT;
	if(old_next!=NULL) old_next->PREV = new_last;
	new_last->NEXT = old_next;
	oldnode->NEXT = NULL;

	if(oldnode->PARENT!=NULL) Hier_ReplaceChild(oldnode->PARENT, oldnode, newnode);
	oldnode->PARENT = NULL;
}
// In this variant the old node is completely freed
void Hier_ReplaceNode_Free(hier_t* H, hier_node_t* oldnode, hier_node_t* newnode) {
	// For debug: the replacement node must not be the being deleted
	if(newnode!=NULL) {
		assert(oldnode->PREV!=newnode && oldnode->NEXT!=newnode);
	}
	else {
		assert(oldnode->NEXT!=oldnode);
	}
	// Add the new nodes to the lists of nodes of the Hier structure
	if(newnode!=NULL) Hier_Lists_AddLevel(H, newnode);
	// Replace the old node by the new hierarchy level
	Hier_ReplaceNode(oldnode, newnode);
	Hier_Lists_RemoveNode(H, oldnode);
	Hier_FreeNode(oldnode);
}
void Hier_ReplaceLevel_Free(hier_t* H, hier_node_t* oldnode, hier_node_t* newnode) {
	// Add the new nodes to the lists of nodes of the Hier structure
	if(newnode!=NULL) Hier_Lists_AddLevel(H, newnode);
	// Replace the old Level by the new hierarchy level
	hier_node_t* prev_node = oldnode->PREV;
	if(prev_node!=NULL) {
		Hier_CutLinkBefore(oldnode);
		if(newnode!=NULL) Hier_Nodes_Link(prev_node, newnode);
	}
	else if(oldnode->PARENT!=NULL) {
		Hier_ReplaceChild(oldnode->PARENT, oldnode, newnode);
	}
	else {
		// A Process node or a Label only reachable via Jumps... no need to handle
		abort();
	}
	// Free the old Level
	Hier_Lists_RemoveLevel(H, oldnode);
	Hier_FreeNodeLevel(oldnode);
}

// The old nodes are completely freed.
void Hier_RemoveLevel_Free(hier_t* H, hier_node_t* oldnode) {
	Hier_CutLinkBefore(oldnode);
	Hier_Lists_RemoveLevel(H, oldnode);
	Hier_FreeNodeLevel(oldnode);
}

void Hier_InsertLevelAfterNode(hier_node_t* oldnode, hier_node_t* newnode) {
	if(oldnode==NULL || newnode==NULL) return;
		// Connect the end
	if(oldnode->NEXT!=NULL) Hier_Nodes_Link(Hier_Level_GetLastNode(newnode), oldnode->NEXT);
	// Connect the beginning
	Hier_Nodes_Link(oldnode, newnode);
	newnode->PARENT = NULL;
}
void Hier_InsertLevelAfterLevel(hier_node_t* node, hier_node_t* newnode) {
	if(node==NULL || newnode==NULL) return;
	Hier_Nodes_Link(Hier_Level_GetLastNode(node), newnode);
	newnode->PARENT = NULL;
}
void Hier_InsertLevelBeforeNode(hier_node_t* node, hier_node_t* newnode) {
	// Connect the PREV
	if(node->PREV==NULL) {
		assert(node->PARENT!=NULL);
		newnode->PREV = NULL;
		Hier_ReplaceChild(node->PARENT, node, newnode);
	}
	else Hier_Nodes_Link(node->PREV, newnode);
	// Connect the NEXT
	Hier_Nodes_Link(Hier_Level_GetLastNode(newnode), node);
}

// The specified node is supposed to be a STATE node
// If the parent node is a BB, it is split if needed
void Hier_ReplaceState_SplitBB(hier_t* H, hier_node_t* nodeState, hier_node_t* newLevel) {
	Hier_Lists_AddLevel(H, newLevel);

	hier_node_t* parentBB = Hier_GetParentNode(nodeState);
	if(parentBB!=NULL && parentBB->TYPE==HIERARCHY_BB) {

		// Separate the States that follow
		if(nodeState->NEXT!=NULL) {
			hier_node_t* after_bb_body = after_bb_body = nodeState->NEXT;
			Hier_CutLinkBefore(after_bb_body);
			// Create a new BB with the body After
			hier_node_t* afterBB = Hier_Node_NewType(HIERARCHY_BB);
			hier_bb_t* afterBB_data = &afterBB->NODE.BB;
			afterBB_data->body = after_bb_body;
			after_bb_body->PARENT = afterBB;
			afterBB->CHILDREN = addchain(afterBB->CHILDREN, after_bb_body);
			// Insert the new BB after the newLevel
			Hier_InsertLevelAfterLevel(newLevel, afterBB);
		}

		// Insert the newLevel
		if(nodeState->PREV==NULL) {
			Hier_ReplaceNode_Free(H, parentBB, newLevel);
		}
		else {
			Hier_ReplaceNode_Free(H, nodeState, NULL);
			Hier_InsertLevelAfterNode(parentBB, newLevel);
		}

	}
	else {
		// Note: This is for paranoia
		Hier_ReplaceNode_Free(H, nodeState, newLevel);
	}
}
void Hier_InsertLevelBeforeNode_SplitBB(hier_t* H, hier_node_t* nodeState, hier_node_t* newLevel) {
	Hier_Lists_AddLevel(H, newLevel);

	hier_node_t* parentBB = Hier_GetParentNode(nodeState);
	if(parentBB!=NULL && parentBB->TYPE==HIERARCHY_BB) {
		if(nodeState->PREV==NULL) {
			Hier_InsertLevelBeforeNode(parentBB, newLevel);
			return;
		}

		Hier_CutLinkBefore(nodeState);

		// Create a new BB with the current State as first body node
		hier_node_t* newBB = Hier_Node_NewType(HIERARCHY_BB);
		Hier_Lists_AddNode(H, newBB);
		hier_bb_t* newBB_data = &newBB->NODE.BB;
		newBB_data->body = nodeState;
		nodeState->PARENT = newBB;
		newBB->CHILDREN = addchain(newBB->CHILDREN, nodeState);

		// Insert the new BB and the new Level
		Hier_InsertLevelAfterNode(parentBB, newBB);
		Hier_InsertLevelAfterNode(parentBB, newLevel);
	}
	else {
		// Note: This is for paranoia
		Hier_InsertLevelBeforeNode(nodeState, newLevel);
	}
}
void Hier_InsertLevelAfterNode_SplitBB(hier_t* H, hier_node_t* nodeState, hier_node_t* newLevel) {
	Hier_Lists_AddLevel(H, newLevel);

	hier_node_t* parentBB = Hier_GetParentNode(nodeState);
	if(parentBB!=NULL && parentBB->TYPE==HIERARCHY_BB) {
		if(nodeState->NEXT==NULL) {
			Hier_InsertLevelAfterNode(parentBB, newLevel);
			return;
		}

		// Switch to the next State, and do Hier_InsertLevelBeforeNode_SplitBB()
		nodeState = nodeState->NEXT;

		Hier_CutLinkBefore(nodeState);

		// Create a new BB with the current State as first body node
		hier_node_t* newBB = Hier_Node_NewType(HIERARCHY_BB);
		Hier_Lists_AddNode(H, newBB);
		hier_bb_t* newBB_data = &newBB->NODE.BB;
		newBB_data->body = nodeState;
		nodeState->PARENT = newBB;
		newBB->CHILDREN = addchain(newBB->CHILDREN, nodeState);

		// Insert the new BB and the new Level
		Hier_InsertLevelAfterNode(parentBB, newBB);
		Hier_InsertLevelAfterNode(parentBB, newLevel);
	}
	else {
		// Note: This is for paranoia
		Hier_InsertLevelAfterNode(nodeState, newLevel);
	}
}

// Note: Abort on dest index conflict
// Warning: No handling of mem access
void Hier_State_MergeSameSymAsg(hier_node_t* nodeState) {
	hier_state_t* state_data = &nodeState->NODE.STATE;

	avl_p_tree_t tree_syms;
	avl_p_init(&tree_syms);

	foreach(state_data->actions, scanAction) {
		// Get the ref action
		hier_action_t* action = scanAction->DATA;
		char* name = hvex_asg_get_destname(action->expr);
		assert(name!=NULL);  // paranoia
		if(avl_p_isthere(&tree_syms, name)==true) continue;
		if(hvex_asg_get_addr(action->expr)!=NULL) continue;
		// Search another action with same dest symbol
		foreach(scanAction->NEXT, scanOtherAction) {
			hier_action_t* otherAction = scanOtherAction->DATA;
			char* otherName = hvex_asg_get_destname(otherAction->expr);
			if(otherName!=name) continue;
			// Another Action is found
			avl_p_add_overwrite(&tree_syms, name, name);
			break;
		}
	}  // scan all actions

	avl_p_foreach(&tree_syms, scan) {
		char* name = scan->data;

		unsigned left_max = 0;
		unsigned right_min = 0;

		// Store Actions: key = RIGHT index of dest
		avl_ip_tree_t tree_idx_acts;
		avl_ip_init(&tree_idx_acts);

		foreach(state_data->actions, scanAction) {
			hier_action_t* action = scanAction->DATA;
			char* loc_name = hvex_asg_get_destname(action->expr);
			if(loc_name!=name) continue;
			if(hvex_asg_get_cond(action->expr)!=NULL) {
				printf("INTERNAL ERROR %s:%u : Merging multiple ASG with same dest does not handle wired conditions on sym '%s'\n", __FILE__, __LINE__, name);
				abort();
			}
			hvex_t* loc_dest = hvex_asg_get_dest(action->expr);
			bool b = avl_ip_add_keep(&tree_idx_acts, loc_dest->right, action);
			if(b==false) {
				printf("INTERNAL ERROR %s:%u : Multiple ASG with overlapping dest indexes on sym '%s'\n", __FILE__, __LINE__, name);
				abort();
			}
			left_max = GetMax(left_max, loc_dest->left);
			right_min = GetMin(right_min, loc_dest->right);
		}

		// Create the replacement Vex expression
		// The operands of this CONCAT operations will be added rightmost to leftmost
		hvex_t* vex_expr = hvex_newmodel(HVEX_CONCAT, left_max - right_min + 1);

		unsigned last_left = 0;
		do {
			avl_ip_node_t* ip_node = avl_ip_first(&tree_idx_acts);
			if(ip_node==NULL) break;
			hier_action_t* action = ip_node->data;
			hvex_t* loc_expr = hvex_asg_get_expr(action->expr);
			hvex_t* loc_dest = hvex_asg_get_dest(action->expr);
			if(vex_expr->operands!=NULL) {
				if(loc_dest->right <= last_left) {
					printf("INTERNAL ERROR %s:%u : Multiple ASG with overlapping dest indexes on sym '%s'\n", __FILE__, __LINE__, name);
					abort();
				}
				if(loc_dest->right > last_left + 1) {
					hvex_t* vex_fill = hvex_newvec(name, loc_dest->right-1, last_left + 1);
					hvex_addop_head(vex_expr, vex_fill);
				}
			}
			hvex_addop_head(vex_expr, hvex_dup(loc_expr));
			last_left = loc_dest->left;
			// Remove this Action
			Hier_Action_FreeFull(action);
			avl_ip_rem(&tree_idx_acts,ip_node);
		} while(1);

		assert(last_left==left_max);

		// Create the next Action
		hier_action_t* newaction = Hier_Action_New();
		newaction->expr = hvex_asg_make(
			hvex_newvec(name, left_max, right_min), NULL, vex_expr, NULL
		);
		Hier_Action_Link(nodeState, newaction);

		// Clean
		avl_ip_reset(&tree_idx_acts);

	}  // Scan all syms to replace

	// Clean
	avl_p_reset(&tree_syms);

}
void Hier_MergeSameSymAsg(hier_t* H) {
	avl_p_foreach(&H->NODES[HIERARCHY_STATE], scan) Hier_State_MergeSameSymAsg(scan->data);
}



//===================================================
// Manipulation of PROCESS nodes
//===================================================

hier_node_t* Hier_GetProc(hier_t* H, const char* name) {
	avl_p_foreach(&H->NODES[HIERARCHY_PROCESS], scan) {
		hier_node_t* nodeProc = scan->data;
		if(nodeProc->NODE.PROCESS.name_orig==name) return nodeProc;
	}
	return NULL;
}

void Hier_FlagNodesUsed(hier_node_t* fromNode, avl_p_tree_t* tree_nodes) {
	if(fromNode==NULL) return;

	// Scan the current Level
	foreach(fromNode, node) {
		// Check if already processed, and add
		bool b = avl_p_add_overwrite(tree_nodes, node, node);
		if(b==false) return;
		// Recursivity
		foreach(node->CHILDREN, scanChild) Hier_FlagNodesUsed(scanChild->DATA, tree_nodes);
		// Launch on other branches
		if(node->TYPE==HIERARCHY_JUMP) {
			Hier_FlagNodesUsed(node->NODE.JUMP.target, tree_nodes);
			return;
		}
		if(node->TYPE==HIERARCHY_CALL) Hier_FlagNodesUsed(node->NODE.JUMP.target, tree_nodes);
		if(node->TYPE==HIERARCHY_RETURN) return;
		// Infinite loops: don't continue the Level and don't try to get the parent node
		if(node->TYPE==HIERARCHY_LOOP) {
			hier_loop_t* loop_data = &node->NODE.LOOP;
			if(hier_loop_chkalways(loop_data->flags) || loop_data->testinfo->cond==NULL || Hier_TestInfo_GetVal(loop_data->testinfo)==true) return;
		}
	}

	// Now, in case we got in this Level from a Label in the middle,
	// We have to get the parent Node in case it's a Loop
	while(fromNode->PREV != NULL) fromNode = fromNode->PREV;
	hier_node_t* parentNode = fromNode->PARENT;

	// Handle when there is no Parent node
	if(parentNode==NULL) {
		// Note: Only handle PROCESS node type
		// Because for other types of nodes the control flow would be badly defined and we want to catch that
		if(fromNode->TYPE!=HIERARCHY_PROCESS) abort();
	}

	else if(parentNode->TYPE==HIERARCHY_BB) {
		// Nothing to do here because we must have reached the BB body from the BB node itself.
	}

	else if(parentNode->TYPE==HIERARCHY_LOOP) {
		Hier_FlagNodesUsed(parentNode, tree_nodes);
	}

	else if(parentNode->TYPE==HIERARCHY_SWITCH) {
		if(parentNode->NEXT!=NULL) Hier_FlagNodesUsed(parentNode->NEXT, tree_nodes);
	}
	else if(parentNode->TYPE==HIERARCHY_CASE) {
		hier_node_t* switchNode = parentNode->PARENT;
		assert(switchNode!=NULL);
		assert(switchNode->TYPE==HIERARCHY_SWITCH);
		if(switchNode->NEXT!=NULL) Hier_FlagNodesUsed(switchNode->NEXT, tree_nodes);
	}

	// Unhandled node type?
	else {
		abort();
	}

}

// Warning: This function does not handle inter-Proc JUMP
// So this function should only be used early in the loading process
void Hier_RemoveProc(hier_t* H, hier_node_t* nodeProc) {
	assert(nodeProc->NODE.PROCESS.calls==NULL);
	assert(H->PROCESS!=nodeProc);

	avl_p_tree_t tree_delnodes;
	avl_p_init(&tree_delnodes);

	// Launch scan
	Hier_FlagNodesUsed(nodeProc, &tree_delnodes);

	// A sanity check: for all LABELs to delete, their PARENT and PREV nodes must also be deleted
	#ifndef NDEBUG
	avl_p_foreach(&tree_delnodes, scan) {
		hier_node_t* node = scan->data;
		if(node->PARENT!=NULL) assert(avl_p_isthere(&tree_delnodes, node->PARENT)==true);
		if(node->PREV!=NULL) assert(avl_p_isthere(&tree_delnodes, node->PREV)==true);
		if(node->NEXT!=NULL) assert(avl_p_isthere(&tree_delnodes, node->NEXT)==true);
	}
	#endif

	// Remove all nodes
	// Node: Hier_RemoveLevel_Free() is recursive so it is launched only on head-of-level nodes
	while(avl_p_isempty(&tree_delnodes)==false) {
		avl_p_node_t* p_node = avl_p_deepest(&tree_delnodes);
		hier_node_t* node = p_node->data;
		if(avl_p_isthere(&H->NODES_ALL, node)==false) { avl_p_rem(&tree_delnodes, p_node); continue; }
		node = Hier_GetTopNode(node);
		assert(avl_p_isthere(&tree_delnodes, node)==true);
		Hier_RemoveLevel_Free(H, node);
	}

	// Clean
	avl_p_reset(&tree_delnodes);
}

// Return the number of nodes removed
unsigned Hier_RemoveUnusedNodes(hier_t* H) {
	assert(H->PROCESS!=NULL);

	avl_p_tree_t tree_usenodes;
	avl_p_init(&tree_usenodes);

	Hier_FlagNodesUsed(H->PROCESS, &tree_usenodes);

	// Work on a copy of the list of nodes
	chain_list* list_nodes = NULL;
	avl_p_foreach(&H->NODES_ALL, scan) list_nodes = addchain(list_nodes, scan->data);

	// Scan all nodes and remove the unused ones
	unsigned count = 0;
	foreach(list_nodes, scan) {
		hier_node_t* node = scan->DATA;
		if(avl_p_isthere(&H->NODES_ALL, node)==false) continue;
		if(avl_p_isthere(&tree_usenodes, node)==true) continue;
		// Note: Unlink child levels because otherwise, they would be entirely deleted,
		// regardless whether some nodes are reachable through jumps
		while(node->CHILDREN!=NULL) Hier_ReplaceChild(node, node->CHILDREN->DATA, NULL);
		Hier_ReplaceNode_Free(H, node, NULL);
		count++;
	}

	// Clean
	freechain(list_nodes);
	avl_p_reset(&tree_usenodes);

	return count;
}



//=====================================================================
// Debug functions
//=====================================================================

// Check integrity of the Hier graph
void Hier_Check_Integrity(hier_t* H) {
	bitype_list* list_errors = NULL;

	#define adderror(node, msg) \
		do { list_errors = addbitype(list_errors, __LINE__, node, msg); } while(0)

	char buf[1014];
	#define adderrorf(node, msg, ...) \
		do { sprintf(buf, msg, ##__VA_ARGS__); adderror(node, stralloc(buf)); } while(0)

	// Check all nodes
	avl_p_foreach(&H->NODES_ALL, scanNode) {
		hier_node_t* node = scanNode->data;

		// Don't fail that strong when a node not PROCESS nor LABEL is unreachable
		// It is often an intermediate state between simplification passes
		#if 0
		if(node->PREV==NULL && node->PARENT==NULL) {
			if(node->TYPE!=HIERARCHY_PROCESS && node->TYPE!=HIERARCHY_LABEL) adderror(node, "No PREV nor PARENT and bad TYPE");
		}
		#endif

		// Ensure all children are indexed in the Hier, ensure clean link parent/child
		foreach(node->CHILDREN, scanChild) {
			hier_node_t* nodeChild = scanChild->DATA;
			if(avl_p_isthere(&H->NODES_ALL, nodeChild)==false) adderror(node, "Child not listed");
			if(nodeChild->PARENT!=node) adderror(node, "Bad Child PARENT field");
			if(nodeChild->PREV!=NULL) adderror(node, "Child has PREV");
		}

		// Ensure the PREV, NEXT and PARENT nodes are indexed in the Hier
		if(node->PREV!=NULL) {
			if(avl_p_isthere(&H->NODES_ALL, node->PREV)==false) adderror(node, "PREV not listed");
			if(node->PREV->NEXT!=node) adderror(node, "Bad PREV->NEXT");
		}
		if(node->NEXT!=NULL) {
			if(avl_p_isthere(&H->NODES_ALL, node->NEXT)==false) adderror(node, "NEXT not listed");
			if(node->NEXT->PREV!=node) adderror(node, "Bad NEXT->PREV");
		}
		if(node->PARENT!=NULL) {
			if(avl_p_isthere(&H->NODES_ALL, node->PARENT)==false) adderror(node, "PARENT not listed");
			if(ChainList_IsDataHere(node->PARENT->CHILDREN, node)==false) adderror(node, "Not in PARENT list of CHILDREN");
		}
		if(node->PARENT!=NULL && node->PREV!=NULL) adderror(node, "PREV and PARENT both used");

		if(node->TYPE < 0 || node->TYPE >= HIERARCHY_TYPES_NB) {
			adderrorf(node, "Node has invalid type %d", node->TYPE);
			adderror(node, stralloc(buf));
		}
		else {
			if(avl_p_isthere(&H->NODES[node->TYPE], node)==false) {
				char* name = Hier_NodeType2str(node->TYPE);
				adderrorf(node, "Node %s not in list of nodes %s", name, name);
			}
		}

		// Check the node data
		switch(node->TYPE) {

			case HIERARCHY_PROCESS: {
				hier_proc_t* proc_data = &node->NODE.PROCESS;
				if(proc_data->name_orig==NULL) adderror(node, "Unnamed PROCESS");
				if(node->CHILDREN!=NULL || node->PREV!=NULL) adderror(node, "Proc has CHILDREN or PREV");
				else {
					avl_p_foreach(&H->NODES[HIERARCHY_PROCESS], scanOther) {
						hier_node_t* otherProc = scanOther->data;
						if(otherProc==node) continue;
						if(otherProc->NODE.PROCESS.name_orig==proc_data->name_orig) adderror(node, "Multiple PROCESS with same name");
					}
				}
				foreach(proc_data->calls, scanCall) {
					hier_node_t* nodeCall = scanCall->DATA;
					if(avl_p_isthere(&H->NODES_ALL, nodeCall)==false) adderror(node, "Call of Process not listed");
					// Note: The validity of the Call node content is checked from the Call node type
				}
				foreach(proc_data->returns, scanRet) {
					hier_node_t* nodeRet = scanRet->DATA;
					if(avl_p_isthere(&H->NODES_ALL, nodeRet)==false) adderror(node, "Return of Process not listed");
					// Note: The validity of the Return node content is checked from the Return node type
				}
				break;
			}

			case HIERARCHY_BB: {
				hier_bb_t* bb_data = &node->NODE.BB;
				if(bb_data->body==NULL) {
					if(node->CHILDREN!=NULL) adderror(node, "BB with no body has CHILDREN");
					foreach(bb_data->body, nodeState) {
						if(nodeState->TYPE!=HIERARCHY_STATE) adderror(node, "BB with node not STATE in body");
					}
				}
				else {
					if(ChainList_Count(node->CHILDREN)!=1) adderror(node, "BB with body has whong number of CHILDREN");
					if(ChainList_IsDataHere(node->CHILDREN, bb_data->body)==false) adderror(node, "BB body not in CHILDREN");
				}
				break;
			}

			case HIERARCHY_STATE: {
				hier_state_t* state_data = &node->NODE.STATE;
				if(node->CHILDREN!=NULL) adderror(node, "State has CHILDREN");
				foreach(state_data->actions, scanAct) {
					hier_action_t* action = scanAct->DATA;
					if(action->node!=node) adderror(node, "Bad node field in Action");
					hvex_checkabort(action->expr);
				}
				break;
			}

			case HIERARCHY_LOOP: {
				hier_loop_t* loop_data = &node->NODE.LOOP;
				unsigned nb_bodies = 0;
				if(loop_data->body_after!=NULL) {
					if(ChainList_IsDataHere(node->CHILDREN, loop_data->body_after)==false) adderror(node, "Loop body_after not in CHILDREN");
					nb_bodies++;
				}
				if(loop_data->body_before!=NULL) {
					if(ChainList_IsDataHere(node->CHILDREN, loop_data->body_before)==false) adderror(node, "Loop body_before not in CHILDREN");
					nb_bodies++;
				}
				if(ChainList_Count(node->CHILDREN)!=nb_bodies) adderror(node, "LOOP has wrong number of bodies");
				break;
			}

			case HIERARCHY_SWITCH: {
				hier_switch_t* switch_data = &node->NODE.SWITCH;
				if(switch_data->info.default_case!=NULL) {
					if(ChainList_IsDataHere(node->CHILDREN, switch_data->info.default_case)==false) adderror(node, "Default Case of SWITCH not in CHILDREN");
					if(switch_data->info.default_case->NODE.CASE.is_default==0) adderror(node, "Default Case of SWITCH not flagged as default");
				}
				foreach(node->CHILDREN, scanChild) {
					hier_node_t* nodeChild = scanChild->DATA;
					if(nodeChild->TYPE!=HIERARCHY_CASE) adderror(node, "SWITCH with child not CASE");
				}
				break;
			}

			case HIERARCHY_CASE: {
				hier_case_t* case_data = &node->NODE.CASE;
				if(node->PARENT==NULL) adderror(node, "CASE with no parent");
				else if(node->PARENT->TYPE!=HIERARCHY_SWITCH) adderror(node, "CASE with parent not SWITCH");
				if(case_data->body==NULL) {
					if(node->CHILDREN!=NULL) adderror(node, "CASE with no body has CHILDREN");
				}
				else {
					if(ChainList_Count(node->CHILDREN)!=1) adderror(node, "CASE with body has whong number of CHILDREN");
					if(ChainList_IsDataHere(node->CHILDREN, case_data->body)==false) adderror(node, "CASE body not in CHILDREN");
				}
				if(case_data->is_default!=0) {
					if(node->PARENT->NODE.SWITCH.info.default_case!=node) adderror(node, "Case flagged as default is not flagged in SWITCH");
				}
				break;
			}

			case HIERARCHY_LABEL: {
				hier_label_t* label_data = &node->NODE.LABEL;
				foreach(label_data->jumps, scanJump) {
					hier_node_t* nodeJump = scanJump->DATA;
					if(avl_p_isthere(&H->NODES_ALL, nodeJump)==false) adderror(node, "Source jump of LABEL not listed");
					if(nodeJump->TYPE!=HIERARCHY_JUMP) adderror(node, "Source jump of LABEL is not JUMP");
					else {
						if(nodeJump->NODE.JUMP.target!=node) adderror(node, "Source JUMP of LABEL has wrong target");
					}
				}
				break;
			}

			case HIERARCHY_JUMP: {
				hier_jump_t* jump_data = &node->NODE.JUMP;
				if(jump_data->target==NULL) adderror(node, "JUMP has no target");
				else {
					if(avl_p_isthere(&H->NODES_ALL, jump_data->target)==false) adderror(node, "Target of JUMP not listed");
					if(jump_data->target->TYPE!=HIERARCHY_LABEL) adderror(node, "Target of JUMP not LABEL");
					else {
						if(ChainList_IsDataHere(jump_data->target->NODE.LABEL.jumps, node)==false) adderror(node, "JUMP not in list of jumps in LABEL");
					}
				}
				break;
			}

			case HIERARCHY_CALL: {
				hier_jump_t* call_data = &node->NODE.JUMP;
				if(call_data->target==NULL) adderror(node, "CALL has no target");
				else {
					if(avl_p_isthere(&H->NODES_ALL, call_data->target)==false) adderror(node, "Target of CALL not listed");
					if(call_data->target->TYPE!=HIERARCHY_PROCESS) adderror(node, "Target of CALL not PROCESS");
					else {
						if(Hier_GetTopNode(node)==call_data->target) adderror(node, "Forbidden recursive call");
						if(ChainList_IsDataHere(call_data->target->NODE.PROCESS.calls, node)==false) adderror(node, "CALL not in list of calls in PROCESS");
					}
				}
				break;
			}

			case HIERARCHY_RETURN: {
				hier_jump_t* ret_data = &node->NODE.JUMP;
				if(ret_data->target==NULL) adderror(node, "RETURN has no target");
				else {
					if(avl_p_isthere(&H->NODES_ALL, ret_data->target)==false) adderror(node, "Target of RETURN not listed");
					if(ret_data->target->TYPE!=HIERARCHY_PROCESS) adderror(node, "Target of RETURN not PROCESS");
					else {
						if(ChainList_IsDataHere(ret_data->target->NODE.PROCESS.returns, node)==false) {
							adderror(node, "RETURN not in list of returns in PROCESS");
						}
					}
				}
				break;
			}

			default: {
				char buf[1014];
				sprintf(buf, "Node has invalid type %d", node->TYPE);
				adderror(node, stralloc(buf));
				break;
			}

		}  // Switch on node type

	}  // Loop on all nodes of the Hier

	// Check lists of nodes for each node type
	for(unsigned type=0; type<HIERARCHY_TYPES_NB; type++) {
		char* name = Hier_NodeType2str(type);
		avl_p_foreach(&H->NODES[type], scanNode) {
			hier_node_t* node = scanNode->data;
			if(avl_p_isthere(&H->NODES_ALL, node)==false) {
				adderrorf(node, "Node in %s is not in list of all nodes", name);
			}
			if(node->TYPE!=type) {
				adderrorf(node, "Node in %s has wrong type", name);
			}
		}
	}

	// Ensure the top-level PROC is in the list of PROC
	if(H->PROCESS!=NULL) {
		if(avl_p_isthere(&H->NODES[HIERARCHY_PROCESS], H->PROCESS)==false) adderror(H->PROCESS, "Main PROCESS not listed");
	}

	// Display errors
	if(list_errors!=NULL) {
		unsigned count = 0;
		foreach(list_errors, scan) {
			printf("ERROR node %p: %s (code %lu)\n", scan->DATA_FROM, (char*)scan->DATA_TO, scan->TYPE);
			count++;
		}
		printf("Errors found: %u\n", count);
		// Note: the list is intentionally not freed to enable manual scan for debug
		abort();
	}

	#undef adderror
	#undef adderrorf
}

void Hier_CheckAccessToNodes_ScanLevel(hier_node_t* node, avl_p_tree_t* tree) {
	for( ; node!=NULL; node=node->NEXT) {
		avl_p_add_overwrite(tree, node, node);
		foreach(node->CHILDREN, scan) {
			Hier_CheckAccessToNodes_ScanLevel(scan->DATA, tree);
		}
	}
}
int Hier_CheckAccessToNodes(hier_t* H) {
	avl_p_tree_t tree;
	avl_p_init(&tree);

	avl_p_foreach(&H->NODES[HIERARCHY_PROCESS], scan) {
		Hier_CheckAccessToNodes_ScanLevel(scan->data, &tree);
	}

	unsigned not_accessible = 0;
	avl_p_foreach(&H->NODES_ALL, scan) {
		hier_node_t* node = scan->data;
		bool b = avl_p_isthere(&tree, node);
		if(b==false) {
			not_accessible++;
			printf("INTERNAL ERROR %s:%u: A node is not accessible (type %s", __FILE__, __LINE__, Hier_NodeType2str(node->TYPE));
			LineList_Print_be(node->source_lines, ", lines ", ".\n");
		}
	}
	printf("DEBUG %s:%u: Nodes not accessible: %u\n", __FILE__, __LINE__, not_accessible);

	unsigned not_listed = 0;
	avl_p_foreach(&tree, scan) {
		hier_node_t* node = scan->data;
		bool b = avl_p_isthere(&H->NODES_ALL, node);
		if(b==false) {
			not_listed++;
			printf("INTERNAL ERROR %s:%u: A node is not listed (type %s", __FILE__, __LINE__, Hier_NodeType2str(node->TYPE));
			LineList_Print_be(node->source_lines, ", lines ", ".\n");
		}
	}
	printf("DEBUG %s:%u: Nodes not listed: %u\n", __FILE__, __LINE__, not_listed);

	avl_p_reset(&tree);

	assert(not_accessible==0);
	assert(not_listed==0);

	return not_accessible + not_listed;
}

void Implem_Vex_CheckSymbolWidthes(implem_t* Implem, hvex_t* Expr) {
	unsigned comp_width = 0;
	char* name = NULL;

	if(Expr->model==HVEX_VECTOR) {
		name = hvex_vec_getname(Expr);

		netlist_comp_t* comp = Netlist_Comp_GetChild(Implem->netlist.top, name);
		netlist_port_t* top_port = Netlist_Comp_GetPort(Implem->netlist.top, name);
		if(comp!=NULL) {
			if(comp->model==NETLIST_COMP_REGISTER) {
				netlist_register_t* reg_data = comp->data;
				comp_width = reg_data->width;
			}
			else if(comp->model==NETLIST_COMP_SIGNAL) {
				netlist_signal_t* sig_data = comp->data;
				comp_width = sig_data->width;
			}
		}
		else if(top_port!=NULL) {
			comp_width = top_port->width;
		}

	}

	else if(Expr->model==HVEX_INDEX) {
		Implem_Vex_CheckSymbolWidthes(Implem, Expr->operands);

		name = hvex_index_getname(Expr);
		netlist_comp_t* comp = Netlist_Comp_GetChild(Implem->netlist.top, name);

		if(comp!=NULL && comp->model==NETLIST_COMP_MEMORY) {
			netlist_memory_t* mem_data = comp->data;
			comp_width = mem_data->data_width;
		}

	}

	// FIXME Correctly handle ASG to memory

	else {
		hvex_foreach(Expr->operands, VexOp) Implem_Vex_CheckSymbolWidthes(Implem, VexOp);
	}

	if(comp_width > 0) {
		if(Expr->right >= comp_width || Expr->left >= comp_width || Expr->width > comp_width) {
			errprintfa("Indexes out of component bounds for symbol '%s': %u:%u:%u and comp is %u\n",
				name, Expr->left, Expr->right, Expr->width, comp_width
			);
		}
	}

}

void Implem_CheckIntegrity(implem_t* Implem) {

	// Check the integrity of the Hier graph
	Hier_Check_Integrity(Implem->H);

	// Check the integrity of the Netlist representation
	Netlist_Debug_CheckIntegrity(Implem->netlist.top);
	// FIXME Check the coherence of the pointers to control ports and dignals, FSM, instances of other Implems, etc

	// For Reg, Sig and top-level ports, check the Vex widthes
	avl_p_foreach(&Implem->H->NODES[HIERARCHY_STATE], scanNode) {
		hier_node_t* nodeState = scanNode->data;
		hier_state_t* state_data = &nodeState->NODE.STATE;
		foreach(state_data->actions, scanAct) {
			hier_action_t* action = scanAct->DATA;
			Implem_Vex_CheckSymbolWidthes(Implem, action->expr);
		}
	}

	// Check that each symbol used in the Hier is either a component or a top-level port
	avl_p_tree_t tree_syms;
	avl_p_init(&tree_syms);
	Hier_GetSymbols_tree(Implem->H, &tree_syms, &tree_syms);
	unsigned errors_nb = 0;
	avl_p_foreach(&tree_syms, scan) {
		char* name = scan->data;
		netlist_comp_t* comp = Netlist_Comp_GetChild(Implem->netlist.top, name);
		netlist_port_t* topport = Netlist_Comp_GetPort(Implem->netlist.top, name);
		netlist_access_t* topacc = Netlist_Comp_GetAccess(Implem->netlist.top, name);
		if(comp==NULL && topport==NULL && topacc==NULL) {
			errprintf("Symbol '%s' in Hier is not found in Netlist\n", name);
			errors_nb++;
		}
	}
	avl_p_reset(&tree_syms);
	if(errors_nb > 0) {
		errprintfa("Number of errors: %u\n", errors_nb);
	}

}



//=====================================================================
// Find next BB of each BB
//=====================================================================

static bool Hier_ScanNextBBs_AfterNode(hier_node_t* node, hier_node_t* prev_bb, avl_p_tree_t* scanned_nodes);

// Return true if the level is scanned entirely
static bool Hier_ScanNextBBs_ThisNode(hier_node_t* node, hier_node_t* prev_bb, avl_p_tree_t* scanned_nodes) {
	if(node==NULL) return true;
	if( avl_p_isthere(scanned_nodes, node)==true ) return true;
	avl_p_add_overwrite(scanned_nodes, node, node);

	switch(node->TYPE) {
		case HIERARCHY_BB: {
			hier_bb_t* bb_data = &node->NODE.BB;
			hier_bb_t* prevbb_data = &prev_bb->NODE.BB;
			if(bb_data->prev_bbs==NULL || (bb_data->prev_bbs->DATA != prev_bb)) {
				bb_data->prev_bbs = addchain(bb_data->prev_bbs, prev_bb);
				prevbb_data->next_bbs = addchain(prevbb_data->next_bbs, node);
			}
			return true;
		}
		case HIERARCHY_JUMP: {
			hier_jump_t* jump_data = &node->NODE.JUMP;
			hier_node_t* next_node = jump_data->target;
			assert(next_node->TYPE==HIERARCHY_LABEL);
			return Hier_ScanNextBBs_ThisNode(next_node->NEXT, prev_bb, scanned_nodes);
		}
		case HIERARCHY_CALL: {
			hier_jump_t* call_data = &node->NODE.JUMP;
			hier_node_t* next_node = call_data->target;
			assert(next_node->TYPE==HIERARCHY_PROCESS);
			return Hier_ScanNextBBs_ThisNode(next_node->NEXT, prev_bb, scanned_nodes);
		}
		case HIERARCHY_RETURN: {
			hier_jump_t* ret_data = &node->NODE.JUMP;
			hier_node_t* nodeProc = ret_data->target;
			assert(nodeProc->TYPE==HIERARCHY_PROCESS);
			hier_proc_t* proc_data = &nodeProc->NODE.PROCESS;
			bool alltrue = true;
			foreach(proc_data->calls, scan) {
				hier_node_t* nodeCall = scan->DATA;
				assert(nodeCall->TYPE==HIERARCHY_CALL);
				bool b = Hier_ScanNextBBs_ThisNode(nodeCall->NEXT, prev_bb, scanned_nodes);
				if(b==false) alltrue = false;
			}
			return alltrue;
		}
		case HIERARCHY_LABEL: {
			return Hier_ScanNextBBs_AfterNode(node, prev_bb, scanned_nodes);
		}
		case HIERARCHY_LOOP: {
			hier_loop_t* loop_data = &node->NODE.LOOP;
			if(loop_data->body_after!=NULL) {
				bool b = Hier_ScanNextBBs_ThisNode(loop_data->body_after, prev_bb, scanned_nodes);
				if(b==true) return true;
			}
			// scan the body_before and the nodes after the loop
			if(loop_data->body_before!=NULL) {
				Hier_ScanNextBBs_ThisNode(loop_data->body_before, prev_bb, scanned_nodes);
			}
			if(loop_data->testinfo->cond!=NULL) {
				return Hier_ScanNextBBs_AfterNode(node, prev_bb, scanned_nodes);
			}
			// If infinite loop, the level is fully scanned
			return true;
		}
		case HIERARCHY_SWITCH: {
			//hier_switch_t* switch_data = &node->NODE.SWITCH;
			bool nodes_all_true = true;
			foreach(node->CHILDREN, scanCase) {
				hier_node_t* nodeCase = scanCase->DATA;
				bool b = Hier_ScanNextBBs_ThisNode(nodeCase, prev_bb, scanned_nodes);
				if(b==false) nodes_all_true = false;
			}
			if(nodes_all_true==true) return true;
			return Hier_ScanNextBBs_AfterNode(node, prev_bb, scanned_nodes);
		}
		case HIERARCHY_CASE: {
			hier_case_t* case_data = &node->NODE.CASE;
			if(case_data->body!=NULL) {
				return Hier_ScanNextBBs_ThisNode(case_data->body, prev_bb, scanned_nodes);
			}
			return false;
		}
		default: {
			printf("INTERNAL ERROR %s:%u : Unexpected node type '%s'\n", __FILE__, __LINE__, Hier_NodeType2str(node->TYPE));
			abort();
		}
	}

}

static bool Hier_ScanNextBBs_AfterNode(hier_node_t* node, hier_node_t* prev_bb, avl_p_tree_t* scanned_nodes) {
	if(node==NULL) return true;

	if(node->NEXT!=NULL) {
		return Hier_ScanNextBBs_ThisNode(node->NEXT, prev_bb, scanned_nodes);
	}

	// Find the first node of the level

	hier_node_t* first_node = node;
	while(first_node->PREV!=NULL) first_node = first_node->PREV;

	hier_node_t* parent_node = first_node->PARENT;
	if(parent_node==NULL) {
		// It should be the top-level process
		return true;
	}

	switch(parent_node->TYPE) {
		case HIERARCHY_LOOP: {
			hier_loop_t* loop_data = &parent_node->NODE.LOOP;

			if(first_node==loop_data->body_after) {
				bool b = false;
				// If there is a body_before, scan it
				if(loop_data->body_before!=NULL) {
					b = Hier_ScanNextBBs_ThisNode(loop_data->body_before, prev_bb, scanned_nodes);
				}
				if(b==false) {
					// Here body_after can loop on itself
					Hier_ScanNextBBs_ThisNode(loop_data->body_after, prev_bb, scanned_nodes);
				}
			}
			else {
				// Here we were in the body_before
				// If there is a body_after, scan it
				if(loop_data->body_after!=NULL) {
					bool b = Hier_ScanNextBBs_ThisNode(loop_data->body_after, prev_bb, scanned_nodes);
					if(b==true) return true;
				}
				// Here the body_before can loop on itself
				Hier_ScanNextBBs_ThisNode(loop_data->body_before, prev_bb, scanned_nodes);
			}

			// If there is a test, scan the nodes after the loop
			if(loop_data->testinfo->cond!=NULL) {
				return Hier_ScanNextBBs_AfterNode(parent_node, prev_bb, scanned_nodes);
			}

			return true;
		}
		case HIERARCHY_SWITCH: {
			return Hier_ScanNextBBs_AfterNode(parent_node, prev_bb, scanned_nodes);
		}
		case HIERARCHY_CASE: {
			return Hier_ScanNextBBs_AfterNode(parent_node, prev_bb, scanned_nodes);
		}
		default: {
			errprintf("Unexpected node type '%s'\n", Hier_NodeType2str(parent_node->TYPE));
			abort();
		}
	}

	// Phony return value, this line is never reached.
	return true;
}

void Hier_ListNextBBofBB(hier_t* H) {

	// Free previous lists
	avl_p_foreach(&H->NODES[HIERARCHY_BB], scanBB) {
		hier_node_t* node = scanBB->data;
		hier_bb_t* bb_data = &node->NODE.BB;
		DoNull(bb_data->prev_bbs, freechain);
		DoNull(bb_data->next_bbs, freechain);
	}

	// Rebuild the lists
	avl_p_foreach(&H->NODES[HIERARCHY_BB], scanBB) {
		hier_node_t* node = scanBB->data;

		// Flag the nodes already done
		avl_p_tree_t nodes_scanned;
		avl_p_init(&nodes_scanned);

		Hier_ScanNextBBs_AfterNode(node, node, &nodes_scanned);

		#if 0  // Display, for debug
		printf("\n");
		dbgprintf("#### List of prevBB from BB with first state:\n");
		Hier_PrintNode(node->NODE.BB.body);
		foreach(node->NODE.BB.prev_bbs, scanPrev) {
			hier_node_t* prevBB = scanPrev->DATA;
			dbgprintf("This BB with first state:\n");
			Hier_PrintNode(prevBB->NODE.BB.body);
		}
		#endif

		avl_p_reset(&nodes_scanned);
	}

}



//=====================================================================
// Scheduler related
//=====================================================================

void Hier_NodeBB_ClearSched(hier_node_t* node) {
	hier_bb_t* bb_data = &node->NODE.BB;
	if(bb_data->sched.data!=NULL) Sched_free(bb_data->sched.data);
	bb_data->sched.data = NULL;
}
void Hier_ClearSched(hier_t* H) {
	if(H==NULL) return;
	avl_p_foreach(&H->NODES[HIERARCHY_BB], scan) {
		Hier_NodeBB_ClearSched(scan->data);
	}
}



//=====================================================================
// Get test information
//=====================================================================

// First, take the previous node
// Search only in a basic block or a level of STATE
hier_action_t* Hier_GetAsg_BeforeNode(hier_t* H, hier_node_t* node, char* name) {

	do {

		// First, search the previous node
		if(node->PREV!=NULL) node = node->PREV;
		else return NULL;

		// Then, process the current node
		if(node->TYPE==HIERARCHY_BB) {
			hier_bb_t* bb_data = &node->NODE.BB;
			node = Hier_Level_GetLastNode(bb_data->body);
		}

		if(node->TYPE!=HIERARCHY_STATE) {
			return NULL;
		}

		// Here the node is of type STATE
		hier_state_t* trans_data = &node->NODE.STATE;
		foreach(trans_data->actions, scanAction) {
			hier_action_t* action = scanAction->DATA;
			char* nameDest = hvex_asg_get_destname(action->expr);
			if(nameDest==name) return action;
		}

		// Nothing found, search again the previous node.

	} while(1);

	return NULL;
}
// Search only in a basic block or a level of STATE
hier_action_t* Hier_GetAsg_FromNode_Back(hier_t* H, hier_node_t* node, char* name) {
	if(node==NULL) return NULL;

	if(node->TYPE==HIERARCHY_BB) {
		hier_bb_t* bb_data = &node->NODE.BB;
		node = Hier_Level_GetLastNode(bb_data->body);
	}
	if(node->TYPE!=HIERARCHY_STATE) return NULL;

	do {

		hier_state_t* trans_data = &node->NODE.STATE;
		foreach(trans_data->actions, scanAction) {
			hier_action_t* action = scanAction->DATA;
			char* nameDest = hvex_asg_get_destname(action->expr);
			if(nameDest==name) return action;
		}

		if(node->PREV==NULL) return NULL;
		node = node->PREV;

	} while(1);

	return NULL;
}

// Computes again the interesting data
int Hier_TestInfo_GetSignal(implem_t* Implem, hier_testinfo_t* testinfo) {
	hvex_t* cond = testinfo->cond;
	Hier_TestInfo_ResetResults(testinfo);

	testinfo->cond_is_literal = false;
	testinfo->sym_is_sig = false;
	testinfo->sym_is_port = false;

	// Search a NOT operation
	testinfo->oper_not = false;
	if(cond->model==HVEX_NOT) {
		cond = cond->operands;
		testinfo->oper_not = true;
	}

	// Search a literal or a signal name
	if(cond->model==HVEX_LITERAL) {
		testinfo->cond_is_literal = true;
		testinfo->lit_value = hvex_lit_evalint(cond);
	}
	else if(cond->model==HVEX_VECTOR) {
		testinfo->sym_name = hvex_vec_getname(cond);
		if(Netlist_Comp_GetPort(Implem->netlist.top, testinfo->sym_name)!=NULL) {
			testinfo->sym_is_port = true;
		}
		else {
			netlist_comp_t* comp = Netlist_Comp_GetChild(Implem->netlist.top, testinfo->sym_name);
			if(comp!=NULL && comp->model==NETLIST_COMP_SIGNAL) {
				testinfo->sym_is_sig = true;
			}
			else {
				return __LINE__;
			}
		}
	}
	else {
		return __LINE__;
	}

	testinfo->is_simple = true;

	return 0;
}
int Hier_TestInfo_Simple(implem_t* Implem, hier_testinfo_t* testinfo, hier_node_t* node) {

	int z = Hier_TestInfo_GetSignal(Implem, testinfo);
	if(testinfo->is_simple==false) return z;
	if(testinfo->sym_is_sig==false) return __LINE__;

	// Search the Action where the Signal is written
	testinfo->sig_asg = Hier_GetAsg_BeforeNode(Implem->H, node, testinfo->sym_name);
	if(testinfo->sig_asg==NULL) {
		return __LINE__;
	}

	hvex_t* source = hvex_asg_get_expr(testinfo->sig_asg->expr);
	if(source->model==HVEX_LITERAL) {
		testinfo->asg_is_literal = true;
		testinfo->lit_value = hvex_lit_evalint(source);
	}
	else testinfo->asg_is_literal = false;

	return 0;
}
int Hier_TestInfo_Loop(implem_t* Implem, hier_node_t* node) {
	if(node->TYPE!=HIERARCHY_LOOP) return __LINE__;
	hier_loop_t* loop_data = &node->NODE.LOOP;
	hier_testinfo_t* testinfo = loop_data->testinfo;

	int z = Hier_TestInfo_GetSignal(Implem, testinfo);
	if(testinfo->is_simple==false) return z;
	if(testinfo->sym_is_sig==false) return __LINE__;

	if(loop_data->body_after==NULL) return __LINE__;

	// Search the Action where the Signal is written
	testinfo->sig_asg = Hier_GetAsg_FromNode_Back(Implem->H, Hier_Level_GetLastNode(loop_data->body_after), testinfo->sym_name);
	if(testinfo->sig_asg==NULL) {
		return __LINE__;
	}

	hvex_t* source = hvex_asg_get_expr(testinfo->sig_asg->expr);
	if(source->model==HVEX_LITERAL) {
		testinfo->asg_is_literal = true;
		testinfo->lit_value = hvex_lit_evalint(source);
	}
	else testinfo->asg_is_literal = false;

	return 0;
}

// If the test result is known: return true if cond is true, else false
// Return <0 if unknown
int Hier_TestInfo_GetVal(hier_testinfo_t* testinfo) {
	assert(testinfo->cond!=NULL);

	if(testinfo->is_simple==false) return -1;

	if(testinfo->cond_is_literal==true) {
		long val = testinfo->lit_value;
		return val!=0 ? true : false;
	}

	if(testinfo->sym_is_sig==true && testinfo->sig_asg!=NULL && testinfo->asg_is_literal==true) {
		long val = testinfo->lit_value;
		if(testinfo->oper_not==true) val = !val;
		return val!=0 ? true : false;
	}

	return -1;
}



//=====================================================================
// Display
//=====================================================================

// FIXME deprecate these 3 functions, make them aliases for new Hier_PrintAct_raw()
void HierAct_FPrint(FILE* F, hier_action_t* action) {
	hvex_fprint_b(F, action->expr);

	if(action->source_lines!=NULL) {
		fprintf(F, " ");
		LineList_Print_fbe(F, action->source_lines, "LINES = ", NULL);
	}

	fprintf(F, " FLAGS=0x%x\n", action->flags);
}
void HierAct_Print(hier_action_t* action) {
	HierAct_FPrint(stdout, action);
}
void HierAct_DebugPrint(hier_action_t* action) {
	printf("(%p) ", action);
	HierAct_Print(action);
}

#define TabPrintf(nb, ...) do{ for(int i=0; i<nb; i++) fprintf(F, "  "); fprintf(F, __VA_ARGS__); }while(0)
#define Tab(nb)            do{ for(int i=0; i<nb; i++) fprintf(F, "  "); }while(0)

void Hier_PrintAct_raw(FILE* F, hier_action_t* action, unsigned level, unsigned flags) {
	Tab(level);

	if( (flags & HIER_PRINT_DEBUG)!=0 ) fprintf(F, "[%p] ", action);
	hvex_fprint_b(F, action->expr);

	if(action->source_lines!=NULL) {
		fprintf(F, " ");
		LineList_Print_fbe(F, action->source_lines, "LINES = ", NULL);
	}
	fprintf(F, " FLAGS=0x%x", action->flags);

	if( (flags & HIER_PRINT_TIME)!=0 ) {
		fprintf(F, " delay=%g cycles=%u mapdelay=%g", action->delay, action->nb_clk, action->map_delay);
	}

	fprintf(F, "\n");
}

void Hier_PrintNode_raw(FILE* F, hier_node_t *H, unsigned level, unsigned flags) {
	if(H==NULL) return;

	Tab(level);

	// Debug mode: print node pointers
	if( (flags & HIER_PRINT_DEBUG)!=0 ) {
		fprintf(F, "[%p] ", H);
	}

	switch(H->TYPE) {

		case HIERARCHY_PROCESS : {
			hier_proc_t* proc_data = &H->NODE.PROCESS;
			fprintf(F, "PROCESS name '%s' NumCalls %u NumReturns %u\n", proc_data->name_orig,
				ChainList_Count(proc_data->calls), ChainList_Count(proc_data->returns)
			);
			break;
		}

		case HIERARCHY_STATE : {
			hier_state_t* state_data = &H->NODE.STATE;
			fprintf(F, "STATE (%d actions)", ChainList_Count(H->NODE.STATE.actions));
			if( (flags & HIER_PRINT_TIME)!=0 ) {
				fprintf(F, " (delay=%g cycles=%u mapdelay=%g mapcycles=%u)",
					state_data->time.delay, state_data->time.nb_clk, state_data->time.map_delay, state_data->time.map_nb_clk
				);
			}
			fprintf(F, "\n");
			if( (flags & HIER_PRINT_NOACT)==0 ) {
				foreach(state_data->actions, scanAction) {
					hier_action_t* action = scanAction->DATA;
					Hier_PrintAct_raw(F, action, level+1, flags);
				}
			}
			break;
		}

		case HIERARCHY_JUMP : {
			hier_jump_t* node_jump = &H->NODE.JUMP;
			fprintf(F, "JUMP ");
			switch(node_jump->type) {
				case HIER_JUMP_NONE  : break;
				case HIER_JUMP_BREAK : fprintf(F, "(break)"); break;
				case HIER_JUMP_CONT  : fprintf(F, "(continue)"); break;
				case HIER_JUMP_GOTO  : fprintf(F, "(goto)"); break;
				case HIER_JUMP_CALL  : fprintf(F, "(call)"); break;
				case HIER_JUMP_RET   : fprintf(F, "(return)"); break;
			}
			fprintf(F, " dest ");
			assert(node_jump->target->TYPE==HIERARCHY_LABEL);
			hier_label_t* label_data = &node_jump->target->NODE.LABEL;
			if(label_data->name!=NULL) fprintf(F, "name \"%s\" ", label_data->name);
			else fprintf(F, "unnamed ");
			fprintf(F, "(ptr %p)\n", node_jump->target);
			break;
		}

		case HIERARCHY_LABEL : {
			hier_label_t* node_label = &H->NODE.LABEL;
			fprintf(F, "LABEL ");
			if(node_label->name!=NULL) fprintf(F, "name \"%s\"", node_label->name);
			else fprintf(F, "unnamed");
			fprintf(F, " (ptr %p)", H);
			fprintf(F, " (jumps: %u)\n", ChainList_Count(node_label->jumps));
			break;
		}

		case HIERARCHY_BB : {
			hier_bb_t* bb_data = &H->NODE.BB;
			fprintf(F, "BASIC BLOCK (states: %u)", Hier_Level_GetNbOfNodes(bb_data->body));
			if( (flags & HIER_PRINT_NOBBBODY)!=0 ) {
				fprintf(F, "\n");
			}
			else {
				fprintf(F, " {\n");
				Hier_PrintLevel_raw(F, bb_data->body, level+1, flags);
				TabPrintf(level, "} END BASIC BLOCK\n");
			}
			break;
		}

		case HIERARCHY_LOOP : {
			hier_loop_t* node_loop = &H->NODE.LOOP;
			fprintf(F, "LOOP {\n");
			if(node_loop->label_name!=NULL) TabPrintf(level+1, "// Label: %s\n", node_loop->label_name);
			TabPrintf(level+1, "// Lines: "); LineList_Print_fbe(F, H->source_lines, NULL, "\n");
			if(hier_for_chkok(node_loop->flags)) {
				TabPrintf(level+1, "// FOR loop type.\n");
			}
			if( (flags & HIER_PRINT_DEBUG)!=0 ) {
				TabPrintf(level+1, "// Debug info: Flags 0x%08x error code %i\n", node_loop->flags, node_loop->iter.error_code);
			}
			if(node_loop->testinfo->cond!=NULL) {
				TabPrintf(level+1, "COND = ");
				hvex_fprint_bn(F, node_loop->testinfo->cond);
			}
			if(node_loop->body_after!=NULL) {
				TabPrintf(level+1, "BODY AFTER {\n");
				Hier_PrintLevel_raw(F, node_loop->body_after, level+2, flags);
				TabPrintf(level+1, "} END BODY AFTER\n");
			}
			if(node_loop->body_before!=NULL) {
				TabPrintf(level+1, "BODY BEFORE {\n");
				Hier_PrintLevel_raw(F, node_loop->body_before, level+2, flags);
				TabPrintf(level+1, "} END BODY BEFORE\n");
			}
			TabPrintf(level, "} END LOOP\n");
			break;
		}

		case HIERARCHY_SWITCH : {
			hier_switch_t* switch_data = &H->NODE.SWITCH;
			fprintf(F, "SWITCH(...) {\n");
			TabPrintf(level+1, "// Lines: "); LineList_Print_fbe(F, H->source_lines, NULL, "\n");
			if(switch_data->info.switch_test!=NULL) {
				TabPrintf(level+1, "TEST = ");
				hvex_fprint_bn(F, switch_data->info.switch_test);
			}
			foreach(H->CHILDREN, scanChild) {
				hier_node_t *node = scanChild->DATA;
				Hier_PrintLevel_raw(F, node, level+1, flags);
			}
			TabPrintf(level, "} END SWITCH\n");
			break;
		}

		case HIERARCHY_CASE : {
			hier_case_t* case_data = &H->NODE.CASE;
			fprintf(F, "CASE (");

			// Scan all values associated to the Case
			bool first_val = true;
			avl_p_foreach(&case_data->values, scanval) {
				hier_testinfo_t* testinfo = scanval->data;
				if(first_val==true) first_val = false; else fprintf(F, ", ");
				hvex_t* vex_val = Hier_SwitchCase_GetTestValue(H, testinfo);
				if(vex_val!=NULL) {
					fprintf(F, "0b%s", hvex_lit_getval(vex_val));
				}
				else {
					hvex_fprint_b(F, testinfo->cond);  // FIXME Here a bug is fixed, from from branch master
				}
			}
			if(case_data->is_default!=0) {
				if(first_val==true) first_val = false; else fprintf(F, ", ");
				fprintf(F, "default");
			}

			fprintf(F, ") {\n");
			TabPrintf(level+1, "// Lines: "); LineList_Print_fbe(F, H->source_lines, NULL, "\n");
			Hier_PrintLevel_raw(F, case_data->body, level+1, flags);
			TabPrintf(level, "} END CASE\n");
			break;
		}

		case HIERARCHY_CALL : {
			hier_jump_t* node_call = &H->NODE.JUMP;
			assert(node_call->target->TYPE==HIERARCHY_PROCESS);
			hier_proc_t* proc_data = &node_call->target->NODE.PROCESS;
			fprintf(F, "CALL -> %s\n", proc_data->name_orig);
			break;
		}

		case HIERARCHY_RETURN : {
			hier_jump_t* ret_data = &H->NODE.JUMP;
			assert(ret_data->target->TYPE==HIERARCHY_PROCESS);
			hier_proc_t* proc_data = &ret_data->target->NODE.PROCESS;
			fprintf(F, "RETURN -> %s\n", proc_data->name_orig);
			break;
		}

		default : abort();

	}  // Switch on H->TYPE

}

#undef TabPrintf
#undef Tab

void Hier_PrintLevel_raw(FILE* F, hier_node_t *node, unsigned level, unsigned flags) {
	for( ; node!=NULL; node=node->NEXT) Hier_PrintNode_raw(F, node, level, flags);
}

void Hier_Print_raw(FILE* F, hier_t *H, unsigned flags) {
	// Print the top-level process
	fputc('\n', F);
	if(H->PROCESS!=NULL) {
		Hier_PrintLevel_raw(F, H->PROCESS, 0, flags);
		fputc('\n', F);
	}
	// Print all other processes
	avl_p_foreach(&H->NODES[HIERARCHY_PROCESS], scan) {
		hier_node_t* node = scan->data;
		if(node==H->PROCESS) continue;
		Hier_PrintLevel_raw(F, node, 0, flags);
		fputc('\n', F);
	}
	// Print all Levels that begin by a Label reachable only through Jumps
	avl_p_foreach(&H->NODES[HIERARCHY_LABEL], scan) {
		hier_node_t* node = scan->data;
		if(node->PREV!=NULL || node->PARENT!=NULL) continue;
		Hier_PrintLevel_raw(F, node, 0, flags);
		fputc('\n', F);
	}
}

void Hier_PrintDetails(hier_t* H) {
	if(H==NULL) return;
	unsigned count = 0;
	printf("Number of nodes:");
	for(unsigned type=0; type<HIERARCHY_TYPES_NB; type++) {
		unsigned nb = avl_p_count(&H->NODES[type]);
		if(nb>0) printf(" %s:%u", Hier_NodeType2str(type), nb);
		count += nb;
	}
	printf(" (total %u)\n", count);
}



//=====================================================================
// Execution of user commands
//=====================================================================

#include "command.h"

int Hier_String2ActFlag_Verbose(const char* str) {
	int flag = 0;
	if(strcmp(str, "nopropag-from")==0) {
		flag = HIER_ACT_NOPROPAG_FROM;
	}
	else if(strcmp(str, "nopropag-to")==0) {
		flag = HIER_ACT_NOPROPAG_TO;
	}
	else {
		printf("Error : Unknown flag '%s'.\n", str);
	}
	return flag;
}

// Declarations for regular expressions
#include <sys/types.h>
#include <regex.h>

int Hier_Cmd_ActFlagLabels(implem_t* Implem, command_t* cmd_data) {
	unsigned errors_nb = 0;

	// The flags to be added or removed
	int flags_add = 0;
	int flags_rem = 0;

	// The list of label names, stored in a tree for fast search
	avl_p_tree_t tree_names;
	avl_p_init(&tree_names);

	// An array of reg expr patterns
	unsigned regexp_nb = 0;
	unsigned regexp_size = 0;
	regex_t* regexp_array = NULL;

	// Default flags for compilation of reg expr
	int flags_reg = REG_NOSUB;

	// Scan all parameters

	do {
		char const * param = Command_PopParam(cmd_data);
		if(param==NULL) break;

		if(strcmp(param, "-i")==0) {
			flags_reg |= REG_ICASE;
		}

		else if(strcmp(param, "-e")==0 || strcmp(param, "-E")==0) {
			// Get the reg expr pattern
			const char* patt = Command_PopParam(cmd_data);
			if(patt==NULL) {
				printf("Error : Missing pattern for parameter -e.\n");
				errors_nb ++;
				break;
			}
			// Realloc the array if needed
			if(regexp_nb >= regexp_size) {
				regexp_size = regexp_size / 2 + 16;
				regexp_array = realloc(regexp_array, regexp_size * sizeof(*regexp_array));
			}
			// Add flag EXTENDED for extended reg expr
			int flag_ext = 0;
			if(strcmp(param, "-E")==0) flag_ext = REG_EXTENDED;
			// Perform comilation of the reg expr
			regex_t* preg = regexp_array + regexp_nb;
			int z = regcomp(preg, patt, flags_reg | flag_ext);
			if(z != 0) {
				// FIXME The doc is unclear about whether a failed compilation requires free
				printf("Error : Invalid pattern '%s'.\n", patt);
				errors_nb ++;
				break;
			}
			// The new reg expr is now officially in the list
			regexp_nb++;
		}

		else if(param[0]=='-') {
			int flag = Hier_String2ActFlag_Verbose(param+1);
			if(flag==0) { errors_nb++; break; }
			flags_rem |= flag;
		}
		else if(param[0]=='+') {
			int flag = Hier_String2ActFlag_Verbose(param+1);
			if(flag==0) { errors_nb++; break; }
			flags_add |= flag;
		}

		else {
			char* name = stralloc(param);
			avl_p_add_overwrite(&tree_names, name, name);
		}

	} while(1);

	// Handle error situations

	if(errors_nb != 0) goto CLEAN;

	if(avl_p_isempty(&tree_names)==true && regexp_nb==0) {
		printf("Error : No label name nor reg expr is given.\n");
		goto CLEAN;
	}

	if(flags_add==0 && flags_rem==0) {
		printf("Error : No flags specified.\n");
		goto CLEAN;
	}

	// Now, start scanning Label nodes

	unsigned done_nb = 0;

	// Find all Label names, the corresponding State and Action, add/remove flag
	avl_p_foreach(&Implem->H->NODES[HIERARCHY_LABEL], scanLabel) {
		hier_node_t* nodeLabel = scanLabel->data;
		char* name = nodeLabel->NODE.LABEL.name;
		if(name==NULL) continue;

		// First check against the list of names, then agains the regular expressions
		bool label_ok = false;
		if(avl_p_isthere(&tree_names, name)==true) label_ok = true;
		if(label_ok==false) {
			for(unsigned i=0; i<regexp_nb; i++) {
				regex_t* preg = regexp_array + i;
				int z = regexec(preg, name, 0, NULL, 0);
				if(z!=0) continue;
				label_ok = true;
				break;
			}
			// Check against the regular expressions
		}
		if(label_ok==false) continue;

		// Get the State/Action that follows
		// TODO skip other Label nodes
		if(nodeLabel->NEXT==NULL) continue;
		if(nodeLabel->NEXT->TYPE!=HIERARCHY_BB) continue;
		hier_node_t* nodeBB = nodeLabel->NEXT;
		hier_node_t* nodeState = nodeBB->NODE.BB.body;
		if(nodeState==NULL) continue;

		// Add the flags to the Actions
		hier_state_t* state_data = &nodeState->NODE.STATE;
		if(state_data->actions==NULL) continue;
		if(state_data->actions->NEXT!=NULL) {
			printf("Warning: Label '%s' targets a State with multiple Actions\n", name);
		}
		foreach(state_data->actions, scanAction) {
			hier_action_t* action = scanAction->DATA;
			action->flags |= flags_add;
			action->flags &= ~flags_rem;
			done_nb ++;
		}

	}  // Scan all Label nodes

	if(done_nb==0) {
		printf("Warning: No target Action was found.\n");
	}

	CLEAN:

	avl_p_reset(&tree_names);

	if(regexp_array != NULL) {
		for(unsigned i=0; i<regexp_nb; i++) { regfree(regexp_array + i); }
		free(regexp_array);
	}

	if(errors_nb != 0) return -1;
	return 0;
}

int Hier_Command(implem_t* Implem, command_t* cmd_data) {
	const char* cmd = Command_PopParam(cmd_data);
	if(cmd==NULL) {
		printf("Error 'hier' : No command received. Try 'help'.\n");
		return -1;
	}

	bool cmdnotdone = false;

	if(strcmp(cmd, "help")==0) {
		printf(
			"\n"
			"help                   Display this help.\n"
			"\n"
			"Setting parameters:\n"
			"  auto-asg-propag [<bool>]\n"
			"                         Enable assign propagation [init=yes, default=yes].\n"
			"  auto-asg-propag-notsimple [<bool>]\n"
			"                         Enable propagation of sign extension [init=no, default=yes].\n"
			"  auto-asg-propag-symuse [<bool>]\n"
			"                         Enable assign propagation based on Symyse flags [init=no, default=yes].\n"
			"  auto-asg-circular [<bool>]\n"
			"                         Enable removal of circular assignments [init=yes, default=yes].\n"
			"  auto-inline [<bool>]   Enable auto inline of functions [init=yes, default=yes].\n"
			"  auto-insert-tempregs-inbb [<bool>] \n"
			"                         Enable systematic insertion of temporary registers [init=no, default=yes].\n"
			"  actsimp-verbose [<bool>]\n"
			"                         Display details during simplification of Actions [init=no, default=yes].\n"
			"  nodesimp-verbose [<bool>]\n"
			"                         Display details during simplification of control nodes [init=no, default=yes].\n"
			"  en-overbig-loop-unroll [<bool>]\n"
			"                         Enable unroll of very big loops [init=no, default=yes].\n"
			"\n"
			"Setting flags:\n"
			"  symflag <+|-><flag> <sym1> [<sym2> ...]\n"
			"                         Add or remove a flag to all specified symbols.\n"
			"                         Available flags: nopropag, nodel\n"
			"  actflag-labels <+|-><flag1> [<label1>] [-i] [-<e|E> <pattern1>]\n"
			"                         Add or remove flags to Actions in the States that immediately follow Labels.\n"
			"                         Available flags: nopropag-from, nopropag-to\n"
			"                         Multiple flags can be specified.\n"
			"                         Multiple label names and regular expression patterns can be specified.\n"
			"                         -e means basic regular expression, -E means extended.\n"
			"                         -i means case insensitive for regexp patterns that follow.\n"
			"\n"
			"Available commands:\n"
			"  simp | simplify | upd | update\n"
			"                         Simplify the entire Hierarchy: Vex expressions, Actions, nodes, control flow...\n"
			"  upd-control            Detect Actions involved in the control flow of Loop and Switch nodes.\n"
			"  check-integrity        Check integrity and coherence of the entire Hier graph. Exit on failure.\n"
			"  time                   Commands related to execution time.\n"
			"    show | disp          Display the execution time of the design.\n"
			"    upd | update         Compute the execution time of the entire design.\n"
			"  disp-tree [options]    Print the entire Hier graph.\n"
			"    -noact               Don't print the Actions.\n"
			"    -nobbst              Don't print the Basic Block contents.\n"
			"    -time                Print the execution time of all nodes and Actions.\n"
			"    -debug               Show the pointers to all Hier elements.\n"
			"    -o <file>            Output in a file.\n"
			"  disp-details           [missing help]\n"  // FIXME
			"  clockcycles            Display a summary of the number of clock cycles of the states.\n"
			"  symuse-build           Analyze register life scope in the whole Hier graph.\n"
			"  symuse-disp            Display register life scope in the whole Hier graph.\n"
			"  cram [-keep-grouped] <label1> <label2>\n"
			"                         Cram in one state what is between the two labels.\n"
			"                         Optionally add inter-Action dependencies so they keep grouped.\n"
			"\n"
			"Inner command interpreters:\n"
			"  delay                  Exploration of the combinatorial delay of the states.\n"
			"  node-loop              Operations on Loop nodes.\n"
			"  node-switch            Operations on Switch nodes.\n"
			"  node-bb                Operations on Basic Block nodes.\n"
			"  node-state             Operations on State nodes.\n"
			"  node-label             Operations on Label nodes.\n"
			"\n"
		);
		return 0;
	}

	// Little local sub- command interpreters

	if(strcmp(cmd, "node-loop")==0) {
		return Hier_Loops_Command(Implem, cmd_data);
	}
	if(strcmp(cmd, "node-switch")==0) {
		return Hier_Switch_Command(Implem, cmd_data);
	}

	// Stuff that behaves like sub-command interpreters

	if(strcmp(cmd, "node-bb")==0) {
		// Search action
		const char* param = Command_PopParam(cmd_data);
		if(param==NULL) {
			printf("Error : Missing parameters. Try 'help'.\n");
			return -1;
		}

		if(strcmp(param, "help")==0) {
			printf(
				"Actions on basic blocks.\n"
				"  Usage : help             Display this help section.\n"
				"          merge            Merge consecutive basic blocks.\n"
				"                           Useful after a loop unroll of condition cabling.\n"
				"          clean            Remove empty States.\n"
			);
			return 0;
		}

		if(Implem->H==NULL) {
			printf("Error : The hierarchy is not built (or unknown command '%s').\n", param);
			return -1;
		}

		if(strcmp(param, "list")==0) {
			printf("There are %u basic blocks.\n", avl_p_count(&Implem->H->NODES[HIERARCHY_BB]));
			int index = 0;
			avl_p_foreach(&Implem->H->NODES[HIERARCHY_BB], scan) {
				hier_node_t* node = scan->data;
				printf("Basic block index %d :\n", index);
				LineList_Print_be(node->source_lines, "  Source lines : ", "\n");
				// Display the timings
				minmax_t t = Hier_Timing_GetLevelTime(node->NODE.BB.body);
				minmax_print(&t, "  Execution time         : ", " (clock cycles)\n");
				minmax_print(&node->timing.global, "  Execution time, global : ", " (clock cycles)\n");
				index++;
			}
		}
		else {
			printf("Error : Unknown parameter '%s'.\n", param);
			return -1;
		}
	}
	else if(strcmp(cmd, "node-state")==0) {
		// Search action
		const char* param = Command_PopParam(cmd_data);
		if(param==NULL) {
			printf("Error : Missing parameters for '%s'. Try 'help'.\n", cmd);
			return -1;
		}

		if(strcmp(param, "help")==0) {
			printf(
				"Actions on states (clock cycles).\n"
				"Possible parameters :\n"
				"  disp [options]\n"
				"Available options :\n"
				"    delay-min <value>   Set a minimum delay.\n"
				"    delay-max <value>   Set a maximum delay.\n"
				"    asg <name>          Only display assignments to symbol <name>.\n"
				"    max <value>         Limit the number of displayed states.\n"
			);
			return 0;
		}

		if(Implem->H==NULL) {
			printf("Error : The hierarchy is not built (or unknown command '%s').\n", param);
			return -1;
		}

		if(strcmp(param, "disp")==0) {
			// Get the parameters
			double delay_min = -1;
			double delay_max = -1;
			char* assign_to = NULL;
			int max = -1;
			do {
				const char* param_disp = Command_PopParam(cmd_data);
				if(param_disp==NULL) break;
				if(strcmp(param_disp, "delay-min")==0) {
					const char* param_disp = Command_PopParam(cmd_data);
					if(param_disp==NULL) {
						printf("Error : Missing parameter for '%s'.\n", param_disp);
						return -1;
					}
					delay_min = atof(param_disp);
				}
				else if(strcmp(param_disp, "delay-max")==0) {
					const char* param_disp = Command_PopParam(cmd_data);
					if(param_disp==NULL) {
						printf("Error : Missing parameter for '%s'.\n", param_disp);
						return -1;
					}
					delay_max = atof(param_disp);
				}
				else if(strcmp(param_disp, "asg")==0) {
					const char* param_disp = Command_PopParam(cmd_data);
					if(param_disp==NULL) {
						printf("Error : Missing parameter for '%s'.\n", param_disp);
						return -1;
					}
					assign_to = namealloc(param_disp);
				}
				else if(strcmp(param_disp, "nb")==0) {
					const char* param_disp = Command_PopParam(cmd_data);
					if(param_disp==NULL) {
						printf("Error : Missing parameter for '%s'.\n", param_disp);
						return -1;
					}
					max = atoi(param_disp);
				}
				else {
					printf("Error : Unknown parameter '%s' for command '%s'.\n", param_disp, param);
					return -1;
				}
			}while(1);
			// Here we have all the parameters.
			// Display all matching states.

			int count_trans = 0;
			int count_act = 0;
			avl_p_foreach(&Implem->H->NODES[HIERARCHY_STATE], scanNode) {
				hier_node_t* node = scanNode->data;
				hier_state_t* state_data = &node->NODE.STATE;
				if(state_data->actions==NULL) continue;
				if(delay_min>=0 && state_data->time.delay<delay_min) continue;
				if(delay_max>=0 && state_data->time.delay>delay_max) continue;
				// Display the State
				printf("New state (delay %g) :\n", state_data->time.delay);
				// Display all actions
				foreach(state_data->actions, scanAction) {
					hier_action_t* action = scanAction->DATA;
					if(assign_to!=NULL && hvex_asg_get_destname(action->expr)!=assign_to) continue;
					HierAct_Print(action);
					count_act++;
				}
				count_trans++;
				if(max>=0 && count_trans>=max) break;
			}  // Scan all the hierarchy nodes
			printf("%d states and %d actions were displayed.\n", count_trans, count_act);
		}
		else {
			printf("Error : Unknown parameter '%s' for command '%s'.\n", param, cmd);
			return -1;
		}
	}
	else if(strcmp(cmd, "node-label")==0) {
		// Search action
		const char* param = Command_PopParam(cmd_data);
		if(param==NULL) {
			printf("Error : Missing parameters for '%s'. Try 'help'.\n", cmd);
			return -1;
		}

		if(strcmp(param, "help")==0) {
			printf(
				"Actions on states (clock cycles).\n"
				"Possible parameters :\n"
				"  disp                  Display the label nodes.\n"
				"  rem-unused [<names>]  Remove all unused nodes.\n"
			);
			return 0;
		}

		if(Implem->H==NULL) {
			printf("Error : The hierarchy is not built (or unknown command '%s').\n", param);
			return -1;
		}

		if(strcmp(param, "disp")==0) {
			avl_p_foreach(&Implem->H->NODES[HIERARCHY_LABEL], scanNode) {
				hier_node_t* node = scanNode->data;
				hier_label_t* label_data = &node->NODE.LABEL;
				printf("Label: ");
				if(label_data->name==NULL) printf("(unnamed)");
				else printf("%s", label_data->name);
			}
		}
		else if(strcmp(param, "rem-unused")==0) {
			int names_nb = 0;
			do {
				const char* name = Command_PopParam_stralloc(cmd_data);
				if(name==NULL) break;
				Hier_Simp_RemoveUnusedLabels_Name(Implem, name);
				names_nb++;
			} while(1);
			if(names_nb==0) Hier_Simp_RemoveUnusedLabels(Implem);
		}
		else {
			printf("Error : Unknown parameter '%s' for command '%s'.\n", param, cmd);
			return -1;
		}
	}

	else if(strcmp(cmd, "delay")==0) {
		const char* param = Command_PopParam(cmd_data);
		if(param==NULL) {
			printf("Error : Missing parameters. Try 'help'.\n");
			return -1;
		}

		if(strcmp(param, "help")==0) {
			printf(
				"Exploration of the latency of the states.\n"
				"Possible parameters :\n"
				"  help               Display this help section.\n"
				"  worst [<nb>]       Display the <nb> states with the highest latency.\n"
				"                     If <nb> is omitted, display all states.\n"
				"  above <value>      Count the states that have a latency higher or equal to <value>.\n"
				"                     <value> is floating-point, in nanoseconds.\n"
			);
			return 0;
		}

		if(Implem->H==NULL) {
			printf("Error : The hierarchy is not built (or unknown command '%s').\n", param);
			return -1;
		}

		if(strcmp(param, "worst")==0) {
			int nb = -1;
			const char* param_nb = Command_PopParam(cmd_data);
			if(param_nb!=NULL) nb = atoi(param_nb);
			Hier_Latency_DisplayWorse(Implem, nb);
		}
		else if(strcmp(param, "above")==0) {
			const char* param_delay = Command_PopParam(cmd_data);
			if(param_delay==NULL) {
				printf("Error : Missing parameters for '%s'. Try 'help'.\n", param);
				return -1;
			}
			double delay = atof(param_delay);
			unsigned nb = Hier_Latency_CountAbove(Implem, delay);
			printf("%d states with latency above %g were found.\n", nb, delay);
		}
		else {
			printf("Error 'hier' : Unknown parameter '%s'.\n", param);
			return -1;
		}
	}

	// Setting parameters

	else if(strcmp(cmd, "auto-asg-propag")==0) {
		int z = Command_PopParam_YesNoDef_Verbose(cmd_data, true, cmd);
		if(z<0) return -1;
		asgpropag_en = z;
		return 0;
	}
	else if(strcmp(cmd, "auto-asg-propag-notsimple")==0) {
		int z = Command_PopParam_YesNoDef_Verbose(cmd_data, true, cmd);
		if(z<0) return -1;
		asgpropag_notsimple_en = z;
		return 0;
	}
	else if(strcmp(cmd, "auto-asg-propag-symuse")==0) {
		int z = Command_PopParam_YesNoDef_Verbose(cmd_data, true, cmd);
		if(z<0) return -1;
		asgpropag_symuse_en = z;
		return 0;
	}
	else if(strcmp(cmd, "auto-asg-circular")==0) {
		int z = Command_PopParam_YesNoDef_Verbose(cmd_data, true, cmd);
		if(z<0) return -1;
		circularuse_en = z;
		return 0;
	}
	else if(strcmp(cmd, "auto-inline")==0) {
		int z = Command_PopParam_YesNoDef_Verbose(cmd_data, true, cmd);
		if(z<0) return -1;
		autoinline_en = z;
		return 0;
	}
	else if(strcmp(cmd, "auto-insert-tempregs-inbb")==0) {
		int z = Command_PopParam_YesNoDef_Verbose(cmd_data, true, cmd);
		if(z<0) return -1;
		inserttempregs_inbb_en = z;
		return 0;
	}
	else if(strcmp(cmd, "actsimp-verbose")==0) {
		int z = Command_PopParam_YesNoDef_Verbose(cmd_data, true, cmd);
		if(z<0) return -1;
		actsimp_verbose = z;
		return 0;
	}
	else if(strcmp(cmd, "nodesimp-verbose")==0) {
		int z = Command_PopParam_YesNoDef_Verbose(cmd_data, true, cmd);
		if(z<0) return -1;
		nodesimp_verbose = z;
		return 0;
	}
	else if(strcmp(cmd, "en-overbig-loop-unroll")==0) {
		int z = Command_PopParam_YesNoDef_Verbose(cmd_data, true, cmd);
		if(z<0) return -1;
		overbigloop_unroll_en = z;
		return 0;
	}

	// Setting flags

	else if(strcmp(cmd, "symflag")==0) {
		char const * param = Command_PopParam(cmd_data);

		if(param==NULL) {
			printf("Error : Missing parameters.\n");
			return -1;
		}

		bool do_add = false;
		unsigned flag = 0;

		// Get what to do: remove or add
		if (param[0]=='-') do_add = false;
		else if(param[0]=='+') do_add = true;
		else {
			printf("Error : Missing +/- flag prefix.\n");
			return -1;
		}
		param++;

		// Get the flag
		if(strcmp(param, "nopropag")==0) {
			flag = SYM_PROTECT_NOPROPAG;
		}
		else if(strcmp(param, "nodel")==0) {
			flag = SYM_PROTECT_NODEL;
		}
		else {
			printf("Error : Unknown flag '%s'.\n", param);
			return -1;
		}

		unsigned done_nb = 0;

		// Parse the list of symbol names
		do {
			const char* name = Command_PopParam(cmd_data);
			if(name==NULL) break;
			// Set/clear the flag
			if(do_add==true) Implem_SymFlag_Add(Implem, name, flag);
			else Implem_SymFlag_Rem(Implem, name, flag);
			done_nb ++;
		} while(1);

		if(done_nb==0) {
			printf("Error: No target symbol was specified\n");
			return -1;
		}

		return 0;
	}

	else {
		cmdnotdone = true;
	}

	if(cmdnotdone==false) return 0;

	// The other commands require that the hierarchy exists

	if(Implem->H==NULL) {
		printf("Warning 'hier': Skipping command '%s' on design with no instructions.\n", cmd);
		return 0;
	}

	if(strcmp(cmd, "actflag-labels")==0) {
		return Hier_Cmd_ActFlagLabels(Implem, cmd_data);
	}

	else if(strcmp(cmd, "clockcycles")==0) {
		Hier_ClockCycles_Display(Implem);
	}
	else if(strcmp(cmd, "time")==0) {
		const char* cmd_time = Command_PopParam(cmd_data);
		if(cmd_time==NULL) {
			printf("Error : Missing parameters.\n");
			return -1;
		}
		if(strcmp(cmd_time, "show")==0 || strcmp(cmd_time, "disp")==0) {
			Hier_Timing_Show(Implem);
		}
		else if(strcmp(cmd_time, "upd")==0 || strcmp(cmd_time, "update")==0) {
			Hier_Timing_Update(Implem, false);
		}
		else {
			printf("Error : Unknown command '%s'.\n", cmd_time);
			return -1;
		}
	}

	else if(strcmp(cmd, "simp")==0 || strcmp(cmd, "simplify")==0 || strcmp(cmd, "upd")==0 || strcmp(cmd, "update")==0) {
		Hier_Update_All(Implem);
	}
	else if(strcmp(cmd, "upd-control")==0) {
		Hier_Update_Control(Implem);
	}
	else if(strcmp(cmd, "check-integrity")==0) {
		Hier_Check_Integrity(Implem->H);
	}

	else if(strcmp(cmd, "disp-tree")==0) {
		int flags = 0;
		char* filename = NULL;
		// Read parameters
		do {
			const char* param = Command_PopParam(cmd_data);
			if(param==NULL) break;
			if     (strcmp(param, "-noact")==0)  flags |= HIER_PRINT_NOACT;
			else if(strcmp(param, "-nobbst")==0) flags |= HIER_PRINT_NOBBBODY;
			else if(strcmp(param, "-time")==0)   flags |= HIER_PRINT_TIME;
			else if(strcmp(param, "-debug")==0)  flags |= HIER_PRINT_DEBUG;
			else if(strcmp(param, "-o")==0) {
				filename = Command_PopParam_stralloc(cmd_data);
				if(filename==NULL) {
					printf("Error 'hier' : Command '%s' : Missing file name after parameter '%s'. Try 'help'.\n", cmd, param);
					return -1;
				}
			}
			else {
				printf("Error 'hier' : Unknown parameter '%s' for command '%s'. Try 'help'.\n", param, cmd);
				return -1;
			}
		} while(1);
		// Open the output file if asked to
		FILE* F = stdout;
		if(filename!=NULL) {
			F = fopen(filename, "wb");
			if(F==NULL) {
				printf("Error 'hier' : Command '%s' : Can't open file '%s'. Try 'help'.\n", cmd, filename);
				return -1;
			}
		}
		// Launch
		Hier_Print_raw(F, Implem->H, flags);
		// Close the file
		if(filename!=NULL) fclose(F);
	}

	else if(strcmp(cmd, "disp-details")==0) {
		Hier_PrintDetails(Implem->H);
	}
	else if(strcmp(cmd, "disp-tree-dbg")==0) {
		Hier_Print_dbg(Implem->H);
	}

	else if(strcmp(cmd, "symuse-build")==0) {
		Hier_SymUse_FillHier(Implem);
	}
	else if(strcmp(cmd, "symuse-disp")==0) {
		Hier_SymUse_Print_Implem(Implem);
	}

	else if(strcmp(cmd, "cram")==0) {
		char const * label1_name = NULL;
		char const * label2_name = NULL;
		const char* param_opt = Command_PopParam(cmd_data);
		int flags = 0;

		// Get optional parameter -keep-grouped
		if(strcmp(param_opt, "-keep-grouped")==0) {
			flags |= HIER_CRAM_KEEP_GROUPED;
		}
		else label1_name = param_opt;

		if(label1_name==NULL) label1_name = Command_PopParam(cmd_data);
		label2_name = Command_PopParam(cmd_data);
		if(label2_name==NULL) {
			printf("Error: Missing parameters for command '%s'. Try 'help'.\n", cmd);
			return -1;
		}

		label1_name = stralloc(label1_name);
		label2_name = stralloc(label2_name);

		// Find the 2 label nodes, ensure they are unique
		hier_node_t* label1_node = Hier_GetLabelUnique_Verbose(Implem->H, label1_name);
		hier_node_t* label2_node = Hier_GetLabelUnique_Verbose(Implem->H, label2_name);
		if(label1_node==NULL || label2_node==NULL) return -1;

		// Ensure the second label is in the same Level, and after, the label 1
		bool found = false;
		for(hier_node_t* node=label1_node; node!=NULL; node=node->NEXT) {
			if(node==label2_node) { found = true; break; }
		}
		if(found==false) {
			printf("Error: Label '%s' is not after label '%s', or not in the same level.\n", label2_name, label1_name);
			return -1;
		}

		// Ensure there are nodes between the labels
		if(label1_node->NEXT==label2_node) {
			printf("Warning: There is nothing to cram between labels '%s' and '%s'.\n", label1_name, label2_name);
			return 0;
		}

		// Launch the big cramming!
		int z = Hier_Cram_Level2BB(Implem, label1_node->NEXT, label2_node, flags);
		if(z!=0) return -1;

		// Needed in case BB cramming messed up with Actions
		Hier_Update_Control(Implem);

		// Update the needed operators
		Map_Netlist_CompleteComponents(Implem);

		return 0;
	}

	else if(strcmp(cmd, "merge")==0) {
		// Get the list of Labels
		// Only one Label of each name must be found
		chain_list* list_labels = NULL;
		int flags = 0;
		do {
			const char* name = Command_PopParam_stralloc(cmd_data);
			if(name==NULL) break;
			// Get the Label node
			hier_node_t* nodeLabel = NULL;
			avl_p_foreach(&Implem->H->NODES[HIERARCHY_LABEL], scanLabel) {
				hier_node_t* loc_label = scanLabel->data;
				hier_label_t* label_data = &loc_label->NODE.LABEL;
				if(label_data->name!=name) continue;
				if(nodeLabel!=NULL) {
					printf("Error: Several Label nodes with name '%s' are found. Only one is possible.\n", name);
					freechain(list_labels);
					return -1;
				}
				nodeLabel = loc_label;
			}
			if(nodeLabel==NULL) {
				printf("Error: No Label node with name '%s' was found.\n", name);
				freechain(list_labels);
				return -1;
			}
			list_labels = addchain(list_labels, nodeLabel);
		} while(1);
		// Put Label names in the correct order
		list_labels = reverse(list_labels);
		// Sanity check
		if(ChainList_Count(list_labels) < 3) {
			printf("Error: Not enough Label names given for command '%s'. At least 3 are needed.\n", cmd);
			freechain(list_labels);
			return -1;
		}
		// Perform the modification
		int z = Hier_Merge_Replace(Implem, list_labels, flags);
		freechain(list_labels);
		if(z!=0) {
			printf("Error: Merging failed (code %i).\n", z);
			return -1;
		}
		return z;
	}

	#ifndef NDEBUG  // Commands for debug purposes only

	else if(strcmp(cmd, "disp-tree-dbg")==0) {
		Hier_Print_dbg(Implem->H);
	}
	// This is only for the symbols to exist after compilation/link, so they can be called from debugger
	else if(strcmp(cmd, "disp-node-dbg")==0) {
		Hier_PrintNode_dbg(Implem->H->PROCESS);
	}
	else if(strcmp(cmd, "disp-level-dbg")==0) {
		Hier_PrintLevel_dbg(Implem->H->PROCESS);
	}

	#endif

	else {
		printf("Error 'hier' : Unknown command '%s'.\n", cmd);
		return -1;
	}

	return 0;
}


