
// Contents of this file: Transform a part of the Hier graph so all fits into one State

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <assert.h>
#include <inttypes.h>

#include "auto.h"
#include "hierarchy.h"
#include "hierarchy_symuse.h"
#include "schedule.h"
#include "techno.h"
#include "map.h"
#include "../hvex/hvex_misc.h"



/* Cramming BB: algorithm

Problem of WaW for register
Example:

	State 1:
		x = a + c;
		a = stuffA;
		c = a + stuffC;
	State 2:
		y = -x;

If we replace x, then c, then a:
a will be replaced also in stuff that replaces c, which is WRONG.

For this reason, we must find all symbols to replace in State 2,
and pre-compute their replacement but without actually performing the replacement.
Once all replacements for State2 are built, apply them all.

As a next step, scan all Actions in State 1, and copy them in State 2,
but in case of WaW, keep only the bits not overwritten.
To do so efficiently, first extend the Actions to the max indexes between State 1 and State 2.

*/

// Make the BB a one-state BB: cram all Actions in one State
int Hier_Cram_BB(implem_t* Implem, hier_node_t* node, int flags) {
	assert(node->TYPE==HIERARCHY_BB);
	hier_bb_t* bb_data = &node->NODE.BB;
	if(bb_data->body==NULL) return 0;

	// Reject when there are non-zero timed dependencies in the BB
	foreach(bb_data->body, nodeState) {
		hier_state_t* state_data = &nodeState->NODE.STATE;
		foreach(state_data->actions, scanAct) {
			hier_action_t* action = scanAct->DATA;
			avl_pi_foreach(&action->deps_timed, scanDep) {
				if(scanDep->data!=0) {
					printf("Error: Actions with non-zero timed dependencies are not allowed. Can't cram BB.\n");
					return -1;
				}
			}
		}
	}

	while(bb_data->body->NEXT!=NULL) {
		hier_node_t* curState = bb_data->body;
		hier_node_t* nextState = bb_data->body->NEXT;
		hier_state_t* cur_data = &curState->NODE.STATE;
		hier_state_t* next_data = &nextState->NODE.STATE;

		// Check Actions in curState
		foreach(cur_data->actions, scanAct) {
			hier_action_t* action = scanAct->DATA;

			// Get the destination VEX and name
			hvex_t* vex_dest = hvex_asg_get_dest(action->expr);
			assert(vex_dest!=NULL);
			char* name = hvex_vec_getname(vex_dest);
			assert(name!=NULL);

			// Reject wired conditions
			hvex_t* vex_cond = hvex_asg_get_cond(action->expr);
			if(vex_cond!=NULL) {
				printf("Error: Unhandled Action with wired condition (writes to '%s'). Can't cram BB.\n", name);
				return -1;
			}

			// If the symbol is a component, only accept SIG and REG. Ports are always OK.
			// Also, extend nextState Actions when WaW on REG
			netlist_comp_t* comp = Netlist_Comp_GetChild(Implem->netlist.top, name);
			if(comp!=NULL) {
				if(comp->model!=NETLIST_COMP_SIGNAL && comp->model!=NETLIST_COMP_REGISTER) {
					printf("Error: Unhandled write Action to comp '%s' model '%s'. Can't cram BB.\n", comp->name, comp->model->name);
					return -1;
				}
				if(comp->model==NETLIST_COMP_REGISTER) {
					// Check in nextState in case there is WaW, extend the ASG
					foreach(next_data->actions, scanNextAct) {
						hier_action_t* nextAction = scanNextAct->DATA;
						hvex_t* next_dest = hvex_asg_get_dest(nextAction->expr);
						assert(next_dest!=NULL);
						char* locname = hvex_vec_getname(next_dest);
						if(locname!=name) continue;
						// Extend the assignment to include the indexes of the curState
						// Find the leftmost and rightmost indexes of the assignments
						unsigned leftmax = vex_dest->left;
						unsigned rightmin = vex_dest->right;
						leftmax = GetMax(leftmax, next_dest->left);
						rightmin = GetMin(rightmin, next_dest->right);
						// Build a NULL ASG with the max indexes: dest = dest
						hvex_t* new_expr = hvex_newvec(name, leftmax, rightmin);
						hvex_ReplaceSubExpr_FromPrevExpr_All(new_expr, next_dest, hvex_asg_get_expr(nextAction->expr));
						// Replace the expression
						hvex_free(nextAction->expr);
						nextAction->expr = hvex_asg_make(
							hvex_newvec(name, leftmax, rightmin), NULL, new_expr, NULL
						);
					}  // Scan nextState Actions
				}  // Comp is REG
			}  // Dest is a component

		}  // Check all Actions in curState

		// Take all Actions in curState, incorporate them in nextState
		// FIXME All timed dependencies will be removed

		// The list of Actions to link into nextState after all curState actions are scanned
		chain_list* list_new_act = NULL;
		// The tree of Expr in nextState, and their replacement
		avl_pp_tree_t tree_newexpr;
		avl_pp_init(&tree_newexpr);

		foreach(cur_data->actions, scanAct) {
			hier_action_t* action = scanAct->DATA;

			hvex_t* vex_dest = hvex_asg_get_dest(action->expr);
			char* name = hvex_vec_getname(vex_dest);

			hvex_t* vex_expr = hvex_asg_get_expr(action->expr);

			// Handle the case of registers separately
			netlist_comp_t* comp = Netlist_Comp_GetChild(Implem->netlist.top, name);
			if(comp!=NULL && comp->model==NETLIST_COMP_REGISTER) {
				bool keep_act = true;
				foreach(next_data->actions, scanNextAct) {
					hier_action_t* nextAction = scanNextAct->DATA;

					// Find occurrences of the symbol
					chain_list* listOccur = NULL;
					listOccur = hvex_SearchSym_AddList(listOccur, hvex_asg_get_expr(nextAction->expr), name);
					listOccur = hvex_SearchSym_AddList(listOccur, hvex_asg_get_addr(nextAction->expr), name);
					listOccur = hvex_SearchSym_AddList(listOccur, hvex_asg_get_cond(nextAction->expr), name);

					// Check flags NOPROPAG
					if(listOccur!=NULL) {
						// Reject when the source Action has the flag NOPROPAG_FROM
						if((action->flags & HIER_ACT_NOPROPAG_FROM) != 0) {
							printf("Error: Unhandled Action with flag NOPROPAG_FROM (writes to '%s'). Can't cram BB.\n", name);
							return -1;
						}
						// Reject when the destination Action has the flag NOPROPAG_TO
						if((nextAction->flags & HIER_ACT_NOPROPAG_TO) != 0) {
							printf("Error: Unhandled Action with flag NOPROPAG_TO (writes to '%s'). Can't cram BB.\n", name);
							return -1;
						}
					}

					// Prepare a replacement Vex for each occurrence
					foreach(listOccur, scanOccur) {
						hvex_t* Expr = scanOccur->DATA;
						hvex_t* replacement = hvex_ReplaceSubExpr_FromPrevExpr_dup(Expr, vex_dest, vex_expr);
						assert(avl_pp_isthere(&tree_newexpr, Expr)==false);
						avl_pp_add_overwrite(&tree_newexpr, Expr, replacement);
					}
					freechain(listOccur);

					// Check the destination
					// If it is the same symbol, the curState Action will not be kept
					hvex_t* next_dest = hvex_asg_get_dest(nextAction->expr);
					assert(next_dest!=NULL);
					char* locname = hvex_vec_getname(next_dest);
					if(locname==name) keep_act = false;
				}  // Scan next Saction

				// Add to the list of actions to later link into nextState
				if(keep_act==true) list_new_act = addchain(list_new_act, action);
				// This action is now fully processed
				continue;
			}  // Case where the destination is a register

			// Here the dest symbol is not a Register
			// So it is either a top-level port, or a Signal

			// If it is written in nextState, tolerate only if the two Actions are identical
			bool keepaction = true;
			foreach(next_data->actions, scanNextAct) {
				hier_action_t* nextAction = scanNextAct->DATA;
				hvex_t* next_dest = hvex_asg_get_dest(nextAction->expr);
				assert(next_dest!=NULL);
				char* locname = hvex_vec_getname(next_dest);
				if(locname!=name) continue;
				// Compare the Actions
				int z = hvex_cmp(action->expr, nextAction->expr);
				if(z!=0) {
					printf("Error: Unhandled non-identical WaW Actions on non-register symbol '%s'. Can't cram BB.\n", name);
					goto ERRFREE;
				}
				// This action will not be kept, we already have a copy in nextState
				keepaction = false;
			}
			if(keepaction==true) list_new_act = addchain(list_new_act, action);

		}  // Process all Actions of curState

		// Replace the Expr
		avl_pp_foreach(&tree_newexpr, scan) {
			hvex_t* Expr = scan->key;
			hvex_clearinside(Expr);
			hvex_replace_free(Expr, scan->data);
		}
		avl_pp_reset(&tree_newexpr);

		// Add the new Actions into nextState
		foreach(list_new_act, scan) {
			hier_action_t* action = scan->DATA;
			Hier_Action_Unlink(action);
			Hier_Action_Link(nextState, action);
		}
		freechain(list_new_act);

		// Then remove curState
		Hier_ReplaceNode_Free(Implem->H, curState, NULL);

		continue;

		ERRFREE:

		foreach(list_new_act, scan) Hier_Action_FreeFull(scan->DATA);
		freechain(list_new_act);
		avl_pp_foreach(&tree_newexpr, scan) hvex_free(scan->data);
		avl_pp_reset(&tree_newexpr);

		return -1;

	}  // Loop while the BB has more than one State

	if( (flags & HIER_CRAM_KEEP_GROUPED) != 0 ) {
		hier_node_t* curState = bb_data->body;
		hier_state_t* state_data = &curState->NODE.STATE;
		foreach(state_data->actions, scanAct1) {
			hier_action_t* action1 = scanAct1->DATA;
			foreach(scanAct1->NEXT, scanAct2) {
				hier_action_t* action2 = scanAct2->DATA;
				avl_pi_add_overwrite(&action1->deps_timed, action2, 0);
				avl_pi_add_overwrite(&action2->deps_timed, action1, 0);
			}
		}
	}

	return 0;
}

// Process a node towards all BB. May create new nodes.
// If pNextNode!=NULL, it is set to the first created node or the next node to process
// Return 0 if OK
static int Hier_Cram_ProcessNode(implem_t* Implem, hier_node_t* node, hier_node_t** pNextNode, int flags) {
	hier_node_t* nextNode = node->NEXT;

	#if 0  // Display information for debug
	dbgprintf("Entering func %s with node type %s\n", __func__, Hier_NodeType2str(node->TYPE));
	Hier_Check_Integrity(Implem->H);
	#endif

	switch(node->TYPE) {

		// FIXMEEE missing addLists

		// Stray State out of a BB?
		case HIERARCHY_STATE: {
			nextNode = Hier_Node_NewType(HIERARCHY_BB);
			hier_bb_t* bb_data = &nextNode->NODE.BB;
			bb_data->body = Hier_Node_Dup_State(node);
			bb_data->body->PARENT = nextNode;
			nextNode->CHILDREN = addchain(nextNode->CHILDREN, bb_data->body);
			Hier_ReplaceNode_Free(Implem->H, node, nextNode);
			break;
		}

		case HIERARCHY_BB: {
			hier_bb_t* bb_data = &node->NODE.BB;
			if(bb_data->body==NULL) {
				Hier_ReplaceNode_Free(Implem->H, node, NULL);
			}
			else nextNode = node;
			break;
		}

		case HIERARCHY_LABEL: {
			hier_label_t* label_data = &node->NODE.LABEL;
			if(label_data->jumps!=NULL) {
				printf("Error: Can't cram a Label node that has dubious jumps leading to it.\n");
				return __LINE__;
			}
			Hier_ReplaceNode_Free(Implem->H, node, NULL);
			break;
		}

		case HIERARCHY_JUMP: {
			printf("Error: Jump nodes can't be crammed.\n");
			return __LINE__;
			break;
		}

		case HIERARCHY_LOOP: {
			// Cram all bodies
			foreach(node->CHILDREN, scan) {
				hier_node_t* child = scan->DATA;
				int z = Hier_Cram_Level2BB(Implem, child, NULL, flags);
				if(z!=0) return z;
			}
			// Needed in case BB cramming messed up with Actions
			Hier_Loop_UpdControl(Implem, node);
			// Try to fully unroll the loop
			nextNode = Hier_For_GetFullUnroll(Implem->H, node, 0);
			if(nextNode==NULL) {
				printf("Error: Can't fully unroll a loop. Cram is not possible.\n");
				return __LINE__;
			}
			// Replace the node by the unrolled body
			Hier_ReplaceNode_Free(Implem->H, node, nextNode);
			break;
		}

		case HIERARCHY_SWITCH: {
			// Needed in case unused SIG ASG remains
			Hier_RemoveUnusedSym_OnePass(Implem);
			// Cram all bodies
			foreach(node->CHILDREN, scan) {
				hier_node_t* nodeCase = scan->DATA;
				hier_case_t* case_data = &nodeCase->NODE.CASE;
				if(case_data->body!=NULL) {
					int z = Hier_Cram_Level2BB(Implem, case_data->body, NULL, flags);
					if(z!=0) return z;
				}
			}
			// Another simplification to help the wiring process
			Hier_RemoveUnusedSym_OnePass(Implem);
			Hier_Switch_UpdControl(Implem, node);
			// Try to wire the switch
			nextNode = Hier_Switch_GetWired_OneCycle(Implem, node);
			if(nextNode==NULL) {
				printf("Error: Can't wire a Switch. Cram is not possible.\n");
				return __LINE__;
			}
			// Replace the node by the wired body
			Hier_ReplaceNode_Free(Implem->H, node, nextNode);
			break;
		}

		default: abort();
	}

	if(pNextNode!=NULL) *pNextNode = nextNode;
	return 0;
}

// Cram in-place
// If stopNode!=NULL, stop there, don't touch stopNode
// return 0 if OK
int Hier_Cram_Level2BB(implem_t* Implem, hier_node_t* begNode, hier_node_t* stopNode, int flags) {
	while(begNode->TYPE!=HIERARCHY_BB) {
		hier_node_t* crammed = NULL;
		int z = Hier_Cram_ProcessNode(Implem, begNode, &crammed, flags);
		if(z!=0) return z;
		if(crammed==NULL || crammed==stopNode) return 0;
		begNode = crammed;
	}

	// Here begNode is a BB, and it is not empty
	// Continue cramming what's next and merge into begNode

	do {
		hier_node_t* nextNode = begNode->NEXT;
		if(nextNode==NULL || nextNode==stopNode) break;
		if(nextNode->TYPE==HIERARCHY_BB) {
			// Merge into begBB
			hier_bb_t* begbb_data = &begNode->NODE.BB;
			hier_bb_t* nextbb_data = &nextNode->NODE.BB;
			hier_node_t* body_next = nextbb_data->body;
			if(body_next!=NULL) {
				// Extract the body of the next node
				Hier_ReplaceChild(nextNode, body_next, NULL);
				// Concat the two bodies
				Hier_InsertLevelAfterLevel(begbb_data->body, body_next);
			}
			// Remove the next node
			Hier_ReplaceNode_Free(Implem->H, nextNode, NULL);
			continue;
		}
		// Cram the next node
		hier_node_t* crammed = NULL;
		int z = Hier_Cram_ProcessNode(Implem, nextNode, &crammed, flags);
		if(z!=0) return z;
		nextNode = crammed;
	} while(1);

	// Here we have to guarantee that begNode is a one-state BB
	// Cram all state Actions in one State
	return Hier_Cram_BB(Implem, begNode, flags);
}


