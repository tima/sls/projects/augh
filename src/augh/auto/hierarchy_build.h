
#ifndef _HIERARCHY_BUILD_H_
#define _HIERARCHY_BUILD_H_

#include "auto.h"


//======================================================================
// Declarations of data structures
//======================================================================

// This data structure makes it possible to track which top-level uses which symbol
typedef struct build_symtops_data_t {
	// The original Implem
	implem_t* Implem;

	// The new top-level Implems. Key = func name, data = modImplem
	avl_pp_tree_t tree_toplevels;
	// The tree that links the new Implems to their component instantiation
	avl_pp_tree_t tree_imp2comp;

	// For replacement of built-in functions:
	// A tree that lists all symbols that must be avoided when creating new ports/accesses for top-levels
	avl_p_tree_t tree_avoidsym;

	// Key = symbol, data = avl_pp_tree_t* (whose key is name and data the chain_list* of occurrences)
	avl_pp_tree_t tree_syms_orig;  // Symbols, original usage
	avl_pp_tree_t tree_syms_acc;   // Symbols here correspond to ports or accesses
	avl_pp_tree_t tree_syms_done;  // Symbols already processed
} build_symtops_data_t;

typedef enum builtinfunc_type_t {
	HIER_BUILTIN_NONE = 0,  // Replaced last
	HIER_BUILTIN_IF   = 1,
	HIER_BUILTIN_BUF  = 2,
	HIER_BUILTIN_TRANSFER = 3,
} builtinfunc_type_t;

// To declare callback functions
typedef struct builtinfunc_t  builtinfunc_t;
struct builtinfunc_t {
	char* func_name;

	// How the function calls should be found
	bool call_is_act;     // The func call must not even return something
	bool alone_in_state;  // There must be no other Action in the State

	// The type: defines the order in which builtin function calls are replaced
	builtinfunc_type_t type;

	// The callback function
	// It performs VEX expression replacement and netlist modification if needed
	// In case there are several top-levels, modImplem and compInst indicate the instantiation component
	int (*func_vex)(builtinfunc_t* builtin, build_symtops_data_t* data, implem_t* modImplem, netlist_comp_t* compInst, hvex_t* Expr);

	// Reference to the plugin that owns this component model (NULL if AUGH)
	void* plugin;
	// For generic replacement callback, may need some descriptive data
	void* data;
};


//======================================================================
// Declarations of functions
//======================================================================

builtinfunc_t* Hier_BuiltinFunc_New();
void Hier_BuiltinFunc_Free(builtinfunc_t* builtin);
builtinfunc_t* Hier_BuiltinFunc_Get(char* name);
bool Hier_BuiltinFunc_Add(builtinfunc_t* builtin);
void Hier_BuiltinFunc_Remove(builtinfunc_t* builtin);


#endif  // _HIERARCHY_BUILD_H_
