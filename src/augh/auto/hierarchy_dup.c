
// Contents of this file: duplication of Hier nodes or the entier Hier graph

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <ctype.h>
#include <assert.h>

#include "auto.h"
#include "hierarchy.h"
#include "linelist.h"



hier_node_t* Hier_Level_Dup(hier_node_t* node);

hier_action_t* Hier_Action_Dup(hier_action_t* action) {
	hier_action_t* newact = Hier_Action_New();
	newact->expr = hvex_dup(action->expr);
	newact->source_lines = LineList_Dup(action->source_lines);
	newact->flags = action->flags;
	newact->delay = action->delay;
	newact->nb_clk = action->nb_clk;
	newact->map_delay = action->map_delay;
	return newact;
}

hier_node_t* Hier_Node_Dup_State(hier_node_t* node) {
	hier_node_t* newnode = Hier_Node_NewType(node->TYPE);
	hier_state_t* node_pt = &node->NODE.STATE;
	hier_state_t* newnode_pt = &newnode->NODE.STATE;

	// Keep correspondence for timed dependencies
	avl_pp_tree_t old2new;
	avl_pp_init(&old2new);

	void* local_old2new(void* oldptr) {
		void* newptr = NULL;
		avl_pp_find_data(&old2new, oldptr, &newptr);
		return newptr;
	}

	// Duplicate Actions
	foreach(node_pt->actions, scanAction) {
		hier_action_t* action = scanAction->DATA;
		hier_action_t* newaction = Hier_Action_Dup(action);
		Hier_Action_Link(newnode, newaction);
		avl_pp_add_overwrite(&old2new, action, newaction);
	}

	// Duplicate inter-Actions stuff
	foreach(node_pt->actions, scanAction) {
		hier_action_t* action = scanAction->DATA;
		hier_action_t* newaction = local_old2new(action);
		// Timed dependencies
		avl_pi_foreach(&action->deps_timed, scandep) {
			hier_action_t* other_action = scandep->key;
			hier_action_t* newother_action = local_old2new(other_action);
			int time_val = scandep->data;
			if(newother_action==NULL) {
				assert(time_val!=0);
				printf("WARNING: Skipping timed dependencies between Actions of different States.\n");
				continue;
			}
			avl_pi_add_overwrite(&newaction->deps_timed, newother_action, time_val);
		}
		// Inline Actions
		foreach(action->acts_inline, scanInl) {
			hier_action_t* other_action = scanInl->DATA;
			hier_action_t* newother_action = local_old2new(other_action);
			assert(newother_action!=NULL);
			newaction->acts_inline = addchain(newaction->acts_inline, newother_action);
		}
	}

	avl_pp_reset(&old2new);

	newnode_pt->time.delay  = node_pt->time.delay;
	newnode_pt->time.nb_clk = node_pt->time.nb_clk;
	newnode_pt->time.map_delay = node_pt->time.map_delay;
	newnode_pt->time.map_nb_clk = node_pt->time.map_nb_clk;

	newnode->source_lines = LineList_Dup(node->source_lines);

	return newnode;
}

static hier_node_t* Hier_Node_Dup_BB(hier_node_t* node) {
	hier_node_t* newnode = Hier_Node_NewType(node->TYPE);
	hier_bb_t* node_bb = &node->NODE.BB;
	hier_bb_t* newnode_bb = &newnode->NODE.BB;

	if(node_bb->body!=NULL) {
		newnode_bb->body = Hier_Level_Dup(node_bb->body);
		newnode_bb->body->PARENT = newnode;
		newnode->CHILDREN = addchain(newnode->CHILDREN, newnode_bb->body);
	}

	return newnode;
}

// WARNING: The pointer to the target node is kept as is, the calling process is responsible for replacement
static hier_node_t* Hier_Node_Dup_Jump(hier_node_t* node) {
	hier_node_t* newnode = Hier_Node_NewType(node->TYPE);
	hier_jump_t* node_jump = &node->NODE.JUMP;
	hier_jump_t* newnode_jump = &newnode->NODE.JUMP;

	newnode_jump->type   = node_jump->type;
	newnode_jump->target = node_jump->target;

	return newnode;
}

static hier_node_t* Hier_Node_Dup_Label(hier_node_t* node) {
	hier_node_t* newnode = Hier_Node_NewType(node->TYPE);
	hier_label_t* label_data = &node->NODE.LABEL;
	hier_label_t* newlabel_data = &newnode->NODE.LABEL;

	newlabel_data->name = label_data->name;

	// Note: the list of jump nodes that lead here is not duplicated
	// It may be preferrable to handle this from the JUMP nodes

	return newnode;
}

// WARNING: The pointers .build in Case are kept as is, the calling process is responsible for replacement
static hier_node_t* Hier_Node_Dup_Case(hier_node_t* node) {
	hier_node_t* newnode = Hier_Node_NewType(node->TYPE);
	hier_case_t* case_data = &node->NODE.CASE;
	hier_case_t* newcase_data = &newnode->NODE.CASE;

	newcase_data->build = case_data->build;

	if(case_data->body!=NULL) {
		newcase_data->body = Hier_Level_Dup(case_data->body);
		newcase_data->body->PARENT = newnode;
		newnode->CHILDREN = ChainList_Add_NoNull(newnode->CHILDREN, newcase_data->body);
	}

	// Duplicate the test info
	newcase_data->is_default = case_data->is_default;
	avl_p_foreach(&case_data->values, scanval) {
		hier_testinfo_t* testinfo = scanval->data;
		hier_testinfo_t* newtestinfo = Hier_TestInfo_New();
		newtestinfo->cond = hvex_dup(testinfo->cond);
		avl_p_add_overwrite(&newcase_data->values, newtestinfo, newtestinfo);
	}

	// The user annotations
	newcase_data->user = case_data->user;

	return newnode;
}
// This function cleanly updates inter-Case links
static hier_node_t* Hier_Node_Dup_Switch(hier_node_t* node) {
	hier_node_t* newnode = Hier_Node_NewType(node->TYPE);
	hier_switch_t* switch_data = &node->NODE.SWITCH;
	hier_switch_t* newswitch_data = &newnode->NODE.SWITCH;

	// Keep correspondence for inter-Case links
	avl_pp_tree_t old2new;
	avl_pp_init(&old2new);

	void* local_old2new(void* oldptr) {
		void* newptr = NULL;
		avl_pp_find_data(&old2new, oldptr, &newptr);
		return newptr;
	}

	// Duplicate the child nodes
	foreach(node->CHILDREN, scanChild) {
		hier_node_t* nodeChild = scanChild->DATA;
		hier_node_t* newChild = Hier_Node_Dup_Case(nodeChild);
		newChild->PARENT = newnode;
		newnode->CHILDREN = ChainList_Add_NoNull(newnode->CHILDREN, newChild);
		// Detect the default case
		hier_case_t* case_data = &nodeChild->NODE.CASE;
		if(case_data->is_default!=0) newswitch_data->info.default_case = newChild;
		// Save in old2new tree
		avl_pp_add_overwrite(&old2new, nodeChild, newChild);
	}

	// Update inter-Case links
	foreach(newnode->CHILDREN, scanChild) {
		hier_node_t* newChild = scanChild->DATA;
		hier_case_t* newcase_data = &newChild->NODE.CASE;
		newcase_data->build.case_after = local_old2new(newcase_data->build.case_after);
		newcase_data->build.case_before = local_old2new(newcase_data->build.case_before);
	}

	// Duplicate miscellaneous fields
	newswitch_data->label_name = switch_data->label_name;
	newswitch_data->info.was_if = switch_data->info.was_if;
	newswitch_data->info.switch_test = hvex_dup(switch_data->info.switch_test);

	// The CORE annotations
	newswitch_data->core = switch_data->core;

	// Clean
	avl_pp_reset(&old2new);

	return newnode;
}

static hier_node_t* Hier_Node_Dup_Loop(hier_node_t* node) {
	hier_node_t* newnode = Hier_Node_NewType(node->TYPE);
	hier_loop_t* node_loop = &node->NODE.LOOP;
	hier_loop_t* newnode_loop = &newnode->NODE.LOOP;

	// Duplicate the children

	if(node_loop->body_after!=NULL) {
		newnode_loop->body_after = Hier_Level_Dup(node_loop->body_after);
		newnode_loop->body_after->PARENT = newnode;
		newnode->CHILDREN = addchain(newnode->CHILDREN, newnode_loop->body_after);
	}

	if(node_loop->body_before!=NULL) {
		newnode_loop->body_before = Hier_Level_Dup(node_loop->body_before);
		newnode_loop->body_before->PARENT = newnode;
		newnode->CHILDREN = addchain(newnode->CHILDREN, newnode_loop->body_before);
	}

	// Duplicate the test info
	newnode_loop->testinfo->cond = hvex_dup(node_loop->testinfo->cond);

	// Other fields
	newnode_loop->label_name = node_loop->label_name;
	newnode_loop->flags = node_loop->flags;
	newnode_loop->user  = node_loop->user;
	newnode_loop->core  = node_loop->core;

	return newnode;
}

hier_node_t* Hier_Node_Dup(hier_node_t* node) {
	if(node==NULL) return NULL;

	hier_node_t* newnode = NULL;

	switch(node->TYPE) {

		case HIERARCHY_JUMP :
		case HIERARCHY_CALL :
		case HIERARCHY_RETURN :
			newnode = Hier_Node_Dup_Jump(node);
			break;
		case HIERARCHY_LABEL :
			newnode = Hier_Node_Dup_Label(node);
			break;
		case HIERARCHY_STATE :
			newnode = Hier_Node_Dup_State(node);
			break;
		case HIERARCHY_BB :
			newnode = Hier_Node_Dup_BB(node);
			break;
		case HIERARCHY_SWITCH :
			newnode = Hier_Node_Dup_Switch(node);
			break;
		case HIERARCHY_LOOP :
			newnode = Hier_Node_Dup_Loop(node);
			break;
		default :
			printf("ERROR %s.%d : won't duplicate the node with type %d.\n", __FILE__, __LINE__, node->TYPE);
			exit(EXIT_FAILURE);
	}

	if(newnode->source_lines==NULL) newnode->source_lines = LineList_Dup(node->source_lines);
	newnode->timing = node->timing;

	return newnode;
}

// Duplicate a Level of nodes

static avl_pp_tree_t* sh_old2new = NULL;

hier_node_t* Hier_Level_Dup_stopnode(hier_node_t* node, hier_node_t* stopNode) {
	if(node==NULL) return NULL;

	hier_node_t* newnode_first = NULL;
	hier_node_t* newnode_prev = NULL;

	for( ; node!=stopNode; node=node->NEXT) {
		hier_node_t* newnode = Hier_Node_Dup(node);
		assert(newnode!=NULL);
		if(newnode_prev!=NULL) Hier_Nodes_Link(newnode_prev, newnode);
		else newnode_first = newnode;
		if(sh_old2new!=NULL) avl_pp_add_overwrite(sh_old2new, node, newnode);
		newnode_prev = newnode;
	}

	return newnode_first;
}
hier_node_t* Hier_Level_Dup(hier_node_t* node) {
	return Hier_Level_Dup_stopnode(node, NULL);
}

// A wrapper for the above function: add a tree to link old->new nodes
hier_node_t* Hier_Level_Dup_link(hier_node_t* node, avl_pp_tree_t* tree_old2new) {
	sh_old2new = tree_old2new;
	hier_node_t* newnode = Hier_Level_Dup(node);
	sh_old2new = NULL;
	return newnode;
}

// Duplicate an entire PROCESS body, with all other Levels it can JUMP to
// Note: All duplicated nodes are aded to the Hier lists of nodes

hier_node_t* Hier_Dup_ProcBody_link(hier_t* H, hier_node_t* nodeProc, avl_pp_tree_t* tree_old2new) {
	// - Create a label for all RETURN nodes to go there
	// - Convert all RETURN nodes into JUMP nodes

	hier_node_t* newBody = NULL;

	hier_node_t* nodeRetLabel = Hier_Node_NewType(HIERARCHY_LABEL);
	nodeRetLabel->source_lines = LineList_Dup(nodeProc->source_lines);

	#if 0  // For debug: give the Label a meaningful name
	hier_proc_t* proc_data = &nodeProc->NODE.PROCESS;
	char buf[strlen(proc_data->name_orig) + 50];
	sprintf(buf, "hier_retfromproc_%s", proc_data->name_orig);
	nodeRetLabel->NODE.LABEL.name = namealloc(buf);
	#endif

	Hier_Lists_AddNode(H, nodeRetLabel);

	// Init: list the main Level of the Process to be duplicated
	chain_list* list_levels_scan = addchain(NULL, nodeProc->NEXT);
	chain_list* list_nextlevels = NULL;

	// Recursive scan of a Level (launch it on freshly duplicated Levels)
	// Search new Jumps and list the next Levels to scan
	void scanlevel(hier_node_t* node) {
		for( ; node!=NULL; node=node->NEXT) {
			foreach(node->CHILDREN, scanChild) scanlevel(scanChild->DATA);
			if(node->TYPE==HIERARCHY_JUMP) {
				hier_jump_t* jump_data = &node->NODE.JUMP;
				hier_node_t* nodeLabel = jump_data->target;
				assert(nodeLabel->TYPE==HIERARCHY_LABEL);
				// Only consider Labels that have no PREV nor PARENT nodes
				// Because if it has one, the Level should be duplicated from there
				if(jump_data->target->PREV==NULL && jump_data->target->PARENT==NULL) {
					if(avl_pp_isthere(tree_old2new, jump_data->target)==false) {
						list_nextlevels = ChainList_Add_NoDup(list_nextlevels, jump_data->target);
					}
				}
			}
		}  // Scan the Level
	}  // Local function: scanlevel

	// Loop while there are Levels to duplicate
 	do {

		// Duplicate the listed levels and scan them
		foreach(list_levels_scan, scanLevel) {
			hier_node_t* origNode = scanLevel->DATA;
			if(avl_pp_isthere(tree_old2new, origNode)==true) continue;  // Paranoia
			hier_node_t* newNode = Hier_Level_Dup_link(origNode, tree_old2new);
			Hier_Lists_AddLevel(H, newNode);
			scanlevel(newNode);
			if(origNode==nodeProc->NEXT) {
				assert(newBody==NULL);
				newBody = newNode;
			}
		}

		// Swap lists
		freechain(list_levels_scan);
		list_levels_scan = list_nextlevels;

	} while(list_levels_scan!=NULL);
	assert(newBody!=NULL);

	// Link the return Label
	Hier_Nodes_Link(Hier_Level_GetLastNode(newBody), nodeRetLabel);

	// Update inter-node links (nodes RETURN, JUMP, CASE)
	avl_pp_foreach(tree_old2new, scan) {
		hier_node_t* node = scan->data;  // The field data contains the new node
		if(node->TYPE==HIERARCHY_RETURN) {
			hier_jump_t* ret_data = &node->NODE.JUMP;
			assert(ret_data->target==nodeProc);
			// Unlink the node to cleanly replace it
			ret_data->target = NULL;
			Hier_Lists_RemoveNode(H, node);
			// Simply change the type of the node
			// FIXME This is not very clean. It will work as long as the data structure is same for RETURN and JUMP.
			node->TYPE = HIERARCHY_JUMP;
			ret_data->type = HIER_JUMP_RET;
			ret_data->target = nodeRetLabel;
			hier_label_t* retlabel_data = &nodeRetLabel->NODE.LABEL;
			retlabel_data->jumps = addchain(retlabel_data->jumps, node);
			// Re-link the node
			Hier_Lists_AddNode(H, node);
		}
		else if(node->TYPE==HIERARCHY_CALL) {
			hier_jump_t* call_data = &node->NODE.JUMP;
			assert(call_data->target!=nodeProc);
		}
		else if(node->TYPE==HIERARCHY_JUMP) {
			hier_jump_t* jump_data = &node->NODE.JUMP;
			hier_node_t* newLabel = NULL;
			avl_pp_find_data(tree_old2new, jump_data->target, (void*)&newLabel);
			assert(newLabel!=NULL);
			jump_data->target = newLabel;
			hier_label_t* label_data = &newLabel->NODE.LABEL;
			label_data->jumps = addchain(label_data->jumps, node);
		}
	}

	return newBody;
}

// Duplicate a Hier structure into a new Implem

void Hier_Dup(implem_t* newImplem) {
	implem_t* Implem = newImplem->DUP.father;

	assert(newImplem->H==NULL);
	newImplem->H = Hier_New();

	void* local_old2new(void* oldptr) {
		void* newptr = NULL;
		avl_pp_find_data(&newImplem->DUP.pointers_old2new, oldptr, &newptr);
		return newptr;
	}

	// Duplicate the nodes
	// FIXME isn't this a big duplicate of above functions ?
	avl_p_foreach(&Implem->H->NODES_ALL, scanNode) {
		hier_node_t* node = scanNode->data;
		hier_node_t* newnode = Hier_Node_NewType(node->TYPE);

		avl_p_add_keep(&newImplem->H->NODES_ALL, newnode, newnode);
		avl_p_add_keep(&newImplem->H->NODES[newnode->TYPE], newnode, newnode);
		Implem_Dup_LinkPointers(newImplem, node, newnode);

		newnode->timing = node->timing;
		newnode->source_lines = LineList_Dup(node->source_lines);

		switch(node->TYPE) {

			case HIERARCHY_PROCESS : {
				hier_proc_t* proc_data = &node->NODE.PROCESS;
				hier_proc_t* newproc_data = &newnode->NODE.PROCESS;

				newproc_data->name_orig = proc_data->name_orig;
				if(Implem->H->PROCESS==node) newImplem->H->PROCESS = newnode;

				break;
			}

			case HIERARCHY_JUMP : {
				hier_jump_t* jump_data = &node->NODE.JUMP;
				hier_jump_t* newjump_data = &newnode->NODE.JUMP;

				newjump_data->type = jump_data->type;

				break;
			}

			case HIERARCHY_CALL : {
				// Nothing to do here
				break;
			}
			case HIERARCHY_RETURN : {
				// Nothing to do here
				break;
			}

			case HIERARCHY_LABEL : {
				hier_label_t* label_data = &node->NODE.LABEL;
				hier_label_t* newlabel_data = &newnode->NODE.LABEL;

				newlabel_data->name = label_data->name;

				break;
			}

			case HIERARCHY_BB : {
				break;
			}

			case HIERARCHY_STATE : {
				hier_state_t* node_pt = &node->NODE.STATE;
				hier_state_t* new_pt = &newnode->NODE.STATE;

				new_pt->time.delay  = node_pt->time.delay;
				new_pt->time.nb_clk = node_pt->time.nb_clk;
				new_pt->time.map_delay = node_pt->time.map_delay;
				new_pt->time.map_nb_clk = node_pt->time.map_nb_clk;

				// Duplicate Actions
				foreach(node_pt->actions, scanAction) {
					hier_action_t* action = scanAction->DATA;
					hier_action_t* newaction = Hier_Action_Dup(action);
					Hier_Action_Link(newnode, newaction);
					Implem_Dup_LinkPointers(newImplem, action, newaction);
				}

				break;
			}

			case HIERARCHY_LOOP : {
				hier_loop_t* node_loop = &node->NODE.LOOP;
				hier_loop_t* new_loop = &newnode->NODE.LOOP;

				new_loop->testinfo->cond = hvex_dup(node_loop->testinfo->cond);

				new_loop->label_name = node_loop->label_name;
				new_loop->flags = node_loop->flags;
				new_loop->user = node_loop->user;
				new_loop->core = node_loop->core;

				break;
			}

			case HIERARCHY_CASE : {
				hier_case_t* case_data = &node->NODE.CASE;
				hier_case_t* newcase_data = &newnode->NODE.CASE;

				newcase_data->is_default = case_data->is_default;

				avl_p_foreach(&case_data->values, scanval) {
					hier_testinfo_t* testinfo = scanval->data;
					hier_testinfo_t* newtestinfo = Hier_TestInfo_New();
					newtestinfo->cond = hvex_dup(testinfo->cond);
					avl_p_add_overwrite(&newcase_data->values, newtestinfo, newtestinfo);
				}

				newcase_data->user = case_data->user;

				break;
			}

			case HIERARCHY_SWITCH : {
				hier_switch_t* switch_data = &node->NODE.SWITCH;
				hier_switch_t* newswitch_data = &newnode->NODE.SWITCH;

				newswitch_data->label_name = switch_data->label_name;
				newswitch_data->info.was_if = switch_data->info.was_if;
				newswitch_data->info.switch_test = hvex_dup(switch_data->info.switch_test);

				newswitch_data->core = switch_data->core;

				break;
			}

			default : {
				DEBUGLINE();
				exit(EXIT_FAILURE);
				break;
			}

		}  // switch(TYPE)

	}  // Duplicate all nodes without linking them

	// Link the nodes together (next/prev, parent/child)
	// Fix internal fields related to other nodes
	avl_p_foreach(&Implem->H->NODES_ALL, scanNode) {
		hier_node_t* node = scanNode->data;
		hier_node_t* newnode = local_old2new(node);

		// Set the Prev/Next links
		newnode->PREV = local_old2new(node->PREV);
		newnode->NEXT = local_old2new(node->NEXT);
		newnode->PARENT = local_old2new(node->PARENT);

		foreach(node->CHILDREN, scan) {
			hier_node_t* newChild = local_old2new(scan->DATA);
			assert(newChild!=NULL);
			newnode->CHILDREN = addchain(newnode->CHILDREN, newChild);
		}

		// Set the internal data
		switch(node->TYPE) {

			case HIERARCHY_PROCESS: {
				break;
			}

			case HIERARCHY_JUMP: {
				hier_jump_t* jump_data = &node->NODE.JUMP;
				hier_jump_t* newjump_data = &newnode->NODE.JUMP;

				newjump_data->target = local_old2new(jump_data->target);

				hier_label_t* newlabel_data = &newjump_data->target->NODE.LABEL;
				newlabel_data->jumps = addchain(newlabel_data->jumps, newnode);

				break;
			}

			case HIERARCHY_CALL: {
				hier_jump_t* call_data = &node->NODE.JUMP;
				hier_jump_t* newcall_data = &newnode->NODE.JUMP;

				newcall_data->target = local_old2new(call_data->target);

				hier_proc_t* newproc_data = &newcall_data->target->NODE.PROCESS;
				newproc_data->calls = addchain(newproc_data->calls, newnode);

				break;
			}
			case HIERARCHY_RETURN: {
				hier_jump_t* ret_data = &node->NODE.JUMP;
				hier_jump_t* newret_data = &newnode->NODE.JUMP;

				newret_data->target = local_old2new(ret_data->target);

				hier_proc_t* newproc_data = &newret_data->target->NODE.PROCESS;
				newproc_data->returns = addchain(newproc_data->returns, newnode);

				break;
			}

			case HIERARCHY_LABEL : {
				// Nothing to do: handled in JUMP node type
				break;
			}

			case HIERARCHY_BB: {
				hier_bb_t* bb_data = &node->NODE.BB;
				hier_bb_t* newbb_data = &newnode->NODE.BB;

				newbb_data->body = local_old2new(bb_data->body);

				break;
			}

			case HIERARCHY_STATE: {
				hier_state_t* state_data = &node->NODE.STATE;
				//hier_state_t* newstate_data = &newnode->NODE.STATE;

				// Duplicate inter-Actions stuff
				foreach(state_data->actions, scanAction) {
					hier_action_t* action = scanAction->DATA;
					if(avl_pi_isempty(&action->deps_timed)==true) continue;
					hier_action_t* newaction = local_old2new(action);
					// Timed dependencies
					avl_pi_foreach(&action->deps_timed, scanDep) {
						hier_action_t* target_action = scanDep->key;
						hier_action_t* newtarget_action = local_old2new(target_action);
						assert(newtarget_action!=NULL);
						int time_val = scanDep->data;
						avl_pi_add_overwrite(&newaction->deps_timed, newtarget_action, time_val);
					}  // Scan dependencies
					// Inline Actions
					foreach(action->acts_inline, scanInl) {
						hier_action_t* other_action = scanInl->DATA;
						hier_action_t* newother_action = local_old2new(other_action);
						assert(newother_action!=NULL);
						newaction->acts_inline = addchain(newaction->acts_inline, newother_action);
					}
				}  // Scan Actions

				break;
			}

			case HIERARCHY_LOOP: {
				hier_loop_t* node_loop = &node->NODE.LOOP;
				hier_loop_t* new_loop = &newnode->NODE.LOOP;

				new_loop->body_after = local_old2new(node_loop->body_after);
				new_loop->body_before = local_old2new(node_loop->body_before);

				break;
			}

			case HIERARCHY_CASE : {
				hier_case_t* case_data = &node->NODE.CASE;
				hier_case_t* newcase_data = &newnode->NODE.CASE;

				newcase_data->body = local_old2new(case_data->body);

				break;
			}

			case HIERARCHY_SWITCH : {
				hier_switch_t* switch_data = &node->NODE.SWITCH;
				hier_switch_t* newswitch_data = &newnode->NODE.SWITCH;

				newswitch_data->info.default_case = local_old2new(switch_data->info.default_case);

				break;
			}

			default: {
				DEBUGLINE();
				exit(EXIT_FAILURE);
				break;
			}

		}  // switch(type)

	}  // Link the nodes together (prev/next, parent/child)

	// A big check to be sure
	#ifndef NDEBUG
	Hier_Check_Integrity(newImplem->H);
	#endif
}


