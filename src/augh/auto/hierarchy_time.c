
// Contents of this file: computation of the design execution time

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <assert.h>
#include <inttypes.h>
#include <math.h>

#include "auto.h"
#include "hierarchy.h"



// Delay of assignations

int Hier_Latency_ComputeAction(implem_t* Implem, hier_action_t* action) {
	action->delay = 0;
	action->nb_clk = 1;

	double delay = Techno_Eval_Latency_VexAsg(Implem, action->expr);
	if(delay<0) return -1;

	action->delay = delay;
	if(Implem->synth_target->clk_period_ns>0) {
		action->nb_clk = ceil(action->delay/Implem->synth_target->clk_period_ns);
	}
	if(action->nb_clk < 1) action->nb_clk = 1;

	//dbgprintf("Action %g ns %u clk\n", action->delay, action->nb_clk);

	return 0;
}
void Hier_Latency_ComputeAllStates(implem_t* Implem) {
	hier_t* H = Implem->H;

	// Reset global parameters
	Implem->time.max_state_delay = 0;
	Implem->time.max_cycles_per_state = 0;

	// Scan all States
	avl_p_foreach(&H->NODES[HIERARCHY_STATE], scanNode) {
		hier_node_t* node = scanNode->data;
		hier_state_t* state_data = &node->NODE.STATE;

		// Clear previous delay values
		state_data->time.delay = 0;
		state_data->time.nb_clk = 0;

		// Scan all Actions, keep previously computed delays
		foreach(state_data->actions, scanAction) {
			hier_action_t* action = scanAction->DATA;
			Hier_Latency_ComputeAction(Implem, action);
			if(action->delay > state_data->time.delay)   state_data->time.delay = action->delay;
			if(action->nb_clk > state_data->time.nb_clk) state_data->time.nb_clk = action->nb_clk;
		}

		//dbgprintf("State %g ns %u clk\n", state_data->time.delay, state_data->time.nb_clk);
		if(state_data->time.delay > Implem->time.max_state_delay) Implem->time.max_state_delay = state_data->time.delay;
		if(state_data->time.nb_clk > Implem->time.max_cycles_per_state) Implem->time.max_cycles_per_state = state_data->time.nb_clk;
	}  // Scan States
}

void Hier_Latency_ClearMapper(implem_t* Implem) {
	// Clear global parameters
	Implem->time.map_max_action_delay = 0;
	Implem->time.map_max_cycles_per_state = 0;
	// Clear per-State annotations
	avl_p_foreach(&Implem->H->NODES[HIERARCHY_STATE], scanNode) {
		hier_node_t* node = scanNode->data;
		hier_state_t* state_data = &node->NODE.STATE;
		foreach(state_data->actions, scanAction) {
			hier_action_t* action = scanAction->DATA;
			action->map_delay = 0;
		}
		state_data->time.map_delay = 0;
		state_data->time.map_nb_clk = 0;
	}
}

chain_pd_t* Hier_Latency_GetOrderedList(implem_t* Implem) {
	hier_t* H = Implem->H;

	chain_pd_t* list_trans = NULL;

	avl_p_foreach(&H->NODES[HIERARCHY_STATE], scanNode) {
		hier_node_t* node = scanNode->data;
		hier_state_t* state_data = &node->NODE.STATE;
		if(state_data->time.delay<=0) continue;
		list_trans = chain_pd_add(list_trans, node, state_data->time.delay);
	}  // Scan all the hierarchy nodes

	list_trans = chain_pd_sort_dec(list_trans);
	return list_trans;
}

void Hier_Latency_DisplayWorse(implem_t* Implem, int nb) {
	//hier_t* H = Implem->H;

	chain_pd_t* list_trans = Hier_Latency_GetOrderedList(Implem);

	// Display
	int count = 0;
	foreach(list_trans, scanNode) {
		if(nb>=0 && count>=nb) break;
		hier_node_t* node = scanNode->p;
		printf("State with delay %g ns.\n", node->NODE.STATE.time.delay);
		count++;
	}
	printf("%d states were displayed.\n", count);

	chain_pd_del(list_trans);
}

unsigned Hier_Latency_CountAbove(implem_t* Implem, double lat) {
	//hier_t* H = Implem->H;

	chain_pd_t* list_trans = Hier_Latency_GetOrderedList(Implem);

	unsigned count = 0;
	foreach(list_trans, scan) if(scan->d>lat) count++;
	chain_pd_del(list_trans);

	return count;
}

// Compute the execution time "once"

minmax_t Hier_Timing_GetLevelTime(hier_node_t* node) {
	minmax_t t = minmax_make_cst(0);
	for( ; node!=NULL; node=node->NEXT) minmax_add(&t, &node->timing.once);
	return t;
}

static void Hier_Timing_ComputeLevelTime(hier_node_t* node, double factor, bool from_mapper);

static void Hier_Timing_ComputeNodeTime(hier_node_t* node, double factor, bool from_mapper) {
	if(node==NULL) return;

	switch(node->TYPE) {

		case HIERARCHY_STATE : {
			hier_state_t* state_data = &node->NODE.STATE;
			if(from_mapper==false) minmax_set_cst(&node->timing.once, state_data->time.nb_clk);
			else minmax_set_cst(&node->timing.once, state_data->time.map_nb_clk);
			time_casefactor_set_factor(&node->timing, factor);
			break;
		}

		case HIERARCHY_BB : {
			hier_bb_t* node_bb = &node->NODE.BB;
			Hier_Timing_ComputeLevelTime(node_bb->body, factor, from_mapper);
			node->timing.once = Hier_Timing_GetLevelTime(node_bb->body);
			time_casefactor_set_factor(&node->timing, factor);
			break;
		}

		case HIERARCHY_LOOP : {
			hier_loop_t* node_loop = &node->NODE.LOOP;
			minmax_set_cst(&node->timing.once, 0);

			// Get the number of times each body is executed
			double factor_body_after = 0;
			double factor_body_before = 0;

			if(hier_for_chkiternb(node_loop->flags)) {
				factor_body_after  = node_loop->iter.testfalse_nb + 1;
				factor_body_before = node_loop->iter.testfalse_nb;
			}
			else if(node_loop->user.iter_nb_given==true) {
				factor_body_after  = node_loop->user.iter_nb;
				factor_body_before = node_loop->user.iter_nb;
				if(node_loop->body_after!=NULL && node_loop->body_before!=NULL) factor_body_after++;
			}
			else {
				factor_body_after  = 1;
				factor_body_before = 1;
				if(node_loop->body_after!=NULL && node_loop->body_before!=NULL) factor_body_after++;
			}
			//dbgprintf("Loop iteration factors: after %g before %g\n", factor_body_after, factor_body_before);

			// Apply to the children, compute the total execution time
			minmax_t t;
			if(node_loop->body_after!=NULL) {
				Hier_Timing_ComputeLevelTime(node_loop->body_after, factor_body_after*factor, from_mapper);
				t = Hier_Timing_GetLevelTime(node_loop->body_after);
				minmax_addmult(&node->timing.once, &t, factor_body_after);
			}
			if(node_loop->body_before!=NULL) {
				Hier_Timing_ComputeLevelTime(node_loop->body_before, factor_body_before*factor, from_mapper);
				t = Hier_Timing_GetLevelTime(node_loop->body_before);
				minmax_addmult(&node->timing.once, &t, factor_body_before);
			}

			// Assume the loop takes one cycle for the test synchronization
			if(node->timing.once.avg==0) minmax_set_cst(&node->timing.once, 1);
			// Apply the global factor
			time_casefactor_set_factor(&node->timing, factor);

			break;
		}

		case HIERARCHY_SWITCH : {
			// Note: we assume branch probabilities of Case nodes are ALWAYS set
			// This is done as a post-parsing operation
			minmax_set_cst(&node->timing.once, 0);
			// Compute the entire execution time
			foreach(node->CHILDREN, scanChild) {
				hier_node_t* child = scanChild->DATA;
				hier_case_t* node_case = &child->NODE.CASE;
				double prob = node_case->user.branch_prob;
				Hier_Timing_ComputeLevelTime(child, prob*factor, from_mapper);
				minmax_add_prob(&node->timing.once, &child->timing.once, prob);
			}
			// Apply the global factor
			time_casefactor_set_factor(&node->timing, factor);
			break;
		}

		case HIERARCHY_CASE : {
			hier_case_t* node_case = &node->NODE.CASE;
			minmax_set_cst(&node->timing.once, 0);
			Hier_Timing_ComputeLevelTime(node_case->body, factor, from_mapper);
			node->timing.once = Hier_Timing_GetLevelTime(node_case->body);
			time_casefactor_set_factor(&node->timing, factor);
			break;
		}

		case HIERARCHY_CALL : {
			hier_jump_t* call_data = &node->NODE.JUMP;
			if(call_data->target->timing.factor==0) {
				Hier_Timing_ComputeLevelTime(call_data->target, 1, from_mapper);
				// FIXMEEE This hack is ugly. The PROC body should be a field of the structure, not in the same LEVEL
				call_data->target->timing.once = Hier_Timing_GetLevelTime(call_data->target->NEXT);
				time_casefactor_set_factor(&call_data->target->timing, 1);
			}
			node->timing.once = call_data->target->timing.once;
			time_casefactor_set_factor(&node->timing, factor);
			break;
		}

		default : {
			// Default value
			minmax_set_cst(&node->timing.once, 0);
			time_casefactor_set_factor(&node->timing, factor);
			if(node->CHILDREN!=NULL) printf("WARNING %s : A node of type %d has children.\n", __func__, node->TYPE);
			break;
		}
	}

}

static void Hier_Timing_ComputeLevelTime(hier_node_t* node, double factor, bool from_mapper) {
	for( ; node!=NULL; node=node->NEXT) Hier_Timing_ComputeNodeTime(node, factor, from_mapper);
}

void Hier_Timing_Update(implem_t* Implem, bool from_mapper) {
	hier_t* H = Implem->H;

	// Reset the timings of the Processes
	avl_p_foreach(&H->NODES[HIERARCHY_PROCESS], scan) {
		hier_node_t* node = scan->data;
		time_casefactor_reset(&node->timing);
	}

	// Compute the delay of all Actions and States
	if(from_mapper==false) Hier_Latency_ComputeAllStates(Implem);

	// Compute the execution time of all nodes
	Hier_Timing_ComputeLevelTime(H->PROCESS, 1, from_mapper);

	// FIXME This is ugly
	H->PROCESS->timing.once = Hier_Timing_GetLevelTime(H->PROCESS->NEXT);
	time_casefactor_set_factor(&H->PROCESS->timing, 1);

	// Set the global execution time
	if(from_mapper==false) Implem->time.execution_cycles = H->PROCESS->timing.once;
	else Implem->time.map_execution_cycles = H->PROCESS->timing.once;
}

void Hier_Timing_ResetAnnotationsUpdate(implem_t* Implem) {
	hier_t* H = Implem->H;

	dbgprintf("Will reset annotations.");
	minmax_print(&Implem->time.execution_cycles, "  Total execution time : ", " (clock cycles)\n");

	// Reset annotations
	avl_p_foreach(&H->NODES_ALL, scan) {
		hier_node_t* node = scan->data;
		if(node->TYPE==HIERARCHY_CASE) {
			hier_case_t* case_data = &node->NODE.CASE;
			case_data->user.never_taken       = false;
			case_data->user.wired_wanted      = false;
			case_data->user.branch_prob_given = false;
		}
		if(node->TYPE==HIERARCHY_LOOP) {
			hier_loop_t* loop_data = &node->NODE.LOOP;
			loop_data->user.iter_nb_given        = false;
			loop_data->user.iter_nb_power2_given = false;
			loop_data->user.parallel_given       = false;
		}
	}

	// Update the general execution time
	Hier_Timing_Update(Implem, false);

	dbgprintf("Annotations are reset.");
	minmax_print(&Implem->time.execution_cycles, "  Total execution time : ", " (clock cycles)\n");
}

void Hier_Timing_Show_Nodes(implem_t* Implem) {
	hier_t* H = Implem->H;

	printf("Global timings for the PROC nodes :");
	avl_p_foreach(&H->NODES[HIERARCHY_PROCESS], scan) {
		hier_node_t* node = scan->data;
		printf(" '%s'=%g", node->NODE.PROCESS.name_orig, node->timing.global.avg);
	}
	printf("\n");

	printf("Global timings for the STATE nodes :");
	avl_p_foreach(&H->NODES[HIERARCHY_STATE], scan) {
		hier_node_t* node = scan->data;
		printf(" %g", node->timing.global.avg);
	}
	printf("\n");

	printf("Global timings for the LOOP nodes :");
	avl_p_foreach(&H->NODES[HIERARCHY_LOOP], scan) {
		hier_node_t* node = scan->data;
		printf(" %g", node->timing.global.avg);
	}
	printf("\n");

	printf("Global timings for the SWITCH :");
	avl_p_foreach(&H->NODES[HIERARCHY_SWITCH], scan) {
		hier_node_t* node = scan->data;
		printf(" %g", node->timing.global.avg);
	}
	printf("\n");

	printf("  Internal timings for the CASE :");
	avl_p_foreach(&H->NODES[HIERARCHY_CASE], scan) {
		hier_node_t* node = scan->data;
		printf(" %g", node->timing.once.avg);
	}
	printf("\n");

	double sum = 0;
	printf("Global timings for the Basic Blocks :");
	avl_p_foreach(&H->NODES[HIERARCHY_BB], scan) {
		hier_node_t* node = scan->data;
		printf(" %g", node->timing.global.avg);
		sum += node->timing.global.avg;
	}
	printf(" (total %g)\n", sum);
}

void Hier_Timing_Show(implem_t* Implem) {
	hier_t* H = Implem->H;

	double minfreq = 0;
	if(Implem->time.max_state_delay > 0) minfreq = 1000 / Implem->time.max_state_delay;
	double minfreq_map = 0;
	if(Implem->time.map_max_action_delay > 0) minfreq_map = 1000 / Implem->time.map_max_action_delay;

	printf("  Target frequency/period is : %g MHz / %g ns\n", Implem->synth_target->clk_freq, Implem->synth_target->clk_period_ns);
	minmax_print(&Implem->time.execution_cycles, "  Total execution time (estim)  : ", " (clock cycles)\n");
	minmax_print(&Implem->time.map_execution_cycles, "  Total execution time (mapper) : ", " (clock cycles)\n");
	printf("  Maximum delay (estim)  : %g ns / %g MHz\n", Implem->time.max_state_delay, minfreq);
	printf("  Maximum delay (mapper) : %g ns / %g MHz\n", Implem->time.map_max_action_delay, minfreq_map);
	printf("  Maximum clock cycles per control step : estim %g, mapper %u\n",
		Implem->time.max_cycles_per_state, Implem->time.map_max_cycles_per_state
	);

	// Find the first infinite loop and display its internal execution time
	for(hier_node_t* node=H->PROCESS; node!=NULL; node=node->NEXT) {
		minmax_t t;
		int found = 0;
		if(node->TYPE==HIERARCHY_LOOP) {
			hier_loop_t* node_loop = &node->NODE.LOOP;
			if(hier_loop_chkalways(node_loop->flags)) {
				t = Hier_Timing_GetLevelTime(node_loop->body_after);
				minmax_t t2 = Hier_Timing_GetLevelTime(node_loop->body_before);
				minmax_add(&t, &t2);
				found = 1;
			}
		}
		if(found==0) continue;
		minmax_print(&t, "First infinite loop, internal execution time : ", " (clock cycles)\n");
		break;
	}

}

// List usage : TYPE = number of control steps, DATA = chained list of State nodes
ptype_list* Hier_ClockCycles_GetOrderedList(implem_t* Implem) {
	hier_t* H = Implem->H;

	ptype_list* list_ctrl_steps = NULL;

	avl_p_foreach(&H->NODES[HIERARCHY_STATE], scanNode) {
		hier_node_t* node = scanNode->data;
		if(node->timing.once.avg==0) continue;  // In case it was a bypassed State
		int nb_clock_cycles = ceil(node->timing.once.avg);
		// Here the State is taken into account
		ptype_list* elt = ChainPType_SearchType(list_ctrl_steps, nb_clock_cycles);
		if(elt==NULL) {
			list_ctrl_steps = addptype(list_ctrl_steps, nb_clock_cycles, NULL);
			elt = list_ctrl_steps;
		}
		// Add the statistis
		elt->DATA = addchain(elt->DATA, node);
	}  // Scan all the hierarchy nodes

	list_ctrl_steps = ChainPType_Sort_Inc(list_ctrl_steps);
	return list_ctrl_steps;
}

void Hier_ClockCycles_Display(implem_t* Implem) {
	//hier_t* H = Implem->H;

	ptype_list* list_ctrl_steps = Hier_ClockCycles_GetOrderedList(Implem);

	// Display
	foreach(list_ctrl_steps, scanCycles) {
		double nb_trans = 0;
		double nb_trans_hier = 0;
		foreach((chain_list*)scanCycles->DATA, scan) {
			hier_node_t* node = scan->DATA;
			nb_trans ++;
			nb_trans_hier += node->timing.global.avg;
		}
		printf("States that take %ld clock cycles : %g (total %g clock cycles)\n", scanCycles->TYPE, nb_trans, nb_trans_hier);
	}

	// Free
	foreach(list_ctrl_steps, scan) freechain(scan->DATA);
	freeptype(list_ctrl_steps);
}



//=====================================================================
// Saving all timing annotations
//=====================================================================

typedef struct hier_savetime_node_t {
	// Generic data
	hier_node_t* node;
	time_casefactor_t timing;
	// For CASE, LOOP
	bool     never_taken;
	bool     wired_wanted;
	bool     branch_prob_given;
	double   branch_prob;
	bool     iter_nb_given;
	double   iter_nb;
	bool     iter_nb_power2_given;
	unsigned iter_nb_power2;
	bool     parallel_given;
} hier_savetime_node_t;

// Create a pool of type : hier_savetime_node_t

#define POOL_prefix      pool_hier_savetime_node
#define POOL_data_t      hier_savetime_node_t
#define POOL_ALLOC_SIZE  1000

#define POOL_INSTANCE             POOL_HIER_SAVETIME_NODE
#define POOL_INSTANCE_ATTRIBUTES  static

#include "../pool.h"

// Allocation wrappers

static hier_savetime_node_t* Hier_Savetime_Node_New() {
	hier_savetime_node_t* elt = pool_hier_savetime_node_pop(&POOL_HIER_SAVETIME_NODE);
	memset(elt, 0, sizeof(*elt));
	elt->never_taken          = false;
	elt->wired_wanted         = false;
	elt->branch_prob_given    = false;
	elt->iter_nb_given        = false;
	elt->iter_nb_power2_given = false;
	elt->parallel_given       = false;
	return elt;
}

static void Hier_Savetime_Node_Free(hier_savetime_node_t* elt) {
	pool_hier_savetime_node_push(&POOL_HIER_SAVETIME_NODE, elt);
}

// Main functions

void Hier_SaveTime_Build(hier_t* H, avl_pp_tree_t* node2save) {
	// Process all nodes
	avl_p_foreach(&H->NODES_ALL, scan) {
		hier_node_t* node = scan->data;
		hier_savetime_node_t* savetime = Hier_Savetime_Node_New();
		avl_pp_add_overwrite(node2save, node, savetime);
		// Add content
		savetime->node = node;
		savetime->timing = node->timing;
		if(node->TYPE==HIERARCHY_CASE) {
			hier_case_t* case_data = &node->NODE.CASE;
			savetime->never_taken       = case_data->user.never_taken;
			savetime->wired_wanted      = case_data->user.wired_wanted;
			savetime->branch_prob_given = case_data->user.branch_prob_given;
			savetime->branch_prob       = case_data->user.branch_prob;
		}
		if(node->TYPE==HIERARCHY_LOOP) {
			hier_loop_t* loop_data = &node->NODE.LOOP;
			savetime->iter_nb_given        = loop_data->user.iter_nb_given;
			savetime->iter_nb              = loop_data->user.iter_nb;
			savetime->iter_nb_power2_given = loop_data->user.iter_nb_power2_given;
			savetime->iter_nb_power2       = loop_data->user.iter_nb_power2;
			savetime->parallel_given       = loop_data->user.parallel_given;
		}
	}
}

void Hier_SaveTime_Commit(hier_t* H, avl_pp_tree_t* node2save) {
	avl_pp_foreach(node2save, scan) {
		hier_savetime_node_t* savetime = scan->data;
		hier_node_t* node = savetime->node;
		// Callback contents
		node->timing = savetime->timing;
		if(node->TYPE==HIERARCHY_CASE) {
			hier_case_t* case_data = &node->NODE.CASE;
			case_data->user.never_taken       = savetime->never_taken;
			case_data->user.wired_wanted      = savetime->wired_wanted;
			case_data->user.branch_prob_given = savetime->branch_prob_given;
			case_data->user.branch_prob       = savetime->branch_prob;
		}
		if(node->TYPE==HIERARCHY_LOOP) {
			hier_loop_t* loop_data = &node->NODE.LOOP;
			loop_data->user.iter_nb_given        = savetime->iter_nb_given;
			loop_data->user.iter_nb              = savetime->iter_nb;
			loop_data->user.iter_nb_power2_given = savetime->iter_nb_power2_given;
			loop_data->user.iter_nb_power2       = savetime->iter_nb_power2;
			loop_data->user.parallel_given       = savetime->parallel_given;
		}
	}
}

void Hier_SaveTime_Clear(avl_pp_tree_t* node2save) {
	avl_pp_foreach(node2save, scan) Hier_Savetime_Node_Free(scan->data);
	avl_pp_reset(node2save);
}

void Hier_SaveTime_InitBuild(hier_t* H, avl_pp_tree_t* node2save) {
	avl_pp_init(node2save);
	Hier_SaveTime_Build(H, node2save);
}
void Hier_SaveTime_CommitClear(hier_t* H, avl_pp_tree_t* node2save) {
	Hier_SaveTime_Commit(H, node2save);
	Hier_SaveTime_Clear(node2save);
}

void Hier_SaveTime_CommitClearUpdate(implem_t* Implem, avl_pp_tree_t* node2save) {
	hier_t* H = Implem->H;

	Hier_SaveTime_CommitClear(H, node2save);

	// Compute the execution time of all nodes
	Hier_Timing_ComputeLevelTime(H->PROCESS, 1, false);

	// Set the general execution time
	Implem->time.execution_cycles = Hier_Timing_GetLevelTime(H->PROCESS);

	printf("DEBUG %s:%u : Annotations set back.", __FILE__, __LINE__);
	minmax_print(&Implem->time.execution_cycles, " Total execution time : ", " (clock cycles)\n");

}


