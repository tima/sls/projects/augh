
#ifndef _LINELIST_H_
#define _LINELIST_H_

#include <stdbool.h>

#include <mut.h>



bitype_list* LineList_Dup(bitype_list* list);
void         LineList_Free(bitype_list* list);
bitype_list* LineList_Add(bitype_list* list, char* name, int line);

void LineList_Print_fbse(FILE* F, bitype_list* list, char* beg, char* sep_nameline, char* sep_lines, char* sep_names, char* end);
void LineList_Print_fbe(FILE* F, bitype_list* list, char* beg, char* end);
void LineList_Print_f(FILE* F, bitype_list* list);
void LineList_Print_be(bitype_list* list, char* beg, char* end);
void LineList_Print(bitype_list* list);

void         LineList_FPrint_machine(FILE* F, bitype_list* lines);
bitype_list* LineList_FScan_machine(FILE* F);

bool         LineList_IsThereNameLine(bitype_list* lines, char* name, long line);
bool         LineList_IsIncluded(bitype_list* ref, bitype_list* lines);



#endif // _LINELIST_H_

