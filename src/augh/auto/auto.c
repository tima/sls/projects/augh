
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>   // For command-line parameters parsing
#include <unistd.h>    // for readlink()
#include <limits.h>    // For PATH_MAX
#include <assert.h>

#include "auto.h"
#include "hierarchy.h"
#include "command.h"
#include "techno.h"
#include "schedule.h"
#include "core.h"
#include "addops.h"
#include "map.h"
#include "implem_models.h"



//======================================================================
// Creation/Free of main structures
//======================================================================

// The globals structure

augh_globals_t* augh_globals_new() {
	augh_globals_t* globals = calloc(1, sizeof(*globals));

	// Initialize the time
	TimeSpec_GetReal(&globals->time_real);
	TimeSpec_GetCpu(&globals->time_cpu);

	// Verbosity levels
	globals->verbose.level = 0;
	globals->verbose.silent = true;
	globals->verbose.lvl1 = false;
	globals->verbose.lvl2 = false;

	// Default optim level
	globals->optim_level = 0;

	// Default generation options
	globals->gen_vhdl = true;
	globals->gen_synth_proj = true;
	globals->stop_vhdl = false;

	// The tree of Implem models
	avl_pp_init(&globals->implem_models);

	// The data for duplication
	avl_pp_init(&globals->dup.pointers_new2old);
	avl_pp_init(&globals->dup.pointers_old2new);
	avl_p_init(&globals->dup.children);

	return globals;
}

// Free the content but not the child globals, these are supposed to be handled separately.
void augh_globals_free(augh_globals_t* globals) {

	// The duplicated structures
	if(globals->dup.father!=NULL) {
		avl_pp_reset(&globals->dup.pointers_new2old);
		avl_pp_reset(&globals->dup.pointers_old2new);
		avl_p_rem_key(&globals->dup.father->dup.children, globals);
	}
	avl_p_foreach(&globals->dup.children, scan) {
		augh_globals_t* child_globals = scan->data;
		child_globals->dup.father = NULL;
	}
	avl_p_reset(&globals->dup.children);

	// The Implem structures
	avl_pp_foreach(&globals->implem_models, scan) {
		implem_t* Implem = scan->data;
		augh_implem_freechildren(Implem);
		augh_implem_free(Implem);
	}
	avl_pp_reset(&globals->implem_models);

	free(globals);
}

// Free all children of a globals structure.
void augh_globals_freechildren(augh_globals_t* globals) {
	avl_p_foreach(&globals->dup.children, scan) {
		augh_globals_t* child_globals = scan->data;
		augh_globals_freechildren(child_globals);
		augh_globals_free(child_globals);
	}
	avl_p_reset(&globals->dup.children);
}

// The Implem structure

implem_t* augh_implem_new(augh_globals_t* globals) {
	assert(globals!=NULL);

	implem_t* Implem = calloc(1, sizeof(*Implem));

	// The "env" fields
	Implem->env.globals = globals;
	Implem->env.entity = namealloc(DEFAULT_ENTITY);
	Implem->env.vhdl_dir = namealloc(DEFAULT_VHDL_DIR);
	Implem->env.wait_on_start = true;
	Implem->env.inline_all = false;
	Implem->env.auto_mem_readbuf = true;

	// The duplicated Implem structures
	avl_pp_init(&Implem->DUP.pointers_new2old);
	avl_pp_init(&Implem->DUP.pointers_old2new);
	avl_p_init(&Implem->DUP.children);

	avl_pi_init(&Implem->symbols_flags);

	// The scheduling data
	Sched_ImplemInit(Implem);
	// The Elaboration Core data
	Core_ImplemInit(Implem);
	// The Netlist data
	Map_Netlist_Implem_Init(Implem);

	// The data for Implem models
	Implem->compmod.time_weight = 1;
	Implem->compmod.model_name = Implem->env.entity;

	Implem->synth_target = Synth_Target_New();
	// FIXME This Implem is not added to the list of Implems in the synth_target structure?

	// The other structures are already initialized to zero/NULL by calloc().
	return Implem;
}

// Free an Implementation structure. Child Implem are not freed.
void augh_implem_free(implem_t* Implem) {

	// Unlink from the globals structure
	if(Implem==Implem->env.globals->top_implem) Implem->env.globals->top_implem = NULL;
	avl_pp_node_t* pp_node = NULL;
	bool b = avl_pp_find(&Implem->env.globals->implem_models, Implem->compmod.model_name, &pp_node);
	if(b==true && Implem==pp_node->data) {
		avl_pp_rem(&Implem->env.globals->implem_models, pp_node);
	}

	// The duplicated Implem structures
	avl_pp_reset(&Implem->DUP.pointers_new2old);
	avl_pp_reset(&Implem->DUP.pointers_old2new);
	if(Implem->DUP.father!=NULL) avl_p_rem_key(&Implem->DUP.father->DUP.children, Implem);
	avl_p_foreach(&Implem->DUP.children, childNode) {
		implem_t* childImplem = childNode->data;
		childImplem->DUP.father = NULL;
	}
	avl_p_reset(&Implem->DUP.children);

	// The scheduling data
	Sched_ImplemFree(Implem);

	// The Elaboration Core data
	Core_ImplemFree(Implem);

	// The Hierarchy structure
	if(Implem->H!=NULL) Hier_Free(Implem->H);

	avl_pi_reset(&Implem->symbols_flags);

	Map_Netlist_Implem_Free(Implem);

	// The Implem structure itself
	free(Implem);
}

// Free all children of an Implem structure.
void augh_implem_freechildren(implem_t* Implem) {
	avl_p_foreach(&Implem->DUP.children, childNode) {
		implem_t* childImplem = childNode->data;
		augh_implem_freechildren(childImplem);
		augh_implem_free(childImplem);
	}
	avl_p_reset(&Implem->DUP.children);
}



//======================================================================
// Convert some parameters
//======================================================================

ptype_list* augh_read_hwlimits(techno_t* techno, const char* string) {
	ptype_list* list = NULL;

	char const * scanptr = string;
	do {
		if(*scanptr==0) break;
		// Read the resource ID : count its size
		char const * name_begin = scanptr;
		unsigned name_size = 0;
		do {
			if(isalnum(*scanptr)!=0 || (*scanptr)=='-' || (*scanptr)=='_') { name_size++; scanptr++; }
			else break;
		}while(1);
		if(name_size<1) {
			printf("ERROR Invalid format for hardware resource limits (name not found).\n");
			exit(EXIT_FAILURE);
		}
		char name_buffer[name_size+2];
		sprintf(name_buffer, "%.*s", name_size, name_begin);
		if(techno->isresnamevalid(namealloc(name_buffer))==false) {
			printf("ERROR Invalid resource type name '%s'.\n", name_buffer);
			exit(EXIT_FAILURE);
		}
		// Skip non-numeric characters
		do {
			if(*scanptr==0) {
				printf("ERROR Invalid format for hardware resource limits (value not found).\n");
				exit(EXIT_FAILURE);
			}
			if(isdigit(*scanptr)==0) scanptr++; else break;
		}while(1);
		int limit = strtol(scanptr, (char**)&scanptr, 0);
		if(limit<1) {
			printf("ERROR Invalid format for hardware resource limits (invalid value).\n");
			exit(EXIT_FAILURE);
		}
		// Skip the separation character
		if(*scanptr!=0) scanptr++;

		// Add this resource element to the chained list
		list = addptype(list, limit, namealloc(name_buffer));

	}while(1);

	return list;
}

void OptimLevel_Set(augh_globals_t* globals, unsigned l) {
	globals->optim_level = l;
	if(l<3) {
		asgpropag_symuse_en = false;
		sched_use_vexalloc_share = false;
		core_dupimplem_inxs = false;
		core_param_weight_fork = false;
		core_param_max_degrees = 0;
	}
	else if(l==3) {
		asgpropag_symuse_en = true;
		sched_use_vexalloc_share = true;  // FIXME for idct, activating this actually slows down the generated circuit!!!
		//core_dupimplem_inxs = true;  // Can be used only with sched_use_vexalloc_share = true
		core_param_weight_fork = true;
		core_param_max_degrees = 1;
	}
}



//======================================================================
// SplitString functions
//======================================================================

// Find the different words. Replace the space characters by the null character.
// Return a list of pointers to the beginning of each word.
chain_list* SplitString_allwords(char* str) {
	chain_list* list = NULL;

	while(str!=NULL) {
		char* word = SplitString_firstword(str, &str);
		if(word==NULL) break;
		list = addchain(list, word);
	}

	return reverse(list);
}

// Find the first word. Replace the next space character by the null character.
// Return the pointer to the beginning of the first word, or NULL if no word found.
char* SplitString_firstword(char* str, char** after) {
	if(str==NULL) {
		if(after!=NULL) *after = NULL;
		return NULL;
	}

	char* word = NULL;

	do {
		char c = *str;
		if(c==0) {  // It is the end of the entered words
			if(after!=NULL) *after=NULL;
			break;
		}
		if(isspace(c)==0) {
			if(word==NULL) word = str;
		}
		else {
			if(after!=NULL) *after = str+1;
			if(word!=NULL) { *str = 0; break; }
		}
		str++;
	} while(1);

	return word;
}

// Find the first word, optionally delimited by quotes (which are removed).
// Move quoted data at the beginning to pack concatenations.
// Replace the next space character by the null character.
// Return the pointer to the beginning of the first word, or NULL if no word found.
char* SplitString_firstword_quoted(char* str, char** after) {
	if(str==NULL) {
		if(after!=NULL) *after = NULL;
		return NULL;
	}

	char* ptr  = NULL;
	char* word = NULL;
	char quote = 0;

	do {
		char c = *str;

		// Handle end of line
		if(c==0) {
			if(after!=NULL) *after=NULL;
			break;
		}

		// Handle end of quoted sequence
		if(c==quote) {
			quote = 0;
			goto NEXTCHAR;
		}

		// Handle beginning of quoted sequence
		if( quote==0 && (c=='\'' || c=='\"') ) {
			quote = c;
			goto NEXTCHAR;
		}

		// Handle space character that is not in quoted sequence
		if(isspace(c)!=0 && quote==0) {
			if(after!=NULL) *after = str + 1;
			if(word!=NULL) { *ptr = 0; break; }
			else goto NEXTCHAR;
		}

		// Keep this character
		if(word==NULL) {
			word = str;
			ptr = str + 1;
		}
		else {
			*(ptr++) = c;
		}

		NEXTCHAR:
		str++;

	} while(1);

	return word;
}


// Get the first non-space character.
char* SplitString_trim_beg(char* str) {
	if(str==NULL) return NULL;
	do {
		char c = *str;
		if(c==0) return NULL;
		if(isspace(c)==0) return str;
		str++;
	} while(1);
	return NULL;
}

// Make the string end after the last non-space character.
char* SplitString_trim_end(char* str) {
	if(str==NULL) return NULL;
	char* ptr_last = NULL;
	char* ptr_beg = str;
	do {
		char c = *str;
		if(c==0) break;
		if(isspace(c)==0) ptr_last = str;
		str++;
	} while(1);
	if(ptr_last!=NULL) ptr_last[1] = 0; else ptr_beg[0] = 0;
	return str;
}

// Make the string end after the last non-space character.
// Return the string that begins at the first non-space character.
char* SplitString_trim(char* str) {
	if(str==NULL) return NULL;
	char* ptr_first = NULL;
	char* ptr_last = NULL;
	do {
		char c = *str;
		if(c==0) break;
		if(isspace(c)==0) {
			if(ptr_first==NULL) ptr_first = str;
			ptr_last = str;
		}
		str++;
	} while(1);
	if(ptr_last!=NULL) ptr_last[1] = 0;
	return ptr_first;
}


int SplitString_istherestring(char* str, const char* token, char** after) {
	if(str==NULL || token==NULL) return 0;

	int index = 0;
	do {
		// Case where the end of the token is reached
		if(token[index]==0) {
			if(isspace(str[index])!=0) {
				// Set the pointer 'after'
				if(after!=NULL) *after = SplitString_trim_beg(str + index);
				return 1;
			}
			return 0;
		}
		// Compare the two characters
		if(str[index]!=token[index]) return 0;
		index++;
	} while(1);

	return 0;
}

char* SplitString_findword(char* str, const char* token, char** after) {
	if(str==NULL) {
		if(after!=NULL) *after = NULL;
		return NULL;
	}

	while(*str!=0) {
		int z = SplitString_istherestring(str, token, after);
		if(z!=0) return str;
		str++;
	}

	return NULL;
}

chain_list* SplitString_tokensplit(char* str, const char* token) {
	if(str==NULL) return NULL;

	chain_list* list = NULL;
	do {
		char* after = NULL;
		list = addchain(list, str);
		char* tokenfound = SplitString_findword(str, token, &after);
		if(tokenfound==NULL) break;
		if(after==NULL) break;
		str = after;
	}while(1);

	return list;
}



//======================================================================
// Misc functions
//======================================================================

unsigned uint32_dec_length(uint32_t val) {
	if(val<9) return 1;
	if(val<99) return 2;
	if(val<999) return 3;
	if(val<9999) return 4;
	if(val<99999) return 5;
	if(val<999999) return 6;
	if(val<9999999) return 7;
	if(val<99999999) return 8;
	return 9;
}

// Count the number of bits needed to store the value
// This also returns 1 for the value 0
unsigned uint_bitsnb(unsigned v) {
	if(v==0) return 1;
	unsigned c = 0;
	while(v!=0) { v >>= 1; c++; }
	return c;
}

// Return log in any base
unsigned uint_log_floor(unsigned base, unsigned n) {
	assert(base>=2);
	unsigned res = 0;
	while(n>=base) { res++; n /= base; }
	return res;
}
unsigned uint_log_ceil(unsigned base, unsigned n) {
	assert(base>=2);
	if(n==0) return 1;
	unsigned res = 0;
	while(n>=1) { res++; n /= base; }
	return res;
}

// Return a power of an integer
unsigned uint_pow(unsigned v, unsigned p) {
	unsigned res = 1;
	while(p!=0) {
		if((p & 0x01)!=0) res *= v;
		v *= v;
		p >>= 1;
	}
	return res;
}

// Round to a power of 2
unsigned uint_rndpow2_ceil(unsigned v) {
	v--;
	v |= v >> 1;
	v |= v >> 2;
	v |= v >> 4;
	v |= v >> 8;
	v |= v >> 16;

	// And another operation for systems where an unsigned is 64 bits
	#ifdef __SIZEOF_INT__
		#if __SIZEOF_INT__ > 4
			v |= v >> 32;
		#endif
	#else
		// this code should be optimized out by the compiler
		v = sizeof(v) > 4 ? v | v >> 32 : v;
	#endif

	v++;
	return v;
}

// Round to a power in any base
unsigned uint_rndpow_floor(unsigned base, unsigned n) {
	assert(base>0 && n>0);
	unsigned p = 1;
	while(base * p <= n) p *= base;
	return p;
}
unsigned uint_rndpow_ceil(unsigned base, unsigned n) {
	assert(base>0 && n>0);
	unsigned p = 1;
	while(p < n) p *= base;
	return p;
}

// Round to a power in any base, with factor: round to f*base^e
unsigned uint_rndpow_fac_floor(unsigned f, unsigned base, unsigned n) {
	assert(base>0 && n>0 && f>0 && f<=n);
	while(f * base <= n) f *= base;
	return f;
}
unsigned uint_rndpow_fac_ceil(unsigned f, unsigned base, unsigned n) {
	assert(base>0 && n>0 && f>0);
	while(f < n) f *= base;
	return f;
}

// Parse a string composed of multiple unsigned numbers with multipliers: "233G12k3".
// The last multiplier is optional.
// Return a double.
double ParseWithUnit(char *arg) {
	double val = 0;

	char* ptr = arg;
	do {
		if(!isdigit(*ptr)) break;
		double t = atoi(ptr);
		while(isdigit(*ptr)) ptr++;
		switch(*ptr) {
			case 'E' : val += t * 1e18;  break;
			case 'P' : val += t * 1e15;  break;
			case 'T' : val += t * 1e12;  break;
			case 'G' : val += t * 1e9;   break;
			case 'M' : val += t * 1e6;   break;
			case 'k' : val += t * 1e3;   break;
			case 'm' : val += t * 1e-3;  break;
			case 'u' : val += t * 1e-6;  break;
			case 'n' : val += t * 1e-9;  break;
			case 'p' : val += t * 1e-12; break;
			case 'f' : val += t * 1e-15; break;
			// If the last char is the nul char or any non-multiplier char, parsing is finished
			default : val += t; goto EXITPOINT;
		}
		ptr++;
	}while(1);

	EXITPOINT:

	// Display, for debug
	//dbgprintf("Read %g\n", val);

	return val;
}

// Replacing one character with another
char* str_replace_one_buf(char* str, char c, char c2) {
	static char* buf = NULL;
	static unsigned buf_size = 0;
	unsigned len = strlen(str) + 1;
	if(len > buf_size) {
		buf_size = buf_size + buf_size / 2 + len;
		buf = realloc(buf, buf_size);
	}
	strcpy(buf, str);
	for(char* ptr=buf; (*ptr)!=0; ptr++) if(*ptr==c) { *ptr = c2; break; }
	return buf;
}

// Parse a string composed of one floating-point number with an optional multiplier: "233.33M", "233.33e6"
// WARNING: Using inputs written using the syntax "<mantissa>E<exp>" is not advised.
// Errors are not detected.
double atof_multiplier(const char *arg) {
	char* exp = NULL;
	double val = strtod(arg, &exp);

	switch(*exp) {
		case 'E' : val *= 1e18;  break;
		case 'P' : val *= 1e15;  break;
		case 'T' : val *= 1e12;  break;
		case 'G' : val *= 1e9;   break;
		case 'M' : val *= 1e6;   break;
		case 'k' : val *= 1e3;   break;
		case 'm' : val *= 1e-3;  break;
		case 'u' : val *= 1e-6;  break;
		case 'n' : val *= 1e-9;  break;
		case 'p' : val *= 1e-12; break;
		case 'f' : val *= 1e-15; break;
	}

	// Display, for debug
	//dbgprintf("Read %g\n", val);

	return val;
}

ptype_list* ReadString_NameNum(const char* str) {
	ptype_list* list = NULL;

	if(str==NULL) return NULL;

	do {
		if(*str==0) break;

		// Read the name
		const char* name_begin = str;
		unsigned name_size = 0;
		do {
			if(isalnum(*str)!=0 || (*str)=='-' || (*str)=='_') { name_size++; str++; }
			else break;
		}while(1);
		if(name_size<1) { freeptype(list); return NULL; }
		char name_buffer[name_size+2];
		sprintf(name_buffer, "%.*s", name_size, name_begin);

		// Skip non-numeric characters
		do {
			if(*str==0) { freeptype(list); return NULL; }
			if(isdigit(*str)==0) str++; else break;
		}while(1);

		// Get the number
		int limit = strtol(str, (char**)&str, 0);

		// Add this element to the list
		list = addptype(list, limit, namealloc(name_buffer));

		// Next element
		if(*str==0) break;
		str++;

	}while(1);

	return list;
}

double TimeSpec_ToDouble(struct timespec *ts) {
	return (double)ts->tv_sec + ((double)ts->tv_nsec)/1.0e9;
}
double TimeSpec_GetDiff_Double(struct timespec *oldtime, struct timespec *newtime) {
	return (double)(newtime->tv_sec - oldtime->tv_sec) +
	       (double)(newtime->tv_nsec - oldtime->tv_nsec)/1.0e9;
}

double TimeSpec_DiffCurrReal_Double(struct timespec *oldtime) {
	struct timespec newtime;
	TimeSpec_GetReal(&newtime);
	return TimeSpec_GetDiff_Double(oldtime, &newtime);
}
double TimeSpec_DiffCurrCpu_Double(struct timespec *oldtime) {
	struct timespec newtime;
	TimeSpec_GetCpu(&newtime);
	return TimeSpec_GetDiff_Double(oldtime, &newtime);
}

// WARNING: The result is not allocated
// Info: for other operating systems, see there:
//   http://stackoverflow.com/questions/1023306/finding-current-executables-path-without-proc-self-exe
char* GetExePath(char* append) {
	static char* path = NULL;
	if(path!=NULL) return path;

	char exepath[PATH_MAX + 1];
	exepath[0] = 0;
	ssize_t z = readlink("/proc/self/exe", exepath, PATH_MAX);
	if(z == -1) return NULL;
	exepath[z] = 0;
	path = stralloc(exepath);

	return path;
}
char* GetExePath_CutBin() {
	static char* path = NULL;
	if(path!=NULL) return path;

	char exepath[PATH_MAX + 1];
	exepath[0] = 0;
	ssize_t z = readlink("/proc/self/exe", exepath, PATH_MAX);
	if(z == -1) return NULL;
	exepath[z] = 0;

	// Get the last character '/' and its position in the string
	bool found = false;
	for( ; z>=0; z--) if(exepath[z]=='/') { found = true; break; }
	if(found==false) return NULL;
	if(z<4) return NULL;
	z -= 4;
	if(strncmp(exepath + z, "/bin", 4)!=0) return NULL;
	exepath[z] = 0;
	path = stralloc(exepath);

	return path;
}

// Check that a name is a normal file or directory name
// Forbid "." ".."
// Forbid beginning by space characters
// Forbid unprintable characters
// Forbid characters \ / ' " \n \r \t
bool CheckDirName(char* name) {
	if(name[0]==0) return false;

	if(strcmp(name, ".")==0) return false;
	if(strcmp(name, "..")==0) return false;

	if(isspace(name[0])!=0) return false;

	char* ptr = name;
	do {
		char c = *ptr;
		if(c==0) break;
		if(isprint(c)==0) return false;
		if(c=='\'' || c=='"' || c=='/' || c=='\\' || c=='\n' || c=='\r' || c=='\t') return false;
		ptr++;
	} while(1);

	return true;
}
bool CheckDirName_Verbose(char* name) {
	bool b = CheckDirName(name);
	if(b==false) errprintf("String not accepted as name of file or directory: %s\n", name);
	return b;
}

void Implem_SymFlag_Add(implem_t* Implem, const char* name, int flag) {
	avl_pi_node_t* pi_node = NULL;
	bool b = avl_pi_add(&Implem->symbols_flags, name, &pi_node);
	if(b==true) pi_node->data = 0;
	pi_node->data |= flag;
}
void Implem_SymFlag_Rem(implem_t* Implem, const char* name, int flag) {
	avl_pi_node_t* pi_node = NULL;
	bool b = avl_pi_find(&Implem->symbols_flags, name, &pi_node);
	if(b==true) pi_node->data &= ~flag;
}
bool Implem_SymFlag_Chk(implem_t* Implem, const char* name, int flag) {
	avl_pi_node_t* pi_node = NULL;
	bool b = avl_pi_find(&Implem->symbols_flags, name, &pi_node);
	if(b==false) return false;
	if( (pi_node->data & flag) != 0) return true;
	return false;
}
void Implem_SymFlag_FPrint(FILE* F, implem_t* Implem, const char* name) {
	avl_pi_node_t* pi_node = NULL;
	bool b = avl_pi_find(&Implem->symbols_flags, name, &pi_node);
	if(b==false) fprintf(F, "(none)");
	else fprintf(F, "0x%08x", pi_node->data);
}



//======================================================================
// Standard synthesis flow
//======================================================================

// Function that performs an automatic synthesis
// With elaboration under limited resources
int augh_automatic(implem_t* Implem) {

	if(Implem->env.input_file==NULL || strcmp(Implem->env.input_file, "")==0) 	{
		fprintf(stderr, "Error: No input file is given.\n");
		return -1;
	}

	int z;

	// Load the source file
	z = Hier_Load(Implem, Implem->env.input_file);
	if(z!=0) {
		printf("ERROR %s:%d : Loading the design failed.\n", __FILE__, __LINE__);
		return -1;
	}

	#if 0  // For debug
	printf("\nDEBUG %s:%u : Hier after load\n\n", __FILE__, __LINE__);
	Hier_Print(Implem->H);
	printf("\n");
	#endif

	if(Implem->env.globals->optim_level==0) {

		avl_pp_foreach(&Implem->env.globals->implem_models, scan) {
			implem_t* modImplem = scan->data;
			if(modImplem->H==NULL) continue;
			Hier_Update_NoAct(modImplem);
		}

		goto MAPPING;
	}

	if(Implem->env.globals->verbose.level>VERBOSE_SILENT)
		printf("Initial simplification launched.\n");

	// A big simplification pass
	avl_pp_foreach(&Implem->env.globals->implem_models, scan) {
		implem_t* modImplem = scan->data;
		if(modImplem->H==NULL) continue;
		Hier_Update_All(modImplem);
	}

	#if 0  // For debug
	printf("\nDEBUG %s:%u : Hier after initial simplification\n\n", __FILE__, __LINE__);
	Hier_Print(Implem->H);
	printf("\n");
	#endif

	if(Implem->env.globals->optim_level==1) goto SCHED;
	else goto ELABO;


	SCHED:

	if(Implem->env.globals->verbose.level>VERBOSE_SILENT)
		printf("Scheduling launched.\n");

	avl_pp_foreach(&Implem->env.globals->implem_models, scan) {
		implem_t* modImplem = scan->data;
		if(modImplem->H==NULL) continue;

		z = Schedule(modImplem, SCHED_ALGO_LIST);
		if(z < 0) {
			printf("ERROR: Scheduling failed for top-level '%s'.\n", modImplem->compmod.model_name);
			return -1;
		}

		Hier_Timing_Update(modImplem, false);
	}

	goto MAPPING;


	ELABO:

	if(Implem->synth_target->clk_freq==0) {
		printf("Error : No target frequency is set.\n");
		return -1;
	}
	if(Implem->synth_target->resources==NULL) {
		printf("Error : No hardware resource limits are set.\n");
		return -1;
	}

	if(Implem->env.globals->verbose.level > VERBOSE_SILENT)
		printf("Elaboration launched.\n");

	// FIXME Missing DSE with several modImplems
	z = Core_Elaborate(Implem);
	if(z!=0) {
		printf("ERROR: The elaboration failed.\n");
		return -1;
	}


	MAPPING:

	if(Implem->env.globals->verbose.level > VERBOSE_SILENT)
		printf("Mapping launched.\n");

	avl_pp_foreach(&Implem->env.globals->implem_models, scan) {
		implem_t* modImplem = scan->data;
		augh_postprocess(modImplem);
	}

	#if 0  // For debug
	printf("\nDEBUG %s:%u : Hier after mapping\n\n", __FILE__, __LINE__);
	Hier_Print(Implem->H);
	printf("\n");
	#endif

	if(Implem->H!=NULL) {
		Hier_PrintDetails(Implem->H);
		Hier_Timing_Show(Implem);
		Hier_ClockCycles_Display(Implem);
	}

	// Techno eval: size, delay
	if(Implem->synth_target->techno!=NULL && Implem->env.globals->verbose.level > VERBOSE_SILENT) {
		ImpMod_Count_Update(Implem->env.globals);
		ImpMod_ComputeSizes(Implem->env.globals);

		printf("Technology evaluation:\n");

		avl_pp_foreach(&Implem->env.globals->implem_models, scan) {
			implem_t* modImplem = scan->data;
			printf("Implem model '%s':\n", modImplem->compmod.model_name);
			Techno_NameOpRes_Print(modImplem->CORE.resources_opers);
		}

		double latmax = 0;
		avl_pp_foreach(&Implem->env.globals->implem_models, scan) {
			implem_t* modImplem = scan->data;
			if(modImplem->H==NULL) continue;
			double loc_latmax = Techno_Eval_Latency_HierMax(modImplem);
			if(loc_latmax > latmax) latmax = loc_latmax;
		}

		printf("Estimation of the datapath worst latency: %g ns\n", latmax);
	}


	// Generation of synthesis project files

	if(Implem->env.globals->gen_vhdl==false) return 0;

	avl_pp_foreach(&Implem->env.globals->implem_models, scanModel) {
		implem_t* modImplem = scanModel->data;
		Map_Netlist_GenVHDL(modImplem);
	}


	// Generation of synthesis project files

	if(Implem->env.globals->gen_synth_proj==false) return 0;

	if(Implem->synth_target->techno==NULL) {
		printf("Warning: No technology is set, can't generate synthesis project files.\n");
		return 0;
	}
	if(Implem->synth_target->techno->synth_gen_prj==NULL) {
		printf("Warning: The technology '%s' has no function to generate synthesis project files.\n", Implem->synth_target->techno->name);
		return 0;
	}
	z = Implem->synth_target->techno->synth_gen_prj(Implem);
	if(z!=0) {
		printf("Error: Generation of synthesis project files failed.\n");
		return z;
	}


	// Launch back-end

	if(Implem->env.globals->stop_vhdl==true) return 0;

	if(Implem->synth_target->techno->synth_launch==NULL) {
		printf("Warning: The technology '%s' has no function to launch back-end synthesis.\n", Implem->synth_target->techno->name);
		return 0;
	}
	z = Implem->synth_target->techno->synth_launch(Implem);
	if(z!=0) {
		printf("Error: Back-end synthesis failed.\n");
		return z;
	}

	return 0;
}

// All that can be done after scheduling
void augh_postprocess(implem_t* Implem) {
	if(Implem->H==NULL) {

		if(Implem->env.globals->verbose.level>VERBOSE_SILENT)
			printf("Connecting system ports and signals...\n");

		Map_Netlist_CheckConnectSystemPorts(Implem);

		if(Implem->env.globals->verbose.level>VERBOSE_SILENT)
			printf("Simplifying Netlist...\n");

		Map_Netlist_Simplify(Implem);

		return;
	}

	// Only to know the execution time
	struct timespec oldtime;
	TimeSpec_GetReal(&oldtime);

	if(Implem->env.globals->verbose.level>VERBOSE_SILENT)
		printf("Extending components...\n");
	Map_Netlist_CompleteComponents(Implem);

	// After some transformations, some things can become unuseful...
	if(Implem->env.globals->verbose.level>VERBOSE_SILENT)
		printf("Removing unused components...\n");
	Map_Netlist_RemoveComponents(Implem);

	if(Implem->env.globals->verbose.level>VERBOSE_SILENT)
		printf("Mapping...\n");
	Map_Netlist(Implem);

	// Display the execution time
	double diff = TimeSpec_DiffCurrReal_Double(&oldtime);
	printf("Post-processing done in %g seconds.\n", diff);

	// Set the status flag
	Implem->synth_state.postproc = 1;
}



//======================================================================
// Duplicating an Implem structure
//======================================================================

void Implem_Dup_LinkPointers(implem_t* newImplem, void* oldptr, void* newptr) {
	avl_pp_add_keep(&newImplem->DUP.pointers_old2new, oldptr, newptr);
	avl_pp_add_keep(&newImplem->DUP.pointers_new2old, newptr, oldptr);
}
void* Implem_Dup_New2Old(implem_t* newImplem, void* newptr) {
	void* oldptr = NULL;
	avl_pp_find_data(&newImplem->DUP.pointers_new2old, newptr, &oldptr);
	return oldptr;
}
void* Implem_Dup_Old2New(implem_t* newImplem, void* oldptr) {
	void* newptr = NULL;
	avl_pp_find_data(&newImplem->DUP.pointers_old2new, oldptr, &newptr);
	return newptr;
}

// What this function does NOT set:
// - the newImplem is not linked inside the new_globals
// - the tree compmod->tree_comp2mod is NOT filled
implem_t* Implem_Dup_internal(implem_t* Implem, augh_globals_t* new_globals) {
	implem_t* newImplem = augh_implem_new(new_globals);

	// The env flags
	newImplem->env.cflags      = dupchain(Implem->env.cflags);
	newImplem->env.input_file  = Implem->env.input_file;
	newImplem->env.entity      = Implem->env.entity;
	newImplem->env.vhdl_dir    = Implem->env.vhdl_dir;
	newImplem->env.vhdl_prefix = Implem->env.vhdl_prefix;
	newImplem->env.wait_on_start = Implem->env.wait_on_start;

	// Initialize the structure linking the new and old Implem
	newImplem->DUP.father = Implem;
	avl_p_add_keep(&Implem->DUP.children, newImplem, newImplem);

	// Copy the miscellaneous fields
	newImplem->time = Implem->time;
	newImplem->synth_state = Implem->synth_state;

	// Duplicate the resources for the Schedule process
	if(Implem->SCHED.hwres_available!=NULL) newImplem->SCHED.hwres_available = hwres_heap_dup(Implem->SCHED.hwres_available);

	// Build a new Hierarchy in the new Implem
	Hier_Dup(newImplem);

	// Duplicate the netlist
	Map_Netlist_Dup(newImplem);

	avl_pi_dup(&newImplem->symbols_flags, &Implem->symbols_flags);

	// Update the control of the Hier nodes
	// FOR loop structures, SWITCH, etc
	Hier_Update_Control(newImplem);

	// Simply link to the original synth_target structure
	newImplem->synth_target = Implem->synth_target;

	// The imformation about component model
	newImplem->compmod.model_name = Implem->compmod.model_name;
	newImplem->compmod.instance_nb = Implem->compmod.instance_nb;
	newImplem->compmod.time_weight = Implem->compmod.time_weight;

	return newImplem;
}

void Globals_Dup_LinkPointers(augh_globals_t* newGlobals, void* oldptr, void* newptr) {
	avl_pp_add_keep(&newGlobals->dup.pointers_old2new, oldptr, newptr);
	avl_pp_add_keep(&newGlobals->dup.pointers_new2old, newptr, oldptr);
}
void* Globals_Dup_New2Old(augh_globals_t* newGlobals, void* newptr) {
	void* oldptr = NULL;
	avl_pp_find_data(&newGlobals->dup.pointers_new2old, newptr, &oldptr);
	return oldptr;
}
void* Globals_Dup_Old2New(augh_globals_t* newGlobals, void* oldptr) {
	void* newptr = NULL;
	avl_pp_find_data(&newGlobals->dup.pointers_old2new, oldptr, &newptr);
	return newptr;
}

augh_globals_t* Globals_Dup(augh_globals_t* globals) {
	augh_globals_t* new_globals = augh_globals_new();

	// Copy the fields
	new_globals->time_real = globals->time_real;
	new_globals->time_cpu  = globals->time_cpu;
	new_globals->verbose   = globals->verbose;
	new_globals->optim_level = globals->optim_level;

	// Duplicate the Implem models
	avl_pp_foreach(&globals->implem_models, scan) {
		implem_t* Implem = scan->data;
		implem_t* newImplem = Implem_Dup_internal(Implem, new_globals);
		avl_pp_add_overwrite(&new_globals->implem_models, newImplem->compmod.model_name, newImplem);
		if(Implem==globals->top_implem) new_globals->top_implem = newImplem;
		Globals_Dup_LinkPointers(new_globals, Implem, newImplem);
	}

	return new_globals;
}



//======================================================================
// Initialization of all modules of the AUGH lib
//======================================================================

// These headers are needed for initialization of all modules from Alliance
#include <mut.h>
#include <aut.h>
#include <autenv.h>

#include "../../../config.h"

#include "auto.h"
#include "techno.h"
#include "command.h"
#include "forksynth.h"
#include "../plugins/plugins.h"

void aughlib_init() {

	mbkenv();
	autenv();

	AUT_EXIT_CORE = 1;

	Plugin_Init();
	Netlist_Init();
	Hier_Build_Init();
	AughTechno_Init();
	ForkSynth_Init();
	AughCmd_Init();
	hvex_init();

	// Must be used after hvex_init
	Map_Init_Hvex_Callbacks();

}

void aughlib_clear() {
	nameclearall();
}



