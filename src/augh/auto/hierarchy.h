
#ifndef _HIERARCHY_H_
#define _HIERARCHY_H_

#include <stdint.h>

#include "../chain.h"
#include "../indent.h"
#include "auto.h"
#include "core.h"



//======================================================================
// Declarations of constants
//======================================================================

#define HIER_ACT_BEG  0x001  // Must schedule at the first cycle of the BB
#define HIER_ACT_END  0x002  // Must schedule at the final cycle of the BB

#define HIER_ACT_TEST  0x040  // The destination (a Signal) is used in a test

// The Action assigns an intermediate Signal
// The Action is created/duplicated as a dependency for other Actions
// It has no timed dependency or anything, It can depend on other inlined Actions.
// FIXME DO NOT USE UNTIL FULLY INTEGRATED
#define HIER_ACT_INL  0x008

// To prevent assignment propagation to/from the given Action
// This gives some user control on specific datapaths
#define HIER_ACT_NOPROPAG_FROM  0x010
#define HIER_ACT_NOPROPAG_TO    0x020



//======================================================================
// Structures and functions working with execution time
//======================================================================

#include "../minmax.h"

typedef struct {
	minmax_t once;
	minmax_t global;
	double factor;
} time_casefactor_t;

static inline time_casefactor_t* time_casefactor_reset(time_casefactor_t *dest) {
	minmax_set_cst(&dest->once, 0);
	minmax_set_cst(&dest->global, 0);
	dest->factor = 0;
	return dest;
}

static inline time_casefactor_t* time_casefactor_update(time_casefactor_t *dest) {
	minmax_set_mult(&dest->global, &dest->once, dest->factor);
	return dest;
}

static inline time_casefactor_t* time_casefactor_set_once_cst(time_casefactor_t *dest, double d) {
	minmax_set_cst(&dest->once, d);
	time_casefactor_update(dest);
	return dest;
}

static inline time_casefactor_t* time_casefactor_set_factor(time_casefactor_t *dest, double factor) {
	dest->factor = factor;
	time_casefactor_update(dest);
	return dest;
}

static inline time_casefactor_t* time_casefactor_set_once(time_casefactor_t *dest, minmax_t *src) {
	dest->once = *src;
	time_casefactor_update(dest);
	return dest;
}

static inline time_casefactor_t* time_casefactor_add_once(time_casefactor_t *dest, minmax_t *src) {
	minmax_add(&dest->once, src);
	time_casefactor_update(dest);
	return dest;
}



//======================================================================
// Declarations of data structures
//======================================================================


// This structure type contains all the hierarchy elements
typedef struct hier_t             hier_t;

// This structure type describes one hierarchy level/tree
typedef struct hier_node_t        hier_node_t;

typedef struct hier_proc_t        hier_proc_t;

typedef struct hier_bb_t          hier_bb_t;
typedef struct hier_state_t       hier_state_t;

typedef struct hier_loop_t        hier_loop_t;
typedef struct hier_switch_t      hier_switch_t;
typedef struct hier_case_t        hier_case_t;

typedef struct hier_jump_t        hier_jump_t;
typedef struct hier_label_t       hier_label_t;

// An Action, or assignment
typedef struct hier_action_t      hier_action_t;

#include "schedule.h"



//======================================================================
// Things to ease building usual Hier constructs
//======================================================================

// Building a template basic block

typedef struct hier_build_bbstact_t {
	hier_node_t*   bb;
	hier_node_t*   state;
	hier_action_t* action;
} hier_build_bbstact_t;

hier_node_t* Hier_Build_BBFromState(hier_node_t* nodeState);
hier_node_t* Hier_Build_StAct(hier_build_bbstact_t* bbstact);
hier_node_t* Hier_Build_BBState(hier_build_bbstact_t* bbstact);
hier_node_t* Hier_Build_BBStAct(hier_build_bbstact_t* bbstact);

// Building a template FOR loop

typedef struct hier_build_for_t {
	hier_node_t*         loop;
	hier_build_bbstact_t init;
	hier_build_bbstact_t test;
	hier_build_bbstact_t inc;
} hier_build_for_t;

hier_node_t* Hier_Build_For_raw(
	hier_build_for_t* forloop,
	char* name_reg, char* name_sig,
	unsigned beg, unsigned iter_nb, unsigned step,
	unsigned width_reg
);
hier_node_t* Hier_Build_For_onecycle(
	hier_build_for_t* forloop,
	char* name_reg, char* name_sig,
	unsigned beg, unsigned iter_nb, unsigned step,
	unsigned width_reg
);



//======================================================================
// Misc definitions
//======================================================================

// Common structure that describes a test
typedef struct hier_testinfo_t  hier_testinfo_t;
struct hier_testinfo_t{
	hvex_t*        cond;
	// Data only for the 'simple' form: the VEX cond is "test" or "not(test)" and test is a VEX_ATOM
	bool           is_simple;
	bool           cond_is_literal;    // If literal, there is no symbol name.
	long           lit_value;
	bool           oper_not;
	char*          sym_name;
	bool           sym_is_sig;
	bool           sym_is_port;
	hier_action_t* sig_asg;
	bool           asg_is_literal;   // If the Action is : signal = integer
};



//======================================================================
// The nodes of the hierarchical internal representation
//======================================================================

// This identifies the type of a node
typedef enum hier_type_t {
	HIERARCHY_PROCESS   = 0,

	HIERARCHY_BB        = 1,
	HIERARCHY_STATE     = 2,

	HIERARCHY_LOOP      = 3,
	HIERARCHY_SWITCH    = 4,
	HIERARCHY_CASE      = 5,

	HIERARCHY_JUMP      = 6,   // Break, continue or goto
	HIERARCHY_LABEL     = 7,   // The destination of a JUMP (or a simple marker)
	HIERARCHY_CALL      = 8,   // Function call
	HIERARCHY_RETURN    = 9,   // Return of Process
} hier_type_t;

#define HIERARCHY_TYPES_NB  10

// Phony ID for specific HierID purposes
// Important: it must be different from the hierarchy node IDs
#define HIERARCHY_LEVEL 100


// This identifies a child inside a node.
typedef enum hier_child_id_t {
	HIER_CHILD_NONE       = 0,

	HIER_CHILD_LOOP_AFTER  = 1,
	HIER_CHILD_LOOP_BEFORE = 2,

	HIER_CHILD_BB_BODY    = 3,

	HIER_CHILD_SW_CASE    = 4,

	HIER_CHILD_CASE_BODY  = 5,
} hier_child_id_t;


struct hier_action_t {
	// The parent node, always STATE type, representing a clock cycle.
	hier_node_t* node;

	// This HVEX expression includes:
	// the destination, the source and the wired condition (if any).
	hvex_t* expr;

	// Some flags to indicate the scheduling time
	unsigned flags;

	// Timed dependencies with other Actions (possibly from other State nodes)
	// Key = target Action, Data = time value (in clock cycles)
	// Example: if data is -2 it means the target Action must be scheduled 2 cycles BEFORE
	// Only with non-inline Actions!
	avl_pi_tree_t deps_timed;

	// Dependencies on inlined Actions
	chain_list* acts_inline;

	// Timing information
	double   delay;
	unsigned nb_clk;

	// Timing information base on mapper back-annotation
	// FIXME Not used yet, the mapper doesn't extract per-Action delays
	double map_delay;

	// Can be NULL if lots of transformations have been made.
	bitype_list* source_lines;
};

struct hier_state_t {
	struct {
		double        delay;
		unsigned      nb_clk;
		double        map_delay;
		unsigned      map_nb_clk;
	} time;
	chain_list* actions;
};

typedef enum hier_jump_type_t {
	HIER_JUMP_NONE   = 0,  // Used by AUGH for special optimizations before mapping
	HIER_JUMP_GOTO   = 1,
	HIER_JUMP_BREAK  = 2,
	HIER_JUMP_CONT   = 3,
	HIER_JUMP_CALL   = 4,
	HIER_JUMP_RET    = 5,  // The target is the PROCESS node of the current function
} hier_jump_type_t;

// Structure used for node types JUMP, CALL, RETURN
struct hier_jump_t {
	// Details about the destination (only for JUMP node type)
	hier_jump_type_t type;
	// Target is type PROCESS for the CALL and RETURN types
	// Target is type LABEL for JUMP type
	hier_node_t* target;
};

struct hier_label_t {
	// An optional name given by the user in the app code
	char* name;
	// The list of all JUMP nodes that lead here (can be empty)
	chain_list* jumps;
};

// The beginning of a PROCESS (also functions)
// The body is actually after: use the NEXT field
struct hier_proc_t {
	// Name as given in the user code
	char* name_orig;

	// The list of all CALL nodes that lead here
	chain_list* calls;
	// The list of all RETURN nodes of the current function
	chain_list* returns;

	// Data used only during build of the Hier
	struct {
		chain_list* list_args;  // Stored data: hvex_t* (only type VECTOR)
		hvex_t* vex_ret;
	} build;

	// WARNING In the Hier nodes, there is no list of arguments
	// Assignment of the arguments is assumed to be done by the parser
	// With this we can create a new transformation: inline of function

	struct {
		bool only_declared;
		// Annotations
		bool is_toplevel;
		bool no_autoinline;
	} user;
};

// This structure describes one basic block
struct hier_bb_t {
	// This child hier level is assumed to contain ONLY nodes of type STATE
	hier_node_t* body;

	// Scheduler data
	struct {
		schedule_t* data;
	} sched;

	// Previous and next BBs
	// Useful to build register usage
	chain_list* prev_bbs;
	chain_list* next_bbs;
};

// This structure describes a generic loop node.
// It can represent the loop types FOR, DO, WHILE, and infinite loop.
struct hier_loop_t {
	// The children

	// This is the body executed right after entry in the loop
	hier_node_t *body_after;
	// This is the body executed after the test (it's connected BEFORE the entry in the loop)
	hier_node_t *body_before;

	// All flags about iteration and structure of traditional 'for' loop
	unsigned flags;
	// The label that was given in the C source file
	char* label_name;

	// Branch condition
	// The loop iterates as long as the VEX condition is '1'
	hier_testinfo_t* testinfo;

	// Fields used for unroll & parallelization (FOR loop type)
	struct {
		int error_code;

		// Must be found.
		char*          iterator_name;
		hvex_t*        vex_iterator;

		// The initialization Action and value, if found.
		// Can be NULL if the Action is not found (computed in a previous IF, etc).
		hier_action_t* init_act;
		int64_t        val_init;

		// The end expression can be a non-literal.
		hvex_t*        vex_end;
		int64_t        val_end;

		// Here the Action and value must be found.
		hier_action_t* inc_act;     // The increment ACT
		int64_t        val_step;    // Must be a literal

		// Number of iterations, when known
		int64_t        iterations_nb;  // Point of view of the main loop body
		int64_t        testfalse_nb;   // The number of times the exit test is false
		int64_t        inc_nb;         // The number of times the iterator is incremented

	} iter;

	struct {
		bool     iter_nb_given;
		double   iter_nb;
		bool     iter_nb_power2_given;
		unsigned iter_nb_power2;
		bool     parallel_given;
	} user;

	// Parameters for the elaboration core

	struct {
		int full_unroll_impossible;
		int part_unroll_impossible;
	} core;

};

#define HIER_LOOP_NEVER      0x0001
#define HIER_LOOP_ALWAYS     0x0002

#define HIER_FOR_OK          0x0010
#define HIER_FOR_INIT_LIT    0x0020
#define HIER_FOR_INC_LIT     0x0040
#define HIER_FOR_END_LIT     0x0080
#define HIER_FOR_ITER_NB     0x0100
#define HIER_FOR_TESTFIRST   0x0200  // Test before write of INC Action (same State)
#define HIER_FOR_ONEBODY     0x0400
// Indicates the loop has dubious gotos/labels/calls/returns inside
#define HIER_FOR_HASJUMPS    0x0800
#define HIER_FOR_OVERFLOW    0x1000

#define hier_loop_chkflag(i, f) (((i) & (f)) != 0)
#define hier_loop_chkflags_or(i, f)  (((i) & (f)) != 0)
#define hier_loop_chkflags_and(i, f) (((i) & (f)) == (f))

#define hier_loop_chknever(i)    hier_loop_chkflag(i, HIER_LOOP_NEVER)
#define hier_loop_chkalways(i)   hier_loop_chkflag(i, HIER_LOOP_ALWAYS)
#define hier_for_chkok(i)        hier_loop_chkflag(i, HIER_FOR_OK)
#define hier_for_chkinitlit(i)   hier_loop_chkflag(i, HIER_FOR_INIT_LIT)
#define hier_for_chkinclit(i)    hier_loop_chkflag(i, HIER_FOR_INC_LIT)
#define hier_for_chkendlit(i)    hier_loop_chkflag(i, HIER_FOR_END_LIT)
#define hier_for_chkiternb(i)    hier_loop_chkflag(i, HIER_FOR_ITER_NB)
#define hier_for_chktestfirst(i) hier_loop_chkflag(i, HIER_FOR_TESTFIRST)
#define hier_for_chkonebody(i)   hier_loop_chkflag(i, HIER_FOR_ONEBODY)
#define hier_for_chkoverflow(i)  hier_loop_chkflag(i, HIER_FOR_OVERFLOW)

// This structure describes a CASE node
struct hier_case_t {
	struct {
		hier_node_t* case_before;
		hier_node_t* case_after;
	} build;

	hier_node_t* body;

	int is_default;

	// The Case is taken if the VEX condition is '1'
	// This tree contains pointers to hier_testinfo_t*
	avl_p_tree_t values;

	// User annotations
	struct {
		bool never_taken;  // FIXME Not used yet
		bool wired_wanted;  // FIXME Not used yet
		bool branch_prob_given;
		double branch_prob;
	} user;
};

// This structure describes a SWITCH node. Includes the IF behaviour.
// Note: the list of child Cases is meant be exhaustive.
//   In particular, if there is no default Case, then it means one of the listed Cases is guarenteed to be taken.
//   This specification eases graph simplification.
struct hier_switch_t {
	// Note : The different CASEs are in the list CHILDREN of the node structure.

	// The label that was given in the C source file
	char* label_name;

	struct {
		int             was_if;
		// Useful field for build, could be used later but take care to coherence after transformations.
		hvex_t*         switch_test;
		hier_node_t*    default_case;
	} info;

	// Parameters for the elaboration core
	struct {
		int wire_impossible;
	} core;
};


// The generic hierarchy node
struct hier_node_t {
	hier_type_t       TYPE;

	union {
		hier_proc_t     PROCESS;
		hier_bb_t       BB;
		hier_state_t    STATE;
		hier_loop_t     LOOP;
		hier_switch_t   SWITCH;
		hier_case_t     CASE;
		hier_jump_t     JUMP;
		hier_label_t    LABEL;
	} NODE;

	// Analysis of symbol usage (only registers)
	// Key = symbol name, data = a bb_symuse_t* structure pointer
	// FIXME having this tree in this node is an overhead for init/clear for too many nodes
	//   Consider storing this elsewhere ?
	avl_pp_tree_t symuse;

	bitype_list* source_lines;

	// Execution time, in clock cycles
	time_casefactor_t timing;

	hier_node_t*      PARENT;
	chain_list*       CHILDREN;

	// Previous and next executed nodes.
	// Also used as linked list for allocation/free.
	hier_node_t*      PREV;
	hier_node_t*      NEXT;
};


// The structure that contains all the hierarchy
struct hier_t {
	// The entry in the algorithm
	hier_node_t* PROCESS;
	// All nodes
	avl_p_tree_t NODES_ALL;
	// These trees are handy to search/process some node types
	avl_p_tree_t NODES[HIERARCHY_TYPES_NB];
};



//======================================================================
// Shared variables
//======================================================================

extern bool asgpropag_symuse_en;
extern bool actsimp_verbose;
extern bool nodesimp_verbose;


//======================================================================
// Declarations of functions
//======================================================================

// Init

void Hier_Build_Init();

// Allocation and Construction

int Hier_Load(implem_t* Implem, const char* name);

hier_t* Hier_New(void);
void    Hier_Free(hier_t* H);

void hier_node_del(hier_node_t* elt);
hier_node_t* Hier_Node_NewType(hier_type_t type);

void Hier_DumpAllHier(hier_t* H);
void Hier_DumpNodeData(hier_node_t* node);
void Hier_FreeNode(hier_node_t* node);
void Hier_FreeNodeLevel(hier_node_t* node);

hier_testinfo_t* Hier_TestInfo_New();
void Hier_TestInfo_Free(hier_testinfo_t* caseval);
void Hier_TestInfo_FreeFull(hier_testinfo_t* caseval);

void Hier_TestInfo_ResetResults(hier_testinfo_t* testinfo);
void Hier_TestInfo_Clear(hier_testinfo_t* testinfo);


// Scheduler data

void Hier_NodeBB_ClearSched(hier_node_t* node);
void Hier_ClearSched(hier_t* H);


// Actions

hier_action_t* Hier_Action_New(void);
void           Hier_Action_Free(hier_action_t* action);

void Hier_Action_FreeFull(hier_action_t* action);
void Hier_Action_Unlink(hier_action_t* action);
void Hier_CutLinkBefore(hier_node_t* node);
void Hier_CutLinkAfter(hier_node_t* node);
void Hier_Action_Link(hier_node_t* node, hier_action_t* action);

static inline void HierAct_FlagSet(hier_action_t* action, unsigned flag) {
	action->flags |= flag;
}
static inline void HierAct_FlagClear(hier_action_t* action, unsigned flag) {
	action->flags &= ~flag;
}
static inline bool HierAct_FlagCheck(hier_action_t* action, unsigned flag) {
	if( (action->flags & flag) != 0 ) return true;
	return false;
}


// Misc Node and Level manipulation

void Hier_Lists_AddNode(hier_t* H, hier_node_t* node);
void Hier_Lists_AddLevel(hier_t* H, hier_node_t* node);
void Hier_Lists_RemoveNode(hier_t* H, hier_node_t* node);
void Hier_Lists_RemoveLevel(hier_t* H, hier_node_t* node);

void Hier_ReplaceNode(hier_node_t* oldnode, hier_node_t* newnode);
void Hier_ReplaceChild(hier_node_t* parent, hier_node_t* oldnode, hier_node_t* newnode);
void Hier_UnlinkNode(hier_node_t* node);
void Hier_ReplaceNode_Free(hier_t* H, hier_node_t* oldnode, hier_node_t* newnode);
void Hier_ReplaceLevel_Free(hier_t* H, hier_node_t* oldnode, hier_node_t* newnode);
void Hier_RemoveLevel_Free(hier_t* H, hier_node_t* oldnode);

static inline void Hier_Nodes_Link(hier_node_t* node_prev, hier_node_t* node_next) {
	node_prev->NEXT = node_next;
	node_next->PREV = node_prev;
}

static inline void Hier_JumpLabel_Link(hier_node_t* nodeJump, hier_node_t* nodeLabel) {
	hier_jump_t* jump_data = &nodeJump->NODE.JUMP;
	hier_label_t* label_data = &nodeLabel->NODE.LABEL;
	jump_data->target = nodeLabel;
	label_data->jumps = addchain(label_data->jumps, nodeJump);
}
static inline void Hier_CallProc_Link(hier_node_t* nodeCall, hier_node_t* nodeProc) {
	hier_jump_t* call_data = &nodeCall->NODE.JUMP;
	hier_proc_t* proc_data = &nodeProc->NODE.PROCESS;
	call_data->target = nodeProc;
	proc_data->calls = addchain(proc_data->calls, nodeCall);
}
static inline void Hier_RetProc_Link(hier_node_t* nodeRet, hier_node_t* nodeProc) {
	hier_jump_t* ret_data = &nodeRet->NODE.JUMP;
	hier_proc_t* proc_data = &nodeProc->NODE.PROCESS;
	ret_data->target = nodeProc;
	proc_data->returns = addchain(proc_data->returns, nodeRet);
}

char* Hier_NodeType2str(hier_type_t type);
int Hier_CheckAccessToNodes(hier_t* H);

hier_node_t *Hier_GetParentNode(hier_node_t *H);
hier_node_t* Hier_GetTopNode(hier_node_t *H);

void Hier_InsertLevelAfterNode(hier_node_t* oldnode, hier_node_t* newnode);
void Hier_InsertLevelAfterLevel(hier_node_t* node, hier_node_t* newnode);
void Hier_InsertLevelBeforeNode(hier_node_t* node, hier_node_t* newnode);

void Hier_ReplaceState_SplitBB(hier_t* H, hier_node_t* nodeState, hier_node_t* newLevel);
void Hier_InsertLevelBeforeNode_SplitBB(hier_t* H, hier_node_t* nodeState, hier_node_t* newLevel);
void Hier_InsertLevelAfterNode_SplitBB(hier_t* H, hier_node_t* nodeState, hier_node_t* newLevel);

int Hier_TestInfo_GetSignal(implem_t* Implem, hier_testinfo_t* testinfo);
int Hier_TestInfo_Simple(implem_t* Implem, hier_testinfo_t* testinfo, hier_node_t* node);
int Hier_TestInfo_Loop(implem_t* Implem, hier_node_t* node);
int Hier_TestInfo_GetVal(hier_testinfo_t* testinfo);

hier_action_t* Hier_GetAsg_BeforeNode(hier_t* H, hier_node_t* node, char* name);

bool Hier_IsLevelOnlyType(hier_node_t* node, hier_type_t type);
hier_node_t* Hier_IsLevelOneType_GetSkipLabels(hier_node_t* node, hier_type_t type);
bool Hier_Level_IsEmpty_SkipLabels(hier_node_t* node);
bool Hier_Level_HasDubiousJumpLabel_Recurs(hier_node_t* node);
void Hier_Level_UpdateJumpTarget_Recurs(hier_node_t* node);

hier_node_t* Hier_GetLabelUnique_Verbose(hier_t* H, const char* name);

bool Hier_Level_IsThereTypeInside(hier_node_t* node, hier_type_t type);
bool Hier_Node_IsThereTypeInside(hier_node_t* node, hier_type_t type);

chain_list* Hier_Level_ListInsideType(hier_node_t* node, hier_type_t type);
chain_list* Hier_Node_ListInsideType(hier_node_t* node, hier_type_t type);

unsigned Hier_Level_GetNbOfNodes(hier_node_t* node);

void Hier_Check_Integrity(hier_t* H);
void Implem_Vex_CheckSymbolWidthes(implem_t* Implem, hvex_t* vex);
void Implem_CheckIntegrity(implem_t* Implem);

hier_action_t* Hier_Action_GetFromVex(hvex_t* Expr);
void Hier_Action_SetExprFather(hier_action_t* action);
void Hier_AllActions_SetExprFather(hier_t* H);
void Hier_Level_SetExprFather(hier_node_t* node);

chain_list* Hier_ListActions_AddInline(chain_list* list_actions);

chain_list* Hier_BB_GetAllAct(hier_node_t* node);

void Hier_ListNextBBofBB(hier_t* H);


// Manipulation of Processes

hier_node_t* Hier_GetProc(hier_t* H, const char* name);
void Hier_RemoveProc(hier_t* H, hier_node_t* nodeProc);

void Hier_FlagNodesUsed(hier_node_t* fromNode, avl_p_tree_t* tree_nodes);

unsigned Hier_RemoveUnusedNodes(hier_t* H);


// Simplification

int Hier_NodeLocal_SimplifyExpr(hier_node_t* node);
int Hier_Level_SimplifyExpr(hier_node_t* node);
int Hier_SimplifyExpr(hier_t* H);

int Hier_RemoveUnusedSym_OnePass(implem_t* Implem);
int Hier_Simp_Actions(implem_t* Implem);
int Hier_Simp_Nodes(implem_t* Implem);

void Hier_Update_Control(implem_t* Implem);
void Hier_Update_SimpStruct(implem_t* Implem);
int  Hier_Update_All(implem_t* Implem);
void Hier_Update_NoAct(implem_t* Implem);

// Simplify label nodes
int Hier_Simp_RemoveUnusedLabels_opt(implem_t* Implem, const char* name, bool only_unnamed);

static inline int Hier_Simp_RemoveUnusedLabels(implem_t* Implem) {
	return Hier_Simp_RemoveUnusedLabels_opt(Implem, NULL, false);
}
static inline int Hier_Simp_RemoveUnusedLabels_Name(implem_t* Implem, const char* name) {
	return Hier_Simp_RemoveUnusedLabels_opt(Implem, name, false);
}
static inline int Hier_Simp_RemoveUnusedLabels_Unnamed(implem_t* Implem) {
	return Hier_Simp_RemoveUnusedLabels_opt(Implem, NULL, true);
}

// Duplicaton of nodes

hier_action_t* Hier_Action_Dup(hier_action_t* action);
hier_node_t* Hier_Node_Dup_State(hier_node_t* node);

hier_node_t* Hier_Node_Dup(hier_node_t* node);
hier_node_t* Hier_Level_Dup(hier_node_t* node);
hier_node_t* Hier_Level_Dup_link(hier_node_t* node, avl_pp_tree_t* tree_old2new);
hier_node_t* Hier_Level_Dup_stopnode(hier_node_t* node, hier_node_t* stopNode);

hier_node_t* Hier_Dup_ProcBody_link(hier_t* H, hier_node_t* nodeProc, avl_pp_tree_t* tree_old2new);

void Hier_Dup(implem_t* newImplem);


// Exploration

hier_node_t* Hier_Level_GetLastNode(hier_node_t* node);

chain_list* Hier_Node_AddAct_SymIsDest(chain_list* list, hier_node_t* node, char* name);
chain_list* Hier_Level_AddAct_SymIsDest(chain_list* list, hier_node_t* node, char* name);
static inline chain_list* Hier_Node_GetAct_SymIsDest(hier_node_t* node, char* name) { return Hier_Node_AddAct_SymIsDest(NULL, node, name); }
static inline chain_list* Hier_Level_GetAct_SymIsDest(hier_node_t* node, char* name) { return Hier_Level_AddAct_SymIsDest(NULL, node, name); }
chain_list* Hier_GetAct_SymIsDest(hier_t* H, char* name);

chain_list* Hier_Node_AddSymbolOccur_opt(chain_list* list, hier_node_t* node, char* name, bool doLevel, bool doRecurs);
static inline chain_list* Hier_Node_GetSymbolOccur(hier_node_t* node, char* name) { return Hier_Node_AddSymbolOccur_opt(NULL, node, name, false, true); }
static inline chain_list* Hier_Level_GetSymbolOccur(hier_node_t* node, char* name) { return Hier_Node_AddSymbolOccur_opt(NULL, node, name, true, true); }
chain_list* Hier_GetSymbolOccur(hier_t* H, char* name);

chain_list* Hier_AddArrayOccur_opt(chain_list* list, hier_node_t* node, char* name, bool doLevel, bool doRecurs);
static inline chain_list* Hier_Node_GetArrayOccur(hier_node_t* node, char* name) { return Hier_AddArrayOccur_opt(NULL, node, name, false, true); }
static inline chain_list* Hier_Level_GetArrayOccur(hier_node_t* node, char* name) { return Hier_AddArrayOccur_opt(NULL, node, name, true, true); }
chain_list* Hier_GetArrayOccur(hier_t* H, char* name);

void Hier_Node_GetSymbols_tree(hier_node_t* node, avl_p_tree_t* tree_read, avl_p_tree_t* tree_write);
void Hier_GetSymbols_tree(hier_t* H, avl_p_tree_t* tree_read, avl_p_tree_t* tree_write);

void Hier_Node_GetSymbols_treevex(hier_node_t* node, avl_pp_tree_t* tree_read, avl_pp_tree_t* tree_write);
void Hier_GetSymbols_treevex(hier_t* H, avl_pp_tree_t* tree_read, avl_pp_tree_t* tree_write);

void Hier_AllSymbols_GetVexOccur_treelist(hier_t* H, avl_pp_tree_t* tree);

chain_list* Hier_GetNodes_MatchOneLine_list(chain_list* list, char* name, int line);
chain_list* Hier_GetNodes_MatchOneLine_tree(avl_p_tree_t* tree, char* name, int line);
chain_list* Hier_GetNodes_MatchLines_list(chain_list* list, bitype_list* lines);
chain_list* Hier_GetNodes_MatchLines_tree(avl_p_tree_t* tree, bitype_list* lines);

void Hier_Nodes_SetLines(hier_node_t* node, bitype_list* lines, bool do_recurs, bool do_level);

ptype_list*  HierID_Get(hier_node_t *node);
hier_node_t* HierID_GetNode(hier_node_t* node, ptype_list* list);
void         HierID_Free(ptype_list* list);

void Hier_State_MergeSameSymAsg(hier_node_t* nodeState);
void Hier_MergeSameSymAsg(hier_t* H);


// Display

#define HIER_PRINT_NOACT     0x0001
#define HIER_PRINT_NOBBBODY  0x0002
#define HIER_PRINT_TIME      0x0004
#define HIER_PRINT_DEBUG     0x1000

void HierAct_FPrint(FILE* F, hier_action_t* action);
void HierAct_Print(hier_action_t* action);
void HierAct_DebugPrint(hier_action_t* action);

void Hier_PrintAct_raw(FILE* F, hier_action_t* action, unsigned level, unsigned flags);
void Hier_PrintNode_raw(FILE* F, hier_node_t *node, unsigned level, unsigned flags);
void Hier_PrintLevel_raw(FILE* F, hier_node_t *node, unsigned level, unsigned flags);
void Hier_Print_raw(FILE* F, hier_t *H, unsigned flags);

static inline void Hier_PrintNode_f(FILE* F, hier_node_t *node)  { Hier_PrintNode_raw(F, node, 0, 0); }
static inline void Hier_PrintNode(hier_node_t *node) 	           { Hier_PrintNode_f(stdout, node); }
static inline void Hier_PrintLevel_f(FILE* F, hier_node_t *node) { Hier_PrintLevel_raw(F, node, 0, 0); }
static inline void Hier_PrintLevel(hier_node_t *node)            { Hier_PrintLevel_f(stdout, node); }
static inline void Hier_Print_f(FILE* F, hier_t *H)              { Hier_Print_raw(F, H, 0); }
static inline void Hier_Print(hier_t *H)                         { Hier_Print_raw(stdout, H, 0); }

static inline void Hier_PrintNode_dbg(hier_node_t *node)  { Hier_PrintNode_raw(stdout, node, 0, HIER_PRINT_DEBUG); }
static inline void Hier_PrintLevel_dbg(hier_node_t *node) { Hier_PrintLevel_raw(stdout, node, 0, HIER_PRINT_DEBUG); }
static inline void Hier_Print_dbg(hier_t *H)              { Hier_Print_raw(stdout, H, HIER_PRINT_DEBUG); }

static inline void Hier_PrintNodeSkel(hier_node_t *node) { Hier_PrintNode_raw(stdout, node, 0, HIER_PRINT_NOACT | HIER_PRINT_NOBBBODY); }
static inline void Hier_PrintSkel_f(FILE* F, hier_t *H) { Hier_Print_raw(F, H, HIER_PRINT_NOACT | HIER_PRINT_NOBBBODY); }
static inline void Hier_PrintSkel(hier_t *H)            { Hier_PrintSkel_f(stdout, H); }

void Hier_PrintDetails(hier_t* H);


// Compute execution time of the hierarchy nodes

int  Hier_Latency_ComputeAction(implem_t* Implem, hier_action_t* action);
void Hier_Latency_ComputeAllStates(implem_t* Implem);

void Hier_Latency_ClearMapper(implem_t* Implem);

void Hier_Latency_DisplayWorse(implem_t* Implem, int nb);
unsigned Hier_Latency_CountAbove(implem_t* Implem, double lat);

void Hier_Timing_ResetAnnotationsUpdate(implem_t* Implem);

minmax_t Hier_Timing_GetLevelTime(hier_node_t* node);

void Hier_Timing_Update(implem_t* Implem, bool from_mapper);

void Hier_Timing_Show_Nodes(implem_t* Implem);
void Hier_Timing_Show(implem_t* Implem);

void Hier_ClockCycles_Display(implem_t* Implem);


// Saving all timing annotations

void Hier_SaveTime_Build(hier_t* H, avl_pp_tree_t* node2save);
void Hier_SaveTime_Commit(hier_t* H, avl_pp_tree_t* node2save);
void Hier_SaveTime_Clear(avl_pp_tree_t* node2save);

void Hier_SaveTime_InitBuild(hier_t* H, avl_pp_tree_t* node2save);
void Hier_SaveTime_CommitClear(hier_t* H, avl_pp_tree_t* node2save);

void Hier_SaveTime_CommitClearUpdate(implem_t* Implem, avl_pp_tree_t* node2save);


// Processes that work on the FOR loops

#define HIER_LOOPUNROLL_KEEPITER  0x01

void Hier_Loop_UpdControl(implem_t* Implem, hier_node_t* node);
int Hier_For_DetectIterator(hier_t* H, hier_node_t* node);
int Hier_Loops_DetectControl(implem_t* Implem);

int Hier_For_PartialUnroll(implem_t* Implem, hier_node_t* node, int factor, int flags);

hier_node_t* Hier_For_GetFullUnroll(hier_t* H, hier_node_t* node, int flags);
int Hier_For_ReplaceByFullUnroll(implem_t* Implem, hier_node_t* node, int flags);

void Hier_For_PrintDetails(implem_t* Implem, hier_node_t* node, indent_t* indent);

chain_list* Hier_For_FreedomDegrees_Get(implem_t* Implem);


// Processes that work on SWITCH structures

void Hier_Switch_UpdControl(implem_t* Implem, hier_node_t* nodeSwitch);
void Hier_Switch_UpdControl_All(implem_t* Implem);

hvex_t* Hier_SwitchCase_GetTestValue(hier_node_t* node, hier_testinfo_t* testinfo);

hvex_t* Hier_Switch_GetSigAsgExpr(hier_node_t* nodeSwitch, char* name);
hvex_t* Hier_Case_BuildFullTestExpr(hier_node_t* nodeCase, hier_testinfo_t* testinfo);

hvex_t* Hier_Switch_NorTests(hier_node_t* nodeSwitch);
hvex_t* Hier_Case_OrTests(hier_node_t* nodeCase);
hvex_t* Hier_Case_FullTest(hier_node_t* nodeCase);

hvex_t* Hier_Switch_NorConds(hier_node_t* nodeSwitch);
hvex_t* Hier_Case_OrConds(hier_node_t* nodeCase);
hvex_t* Hier_Case_FullCond(hier_node_t* nodeCase);

typedef enum {
	HIER_WIRE_STYLE_NONE        = 0,
	HIER_WIRE_STYLE_SEQ_AUTO    = 2,
	HIER_WIRE_STYLE_SEQ_MUX     = 3,
	HIER_WIRE_STYLE_SEQ_ASGCOND = 4,
	HIER_WIRE_STYLE_ONE_CYCLE   = 5,
} hier_wire_style_t;

char* Hier_WireStyle_ID2Str(hier_wire_style_t style);
hier_wire_style_t Hier_WireStyle_Str2ID(const char* str);

hier_node_t* Hier_Switch_GetWired_OneCycle(implem_t* Implem, hier_node_t* nodeSwitch);
int Hier_Switch_ReplaceByWired_SelectStyle(implem_t* Implem, hier_node_t* node, hier_wire_style_t style);

chain_list* Hier_Switch_FreedomDegrees_Get(implem_t* Implem);


// List the freedom degrees about standard allocation

chain_list* Hier_Freedom_Degrees_Get(implem_t* Implem);


// Cramming

#define HIER_CRAM_KEEP_GROUPED  0x01

int Hier_Cram_BB(implem_t* Implem, hier_node_t* node, int flags);
int Hier_Cram_Level2BB(implem_t* Implem, hier_node_t* begNode, hier_node_t* stopNode, int flags);


// Merging entire Levels of nodes

hier_node_t* Hier_Merge(chain_list* list_labels, int flags);
int Hier_Merge_Replace(implem_t* Implem, chain_list* list_labels, int flags);


// User commands

#include "command.h"

int Build_Command(implem_t* Implem, command_t* cmd_data);
int Hier_Loops_Command(implem_t* Implem, command_t* cmd_data);
int Hier_Switch_Command(implem_t* Implem, command_t* cmd_data);
int Hier_Command(implem_t* Implem, command_t* cmd_data);



#endif  // _HIERARCHY_H_


