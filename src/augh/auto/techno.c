
/*

This file contains the functions that do HW resources & latency estimations.

*/


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <assert.h>

#include "auto.h"
#include "addops.h"
#include "techno.h"

// For Reset_GetActiveState()
#include "../netlist/netlist_access.h"



//======================================================================
// Pools of data structures
//======================================================================

// The technology structure

#define POOL_prefix      pool_techno
#define POOL_data_t      techno_t
#define POOL_ALLOC_SIZE  20

#define POOL_INSTANCE             POOL_TECHNO
#define POOL_INSTANCE_ATTRIBUTES  static

#include "../pool.h"

// Allocation wrappers
techno_t* Techno_New() {
	techno_t* techno = pool_techno_pop(&POOL_TECHNO);
	memset(techno, 0, sizeof(*techno));
	avl_pp_init(&techno->timings);
	avl_pp_init(&techno->packages);
	return techno;
}
void Techno_Free(techno_t* techno) {
	pool_techno_push(&POOL_TECHNO, techno);
}

// The technology timings

#define POOL_prefix      pool_techno_timing
#define POOL_data_t      techno_timing_t
#define POOL_ALLOC_SIZE  100

#define POOL_INSTANCE             POOL_TECHNO_TIMING
#define POOL_INSTANCE_ATTRIBUTES  static

#include "../pool.h"

// Allocation wrappers
techno_timing_t* Techno_Timing_New() {
	techno_timing_t* timing = pool_techno_timing_pop(&POOL_TECHNO_TIMING);
	memset(timing, 0, sizeof(*timing));
	return timing;
}
void Techno_Timing_Free(techno_timing_t* timing) {
	pool_techno_timing_push(&POOL_TECHNO_TIMING, timing);
}

// The chip models

#define POOL_prefix      pool_fpga_model
#define POOL_data_t      fpga_model_t
#define POOL_ALLOC_SIZE  100

#define POOL_INSTANCE             POOL_FPGA_MODEL
#define POOL_INSTANCE_ATTRIBUTES  static

#include "../pool.h"

// Allocation wrappers
fpga_model_t* FPGA_Model_New() {
	fpga_model_t* model = pool_fpga_model_pop(&POOL_FPGA_MODEL);
	memset(model, 0, sizeof(*model));
	return model;
}
void FPGA_Model_Free(fpga_model_t* model) {
	pool_fpga_model_push(&POOL_FPGA_MODEL, model);
}

// The FPGA packages

#define POOL_prefix      pool_fpga_pkg_model
#define POOL_data_t      fpga_pkg_model_t
#define POOL_ALLOC_SIZE  100

#define POOL_INSTANCE             POOL_FPGA_PKG_MODEL
#define POOL_INSTANCE_ATTRIBUTES  static

#include "../pool.h"

// Allocation wrappers
fpga_pkg_model_t* FPGA_PkgModel_New() {
	fpga_pkg_model_t* pkg = pool_fpga_pkg_model_pop(&POOL_FPGA_PKG_MODEL);
	memset(pkg, 0, sizeof(*pkg));
	return pkg;
}
void FPGA_PkgModel_Free(fpga_pkg_model_t* pkg) {
	pool_fpga_pkg_model_push(&POOL_FPGA_PKG_MODEL, pkg);
}

// The FPGA boards

#define POOL_prefix      pool_board
#define POOL_data_t      board_t
#define POOL_ALLOC_SIZE  100

#define POOL_INSTANCE             POOL_BOARD
#define POOL_INSTANCE_ATTRIBUTES  static

#include "../pool.h"

// Allocation wrappers
board_t* Board_New() {
	board_t* board = pool_board_pop(&POOL_BOARD);
	memset(board, 0, sizeof(*board));
	avl_pp_init(&board->fpgas);
	return board;
}
void Board_Free(board_t* board) {
	pool_board_push(&POOL_BOARD, board);
}

// The FPGA instance on a board

#define POOL_prefix      pool_board_fpga
#define POOL_data_t      board_fpga_t
#define POOL_ALLOC_SIZE  100

#define POOL_INSTANCE             POOL_BOARD_FPGA
#define POOL_INSTANCE_ATTRIBUTES  static

#include "../pool.h"

// Allocation wrappers
board_fpga_t* Board_FPGA_New() {
	board_fpga_t* fpga = pool_board_fpga_pop(&POOL_BOARD_FPGA);
	memset(fpga, 0, sizeof(*fpga));
	avl_pp_init(&fpga->port2pin);
	fpga->jtag_id = -1;
	return fpga;
}
void Board_FPGA_Free(board_fpga_t* fpga) {
	pool_board_fpga_push(&POOL_BOARD_FPGA, fpga);
}

// Hardware target for synthesis operations

#define POOL_prefix      pool_synth_target
#define POOL_data_t      synth_target_t
#define POOL_ALLOC_SIZE  100

#define POOL_INSTANCE             POOL_SYNTH_TARGET
#define POOL_INSTANCE_ATTRIBUTES  static

#include "../pool.h"

// Allocation wrappers
synth_target_t* Synth_Target_New() {
	synth_target_t* synth = pool_synth_target_pop(&POOL_SYNTH_TARGET);
	memset(synth, 0, sizeof(*synth));
	synth->reset_active_state = '1';
	synth->usage_is_default = true;
	avl_pp_init(&synth->impacc2boardacc);
	avl_pp_init(&synth->port2pin);
	return synth;
}
void Synth_Target_Free(synth_target_t* synth) {
	pool_synth_target_push(&POOL_SYNTH_TARGET, synth);
}



//======================================================================
// All declared technos, fpgas, etc
//======================================================================

static avl_pp_tree_t all_technos;
static avl_pp_tree_t all_fpgas;
static avl_pp_tree_t all_boards;

bool Techno_DeclareTechno(techno_t* techno) {
	bool b = avl_pp_isthere(&all_technos, techno->name);
	if(b==true) return false;
	avl_pp_add_overwrite(&all_technos, techno->name, techno);
	return true;
}
bool Techno_DeclareFPGA(fpga_model_t* fpga) {
	bool b = avl_pp_isthere(&all_fpgas, fpga->name);
	if(b==true) return false;
	avl_pp_add_overwrite(&all_fpgas, fpga->name, fpga);
	return true;
}
bool Techno_DeclareBoard(board_t* board) {
	bool b = avl_pp_isthere(&all_boards, board->name);
	if(b==true) return false;
	avl_pp_add_overwrite(&all_boards, board->name, board);
	return true;
}

techno_t* Techno_GetTechno(const char* name) {
	void* techno = NULL;
	avl_pp_find_data(&all_technos, name, &techno);
	return techno;
}
techno_timing_t* Techno_GetSpeed(techno_t* techno, const char* name) {
	void* speed = NULL;
	avl_pp_find_data(&techno->timings, name, &speed);
	return speed;
}
fpga_pkg_model_t* Techno_GetPackage(techno_t* techno, const char* name) {
	void* pkg = NULL;
	avl_pp_find_data(&techno->packages, name, &pkg);
	return pkg;
}
fpga_model_t* Techno_GetKnownFPGA(const char* name) {
	fpga_model_t* fpga = NULL;
	avl_pp_find_data(&all_fpgas, name, (void**)&fpga);
	return fpga;
}
fpga_model_t* Techno_GetFPGA(const char* name) {
	fpga_model_t* fpga = Techno_GetKnownFPGA(name);
	if(fpga!=NULL) return fpga;

	avl_pp_foreach(&all_technos, scanTechno) {
		techno_t* techno = scanTechno->data;
		if(techno->chipmodel_fromcode==NULL) continue;
		fpga = techno->chipmodel_fromcode(name);
		if(fpga!=NULL) return fpga;
	}

	return NULL;
}
fpga_pkg_model_t* Techno_FPGA_GetPkg(fpga_model_t* fpga, const char* name) {
	foreach(fpga->packages, scan) {
		fpga_pkg_model_t* pkg = scan->DATA;
		if(name==pkg->name) return pkg;
	}
	return NULL;
}
techno_timing_t* Techno_FPGA_GetSpeed(fpga_model_t* fpga, const char* name) {
	foreach(fpga->timings, scan) {
		techno_timing_t* speed = scan->DATA;
		if(name==speed->name) return speed;
	}
	return NULL;
}
board_t* Techno_GetBoard(const char* name) {
	void* board = NULL;
	avl_pp_find_data(&all_boards, name, &board);
	return board;
}

static void Techno_DispChipData(FILE* F, fpga_model_t* chip_data, indent_t* indent) {
	findentprintf(F, indent, "Name .......... %s\n", chip_data->name);
	findentprintf(F, indent, "Techno ........ %s\n", chip_data->techno->name);
	if(chip_data->description!=NULL) {
		findentprintf(F, indent, "Description ... %s\n", chip_data->description);
	}
	findentprintf(F, indent, "Speed grades .. ");
	foreach(chip_data->timings, scan) {
		techno_timing_t* timing = scan->DATA;
		if(scan!=chip_data->timings) fprintf(F, ", ");
		fprintf(F, "%s", timing->name);
	}
	fprintf(F, "\n");
	findentprintf(F, indent, "Packages ...... ");
	foreach(chip_data->packages, scan) {
		fpga_pkg_model_t* pkg = scan->DATA;
		if(scan!=chip_data->packages) fprintf(F, ", ");
		fprintf(F, "%s", pkg->name);
	}
	fprintf(F, "\n");
	findentprintf(F, indent, "Resources ..... ");
	ptype_list* list_res = chip_data->techno->getres(chip_data);
	foreach(list_res, scan) {
		if(scan!=list_res) fprintf(F, ", ");
		fprintf(F, "%s:%lu", (char*)scan->DATA, scan->TYPE);
	}
	fprintf(F, "\n");
	freeptype(list_res);
}
int Techno_DispChipData_user(const char* name, indent_t* indent) {
	fpga_model_t* chip_data = Techno_GetFPGA(namealloc(name));
	if(chip_data==NULL) {
		printf("ERROR: Unknown chip '%s'.\n", name);
		return __LINE__;
	}
	Techno_DispChipData(stdout, chip_data, indent);
	return 0;
}

void Techno_DispAllChipsData(indent_t* indent) {
	avl_pp_foreach(&all_fpgas, scan) {
		printf("\n");
		fpga_model_t* chip_data = scan->data;
		indentprintf(indent, "==== %s ====\n", chip_data->name);
		Techno_DispChipData(stdout, chip_data, indent);
	}
	printf("\n");
}
void Techno_DispAllChips(indent_t* indent) {
	avl_pp_foreach(&all_fpgas, scan) {
		fpga_model_t* chip_data = scan->data;
		indentprintf(indent, "%s (techno %s)", chip_data->name, chip_data->techno->name);
		if(chip_data->description!=NULL) printf(": %s", chip_data->description);
		printf("\n");
	}
}

static void Techno_DispTechnoData(FILE* F, techno_t* techno, indent_t* indent) {
	findentprintf(F, indent, "Name ......... %s\n", techno->name);
	if(techno->description!=NULL) {
		findentprintf(F, indent, "Description .. %s\n", techno->description);
	}
	findentprintf(F, indent, "Timings ...... ");
	bool is_first = true;
	avl_pp_foreach(&techno->timings, scan) {
		techno_timing_t* timing = scan->data;
		if(is_first==true) is_first = false; else fprintf(F, ", ");
		fprintf(F, "%s", timing->name);
	}
	fprintf(F, "\n");
	if(techno->fprint_timing!=NULL) {
		avl_pp_foreach(&techno->timings, scan) {
			techno_timing_t* timing = scan->data;
			indentprintf(indent, "==== Timing %s ====\n", timing->name);
			techno->fprint_timing(F, timing, indent);
		}
	}
}
int Techno_DispTechnoData_user(const char* name, indent_t* indent) {
	techno_t* techno_data = Techno_GetTechno(namealloc(name));
	if(techno_data==NULL) {
		printf("ERROR: Unknown technology '%s'.\n", name);
		return __LINE__;
	}
	Techno_DispTechnoData(stdout, techno_data, indent);
	return 0;
}

void Techno_DispAllTechnoData(indent_t* indent) {
	printf("\n");
	avl_pp_foreach(&all_technos, scan) {
		techno_t* techno_data = scan->data;
		indentprintf(indent, "==== Techno %s ====\n", techno_data->name);
		Techno_DispTechnoData(stdout, techno_data, indent);
		printf("\n");
	}
}
void Techno_DispAllTechno(indent_t* indent) {
	avl_pp_foreach(&all_technos, scan) {
		techno_t* techno_data = scan->data;
		indentprintf(indent, "%s", techno_data->name);
		if(techno_data->description!=NULL) printf(": %s\n", techno_data->description);
		else printf("\n");
	}
}



//======================================================================
// Set technology choices to a synth_target
//======================================================================

void Techno_SynthTarget_Reset(synth_target_t* synth) {
	if(synth->techno==NULL) return;

	if(synth->resources!=NULL) {
		freeptype(synth->resources);
		synth->resources = NULL;
	}

	synth->model = NULL;
	synth->usage_is_default = true;
	synth->model_ratio = 0;

	// FIXME missing clear of impacc2boardacc, port2pin

	if(synth->techno->privatedata_free!=NULL) {
		synth->techno->privatedata_free(synth);
	}

	synth->techno = NULL;
	synth->timing = NULL;
	synth->package = NULL;
	synth->board_fpga = NULL;
}

void Techno_SynthTarget_SetTechno(synth_target_t* synth, techno_t* techno) {
	if(synth->techno==techno) return;

	// Reset all existing stuff
	if(synth->techno!=NULL && synth->techno!=techno) {
		Techno_SynthTarget_Reset(synth);
	}

	// Set the new techno
	synth->techno = techno;

	// Set the default speed grade
	if(techno->default_timing!=NULL) synth->timing = techno->default_timing;
	else synth->timing = avl_pp_root_data(&techno->timings);

	// Initialize private data
	if(synth->techno->privatedata_init!=NULL) {
		synth->techno->privatedata_init(synth);
	}
}

void Techno_SynthTarget_SetTechnoSpeed(synth_target_t* synth, techno_timing_t* timing) {
	if(synth->timing==timing) return;

	if(synth->techno!=NULL && synth->techno!=timing->techno) {
		Techno_SynthTarget_SetTechno(synth, timing->techno);
	}

	synth->timing = timing;
}

void Techno_SynthTarget_SetChipModel(synth_target_t* synth, fpga_model_t* model) {
	if(synth->model==model) return;

	Techno_SynthTarget_SetTechno(synth, model->techno);

	if(synth->timing!=NULL) {
		if(ChainList_IsDataHere(model->timings, synth->timing)==false) synth->timing = NULL;
	}
	if(synth->timing==NULL) {
		synth->timing = model->timings->DATA;
	}

	if(synth->resources!=NULL) freeptype(synth->resources);
	synth->resources = model->techno->getres(model);

	if(model->packages!=NULL) synth->package = model->packages->DATA;
	else synth->package = NULL;

	synth->model = model;
	synth->usage_is_default = true;

	// Initialize private data
	if(synth->techno->privatedata_initchip!=NULL) {
		synth->techno->privatedata_initchip(synth);
	}
}

void Techno_SetBoardFpga(implem_t* Implem, board_fpga_t* board_fpga) {
	synth_target_t* synth = Implem->synth_target;
	if(board_fpga==synth->board_fpga) return;

	Techno_SynthTarget_SetChipModel(synth, board_fpga->model);
	synth->timing = board_fpga->speed;
	synth->package = board_fpga->package;
	synth->board_fpga = board_fpga;

	// Select the default features of this chip

	if(board_fpga->default_clock!=NULL) {
		Techno_SetBoardClock_Access(Implem, board_fpga->default_clock);
	}

	if(board_fpga->default_reset!=NULL) {
		// Create a signal and a port
		if(Implem->netlist.port_reset==NULL) {
			Map_Netlist_CreatePortReset(Implem);
		}
		else {
			// Remove existing user-defined pin settings
			avl_pp_node_t* pp_node = NULL;
			bool b = avl_pp_find(&Implem->synth_target->port2pin, Implem->netlist.port_reset, &pp_node);
			if(b==true) {
				Implem->synth_target->techno->portattr_free(pp_node->data);
				avl_pp_rem(&Implem->synth_target->port2pin, pp_node);
			}
		}
		// Link the Implem port to the board_fpga port
		avl_pp_add_overwrite(&synth->impacc2boardacc, Implem->netlist.port_reset->access, board_fpga->default_reset);
		// Set the active value
		Implem->synth_target->reset_active_state = Netlist_Access_Reset_GetActiveState(board_fpga->default_reset);
	}

	// Don't create a wait-on-start port because for now boards don't declare a default Start port
	// The user can still add its own wait loop that reads any input board port
	Implem->env.wait_on_start = false;
}

// Set the clock source. Return !0 on error.
int Techno_SetBoardClock_FromName(implem_t* Implem, const char* name) {
	board_fpga_t* board_fpga = Implem->synth_target->board_fpga;
	if(board_fpga==NULL) {
		printf("ERROR: No board is selected. Can't set clock source '%s'.\n", name);
		return -1;
	}

	// Get the name of the clock source
	netlist_access_t* access = Netlist_Comp_GetAccess(board_fpga->connect, name);
	if(access==NULL) {
		printf("ERROR: No clock source named '%s' was found.\n", name);
		return -1;
	}

	// Set the clock source
	return Techno_SetBoardClock_Access(Implem, access);
}
int Techno_SetBoardClock_Access(implem_t* Implem, netlist_access_t* access) {
	if(access->model==NETLIST_ACCESS_CLOCK) {
		Techno_SetBoardClock_AccessClock(Implem, access);
	}
	else if(access->model==NETLIST_ACCESS_DIFFCLOCK) {
		Techno_SetBoardClock_AccessDiffClock(Implem, access);
	}
	else {
		printf("ERROR: The access '%s' model '%s' is not a valid clock source.\n", access->name, access->model->name);
		return -1;
	}
	return 0;
}

void Techno_SetBoardClock_AccessClock(implem_t* Implem, netlist_access_t* access) {
	assert(access->model==NETLIST_ACCESS_CLOCK);
	// Create a signal and a port
	if(Implem->netlist.port_clock==NULL) {
		Map_Netlist_CreatePortClock(Implem);
	}
	else {
		// Remove existing user-defined pin settings
		avl_pp_node_t* pp_node = NULL;
		bool b = avl_pp_find(&Implem->synth_target->port2pin, Implem->netlist.port_clock, &pp_node);
		if(b==true) {
			Implem->synth_target->techno->portattr_free(pp_node->data);
			avl_pp_rem(&Implem->synth_target->port2pin, pp_node);
		}
	}
	// Link the Implem access to the board_fpga access
	avl_pp_add_overwrite(&Implem->synth_target->impacc2boardacc, Implem->netlist.port_clock->access, access);
	// Set the target frequency
	Techno_SynthTarget_SetFrequency(Implem->synth_target, Netlist_Access_Clock_GetFrequency(access));
}
void Techno_SetBoardClock_AccessDiffClock(implem_t* Implem, netlist_access_t* boardacc) {
	assert(boardacc->model==NETLIST_ACCESS_DIFFCLOCK);

	// Create the top-level Implem access
	netlist_access_t* impacc = Netlist_Access_Dup(boardacc, Implem->netlist.top, NULL);
	// Link the Implem access to the board_fpga access
	avl_pp_add_overwrite(&Implem->synth_target->impacc2boardacc, impacc, boardacc);

	// Create the interface DiffClock component
	char* comp_name = Map_Netlist_MakeInstanceName_TryThis(Implem, boardacc->name);
	netlist_comp_t* comp = Netlist_Comp_DiffClock_New(comp_name);
	Netlist_Comp_SetChild(Implem->netlist.top, comp);

	netlist_diffclock_t* comp_data = comp->data;
	netlist_access_diffclock_t* compacc_data = comp_data->access_diffclock->data;
	netlist_access_diffclock_t* impacc_data = impacc->data;

	// Copy config
	Netlist_Access_DiffClock_CopyParams(impacc, boardacc);
	Netlist_Access_DiffClock_CopyParams(comp_data->access_diffclock, boardacc);

	// Connect the ports for differential clock
	compacc_data->port_p->source = Netlist_ValCat_MakeList_FullPort(impacc_data->port_p);
	compacc_data->port_n->source = Netlist_ValCat_MakeList_FullPort(impacc_data->port_n);

	// Connect the clock signal
	netlist_signal_t* clk_data = Implem->netlist.sig_clock->data;
	if(clk_data->port_in->source!=NULL) {
		printf("Warning: Overwriting source of port '%s' of comp '%s' model '%s'.\n",
			clk_data->port_in->name, clk_data->port_in->component->name, clk_data->port_in->component->model->name
		);
		Netlist_ValCat_FreeList(clk_data->port_in->source);
	}
	clk_data->port_in->source = Netlist_ValCat_MakeList_FullPort(comp_data->port_clk_out);
	Netlist_PortTargets_SetSource(clk_data->port_in);

	// Remaining Implem settings
	Implem->netlist.port_clock = comp_data->port_clk_out;
	Techno_SynthTarget_SetFrequency(Implem->synth_target, Netlist_Access_DiffClock_GetFrequency(impacc));
}

// Set fields

void Techno_SynthTarget_SetFrequency(synth_target_t* synth, double frequency) {
	if(frequency <= 0) {
		synth->clk_freq = 0;
		synth->clk_period_ns = 0;
	}
	else {
		synth->clk_freq = frequency;
		synth->clk_period_ns = 1e9/frequency;
	}
}



//======================================================================
// Initialization functions
//======================================================================

void AughTechno_Init() {
	avl_pp_init(&all_technos);
	avl_pp_init(&all_fpgas);
	avl_pp_init(&all_boards);
}



//======================================================================
// Manipulation of hardware resources - By name
//======================================================================

void Techno_NameRes_Print(ptype_list* list, const char* beg, const char* end) {
	if(beg!=NULL) printf("%s", beg);
	if(list!=NULL) {
		foreach(list, scanRes) {
			if(scanRes!=list) printf(", ");
			printf("%s:%ld", (char*)scanRes->DATA, scanRes->TYPE);
		}
	}
	else printf("none");
	if(beg!=NULL) printf("%s", end);
}

// minzero : prohibit negative resources
// noerr   : no error if negative result. zero is used instead.
int Techno_NameRes_Sub_raw(ptype_list* dest, ptype_list* src, bool minzero, bool noerr) {
	foreach(src, scan) {
		ptype_list* elt = ChainPType_SearchData(dest, scan->DATA);
		if(elt==NULL) return -1;
		if(minzero==false) elt->TYPE -= scan->TYPE;
		else {
			if(scan->TYPE > elt->TYPE) {
				if(noerr==false) return -1;
				elt->TYPE = 0;
			}
			else elt->TYPE -= scan->TYPE;
		}
	}
	return 0;
}
ptype_list* Techno_NameRes_AddMult(ptype_list* dest, ptype_list* src, int nb) {
	foreach(src, scan) {
		ptype_list* elt = ChainPType_SearchData(dest, scan->DATA);
		if(elt!=NULL) elt->TYPE += scan->TYPE * nb;
		else dest = addptype(dest, scan->TYPE*nb, scan->DATA);
	}
	return dest;
}
void Techno_NameRes_MultCst(ptype_list* dest, int nb) {
	foreach(dest, scan) scan->TYPE *= nb;
}

ptype_list* Techno_NameRes_Add_One(ptype_list* dest, char* name, int val) {
	ptype_list* elt_dest = ChainPType_SearchData(dest, name);
	if(elt_dest==NULL) {
		dest = addptype(dest, 0, name);
		elt_dest = dest;
	}
	elt_dest->TYPE += val;
	return dest;
}
ptype_list* Techno_NameRes_Add(ptype_list* dest, ptype_list* src) {
	foreach(src, elt_src) {
		dest = Techno_NameRes_Add_One(dest, elt_src->DATA, elt_src->TYPE);
	}
	return dest;
}

ptype_list* Techno_NameRes_SetMax_One(ptype_list* dest, char* name, int val) {
	ptype_list* elt_dest = ChainPType_SearchData(dest, name);
	if(elt_dest==NULL) {
		dest = addptype(dest, val, name);
		elt_dest = dest;
	}
	else {
		elt_dest->TYPE = GetMax(elt_dest->TYPE, val);
	}
	return dest;
}
ptype_list* Techno_NameRes_SetMax(ptype_list* dest, ptype_list* src) {
	foreach(src, elt_src) {
		dest = Techno_NameRes_SetMax_One(dest, elt_src->DATA, elt_src->TYPE);
	}
	return dest;
}

bitype_list* Techno_NameOpRes_Add_One(bitype_list* dest, char* name, unsigned nb, ptype_list* res) {
	bitype_list* elt_dest = ChainBiType_SearchFrom(dest, name);
	if(elt_dest==NULL) {
		dest = addbitype(dest, 0, name, NULL);
		elt_dest = dest;
	}
	elt_dest->TYPE += nb;
	elt_dest->DATA_TO = Techno_NameRes_AddMult(elt_dest->DATA_TO, res, nb);
	return dest;
}
bitype_list* Techno_NameOpRes_Add(bitype_list* dest, bitype_list* src) {
	foreach(src, elt_src) {
		dest = Techno_NameOpRes_Add_One(dest, elt_src->DATA_FROM, elt_src->TYPE, elt_src->DATA_TO);
	}
	return dest;
}
ptype_list* Techno_NameOpRes_GetTotal(bitype_list* list) {
	ptype_list* res_total = NULL;
	foreach(list, elt) {
		res_total = Techno_NameRes_Add(res_total, elt->DATA_TO);
	}
	return res_total;
}

// Note: THe boxes are printed with ASCII characters
void Techno_NameOpRes_FsPrint(FILE* F, bitype_list* list, char* indent_str) {
	ptype_list* res_total = Techno_NameOpRes_GetTotal(list);

	char* comptitle = "Component";

	// Compute the max length of the operator name
	unsigned col_nb = ChainPType_Count(res_total);
	char* col_name[col_nb];
	unsigned col_width[col_nb];

	int idx = 0;
	foreach(res_total, elt) {
		int width = 16;  // space, raw value (6 digits), space, percent (6 digits), %, space
		int len = strlen((char*)elt->DATA);
		if(len>width) width = len;
		col_width[idx] = width;
		col_name[idx] = elt->DATA;
		idx++;
	}

	int l_name = 0;
	for(bitype_list *v=list; v!=NULL; v=v->NEXT) {
		int l = strlen(v->DATA_FROM);
		if(l>l_name) l_name = l;
	}
	int l = strlen(comptitle);
	if(l>l_name) l_name = l;

	// Print the table header

	// The top line
	if(indent_str!=NULL) fprintf(F, indent_str);
	fprintf(F, "+-");
	for(int i=0; i<l_name; i++) fputc('-', F);
	fprintf(F, "-+-------+");
	for(unsigned idx_col=0; idx_col<col_nb; idx_col++) {
		for(int i=0; i<col_width[idx_col]; i++) fputc('-', F);
		fputc('+', F);
	}
	fputc('\n', F);
	// The column titles
	if(indent_str!=NULL) fprintf(F, indent_str);
	fprintf(F, "| %*s |   #   |", l_name, comptitle);
	for(unsigned idx_col=0; idx_col<col_nb; idx_col++) {
		int margins_both = col_width[idx_col] - strlen(col_name[idx_col]);
		int margin_left = margins_both / 2;
		int margin_right = margins_both - margin_left;
		for(int i=0; i<margin_left; i++) fputc(' ', F);
		fprintf(F, "%s", col_name[idx_col]);
		for(int i=0; i<margin_right; i++) fputc(' ', F);
		fputc('|', F);
	}
	fputc('\n', F);
	// The bottom line
	if(indent_str!=NULL) fprintf(F, indent_str);
	fprintf(F, "+-");
	for(int i=0; i<l_name; i++) fputc('-', F);
	fprintf(F, "-+-------+");
	for(unsigned idx_col=0; idx_col<col_nb; idx_col++) {
		for(int i=0; i<col_width[idx_col]; i++) fputc('-', F);
		fputc('+', F);
	}
	fputc('\n', F);

	// Print data

	foreach(list, elt_op) {
		if(indent_str!=NULL) fprintf(F, indent_str);
		fprintf(F, "| %*s | %*lu |", l_name, (char*)elt_op->DATA_FROM, 5, elt_op->TYPE);
		for(unsigned idx_col=0; idx_col<col_nb; idx_col++) {
			unsigned val = 0;
			ptype_list* elt_usage = ChainPType_SearchData(elt_op->DATA_TO, col_name[idx_col]);
			if(elt_usage!=NULL) val = elt_usage->TYPE;
			ptype_list* elt_total = ChainPType_SearchData(res_total, col_name[idx_col]);
			double percent = 0;
			if(elt_total->TYPE>0) percent = 100 * (double)val / elt_total->TYPE;
			// Compute margins
			int margins_both = col_width[idx_col] - 14;
			int margin_left = margins_both / 2;
			int margin_right = margins_both - margin_left;
			// Print data
			for(int i=0; i<margin_left; i++) fputc(' ', F);
			fprintf(F, "%6u %6.2f%%", val, percent);
			for(int i=0; i<margin_right; i++) fputc(' ', F);
			fputc('|', F);
		}
		fputc('\n', F);
	}

	// Print the table footer : total resources

	// The top line
	if(indent_str!=NULL) fprintf(F, indent_str);
	fprintf(F, "+-");
	for(int i=0; i<l_name; i++) fputc('-', F);
	fprintf(F, "-+-------+");
	for(unsigned idx_col=0; idx_col<col_nb; idx_col++) {
		for(int i=0; i<col_width[idx_col]; i++) fputc('-', F);
		fputc('+', F);
	}
	fputc('\n', F);
	// Data
	if(indent_str!=NULL) fprintf(F, indent_str);
	fprintf(F, "| %*s |       |", l_name, "Total");
	for(unsigned idx_col=0; idx_col<col_nb; idx_col++) {
		ptype_list* elt_total = ChainPType_SearchData(res_total, col_name[idx_col]);
		// Compute margins
		int margins_both = col_width[idx_col] - 14;
		int margin_left = margins_both / 2;
		int margin_right = margins_both - margin_left + 1 + 6 + 1;  // Add the space taken by the percent
		// Print data
		for(int i=0; i<margin_left; i++) fputc(' ', F);
		fprintf(F, "%6lu", elt_total->TYPE);
		for(int i=0; i<margin_right; i++) fputc(' ', F);
		fputc('|', F);
	}
	fputc('\n', F);
	// The bottom line
	if(indent_str!=NULL) fprintf(F, indent_str);
	fprintf(F, "+-");
	for(int i=0; i<l_name; i++) fputc('-', F);
	fprintf(F, "-+-------+");
	for(unsigned idx_col=0; idx_col<col_nb; idx_col++) {
		for(int i=0; i<col_width[idx_col]; i++) fputc('-', F);
		fputc('+', F);
	}
	fputc('\n', F);

	// Clean
	freeptype(res_total);
}
void Techno_NameOpRes_FiPrint(FILE* F, bitype_list* list, indent_t* indent) {
	// Note: The indentation level here is the last.
	// So use the efficient way: precompute the indent string
	char* indent_str = NULL;
	if(indent!=NULL) {
		char indent_buf[indent->l * indent->n + 1];
		unsigned idx = 0;
		for(unsigned l=0; l<indent->l; l++) for(unsigned n=0; n<indent->n; n++) indent_buf[idx++] = indent->c;
		indent_buf[idx] = 0;
		indent_str = stralloc(indent_buf);
	}
	// Launch display
	Techno_NameOpRes_FsPrint(F, list, indent_str);
}
void Techno_NameOpRes_Free(bitype_list* list) {
	foreach(list, elt) freeptype(elt->DATA_TO);
	freebitype(list);
}

// Write the resources through a file, no beautiful display
void Techno_NameOpRes_fprint(FILE* F, bitype_list* list) {
	// Print resources for all operators
	fprintf(F, "ops-nb %u", ChainBiType_Count(list));
	foreach(list, v) {
		fprintf(F, " %s %lu", (char*)v->DATA_FROM, v->TYPE);
		ptype_list* list_res = v->DATA_TO;
		fprintf(F, " res-nb %u", ChainPType_Count(list_res));
		foreach(list_res, res) fprintf(F, " %s %lu", (char*)res->DATA, res->TYPE);
	}
	fprintf(F, "\n");
}
// Read the resources from a file
int  Techno_NameOpRes_fscanf(FILE* F, bitype_list** res_list) {
	char buffer[200];
	int z;
	bitype_list* list = NULL;
	unsigned nb = 0;
	// Get the number of operators
	z = fscanf(F, "%s", buffer);
	if(z!=1) return __LINE__;
	if(strcmp(buffer, "ops-nb")!=0) return __LINE__;
	z = fscanf(F, "%u", &nb);
	if(z!=1) return __LINE__;
	// Loop on the operator type
	for(unsigned idx_op=0; idx_op<nb; idx_op++) {
		list = addbitype(list, 0, NULL, NULL);

		// Get the name of the operator, and the number.
		z = fscanf(F, "%s", buffer);
		if(z!=1) { Techno_NameOpRes_Free(list); return __LINE__; }
		list->DATA_FROM = namealloc(buffer);
		z = fscanf(F, "%lu", &list->TYPE);
		if(z!=1) { Techno_NameOpRes_Free(list); return __LINE__; }

		// Get the number of resource types
		unsigned res_nb;
		z = fscanf(F, "%s", buffer);
		if(z!=1) return __LINE__;
		if(strcmp(buffer, "res-nb")!=0) { Techno_NameOpRes_Free(list); return __LINE__; }
		z = fscanf(F, "%u", &res_nb);
		if(z!=1) { Techno_NameOpRes_Free(list); return __LINE__; }

		// Get the resource usage for this operator
		for(unsigned idx_res=0; idx_res<res_nb; idx_res++) {
			ptype_list* list_res = addptype(list->DATA_TO, 0, NULL);
			list->DATA_TO = list_res;
			// Get the name of the resource type, and the number.
			z = fscanf(F, "%s", buffer);
			if(z!=1) { Techno_NameOpRes_Free(list); return __LINE__; }
			list_res->DATA = namealloc(buffer);
			z = fscanf(F, "%lu", &list_res->TYPE);
			if(z!=1) { Techno_NameOpRes_Free(list); return __LINE__; }
		}
	}
	*res_list = list;
	return 0;
}

// Write the resources through a file, no beautiful display
void Techno_NameRes_fprintf(FILE* F, ptype_list* list) {
	// Print resources for all operators
	fprintf(F, "res-nb %u", ChainPType_Count(list));
	foreach(list, v) fprintf(F, " %s %lu", (char*)v->DATA, v->TYPE);
	fprintf(F, "\n");
}
// Read the resources from a file
int  Techno_NameRes_fscanf(FILE* F, ptype_list** res_list) {
	char buffer[200];
	int z;
	ptype_list* list = NULL;
	unsigned nb = 0;
	// Get the number of resource types
	z = fscanf(F, "%s", buffer);
	if(z!=1) return -1;
	if(strcmp(buffer, "res-nb")!=0) return -1;
	z = fscanf(F, "%u", &nb);
	if(z!=1) return -1;
	// Get the resources
	for(unsigned i=0; i<nb; i++) {
		list = addptype(list, 0, NULL);
		z = fscanf(F, "%s", buffer);
		if(z!=1) { freeptype(list); return -1; }
		list->DATA = namealloc(buffer);
		z = fscanf(F, "%lu", &list->TYPE);
		if(z!=1) { freeptype(list); return -1; }
	}
	*res_list = list;
	return 0;
}

bool Techno_NameRes_CheckInclude(ptype_list* dest, ptype_list* src) {
	if(src==NULL) return true;
	if(dest==NULL) return false;
	foreach(src, scanSrc) {
		ptype_list* dest_elt = ChainPType_SearchData(dest, scanSrc->DATA);
		if(dest_elt==NULL) return false;
		if(dest_elt->TYPE < scanSrc->TYPE) return false;
	}
	return true;
}



//=================================================================
// Latency evaluation
//=================================================================

double Techno_Eval_Latency_VexAsg(implem_t* Implem, hvex_t* Expr) {
	techno_t* techno = Implem->synth_target->techno;
	if(techno==NULL) return 0;
	return techno->vexcoarseevaldelay(Implem, Expr);
}

double Techno_Eval_Latency_HierState(implem_t* Implem, hier_node_t* node) {
	hier_state_t* state_data = &node->NODE.STATE;
	double lat_max = 0;

	foreach(state_data->actions, scanAction) {
		hier_action_t* action = scanAction->DATA;

		double l = Techno_Eval_Latency_VexAsg(Implem, action->expr);
		//assert(l>=0);
		if(l<0) continue;

		if(l>lat_max) lat_max = l;
	}

	return lat_max;
}

double Techno_Eval_Latency_HierMax(implem_t* Implem) {
	// FIXME Take into account the Implem component models
	if(Implem->H==NULL) return 0;

	double lat_max = 0;

	avl_p_foreach(&Implem->H->NODES[HIERARCHY_STATE], scan) {
		hier_node_t* node = scan->data;

		double l = Techno_Eval_Latency_HierState(Implem, node);
		//assert(l>=0);
		if(l<0) continue;

		if(l>lat_max) lat_max = l;
	}

	return lat_max;
}



//======================================================================
// Evaluation of an entire design - New netlist
//======================================================================

// Evaluate the HW cost of transforming a memory into registers
// For consistency, the mem is supposed to be accessed only by literal adresses.
ptype_list* Techno_Netlist_Eval_Mem2Reg(implem_t* Implem, const char* name) {
	return Implem->synth_target->techno->sizeeval_mem2reg(Implem, name);
}

// Evaluate the HW cost of adding Read ports.
ptype_list* Techno_Netlist_Eval_AddReadPorts(implem_t* Implem, const char* name, unsigned ports_add_nb) {
	return Implem->synth_target->techno->sizeeval_addreadports(Implem, name, ports_add_nb);
}

static bool evalsize_dorecurs = true;

ptype_list* Techno_EvalSize_addrecurs(ptype_list* list, synth_target_t* synth, netlist_comp_t* comp) {
	if(evalsize_dorecurs==false) return list;

	avl_pp_foreach(&comp->children, scanChild) {
		netlist_comp_t* comp = scanChild->data;
		ptype_list* local_list = Techno_EvalSize_Comp(synth, comp);
		list = Techno_NameRes_Add(list, local_list);
		freeptype(local_list);
	}

	return list;
}

// The returned data uses the names for components and resources.
static bitype_list* Techno_Netlist_EvalSize_WithTypes_internal(bitype_list* list, synth_target_t* synth, netlist_comp_t* comp) {

	// Eval the size of the chidren
	avl_pp_foreach(&comp->children, scanChild) {
		list = Techno_Netlist_EvalSize_WithTypes_internal(list, synth, scanChild->data);
	}

	// The size of the component itself
	ptype_list* local_size = Techno_EvalSize_Comp(synth, comp);
	if(local_size!=NULL) {
		list = Techno_NameOpRes_Add_One(list, comp->model->name, 1, local_size);
		freeptype(local_size);
	}

	return list;
}
bitype_list* Techno_Netlist_EvalSize_WithTypes(synth_target_t* synth, netlist_comp_t* comp) {

	// To avoid total size appear twice as what it actually is
	evalsize_dorecurs = false;

	// Eval the size
	bitype_list* list = Techno_Netlist_EvalSize_WithTypes_internal(NULL, synth, comp);

	// Back to normal setting
	evalsize_dorecurs = true;

	return list;
}

void Techno_Netlist_Size_EvalPrint(synth_target_t* synth, netlist_comp_t* comp) {
	bitype_list* list_res = Techno_Netlist_EvalSize_WithTypes(synth, comp);
	Techno_NameOpRes_Print(list_res);
	Techno_NameOpRes_Free(list_res);
}



//======================================================================
// Special evaluations
//======================================================================

// FIXME only using the output width is not enough for multiplier
// FIXME input from a comp model structure would be cleaner
ptype_list* Techno_Netlist_Get_NameRes_FromOutWidth(implem_t* Implem, char* type, unsigned width) {
	return Implem->synth_target->techno->sizeeval_compfromoutwidth(Implem, type, width);
}

ptype_list* Techno_EvalRes_Mux_Direct(implem_t* Implem, unsigned width, unsigned inputs) {
	return Implem->synth_target->techno->sizeeval_muxdirect(Implem, width, inputs);
}

ptype_list* Techno_EvalRes_FsmOut_OneBit_OneHot(implem_t* Implem, unsigned actions_nb) {
	return Implem->synth_target->techno->sizeeval_fsmout1(Implem, actions_nb);
}



//=================================================================
// Execution of user commands
//=================================================================

int Techno_Command(implem_t* Implem, command_t* cmd_data) {
	const char* cmd = Command_PopParam(cmd_data);
	if(cmd==NULL) {
		printf("Error 'techno' : No command received. Try 'help'.\n");
		return -1;
	}

	if(strcmp(cmd, "help")==0) {
		printf("Commands to set parameters:\n");
		printf("  disable-dsp    Disable the use of DSP cores for multipliers.\n");
		printf("  enable-dsp     Enable the use of DSP cores for multipliers.\n");
		printf("Other commands:\n");
		printf("  set-techno <name>\n");
		printf("                 Target a particular FPGA techology.\n");
		printf("  set-speed <name>\n");
		printf("                 Target a particular techology speed grade.\n");
		printf("  set-chip <name>\n");
		printf("                 Target a particular FPGA.\n");
		printf("  set-board [<fpga-name>]\n");
		printf("                 Target a particular FPGA board.\n");
		printf("  set-board-clock <clock-name>\n");
		printf("                 Select a particular clock source on the target board.\n");
		printf("  eval           Print hardware resources usage.\n");
		printf("  technos        List the available technologies.\n");
		printf("  technos-data   List the available technologies, with all details.\n");
		printf("  techno <name>  Display details for one technology.\n");
		printf("  chips          List the available chips.\n");
		printf("  chips-data     List the available chips, with all details.\n");
		printf("  chip <name>    Display details for one chip.\n");
		printf("  current        Display the current technology.\n");
		printf("  cmd <commands> Call the command interpreter of the technology.\n");
		printf("  help           Display this help.\n");
		return 0;
	}

	if(strcmp(cmd, "cmd")==0) {
		if(Implem->synth_target->techno==NULL) {
			printf("ERROR: No techno is selected. Can't call command '%s'.\n", cmd);
			return -1;
		}
		if(Implem->synth_target->techno->command==NULL) {
			printf("ERROR: The techno does not have a command interpreter. Can't call command '%s'.\n", cmd);
			return -1;
		}
		return Implem->synth_target->techno->command(Implem, cmd_data);
	}

	else if(strcmp(cmd, "eval")==0) {
		Techno_Netlist_Size_EvalPrint(Implem->synth_target, Implem->netlist.top);
		printf("Estimation of the datapath worst combinatorial delay: %g ns\n", Techno_Eval_Latency_HierMax(Implem));
	}

	else if(strcmp(cmd, "set-techno")==0) {
		char* name = Command_PopParam_namealloc(cmd_data);
		if(name==NULL) {
			printf("ERROR : Missing value for command '%s'.\n", cmd);
			return -1;
		}
		techno_t* techno = Techno_GetTechno(name);
		if(techno==NULL) {
			printf("ERROR : The technology '%s' was not found.\n", name);
			return -1;
		}
		Techno_SynthTarget_SetTechno(Implem->synth_target, techno);
	}
	else if(strcmp(cmd, "set-speed")==0) {
		char* name = Command_PopParam_namealloc(cmd_data);
		if(name==NULL) {
			printf("ERROR : Missing value for command '%s'.\n", cmd);
			return -1;
		}
		if(Implem->synth_target->techno==NULL) {
			printf("ERROR : A target technology must be set for command '%s'.\n", cmd);
			return -1;
		}
		techno_timing_t* speed = Techno_GetSpeed(Implem->synth_target->techno, name);
		if(speed==NULL) {
			printf("ERROR : The speed grade '%s' was not found.\n", name);
			return -1;
		}
		Techno_SynthTarget_SetTechnoSpeed(Implem->synth_target, speed);
	}
	else if(strcmp(cmd, "set-chip")==0) {
		char* name = Command_PopParam_namealloc(cmd_data);
		if(name==NULL) {
			printf("ERROR : Missing value for command '%s'.\n", cmd);
			return -1;
		}
		fpga_model_t* fpga = Techno_GetFPGA(name);
		if(fpga==NULL) {
			printf("ERROR : The FPGA '%s' was not found.\n", name);
			return -1;
		}
		Techno_SynthTarget_SetChipModel(Implem->synth_target, fpga);
	}
	else if(strcmp(cmd, "set-package")==0) {
		char* name = Command_PopParam_namealloc(cmd_data);
		if(name==NULL) {
			printf("ERROR : Missing value for command '%s'.\n", cmd);
			return -1;
		}
		if(Implem->synth_target->model==NULL) {
			printf("ERROR : A target FPGA must be set for command '%s'.\n", cmd);
			return -1;
		}
		fpga_pkg_model_t* pkg = Techno_FPGA_GetPkg(Implem->synth_target->model, name);
		if(pkg==NULL) {
			printf("ERROR : The package '%s' was not found.\n", name);
			return -1;
		}
		Implem->synth_target->package = pkg;
	}

	else if(strcmp(cmd, "set-board")==0) {
		char* board_name = Command_PopParam_namealloc(cmd_data);
		if(board_name==NULL) {
			printf("ERROR : Missing value for command '%s'.\n", cmd);
			return -1;
		}
		board_t* board = Techno_GetBoard(board_name);
		if(board==NULL) {
			printf("ERROR : The board '%s' was not found.\n", board_name);
			return -1;
		}

		char* fpga_name = Command_PopParam_namealloc(cmd_data);
		if(fpga_name==NULL) {
			if(avl_pp_count(&board->fpgas)!=1) {
				printf("ERROR: Only 1-FPGA boards are handled.\n");
				exit(EXIT_FAILURE);
			}
			board_fpga_t* board_fpga = avl_pp_root_data(&board->fpgas);
			Techno_SetBoardFpga(Implem, board_fpga);
		}
		else {
			board_fpga_t* fpga = NULL;
			avl_pp_find_data(&board->fpgas, fpga_name, (void**)&fpga);
			if(fpga==NULL) {
				printf("ERROR : The FPGA '%s' on board '%s' was not found.\n", fpga_name, fpga_name);
				return -1;
			}
			Techno_SetBoardFpga(Implem, fpga);
		}
	}
	else if(strcmp(cmd, "set-board-clock")==0) {
		char* name = Command_PopParam_namealloc(cmd_data);
		if(name==NULL) {
			printf("ERROR: Missing value for command '%s'.\n", cmd);
			return -1;
		}
		return Techno_SetBoardClock_FromName(Implem, name);
	}

	else if(strcmp(cmd, "technos")==0) {
		Techno_DispAllTechno(NULL);
	}
	else if(strcmp(cmd, "technos-data")==0) {
		Techno_DispAllTechnoData(NULL);
	}
	else if(strcmp(cmd, "techno")==0) {
		const char* name = Command_PopParam(cmd_data);
		if(name==NULL) {
			printf("ERROR : Missing techno name for command '%s'.\n", cmd);
			return -1;
		}
		return Techno_DispTechnoData_user(name, NULL);
	}
	else if(strcmp(cmd, "chips")==0) {
		Techno_DispAllChips(NULL);
	}
	else if(strcmp(cmd, "chips-data")==0) {
		Techno_DispAllChipsData(NULL);
	}
	else if(strcmp(cmd, "chip")==0) {
		const char* name = Command_PopParam(cmd_data);
		if(name==NULL) {
			printf("ERROR : Missing chip name for command '%s'.\n", cmd);
			return -1;
		}
		return Techno_DispChipData_user(name, NULL);
	}

	else if(strcmp(cmd, "current")==0) {
		techno_t* techno = Implem->synth_target->techno;
		if(techno==NULL) {
			printf("ERROR : No technology is set.\n");
			return __LINE__;
		}
		Techno_DispTechnoData(stdout, techno, NULL);
	}

	// FIXME This is way too dirty
	else if(strcmp(cmd, "disable-dsp")==0) {
#if 0
		techno_t* techno = Implem->synth_target->techno;
		if(techno==NULL) {
			printf("ERROR : No technology is currently in use.\n");
			return __LINE__;
		}
		T->dsp48e.enable = false;
#endif
	}
	else if(strcmp(cmd, "enable-dsp")==0) {
#if 0
		if(T==NULL) {
			printf("ERROR : No technology is currently in use.\n");
			return __LINE__;
		}
		if(T->dsp48e.avail==false) {
			printf("ERROR : DSP cores are not available in techno '%s'. Can't enable their use.\n", T->name);
			return __LINE__;
		}
		T->dsp48e.enable = true;
#endif
	}

	else {
		printf("Error 'techno' : Unknown command '%s'.\n", cmd);
		return -1;
	}

	return 0;
}


