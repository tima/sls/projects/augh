
#ifndef _TECHNO_DELAY_H_
#define _TECHNO_DELAY_H_


// A structure to store pre-computed values, for a given state
typedef struct techno_delay_precomp_t  techno_delay_precomp_t;
struct techno_delay_precomp_t {
	// The FSM state this structure is related to
	netlist_fsm_state_t* state;

	// The MUX inputs that are possibly used
	avl_p_tree_t mux_inputs;

	// The components that are possibly written to
	chain_list* write_comp;

	// The access structures where a Write is possibly done
	//   (if write to component is not precise enough)
	chain_list* write_access;
};

typedef struct techno_delay_t  techno_delay_t;
struct techno_delay_t {
	implem_t* Implem;

	// To avoid printing too many similar messages
	// "Warning: No delay propagation function is defined for component model ..."
	avl_pi_tree_t warn_nopropfunc_comp;

	// Pre-computed values, before scanning all states
	// Key = FSM state, data = techno_delay_precomp_t*
	avl_pp_tree_t precomp_perstate;
	// Key = mux input access, data = chain_list* of states where possibly active
	avl_pp_tree_t precomp_muxinstates;
	// Key = FSM outval, data = chain_list* of states where possibly active
	avl_pp_tree_t precomp_fsmoutstates;

	// Key = port, data = an array of double (the delay for each bit)
	// A negative value means unset
	avl_pp_tree_t ports_delays;

	// Data for the state being scanned
	techno_delay_precomp_t* curstate;

	// When processing a given state, add scanned ports to this tree
	// So when beginning process of a new state, only reset the previously used ports
	avl_p_tree_t ports_used;

	// For the state being scanned, the components being recursively scanned (to avoid infinite recursion)
	//   Even if these combinatorial loops should not exist, this is a safety
	avl_p_tree_t combloop_outports;

	// FIXME allow to store data for each component
	//   Data that won't change between scanned states
	// Example: for a MUX, the max delay of all Enable ports from the FSM, the number of LUT stages, etc

};


void Techno_Delay_Process_PortIn(techno_delay_t* data, netlist_port_t* port, double* delays);
void Techno_Delay_Process_PortOut(techno_delay_t* data, netlist_port_t* port, double* delays);
double Techno_Delay_Process_PortIn_GetMax(techno_delay_t* data, netlist_port_t* port);

double* Techno_Delay_PortDelays_GetAdd(techno_delay_t* data, netlist_port_t* port);
double* Techno_Delay_PortDelays_Get(techno_delay_t* data, netlist_port_t* port);

void Techno_Delay(implem_t* Implem);


#endif  // _TECHNO_DELAY_H_

