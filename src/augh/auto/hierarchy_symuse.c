
/*
This file contains various functions to manipulate the internal representation,
based on flags indicating usage of each register bit.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <ctype.h>
#include <assert.h>
#include <inttypes.h>

#include "auto.h"
#include "hierarchy.h"
#include "hierarchy_symuse.h"
#include "../hvex/hvex_misc.h"

extern bool actsimp_verbose;



//=====================================================================
// Allocation
//=====================================================================

// Create a pool of type : bb_symuse_t

#define POOL_prefix      pool_bb_symuse
#define POOL_data_t      bb_symuse_t
#define POOL_ALLOC_SIZE  1000

#define POOL_INSTANCE             POOL_BB_SYMUSE
#define POOL_INSTANCE_ATTRIBUTES  static

#include "../pool.h"

// Allocation wrappers

static bb_symuse_t* bb_symuse_new() {
	bb_symuse_t* elt = pool_bb_symuse_pop(&POOL_BB_SYMUSE);
	memset(elt, 0, sizeof(*elt));
	return elt;
}
void Hier_SymUse_Free(bb_symuse_t* elt) {
	if(elt->bits != NULL) free(elt->bits);
	pool_bb_symuse_push(&POOL_BB_SYMUSE, elt);
}

void Hier_SymUse_Clear(bb_symuse_t* symuse) {
	symuse->allbits = 0;
	memset(symuse->bits, 0, symuse->width * sizeof(*symuse->bits));
}
bb_symuse_t* Hier_SymUse_NewInit(char* name, unsigned width) {
	bb_symuse_t* symuse = bb_symuse_new();
	symuse->name  = name;
	symuse->width = width;
	symuse->bits  = calloc(width, sizeof(*symuse->bits));
	return symuse;
}
bb_symuse_t* Hier_SymUse_GetAdd(avl_pp_tree_t* tree, char* name, unsigned width) {
	avl_pp_node_t* avl_node = NULL;
	bool b = avl_pp_add(tree, name, &avl_node);
	if(b==true) {
		avl_node->data = bb_symuse_new();
		bb_symuse_t* symuse = avl_node->data;
		symuse->name  = name;
		symuse->width = width;
		symuse->bits  = calloc(width, sizeof(*symuse->bits));
	}
	return avl_node->data;
}
bb_symuse_t* Hier_SymUse_Get(avl_pp_tree_t* tree, char* name) {
	bb_symuse_t* symuse = NULL;
	avl_pp_find_data(tree, name, (void**)&symuse);
	return symuse;
}

void Hier_SymUse_Rem(avl_pp_tree_t* tree, char* name) {
	avl_pp_node_t* pp_node = NULL;
	bool b = avl_pp_find(tree, name, &pp_node);
	if(b==true) {
		Hier_SymUse_Free(pp_node->data);
		avl_pp_rem(tree, pp_node);
	}
}

// Clear previous symuse data
void Hier_SymUse_ClearNode(hier_node_t* node) {
	avl_pp_foreach(&node->symuse, scanSym) Hier_SymUse_Free(scanSym->data);
	avl_pp_reset(&node->symuse);
}
void Hier_SymUse_ClearImplem(implem_t* Implem) {
	avl_p_foreach(&Implem->H->NODES_ALL, scan) {
		hier_node_t* node = scan->data;
		avl_pp_foreach(&node->symuse, scanSym) Hier_SymUse_Free(scanSym->data);
		avl_pp_reset(&node->symuse);
	}
}

// Display symbol usage
void Hier_SymUse_FPrint_Bit(FILE* F, uint8_t val) {
	#if 0
	fprintf(F, "%c%c%c",
		(val & BB_SYMUSE_IS_R)!=0 ? 'R' : '-',
		(val & BB_SYMUSE_IS_W)!=0 ? 'W' : '-',
		(val & BB_SYMUSE_IS_LIVE)!=0 ? 'L' : (val & BB_SYMUSE_IS_LA)!=0 ? 'A' : '-'
	);
	#endif
	#if 1
	fprintf(F, "%c%c%c%c",
		(val & BB_SYMUSE_IS_R)!=0    ? 'R' : '-',
		(val & BB_SYMUSE_IS_W)!=0    ? 'W' : '-',
		(val & BB_SYMUSE_IS_LIVE)!=0 ? 'L' : '-',
		(val & BB_SYMUSE_IS_LA)!=0   ? 'A' : '-'
	);
	#endif
}
void Hier_SymUse_FPrint_Sym(FILE* F, bb_symuse_t* symuse) {
	fprintf(F, "'%s' : ", symuse->name);
	Hier_SymUse_FPrint_Bit(F, symuse->allbits);
	fprintf(F, " :");
	for(int i=symuse->width-1; i>=0; i--) {
		uint8_t val = symuse->bits[i];
		fputc(' ', F);
		Hier_SymUse_FPrint_Bit(F, val);
	}
	fputc('\n', F);
}
void Hier_SymUse_FPrint_Implem(FILE* F, implem_t* Implem) {
	fprintf(F, "\n");
	fprintf(F, "==== Begin dump of register usage status in each State\n");
	avl_p_foreach(&Implem->H->NODES[HIERARCHY_STATE], scanState) {
		hier_node_t* node = scanState->data;
		fprintf(F, "\n");
		Hier_PrintNode(node);
		fprintf(F, "Register usage:\n");
		avl_pp_foreach(&node->symuse, scanSrc) {
			bb_symuse_t* symuse = scanSrc->data;
			printf("  ");
			Hier_SymUse_FPrint_Sym(F, symuse);
		}
	}  // Process each State
	fprintf(F, "\n");
}
void Hier_SymUse_FPrint_BB(FILE* F, hier_node_t* nodeBB) {
	hier_bb_t* bb_data = &nodeBB->NODE.BB;
	foreach(bb_data->body, nodeState) {
		fprintf(F, "\n");
		Hier_PrintNode(nodeState);
		fprintf(F, "Register usage:\n");
		avl_pp_foreach(&nodeState->symuse, scanSrc) {
			bb_symuse_t* symuse = scanSrc->data;
			printf("  ");
			Hier_SymUse_FPrint_Sym(F, symuse);
		}
	}  // Process each State
	fprintf(F, "\n");
}



//=====================================================================
// Adding flags R and W
//=====================================================================

void Hier_SymUse_Vex_SetFlag(implem_t* Implem, avl_pp_tree_t* tree, hvex_t* Expr, uint8_t flag) {
	char* name = hvex_vecidx_getname(Expr);
	if(name==NULL) return;  // paranoia
	// Get the component
	netlist_comp_t* comp = Map_Netlist_GetComponent(Implem, name);
	if(comp==NULL) return;
	if(comp->model!=NETLIST_COMP_REGISTER) return;
	// Get the element in the BB data
	bb_symuse_t* symuse = Hier_SymUse_GetAdd(tree, name, ((netlist_register_t*)comp->data)->width);
	// Set flags
	for(unsigned i=0; i<Expr->width; i++) {
		uint8_t* ptr = &symuse->bits[Expr->right + i];
		*ptr |= flag;
		symuse->allbits |= (*ptr);
	}
}
void Hier_SymUse_Action_SetRW(implem_t* Implem, avl_pp_tree_t* tree, hier_action_t* action) {

	// Get all read symbols
	// FIXME this is not efficient because a HUGE lot of little VEX expr can be obtained
	//   and just after, for each occurrence, it is tested whether the symbols are regs, protected etc
	//   these checks happen too often
	chain_list* list_read = NULL;
	list_read = hvex_SearchVector_AddList(list_read, hvex_asg_get_expr(action->expr), NULL);
	list_read = hvex_SearchVector_AddList(list_read, hvex_asg_get_addr(action->expr), NULL);
	list_read = hvex_SearchVector_AddList(list_read, hvex_asg_get_cond(action->expr), NULL);

	// Add Read flags in the State symuse structure
	foreach(list_read, scan) {
		hvex_t* Expr = scan->DATA;
		Hier_SymUse_Vex_SetFlag_R(Implem, tree, Expr);
	}
	freechain(list_read);

	// Get the written symbol
	hvex_t* Expr = hvex_asg_get_dest(action->expr);
	if(Expr!=NULL) {
		Hier_SymUse_Vex_SetFlag_W(Implem, tree, Expr);
	}

}
void Hier_SymUse_State_SetRW(implem_t* Implem, hier_node_t* node) {
	hier_state_t* state_data = &node->NODE.STATE;
	foreach(state_data->actions, scanAct) {
		hier_action_t* action = scanAct->DATA;
		Hier_SymUse_Action_SetRW(Implem, &node->symuse, action);
	}
}

// Remove and re-set all flags R and W
void Hier_SymUse_State_RedoRW(implem_t* Implem, hier_node_t* node) {
	// Remove all flags R and W
	avl_pp_foreach(&node->symuse, scan) {
		bb_symuse_t* symuse = scan->data;
		for(unsigned i=0; i<symuse->width; i++) symuse->bits[i] &= ~(BB_SYMUSE_IS_R | BB_SYMUSE_IS_W);
	}

	// Re-add flags R and W
	Hier_SymUse_State_SetRW(Implem, node);
}



//=====================================================================
// Adding or removing LIVE flag, propagation throughout the Hier graph
//=====================================================================

#define SYMUSE_RECURS_ONLY_ADD 0x01

// Declaration
static void Hier_SymUse_RecursUpdLive_PrevBB(bb_symuse_t* symuse_src, hier_node_t* nodeBB, int flags);

// Backward propagation: remove, or add LIVE flag
// The input symuse structure is from ANOTHER, next, node
static void Hier_SymUse_RecursUpdLive_FromState(bb_symuse_t* symuse_src, hier_node_t* node, int flags) {

	do {
		// Get/create local symuse structure if needed
		bb_symuse_t* symuse_dest = Hier_Node_SymUse_GetAdd(node, symuse_src->name, symuse_src->width);

		// Reset current global field, it is rebuild right after
		symuse_dest->allbits = 0;

		// Process all bits
		unsigned count_new = 0;
		for(unsigned i=0; i<symuse_src->width; i++) {
			uint8_t val_src = symuse_src->bits[i];
			uint8_t val_dest = symuse_dest->bits[i];

			// Note about param flag SYMUSE_RECURS_ONLY_ADD:
			// If updating, symuse_source is supposed to contain the OR of all nextBB
			// But if only adding fields, this OR was not done, so we must not delete anything

			if( (val_src & (BB_SYMUSE_IS_R | BB_SYMUSE_IS_LIVE)) != 0 ) {
				// Add the flag LA
				if( (val_dest & BB_SYMUSE_IS_LA) != 0 ) goto GLOBFIELD;
				val_dest |= BB_SYMUSE_IS_LA;
				// Case of the flag LIVE
				if( (val_dest & BB_SYMUSE_IS_W) != 0 ) {
					if( (flags & SYMUSE_RECURS_ONLY_ADD) != 0 ) goto GLOBFIELD;
					// Clear the flag LIVE
					if( (val_dest & BB_SYMUSE_IS_LIVE) == 0) goto GLOBFIELD;
					val_dest &= ~BB_SYMUSE_IS_LIVE;
				}
				else {
					// Add the flag LIVE
					val_dest |= BB_SYMUSE_IS_LIVE;
				}
			}
			else {
				if( (flags & SYMUSE_RECURS_ONLY_ADD) != 0 ) goto GLOBFIELD;
				// Remove the flags LIVE, LA
				if( (val_dest & (BB_SYMUSE_IS_LA | BB_SYMUSE_IS_LIVE)) == 0 ) goto GLOBFIELD;
				val_dest &= ~(BB_SYMUSE_IS_LA | BB_SYMUSE_IS_LIVE);
				if( (val_dest & (BB_SYMUSE_IS_R | BB_SYMUSE_IS_W)) != 0 ) goto GLOBFIELD;
			}

			count_new++;

			GLOBFIELD:
			symuse_dest->bits[i] = val_dest;
			symuse_dest->allbits |= val_dest;
		}

		// If no flag remains, propagation is finished for this series of States
		if(symuse_dest->allbits==0) {
			// Note: we can't remove the SymUse structure because of recursion
			//Hier_Node_SymUse_Rem(node, symuse_src->name);
			return;
		}

		// If nothing was added/removed, propagation is finished for this series of States
		if(count_new==0) return;

		// Continue backward scan
		symuse_src = symuse_dest;
		if(node->PREV==NULL) break;
		node = node->PREV;

	} while(1);

	// Continue the propagation in the previous BBs
	hier_node_t* parent_bb = node->PARENT;
	assert(parent_bb!=NULL);
	Hier_SymUse_RecursUpdLive_PrevBB(symuse_src, parent_bb, flags);
}

// The input symuse structure is from ANOTHER, next, node
static void Hier_SymUse_RecursUpdLive_PrevBB(bb_symuse_t* symuse_src, hier_node_t* nodeBB, int flags) {
	hier_bb_t* cur_bb = &nodeBB->NODE.BB;

	if( (flags & SYMUSE_RECURS_ONLY_ADD) == 0 ) {
		goto FULLCHECK;
	}

	// Propagate in all previous BBs
	foreach(cur_bb->prev_bbs, scan) {
		hier_node_t* prevBB = scan->DATA;
		hier_bb_t* prevbb_data = &prevBB->NODE.BB;
		if(prevbb_data->body!=NULL) {
			hier_node_t* lastState = Hier_Level_GetLastNode(prevbb_data->body);
			Hier_SymUse_RecursUpdLive_FromState(symuse_src, lastState, flags);
		}
		else {
			Hier_SymUse_RecursUpdLive_PrevBB(symuse_src, prevBB, flags);
		}
	}  // Scan all prevBB

	return;


	bb_symuse_t* symuse_allnext = NULL;

	FULLCHECK:

	symuse_allnext = Hier_SymUse_NewInit(symuse_src->name, symuse_src->width);

	// Propagate in all previous BBs
	foreach(cur_bb->prev_bbs, scan) {
		hier_node_t* prevBB = scan->DATA;
		hier_bb_t* prevbb_data = &prevBB->NODE.BB;

		// To remove LIVE flags in the prevBB, we must check in ALL nextBB of prevBB
		bb_symuse_t* symuse_usethis = symuse_src;
		if(ChainList_Count(prevbb_data->next_bbs)==1) goto DOSCAN;
		symuse_usethis = symuse_allnext;

		// Get the SymUse of all nextBB
		Hier_SymUse_Clear(symuse_allnext);
		foreach(prevbb_data->next_bbs, scan) {
			hier_node_t* nextBB = scan->DATA;
			hier_bb_t* nextbb_data = &nextBB->NODE.BB;
			assert(nextbb_data->body!=NULL);
			hier_node_t* firstState = nextbb_data->body;
			bb_symuse_t* symuse = Hier_Node_SymUse_Get(firstState, symuse_src->name);
			if(symuse==NULL) continue;
			// Copy everything in the temporary symuse structure
			for(unsigned i=0; i<symuse_src->width; i++) symuse_allnext->bits[i] |= symuse->bits[i];
			symuse_allnext->allbits |= symuse->allbits;
		}

		DOSCAN:

		if(prevbb_data->body!=NULL) {
			hier_node_t* lastState = Hier_Level_GetLastNode(prevbb_data->body);
			Hier_SymUse_RecursUpdLive_FromState(symuse_usethis, lastState, flags);
		}
		else {
			Hier_SymUse_RecursUpdLive_PrevBB(symuse_usethis, prevBB, flags);
		}

	}  // Scan the prev BB

	// Clean
	Hier_SymUse_Free(symuse_allnext);

}



// Update the flags LIVE, LA from the given node and ONLY for one symbol
__attribute((__unused__))
static void Hier_SymUse_State_SymUpdLive(hier_node_t* node, bb_symuse_t* symuse) {

	// Reset global field
	symuse->allbits = 0;

	// Keep the flags R, W and LA.
	// Update the flag LIVE depending on flags W and LA (R only intervenes during propagation).
	for(unsigned i=0; i<symuse->width; i++) {
		uint8_t* pval = &symuse->bits[i];
		// Flag LIVE should be set if there is LA but not W.
		if( ((*pval) & BB_SYMUSE_IS_LA) != 0 && ((*pval) & BB_SYMUSE_IS_W) == 0 ) (*pval) |= BB_SYMUSE_IS_LIVE;
		else (*pval) &= ~BB_SYMUSE_IS_LIVE;
		symuse->allbits |= (*pval);
	}

	// Propagate the LIVE flag
	if(node->PREV!=NULL) {
		Hier_SymUse_RecursUpdLive_FromState(symuse, node->PREV, 0);
	}
	else {
		assert(node->PARENT!=NULL);
		Hier_SymUse_RecursUpdLive_PrevBB(symuse, node->PARENT, 0);
	}

	// If the symbol is completely unused, remove the SymUse structure
	if(symuse->allbits==0) {
		Hier_Node_SymUse_Rem(node, symuse->name);
		return;
	}

}
// For re-building entire Hier. Only add flags.
static void Hier_SymUse_State_SymSetLive(hier_node_t* node, bb_symuse_t* symuse) {

	// Check if something to do
	unsigned nb = 0;
	for(unsigned i=0; i<symuse->width; i++) {
		uint8_t val = symuse->bits[i];
		if( (val & BB_SYMUSE_IS_R) == 0 ) continue;
		if( (val & BB_SYMUSE_IS_LIVE) != 0 ) continue;
		nb++;
	}

	// Nothing to do, or everything is already set
	if(nb==0) return;

	// Propagate the LIVE flag
	if(node->PREV!=NULL) {
		Hier_SymUse_RecursUpdLive_FromState(symuse, node->PREV, SYMUSE_RECURS_ONLY_ADD);
	}
	else {
		assert(node->PARENT!=NULL);
		Hier_SymUse_RecursUpdLive_PrevBB(symuse, node->PARENT, SYMUSE_RECURS_ONLY_ADD);
	}

}

// Note: this only updates the flags LIVE, LA. The flags W, R are assumed to be correct.
void Hier_SymUse_State_UpdLive(implem_t* Implem, hier_node_t* node) {

	// Little fix about flags LIVE, LA, and re-compute global field
	avl_pp_foreach(&node->symuse, scan) {
		bb_symuse_t* symuse = scan->data;
		// Reset global field
		symuse->allbits = 0;
		// Scan all bits
		for(unsigned i=0; i<symuse->width; i++) {
			uint8_t* pval = &symuse->bits[i];
			if( ((*pval) & BB_SYMUSE_IS_W) != 0 ) (*pval) &= ~BB_SYMUSE_IS_LIVE;
			else {
				if( ((*pval) & BB_SYMUSE_IS_LIVE) == 0 ) (*pval) &= ~BB_SYMUSE_IS_LA;
			}
			symuse->allbits |= (*pval);
		}
	}

	// Propagate the flag LIVE
	avl_pp_foreach(&node->symuse, scan) {
		bb_symuse_t* symuse = scan->data;
		if(node->PREV!=NULL) {
			Hier_SymUse_RecursUpdLive_FromState(symuse, node->PREV, 0);
		}
		else {
			assert(node->PARENT!=NULL);
			Hier_SymUse_RecursUpdLive_PrevBB(symuse, node->PARENT, 0);
		}
	}

	// Remove SymUse structure when the symbol is completely unused
	chain_list* list = NULL;
	avl_pp_foreach(&node->symuse, scan) {
		bb_symuse_t* symuse = scan->data;
		if(symuse->allbits==0) list = addchain(list, symuse);
	}
	foreach(list, scan) {
		bb_symuse_t* symuse = scan->DATA;
		Hier_Node_SymUse_Rem(node, symuse->name);
	}
	freechain(list);
}

// Clear and compute again the life scope of all registers
void Hier_SymUse_FillHier(implem_t* Implem) {

	// Clear previous symuse data
	Hier_SymUse_ClearImplem(Implem);

	// Initialize all States with flags R and W
	avl_p_foreach(&Implem->H->NODES[HIERARCHY_STATE], scanState) {
		hier_node_t* node = scanState->data;
		Hier_SymUse_State_SetRW(Implem, node);
	}

	#if 0  // FOR DEBUG
	Hier_SymUse_Print(Implem);
	#endif

	// Building of the lists of previous and next BBs
	Hier_ListNextBBofBB(Implem->H);

	// Add the LIVE flags and propagate them
	// Find where each symbol is read, propagate it backwards
	avl_p_foreach(&Implem->H->NODES[HIERARCHY_STATE], scanState) {
		hier_node_t* node = scanState->data;
		avl_pp_foreach(&node->symuse, scanSrc) {
			bb_symuse_t* symuse = scanSrc->data;
			Hier_SymUse_State_SymSetLive(node, symuse);
		}
	}

	#if 0  // Print, for debug
	Hier_SymUse_Print(Implem);
	#endif

}



#if 0  // Note: not used
static void Hier_SymUse_Fill_Level(implem_t* Implem, hier_node_t* node, avl_pp_tree_t* dest) {

	// Scan all states and Actions
	for( ; node!=NULL; node=node->NEXT) {
		// Scan all elements in the current node, add in the tree
		avl_pp_foreach(&node->symuse, scanSrc) {
			char* name = scanSrc->key;
			bb_symuse_t* symuse_src = scanSrc->data;
			// Get the element in the destination tree
			avl_pp_node_t* avl_node = NULL;
			bool b = avl_pp_add(dest, name, &avl_node);
			if(b==true) {
				avl_node->data = bb_symuse_new();
				bb_symuse_t* symuse_dest = avl_node->data;
				symuse_dest->width = symuse_src->width;
				symuse_dest->bits = calloc(symuse_dest->width, sizeof(*symuse_dest->bits));
			}
			bb_symuse_t* symuse_dest = avl_node->data;
			for(unsigned i=0; i<symuse_dest->width; i++) {
				uint8_t val_src = symuse_src->bits[i];
				uint8_t* ptr_dest = &symuse_dest->bits[i];
				*ptr_dest |= val_src & (BB_SYMUSE_IS_R | BB_SYMUSE_IS_W);
				if( (val_src & BB_SYMUSE_R_FIRST) != 0 && ((*ptr_dest) & BB_SYMUSE_W_FIRST) == 0 ) *ptr_dest |= BB_SYMUSE_R_FIRST;
				if( (val_src & BB_SYMUSE_W_FIRST) != 0 && ((*ptr_dest) & BB_SYMUSE_R_FIRST) == 0 ) *ptr_dest |= BB_SYMUSE_W_FIRST;
			}

		}  // Source symbols
	}  // Nodes

	// Set general flags
	avl_pp_foreach(dest, scan) {
		bb_symuse_t* symuse = scan->data;
		for(unsigned i=0; i<symuse->width; i++) {
			symuse->access_in_bb |= symuse->bits[i];
		}
	}

}
#endif



//=====================================================================
// Simplification of Actions based on the SymUse flags
//=====================================================================

typedef struct symuse_propag_data_t  symuse_propag_data_t;
struct symuse_propag_data_t {
	implem_t*      Implem;
	hier_node_t*   state;
	hier_action_t* action;
	// Info about the destination symbol
	hvex_t*        vex_dest;
	char*          name_dest;
	bb_symuse_t*   symuse;
	bool           dest_overwritten;
	// The symbols in the original expression
	avl_pp_tree_t* read_names;
	bool           read_overwritten;
	// The States already scanned
	avl_p_tree_t*  scanned_states;
	// The results
	bool           act_accepted;
	bool           recurs_error;
	bool           recurs_done;
	bitype_list*   list_occur;    // Occurrences of the ref dest symbol (FROM=Action, TO=vexexpr**)
	chain_list*    list_states_w;  // The other States where the ref symbol is written
};

static void Hier_SymUse_PropagAct_DoState(hier_node_t* nodeState, symuse_propag_data_t* data) {

	// Get the symuse structure for the ref symbol
	bb_symuse_t* symuse = Hier_Node_SymUse_Get(nodeState, data->name_dest);
	if(symuse==NULL) {
		// Assume the symbol is completely unused, not live, nothing
		data->recurs_done = true;
		return;
	}

	// Check if the ref dest symbol is still live, or read
	bool level_finished = true;
	for(unsigned i=data->vex_dest->right; i<=data->vex_dest->left; i++) {
		if((symuse->bits[i] & (BB_SYMUSE_IS_R | BB_SYMUSE_IS_LIVE)) != 0 ) { level_finished = false; break; }
	}
	if(level_finished==true) {
		data->recurs_done = true;
		return;
	}

	// Check if this State has already been scanned
	// Note: the reference State IS this tree
	if(avl_p_isthere(data->scanned_states, nodeState)==true) {
		// Yes, already scanned => abort recursion
		data->recurs_error = true;
		data->recurs_done = true;
		return;
	}
	// Flag the State as already scanned
	avl_p_add_overwrite(data->scanned_states, nodeState, nodeState);

	// Check if the ref dest symbol is written and if it's fully written
	bool is_w = false;
	bool is_w_full = true;
	for(unsigned i=data->vex_dest->right; i<=data->vex_dest->left; i++) {
		if( (symuse->bits[i] & BB_SYMUSE_IS_W) != 0 ) is_w = true;
		else is_w_full = false;
	}

	// Handle when the dest symbol is written
	if(is_w==true) {
		// If not fully written, abort recursion
		if(is_w_full==false) {
			data->recurs_error = true;
			data->recurs_done = true;
			return;
		}
		// Save this State in the list where the dest symbol is written
		data->list_states_w = addchain(data->list_states_w, nodeState);
	}
	else {
		// Check if any symbol read in the ref Action is written here
		bool read_is_w = false;
		avl_pp_foreach(data->read_names, scan) {
			char* name = scan->key;
			hvex_t* vex = scan->data;
			bb_symuse_t* symuse_read = Hier_Node_SymUse_Get(nodeState, name);
			if(symuse_read==NULL) continue;
			for(unsigned i=vex->right; i<=vex->left; i++) {
				if((symuse_read->bits[i] & BB_SYMUSE_IS_W)!=0) { read_is_w = true; break; }
			}
			if(read_is_w==true) break;
		}
		// If any symbol read in the ref expr is written here, abort recursion
		if(read_is_w==true) {
			data->recurs_error = true;
			data->recurs_done = true;
			return;
		}
	}

	// Find all occurrences of the ref symbol, in the current state
	hier_state_t* state_data = &nodeState->NODE.STATE;
	foreach(state_data->actions, scanAction) {
		hier_action_t* action = scanAction->DATA;
		chain_list* list = NULL;
		list = hvex_SearchSym_AddList(list, hvex_asg_get_expr(action->expr), data->name_dest);
		list = hvex_SearchSym_AddList(list, hvex_asg_get_addr(action->expr), data->name_dest);
		list = hvex_SearchSym_AddList(list, hvex_asg_get_cond(action->expr), data->name_dest);
		// Add to the results with the Action pointer
		foreach(list, scanOccur) {
			data->list_occur = addbitype(data->list_occur, 0, action, scanOccur->DATA);
		}
		freechain(list);
	}

	if(is_w==true) {
		// The ref symbol is overwritten
		//   And we assume it is written full (the check was done above)
		data->recurs_done = true;
		return;
	}

	// Here, the ref symbol was not overwritten
	// Check if some bits written in the ref Action are still live After
	bool still_live = false;
	for(unsigned i=data->vex_dest->right; i<=data->vex_dest->left; i++) {
		if( (symuse->bits[i] & BB_SYMUSE_IS_LA) != 0 ) { still_live = true; break; }
	}
	// If no bit is live after, then the recursion is finished (clean)
	if(still_live==false) {
		data->recurs_done = true;
		return;
	}

}
// Declaration
static void Hier_SymUse_PropagAct_AfterBB(hier_node_t* nodeBB, symuse_propag_data_t* data);
// Note: the recursion is on the BBs, not the States, to limit the recursion depth
static void Hier_SymUse_PropagAct_FromBB(hier_node_t* nodeBB, symuse_propag_data_t* data) {
	hier_bb_t* bb_data = &nodeBB->NODE.BB;
	// Scan the body, if any
	for(hier_node_t* node=bb_data->body; node!=NULL; node=node->NEXT) {
		Hier_SymUse_PropagAct_DoState(node, data);
		if(data->recurs_done==true) return;
	}
	Hier_SymUse_PropagAct_AfterBB(nodeBB, data);
}
static void Hier_SymUse_PropagAct_AfterBB(hier_node_t* nodeBB, symuse_propag_data_t* data) {
	hier_bb_t* bb_data = &nodeBB->NODE.BB;
	// For the scan of other BBs, we work on a separate data structure
	symuse_propag_data_t otherData_i = *data;
	symuse_propag_data_t* otherData = &otherData_i;
	// Scan the next BBs
	foreach(bb_data->next_bbs, scanBB) {
		hier_node_t* otherBB = scanBB->DATA;
		hier_bb_t* otherbb_data = &otherBB->NODE.BB;
		// Forbid situations where a nextBB has several prevBBs
		if(otherbb_data->prev_bbs!=NULL && otherbb_data->prev_bbs->NEXT!=NULL) {
			data->recurs_error = true;
			break;
		}
		// Reset result fields
		otherData->recurs_error = false;
		otherData->recurs_done = false;
		otherData->list_occur = NULL;
		otherData->list_states_w = NULL;
		// Launch the scan
		Hier_SymUse_PropagAct_FromBB(otherBB, otherData);
		// Scan results
		data->list_occur    = ChainBiType_Concat(otherData->list_occur, data->list_occur);
		data->list_states_w = append(otherData->list_states_w, data->list_states_w);
		if(otherData->recurs_error==true) {
			data->recurs_error = true;
			break;
		}
	}  // Scan all nextBBs
	data->recurs_done = true;
}
static void Hier_SymUse_PropagAct_AfterState(hier_node_t* nodeState, symuse_propag_data_t* data) {
	// Scan the next nodes, if any
	for(hier_node_t* node=nodeState->NEXT; node!=NULL; node=node->NEXT) {
		Hier_SymUse_PropagAct_DoState(node, data);
		if(data->recurs_done==true) return;
	}
	// Get the parent BB
	hier_node_t* nodeBB = Hier_GetParentNode(data->state);
	assert(nodeBB->TYPE==HIERARCHY_BB);
	Hier_SymUse_PropagAct_AfterBB(nodeBB, data);
}

static void Hier_SymUse_PropagAct(symuse_propag_data_t* data) {
	data->act_accepted = false;

	// Refuse wired conditions
	if(hvex_asg_get_cond(data->action->expr)!=NULL) return;

	// Handle only very simple Actions
	// Only vex ATOM types or CONCAT
	// and only non-protected registers
	hvex_t* vex_expr = hvex_asg_get_expr(data->action->expr);
	assert(vex_expr!=NULL);

	if(vex_expr->model==HVEX_LITERAL) data->act_accepted = true;
	else if(vex_expr->model==HVEX_VECTOR) data->act_accepted = true;

	else if(vex_expr->model==HVEX_CONCAT) {
		bool operands_accepted = true;
		hvex_foreach(vex_expr->operands, VexOp) {
			if(VexOp->model!=HVEX_LITERAL && VexOp->model!=HVEX_VECTOR) { operands_accepted = false; break; }
		}
		if(operands_accepted==true) data->act_accepted = true;
	}

	if(data->act_accepted==false) return;

	// Get the names of the symbols, check
	hvex_SearchVectors_treevex(vex_expr, data->read_names);

	avl_pp_foreach(data->read_names, scanName) {
		char* name = scanName->key;
		if(name==data->name_dest) continue;
		// Only registers
		netlist_comp_t* comp = Netlist_Comp_GetChild(data->Implem->netlist.top, name);
		if(comp==NULL) { data->act_accepted=false; break; }
		if(comp->model!=NETLIST_COMP_REGISTER) { data->act_accepted=false; break; }
		// No protected symbols
		if(Implem_SymFlag_ChkAnyProtect(data->Implem, name)==true) { data->act_accepted=false; break; }
	}

	if(data->act_accepted==false) return;

	hier_state_t* state_data = &data->state->NODE.STATE;

	// Check whether some read symbols are written in ANOTHER Action in the same State
	data->read_overwritten = false;
	foreach(state_data->actions, scanAction_other) {
		hier_action_t* action_other = scanAction_other->DATA;
		if(action_other==data->action) continue;
		hvex_t* vex_dest_other = hvex_asg_get_dest(action_other->expr);
		if(vex_dest_other==NULL) continue;
		char* dest_name_other = hvex_vec_getname(vex_dest_other);
		assert(dest_name_other!=NULL);
		// Get the accessed VEX of the reference Action
		hvex_t* vex_read = NULL;
		bool b = avl_pp_find_data(data->read_names, dest_name_other, (void**)&vex_read);
		if(b==false) continue;
		// Compare indexes
		if(vex_dest_other->left < vex_read->right || vex_dest_other->right > vex_read->left) continue;
		data->read_overwritten = true;
		data->act_accepted = false;
		break;
	}

	if(data->act_accepted==false) return;

	// Check whether some bits of the dest name are overwritten in the reference Action
	hvex_t* vex_read = NULL;
	bool b = avl_pp_find_data(data->read_names, data->name_dest, (void**)&vex_read);
	if(b==true) {
		if(data->vex_dest->left < vex_read->right || data->vex_dest->right > vex_read->left) {}
		else data->dest_overwritten = true;
	}

	// Now the big scan can begin
	// Recursively scan the Actions in all nextBB
	//   and take care to BBs already scanned (loops...)
	// WARNING: if anything is propagated, all symuse flags must be rebuilt!!

	avl_p_add_overwrite(data->scanned_states, data->state, data->state);

	data->recurs_error = false;
	data->recurs_done = false;

	Hier_SymUse_PropagAct_AfterState(data->state, data);

}

int Hier_SymUse_RemUnusedAct(implem_t* Implem) {
	unsigned count = 0;

	// For debug: this function is not stable yet.
	//return 0;

	Hier_SymUse_FillHier(Implem);

	// Process all States, all Actions
	avl_p_foreach(&Implem->H->NODES[HIERARCHY_STATE], scanState) {
		hier_node_t* nodeState = scanState->data;
		hier_state_t* state_data = &nodeState->NODE.STATE;

		// The list of Actions to remove in this State
		chain_list* list_actions = dupchain(state_data->actions);

		foreach(list_actions, scanAction) {
			hier_action_t* action = scanAction->DATA;
			hvex_t* vex_dest = hvex_asg_get_dest(action->expr);
			if(vex_dest==NULL) continue;
			char* dest_name = hvex_vec_getname(vex_dest);
			assert(dest_name!=NULL);

			// Only registers
			netlist_comp_t* comp = Netlist_Comp_GetChild(Implem->netlist.top, dest_name);
			if(comp==NULL) continue;
			if(comp->model!=NETLIST_COMP_REGISTER) continue;

			// The dest symbol must not be protected
			if(Implem_SymFlag_ChkAnyProtect(Implem, dest_name)==true) continue;

			// Scan bits usage
			bb_symuse_t* symuse = Hier_Node_SymUse_Get(nodeState, dest_name);
			assert(symuse!=NULL);

			// Count the number of unused bits on the LEFT side
			bool found_used = false;
			unsigned unused_left = 0;
			for(int i=vex_dest->left; i>=(int)vex_dest->right; i--) {
				if((symuse->bits[i] & BB_SYMUSE_IS_LA)!=0) { found_used = true; break; }
				unused_left++;
			}

			// This Action is entirely unused. Remove it.
			if(found_used==false) {
				if(actsimp_verbose==true) {
					printf("Info: Removing unuseful Action: ");
					HierAct_Print(action);
				}

				Hier_Action_FreeFull(action);
				Hier_SymUse_State_Upd(Implem, nodeState);

				count ++;
				continue;
			}

			// Count the number of unused bits on the RIGHT side
			unsigned unused_right = 0;

			// WARNING
			// Cropping right is currently disabled because the mapper extends assignations only on the left
			// So, an assignation to reg(8 downto 4) becomes an assignation to reg(3 downto 0) with (un)signed extension
			#if 0
			for(unsigned i=vex_dest->RIGHT; i<=vex_dest->LEFT; i++) {
				if((symuse->bits[i] & BB_SYMUSE_IS_LA)!=0) break;
				unused_right++;
			}
			#endif

			if(unused_left>0 || unused_right>0) {
				if(actsimp_verbose==true) {
					printf("Cropping Action [%u-%u]: ", unused_left, unused_right);
					HierAct_Print(action);
				}

				// Replace the expression
				hvex_t* Expr = hvex_asg_get_expr(action->expr);
				assert(Expr!=NULL);
				hvex_croprel(Expr, unused_left, unused_right);
				// Replace the destination
				vex_dest->left -= unused_left;
				vex_dest->right += unused_right;
				vex_dest->width = vex_dest->left - vex_dest->right + 1;
				// Update the ASG operation
				action->expr->right = 0;
				action->expr->left = vex_dest->width - 1;
				action->expr->width = vex_dest->width;

				if(actsimp_verbose==true) {
					printf("               Obtained: ");
					HierAct_Print(action);
				}

				Hier_SymUse_State_Upd(Implem, nodeState);

				// Add to the number of things done
				count ++;
				continue;
			}

  		// For debug: skip the propagation functionality
			//continue;

			// Here we check if the Action can be propagated into the next Actions
			symuse_propag_data_t data_i;
			symuse_propag_data_t* data = &data_i;

			data->Implem = Implem;
			data->state  = nodeState;
			data->action = action;

			data->vex_dest  = vex_dest;
			data->name_dest = dest_name;
			data->symuse    = symuse;
			data->dest_overwritten = false;
			data->read_overwritten = false;

			avl_pp_tree_t read_names;
			avl_pp_init(&read_names);
			data->read_names = &read_names;

			avl_p_tree_t scanned_states;
			avl_p_init(&scanned_states);
			data->scanned_states = &scanned_states;

			data->act_accepted = false;
			data->list_occur = NULL;
			data->list_states_w = NULL;
			data->recurs_error = false;
			data->recurs_done = false;

			// Launch the actual search
			Hier_SymUse_PropagAct(data);
			if(data->act_accepted==false) goto CLEANUP;
			if(data->recurs_error==true) goto CLEANUP;

			// If the ref symbol was read only in the Actions where is is written
			//   And if the dest symbols are different,
			//   OR if the expr are the same where the dest symbols overlap
			//   AND if none of these dest symbols have been Written since the present State
			//   Then move these Actions in the present State
			// FIXME TODO
			// Note: in this case, more cases can be accepted in the reference Action:
			//   wired conditions, all kinds of ASG, signals, operations etc
			// Note: it would be much more appropriate to scan the graph backwards...

			// Replace the occurrences of the ref symbol where it was found
			// And remove the present Action

			// If the Act must be kept, perform asg propag only if the expr is a literal
			//   (so, no modification in data dependencies nor life scope of the ref symbol)
			// FIXME TODO

			// FIXME If the ref symbol can be cropped right, and all read occurrences have been identified,
			//   then move the indexes used on the right

			hvex_t* vex_expr = hvex_asg_get_expr(action->expr);

			// The State nodes to simplify
			avl_p_tree_t tree_nodes_simp;
			avl_p_init(&tree_nodes_simp);

			foreach(data->list_occur, scanOccur) {
				hier_action_t* action = scanOccur->DATA_FROM;
				avl_p_add_overwrite(&tree_nodes_simp, action->node, action->node);
				hvex_t* Expr = scanOccur->DATA_TO;
				hvex_ReplaceSubExpr_FromPrevExpr(Expr, vex_dest, vex_expr);
			}

			if(actsimp_verbose==true) {
				printf("Info: Removing propagated Action: ");
				HierAct_Print(action);
			}
			avl_p_add_overwrite(&tree_nodes_simp, action->node, action->node);
			Hier_Action_FreeFull(action);
			count ++;

			// Update the flags R and W in the affected nodes
			avl_p_foreach(&tree_nodes_simp, scanSimp) {
				hier_node_t* simpState = scanSimp->data;
				Hier_SymUse_State_RedoRW(Implem, simpState);
			}
			// Update the flags LIVE, LA in the affected nodes
			avl_p_foreach(&tree_nodes_simp, scanSimp) {
				hier_node_t* simpState = scanSimp->data;
				Hier_SymUse_State_UpdLive(Implem, simpState);
			}

			avl_p_reset(&tree_nodes_simp);

			CLEANUP:

			freebitype(data->list_occur);
			freechain(data->list_states_w);
			hvex_treevex_reset(data->read_names);
			avl_p_reset(data->scanned_states);

		}  // Scan Actions

		freechain(list_actions);

	}  // Scan States

	return count;
}


