
// Work with component models built from C source

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <ctype.h>
#include <assert.h>
#include <inttypes.h>

#include "auto.h"
#include "hierarchy.h"
#include "implem_models.h"



//=====================================================================
// Various processing functions
//=====================================================================

// FIXME Check for recursive inclusions?
static void ImpMod_Count_recurs(implem_t* Implem) {
	// Increment the count
	Implem->compmod.instance_nb++;
	// Go recursive
	avl_pp_foreach(&Implem->netlist.top->children, scan) {
		netlist_comp_t* comp = scan->data;
		if( (comp->flags & NETLIST_COMP_ISEXTERN) == 0 ) continue;
		netlist_top_t* top_data = comp->data;
		implem_t* modImplem = augh_modimplem_get(Implem->env.globals, top_data->extern_model_name);
		assert(modImplem!=NULL);
		ImpMod_Count_recurs(modImplem);
	}
}
void ImpMod_Count_Update(augh_globals_t* globals) {
	// Reset all instance counts
	avl_pp_foreach(&globals->implem_models, scan) {
		implem_t* Implem = scan->data;
		Implem->compmod.instance_nb = 0;
	}
	// Launch the recursive count
	ImpMod_Count_recurs(globals->top_implem);
}

// This is a recursive function
static void ImpMod_ComputeSizes_Model(implem_t* Implem) {
	// If the size of this model is already computed, return
	if(Implem->CORE.eval_done==true) return;

	augh_globals_t* globals = Implem->env.globals;

	// The list of models this Implem depends on
	// Key = model name, data = number of instances
	avl_pi_tree_t tree_models_nb;
	avl_pi_init(&tree_models_nb);

	// Scan all child components, count the number of other Implem models
	avl_pp_foreach(&Implem->netlist.top->children, scanComp) {
		netlist_comp_t* comp = scanComp->data;
		if( (comp->flags & NETLIST_COMP_ISEXTERN) != 0 ) {
			netlist_top_t* top_data = comp->data;
			avl_pi_node_t* pi_node = NULL;
			bool b = avl_pi_add(&tree_models_nb, top_data->extern_model_name, &pi_node);
			if(b==true) pi_node->data = 0;
			pi_node->data ++;
		}
		else {
			ptype_list* local_size = Techno_EvalSize_Comp(Implem->synth_target, comp);
			if(local_size!=NULL) {
				Implem->CORE.resources_opers = Techno_NameOpRes_Add_One(
					Implem->CORE.resources_opers, comp->model->name, 1, local_size
				);
				freeptype(local_size);
			}
		}
	}

	// Recursively launch computing on these Implem models
	avl_pi_foreach(&tree_models_nb, scanMod) {
		char* modname = scanMod->key;
		implem_t* modImplem = augh_modimplem_get(globals, modname);
		assert(modImplem!=NULL);
		ImpMod_ComputeSizes_Model(modImplem);
		// Add this model to the resources
		Implem->CORE.resources_opers = Techno_NameOpRes_Add_One(
			Implem->CORE.resources_opers, modname, scanMod->data, modImplem->CORE.resources_total
		);
	}

	// Clear tree of Implem models
	avl_pi_reset(&tree_models_nb);

	// Set the sum of resources
	Implem->CORE.resources_total = Techno_NameOpRes_GetTotal(Implem->CORE.resources_opers);

	Implem->CORE.eval_done = true;
}
// Compute the size of ALL Implem models
void ImpMod_ComputeSizes(augh_globals_t* globals) {
	// Reset all sizes
	avl_pp_foreach(&globals->implem_models, scan) {
		implem_t* Implem = scan->data;
		if(Implem->CORE.resources_opers!=NULL) Techno_NameOpRes_Free(Implem->CORE.resources_opers);
		Implem->CORE.resources_opers = NULL;
		if(Implem->CORE.resources_total!=NULL) freeptype(Implem->CORE.resources_total);
		Implem->CORE.resources_total = NULL;
		Implem->CORE.eval_done = false;
	}
	// Launch on top-level Implem, it's recursive
	ImpMod_ComputeSizes_Model(globals->top_implem);
}

// This is a recursive function
static void ImpMod_ComputeTimes_Model(implem_t* Implem) {
	// If the size of this model is already computed, return
	if(Implem->CORE.eval_done==true) return;

	augh_globals_t* globals = Implem->env.globals;

	// The list of models this Implem depends on
	// Key = model name, data = number of instances
	avl_pi_tree_t tree_models_nb;
	avl_pi_init(&tree_models_nb);

	// Scan all child components, count the number of other Implem models
	avl_pp_foreach(&Implem->netlist.top->children, scanComp) {
		netlist_comp_t* comp = scanComp->data;
		if( (comp->flags & NETLIST_COMP_ISEXTERN) != 0 ) {
			netlist_top_t* top_data = comp->data;
			avl_pi_node_t* pi_node = NULL;
			bool b = avl_pi_add(&tree_models_nb, top_data->extern_model_name, &pi_node);
			if(b==true) pi_node->data = 0;
			pi_node->data ++;
		}
	}

	// Recursively launch computing on these Implem models
	avl_pi_foreach(&tree_models_nb, scanMod) {
		char* modname = scanMod->key;
		implem_t* modImplem = augh_modimplem_get(globals, modname);
		assert(modImplem!=NULL);
		ImpMod_ComputeTimes_Model(modImplem);
		// Add this model to the resources
		minmax_addmult(&Implem->CORE.execution_time, &modImplem->CORE.execution_time, scanMod->data);
	}

	// Clear tree of Implem models
	avl_pi_reset(&tree_models_nb);

	// Take into account the local design
	minmax_add(&Implem->CORE.execution_time, &Implem->time.execution_cycles);

	Implem->CORE.eval_done = true;
}
// Compute the exec time of ALL Implem models
void ImpMod_ComputeTimes(augh_globals_t* globals) {
	// Reset all times
	avl_pp_foreach(&globals->implem_models, scan) {
		implem_t* Implem = scan->data;
		minmax_set_cst(&Implem->CORE.execution_time, 0);
		Implem->CORE.eval_done = false;
	}
	// Launch on top-level Implem, it's recursive
	ImpMod_ComputeTimes_Model(globals->top_implem);
}

// Input string: "comp.access" (the name can be the top-level component)
//      Or just: "access" (and the component is the top-level component)
netlist_access_t* ImpMod_GetAccess_User(implem_t* Implem, const char* str, bool verbose) {
	unsigned str_len = strlen(str);
	char const * ptr_beg = str;
	unsigned length = 0;

	// Get the component name
	do {
		char c = ptr_beg[length];
		if(c==0) break;
		if(c=='.') break;
		length++;
	} while(1);
	if(length==0) {
		if(verbose==true) {
			printf("Error: Input string seems empty. Can't get component and access structure.\n");
		}
		return NULL;
	}

	char buf[str_len+1];
	memcpy(buf, ptr_beg, length);
	buf[length] = 0;

	// If there is nothing after, assume we have the name of the access from the top-level component
	if(ptr_beg[length]==0) {
		netlist_comp_t* comp = Implem->netlist.top;
		// Get the access from the component
		char* access_name = namealloc(buf);
		netlist_access_t* access = Netlist_Comp_GetAccess(comp, access_name);
		if(access==NULL) {
			if(verbose==true) {
				printf("Error: The component '%s' was not found.\n", access_name);
			}
			return NULL;
		}
		return access;
	}

	// Get the component
	char* comp_name = namealloc(buf);
	netlist_comp_t* comp = NULL;

	if(comp_name==Implem->netlist.top->name) comp = Implem->netlist.top;
	else comp = Netlist_Comp_GetChild(Implem->netlist.top, comp_name);
	if(comp==0) {
		if(verbose==true) {
			printf("Error: The component '%s' was not found.\n", comp_name);
		}
		return NULL;
	}

	// Get the access name
	ptr_beg += length + 1;
	length = 0;
	do {
		char c = ptr_beg[length];
		if(c==0) break;
		if(c=='.') break;
		length++;
	} while(1);
	if(length==0) {
		if(verbose==true) {
			printf("Error: Could not get the name of the access from string '%s'.\n", str);
		}
		return NULL;
	}

	// Get the access from the component
	memcpy(buf, ptr_beg, length);
	buf[length] = 0;
	char* access_name = namealloc(buf);
	netlist_access_t* access = Netlist_Comp_GetAccess(comp, access_name);
	if(access==NULL) {
		if(verbose==true) {
			printf("Error: The access '%s' was not found in component '%s'.\n", access_name, comp_name);
		}
		return NULL;
	}

	return access;
}



//=====================================================================
// Loading a component model from a source file
//=====================================================================

implem_t* ImpMod_Load(augh_globals_t* globals, const char* modname_cst, const char* filename_cst) {
	char* modname = namealloc(modname_cst);
	char* filename = stralloc(filename_cst);

	if(augh_modimplem_get(globals, modname)!=NULL) {
		printf("ERROR: A nomponent model named '%s' already exists.\n", modname);
		return NULL;
	}

	// Create a new Implem
	implem_t* modImplem = augh_implem_new(globals);
	modImplem->compmod.model_name = modname;
	modImplem->env.input_file = filename;
	modImplem->env.wait_on_start = false;

	// Some fixes about component names
	modImplem->env.vhdl_prefix = modname;
	modImplem->env.entity = modname;
	modImplem->netlist.top->name = modname;
	modImplem->netlist.top->flags |= NETLIST_COMP_NOPREFIX;

	// Parse the functionality
	int z = Hier_Load(modImplem, filename);
	if(z!=0) {
		printf("ERROR: Loading the file '%s' failed.\n", filename);
		augh_implem_free(modImplem);
		return NULL;
	}

	// Index in the list of component models
	avl_pp_add_overwrite(&globals->implem_models, modname, modImplem);

	// Inherit the techno and timing of the top-level Implem
	modImplem->synth_target->techno = globals->top_implem->synth_target->techno;
	modImplem->synth_target->timing = globals->top_implem->synth_target->timing;

	return modImplem;
}



//=====================================================================
// Execution of user commands
//=====================================================================

#include "command.h"

int ImpMod_Command_forall(implem_t* Implem, command_t* cmd_data, const char* cmd, bool with_hier) {

	// Grab the entire line
	char* command = NULL;
	int z = Command_PopParam_WholeLine_stralloc_verbose(cmd_data, cmd, &command);
	if(z!=0) return -1;

	// Paranoia: work on a copy of the list of Implem models
	chain_list* listmod = NULL;
	avl_pp_foreach(&Implem->env.globals->implem_models, scan) listmod = addchain(listmod, scan->data);

	// Process all Implem models
	z = 0;
	foreach(listmod, scan) {
		implem_t* impmod = scan->DATA;
		if(with_hier==true && impmod->H==NULL) continue;
		printf("Info: Processing Implem model '%s'...\n", impmod->compmod.model_name);

		// Work on a copy of the command
		char* loc_cmd = NULL;
		if(command!=NULL) loc_cmd = strdup(command);

		// Execute
		z = AughCmd_Exec_str(impmod, loc_cmd);

		// Clean
		if(loc_cmd!=NULL) free(loc_cmd);
		if(z!=0) break;
	}

	// Clean
	freechain(listmod);

	return z;
}

int ImpMod_Command(implem_t* Implem, command_t* cmd_data) {
	const char* cmd = Command_PopParam(cmd_data);
	if(cmd==NULL) {
		printf("Error 'impmod': No command received. Try 'help'.\n");
		return -1;
	}

	if(strcmp(cmd, "help")==0) {
		printf(
			"\n"
			"help                   Display this help.\n"
			"-m <name> <commands>   Launch the specified commands on the Implem model <name>.\n"
			"forall <commands>      Launch the specified commands on all Implem models.\n"
			"forall-hier <commands> Launch the specified commands on all Implem models that have Hier.\n"
			"sizes-compute          Compute the size of all Implem models.\n"
			"sizes-print            Print the size of all Implem models (do not re-compute).\n"
			"times-compute          Compute the exec time of all Implem models.\n"
			"times-print            Print the exec time of all Implem models (do not re-compute).\n"
			"\n"
		);
		return 0;
	}

	// Check if there is the first parameter is "s <modelname>"
	// In this case the corresponding component model is used as targeted
	if(strcmp(cmd, "-m")==0) {
		const char* modname = Command_PopParam_namealloc(cmd_data);
		if(modname==NULL) {
			printf("Error: Missing parameter for command '%s'. Try 'help'.\n", cmd);
			return -1;
		}
		implem_t* impmod = augh_modimplem_get(Implem->env.globals, modname);
		if(impmod==NULL) {
			printf("Error: No Implem model named '%s' was found.\n", modname);
			return -1;
		}
		//return ImpMod_Command(impmod, commands);
		return AughCmd_Exec(impmod, cmd_data);
	}

	// Process normal commands

	if(strcmp(cmd, "forall")==0) {
		return ImpMod_Command_forall(Implem, cmd_data, cmd, false);
	}
	else if(strcmp(cmd, "forall-hier")==0) {
		return ImpMod_Command_forall(Implem, cmd_data, cmd, true);
	}

	else if(strcmp(cmd, "sizes-compute")==0) {
		ImpMod_ComputeSizes(Implem->env.globals);
	}
	else if(strcmp(cmd, "sizes-print")==0) {
		avl_pp_foreach(&Implem->env.globals->implem_models, scan) {
			implem_t* modImplem = scan->data;
			printf("Implem model '%s':\n", modImplem->compmod.model_name);
			Techno_NameOpRes_Print(modImplem->CORE.resources_opers);
		}
	}

	else if(strcmp(cmd, "times-compute")==0) {
		ImpMod_ComputeTimes(Implem->env.globals);
	}
	else if(strcmp(cmd, "times-print")==0) {
		avl_pp_foreach(&Implem->env.globals->implem_models, scan) {
			implem_t* modImplem = scan->data;
			printf("Implem model '%s': \n", modImplem->compmod.model_name);
			minmax_print(&modImplem->time.execution_cycles, "  Local execution time : ", " (clock cycles)\n");
			minmax_print(&modImplem->CORE.execution_time, "  Total execution time : ", " (clock cycles)\n");
		}
	}

	else {
		printf("Error 'impmod': Unknown command '%s'.\n", cmd);
		return -1;
	}

	return 0;
}


