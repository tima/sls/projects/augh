
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>  // For timeout
#include <ctype.h>  // For ParseDelay()
#include <assert.h>
#include <errno.h>

#include "auto.h"
#include "core.h"
#include "hierarchy.h"
#include "schedule.h"
#include "techno.h"
#include "forksynth.h"
#include "addops.h"
#include "memtransfo.h"
#include "map.h"
#include "linelist.h"

#include <sys/stat.h>
#include <fcntl.h>
#include <sys/types.h>
#include <dirent.h>  // To scan directories


// Shared data
char*  elaboration_graph_filename = NULL;
FILE*  elaboration_graph_file = NULL;

// Max nb. of applied FD per iteration (0 = no limit)
unsigned core_param_max_degrees = 0;
// Max ratio of remaining resources allowed to build a group. Range 0..1
static float param_max_res_ratio = 0.15;
// Maximum elaboration time, in seconds. Checked at the beginning of each iteration.
static unsigned param_timeout = 0;
// Use a fork to get the exact weight of a FD
bool core_param_weight_fork = false;
// 0 means no limit
static unsigned param_max_iter = 0;

static bool elabo_pause_end_iter = false;

// For debug
static bool debug_show_avail_fd = false;
static bool debug_show_selected_fd = false;
static bool debug_show_applied_fd = false;

// Shared value!
// WARNING: Not yet stable. Set to true only for debug.
bool core_dupimplem_inxs = false;

// To keep annotations but don't consider them to weight freedom degrees
static bool weights_without_annotations = false;

// Export VHDL of each solution in a dir named like "prefix-005/vhdl/"
static struct {
	bool     en;
	unsigned digits;
	unsigned max_iter_idx;
	char*    prefix;
} export_vhdl = { false, 3, 999, "explored-sol-" };

#define CORE_LISTFD_PARENT  0x01



// Create a pool of type : trans_dep_t

#define POOL_prefix      pool_degree
#define POOL_data_t      freedom_degree_t
#define POOL_ALLOC_SIZE  1000

#define POOL_INSTANCE             POOL_DEGREE
#define POOL_INSTANCE_ATTRIBUTES  static

#include "../pool.h"

// Allocation wrappers

freedom_degree_t* freedom_degree_new() {
	freedom_degree_t* degree = pool_degree_pop(&POOL_DEGREE);
	memset(degree, 0, sizeof(*degree));
	return degree;
}
void freedom_degree_free(freedom_degree_t* degree) {
	switch(degree->type) {
		case FD_ADD_OPS :
			hwres_heap_free(degree->target.addops.heap);
			break;
		case FD_MULTI :
			foreach(degree->target.list_degrees, scan) freedom_degree_free(scan->DATA);
			break;
		default :
			break;
	}
	freeptype(degree->resource_cost);
	pool_degree_push(&POOL_DEGREE, degree);
}

void freedom_degree_freelist(chain_list* list) {
	foreach(list, scan) freedom_degree_free(scan->DATA);
	freechain(list);
}



void Core_ImplemFree(implem_t* Implem) {
	freenum(Implem->CORE.freedom_degrees_skipped_ops);
	Implem->CORE.freedom_degrees_skipped_ops = NULL;

	freedom_degree_freelist(Implem->CORE.freedom_degrees);
	Implem->CORE.freedom_degrees = NULL;

	Techno_NameOpRes_Free(Implem->CORE.resources_opers);
	Implem->CORE.resources_opers = NULL;

	freeptype(Implem->CORE.resources_total);
	Implem->CORE.resources_total = NULL;

	foreach(Implem->CORE.skipfd_addops, scan) hwres_heap_free(scan->DATA);
	freechain(Implem->CORE.skipfd_addops);
	Implem->CORE.skipfd_addops = NULL;

	freechain(Implem->CORE.skipfd_scalar);
	Implem->CORE.skipfd_scalar = NULL;

	avl_pp_reset(&Implem->CORE.refcost_oneop);
	avl_pp_reset(&Implem->CORE.refcost_memport_r_mux);
	avl_pi_reset(&Implem->CORE.dupimplem_inxs);
}
void Core_ImplemInit(implem_t* Implem) {
	// Init the trees of reference hardware costs
	avl_pp_init(&Implem->CORE.refcost_oneop);
	avl_pp_init(&Implem->CORE.refcost_memport_r_mux);
	avl_pi_init(&Implem->CORE.dupimplem_inxs);
	Implem->CORE.eval_done = false;
	// Note: Other fields are initialized to zero/NULL by calloc()
}



static bool dupimplem_disp_time = true;
static char* dupimplem_redirect = "/dev/null";

void Core_DupImplem_EnsureVoid(implem_t* Implem) {
	if(Implem->CORE.dup_implem!=NULL) {
		augh_implem_free(Implem->CORE.dup_implem);
		Implem->CORE.dup_implem = NULL;
	}
}
// Schedule, bind, post-process, evaluate with techno etc.
implem_t* Core_DupImplem_Synthesize(implem_t* Implem, chain_list* list_fd) {

	// Only to know the execution time
	struct timespec oldtime;
	if(dupimplem_disp_time==true) {
		TimeSpec_GetReal(&oldtime);
	}

	// Redirect the output
	int new_fd_stdout = -1;
	int save_fd_stdout = -1;

	// To make this process a bit silent
	if(dupimplem_redirect!=NULL) {
		new_fd_stdout = open(dupimplem_redirect, O_WRONLY | O_CREAT | O_TRUNC, DEFFILEMODE);
		if(new_fd_stdout==-1) {
			printf(
				"\n"
				"  **** ERROR Couldn't open '%s' to redirect stdout for dupImplem ****\n"
				"  **** Reason: %s ****\n"
				"  **** WARNING stdout redirection is now disabled ****\n"
				"\n",
				dupimplem_redirect,
				strerror(errno)
			);
			dupimplem_redirect = NULL;
		}
		else {
			fflush(NULL);
			save_fd_stdout = dup(STDOUT_FILENO);
			close(STDOUT_FILENO);
			dup(new_fd_stdout);
			close(new_fd_stdout);
		}
	}

	printf("INFO: Duplicating an Implem structure...\n");

	implem_t* newImplem = Implem_Dup(Implem);
	assert(newImplem!=NULL);

	// Process

	if(list_fd!=NULL) {
		int z = Core_FreedomDegree_ApplyList(newImplem, list_fd, NULL, CORE_LISTFD_PARENT);
		if(z!=0) exit(EXIT_FAILURE);
		Hier_Update_SimpStruct(newImplem);
		// FIXME this is rather dirty
		newImplem->synth_state.schedule = 0;
		newImplem->synth_state.postproc = 0;
	}

	if(newImplem->synth_state.schedule==0) {
		Hier_Update_All(newImplem);
		Schedule(newImplem, SCHED_ALGO_LIST);
		Hier_Update_All(newImplem);
	}

	if(newImplem->synth_state.postproc==0) {
		augh_postprocess(newImplem);
	}

	printf("INFO: Updating the exec time of the duplicated Implem...\n");

	Hier_Timing_Update(newImplem, false);
	Hier_Timing_Show(newImplem);

	printf("INFO: Getting the size of the duplicated Implem...\n");

	if(newImplem->CORE.resources_opers!=NULL) Techno_NameOpRes_Free(newImplem->CORE.resources_opers);
	newImplem->CORE.resources_opers = Techno_Netlist_EvalSize_WithTypes(Implem->synth_target, newImplem->netlist.top);
	if(newImplem->CORE.resources_total!=NULL) freeptype(newImplem->CORE.resources_total);
	newImplem->CORE.resources_total = Techno_NameOpRes_GetTotal(newImplem->CORE.resources_opers);

	Techno_NameOpRes_Print(newImplem->CORE.resources_opers);

	// Get the old stdout back
	if(new_fd_stdout != -1) {
		fflush(NULL);
		close(STDOUT_FILENO);
		dup(save_fd_stdout);
		close(save_fd_stdout);
	}

	// Display the execution time
	if(dupimplem_disp_time==true) {
		double diff = TimeSpec_DiffCurrReal_Double(&oldtime);
		printf("DupImplem processing done in %g seconds.\n", diff);
	}

	return newImplem;
}
void Core_DupImplem_SetResults(implem_t* Implem, implem_t* dupImplem) {
	// Resources
	if(Implem->CORE.resources_opers!=NULL) Techno_NameOpRes_Free(Implem->CORE.resources_opers);
	Implem->CORE.resources_opers = NULL;  // FIXME kept empty. would it be useful ?
	if(Implem->CORE.resources_total!=NULL) freeptype(Implem->CORE.resources_total);
	Implem->CORE.resources_total = dupptype(dupImplem->CORE.resources_total);
	// Timing
	Implem->CORE.execution_time = dupImplem->time.execution_cycles;
}



// Compute estimator drifts. Mostly for debug.

static bool  estim_drifts_en = false;
static FILE* estim_drifts_file = NULL;

// FIXME This function should NOT, at any cost, refer to resource names directly
static void Core_EstimDrift_DumpFD(implem_t* Implem, freedom_degree_t* degree, forksynth_carac_t* forksynth) {
	if(estim_drifts_file==NULL) {
		estim_drifts_file = fopen("estimation-drifts", "wb");
		if(estim_drifts_file==NULL) return;
	}

	FILE* F = estim_drifts_file;

	ptype_list* elt;

	int    implem_lut;
	int    implem_ff;
	double implem_cycles;

	elt = ChainPType_SearchData(Implem->CORE.resources_total, namealloc("lut6"));
	implem_lut = elt!=NULL ? elt->TYPE : 0;
	elt = ChainPType_SearchData(Implem->CORE.resources_total, namealloc("dff"));
	implem_ff = elt!=NULL ? elt->TYPE : 0;
	implem_cycles = Implem->CORE.execution_time.avg;

	int    estim_lut;
	int    estim_ff;
	double estim_cycles;

	elt = ChainPType_SearchData(degree->resource_cost, namealloc("lut6"));
	estim_lut = elt!=NULL ? elt->TYPE : 0;
	elt = ChainPType_SearchData(degree->resource_cost, namealloc("dff"));
	estim_ff = elt!=NULL ? elt->TYPE : 0;
	estim_cycles = degree->time_gain;

	int    precise_lut;
	int    precise_ff;
	double precise_cycles;

	elt = ChainPType_SearchData(forksynth->res_total, namealloc("lut6"));
	precise_lut = elt!=NULL ? elt->TYPE : 0;
	elt = ChainPType_SearchData(forksynth->res_total, namealloc("dff"));
	precise_ff = elt!=NULL ? elt->TYPE : 0;
	precise_cycles = forksynth->time.avg;

	// Print the FD type
	switch(degree->type) {
		case FD_ADD_OPS : {
			fprintf(F, "addops"); break;
		}
		case FD_MEM_INLINE : {
			netlist_comp_t* comp = Map_Netlist_GetComponent(Implem, degree->target.array.name);
			netlist_memory_t* mem_data = comp->data;
			if(mem_data->ports_wa_nb>0)
			     fprintf(F, "ram-inline");
			else fprintf(F, "rom-inline");
			break;
		}
		case FD_LOOP_UNROLL : {
			fprintf(F, "unroll"); break;
		}
		case FD_COND_WIRE : {
			fprintf(F, "wire"); break;
		}
		default : return;
	}

	fprintf(F, " lut %d %d ff %d %d cycles %g %g\n",
		estim_lut, precise_lut - implem_lut,
		estim_ff, precise_ff - implem_ff,
		estim_cycles, implem_cycles - precise_cycles
	);

	// Sometimes AUGH does some forking : this ensures there won't be multiple fflushs
	fflush(F);
}

static void Core_EstimDrift_ChosenListFD(implem_t* Implem, chain_list* list) {
	foreach(list, scanFD) {
		freedom_degree_t* degree = scanFD->DATA;

		// Get accurate resource usage
		forksynth_carac_t carac_tmp;
		int z = ForkSynth_GetCarac_FreedomDegree(Implem, &carac_tmp, degree);
		if(z!=0) {
			ForkSynth_FreeCarac(&carac_tmp);
			continue;
		}

		// For debug : analyze precision of estimators
		Core_EstimDrift_DumpFD(Implem, degree, &carac_tmp);

		// Clean
		ForkSynth_FreeCarac(&carac_tmp);
	}
}



/* Statistics about the architecture
Used to estimate the HW cost of some transformations.

Adding operators, needed data :
- Operator widthes, and resource cost.
- Multiplexer size of the inputs of the operators.
- Estimation of the number of destinations of the outputs of the operators.
  (could it be the number of States where these operators are used ?)

Wiring conditions, needed data :
- The test expression : Is a new operator needed ?
  If yes, find the hardware cost.
- Multiplexer size (new multiplexer, or only a new input ?)
- Execution of several branches simultaneously... hardware cost ?

Loop unrolling :
- Number of new FSM states
- Size of the new FSM state (logic to activate the FSM registers)
- Size of the logic for FSM outputs (more FSM states will drive the outputs...)
  Implies knowing the number of outputs,
  Implies knowing where are the multiplexers and their width...
  => the number of impacted MUXes is the number of symbols written in the loop body,
     plus the number of shared operators used in the loop body,
       including the number of inputs of these operators,
     plus the number of destinations of the outputs of these operators
       (could it be the number of States where these operators are used ?)

*/

// FIXME to delete. Adapt the Techno function to take into account the usage stats.
static ptype_list* Core_GetHardwareCost_OpsWithUsageStats(implem_t* Implem, char* op_type, op_usage_t* usage_stats) {
	ptype_list* list = NULL;

	// The operator
	ptype_list* res_operator = NULL;
	res_operator = Techno_Netlist_Get_NameRes_FromOutWidth(Implem, op_type, usage_stats->out_max_width);

	list = Techno_NameRes_AddMult(list, res_operator, 1);
	freeptype(res_operator);

	#if 0  // The input multiplexers
	ptype_list* res_in_mux = Techno_OperResources_Get_Mux_Direct(usage_stats->max_operand_width, usage_stats->max_operand_nb);
	Techno_OperResources_Mult(res_in_mux, 2);
	list = Techno_OperResources_AddMult(list, res_in_mux, 1);
	freeptype(res_in_mux);
	#endif

	#if 0  // The destination multiplexers
	ptype_list* res_dest_mux = Techno_OperResources_Get_Mux_Direct(usage_stats->max_output_width, 2);
	Techno_OperResources_Mult(res_dest_mux, usage_stats->trans_nb);
	list = Techno_OperResources_AddMult(list, res_dest_mux, 1);
	freeptype(res_dest_mux);
	#endif

	// FIXME Handle the FSM output logic for the new multiplexers

	return list;
}

// Utility functions
// Add the size of the source FSM output logic if applicable
static bitype_list* add_fsmout_size_check(implem_t* Implem, bitype_list* list, netlist_port_t* port) {
	// Get the FSM port
	if(port->source==NULL) return list;
	if(port->source->NEXT!=NULL) return list;
	netlist_val_cat_t* valcat = port->source->DATA;
	if(valcat->source==NULL) return list;
	netlist_comp_t* compFsm = valcat->source->component;
	if(compFsm->model!=NETLIST_COMP_FSM) return list;
	netlist_fsm_outval_t* outval = Netlist_Comp_Fsm_GetOutVal(valcat->source);
	assert(outval!=NULL);
	// Get the size
	ptype_list* local_size = Techno_EvalRes_FsmOut_OneBit_OneHot(Implem, ChainList_Count(outval->actions));
	list = Techno_NameOpRes_Add_One(list, NETLIST_COMP_FSM->name, 1, local_size);
	freeptype(local_size);
	return list;
}
// Add the size of the source MUX if it exists
// FIXME here we should get the size as a ptype_list*
static bitype_list* add_mux_size_check(implem_t* Implem, bitype_list* list, netlist_port_t* port) {
	// Get the MUX, if any
	if(port->source==NULL) return list;
	if(port->source->NEXT!=NULL) return list;
	netlist_val_cat_t* valcat = port->source->DATA;
	if(valcat->source==NULL) return list;
	netlist_comp_t* compMux = valcat->source->component;
	if(compMux->model!=NETLIST_COMP_MUX) return list;
	// Get the MUX size
	ptype_list* local_size = Techno_EvalSize_Comp(Implem->synth_target, compMux);
	list = Techno_NameOpRes_Add_One(list, compMux->model->name, 1, local_size);
	freeptype(local_size);
	// Get the size of the FSM outputs to drive the MUX
	avl_pp_foreach(&compMux->accesses, scanMuxIn) {
		netlist_access_t* access = scanMuxIn->data;
		netlist_mux_access_t* muxIn_data = access->data;
		list = add_fsmout_size_check(Implem, list, muxIn_data->port_en);
	}
	return list;
}

// Get the hardware cost of adding one operator
ptype_list* FD_HwCost_Op_RefGetMake(implem_t* Implem, char* type_name) {

	// Get an already-computed cost, if any
	avl_pp_node_t* avl_node = NULL;
	bool b = avl_pp_add(&Implem->CORE.refcost_oneop, type_name, &avl_node);
	if(b==false) return avl_node->data;
	avl_node->data = NULL;

	// Compute the size

	if(Implem->CORE.dup_implem==NULL) {
		// No dupImplem is available... count on usage stats

		// Get usage stats
		op_usage_t* usage_stats = calloc(1, sizeof(*usage_stats));
		OpUsageStats_Init(usage_stats);
		OpUsageStats_Get_Implem(Implem, type_name, usage_stats);

		// Get HW cost
		printf("Usage stats for op type '%s' :\n", type_name);
		OpUsageStats_Print(usage_stats);
		ptype_list* res_names = Core_GetHardwareCost_OpsWithUsageStats(Implem, type_name, usage_stats);
		printf("Needed resources :");
		foreach(res_names, scanRes) {
			if(scanRes!=res_names) printf(", ");
			printf("%s:%ld", (char*)scanRes->DATA, scanRes->TYPE);
		}
		printf("\n");

		// Clean
		OpUsageStats_Reset(usage_stats);
		free(usage_stats);

		avl_node->data = res_names;

	}

	else {
		// There is a dupImplem available, read it!
		implem_t* dupImplem = Implem->CORE.dup_implem;

		// Find the max size on all operators
		ptype_list* size_biggest_op = NULL;

		// Scan all operators of this type in the dupImplem
		avl_pp_foreach(&dupImplem->netlist.top->children, scanChild) {
			netlist_comp_t* comp = scanChild->data;
			if(comp->model->name!=type_name) continue;

			// Get the component size
			// FIXME here we had better get the size as a ptype_list*
			bitype_list* size_types = Techno_Netlist_EvalSize_WithTypes(Implem->synth_target, comp);

			// Get the Mux sizes
			// FIXME here we ahd better get the size as a ptype_list*
			avl_pp_foreach(&comp->ports, scanPort) {
				netlist_port_t* port = scanPort->data;
				if(port->direction!=NETLIST_PORT_DIR_IN) continue;
				size_types = add_mux_size_check(Implem, size_types, port);
			}

			// Compute the global size for this operator
			ptype_list* size_total = Techno_NameOpRes_GetTotal(size_types);
			Techno_NameOpRes_Free(size_types);
			// Update max size
			size_biggest_op = Techno_NameRes_SetMax(size_biggest_op, size_total);
			freeptype(size_total);
		}  // Scan all components in the dupImplem

		// Save the final size
		avl_node->data = size_biggest_op;

		// For debug
		printf("INFO : Cost of adding an operator of type '%s' = ", type_name);
		Implem->synth_target->techno->reswriteusage(stdout, avl_node->data, NULL, ", ", "\n");

	}

	return avl_node->data;
}

// Get the hardware cost of adding one Read port to a mem, MUX only
ptype_list* FD_HwCost_MemPortR_MuxOnly_RefGetMake(implem_t* Implem, char* mem_name) {

	// Get an already-computed cost, if any
	avl_pp_node_t* avl_node = NULL;
	bool b = avl_pp_add(&Implem->CORE.refcost_memport_r_mux, mem_name, &avl_node);
	if(b==false) return avl_node->data;
	avl_node->data = NULL;

	// Compute the size

	if(Implem->CORE.dup_implem==NULL) {
		// No dupImplem is avialable... do what you can with usage stats

		// FIXMEEEE missing mem estimations

	}

	else {
		// There is a dupImplem available, read it!
		implem_t* dupImplem = Implem->CORE.dup_implem;

		netlist_comp_t* comp = Map_Netlist_GetComponent(dupImplem, mem_name);
		// The component might have been deleted because unused?
		if(comp==NULL) return NULL;

		// Find the max size on all operators
		ptype_list* size_biggest_access = NULL;

		// Scan all ADDR_R accesses
		avl_pp_foreach(&comp->accesses, scanAccess) {
			netlist_access_t* access = scanAccess->data;
			if(access->model!=NETLIST_ACCESS_MEM_RA) continue;
			netlist_access_mem_addr_t* memR = access->data;

			// Get the component size
			bitype_list* size_types = NULL;
			size_types = add_mux_size_check(Implem, size_types, memR->port_a);

			// Update max size
			ptype_list* size_total = Techno_NameOpRes_GetTotal(size_types);
			size_biggest_access = Techno_NameRes_SetMax(size_biggest_access, size_total);

			// Clean
			Techno_NameOpRes_Free(size_types);
			freeptype(size_total);
		}

		// Save the final size
		avl_node->data = size_biggest_access;

		// For debug
		printf("INFO : Cost (mux only) of adding one R port to comp '%s' type %s = ", comp->name, comp->model->name);
		Implem->synth_target->techno->reswriteusage(stdout, avl_node->data, NULL, ", ", "\n");

	}

	return avl_node->data;
}

// Get the hardware cost of adding one Read port to a mem, MUX only
// The returned list can be freed
ptype_list* FD_HwCost_MemPortsR(implem_t* Implem, char* mem_name, unsigned ports_nb) {

	// Contribution of the mem component itself
	ptype_list* cost = Techno_Netlist_Eval_AddReadPorts(Implem, mem_name, ports_nb);

	// The mux
	ptype_list* cost_mux = FD_HwCost_MemPortR_MuxOnly_RefGetMake(Implem, mem_name);
	cost = Techno_NameRes_AddMult(cost, cost_mux, ports_nb);

	return cost;
}

// Get the hardware cost of adding all in a resource heap (ops & mem)
ptype_list* FD_HwCost_HeapRes(implem_t* Implem, hwres_heap_t* heap) {
	ptype_list* cost = NULL;

	// Scan the operators to add
	avl_pp_foreach(&heap->type_ops, avl_node) {
		hw_res_t* elt = avl_node->data;
		ptype_list* cost_one = FD_HwCost_Op_RefGetMake(Implem, elt->name);
		cost = Techno_NameRes_AddMult(cost, cost_one, elt->number);
	}

	// Scan the memory ports to add
	avl_pp_foreach(&heap->type_mem, avl_node) {
		hw_res_t* elt = avl_node->data;
		if(elt->ports_r<1) continue;
		ptype_list* cost_mem = FD_HwCost_MemPortsR(Implem, elt->name, elt->ports_r);
		cost = Techno_NameRes_AddMult(cost, cost_mem, 1);
		freeptype(cost_mem);
	}

	return cost;
}

// Get the hardware cost of adding the difference between a heap and the current Implem
// The heap of resources is modified and freed inside this function
ptype_list* FD_HwCost_HeapRes_Missing_nodup(implem_t* Implem, hwres_heap_t* heap) {
	hwres_heap_sub_fromavail_forFD(Implem->SCHED.hwres_available, heap);
	hwres_heap_remove_empty(heap);
	if(hwres_heap_isempty(heap)==true) { hwres_heap_free(heap); return NULL; }

	//printf("DEBUG %s:%u : difference in hardware resource heaps\n", __FILE__, __LINE__);
	//indent_t indent = Indent_Make(' ', 2, 0);
	//hwres_heap_print(tmp_heap, &indent);

	// Get the resource cost of the difference
	ptype_list* cost = FD_HwCost_HeapRes(Implem, heap);
	hwres_heap_free(heap);

	return cost;
}
ptype_list* FD_HwCost_HeapRes_Missing(implem_t* Implem, hwres_heap_t* heap) {
	heap = hwres_heap_dup(heap);
	return FD_HwCost_HeapRes_Missing_nodup(Implem, heap);
}

// Get the hardware cost of adding the difference between a heap and the current Implem
ptype_list* FD_HwCost_Level_Missing(implem_t* Implem, hier_node_t* node) {
	hwres_heap_t* heap = NULL;

	if(sched_use_vexalloc_share==true) heap = hwres_heap_getneed_level_vexalloc(node);
	else heap = hwres_heap_getneed_level(node);

	return FD_HwCost_HeapRes_Missing_nodup(Implem, heap);
}



// Compute the final score from the time gain and resource cost.
// The entire hardware cost must be known.
int FreedomDegrees_SetFinalScore(implem_t* Implem) {
	// Paranoia
	if(Implem->env.globals->top_implem->CORE.resources_total==NULL) return -1;
	//if(Implem->CORE.execution_time<=0) return -1;
	// Computate the resource score
	foreach(Implem->CORE.freedom_degrees, ScanDegreeRef) {
		freedom_degree_t* ref_degree = ScanDegreeRef->DATA;
		double score = 0;
		if(Implem->synth_target->resources!=NULL) {
			foreach(ref_degree->resource_cost, scan) {
				ptype_list* eltLimit = ChainPType_SearchData(Implem->synth_target->resources, scan->DATA);
				assert(eltLimit!=NULL);
				assert(eltLimit->TYPE>0);
				double d = eltLimit->TYPE;
				// Note: For component models, use the global resource usage: the top-level component
				ptype_list* eltUsed = ChainPType_SearchData(Implem->env.globals->top_implem->CORE.resources_total, scan->DATA);
				if(eltUsed!=NULL) d -= eltUsed->TYPE;
				if(d>0) d = scan->TYPE / d; else d = 10000000;
				if(d > score) score = d;
			}
		}
		else {
			foreach(ref_degree->resource_cost, scan) if(scan->TYPE > score) score = scan->TYPE;
		}
		// Set the resource score
		ref_degree->resource_score = score;
		// Apply the factor corresponding to the number of instances of this model
		if(Implem->compmod.instance_nb>0) {
			ref_degree->final_score *= Implem->compmod.instance_nb;
			ref_degree->time_gain *= Implem->compmod.instance_nb * Implem->compmod.time_weight;
		}
		// Set final score
		if(score>0) ref_degree->final_score = ref_degree->time_gain / score;
		else ref_degree->final_score = 10000000;
		// If the component model is not used, set score = 0
		if(Implem->compmod.instance_nb==0) ref_degree->final_score = 0;
	}
	return 0;
}

// Only list the freedom degrees.
// The weights are set, no score evaluation.
static void FreedomDegrees_Build_internal(implem_t* Implem) {
	// Force disable some freedom degrees
	//Implem->CORE.freedom_degrees_skipped_ops = ChainNum_Add_NoDup(Implem->CORE.freedom_degrees_skipped_ops, FD_SKIP_MEM_R);
	Implem->CORE.freedom_degrees_skipped_ops = ChainNum_Add_NoDup(Implem->CORE.freedom_degrees_skipped_ops, FD_SKIP_MEM_W);
	// Disabled because it does not check if the symbols used in the test are not written inside
	Implem->CORE.freedom_degrees_skipped_ops = ChainNum_Add_NoDup(Implem->CORE.freedom_degrees_skipped_ops, FD_SKIP_WIRE_SEQ_AUTO);
	// Disabled because it does not check if the symbols used in the test are not written inside
	Implem->CORE.freedom_degrees_skipped_ops = ChainNum_Add_NoDup(Implem->CORE.freedom_degrees_skipped_ops, FD_SKIP_UNROLL_PART);

	// Clear the previous heaps of reference hardware costs
	avl_pp_foreach(&Implem->CORE.refcost_oneop, scan) freeptype(scan->data);
	avl_pp_reset(&Implem->CORE.refcost_oneop);
	avl_pp_foreach(&Implem->CORE.refcost_memport_r_mux, scan) freeptype(scan->data);
	avl_pp_reset(&Implem->CORE.refcost_memport_r_mux);

	avl_pi_reset(&Implem->CORE.dupimplem_inxs);

	// Get the number of components already in excess (curImplem - dupImplem)
	if(core_dupimplem_inxs==true) {
		avl_pp_foreach(&Implem->netlist.top->children, scan) {
			netlist_comp_t* comp = scan->data;
			avl_pi_node_t* pi_node = NULL;
			bool b = avl_pi_add(&Implem->CORE.dupimplem_inxs, comp->model->name, &pi_node);
			if(b==true) pi_node->data = 1;
			else pi_node->data ++;
		}
		avl_pp_foreach(&Implem->CORE.dup_implem->netlist.top->children, scan) {
			netlist_comp_t* comp = scan->data;
			avl_pi_node_t* pi_node = NULL;
			bool b = avl_pi_find(&Implem->CORE.dupimplem_inxs, comp->model->name, &pi_node);
			if(b==false) continue;
			if(pi_node->data>0) pi_node->data --;
		}
		#ifndef NDEBUG
		avl_pi_foreach(&Implem->CORE.dupimplem_inxs, scan) {
			char* name = scan->key;
			unsigned nb = scan->data;
			if(nb>0) dbgprintf("Excess of components: %u %s\n", nb, name);
		}
		#endif
	}

	// Build the list of freedom degrees
	chain_list* list = NULL;
	chain_list* tmp_list = NULL;
	bool b;

	// Ensure the heap of currently available resources are up-to-date
	Resources_Available_Detect(Implem);

	// Freedom degrees about scalar replacement of arrays
	b = ChainNum_IsDataHere(Implem->CORE.freedom_degrees_skipped_ops, FD_SKIP_INLINE);
	if(b==false) {
		tmp_list = Sched_FreedomDegrees_Build_ScalarReplace(Implem);
		list = append(tmp_list, list);
	}

	// Freedom degrees about adding operators
	b = ChainNum_IsDataHere(Implem->CORE.freedom_degrees_skipped_ops, FD_SKIP_ADDOPS);
	if(b==false) {
		tmp_list = Sched_FreedomDegrees_Build_Ops(Implem);
		list = append(tmp_list, list);
	}

	// Freedom degrees: loop unrolling
	b = ChainNum_IsDataHere(Implem->CORE.freedom_degrees_skipped_ops, FD_SKIP_UNROLL);
	if(b==false) {
		tmp_list = Hier_For_FreedomDegrees_Get(Implem);
		list = append(tmp_list, list);
	}

	// Freedom degrees: condition wiring
	b = ChainNum_IsDataHere(Implem->CORE.freedom_degrees_skipped_ops, FD_SKIP_WIRE);
	if(b==false) {
		tmp_list = Hier_Switch_FreedomDegrees_Get(Implem);
		list = append(tmp_list, list);
	}

	// Freedom degrees: Transform ram-based PP to register-based
	b = ChainNum_IsDataHere(Implem->CORE.freedom_degrees_skipped_ops, FD_SKIP_PP_DIRECT);
	if(b==false) {
		tmp_list = Sched_FreedomDegrees_Build_PPDirect(Implem);
		list = append(tmp_list, list);
	}

	// Replace the previous list of freedom degrees
	freedom_degree_freelist(Implem->CORE.freedom_degrees);
	Implem->CORE.freedom_degrees = list;
}

void FreedomDegrees_Build(implem_t* Implem) {

	// To save all timing data
	avl_pp_tree_t save_timing;
	if(weights_without_annotations==true) {
		Hier_SaveTime_InitBuild(Implem->H, &save_timing);
		Hier_Timing_ResetAnnotationsUpdate(Implem);
	}

	// Build the freedom degrees
	FreedomDegrees_Build_internal(Implem);

	// Set the timings again
	if(weights_without_annotations==true) {
		Hier_SaveTime_CommitClearUpdate(Implem, &save_timing);
	}

}

// Full FD list construction and score
void FreedomDegrees_BuildAndScore(implem_t* Implem) {

	// To save all timing data
	avl_pp_tree_t save_timing;
	if(weights_without_annotations==true) {
		Hier_SaveTime_InitBuild(Implem->H, &save_timing);
		Hier_Timing_ResetAnnotationsUpdate(Implem);
		Implem->CORE.execution_time = Implem->time.execution_cycles;
	}

	FreedomDegrees_Build_internal(Implem);

	// Optionally, overwrite all FD weights by a complete circuit evaluation
	if(core_param_weight_fork==true) {
		printf("INFO: Launching precise evaluation of %u FDs... can take a long time...\n", ChainList_Count(Implem->CORE.freedom_degrees));
		foreach(Implem->CORE.freedom_degrees, scanFD) {
			freedom_degree_t* degree = scanFD->DATA;

			// Get accurate resource usage
			forksynth_carac_t carac_tmp;
			int z = ForkSynth_GetCarac_FreedomDegree(Implem, &carac_tmp, degree);
			if(z!=0) {
				ForkSynth_FreeCarac(&carac_tmp);
				continue;
			}

			// For debug : analyze precision of estimators
			//Core_EstimDrift_DumpFD(Implem, degree, &carac_tmp);

			// Apply to the freedom degree
			freeptype(degree->resource_cost);
			degree->resource_cost = dupptype(carac_tmp.res_total);
			Techno_NameRes_Sub_raw(degree->resource_cost, Implem->CORE.resources_total, true, true);
			degree->time_gain = Implem->CORE.execution_time.avg - carac_tmp.time.avg;

			ForkSynth_FreeCarac(&carac_tmp);
		}  // Loop on all FD
	}

	// Set the timings again
	if(weights_without_annotations==true) {
		Hier_SaveTime_CommitClearUpdate(Implem, &save_timing);
		Implem->CORE.execution_time = Implem->time.execution_cycles;
	}

	FreedomDegrees_SetFinalScore(Implem);
}

static void FreedomDegrees_Show_one(freedom_degree_t* degree, indent_t* indent) {
	implem_t* Implem = degree->Implem;

	indent_t indent_plus1;
	if(indent!=NULL) indent_plus1 = Indent_Make_IncLevel(indent);
	else indent_plus1 = Indent_Make(' ', 2, 1);
	indent_t indent_plus2 = Indent_Make_IncLevel(&indent_plus1);

	indentprintf(indent, "Model '%s': Freedom degree : type ", Implem->compmod.model_name);
	switch(degree->type) {
		case FD_NONE           : printf("UNKNOWN"); break;
		case FD_ADD_OPS        : printf("OPS_ADD"); break;
		case FD_MEM_INLINE     : printf("MEM_INLINE"); break;
		case FD_COND_WIRE      : printf("COND_WIRE(%s)", Hier_WireStyle_ID2Str(degree->target.hier.d.wire_style)); break;
		case FD_LOOP_UNROLL    : printf("LOOP_UNROLL(%u)", degree->target.hier.d.unroll_factor); break;
		case FD_PP_DIRECT      : printf("PP_DIRECT"); break;
		case FD_MULTI          : printf("MULTI(%u)", ChainList_Count(degree->target.list_degrees)); break;
	}
	printf(", time gain %g", degree->time_gain);
	printf(", resource score %g", degree->resource_score);
	printf(", final score %g\n", degree->final_score);

	indentprint(&indent_plus1);
	Techno_NameRes_Print(degree->resource_cost, "Resource cost : ", "\n");

	switch(degree->type) {
		case FD_NONE : {
			printf("ERROR unknown FD type!\n");
			break;
		}
		case FD_ADD_OPS : {
			indentprintf(&indent_plus1, "Operators :\n");
			hwres_heap_print(degree->target.addops.heap, &indent_plus2);
			break;
		}
		case FD_MEM_INLINE : {
			indentprintf(&indent_plus1, "Array name : '%s'\n", degree->target.array.name);
			break;
		}
		case FD_COND_WIRE : {
			if(degree->target.hier.node!=NULL) {
				indentprint(&indent_plus1);
				LineList_Print_be(degree->target.hier.node->source_lines, "Node lines : ", "\n");
			}
			break;
		}
		case FD_LOOP_UNROLL : {
			if(degree->target.hier.node!=NULL) {
				indentprint(&indent_plus1);
				LineList_Print_be(degree->target.hier.node->source_lines, "Node lines : ", "\n");
				Hier_For_PrintDetails(Implem, degree->target.hier.node, &indent_plus1);
			}
			break;
		}
		case FD_PP_DIRECT : {
			indentprintf(&indent_plus1, "Array name : '%s'\n", degree->target.array.name);
			break;
		}
		case FD_MULTI :
			foreach(degree->target.list_degrees, scan) {
				FreedomDegrees_Show_one(scan->DATA, &indent_plus1);
			}
			break;
	}
}

static void FreedomDegrees_Show(implem_t* Implem) {
	indent_t indent = Indent_Make(' ', 2, 0);
	printf("There are %u freedom degrees.\n", ChainList_Count(Implem->CORE.freedom_degrees));
	foreach(Implem->CORE.freedom_degrees, scanDegree) {
		freedom_degree_t* degree = scanDegree->DATA;
		FreedomDegrees_Show_one(degree, &indent);
	}
}



// Write the freedom degrees through a file. For fork() related operations.
// If list is not NULL, this list is printed instead of the list in the Implem.
void FreedomDegrees_fprintf(FILE* F, implem_t* Implem, chain_list* list) {
	if(list==NULL) list = Implem->CORE.freedom_degrees;

	// Print resources for all operators
	fprintf(F, "degrees-nb %u\n", ChainList_Count(list));

	// Duplicate the freedom degrees of the child
	foreach(list, scanFD) {
		freedom_degree_t* degree = scanFD->DATA;

		fprintf(F, "type ");
		switch(degree->type) {
			case FD_NONE           : fprintf(F, "unknown"); break;
			case FD_ADD_OPS        : fprintf(F, "add-ops"); break;
			case FD_MEM_INLINE     : fprintf(F, "mem-inline"); break;
			case FD_PP_DIRECT      : fprintf(F, "pp-direct"); break;
			case FD_COND_WIRE      : fprintf(F, "cond-wire %s", Hier_WireStyle_ID2Str(degree->target.hier.d.wire_style)); break;
			case FD_LOOP_UNROLL    : fprintf(F, "loop-unroll %u", degree->target.hier.d.unroll_factor); break;
			// FIXME this should be handled
			case FD_MULTI          : fprintf(F, "multi-unhandled"); break;
		}
		fprintf(F, " time-gain %g", degree->time_gain);
		fprintf(F, " resource-score %g", degree->resource_score);
		fprintf(F, " final-score %g\n", degree->final_score);
		fprintf(F, "\n");

		fprintf(F, "resource-cost\n");
		Techno_NameRes_fprintf(F, degree->resource_cost);

		fprintf(F, "target ");
		switch(degree->type) {
			case FD_ADD_OPS : {
				unsigned nb = 0;
				nb += avl_pp_count(&degree->target.addops.heap->type_ops);
				nb += avl_pp_count(&degree->target.addops.heap->type_mem);
				fprintf(F, "ops-nb %u", nb);
				avl_pp_foreach(&degree->target.addops.heap->type_ops, node) {
					hw_res_t* elt = node->data;
					fprintf(F, " op %s %u", elt->name, elt->number);
				}
				avl_pp_foreach(&degree->target.addops.heap->type_mem, node) {
					hw_res_t* elt = node->data;
					fprintf(F, " mem %s %u", elt->name, elt->ports_r);
				}
				fprintf(F, "\n");
				break;
			}
			case FD_MEM_INLINE : {
				fprintf(F, "%s", degree->target.array.name);
				break;
			}
			case FD_COND_WIRE :
			case FD_LOOP_UNROLL : {
				// Write the node pointer directly
				fprintf(F, "%p\n", degree->target.hier.node);
				break;
			}
			case FD_PP_DIRECT : {
				// Write the node pointer directly
				fprintf(F, "%p\n", degree->target.array.name);
				break;
			}
			default :
				printf("ERROR not handled het !\n");
				break;
		}  // Write the target description
		fprintf(F, "\n");
	}  // Scan the freedom degrees
}
// Read the resources from a file
// If res_list is NULL, the result is saved here and not in the Implem.
int FreedomDegrees_fscanf(FILE* F, implem_t* Implem, chain_list** res_list) {
	char buffer[200];
	int z;
	chain_list* list = NULL;
	unsigned nb = 0;
	// Get the number of freedom degrees
	z = fscanf(F, "%s", buffer);
	if(z!=1) return __LINE__;
	if(strcmp(buffer, "degrees-nb")!=0) return __LINE__;
	z = fscanf(F, "%u", &nb);
	if(z!=1) return __LINE__;
	// Get the freedom degrees
	for(unsigned i=0; i<nb; i++) {
		// Get a new resource element
		freedom_degree_t* degree = freedom_degree_new();
		degree->Implem = Implem;
		list = addchain(list, degree);
		// Get the type
		z = fscanf(F, "%s", buffer);
		if(z!=1 || strcmp(buffer, "type")!=0) { freedom_degree_freelist(list); return __LINE__; }
		z = fscanf(F, "%s", buffer);
		if(z!=1) { freedom_degree_freelist(list); return __LINE__; }
		if(strcmp(buffer, "add-ops")==0)          degree->type = FD_ADD_OPS;
		else if(strcmp(buffer, "mem-inline")==0)  degree->type = FD_MEM_INLINE;
		else if(strcmp(buffer, "pp-direct")==0)   degree->type = FD_PP_DIRECT;
		else if(strcmp(buffer, "cond-wire")==0) {
			degree->type = FD_COND_WIRE;
			// Get the wiring style
			z = fscanf(F, "%s", buffer);
			if(z!=1) { freedom_degree_freelist(list); return __LINE__; }
			degree->target.hier.d.wire_style = Hier_WireStyle_Str2ID(buffer);
			if(degree->target.hier.d.wire_style==HIER_WIRE_STYLE_NONE) { freedom_degree_freelist(list); return __LINE__; }
		}
		else if(strcmp(buffer, "loop-unroll")==0) {
			degree->type = FD_LOOP_UNROLL;
			// Get the unroll factor
			z = fscanf(F, "%s", buffer);
			if(z!=1) { freedom_degree_freelist(list); return __LINE__; }
			degree->target.hier.d.unroll_factor = atoi(buffer);
		}
		else { freedom_degree_freelist(list); return __LINE__; }

		// Get the time gain
		z = fscanf(F, "%s", buffer);
		if(strcmp(buffer, "time-gain")!=0) { freedom_degree_freelist(list); return __LINE__; }
		z = fscanf(F, "%lf", &degree->time_gain);
		if(z!=1) { freedom_degree_freelist(list); return __LINE__; }
		// Get the resource score
		z = fscanf(F, "%s", buffer);
		if(strcmp(buffer, "resource-score")!=0) { freedom_degree_freelist(list); return __LINE__; }
		z = fscanf(F, "%lf", &degree->resource_score);
		if(z!=1) { freedom_degree_freelist(list); return __LINE__; }
		// Get the final score
		z = fscanf(F, "%s", buffer);
		if(strcmp(buffer, "final-score")!=0) { freedom_degree_freelist(list); return __LINE__; }
		z = fscanf(F, "%lf", &degree->final_score);
		if(z!=1) { freedom_degree_freelist(list); return __LINE__; }

		// Get the resource cost
		z = fscanf(F, "%s", buffer);
		if(strcmp(buffer, "resource-cost")!=0) { freedom_degree_freelist(list); return __LINE__; }
		z = Techno_NameRes_fscanf(F, &degree->resource_cost);
		if(z!=0) { freedom_degree_freelist(list); return __LINE__; }

		// Get the target data
		z = fscanf(F, "%s", buffer);
		if(strcmp(buffer, "target")!=0) { freedom_degree_freelist(list); return __LINE__; }
		switch(degree->type) {
			case FD_ADD_OPS : {
				degree->target.addops.heap = hwres_heap_new();
				// Get the number of operators
				unsigned ops_nb;
				z = fscanf(F, "%s", buffer);
				if(strcmp(buffer, "ops-nb")!=0) { freedom_degree_freelist(list); return __LINE__; }
				z = fscanf(F, "%u", &ops_nb);
				if(z!=1) { freedom_degree_freelist(list); return __LINE__; }
				// Read the operators and their number
				for(unsigned idx_op=0; idx_op<ops_nb; idx_op++) {
					z = fscanf(F, "%s", buffer);
					if(z!=1) { freedom_degree_freelist(list); return __LINE__; }
					if(strcmp(buffer, "op")==0) {
						char* op_type;
						unsigned op_nb;
						z = fscanf(F, "%s", buffer);
						if(z!=1) { freedom_degree_freelist(list); return __LINE__; }
						op_type = namealloc(buffer);
						z = fscanf(F, "%u", &op_nb);
						if(z!=1) { freedom_degree_freelist(list); return __LINE__; }
						hw_res_t* elt = hwres_new();
						elt->type = HWRES_TYPE_OP;
						elt->name = op_type;
						elt->number = op_nb;
						avl_pp_add_keep(&degree->target.addops.heap->type_ops, op_type, elt);
					}
					else if(strcmp(buffer, "mem")==0) {
						char* name;
						unsigned ports_r;
						z = fscanf(F, "%s", buffer);
						if(z!=1) { freedom_degree_freelist(list); return __LINE__; }
						name = namealloc(buffer);
						z = fscanf(F, "%u", &ports_r);
						if(z!=1) { freedom_degree_freelist(list); return __LINE__; }
						hw_res_t* elt = hwres_new();
						elt->type = HWRES_TYPE_MEM;
						elt->name = name;
						elt->ports_r = ports_r;
						avl_pp_add_keep(&degree->target.addops.heap->type_mem, name, elt);
					}
					else { freedom_degree_freelist(list); return __LINE__; }
				}
				break;
			}
			case FD_MEM_INLINE : {
				z = fscanf(F, "%s", buffer);
				degree->target.array.name = namealloc(buffer);
				if(degree->target.array.name==NULL) return __LINE__;
				break;
			}
			case FD_PP_DIRECT : {
				z = fscanf(F, "%s", buffer);
				degree->target.array.name = namealloc(buffer);
				if(degree->target.array.name==NULL) return __LINE__;
				break;
			}
			case FD_COND_WIRE :
			case FD_LOOP_UNROLL : {
				// Directly the node pointer
				z = fscanf(F, "%p", &degree->target.hier.node);
				if(degree->target.hier.node==NULL) { freedom_degree_freelist(list); return __LINE__; }
				break;
			}
			default :
				printf("ERROR not handled het !\n");
				break;
		}  // Get the target of the freedom degree

	}  // Get the freedom degrees

	if(res_list!=NULL) {
		*res_list = list;
	}
	else {
		freedom_degree_freelist(Implem->CORE.freedom_degrees);
		Implem->CORE.freedom_degrees = list;
	}
	return 0;
}


int Core_FreedomDegree_Apply(implem_t* Implem, freedom_degree_t* degree, unsigned params) {

	// Apply the selected freedom degree
	switch(degree->type) {
		case FD_ADD_OPS : {

			// Handle the memory ports
			avl_pp_foreach(&degree->target.addops.heap->type_mem, avl_node) {
				hw_res_t* elt = avl_node->data;
				if(elt->ports_r<1) continue;

				// Get the hardware instance
				netlist_comp_t* comp = Netlist_Comp_GetChild(Implem->netlist.top, elt->name);
				if(comp==NULL) {
					printf("WARNING %s:%d : Netlist instance for array '%s' not found.\n", __FILE__, __LINE__, elt->name);
					return -1;
				}
				if(comp->model!=NETLIST_COMP_MEMORY && comp->model!=NETLIST_COMP_PINGPONG_IN) {
					printf("WARNING %s:%d : Netlist instance for array '%s' is of type '%s'.\n", __FILE__, __LINE__, elt->name, comp->model->name);
					return -1;
				}

				if(comp->model==NETLIST_COMP_MEMORY){
					for(int i=0; i<elt->ports_r; i++) {
						netlist_access_t* memIn = Netlist_Mem_AddAccess_RA(comp);
						if(memIn==NULL) {
							printf("WARNING %s:%d : Adding a port for array '%s' failed.\n", __FILE__, __LINE__, elt->name);
							return -1;
						}
					}
				}
				else{
					for(int i=0; i<elt->ports_r; i++) {
						netlist_access_t* ppIn = Netlist_PingPong_AddAccess_RA(comp);
						if(ppIn==NULL) {
							printf("WARNING %s:%d : Adding a port for pingpong-in '%s' failed.\n", __FILE__, __LINE__, elt->name);
							return -1;
						}
					}
				}
			}

			// Handle the operators
			avl_pp_foreach(&degree->target.addops.heap->type_ops, avl_node) {
				hw_res_t* elt = avl_node->data;
				int z = Map_Netlist_AddSomeOperators(Implem, Netlist_CompModel_Get(elt->name), elt->number);
				if(z!=0) {
					printf("ERROR %s:%d : Adding %d operators of type '%s' failed.\n", __FILE__, __LINE__, elt->number, elt->name);
					return -1;
				}
			}

			break;
		}
		case FD_MEM_INLINE : {
			int z = ReplaceMem_name(Implem, degree->target.array.name);
			if(z!=0) {
				printf("ERROR %s:%d : Inlining the memory '%s' failed (code %d).\n", __FILE__, __LINE__, degree->target.array.name, z);
				return -1;
			}
			break;
		}
		case FD_COND_WIRE : {
			hier_node_t* node = degree->target.hier.node;
			if( (params & CORE_LISTFD_PARENT) != 0 ) {
				node = Implem_Dup_Old2New(Implem, node);
				if(node==NULL) {
					printf("ERROR %s:%d : The node was not found.\n", __FILE__, __LINE__);
					return -1;
				}
			}
			int z = Hier_Switch_ReplaceByWired_SelectStyle(Implem, node, degree->target.hier.d.wire_style);
			if(z!=0) {
				printf("ERROR %s:%d : SWITCH condition wiring failed (code %d).\n", __FILE__, __LINE__, z);
				return -1;
			}

			// Update the needed resources
			Map_Netlist_CompleteComponents(Implem);

			#if 0  // Note: not much needed because a simplification pass should be launched after applying transformations
			// Update the flags HIER_ACT_END
			Hier_Update_Control(Implem);
			#endif

			break;
		}
		case FD_LOOP_UNROLL : {
			hier_node_t* node = degree->target.hier.node;
			if( (params & CORE_LISTFD_PARENT) != 0 ) {
				node = Implem_Dup_Old2New(Implem, node);
				if(node==NULL) {
					printf("ERROR %s:%d : The node was not found.\n", __FILE__, __LINE__);
					return -1;
				}
			}

			if(degree->target.hier.d.unroll_factor==0) {
				int z = Hier_For_ReplaceByFullUnroll(Implem, node, 0);
				if(z!=0) {
					printf("ERROR %s:%d : Full unroll failed.\n", __FILE__, __LINE__);
					return -1;
				}
			}
			else {
				int z = Hier_For_PartialUnroll(Implem, node, degree->target.hier.d.unroll_factor, 0);
				if(z!=0) {
					printf("ERROR %s:%d : Partial unroll failed [code %d].\n", __FILE__, __LINE__, z);
					return -1;
				}
			}

			break;
		}
		case FD_PP_DIRECT : {
			netlist_comp_t* comp = Map_Netlist_GetComponent(Implem, degree->target.array.name);

			if(comp==NULL) {
				printf("ERROR %s:%d : Could't find PP '%s'.\n", __FILE__, __LINE__, degree->target.array.name);
				return -1;
			}
			if(comp->model!=NETLIST_COMP_PINGPONG_IN && comp->model!=NETLIST_COMP_PINGPONG_OUT) {
				printf("ERROR %s:%d : Component '%s' is of model '%s', expected ping-pong.\n", __FILE__, __LINE__, degree->target.array.name, comp->model->name);
				return -1;
			}

			Netlist_PingPong_EnableDirect(comp);

			break;
		}
		default : {
			printf("ERROR %s:%d : Unknown freedom degree.\n", __FILE__, __LINE__);
			return -1;
		}
	}

	Implem->synth_state.schedule = 0;
	Implem->synth_state.postproc = 0;

	return 0;
}

// Important: This function assumes all FD refer to the same Implem
int Core_FreedomDegree_ApplyList(implem_t* Implem, chain_list* list, indent_t* indent, unsigned params) {
	foreach(list, scan) {
		freedom_degree_t* degree = scan->DATA;
		int z = Core_FreedomDegree_Apply(Implem, degree, params);
		if(z!=0) {
			indentprintf(indent, "ERROR %s:%d : Could not apply this freedom degree [code %d] :\n", __FILE__, __LINE__, z);
			FreedomDegrees_Show_one(degree, indent);
			abort();
		}
	}
	return 0;
}
// This function takes into account the case where the FDs are applied in duplicated globals with several Implem models
int Core_FreedomDegree_ApplyList_globals(augh_globals_t* globals, chain_list* list, indent_t* indent, unsigned params) {
	foreach(list, scan) {
		freedom_degree_t* degree = scan->DATA;
		// Find the Implem this FD should be applied to
		implem_t* Implem = degree->Implem;
		if( (params & CORE_LISTFD_PARENT) != 0 ) {
			Implem = Globals_Dup_Old2New(globals, Implem);
			if(Implem==NULL) {
				printf("ERROR %s:%d : The target Implem was not found.\n", __FILE__, __LINE__);
				return -1;
			}
		}
		// Apply the FD
		int z = Core_FreedomDegree_Apply(Implem, degree, params);
		if(z!=0) {
			indentprintf(indent, "ERROR %s:%d : Could not apply this freedom degree [code %d] :\n", __FILE__, __LINE__, z);
			FreedomDegrees_Show_one(degree, indent);
			abort();
		}
	}
	return 0;
}

void Core_FreedomDegree_FlagImpossible(implem_t* Implem, freedom_degree_t* degree) {
	switch(degree->type) {
		case FD_ADD_OPS : {
			hwres_heap_t* heap = hwres_heap_dup(degree->target.addops.heap);
			Implem->CORE.skipfd_addops = addchain(Implem->CORE.skipfd_addops, heap);
			break;
		}
		case FD_MEM_INLINE : {
			Implem->CORE.skipfd_scalar = addchain(Implem->CORE.skipfd_scalar, degree->target.array.name);
			break;
		}
		case FD_COND_WIRE : {
			hier_node_t* node = degree->target.hier.node;
			node->NODE.SWITCH.core.wire_impossible = 1;
			break;
		}
		case FD_LOOP_UNROLL : {
			hier_node_t* node = degree->target.hier.node;
			if(degree->target.hier.d.unroll_factor==0) node->NODE.LOOP.core.full_unroll_impossible = 1;
			else node->NODE.LOOP.core.part_unroll_impossible = 1;
			break;
		}
		case FD_PP_DIRECT : {
			Implem->CORE.skipfd_scalar = addchain(Implem->CORE.skipfd_scalar, degree->target.array.name);
			break;
		}
		default : {
			printf("WARNING %s:%d : Not implemented yet !\n", __FILE__, __LINE__);
			break;
		}
	}
}
void Core_FreedomDegree_ClearAllImpossible(implem_t* Implem) {

	// Addops
	foreach(Implem->CORE.skipfd_addops, scan) {
		hwres_heap_t* heap = scan->DATA;
		hwres_heap_free(heap);
	}
	freechain(Implem->CORE.skipfd_addops);
	Implem->CORE.skipfd_addops = NULL;

	// Scalar replacement, mem inline
	freechain(Implem->CORE.skipfd_scalar);
	Implem->CORE.skipfd_scalar = NULL;

	// Wire
	avl_p_foreach(&Implem->H->NODES[HIERARCHY_SWITCH], scanNode) {
		hier_node_t* node = scanNode->data;
		node->NODE.SWITCH.core.wire_impossible = 0;
	}

	// Unroll
	avl_p_foreach(&Implem->H->NODES[HIERARCHY_LOOP], scanNode) {
		hier_node_t* node = scanNode->data;
		node->NODE.LOOP.core.full_unroll_impossible = 0;
		node->NODE.LOOP.core.part_unroll_impossible = 0;
	}

}


static int Core_Graph_WriteHeader(implem_t* Implem) {
	if(elaboration_graph_file==NULL) return -1;
	fprintf(elaboration_graph_file, "iter ");
	Implem->synth_target->techno->reswritenames(elaboration_graph_file, NULL, " ", NULL);
	fprintf(elaboration_graph_file, " exec_time elabo_RealTime elabo_CpuTime\n");
	// The flush is needed because otherwise, the child process in the ForkSynth module
	// writes its buffer before dying...
	fflush(elaboration_graph_file);
	return 0;
}
static int Core_Graph_AddPoint(implem_t* Implem) {
	if(elaboration_graph_file==NULL) return -1;
	fprintf(elaboration_graph_file, "%d ", Implem->CORE.iteration);
	Implem->synth_target->techno->reswriteusage(elaboration_graph_file, Implem->CORE.resources_total, NULL, " ", NULL);
	fprintf(elaboration_graph_file, " %g", Implem->CORE.execution_time.avg);
	// Print the elaboration time
	fprintf(elaboration_graph_file, " %g", TimeSpec_DiffCurrReal_Double(&Implem->CORE.begtime_real));
	fprintf(elaboration_graph_file, " %g", TimeSpec_DiffCurrCpu_Double(&Implem->CORE.begtime_cpu));
	fprintf(elaboration_graph_file, "\n");
	// The flush is needed because otherwise, the child process in the ForkSynth module
	// writes its buffer before dying...
	fflush(elaboration_graph_file);
	return 0;
}



//======================================================================
// Main elaboration function
//======================================================================

static bool enable_duplication = true;

// Remove the freedom degrees that are incompatible
// Example : loop partial and full unroll, ADDOPS...
static void Core_RemoveIncompatibleFD(chain_list** plist, freedom_degree_t* ref_degree) {
	switch(ref_degree->type) {
		case FD_ADD_OPS : {

			// Simple solution : Remove all other ADDOPS degrees
			chain_list* new_list = NULL;
			foreach(*plist, scan) {
				freedom_degree_t* degree = scan->DATA;
				if(degree->Implem==ref_degree->Implem) {
					if(degree->type==FD_ADD_OPS) continue;
				}
				new_list = addchain(new_list, degree);
			}
			freechain(*plist);
			*plist = new_list;

			// And if there is addition of ports, remove the array replacement.

			// Build the list of memories
			chain_list* list_mem = NULL;
			avl_pp_foreach(&ref_degree->target.addops.heap->type_mem, avl_node) {
				hw_res_t* elt = avl_node->data;
				if(elt->ports_r<1) continue;
				list_mem = addchain(list_mem, elt->name);
			}
			if(list_mem!=NULL) {
				// Remove the nodes
				new_list = NULL;
				foreach(*plist, scan) {
					freedom_degree_t* degree = scan->DATA;
					if(degree->Implem==ref_degree->Implem) {
						if(degree->type==FD_MEM_INLINE) {
							if(ChainList_IsDataHere(list_mem, degree->target.array.name)==true) continue;  // Reject this FD !
						}
					}
					new_list = addchain(new_list, degree);
				}
				freechain(list_mem);
				freechain(*plist);
				*plist = new_list;
			}

			break;
		}
		case FD_MEM_INLINE : {
			// There is incompatibility with duplication of memory and addition of ports.
			// Scan the other ADDOPS degrees

			// Simple solution : Remove all other ADDOPS degrees
			chain_list* new_list = NULL;
			foreach(*plist, scan) {
				freedom_degree_t* degree = scan->DATA;
				if(degree->Implem==ref_degree->Implem) {
					if(degree->type==FD_ADD_OPS) {
						hw_res_t* elt = NULL;
						bool b = avl_pp_find_data(&degree->target.addops.heap->type_mem, ref_degree->target.array.name, (void**)&elt);
						if(b==true) {
							if(elt->ports_r>0) continue;
						}
					}
				}
				new_list = addchain(new_list, degree);
			}
			freechain(*plist);
			*plist = new_list;

			break;
		}
		case FD_COND_WIRE : {
			// Remove all other WIRE degrees about this node.
			chain_list* new_list = NULL;
			foreach(*plist, scan) {
				freedom_degree_t* degree = scan->DATA;
				if(degree->Implem==ref_degree->Implem) {
					if(degree->type==FD_COND_WIRE && degree->target.hier.node == ref_degree->target.hier.node) continue;
				}
				new_list = addchain(new_list, degree);
			}
			freechain(*plist);
			*plist = new_list;
			break;
		}
		case FD_LOOP_UNROLL : {
			// Remove all other unroll degrees about this node.
			chain_list* new_list = NULL;
			foreach(*plist, scan) {
				freedom_degree_t* degree = scan->DATA;
				if(degree->Implem==ref_degree->Implem) {
					if(degree->type==FD_LOOP_UNROLL && degree->target.hier.node == ref_degree->target.hier.node) continue;
				}
				new_list = addchain(new_list, degree);
			}
			freechain(*plist);
			*plist = new_list;
			break;
		}
		case FD_MULTI :
			printf("WARNING %s:%d : freedoom degree type MULTI is not handled here.\n", __FILE__, __LINE__);
			break;
		default : break;
	}
}

int Core_Elaborate(implem_t* Implem) {
	int z;
	indent_t indent = Indent_Make(' ', 2, 0);

	if(Implem->H==NULL) {
		printf("ERROR : The elaboration core received an empty design.\n");
		return -1;
	}

	// Do scheduling
	Hier_Update_All(Implem);
	Schedule(Implem, SCHED_ALGO_LIST);
	Hier_Update_All(Implem);

	// The date of the timeout
	unsigned timeout_date = 0;
	if(param_timeout>0) timeout_date = time(NULL) + param_timeout;

	// Initialization of computing time
	TimeSpec_GetCpu(&Implem->CORE.begtime_cpu);
	TimeSpec_GetReal(&Implem->CORE.begtime_real);

	printf("Initial state:\n");

	//Hier_PrintTree(Implem->H);  // Debug
	Hier_Timing_Show(Implem);
	Hier_ClockCycles_Display(Implem);

	// When application of multiple freedom degrees fails, this variable is set to 1.
	// <0 means no limit. 1 is the simple case where only one FD is chosen.
	unsigned multiple_max = core_param_max_degrees;

	// Declared here because a rather large scope is needed
	forksynth_carac_t carac;
	implem_t* dupImplem = NULL;

	ForkSynth_SetOptions("rts");

	// Perform initial synthesis
	if(enable_duplication==true) {
		Core_DupImplem_EnsureVoid(Implem);
		dupImplem = Core_DupImplem_Synthesize(Implem, NULL);
		if(dupImplem==NULL) {
			printf("ERROR : Could not perform the initial synthesis.\n");
			return -1;
		}
		Implem->CORE.dup_implem = dupImplem;
		Core_DupImplem_SetResults(Implem, dupImplem);
	}
	else {
		z = ForkSynth_GetAndSetCarac(Implem);
		if(z!=0) {
			printf("ERROR : Could not perform the initial synthesis.\n");
			return -1;
		}
	}

	// Check that the resource limits are respected.
	Techno_NameRes_Print(Implem->CORE.resources_total, "Initial resource usage: ", "\n");
	bool b = Techno_NameRes_CheckInclude(Implem->synth_target->resources, Implem->CORE.resources_total);
	if(b==false) {
		printf("ERROR %s:%d : The initial design does not respect the resource limits !\n", __FILE__, __LINE__);
		return -1;
	}

	printf("Launching elaboration...\n");

	// Main elaboration loop
	// At each iteration, a bunch of freedom degrees is applied
	//   and the full characteristics of the design are got with a duplication.

	Implem->CORE.iteration = 0;

	do {

		// Check if the timeout delay is reached.
		if(timeout_date>0) {
			if(time(NULL) >= timeout_date) {
				printf("Timeout reached. Stopping elaboration.\n");
				break;
			}
		}
		// Check if the max nb of iterations is reached.
		if(param_max_iter>0) {
			if(Implem->CORE.iteration > param_max_iter) {
				printf("Max number of iterations reached. Stopping elaboration.\n");
				break;
			}
		}

		// Get freedom degrees
		printf("Building freedom degrees...\n");
		FreedomDegrees_BuildAndScore(Implem);
		if(Implem->CORE.freedom_degrees==NULL) {
			printf("No freedom degrees. The elaboration is finished.\n");
			break;
		}

		// Begin a new iteration
		printf("\n====== BEGIN ITERATION %d ====================================\n", Implem->CORE.iteration);

		// Update the output graph file
		// Only add one line. Writing the file header is done when opening the file.
		Core_Graph_AddPoint(Implem);

		// Dump VHDL if enabled
		// WARNING this call can modify the netlist (mostly insertion of signals)
		if(Implem->CORE.dup_implem!=NULL && export_vhdl.en==true) {
			if(Implem->CORE.iteration > export_vhdl.max_iter_idx) {
				printf("Warning: Not dumping VHDL for iteration %u (limit is %u).\n", Implem->CORE.iteration, export_vhdl.max_iter_idx);
			}
			else {
				char buffer[ strlen(export_vhdl.prefix) + 1 + export_vhdl.digits + 1 ];
				sprintf(buffer, "%s%0*u", export_vhdl.prefix, export_vhdl.digits, Implem->CORE.iteration);
				printf("Info: Dumping VHDL for iteration %u in directory %s\n", Implem->CORE.iteration, buffer);
				// Create the directory and change to it
				mkdir(buffer,  S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
				chdir(buffer);
				// Dump VHDL
				Map_Netlist_GenVHDL(Implem->CORE.dup_implem);
				// Dump hierarchy tree
				FILE* F = fopen("hier", "wb");
				Hier_Print_f(F, Implem->H);
				fclose(F);
				// Back at the previous directory
				chdir("..");
			}
		}

		printf("Freedom degrees present: %u\n", ChainList_Count(Implem->CORE.freedom_degrees));
		#if 0  // Display, for debug
		dbgprintf("Raw list of freedom degrees :\n");
		FreedomDegrees_Show(Implem);
		//printf("Press [return] to continue"); fflush(stdout); getchar();
		#endif

		// Compute the remaining resources
		ptype_list* remain_resources = dupptype(Implem->synth_target->resources);
		Techno_NameRes_Sub_raw(remain_resources, Implem->CORE.resources_total, 1, 1);

		Techno_NameRes_Print(Implem->CORE.resources_total, "Resource usage: ", "\n");
		Techno_NameRes_Print(remain_resources, "Remaining resources : ", "\n");

		// Delete the freedom degrees that exceed the resource limits
		chain_list* fd_res_check = NULL;
		foreach(Implem->CORE.freedom_degrees, scan) {
			freedom_degree_t* degree = scan->DATA;
			bool b = Techno_NameRes_CheckInclude(remain_resources, degree->resource_cost);
			if(b==false) {
				freedom_degree_free(degree);
				continue;
			}
			fd_res_check = addchain(fd_res_check, degree);
		}
		freechain(Implem->CORE.freedom_degrees);
		Implem->CORE.freedom_degrees = fd_res_check;
		if(Implem->CORE.freedom_degrees==NULL) {
			printf("All freedom degrees exceed the resource limits. The elaboration is finished.\n");
			break;
		}

		// Delete the freedom degrees that have a negative speedup
		chain_list* fd_time_check = NULL;
		foreach(Implem->CORE.freedom_degrees, scan) {
			freedom_degree_t* degree = scan->DATA;
			if(degree->time_gain<0) {
				freedom_degree_free(degree);
				continue;
			}
			fd_time_check = addchain(fd_time_check, degree);
		}
		freechain(Implem->CORE.freedom_degrees);
		Implem->CORE.freedom_degrees = fd_time_check;
		if(Implem->CORE.freedom_degrees==NULL) {
			printf("All freedom degrees have a negative time weight. The elaboration is finished.\n");
			break;
		}

		printf("Freedom degrees remaining after pruning: %u\n", ChainList_Count(Implem->CORE.freedom_degrees));
		// Display, for debug
		if(debug_show_avail_fd==true) {
			printf("The list of freedom degrees is:\n");
			FreedomDegrees_Show(Implem);
			//printf("Press [return] to continue"); fflush(stdout); getchar();
		}

		// Build a valid group of freedom degrees
		// Resource usage check is internally done.

		// This list will be read after the loop.
		chain_list* list_selected_degrees = NULL;

		do {

			// Resource limit for the group of freedom degrees
			ptype_list* cur_group_res_remain = dupptype(remain_resources);
			foreach(cur_group_res_remain, scan) scan->TYPE = scan->TYPE * param_max_res_ratio;

			chain_list* considered_fds = dupchain(Implem->CORE.freedom_degrees);

			list_selected_degrees = NULL;
			unsigned list_selected_degrees_nb = 0;

			// At each iteration, add one freedom degree to the group
			do {

				// Select the best freedom degree
				freedom_degree_t* selected_degree = NULL;
				double best_score = -1;
				foreach(considered_fds, scan) {
					freedom_degree_t* degree = scan->DATA;
					if(best_score>=0 && degree->final_score<=best_score) continue;
					best_score = degree->final_score;
					selected_degree = degree;
				}

				// FIXME Choose another freedom degree depending on available resources
				// Example : if a partial unroll is selected,
				//  	and if the full unroll also fits into the tight limit for the group,
				//		then select the full unroll instead.
				// WARNING there are cases where the HW cose of a loop unrolling is very badly understood.
				// A loop could use much more resources than expected, which would ruin the elaboration process
				// if several loops behave the same way.




				// The first selected freedom degree is always kept.
				// others are added only if their resource cost is within the margin.
				if(list_selected_degrees==NULL) {
					list_selected_degrees = addchain(list_selected_degrees, selected_degree);
					int z = Techno_NameRes_Sub_raw(cur_group_res_remain, selected_degree->resource_cost, 1, 0);
					if(z!=0) {
						// Not enough resources to add other freedom degrees
						break;
					}
				}
				else {
					// Check that this freedom degree fits in the resources
					bool b = Techno_NameRes_CheckInclude(cur_group_res_remain, selected_degree->resource_cost);
					if(b==true) {
						list_selected_degrees = addchain(list_selected_degrees, selected_degree);
						Techno_NameRes_Sub_raw(cur_group_res_remain, selected_degree->resource_cost, 1, 1);
					}
				}

				// If the limit of list size is set, check it
				list_selected_degrees_nb++;
				if(multiple_max>0 && list_selected_degrees_nb>=multiple_max) break;

				// Next iteration, choose other freedom degrees...
				considered_fds = ChainList_Remove(considered_fds, selected_degree);

				// Remove the freedom degrees that are incompatible
				// Example : loop partial and full unroll, ADDOPS...
				Core_RemoveIncompatibleFD(&considered_fds, selected_degree);

				// Loop finished ?
				if(considered_fds==NULL) break;

			} while(1);  // Loop that builds a list of freedom degrees

			freeptype(cur_group_res_remain);

			// Here we have a list of selected freedom degrees.
			// Their added resource cost is within tight limits.

			if(list_selected_degrees==NULL) {
				printf("INTERNAL ERROR %s:%d : Empty list of freedom degrees !\n", __FILE__, __LINE__);
				return -1;
			}

			// Now, try this list of freedom degrees

			// Display lots of details
			printf("A list of %u freedom degrees is built.\n", ChainList_Count(list_selected_degrees));
			// Display, for debug
			if(debug_show_applied_fd==true) {
				printf("The selected freedom degrees are:\n");
				foreach(list_selected_degrees, scan) {
					FreedomDegrees_Show_one(scan->DATA, &indent);
				}
				//printf("Press [return] to continue"); fflush(stdout); getchar();
			}

			if(enable_duplication==true) {
				printf("Evaluation with a duplication (can take a long time)...\n");

				Core_DupImplem_EnsureVoid(Implem);
				dupImplem = Core_DupImplem_Synthesize(Implem, list_selected_degrees);
				if(dupImplem==NULL) {
					printf("INTERNAL ERROR %s:%d (code %d) : Could not test this list of %u freedom degrees:\n", __FILE__, __LINE__, z,
						ChainList_Count(list_selected_degrees)
					);
					foreach(list_selected_degrees, scan) {
						FreedomDegrees_Show_one(scan->DATA, &indent);
					}
					return -1;
				}

				// Check that the resource limits are respected.
				bool b = Techno_NameRes_CheckInclude(dupImplem->synth_target->resources, dupImplem->CORE.resources_total);
				if(b==true) break;

				// Here the list fails. Too big.
				augh_implem_free(dupImplem);
				dupImplem = NULL;
			}
			else {
				printf("Evaluation with a fork (can take a long time)...\n");

				// Get accurate resource usage
				int z = ForkSynth_GetCarac_ListFreedomDegrees(Implem, &carac, list_selected_degrees);
				if(z!=0) {
					printf("INTERNAL ERROR %s:%d (code %d) : Could not test this list of %u freedom degrees:\n", __FILE__, __LINE__, z,
						ChainList_Count(list_selected_degrees)
					);
					foreach(list_selected_degrees, scan) {
						FreedomDegrees_Show_one(scan->DATA, &indent);
					}
					return -1;
				}

				// Check if the resource limits are respected
				bool b = Techno_NameRes_CheckInclude(Implem->synth_target->resources, carac.res_total);
				if(b==true) {
					// The list of freedom degrees is OK and respects the resource limits, keep it !
					break;
				}

				// Here the list fails. Too big.
				ForkSynth_FreeCarac(&carac);
			}

			// If there was only one freedom degree, flag it as impossible.
			// If there were several, from now on consider only mono-FD lists.
			unsigned count_fd = ChainList_Count(list_selected_degrees);
			if(count_fd>1) {
				multiple_max = 1;
				printf("DEBUG %s:%d : A list of freedom degrees was too big. Now considering single freedom degree iterations.\n", __FILE__, __LINE__);
				// Do nothing more retry again
			}
			else {
				freedom_degree_t* degree = list_selected_degrees->DATA;
				// Flag this FD as impossible
				Core_FreedomDegree_FlagImpossible(Implem, degree);
				// Remove the freedom degrees from the main list in the Implem structure
				freedom_degree_free(degree);
				Implem->CORE.freedom_degrees = ChainList_Remove(Implem->CORE.freedom_degrees, degree);
				if(Implem->CORE.freedom_degrees==NULL) break;
			}

			// Iterate and try to build another group of freedom degrees
			freechain(list_selected_degrees); list_selected_degrees = NULL;

		}while(1);  // Loop that searches for a suitable group of freedom degrees

		//printf("DEBUG %s:%d : Press [return] to continue...", __FILE__, __LINE__); getchar();

		if(Implem->CORE.freedom_degrees==NULL) {
			printf("All freedom degrees exceed the resource limits (after test). The elaboration is finished.\n");
			break;
		}
		assert(list_selected_degrees!=NULL);

		printf("%u freedom degrees are applied in this iteration.\n", ChainList_Count(list_selected_degrees));
		// Display, for debug
		if(debug_show_applied_fd==true) {
			printf("The applied freedom degrees are:\n");
			foreach(list_selected_degrees, scan) {
				FreedomDegrees_Show_one(scan->DATA, &indent);
			}
			//printf("Press [return] to continue"); fflush(stdout); getchar();
		}

		// Mostly for debug : compute estimation drifts for each selected FD
		if(estim_drifts_en==true) {
			Core_EstimDrift_ChosenListFD(Implem, list_selected_degrees);
		}

		// Apply the selected freedom degrees
		z = Core_FreedomDegree_ApplyList(Implem, list_selected_degrees, &indent, 0);
		if(z!=0) return -1;

		freechain(list_selected_degrees);

		// Simplifications, time update
		Hier_Update_All(Implem);

		if(enable_duplication==true) {
			#if 0  // Redo synthesis
			Core_DupImplem_EnsureVoid(Implem);
			implem_t* dupImplem = Core_DupImplem_Synthesize(Implem, NULL);
			if(dupImplem==NULL) {
				printf("ERROR : Could not re-synthesize the design !\n");
				return -1;
			}
			Implem->CORE.dup_implem = dupImplem;
			Core_DupImplem_SetResults(Implem, DupImplem);
			#endif

			#if 1  // Keep the DupImplem data
			assert(dupImplem!=NULL);
			Implem->CORE.dup_implem = dupImplem;
			Core_DupImplem_SetResults(Implem, dupImplem);
			#endif
		}
		else {
			ForkSynth_SetCarac(Implem, &carac);
		}

		// Increment iteration count
		Implem->CORE.iteration++;

		// FIXME here re-scheduling needs to be launched only if operators were added
		// ... assuming that when applying a FD, the resulting BBs are immediately simplified and re-scheduled.

		printf("Re-scheduling...\n");

		// Only to know the execution time
		struct timespec oldtime;
		TimeSpec_GetReal(&oldtime);

		Hier_Update_All(Implem);
		Schedule(Implem, SCHED_ALGO_LIST);
		Hier_Update_All(Implem);

		// Display the execution time
		double diff = TimeSpec_DiffCurrReal_Double(&oldtime);
		printf("Re-scheduling done in %g seconds.\n", diff);

		// Pause, mostly for debug
		if(elabo_pause_end_iter==true) {
			printf("Press [return] to continue"); fflush(stdout);
			getchar();
		}

	} while(1);  // Main iteration loop

	printf("Updating hierarchy...\n");
	Hier_Update_All(Implem);

	if(enable_duplication==true) {
		// FIXME the duplicated Implem is already synthesized
		// It could be kept, instead of doing the same things on the original Implem
		Core_DupImplem_EnsureVoid(Implem);
	}

	return 0;
}



//======================================================================
// Command interpreter
//======================================================================

#include "command.h"

// return number of seconds, -1 on error
static int ParseDelay(const char *arg) {
	int val=0;
	int d=0, h=0, m=0, s=0;

	char const * p = arg;
	do {
		if(!isdigit(*p)) return -1;
		int t = atoi(p);
		while(isdigit(*p)) p++;
		switch(*p) {
			case 'd' : if(d) return -1; d=1; val+=t*24*3600; break;
			case 'h' : if(h) return -1; h=1; val+=t*3600; break;
			case 'm' : if(m) return -1; m=1; val+=t*60; break;
			case 's' : if(s) return -1; s=1; val+=t; break;
			case 0 : if(s) return -1; s=1; val+=t; break;  // same as seconds
			default : return -1;
		}
		p++;
	}while(*p);

	return val;
}

extern int Core_Command(implem_t* Implem, command_t* cmd_data) {
	const char* cmd = Command_PopParam(cmd_data);
	if(cmd==NULL) {
		printf("Error 'core' : No command received. Try 'help'.\n");
		return -1;
	}

	if(strcmp(cmd, "help")==0) {
		printf(
			"Possible commands:\n"
			"  help             Display this help.\n"
			"  fd-build         Build the freedom degrees.\n"
			"  fd-view          Display the freedom degrees.\n"
			"  fd-clear         Make all freedom degrees possible again.\n"
			"  elaborate        Launch the elaboration of the circuit.\n"
			"                   Iterative transformations are applied.\n"
			"                   Note : the design must be scheduled already.\n"
			"\n"
			"Forbid some freedom degrees:\n"
			"  fd-skip-mem-r          Forbid adding memory read ports.\n"
			"  fd-skip-mem-w          Forbid adding memory write ports.\n"
			"  fd-skip-mem-d          Forbid enabling direct access for multiport memories.\n"
			"  fd-skip-addops         Forbid adding operators and read ports to memories.\n"
			"  fd-skip-inline         Forbid replacement of RAM & ROM.\n"
			"  fd-skip-inline-ram     Forbid RAM inlining.\n"
			"  fd-skip-inline-rom     Forbid ROM inlining.\n"
			"  fd-skip-wire-mux-seq   Forbid condition wiring, MUX style.\n"
			"  fd-skip-wire-one-cycle Forbid condition wiring, one-cycle ctyle.\n"
			"  fd-skip-unroll         Forbid applying loop unrolling.\n"
			"  fd-skip-unroll-part    Forbid applying partial loop unrolling.\n"
			"  fd-skip-unroll-full    Forbid applying full loop unrolling.\n"
			"  fd-skip-pp-direct      Forbid implementing ping-pong buffers as registers.\n"
			"\n"
			"Export characteristics during elaboration:\n"
			"  graph-file set <filename>\n"
			"             open [<filename>]\n"
			"             append [<filename>]\n"
			"             close\n"
			"             unset\n"
			"\n"
			"Synthesis of a duplicated Implem:\n"
			"  dupimplem redirect <commands>\n"
			"    Configure stdout redirection during process.\n"
			"    Possible commands:\n"
			"      none                   No redirection. All is dumped in current terminal.\n"
			"      reset                  Set redirection to /dev/null [default].\n"
			"      file <filename>        Set redirection to a particular file (overwritten at each iteration).\n"
			"      show | disp | print    Display redirection configuration.\n"
			"\n"
			"Other parameters:\n"
			"  elabo-fd-max <val>         Set the max number of freedom degrees that can be selected for one iteration.\n"
			"                             The value 0 means unlimited.\n"
			"  elabo-iter-max <val>       Set the max number of iterations.\n"
			"                             The value 0 means unlimited.\n"
			"  elabo-res-ratio-max <val>  Set the maximum resource usage to list the freedom degrees for one iteration.\n"
			"                             The value is the ratio of the remaining resources. The accepted range is 0..1.\n"
			"  elabo-timeout <val>        Set a timeout to the elaboration process. Checked at the beginning of each iteration.\n"
			"  elabo-pause-end-iter [<bool>]\n"
			"                             Pause at end of iteration, wait for hitting <enter> key (init=no, default=yes).\n"
			"                             Example of syntax: 2d4h30m1s\n"
			" weigths-without-annotations [<bool>]\n"
			"                             Dis-handling of annotations when building freedom degree weights.\n"
			"                             Init=no, default=yes.\n"
			" fd-weight-exact [<bool>]    Use exact weighting of freedom degrees. Init=no, default=yes.\n"
			" handle-excess-ops [<bool>]  Take into account the operators already in excess in the design for FD addops.\n"
			"                             WARNING: Not stable yet. Set to true only for debug.\n"
			"                             Init=no, default=yes.\n"
			" dump-vhdl [<bool>]          Dump VHDL of all solutions during exploration. Init=no, default=yes.\n"
			" dump-estim-drifts [<bool>]  Dump estimation drifts. Init=no, default=yes.\n"
			"\n"
			"Debug parameters:\n"
			" debug-show-avail-fd [<bool>]\n"
			" debug-show-select-fd [<bool>]\n"
			" debug-show-applied-fd [<bool>]\n"
		);
		return 0;
	}

	// Set parameters

	if(strcmp(cmd, "graph-file")==0) {
		const char* param = Command_PopParam(cmd_data);
		if(param==NULL) {
			printf("Error 'core', command '%s' : Missing parameter. Try 'help'.\n", cmd);
			return -1;
		}
		if(strcmp(param, "set")==0) {
			// Only set the file name, do not open the file.
			char* filename = Command_PopParam_stralloc(cmd_data);
			if(filename==NULL) {
				printf("Error 'core', command '%s %s' : Missing file name. Try 'help'.\n", cmd, param);
				return -1;
			}
			elaboration_graph_filename = filename;
		}
		else if(strcmp(param, "open")==0 || strcmp(param, "append")==0) {
			// Set the file name, AND open the file
			// If a filename is specified, it replaces the previous one.
			char* filename = Command_PopParam_stralloc(cmd_data);
			if(filename!=NULL) {
				elaboration_graph_filename = filename;
				if(elaboration_graph_file!=NULL) {
					fclose(elaboration_graph_file);
					elaboration_graph_file = NULL;
				}
			}
			if(elaboration_graph_filename==NULL) {
				printf("Error 'core', command '%s %s' : No file name is set. Try 'help'.\n", cmd, param);
				return -1;
			}
			if(elaboration_graph_file==NULL) {
				if(strcmp(param, "append")==0) elaboration_graph_file = fopen(elaboration_graph_filename, "ab");
				else elaboration_graph_file = fopen(elaboration_graph_filename, "wb");
				if(elaboration_graph_file==NULL) {
					printf("Error 'core', command '%s %s' : Could not open the file '%s'.\n", cmd, param, filename);
					return -1;
				}
				Core_Graph_WriteHeader(Implem);
			}
		}
		else if(strcmp(param, "close")==0) {
			// Only close the file.
			if(elaboration_graph_file!=NULL) {
				fclose(elaboration_graph_file);
				elaboration_graph_file = NULL;
			}
		}
		else if(strcmp(param, "unset")==0) {
			// Close the file and erase the file name.
			if(elaboration_graph_filename==NULL) return 0;
			if(elaboration_graph_file!=NULL) {
				fclose(elaboration_graph_file);
				elaboration_graph_file = NULL;
			}
			elaboration_graph_filename = NULL;
		}
		else {
			printf("Error 'core', command '%s' : Unknown parameter '%s'.\n", cmd, param);
			return -1;
		}
		return 0;
	}

	else if(strcmp(cmd, "dupimplem")==0) {
		const char* param = Command_PopParam(cmd_data);
		if(param==NULL) {
			printf("Error 'core', command '%s' : Missing parameter. Try 'help'.\n", cmd);
			return -1;
		}
		if(strcmp(param, "redirect")==0) {
			const char* redir_param = Command_PopParam(cmd_data);
			if(redir_param==NULL) {
				printf("Error 'core', command '%s %s' : Missing parameter. Try 'help'.\n", cmd, param);
				return -1;
			}
			if(strcmp(redir_param, "none")==0) {
				dupimplem_redirect = NULL;
			}
			else if(strcmp(redir_param, "reset")==0) {
				dupimplem_redirect = "/dev/null";
			}
			else if(strcmp(redir_param, "file")==0) {
				// Only set the file name, do not open the file.
				char* filename = Command_PopParam_stralloc(cmd_data);
				if(filename==NULL) {
					printf("Error 'core', command '%s %s %s' : Missing file name. Try 'help'.\n", cmd, param, redir_param);
					return -1;
				}
				dupimplem_redirect = filename;
			}
			else if(strcmp(redir_param, "show")==0 || strcmp(redir_param, "print")==0 ||
			        strcmp(redir_param, "display")==0 || strcmp(redir_param, "disp")==0
			) {
				if(dupimplem_redirect==NULL) {
					printf("No redirection\n");
				}
				else {
					printf("Redirection to %s\n", dupimplem_redirect);
				}
			}
			else {
				printf("Error 'core', command '%s %s' : Unknown parameter '%s'.\n", cmd, param, redir_param);
				return -1;
			}
		}
		else {
			printf("Error 'core', command '%s' : Unknown parameter '%s'.\n", cmd, param);
			return -1;
		}
		return 0;
	}

	else if(strcmp(cmd, "handle-excess-ops")==0) {
		int z = Command_PopParam_YesNoDef_Verbose(cmd_data, true, cmd);
		if(z<0) return -1;
		core_dupimplem_inxs = z;
	}

	else if(strcmp(cmd, "debug-show-avail-fd")==0) {
		int z = Command_PopParam_YesNoDef_Verbose(cmd_data, true, cmd);
		if(z<0) return -1;
		debug_show_avail_fd = z;
	}
	else if(strcmp(cmd, "debug-show-select-fd")==0) {
		int z = Command_PopParam_YesNoDef_Verbose(cmd_data, true, cmd);
		if(z<0) return -1;
		debug_show_selected_fd = z;
	}
	else if(strcmp(cmd, "debug-show-applied-fd")==0) {
		int z = Command_PopParam_YesNoDef_Verbose(cmd_data, true, cmd);
		if(z<0) return -1;
		debug_show_applied_fd = z;
	}
	else if(strcmp(cmd, "weigths-without-annotations")==0) {
		int z = Command_PopParam_YesNoDef_Verbose(cmd_data, true, cmd);
		if(z<0) return -1;
		weights_without_annotations = z;
	}

	else if(strcmp(cmd, "elabo-fd-max")==0) {
		const char* param = Command_PopParam(cmd_data);
		if(param==NULL) {
			printf("Error 'core', command '%s' : Missing parameter. Try 'help'.\n", cmd);
			return -1;
		}
		core_param_max_degrees = atoi(param);
	}
	else if(strcmp(cmd, "elabo-iter-max")==0) {
		const char* param = Command_PopParam(cmd_data);
		if(param==NULL) {
			printf("Error 'core', command '%s' : Missing parameter. Try 'help'.\n", cmd);
			return -1;
		}
		param_max_iter = atoi(param);
	}
	else if(strcmp(cmd, "elabo-res-ratio-max")==0) {
		const char* param = Command_PopParam(cmd_data);
		if(param==NULL) {
			printf("Error 'core', command '%s' : Missing parameter. Try 'help'.\n", cmd);
			return -1;
		}
		double f = atof(param);
		if(f<0 || f>1) {
			printf("Error : For command '%s', accpted parameter range is [0;1] but received %g.\n", cmd, f);
			return -1;
		}
		param_max_res_ratio = f;
	}
	else if(strcmp(cmd, "elabo-timeout")==0) {
		const char* str_time = Command_PopParam(cmd_data);
		if(str_time==NULL) {
			printf("Error 'core', command '%s' : Missing parameter. Try 'help'.\n", cmd);
			return -1;
		}
		int val = ParseDelay(str_time);
		if(val<0) {
			printf("Error: Invalid time parameter for '%s'\n", cmd);
			return -1;
		}
		param_timeout = val;
	}
	else if(strcmp(cmd, "elabo-pause-end-iter")==0) {
		int z = Command_PopParam_YesNoDef_Verbose(cmd_data, true, cmd);
		if(z<0) return -1;
		elabo_pause_end_iter = z;
	}

	else if(strcmp(cmd, "fd-weight-exact")==0) {
		int z = Command_PopParam_YesNoDef_Verbose(cmd_data, true, cmd);
		if(z<0) return -1;
		core_param_weight_fork = z;
	}

	else if(strcmp(cmd, "dump-vhdl")==0) {
		int z = Command_PopParam_YesNoDef_Verbose(cmd_data, true, cmd);
		if(z<0) return -1;
		export_vhdl.en = z;
	}

	else if(strcmp(cmd, "dump-estim-drifts")==0) {
		int z = Command_PopParam_YesNoDef_Verbose(cmd_data, true, cmd);
		if(z<0) return -1;
		estim_drifts_en = z;
	}

	else if(strcmp(cmd, "fd-skip-mem-r")==0) {
		Implem->CORE.freedom_degrees_skipped_ops = ChainNum_Add_NoDup(Implem->CORE.freedom_degrees_skipped_ops, FD_SKIP_MEM_R);
	}
	else if(strcmp(cmd, "fd-skip-mem-w")==0) {
		Implem->CORE.freedom_degrees_skipped_ops = ChainNum_Add_NoDup(Implem->CORE.freedom_degrees_skipped_ops, FD_SKIP_MEM_W);
	}
	else if(strcmp(cmd, "fd-skip-mem-d")==0) {
		Implem->CORE.freedom_degrees_skipped_ops = ChainNum_Add_NoDup(Implem->CORE.freedom_degrees_skipped_ops, FD_SKIP_MEM_D);
	}
	else if(strcmp(cmd, "fd-skip-addops")==0) {
		Implem->CORE.freedom_degrees_skipped_ops = ChainNum_Add_NoDup(Implem->CORE.freedom_degrees_skipped_ops, FD_SKIP_ADDOPS);
	}
	else if(strcmp(cmd, "fd-skip-inline")==0) {
		Implem->CORE.freedom_degrees_skipped_ops = ChainNum_Add_NoDup(Implem->CORE.freedom_degrees_skipped_ops, FD_SKIP_INLINE);
	}
	else if(strcmp(cmd, "fd-skip-inline-ram")==0) {
		Implem->CORE.freedom_degrees_skipped_ops = ChainNum_Add_NoDup(Implem->CORE.freedom_degrees_skipped_ops, FD_SKIP_INLINE_RAM);
	}
	else if(strcmp(cmd, "fd-skip-inline-rom")==0) {
		Implem->CORE.freedom_degrees_skipped_ops = ChainNum_Add_NoDup(Implem->CORE.freedom_degrees_skipped_ops, FD_SKIP_INLINE_ROM);
	}
	else if(strcmp(cmd, "fd-skip-wire-seq-auto")==0) {
		Implem->CORE.freedom_degrees_skipped_ops = ChainNum_Add_NoDup(Implem->CORE.freedom_degrees_skipped_ops, FD_SKIP_WIRE_SEQ_AUTO);
	}
	else if(strcmp(cmd, "fd-skip-wire-one-cycle")==0) {
		Implem->CORE.freedom_degrees_skipped_ops = ChainNum_Add_NoDup(Implem->CORE.freedom_degrees_skipped_ops, FD_SKIP_WIRE_ONECYCLE);
	}
	else if(strcmp(cmd, "fd-skip-unroll")==0) {
		Implem->CORE.freedom_degrees_skipped_ops = ChainNum_Add_NoDup(Implem->CORE.freedom_degrees_skipped_ops, FD_SKIP_UNROLL);
	}
	else if(strcmp(cmd, "fd-skip-unroll-part")==0) {
		Implem->CORE.freedom_degrees_skipped_ops = ChainNum_Add_NoDup(Implem->CORE.freedom_degrees_skipped_ops, FD_SKIP_UNROLL_PART);
	}
	else if(strcmp(cmd, "fd-skip-unroll-full")==0) {
		Implem->CORE.freedom_degrees_skipped_ops = ChainNum_Add_NoDup(Implem->CORE.freedom_degrees_skipped_ops, FD_SKIP_UNROLL_FULL);
	}
	else if(strcmp(cmd, "fd-skip-pp-direct")==0) {
		Implem->CORE.freedom_degrees_skipped_ops = ChainNum_Add_NoDup(Implem->CORE.freedom_degrees_skipped_ops, FD_SKIP_PP_DIRECT);
	}

	// Check

	else if(Implem->H==NULL) {
		printf("Error [core] : No design is loaded.\n");
		return -1;
	}

	// Processes

	else if(strcmp(cmd, "fd-build")==0) {
		FreedomDegrees_Build(Implem);
	}
	else if(strcmp(cmd, "fd-view")==0) {
		FreedomDegrees_Show(Implem);
	}
	else if(strcmp(cmd, "fd-clear")==0) {
		Core_FreedomDegree_ClearAllImpossible(Implem);
	}

	else if(strcmp(cmd, "elaborate")==0) {
		// Only to know the execution time
		struct timespec oldtime;
		TimeSpec_GetReal(&oldtime);

		int z = Core_Elaborate(Implem);

		// Display the execution time
		double diff = TimeSpec_DiffCurrReal_Double(&oldtime);
		printf("Elaboration done in %g seconds.\n", diff);

		return z;
	}

	else {
		printf("Error [core] : Unknown command '%s'.\n", cmd);
		return -1;
	}

	return 0;
}


