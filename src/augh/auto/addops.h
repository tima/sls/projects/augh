
#ifndef _ADDOPS_H_
#define _ADDOPS_H_

#include <stdbool.h>

#include "auto.h"



//================================================
// Definitions of structures
//================================================

typedef struct {
	unsigned trans_nb;
	// Info about output
	unsigned out_max_width;
	unsigned out_max_left;
	unsigned out_min_right;
	// Info about operands
	unsigned  op_max_nb;
	unsigned  op_max_width;
	int       op_max_of_min_width;
	num_list* op_max_widthes;
} op_usage_t;

void OpUsageStats_Init(op_usage_t* res);
void OpUsageStats_Reset(op_usage_t* res);
void OpUsageStats_Print(op_usage_t* res);

void OpUsageStats_Get_Implem(implem_t* Implem, char* op_type, op_usage_t* res);


//================================================
// Functions
//================================================

int addops_command(implem_t* Implem, command_t* cmd_data);



#endif  // _ADDOPS_H_

