
/*
This file contains the scheduling process and all other functions
that exploit the data produced by the scheduler.

FIXME: The scheduler should use VexAlloc for better rapidity and better operator sharing
FIXME: The scheduler should take into account sub-expressions already selected at the current State
  to decide the number of operators missing to schedule another Action
  This is why so many operators are added as freedom degree and deleted after mapping.
  Actually the scheduler needs them to be present to schedule that properly.
FIXME Also, optimize scheduler rapidity, it is very relevant

Idea for powerful operator sharing:
- Insert all sub-expressions of the BB in a VexAlloc dictionary
- Each Action has a list of all needed sub-expressions that involve a shared operator
- At the beginning of the scheduling, all Actions have their list of needed Expr full
- During scheduling, when looking for an extra Action to schedule,
  scan all remaining Actions,
    scan all their needed sub-expr,
      if enough operators remain, this Action is selected
        and all its sub-expressions are added as available
      else it is disabled (for the current State only)

Problem: VexAlloc has no possibility to recognize that VEX_GE and VEX_SUB are the same operator,
but the current solution handles it with VexList_IsHere_SameOper() under the hood.
=> We need to add this functionality to VexAlloc.
Note: The mapper uses VexAlloc... would it fail in the appropriate circumstance?

*/


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <time.h>    // To display the scheduling time

#include "auto.h"
#include "memtransfo.h"
#include "hwres.h"
#include "schedule.h"
#include "techno.h"
#include "core.h"
#include "map.h"
#include "../hvex/hvex_misc.h"



//================================================
// Some parameters
//================================================

// For debug: dump data dependencies in a file
char* depend_dump_filename = NULL;
FILE* depend_dump_file = NULL;

bool sched_use_vexalloc_share = false;



//================================================
// Basic allocation and manipulation of nodes
// For the dependency graph
//================================================

// Create a pool of type : act_dep_t

#define POOL_prefix      pool_act_dep
#define POOL_data_t      act_dep_t
#define POOL_ALLOC_SIZE  1000

#define POOL_INSTANCE             POOL_ACT_DEP
#define POOL_INSTANCE_ATTRIBUTES  static

#include "../pool.h"

// Allocation wrappers

static act_dep_t* act_dep_new() {
	act_dep_t* act = pool_act_dep_pop(&POOL_ACT_DEP);
	memset(act, 0, sizeof(*act));
	avl_pi_init(&act->deps.timed);
	avl_pi_init(&act->deps.before);
	avl_pi_init(&act->deps.after);
	return act;
}
static void act_dep_free(act_dep_t* act) {
	// Dependency links
	avl_pi_reset(&act->deps.timed);
	avl_pi_reset(&act->deps.before);
	avl_pi_reset(&act->deps.after);
	// Other
	hwres_heap_free(act->needed_hwres);
	pool_act_dep_push(&POOL_ACT_DEP, act);
}


// Create a pool of type : state_dep_t

#define POOL_prefix      pool_state_dep
#define POOL_data_t      state_dep_t
#define POOL_ALLOC_SIZE  1000

#define POOL_INSTANCE             POOL_STATE_DEP
#define POOL_INSTANCE_ATTRIBUTES  static

#include "../pool.h"

// Allocation wrappers

static state_dep_t* state_dep_new() {
	state_dep_t* state_dep = pool_state_dep_pop(&POOL_STATE_DEP);
	memset(state_dep, 0, sizeof(*state_dep));
	return state_dep;
}
static void state_dep_free(state_dep_t* state_dep) {
	freechain(state_dep->acts);
	freechain(state_dep->acts_unscheduled);
	hwres_heap_free(state_dep->hwres_remain);
	pool_state_dep_push(&POOL_STATE_DEP, state_dep);
}

static state_dep_t* state_dep_new_enqueue(schedule_t* sched) {
	state_dep_t* state_dep = state_dep_new();
	if(sched->state_dep_first==NULL) {
		sched->state_dep_first = state_dep;
		state_dep->date = 0;
	}
	else {
		state_dep_t* prev_state = sched->state_dep_last;
		prev_state->NEXT = state_dep;
		state_dep->PREV = prev_state;
		state_dep->date = prev_state->date + 1;
	}
	sched->state_dep_last = state_dep;
	sched->state_nb_orig++;
	state_dep->ref_sched = sched;
	return state_dep;
}
static state_dep_t* state_sched_new_enqueue(schedule_t* sched) {
	state_dep_t* state_dep = state_dep_new();
	if(sched->state_sched_first==NULL) {
		sched->state_sched_first = state_dep;
		state_dep->date = 0;
	}
	else {
		state_dep_t* prev_state = sched->state_sched_last;
		prev_state->NEXT = state_dep;
		state_dep->PREV = prev_state;
		state_dep->date = prev_state->date + 1;
	}
	sched->state_sched_last = state_dep;
	sched->state_sched_nb++;
	state_dep->ref_sched = sched;
	return state_dep;
}

// Create a pool of type : schedule_t

#define POOL_prefix      pool_schedule
#define POOL_data_t      schedule_t
#define POOL_ALLOC_SIZE  1000

#define POOL_INSTANCE             POOL_SCHEDULE
#define POOL_INSTANCE_ATTRIBUTES  static

#include "../pool.h"

// Return a newly initialized scheduling structure.
static schedule_t* sched_new() {
	schedule_t* sched = pool_schedule_pop(&POOL_SCHEDULE);
	memset(sched, 0, sizeof(*sched));
	sched->algo  = SCHED_ALGO_LIST;
	sched->nores = false;
	return sched;
}
// Free the contents of the big scheduling structure. There is one per BB.
void Sched_free(schedule_t* sched) {
	foreach(sched->act_dep_list, scanAct) act_dep_free(scanAct->DATA);
	freechain(sched->act_dep_list);
	while(sched->state_dep_first!=NULL) {
		state_dep_t* state_next = sched->state_dep_first->NEXT;
		state_dep_free(sched->state_dep_first);
		sched->state_dep_first = state_next;
	}
	while(sched->state_sched_first!=NULL) {
		state_dep_t* state_next = sched->state_sched_first->NEXT;
		state_dep_free(sched->state_sched_first);
		sched->state_sched_first = state_next;
	}
	freechain(sched->begin_depacts);
	freechain(sched->finish_depacts);
	pool_schedule_push(&POOL_SCHEDULE, sched);
}

// The main Implem structure

void Sched_ImplemFree(implem_t* Implem) {
	Hier_ClearSched(Implem->H);
	if(Implem->SCHED.hwres_available!=NULL) hwres_heap_free(Implem->SCHED.hwres_available);
}
void Sched_ImplemInit(implem_t* Implem) {
}



//================================================
// Miscellaneous exploration
//================================================

state_dep_t* Sched_GetLastState_SymbolWritten_List(state_dep_t* state_ref, chain_list* list) {
	if(list==NULL) return NULL;  // Paranoia
	// Backward scan the States
	for(state_dep_t* state_dep=state_ref->PREV; state_dep!=NULL; state_dep=state_dep->PREV) {
		foreach(state_dep->acts, scanAct) {
			act_dep_t* act = scanAct->DATA;
			char* name = hvex_asg_get_destname(act->action->expr);
			foreach(list, scanVex) {
				hvex_t* vex = scanVex->DATA;
				if(hvex_vecidx_getname(vex)==name) return state_dep;
			}  // Scan the list of searched symbols
		}  // Scan the Act
	}  // Scan the States
	// No occurrence was found
	return NULL;
}

act_dep_t* Sched_GetWriteAct_Before(schedule_t* sched, act_dep_t* act, char* name) {
	foreach(act->state_orig->PREV, state_dep) {
		foreach(state_dep->acts, scanAct) {
			act_dep_t* other_act = scanAct->DATA;
			if(hvex_asg_get_destname(other_act->action->expr)==name) return other_act;
		}
	}
	return NULL;
}
act_dep_t* Sched_GetWriteAct_After(schedule_t* sched, act_dep_t* act, char* name) {
	foreach(act->state_orig->NEXT, state_dep) {
		foreach(state_dep->acts, scanAct) {
			act_dep_t* other_act = scanAct->DATA;
			if(hvex_asg_get_destname(other_act->action->expr)==name) return other_act;
		}
	}
	return NULL;
}



//================================================
// Building dependency links between Actions
//================================================

/* Set the dependency links between all actions of the basic block.
Arc types :
	WRITE_AFTER_WRITE
	READ_AFTER_WRITE
	WRITE_AFTER_READ
	TEST_AFTER_EXEC

Description :
WRITE_AFTER_WRITE
	This flag is set when a variable or array is written to in two consecutive States.
	If the symbol is a variable, and is not read in the second State,
		then actually the first assignation should be removed because the result is overriden by the second assignation.
	If the symbol is an array, the two addresses could be different,
		so the two accesses must remain.
READ_AFTER_WRITE
	This shows that a symbol is read in an action and written in a following one.
	The action where the symbol is written must be in a State before the actions where it is read.
WRITE_AFTER_READ
	Two actions with WaR dependencies can be scheduled in the same State,
	because the write will be done at the next rising edge, while the symbol is read.
	For that reason, it is possible to have several actions with mutual WaR dependency :
	these actions must be scheduled in the same State.
TEST_AFTER_EXEC
	This flag is actually specific to the way the tests and branchments are handled by UGH.
	The assignation is not an actual one, it is only a DEFINE.
	It does consume hardware resources only when the test is done.
	For that reason, the test must be scheduled in the last State of the basic block,
		and the branch will be wired in that State.
	This flag should be set between such an assignment and ALL other actions of the basic block,
		and it has the same behaviour as WRITE_AFTER_READ.

The following function adds the dependency links into its graph structure input.
For array accesses where addresses are constant and different, no dependency link is set.
	This improves scheduling, but more powerful analyzes could be done (see the comment block above).

*/

static inline void sched_depend_addflag(act_dep_t* source, act_dep_t* target, int flag) {
	avl_pi_addflag(&source->deps.after, target, flag);
	avl_pi_addflag(&target->deps.before, source, flag);
}
static inline bool depend_isthereflag(act_dep_t* source, act_dep_t* target, int flag) {
	avl_pi_node_t* avl_node = NULL;
	bool b = avl_pi_find(&source->deps.after, target, &avl_node);
	if(b==false) return false;
	if( (avl_node->data & flag)==0 ) return false;
	return true;
}

static inline chain_list* sched_depend_getSources(act_dep_t* ptr_act, int flag) {
	chain_list* list = NULL;
	avl_pi_foreach(&ptr_act->deps.before, scan) {
		if( (scan->data & flag)==flag ) list = addchain(list, scan->key);
	}
	return list;
}
static inline chain_list* sched_depend_getTargets(act_dep_t* ptr_act, int flag) {
	chain_list* list = NULL;
	avl_pi_foreach(&ptr_act->deps.after, scan) {
		if( (scan->data & flag)==flag ) list = addchain(list, scan->key);
	}
	return list;
}
static inline void sched_depend_removeTarget(act_dep_t* ptr_act, act_dep_t* target) {
	avl_pi_rem_key(&ptr_act->deps.after, target);
}
static inline void sched_depend_removeSource(act_dep_t* ptr_act, act_dep_t* source) {
	avl_pi_rem_key(&ptr_act->deps.before, source);
}
static inline void sched_depend_removeOneFlag(act_dep_t* source, act_dep_t* target, int flag) {
	avl_pi_rmflag(&source->deps.after, target, flag);
	avl_pi_rmflag(&target->deps.before, source, flag);
}


// Debug purposes only
__attribute((__unused__))
static int sched_depend_check(schedule_t* sched) {
	int errors_found = 0;

	foreach(sched->act_dep_list, scanAct) {
		act_dep_t* cur_act = scanAct->DATA;
		if(cur_act->action==NULL) continue;
		avl_pi_foreach(&cur_act->deps.before, scan) {
			act_dep_t* other_act = scan->key;
			if(other_act->action==NULL) {
				printf("ERROR dependency BEFORE with NULL action.\n");
				errors_found++;
				continue;
			}
			avl_pi_node_t* other_link;
			bool b = avl_pi_find(&other_act->deps.after, cur_act, &other_link);
			if(b==false) {
				printf("ERROR one-sided dependency BEFORE with flags 0x%x\n", scan->data);
				errors_found++;
				continue;
			}
			if(other_link->data!=scan->data) {
				printf("ERROR dependency flags mismatch BEFORE 0x%x OTHER 0x%x\n", scan->data, other_link->data);
				errors_found++;
			}
		}
		avl_pi_foreach(&cur_act->deps.after, scan) {
			act_dep_t* other_act = scan->key;
			if(other_act->action==NULL) {
				printf("ERROR dependency AFTER with NULL action.\n");
				errors_found++;
				continue;
			}
			avl_pi_node_t* other_link;
			bool b = avl_pi_find(&other_act->deps.before, cur_act, &other_link);
			if(b==false) {
				printf("ERROR one-sided dependency AFTER with type 0x%x\n", scan->data);
				errors_found++;
				continue;
			}
			if(other_link->data!=scan->data) {
				printf("ERROR dependency flags mismatch AFTER 0x%x OTHER 0x%x\n", scan->data, other_link->data);
				errors_found++;
			}
		}
	}
	if(errors_found>0) {
		printf("ERROR %d dependency errors were found.\n", errors_found);
		int count_links = 0;
		foreach(sched->act_dep_list, scanAct) {
			act_dep_t* cur_act = scanAct->DATA;
			if(cur_act->action==NULL) continue;
			count_links += avl_pi_count(&cur_act->deps.before);
			count_links += avl_pi_count(&cur_act->deps.after);
		}
		printf("ERROR Note : the total number of links is %d.\n", count_links);
	}

	return errors_found;
}
// Debug purposes only
static void sched_depend_dump_bb(FILE* F, schedule_t* sched) {

	fprintf(F, "\n");
	fprintf(F, "=================================================\n");
	fprintf(F, "BB %p sched %p\n", sched->node_bb, sched);
	fprintf(F, "=================================================\n");

	foreach(sched->act_dep_list, scanAct) {
		act_dep_t* cur_act = scanAct->DATA;
		fprintf(F, "\n");
		fprintf(F, "(%p) ", cur_act);
		HierAct_FPrint(F, cur_act->action);

		void print_dep_flags_be(long i, char* beg, char* end) {
			if(beg!=NULL) fprintf(F, beg);
			if( (i & SCHED_DEP_RAW)!=0 ) fprintf(F, " RaW");
			if( (i & SCHED_DEP_WAR)!=0 ) fprintf(F, " WaR");
			if( (i & SCHED_DEP_WAW)!=0 ) fprintf(F, " WaW");
			if(end!=NULL) fprintf(F, end);
		}
		void print_deps(avl_pi_tree_t* tree, char* name, bool flags) {
			unsigned nb_deps = avl_pi_count(tree);
			if(nb_deps==0) return;
			fprintf(F, "  Deps %s (nb %u)\n", name, nb_deps);
			avl_pi_foreach(tree, scan) {
				fprintf(F, "    Act %p :", scan->key);
				if(flags==true) print_dep_flags_be(scan->data, NULL, "\n");
				else fprintf(F, " %d\n", scan->data);
			}
		}

		print_deps(&cur_act->deps.before, "BEF", true);
		print_deps(&cur_act->deps.after, "AFT", true);
		print_deps(&cur_act->deps.timed, "TIME", false);
	}

}

// Build the data dependency links inside a basic block
static void sched_depend_build(schedule_t* sched) {

	// Scan the BB, State by State, and insert dependency flags on the fly.
	// The States are scanned in the execution order (order of the instructions fom the C code).

	// This is a tree containing all actions done for each symbol in the design :
	//   Read and write actions, and the associated Action structures.
	// The tree is designed to avoid memory allocation.
	// The tree data is only one chain_list element.
	// The data of this element is a bitype_list* :
	//   FROM = Action
	//   TO   = The VEX (to handle addresses)
	//   TYPE = R/W (0=read, 1=write, >1=conditional write)

	enum { RW_READ = 0, RW_WRITE = 1 };
	typedef chain_list lastact_t;

	avl_pp_tree_t symbol_last_action;    // The elements are of type lastact_t*
	avl_pp_init(&symbol_last_action);

	void lastact_free(lastact_t* elt) {
		freebitype(elt->DATA);
		freechain(elt);
	}
	lastact_t* lastact_new() {
		return addchain(NULL, NULL);
	}
	lastact_t* lastact_search(char* name) {
		avl_pp_node_t* avl_node = NULL;
		int z = avl_pp_find(&symbol_last_action, name, &avl_node);
		if(z==true) return avl_node->data;
		return NULL;
	}
	lastact_t* lastact_searchadd(char* name) {
		avl_pp_node_t* avl_node = NULL;
		int z = avl_pp_add(&symbol_last_action, name, &avl_node);
		if(z==false) return avl_node->data;
		avl_node->data = lastact_new();
		return avl_node->data;
	}

	// Scan all the States of the Basic Block
	foreach(sched->state_dep_first, state_dep) {

		// Process the Actions of this State.

		// Consider the read symbols
		// Add the READ_AFTER_WRITE dependencies
		foreach(state_dep->acts, scanact) {
			act_dep_t* act_dep = scanact->DATA;
			hier_action_t* action = act_dep->action;

			chain_list* list_inl_acts = addchain(NULL, action);
			list_inl_acts = Hier_ListActions_AddInline(list_inl_acts);

			// List of symbols this Action depends on
			chain_list* list_read = NULL;
			foreach(list_inl_acts, scanInlAction) {
				hier_action_t* inl_action = scanInlAction->DATA;
				list_read = hvex_SearchSym_AddList(list_read, hvex_asg_get_expr(inl_action->expr), NULL);
				list_read = hvex_SearchSym_AddList(list_read, hvex_asg_get_addr(inl_action->expr), NULL);
				list_read = hvex_SearchSym_AddList(list_read, hvex_asg_get_cond(inl_action->expr), NULL);
			}

			// Sort by symbol name
			avl_pp_tree_t sorted_read;
			avl_pp_init(&sorted_read);
			foreach(list_read, scanRead) {
				hvex_t* Expr = scanRead->DATA;
				char* name = hvex_vecidx_getname(Expr);
				avl_pp_node_t* avl_node = NULL;
				bool b = avl_pp_add(&sorted_read, name, &avl_node);
				if(b==true) avl_node->data = NULL;
				avl_node->data = addchain(avl_node->data, Expr);
			}

			// Add the READ_AFTER_WRITE dependencies
			// Process registers and signals
			avl_pp_foreach(&sorted_read, scanSymbol) {
				char* name = scanSymbol->key;

				// Skip arrays
				chain_list* list_occur = scanSymbol->data;
				if(list_occur!=NULL) {
					hvex_t* readExpr = list_occur->DATA;
					if(readExpr->model==HVEX_INDEX) continue;
				}

				lastact_t* lastact = lastact_search(name);
				if(lastact==NULL) continue;

				// Add a dependency with the previous ACTs where the symbol was written
				foreach((bitype_list*)lastact->DATA, scan) {
					act_dep_t* prev_act = scan->DATA_FROM;
					// Ensure no dependency link is set from and to the same action. Example : k = k+1;
					if(prev_act==act_dep) continue;
					if(scan->TYPE!=RW_WRITE) continue;
					// Add the flag
					sched_depend_addflag(prev_act, act_dep, SCHED_DEP_RAW);
					break;
				}  // Scan the last accesses
			}  // Scan register & signal symbols

			// Process arrays
			avl_pp_foreach(&sorted_read, scanSymbol) {
				char* name = scanSymbol->key;

				// Keep only memories
				chain_list* list_occur = scanSymbol->data;
				if(list_occur==NULL) continue;
				hvex_t* readExpr = list_occur->DATA;
				if(readExpr->model!=HVEX_INDEX) continue;

				lastact_t* lastact = lastact_search(name);
				if(lastact==NULL) continue;

				// Add a READ_AFTER_WRITE dependency with the previous ACTs where the symbol was written
				foreach((chain_list*)scanSymbol->data, scanExpr) {
					hvex_t* readExpr = scanExpr->DATA;

					// List all symbols used in the address expression
					chain_list* list_address_read = hvex_SearchSym_AddList(NULL, hvex_index_get_addr(readExpr), NULL);
					// Find the last state where one of these symbols was written
					state_dep_t* address_last_write_state = NULL;
					if(list_address_read!=NULL) {
						address_last_write_state = Sched_GetLastState_SymbolWritten_List(state_dep, list_address_read);
						freechain(list_address_read);
					}

					// Scan all previous accesses and add the flags
					foreach((bitype_list*)lastact->DATA, scan) {
						act_dep_t* prev_act = scan->DATA_FROM;
						// Ensure no dependency link is set from and to the same action. Example : k = k+1;
						if(prev_act==act_dep) continue;
						if(scan->TYPE!=RW_WRITE) continue;
						// Flags
						bool depfound_raw = false;
						bool finished = false;
						// Handle when the symbols used in the address were modified
						if(address_last_write_state!=NULL && address_last_write_state->date>=prev_act->state_orig->date) {
							depfound_raw = true;
							finished = true;
						}
						else {
							// Compare the addresses
							int comp = hvex_Compare(hvex_index_get_addr(readExpr), hvex_asg_get_addr(scan->DATA_TO));
							// Same address
							if(comp==HVEX_COMPARE_EQUAL) depfound_raw = true;
							// Different address
							else if(comp==HVEX_COMPARE_DIFF) {}
							// Don't know if different or equal
							else { depfound_raw = true; finished = true; }
						}
						// Add the flags
						if(depfound_raw==true) sched_depend_addflag(prev_act, act_dep, SCHED_DEP_RAW);
						if(finished==true) break;
					}  // Last array accesses

				} // Scan all expressions read (focus on the addresses)

			}  // Scan array symbols

			// Add the read symbols to the lists of previous actions
			foreach(list_read, scan) {
				hvex_t* readExpr = scan->DATA;
				char* readName = hvex_vecidx_getname(readExpr);
				lastact_t* elt = lastact_searchadd(readName);
				elt->DATA = addbitype(elt->DATA, RW_READ, act_dep, readExpr);
			}  // processing of the list of read symbols

			freechain(list_inl_acts);
			freechain(list_read);
			avl_pp_foreach(&sorted_read, scan) freechain(scan->data);
			avl_pp_reset(&sorted_read);
		}  // Actions of the State

		// Consider the written symbols
		// Add the WRITE_AFTER_READ and WRITE_AFTER_WRITE dependencies
		foreach(state_dep->acts, scanact) {
			act_dep_t* act_dep = scanact->DATA;
			hier_action_t* action = act_dep->action;

			char* name = hvex_asg_get_destname(action->expr);

			lastact_t* lastact = lastact_search(name);
			if(lastact==NULL) continue;

			// Add a WRITE_AFTER_READ dependency with each previous ACT where the symbol was read

			if(hvex_asg_get_addr(action->expr)!=NULL) {  // Memory access

				// List all symbols used in the address expression
				chain_list* list_address_read = hvex_SearchSym_AddList(NULL, hvex_asg_get_addr(action->expr), NULL);
				// Find the last state where one of these symbols was written
				state_dep_t* address_last_write_state = NULL;
				if(list_address_read!=NULL) {
					address_last_write_state = Sched_GetLastState_SymbolWritten_List(state_dep, list_address_read);
					freechain(list_address_read);
				}

				// Scan all previous accesses and add the flags
				foreach((bitype_list*)lastact->DATA, scan) {
					act_dep_t* prev_act = scan->DATA_FROM;
					// Ensure no dependency link is set from and to the same action. Example : k = k+1;
					if(prev_act==act_dep) continue;
					// Flags
					bool depfound_war = false;
					bool depfound_waw = false;
					bool finished = false;
					// Handle when the symbols used in the address were modified
					if(address_last_write_state!=NULL && address_last_write_state->date >= prev_act->state_orig->date) {
						if(scan->TYPE==RW_READ) depfound_war = true;
						else { depfound_waw = true; finished = true; }
					}
					else {
						// Compare the addresses
						if(scan->TYPE==RW_READ) {
							int comp = hvex_Compare(hvex_asg_get_addr(action->expr), hvex_index_get_addr(scan->DATA_TO));
							// WaR if same addresses or if unknown
							if(comp<=0) depfound_war = true;
						}
						else {
							int comp = hvex_Compare(hvex_asg_get_addr(action->expr), hvex_asg_get_addr(scan->DATA_TO));
							// WaW if same addresses or if unknown
							if(comp<=0) { depfound_waw = true; finished = true; }
						}
					}
					// Add the flags
					if(depfound_war==true) sched_depend_addflag(prev_act, act_dep, SCHED_DEP_WAR);
					if(depfound_waw==true) sched_depend_addflag(prev_act, act_dep, SCHED_DEP_WAW);
					if(finished==true) break;
				}  // Last array accesses

			}  // End case array

			else {  // Access to register/signal/port
				foreach((bitype_list*)lastact->DATA, scan) {
					act_dep_t* prev_act = scan->DATA_FROM;
					// Ensure no dependency link is set from and to the same action. Example : k = k+1;
					if(scan->DATA_FROM==act_dep) continue;
					// Add the flags
					if(scan->TYPE==RW_READ) {
						sched_depend_addflag(prev_act, act_dep, SCHED_DEP_WAR);
					}
					if(scan->TYPE==RW_WRITE) {
						sched_depend_addflag(prev_act, act_dep, SCHED_DEP_WAW);
						break;
					}
				}  // Scan the last accesses
			}  // End case for register access

		}  // Actions of the State

		// Set the last ACT where the symbols are written to
		foreach(state_dep->acts, scanact) {
			act_dep_t* act_dep = scanact->DATA;
			hier_action_t* action = act_dep->action;

			char* name = hvex_asg_get_destname(action->expr);

			// The current ACT becomes the last ACT this symbol is written to
			lastact_t* elt = lastact_searchadd(name);
			elt->DATA = addbitype(elt->DATA, RW_WRITE, act_dep, action->expr);

		}  // Actions of the State

	}	 // States of the BB

	// Free the lists of last actions
	avl_pp_foreach(&symbol_last_action, avl_node) lastact_free(avl_node->data);
	avl_pp_reset(&symbol_last_action);

	// Add the SCHED_DEP_WAR flags for branch conditions at end of BB
	avl_p_tree_t last_assign_sig;
	avl_p_init(&last_assign_sig);
	foreach(sched->state_dep_first, state_dep) {
		// Scan all the actions
		foreach(state_dep->acts, scanact) {
			act_dep_t* act_dep = scanact->DATA;
			hier_action_t* action = act_dep->action;
			if( HierAct_FlagCheck(action, HIER_ACT_END)==true ) avl_p_add_overwrite(&last_assign_sig, act_dep, act_dep);
		}
	}
	avl_p_foreach(&last_assign_sig, scanAct) {
		act_dep_t* act_dep = scanAct->data;
		// Add flags with all other actions in the BB
		foreach(sched->act_dep_list, scanOtherAct) {
			act_dep_t* other_act = scanOtherAct->DATA;
			if( avl_p_isthere(&last_assign_sig, other_act)==true ) continue;
			sched_depend_addflag(other_act, act_dep, SCHED_DEP_WAR);
		}
		// Add an immediate timed dependency with the other Signal assignations
		avl_p_foreach(&last_assign_sig, scanOtherAct) {
			act_dep_t* other_act = scanOtherAct->data;
			if(other_act==act_dep) continue;
			avl_pi_add_overwrite(&act_dep->deps.timed, other_act, 0);
		}
	}
	avl_p_reset(&last_assign_sig);

	// For all Actions in the same State, look for circular WaR with several Actions
	// Add corresponding dependencies
	// Example:
	//   Act1 --WaR-> Act2 --WaR-> Act3 --WaR-> Act1
	// Do this:
	//   Act1 --WaR-> Act2
	//   Act1 --WaR-> Act3
	//   Act2 --WaR-> Act1
	//   Act2 --WaR-> Act3
	//   Act3 --WaR-> Act1
	//   Act3 --WaR-> Act2
	// Then it becomes obvious that Act1, Act2 and Act3 must be scheduled in the same State.
	//   And this is handled right after.
	foreach(sched->state_dep_first, state_dep) {
		do {
			bool redo = false;

			// Scan all Actions of this state
			foreach(state_dep->acts, scanAct1) {
				act_dep_t* act1 = scanAct1->DATA;
				chain_list* new_acts = NULL;

				// Scan all dependencies after, focus on WaR
				avl_pi_foreach(&act1->deps.after, scanAct2) {
					act_dep_t* act2 = scanAct2->key;
					if(act2->state_orig!=state_dep) continue;
					if(act2==act1) continue;
					if( (scanAct2->data & SCHED_DEP_WAR)==0 ) continue;
					// Scan all dependencies after, focus on WaR
					avl_pi_foreach(&act2->deps.after, scanAct3) {
						act_dep_t* act3 = scanAct3->key;
						if(act3->state_orig!=state_dep) continue;
						if(act3==act2 || act3==act1) continue;
						if( (scanAct3->data & SCHED_DEP_WAR)==0 ) continue;
						if( depend_isthereflag(act1, act3, SCHED_DEP_WAR)==true ) continue;
						// Save this Act3 to add to Act1 deps
						new_acts = addchain(new_acts, act3);
					}
				}

				// Add dependencies to act1
				if(new_acts!=NULL) {
					foreach(new_acts, scanActNew) {
						act_dep_t* act_new = scanActNew->DATA;
						sched_depend_addflag(act1, act_new, SCHED_DEP_WAR);
					}
					freechain(new_acts);
					redo = true;
				}

			}  // Scan all Actions

			if(redo==false) break;
		} while(1);
	}

	// Build groups of simultaneous Actions
	// FIXME would it be necessary to redo the search with grouped Actions ?

	foreach(sched->act_dep_list, scanAct) {
		act_dep_t* act = scanAct->DATA;
		chain_list* list_sim = NULL;

		// Scan the deps before, search WaR.
		// If the other also has a WaR after, with the current one, they are simultaneous.
		avl_pi_foreach(&act->deps.before, scanDepBefore) {
			if( (scanDepBefore->data & SCHED_DEP_WAR) == 0 ) continue;
			act_dep_t* other_act = scanDepBefore->key;
			avl_pi_node_t* node = NULL;
			if( avl_pi_find_node(&other_act->deps.before, act, &node)==false ) continue;
			if( (node->data & SCHED_DEP_WAR) == 0 ) continue;
			assert( (node->data & (SCHED_DEP_RAW | SCHED_DEP_WAW)) == 0 );
			// Here the two Act are simultaneous
			list_sim = addchain(list_sim, other_act);
		}

		// Create the dependency
		foreach(list_sim, scan) {
			act_dep_t* other_act = scan->DATA;
			// Add the timed dep
			assert( avl_pi_isthere(&act->deps.timed, other_act)==false );
			assert( avl_pi_isthere(&other_act->deps.timed, act)==false );
			avl_pi_add_overwrite(&act->deps.timed, other_act, 0);
			avl_pi_add_overwrite(&other_act->deps.timed, act, 0);
			// Remove the Before/After deps
			avl_pi_rem_key(&act->deps.before, other_act);
			avl_pi_rem_key(&act->deps.after, other_act);
			avl_pi_rem_key(&other_act->deps.before, act);
			avl_pi_rem_key(&other_act->deps.after, act);
		}

		// Clean
		freechain(list_sim);
	}

	// FIXME propagate generic timed dependencies, if any

	// Some cleaning
	// Remove dependencies After and Before with Actions that have timed dependencies
	foreach(sched->act_dep_list, scanAct) {
		act_dep_t* act = scanAct->DATA;
		avl_pi_rem_key(&act->deps.timed, act);
		avl_pi_rem_key(&act->deps.before, act);
		avl_pi_rem_key(&act->deps.after, act);
		avl_pi_foreach(&act->deps.timed, scanTimed) {
			act_dep_t* other_act = scanTimed->key;
			avl_pi_rem_key(&act->deps.before, other_act);
			avl_pi_rem_key(&act->deps.after, other_act);
		}
	}

	#if 0  // DEBUG Check the dependency links. This is also used to verify the check function xD
	printf("DEBUG checking consistency after creating the links.\n");
	sched_depend_check(sched);
	printf("Press [return] to continue... "); getchar();
	#endif

	#if 0  // DEBUG Print the dependency links
	sched_depend_dump_bb(stdout, sched);
	#endif

}



//================================================
// Initialization of scheduler data structures
//================================================

// Initialization of the database for this basic block
static void sched_build_needed_resources(schedule_t* sched) {

	// This will be made default in the future. For now it is rather slow.
	// WARNING: This needs corresponding stuff in mapping for enough operators to be present.
	if(sched_use_vexalloc_share==true) {

		// The dictionary of expressions
		hvex_dico_t vexalloc;
		VexAlloc_Init(&vexalloc);
		// Link between HierAction and VexAlloc_Ref
		// This way, operator sharing done in VexAlloc is kept
		avl_pp_tree_t tree_act2ref;
		avl_pp_init(&tree_act2ref);

		// Insert all expressions in the dictionary
		foreach(sched->act_dep_list, scanAct) {
			act_dep_t* ptr_dep = scanAct->DATA;
			hvex_dico_ref_t* ref = VexAlloc_Add(&vexalloc, ptr_dep->action->expr);
			avl_pp_add_overwrite(&tree_act2ref, ptr_dep->action, ref);
		}

		// Apply more extensive operator sharing
		VexAlloc_Share_Expensive(&vexalloc);

		// Build the needed resources from the VexAlloc dictionary
		foreach(sched->act_dep_list, scanAct) {
			act_dep_t* ptr_dep = scanAct->DATA;
			if(HierAct_FlagCheck(ptr_dep->action, HIER_ACT_INL)==true) continue;
			ptr_dep->needed_hwres = hwres_heap_getneed_actwithinline_vexalloc(ptr_dep->action, &tree_act2ref);
			if(hwres_heap_isempty(ptr_dep->needed_hwres)==true) {
				hwres_heap_free(ptr_dep->needed_hwres);
				ptr_dep->needed_hwres = NULL;
			}
		}  // Scan Actions

		// Clean
		VexAlloc_Clear(&vexalloc);
		avl_pp_reset(&tree_act2ref);

	}

	else {

		// Build the needed resources for each Action
		foreach(sched->act_dep_list, scanAct) {
			act_dep_t* ptr_dep = scanAct->DATA;
			if(HierAct_FlagCheck(ptr_dep->action, HIER_ACT_INL)==true) continue;
			ptr_dep->needed_hwres = hwres_heap_getneed_actwithinline(ptr_dep->action);
			if(hwres_heap_isempty_noreg(ptr_dep->needed_hwres)==true) {
				hwres_heap_free(ptr_dep->needed_hwres);
				ptr_dep->needed_hwres = NULL;
			}
		}  // Scan Actions

	}

	#if 0  // DEBUG display the hardware resources
	printf("\n BASIC BLOCK HARDARE RESOURCES ====================================\n\n");
	foreach(sched->act_dep_list, scanAct) {
		act_dep_t* ptr_dep = scanAct->DATA;
		viewvexexprbound(ptr_dep->act->VEX_ATOM);
		printf(" = ");
		viewvexexprboundln(ptr_dep->act->VEX_EXPR);
		// Print the needed resources
		hwres_heap_print(ptr_dep->needed_hwres);
	}
	#endif

}

// Initialization of the database for this basic block
static void sched_init_bb(schedule_t* sched) {

	// Only count the ACTs
	unsigned all_acts_nb = 0;
	unsigned all_states_nb = 0;
	foreach(sched->node_bb->NODE.BB.body, nodeState) {
		hier_state_t* state_data = &nodeState->NODE.STATE;
		foreach(state_data->actions, scanAction) {
			hier_action_t* action = scanAction->DATA;
			if(HierAct_FlagCheck(action, HIER_ACT_INL)==true) continue;
			all_acts_nb ++;
		}
		if(state_data->actions!=NULL) all_states_nb++;
	}  // Scan the States of the BB
	if(all_acts_nb==0) return;

	// Associate the ACTs to the cells of the dependency array
	// Must be done here because some ACTs might have to be scheduled together.
	sched->state_nb_orig = 0;
	foreach(sched->node_bb->NODE.BB.body, nodeState) {
		hier_state_t* state_data = &nodeState->NODE.STATE;
		if(state_data->actions==NULL) continue;
		// Use a new State element
		state_dep_t* state_dep = state_dep_new_enqueue(sched);
		state_dep->state = nodeState;
		foreach(state_data->actions, scanAction) {
			hier_action_t* action = scanAction->DATA;
			if(HierAct_FlagCheck(action, HIER_ACT_INL)==true) continue;
			act_dep_t* ptr_dep = act_dep_new();
			ptr_dep->action = action;
			ptr_dep->state_orig = state_dep;
			state_dep->acts = addchain(state_dep->acts, ptr_dep);
			sched->act_dep_list = addchain(sched->act_dep_list, ptr_dep);
		}
	}

	// Reorder the actions.
	sched->act_dep_list = reverse(sched->act_dep_list);

	// Copy timed dependencies from the Hier Action
	foreach(sched->act_dep_list, scanDepAct) {
		act_dep_t* depact = scanDepAct->DATA;
		avl_pi_foreach(&depact->action->deps_timed, scanDep) {
			hier_action_t* other_act = scanDep->key;
			int time_val = scanDep->data;
			if(time_val!=0) {
				printf("WARNING: Unhandled non-zero timed dependency. Skipped.\n");
				continue;
			}
			// Find the corresponding act_dep structure
			act_dep_t* other_depact = NULL;
			foreach(sched->act_dep_list, scanOther) {
				act_dep_t* scan_depact = scanOther->DATA;
				if(scan_depact->action!=other_act) continue;
				other_depact = scan_depact;
				break;
			}
			if(other_depact==NULL) {
				printf("WARNING: The target of a timed dependency is not in the same BB. Skipped.\n");
				continue;
			}
			// Add timed dependency
			avl_pi_node_t* pi_node = NULL;
			bool b = avl_pi_add(&depact->deps.timed, other_depact, &pi_node);
			if(b==false) {
				// Check coherency between time values
				if(pi_node->data!=time_val) {
					printf("WARNING: Incompatible timed dependencies found. Skipped.\n");
					continue;
				}
			}
			pi_node->data = time_val;
			// Also add the symmetric dependency
			avl_pi_add_overwrite(&other_depact->deps.timed, depact, -time_val);
		}
	}

	// Build the dependencies
	sched_depend_build(sched);

	// Build the needed operators for the actions
	if(sched->nores==false) {
		sched_build_needed_resources(sched);
	}

}

// Compute the ranks and rebuild the lists of initial and final actions
static void sched_rank_bb(schedule_t* sched) {

	// In case the lists are rebuilt
	freechain(sched->begin_depacts); sched->begin_depacts = NULL;
	freechain(sched->finish_depacts); sched->finish_depacts = NULL;

	// Build the list of last ACTs and first ACTs
	// Fill the dependency array and the hash table Act->DepArray
	foreach(sched->act_dep_list, scanAct) {
		act_dep_t* ptr_dep = scanAct->DATA;
		if(avl_pi_isempty(&ptr_dep->deps.before)==true) sched->begin_depacts = addchain(sched->begin_depacts, ptr_dep);
		if(avl_pi_isempty(&ptr_dep->deps.after)==true) sched->finish_depacts = addchain(sched->finish_depacts, ptr_dep);
	}

	// Build the ranks of the ACTs, starting from the last ACTs of the BB.

	foreach(sched->act_dep_list, scanAct) {
		act_dep_t* ptr_dep = scanAct->DATA;
		ptr_dep->rank = 0;
	}

	// Use trees to speedup the process
	avl_p_tree_t tree0, tree1;
	avl_p_init(&tree0);
	avl_p_init(&tree1);

	avl_p_tree_t* cur_acts = &tree0;
	foreach(sched->finish_depacts, scan) avl_p_add_overwrite(cur_acts, scan->DATA, scan->DATA);
	avl_p_tree_t* next_acts = &tree1;

	unsigned cur_rank = 0;
	do {

		avl_p_foreach(cur_acts, scan) {
			act_dep_t* ptr_dep = scan->data;
			ptr_dep->rank = cur_rank;
			// build the next list of actions
			avl_pi_foreach(&ptr_dep->deps.before, scanprev) {
				if( (scanprev->data & SCHED_DEP_WAW)!=SCHED_DEP_WAW &&
				    (scanprev->data & SCHED_DEP_RAW)!=SCHED_DEP_RAW
				) {
					continue;
				}
				avl_p_add_overwrite(next_acts, scanprev->key, scanprev->key);
			}
		}

		avl_p_reset(cur_acts);
		if(avl_p_isempty(next_acts)==true) break;

		// Swap the two trees
		avl_p_tree_t* tmp = cur_acts;
		cur_acts = next_acts;
		next_acts = tmp;

		cur_rank++;
	}while(1);

	// Paranoia
	avl_p_reset(&tree0);
	avl_p_reset(&tree1);

	//printf("Rank max = %d\n", cur_rank);

#if 0  // DEBUG print the first actions and the last actions.
	printf("\n BASIC BLOCK FIRST ACTS ====================================\n\n");
	foreach(sched->begin_depacts, scan) {
		act_dep_t* ptr_dep = scan->DATA;
		HierAct_DebugPrint(ptr_dep->action);
		printf("  RANK %d\n", ptr_dep->rank);
	}
	printf("\n BASIC BLOCK LAST ACTS ====================================\n\n");
	foreach(sched->finish_depacts, scan) {
		act_dep_t* ptr_dep = scan->DATA;
		HierAct_DebugPrint(ptr_dep->action);
	}
#endif

}



//================================================
// Main Scheduler processes
//================================================

/* Actual scheduling function
Only builds the new assignations of actions on states.

Algorithm :

	Start from the list of ACTs that can be scheduled first (sched->begin_depacts)
	Loop : build States {
		Initialize : All hardware resources are available for the current State
		Loop : add actions to the current State {
			Select one action with the highest rank (cost function etc)
			If no Act can be added, this State is finished, exit
			Update the list of available resources
		}
		If no ACT was selected, ERROR
		Create a new State and add the ACTs to it, enqueue in the new list of states.
		Update the list of ACT that can be scheduled
		If empty list : exit.
	}


NOTE About the action types :
	At scheduling time, no function call, nor break, return etc should remain.

FIXME return a flag indicating if the generated number of States is the maximum
	allowed by the dependency flags.
	It will show if something can be optimized by addition of operators,

FIXME when returning the operators that could make scheduling better,
	only the ones that affect the critical path are interesting.
	It is easy to flag the ACTs that are in ONE of the critical pathes,
	but guaranteeing that one operator WILL make scheduling better is hard.
	Example of data-path :

	        |-> act10 -> act11 -|
	act00 ->|                   |-> act30
	        |-> act20 -> act21 -|

	In the example above, there are 2 critical pathes.
	Adding one operator may improve a path, but the global basic block can only be improved if all the critical pathes are improved...

	The ACTs of the critical pathes (where there are several critical pathes) could be flagged...
	But that is not enough when the graph is complex and intricated...

*/

// Scheduler flag to track the schedulability of Actions
#define ACTSTATUS_LATER   0  // The Action can't be selected for this State
#define ACTSTATUS_AVAIL   1  // The Action is schedulable at the current State
#define ACTSTATUS_SELECT  2  // The Action is selected to be scheduled at the current State
#define ACTSTATUS_SCHED   3  // The Action is already scheduled

// Check data dependencies, only those BEFORE
static bool sched_check_schedulable_depbef(act_dep_t* act_dep) {
	avl_pi_foreach(&act_dep->deps.before, scanBefore) {
		act_dep_t* act_prev = scanBefore->key;
		// No dependency is checked with actions already scheduled.
		if(act_prev->flag_sched == ACTSTATUS_SCHED) continue;
		// Only if the previous action is selected for the current state,
		// this tested action could be selected too, if allowed by the dependency flags.
		if(act_prev->flag_sched <= ACTSTATUS_AVAIL) return false;
		if( (scanBefore->data & SCHED_DEP_RAW)==SCHED_DEP_RAW ) return false;
		if( (scanBefore->data & SCHED_DEP_WAW)==SCHED_DEP_WAW ) return false;
	}
	return true;
}

// return 0 if OK.
static int sched_process_bb(schedule_t* sched, hwres_heap_t* all_resources) {

	#if 0  // DEBUG print all the actions
		printf("\n");
		printf("==== BASIC BLOCK ACTIONS ====================================\n");
		foreach(sched->act_dep_list, scan) {
			act_dep_t* ptr_dep = scan->DATA;
			if(ptr_dep->action==NULL) continue;
			printf("  (%p) RANK = %d ", ptr_dep, ptr_dep->rank);
			HierAct_Print(ptr_dep->action);
		}
		printf("==== END BASIC BLOCK ACTIONS ================================\n");
	#endif

	#if 0  // DEBUG print the number of actions
		unsigned act_nb = 0;
		for(unsigned i=0; i<sched->dep_array_nb; i++) {
			act_dep_t* ptr_dep = sched->dep_array + i;
			if(ptr_dep->act==NULL) continue;
			act_nb++;
		}
		printf("==== BASIC BLOCK : %d actions ================================\n", act_nb);
	#endif

	// Initialize the flags of the ACTs as not scheduled and not selected.
	foreach(sched->act_dep_list, scanAct) {
		act_dep_t* ptr_dep = scanAct->DATA;
		ptr_dep->state_sched = NULL;
		ptr_dep->flag_sched = ACTSTATUS_LATER;
	}

	sched->state_sched_first = NULL;
	sched->state_sched_last = NULL;

	// Build the actions that can be scheduled in the first State
	chain_list* cycle_possible_acts = dupchain(sched->begin_depacts);
	// Handle when the basic block contains only test actions.
	// In this case, all actions are target of WaR dependencies, so nothing is selected...
	if(cycle_possible_acts==NULL) {
		int found = 0;
		foreach(sched->act_dep_list, scanAct) {
			act_dep_t* ptr_dep = scanAct->DATA;
			if(ptr_dep->action==NULL) continue;
			assert(avl_pi_isempty(&ptr_dep->deps.before)==false);  // paranoia
			found ++;
			avl_pi_foreach(&ptr_dep->deps.before, scanDep) assert(scanDep->data==SCHED_DEP_WAR);
		}
		assert(found!=0);  // paranoia
		foreach(sched->act_dep_list, scanAct) {
			act_dep_t* ptr_dep = scanAct->DATA;
			if(ptr_dep->action==NULL) continue;
			cycle_possible_acts = addchain(cycle_possible_acts, ptr_dep);
		}
	}

	// Flag Actions as AVAILABLE
	foreach(cycle_possible_acts, scan) {
		act_dep_t* ptr_act = scan->DATA;
		ptr_act->flag_sched = ACTSTATUS_AVAIL;
	}

	#if 0  // DEBUG print the first actions
		printf("\n");
		printf("==== BASIC BLOCK FIRST ACTIONS ====================================\n");
		foreach(cycle_possible_acts, scan) {
			act_dep_t* ptr_dep = scan->DATA;
			if(ptr_dep->action==NULL) continue;
			printf("  (%p) RANK = %d ", ptr_dep, ptr_dep->rank);
			HierAct_Print(ptr_dep->action);
		}
		printf("==== END BASIC BLOCK FIRST ACTIONS ================================\n");
	#endif

	// Loop on the scheduled States
	do {

		// Initialize the hardware resources : all available.
		hwres_heap_t* cycle_resources = NULL;
		if(sched->nores==false) {
			cycle_resources = hwres_heap_dup(all_resources);
		}

		// Try to select the first Actions from this list
		chain_list* acts_ordered = NULL;

		#if 0  // Action list not sorted
		acts_ordered = dupchain(cycle_possible_acts);
		#endif

		#if 1  // Sort the Action list, decreasing rank
		// Use an AVL tree to do the sorting
		avl_ip_tree_t tmp_tree;
		avl_ip_init(&tmp_tree);
		foreach(cycle_possible_acts, scan) {
			act_dep_t* action = scan->DATA;
			avl_ip_node_t* avl_node = NULL;
			int z = avl_ip_add(&tmp_tree, action->rank, &avl_node);
			if(z==false) avl_node->data = addchain(avl_node->data, action);
			else avl_node->data = addchain(NULL, action);
		}
		// Build the final list by concatenating the sorted lists
		avl_ip_foreach_inc(&tmp_tree, avl_node) {
			acts_ordered = append(avl_node->data, acts_ordered);
		}
		// Free the tree
		avl_ip_reset(&tmp_tree);
		#endif

		#if 0  // DEBUG print the ordered actions
			printf("POSSIBLE ORDERED ACTIONS\n");
			for(chain_list* scan=acts_ordered; scan!=NULL; scan=scan->NEXT) {
				act_dep_t* ptr_dep = scan->DATA;
				viewvexexprbound(ptr_dep->act->VEX_ATOM); printf(" = ");
				viewvexexprbound(ptr_dep->act->VEX_EXPR);
				printf("     RANK %d\n", ptr_dep->rank);
			}
		#endif

		// Select the actions to schedule in the current state
		chain_list* cycle_selected_acts = NULL;

		// Loop on the Actions to schedule.
		// Each iteration adds some Actions to the current state.
		do {

			// Pick the first Action (or group of) that fits in the resources.

			chain_list* chosen_actions = NULL;  // list of act_dep_t* elements
			foreach(acts_ordered, scanAct) {
				act_dep_t* test_act = scanAct->DATA;
				// It could already be selected, by wired conditions or timed dependencies.
				if(ChainList_IsDataHere(chosen_actions, test_act)==true) {
					continue;
				}

				// FIXME Handle generic Timed dependencies (with States != 0) !
				// And, when an action that is source of a timed dependency is selected,
				//   also check that there will be enough resources at the target state,
				//   because several timed dependencies can be present in one Action...
				// => the process must be able to add states after the current one...

				// Check that the needed resources fit into what is available.
				hwres_heap_t* test_res = NULL;
				if(sched->nores==false && test_act->needed_hwres!=NULL) {
					// Use a copy of the available resources.
					test_res = hwres_heap_dup(cycle_resources);
					// The resources listed in the Action itself
					int z = hwres_heap_sub_fromavail(test_res, test_act->needed_hwres, false, false);
					if(z!=0) {
						// Not enough resources, try the next Action
						hwres_heap_free(test_res);
						freechain(chosen_actions); chosen_actions = NULL;
						continue;
					}
				}
				chosen_actions = addchain(chosen_actions, test_act);

				// Handle the timed dependencies. Only simultaneous scheduling is handled.
				bool error_timed = false;
				avl_pi_foreach(&test_act->deps.timed, scanTimed) {
					if(scanTimed->data!=0) {
						printf("WARNING %s:%d : skipping non-zero timed dependency %d.\n", __FILE__, __LINE__, scanTimed->data);
						continue;
					}
					act_dep_t* otherAct = scanTimed->key;
					if(ChainList_IsDataHere(chosen_actions, otherAct)==true) continue;  // Paranoia ?

					// Check data dependencies
					bool is_schedulable = sched_check_schedulable_depbef(otherAct);
					if(is_schedulable==false) { error_timed = true; break; }

					// Take these resources into account
					if(sched->nores==false && otherAct->needed_hwres!=NULL) {
						if(test_res==NULL) test_res = hwres_heap_dup(cycle_resources);
						int z = hwres_heap_sub_fromavail(test_res, otherAct->needed_hwres, false, false);
						if(z!=0) { error_timed = true; break; }
					}

					// Add this action to the current bunch.
					chosen_actions = addchain(chosen_actions, otherAct);
				}  // Scan other Actions that are linked with a timed dependency

				if(error_timed==true) {
					// Not enough resources, try the next Action
					hwres_heap_free(test_res);
					freechain(chosen_actions); chosen_actions = NULL;
					continue;
				}

				// These selected interdependent actions are kept for the current State.

				// Update the resources used for the current State.
				if(sched->nores==false && test_res!=NULL) {
					hwres_heap_free(cycle_resources);
					cycle_resources = test_res;
				}
				break;

			}  // Find a group of interdependent Actions to add to the current State

			if(chosen_actions==NULL) break;  // No more Action was possible. Quit the loop.

			// Remove these Actions from the lists, add them in the current list
			foreach(chosen_actions, scanChosen) {
				act_dep_t* chosen_act = scanChosen->DATA;
				chosen_act->flag_sched = ACTSTATUS_SELECT;
				acts_ordered = ChainList_Remove(acts_ordered, chosen_act);
				cycle_selected_acts = addchain(cycle_selected_acts, chosen_act);
			}

			// Now that these Actions are selected, other actions could be scheduled too. Add them.

			// Check that the next actions have no serious dependency with their previous Actions
			// Example of dependencies :
			//   Act1 -> WaR -> Act2 : Both acts can be selected.
			//   Act1 -> RaW -> Act2 : Only Act1 can be selected.
			//   Act2 -> WaW -> Act2 : Only Act1 can be selected.
			// Note: the SCHED_DEP_WAW can remain when accessing array cells, when addresses are not known.
			//   This is why the two Actions can't be scheduled together and must remain in the input order.
			//   When applied to a register, here we assume that the optimization was done before.

			// FIXME handle generic timed dependencies (i.e. with delay != 0)

			foreach(chosen_actions, scanChosen) {
				act_dep_t* chosen_act = scanChosen->DATA;

				avl_pi_foreach(&chosen_act->deps.after, scanAfter) {
					act_dep_t* next_act = scanAfter->key;
					if(next_act->flag_sched != ACTSTATUS_LATER) continue;

					// Handle timed dependencies
					chain_list* group_acts = addchain(NULL, next_act);
					avl_pi_foreach(&next_act->deps.timed, scanTimed) {
						if(scanTimed->data!=0) printf("WARNING %s:%u : Non-zero timed dependencies are not handled.\n", __FILE__, __LINE__);
						act_dep_t* timed_act = scanTimed->key;
						if(timed_act->flag_sched != ACTSTATUS_LATER) continue;
						group_acts = addchain(group_acts, timed_act);
					}
					// If there are dependencies before, allow only with already scheduled actions.
					bool add_group = true;
					foreach(group_acts, scanGroupAct) {
						act_dep_t* group_act = scanGroupAct->DATA;
						bool is_schedulable = sched_check_schedulable_depbef(group_act);
						if(is_schedulable==false) { add_group=false; break; }
					}
					if(add_group==true) {
						// Keep this action
						foreach(group_acts, scanGroupAct) {
							act_dep_t* group_act = scanGroupAct->DATA;
							group_act->flag_sched = ACTSTATUS_AVAIL;
							acts_ordered = addchain(acts_ordered, group_act);
						}
					}
					freechain(group_acts);

				}  // Search the other actions that can be scheduled

			}  // List of chosen Actions

			freechain(chosen_actions);
			if(acts_ordered==NULL) break;  // Everything possible was scheduled on the current state.

		}while(1);  // Loop that selects the Actions for the current state

		// Error check
		if(cycle_selected_acts==NULL) {
			printf("INTERNAL ERROR %s:%d : No Action was selected for a state.\n", __FILE__, __LINE__);
			printf("%d states are already used.\n", sched->state_sched_nb);
			printf("The possible actions were :\n");
			foreach(cycle_possible_acts, scan) {
				act_dep_t* ptr_dep = scan->DATA;
				printf("Action  Flag %d, Rank %d, dependencies before %u, after %u, timed %u\n",
					ptr_dep->flag_sched, ptr_dep->rank,
					avl_pi_count(&ptr_dep->deps.before), avl_pi_count(&ptr_dep->deps.after),
					avl_pi_count(&ptr_dep->deps.timed)
				);
				if(avl_pi_count(&ptr_dep->deps.before)>0) {
					printf("  Deps Before :");
					avl_pi_foreach(&ptr_dep->deps.before, scanDep) {
						act_dep_t* other_act = scanDep->key;
						printf(" %p(%d,f=%u)", other_act, scanDep->data, other_act->flag_sched);
					}
					printf("\n");
				}
				if(avl_pi_count(&ptr_dep->deps.after)>0) {
					printf("  Deps After  :");
					avl_pi_foreach(&ptr_dep->deps.after, scanDep) {
						act_dep_t* other_act = scanDep->key;
						printf(" %p(%d,f=%u)", other_act, scanDep->data, other_act->flag_sched);
					}
					printf("\n");
				}
				if(avl_pi_count(&ptr_dep->deps.timed)>0) {
					printf("  Deps Timed  :");
					avl_pi_foreach(&ptr_dep->deps.timed, scanDep) {
						act_dep_t* other_act = scanDep->key;
						printf(" %p(%d,f=%u)", other_act, scanDep->data, other_act->flag_sched);
					}
					printf("\n");
				}
				printf("  (%p) ", ptr_dep);
				HierAct_DebugPrint(ptr_dep->action);
			}
			printf("Available hardware resources:\n");
			hwres_heap_print(all_resources, NULL);
			abort();
		}

		// Here we have the list of scheduled actions

		#if 0  // DEBUG print the scheduled actions
		printf("\nSCHEDULED ACTIONS IN STATE :\n\n");
		foreach(cycle_selected_acts, scan) {
			act_dep_t* ptr_dep = scan->DATA;
			HierAct_DebugPrint(ptr_dep->action);
		}
		#endif
		#if 0  // DEBUG print the unscheduled actions
		if(acts_ordered!=NULL) {
			printf("\nNON-SCHEDULED ACTIONS IN STATE :\n\n");
			foreach(acts_ordered, scan) {
				act_dep_t* ptr_dep = scan->DATA;
				HierAct_DebugPrint(ptr_dep->action);
			}
		}
		#endif

		// Add a state with this group of Actions
		state_dep_t* state_dep_new = state_sched_new_enqueue(sched);
		assert(state_dep_new!=NULL);
		// Fill the new state
		state_dep_new->acts = cycle_selected_acts;
		state_dep_new->acts_unscheduled = acts_ordered;
		if(sched->nores==false) {
			hwres_heap_remove_empty(cycle_resources);
			state_dep_new->hwres_remain = cycle_resources;
		}

		// Set the scheduled date in these Actions
		foreach(cycle_selected_acts, scan) {
			act_dep_t* ptr_act = scan->DATA;
			ptr_act->state_sched = state_dep_new;
			ptr_act->flag_sched = ACTSTATUS_SCHED;
		}

		// FIXME handle the timed dependencies !

		// Build the list of ACTs possible in the next state:
		// It is the remaining actions of this state,
		// plus the actions linked after the selected ones, if allowed by their dependencies.
		chain_list* next_possible_acts = dupchain(acts_ordered);
		// Get the Actions after the ones of this state
		foreach(cycle_selected_acts, scanCur) {
			act_dep_t* ptr_act = scanCur->DATA;
			avl_pi_foreach(&ptr_act->deps.after, scanAfter) {
				act_dep_t* next_act = scanAfter->key;
				if(next_act->flag_sched != ACTSTATUS_LATER) continue;
				// Handle timed dependencies
				chain_list* group_acts = addchain(NULL, next_act);
				avl_pi_foreach(&next_act->deps.timed, scanTimed) {
					if(scanTimed->data!=0) printf("WARNING %s:%u : Non-zero timed dependencies are not handled.\n", __FILE__, __LINE__);
					act_dep_t* timed_act = scanTimed->key;
					if(timed_act->flag_sched != ACTSTATUS_LATER) continue;
					group_acts = addchain(group_acts, timed_act);
				}
				// If there are dependencies before, allow only with already scheduled actions.
				bool add_group = true;
				foreach(group_acts, scanGroupAct) {
					act_dep_t* group_act = scanGroupAct->DATA;
					avl_pi_foreach(&group_act->deps.before, scanBefore) {
						act_dep_t* prev_act = scanBefore->key;
						if(prev_act->flag_sched!=ACTSTATUS_SCHED) {	add_group = false; break; }
					}
					if(add_group==false) break;
				}
				if(add_group==true) {
					// Keep this action
					foreach(group_acts, scanGroupAct) {
						act_dep_t* group_act = scanGroupAct->DATA;
						group_act->flag_sched = ACTSTATUS_AVAIL;
						next_possible_acts = addchain(next_possible_acts, group_act);
					}
				}
				freechain(group_acts);
			}  // Actions after
		}  // Actions scheduled in the previous State

		// Free the temporary lists
		freechain(cycle_possible_acts);
		cycle_possible_acts = next_possible_acts;

		// If no ACT is available, exit because the scheduling should be finished.
		if(cycle_possible_acts==NULL) break;

		// Next scheduled State
	}while(1);

	// Check that all Actions were actually scheduled
	chain_list* acts_not_scheduled = NULL;
	foreach(sched->act_dep_list, scanAct) {
		act_dep_t* ptr_act = scanAct->DATA;
		if(ptr_act->action==NULL) continue;
		if(ptr_act->state_sched!=NULL) continue;
		acts_not_scheduled = addchain(acts_not_scheduled, ptr_act);
	}
	if(acts_not_scheduled!=NULL) {
		printf("\nERROR In a basic block, some actions remain unscheduled:\n\n");
		unsigned acts_not_scheduled_nb = 0;
		acts_not_scheduled = reverse(acts_not_scheduled);  // To display in the right order
		foreach(acts_not_scheduled, scan) {
			act_dep_t* ptr_act = scan->DATA;

			HierAct_DebugPrint(ptr_act->action);
			printf("  ActDep: %p\n", ptr_act);

			unsigned sched_deps = 0;
			avl_pi_foreach(&ptr_act->deps.before, scanBefore) {
				act_dep_t* prev_act = scanBefore->key;
				if(prev_act->flag_sched == ACTSTATUS_SCHED) sched_deps++;
			}

			printf("  Rank %u / Status %u / DepBef %u / DepSched %u / DepTimed %u / Flags 0x%x\n",
				ptr_act->rank, ptr_act->flag_sched,
				avl_pi_count(&ptr_act->deps.before), sched_deps,
				avl_pi_count(&ptr_act->deps.timed),
				ptr_act->action->flags
			);

			if(avl_pi_isempty(&ptr_act->deps.before)==false) {
				printf("  DepBef:");
				avl_pi_foreach(&ptr_act->deps.before, scanDepBef) {
					printf(" %p=0x%x", scanDepBef->key, scanDepBef->data);
				}
				printf("\n");
			}
			printf("\n");

			acts_not_scheduled_nb ++;
		}
		printf("%u actions remain unscheduled.\n", acts_not_scheduled_nb);
		printf("%u states are already created.\n", sched->state_sched_nb);
		printf("There are %u actions in the BB.\n", ChainList_Count(sched->act_dep_list));
		printf("\n");
		// Note: the list acts_not_scheduled is intentionally not freed to do tests
		fflush(NULL);
		abort();
	}

	// DEBUG
	//printf("One basic block scheduled in %d States.\n", sched->state_sched_nb);

	return 0;
}


// The new basic block is built and replaces the previous one.
// return 0 if OK, <0 if error, >0 is the number of changes (Actions not scheduled in the same State)
static int sched_apply_bb(schedule_t* sched, implem_t* Implem) {
	hier_node_t* FirstState = NULL;
	hier_node_t* LastState = NULL;

	unsigned count = 0;

	foreach(sched->state_sched_first, state_dep) {

		// Create a new node
		hier_node_t* newState = Hier_Node_NewType(HIERARCHY_STATE);
		// Link the new node
		if(FirstState==NULL) FirstState = newState;
		else Hier_Nodes_Link(LastState, newState);
		LastState = newState;

		chain_list* list_acts = NULL;

		// Transfer all ACTs to this State
		foreach(state_dep->acts, scanAct) {
			act_dep_t* ptr_dep = scanAct->DATA;
			hier_action_t* action = ptr_dep->action;
			Hier_Action_Unlink(action);
			Hier_Action_Link(newState, action);
			// Add to the list of Action, to get inline Actions
			list_acts = addchain(list_acts, action);
			// Check if the Action has changed State number in the BB
			if(ptr_dep->state_orig->date != state_dep->date) count ++;
		}

		avl_pp_tree_t tree_act_old2new;
		avl_pp_init(&tree_act_old2new);

		// Add a copy of the needed inline Actions
		list_acts = Hier_ListActions_AddInline(list_acts);
		foreach(list_acts, scanAct) {
			hier_action_t* action = scanAct->DATA;
			if(HierAct_FlagCheck(action, HIER_ACT_INL)==false) continue;
			hier_action_t* newaction = Hier_Action_Dup(action);
			// Explicitely duplicate the list of inlined Actions
			newaction->acts_inline = dupchain(action->acts_inline);
			// Link this Action to the current State
			Hier_Action_Link(newState, newaction);
			avl_pp_add_overwrite(&tree_act_old2new, action, newaction);
		}
		// Update the lists of inline Actions
		foreach(list_acts, scanAct) {
			hier_action_t* action = scanAct->DATA;
			foreach(action->acts_inline, scanInl) {
				hier_action_t* inlaction = scanInl->DATA;
				hier_action_t* newaction = NULL;
				avl_pp_find_data(&tree_act_old2new, inlaction, (void**)&newaction);
				assert(newaction!=NULL);
				scanInl->DATA = newaction;
			}
		}

		// Clean
		freechain(list_acts);
		avl_pp_reset(&tree_act_old2new);

	}  // Scan each scheduled State

	// Check : ensure no Action remains in the original nodes.
	#ifndef NDEBUG
	foreach(sched->node_bb->NODE.BB.body, node) {
		hier_state_t* state_data = &node->NODE.STATE;
		assert(state_data->actions==NULL);
	}
	#endif

	// Add the new nodes to the hierarchy graph
	Hier_Lists_AddLevel(Implem->H, FirstState);

	hier_node_t* old_body = sched->node_bb->NODE.BB.body;
	Hier_ReplaceChild(sched->node_bb, old_body, FirstState);

	// Free the old nodes
	Hier_Lists_RemoveLevel(Implem->H, old_body);
	Hier_FreeNodeLevel(old_body);

	// Free the data structures related to the old States (keep what is related to the Actions)
	while(sched->state_dep_first!=NULL) {
		state_dep_t* state_next = sched->state_dep_first->NEXT;
		state_dep_free(sched->state_dep_first);
		sched->state_dep_first = state_next;
	}

	// Return the number of changes about scheduling of Actions
	return count;
}


// Return the number of changes about scheduling of Actions
int Schedule(implem_t* Implem, int algo) {

	// Default
	if(algo==0) {
		algo = SCHED_ALGO_LIST;
	}

	bool nores = false;
	if(algo==SCHED_ALGO_ASAP) nores = true;

	//dbgprintf("Launching scheduling with algo %i...\n", algo);

	// If needed, ensure we have the list of available resources
	if(nores==false) {
		// Always rebuild the available resources
		Resources_Available_Detect(Implem);
		if(Implem->SCHED.hwres_available==NULL) {
			printf("Error : Can't schedule because the available resources are unknown.\n");
			return -1;
		}
	}

	// Open dump file if asked
	if(depend_dump_filename!=NULL) {
		TestDo(depend_dump_file, fclose);
		depend_dump_file = fopen(depend_dump_filename, "wb");
	}

	// Free the scheduling data
	if(Implem->env.globals->verbose.level>VERBOSE_SILENT) {
		printf("Cleaning previous scheduling data...\n");
	}
	Hier_ClearSched(Implem->H);

	// Build the basic blocks
	if(Implem->env.globals->verbose.level>VERBOSE_SILENT) {
		printf("Building scheduler structures...\n");
	}

	// The number of scheduling changes done
	unsigned count = 0;

	// Create the scheduling structure for each basic block
	avl_p_foreach(&Implem->H->NODES[HIERARCHY_BB], scan) {
		hier_node_t* node_bb = scan->data;
		// This is the structure containing the data for scheduling, including the dependency links
		schedule_t* sched = sched_new();
		sched->algo    = algo;
		sched->nores   = nores;
		sched->node_bb = node_bb;
		node_bb->NODE.BB.sched.data = sched;
	}

	// Initialize : build the dependency links and the register usage.
	avl_p_foreach(&Implem->H->NODES[HIERARCHY_BB], scan) {
		hier_node_t* nodeBB = scan->data;
		schedule_t* sched = nodeBB->NODE.BB.sched.data;
		sched_init_bb(sched);
		// Dump dependencies if asked
		if(depend_dump_file!=NULL) {
			sched_depend_dump_bb(depend_dump_file, sched);
		}
	}

	// Launch the scheduling of all basic blocks
	if(Implem->env.globals->verbose.level>VERBOSE_SILENT) {
		printf("Launching scheduling...\n");
	}

	avl_p_foreach(&Implem->H->NODES[HIERARCHY_BB], scan) {
		hier_node_t* nodeBB = scan->data;
		schedule_t* sched = nodeBB->NODE.BB.sched.data;

		if(sched->act_dep_list==NULL) {
			// Scheduling is not applicable on this BB.
			continue;
		}
		if(sched->state_nb_orig<2) {
			// Scheduling is not necessary on this BB.
			continue;
		}

		// Build the ranks
		sched_rank_bb(sched);

		// Generate the new scheduling
		sched_process_bb(sched, Implem->SCHED.hwres_available);

		// Apply the new scheduling
		if(sched->state_sched_first!=NULL) {
			count += sched_apply_bb(sched, Implem);
		}

	}  // Iterate on all basic blocks

	// Close dump file
	TestDoNull(depend_dump_file, fclose);

	// Update the needed operators
	if(nores==true) {
		if(Implem->env.globals->verbose.level>VERBOSE_SILENT) {
			printf("Update needed operators...\n");
		}
		Map_Netlist_CompleteComponents(Implem);
	}

	if(Implem->env.globals->verbose.level>VERBOSE_SILENT) {
		printf("Scheduling done.\n");
	}

	// Set the status flag
	Implem->synth_state.schedule = 1;

	// Return the number of changes about scheduling of Actions
	return count;
}

int Schedule_OneBB_OnlyNbClk(hier_node_t* node, hwres_heap_t* avail_resources) {

	// Free the scheduling data
	assert(node->TYPE==HIERARCHY_BB);
	Hier_NodeBB_ClearSched(node);

	// Create the scheduling structure
	schedule_t* sched = sched_new();
	sched->node_bb = node;
	node->NODE.BB.sched.data = sched;

	// Initialize : build the dependency links
	sched_init_bb(sched);

	int nb_clk = -1;
	if(sched->state_nb_orig<=1) {
		nb_clk = sched->state_nb_orig;
	}
	else {

		// Build the ranks
		sched_rank_bb(sched);

		// Generate the new scheduling
		sched_process_bb(sched, avail_resources);

		// Get the result
		nb_clk = sched->state_sched_nb;
		assert(nb_clk>0);
	}

	// Clean
	Hier_NodeBB_ClearSched(node);

	return nb_clk;
}



//================================================
// Freedom degrees
//================================================

// Display the missing resources.
// This shows which operators could be added to the architecture to improve the scheduling.

enum {
	RESOURCE_FILTER_HAVE_MEM_R,
	RESOURCE_FILTER_NO_MEM_R,
	RESOURCE_FILTER_HAVE_MEM_W,
	RESOURCE_FILTER_NO_MEM_W,
};

static int Sched_ViewMissingRes(implem_t* Implem, chain_list* filter) {
	if(Implem->SCHED.hwres_available==NULL) {
		printf("Error : The set of available resources is not available.\n");
		return -1;
	}

	avl_p_foreach(&Implem->H->NODES[HIERARCHY_BB], scan) {
		hier_node_t* nodeBB = scan->data;
		schedule_t* sched = nodeBB->NODE.BB.sched.data;
		foreach(sched->state_sched_first, state_dep) {
			if(state_dep->acts_unscheduled==NULL) continue;

			printf("New State with unscheduled actions.\n");

			foreach(state_dep->acts, scanAct) {
				act_dep_t* dep_act = scanAct->DATA;
				HierAct_Print(dep_act->action);
			}

			//printf("DEBUG STATE REMAIN RESOURCES :\n");
			//hwres_heap_print(state_dep->hwres_remain);

			printf("Unscheduled actions:\n");

			foreach(state_dep->acts_unscheduled, scanAct) {
				act_dep_t* dep_act = scanAct->DATA;
				// Display the expression
				#if 1
				HierAct_Print(dep_act->action);
				#endif
				// Display the resources.
				#if 1
				printf("DEBUG MISSING RESOURCES :\n");
				if(dep_act->needed_hwres!=NULL) {
					hwres_heap_t* tmp_heap = hwres_heap_dup(state_dep->hwres_remain);
					hwres_heap_t* action_resources = hwres_heap_dup(dep_act->needed_hwres);
					hwres_heap_sub_fromavail(tmp_heap, action_resources, true, true);
					hwres_heap_remove_empty(action_resources);
					hwres_heap_print(action_resources, NULL);
					hwres_heap_free(action_resources);
					hwres_heap_free(tmp_heap);
				}
				#endif
			}  // Scan the unscheduled Actions

		}  // Scan the states
	}  // Scan all basic blocks

	return 0;
}

chain_list* Sched_FreedomDegrees_Build_Ops(implem_t* Implem) {
	chain_list* list_freedom_degrees = NULL;

	// Check if the user forbids some freedom degrees
	bool fd_skip_memr = ChainNum_IsDataHere(Implem->CORE.freedom_degrees_skipped_ops, FD_SKIP_MEM_R);
	bool fd_skip_memw = ChainNum_IsDataHere(Implem->CORE.freedom_degrees_skipped_ops, FD_SKIP_MEM_W);
	//printf("DEBUG skip freedom degrees : memR %d, memW %d\n", fd_skip_memr, fd_skip_memw);

	// Scan all actions to see what could be added
	avl_p_foreach(&Implem->H->NODES[HIERARCHY_BB], scanBB) {
		hier_node_t* nodeBB = scanBB->data;
		schedule_t* sched = nodeBB->NODE.BB.sched.data;
		foreach(sched->state_sched_first, state_dep) {
			if(state_dep->acts_unscheduled==NULL) continue;

			foreach(state_dep->acts_unscheduled, scanAct) {
				act_dep_t* dep_act = scanAct->DATA;
				if(dep_act->needed_hwres==NULL) continue;

				// Build the missing resources for this Action
				hwres_heap_t* action_resources = hwres_heap_dup(dep_act->needed_hwres);
				hwres_heap_remove_vex(action_resources);
				hwres_heap_sub_fromavail_forFD(state_dep->hwres_remain, action_resources);
				hwres_heap_remove_empty(action_resources);

				// Here, the heap 'action_resources' contains the missing resources.
				// It should not be empty.
				if(hwres_heap_isempty(action_resources)==true) {
					printf("INTERNAL WARNING %s:%u : A heap of missing resources is empty\n", __FILE__, __LINE__);
					hwres_heap_free(action_resources);
					continue;
				}

				// Check if this kind of freedom degree is forbidden by the user
				if(fd_skip_memr==true) {
					if(hwres_heap_isthere_memr(action_resources)==true) {
						hwres_heap_free(action_resources);
						continue;
					}
				}
				if(fd_skip_memw==true) {
					if(hwres_heap_isthere_memw(action_resources)==true) {
						hwres_heap_free(action_resources);
						continue;
					}
				}

				// Remove operators already in excess
				if(core_dupimplem_inxs==true) {
					avl_pp_foreach(&action_resources->type_ops, scanOp) {
						hw_res_t* hwres = scanOp->data;
						avl_pi_node_t* pi_node = NULL;
						bool b = avl_pi_find(&Implem->CORE.dupimplem_inxs, hwres->name, &pi_node);
						if(b==false) continue;
						if(pi_node->data > (int)hwres->number) hwres->number = 0;
						else hwres->number -= pi_node->data;
					}
					hwres_heap_remove_empty(action_resources);
					if(hwres_heap_isempty(action_resources)==true) {
						hwres_heap_free(action_resources);
						continue;
					}
				}

				// Scan the list of freedom degrees.
				// Create a new one only if it does not already exist.

				bool found = false;
				foreach(list_freedom_degrees, scan_degree) {
					freedom_degree_t* degree = scan_degree->DATA;

					int z = hwres_heap_cmp_forFD(action_resources, degree->target.addops.heap);
					if(z!=0) continue;

					found = true;
					break;
				}  // Scan the list of freedom degrees

				if(found==true) {
					hwres_heap_free(action_resources);
					continue;
				}

				// Create a new freedom degree
				freedom_degree_t* new_degree = freedom_degree_new();
				new_degree->Implem = Implem;

				new_degree->type = FD_ADD_OPS;
				new_degree->target.addops.heap = action_resources;
				new_degree->time_gain = 1;
				list_freedom_degrees = addchain(list_freedom_degrees, new_degree);

				#if 0  // Debug: Display the freedom degrees found
				printf("DEBUG %s:%u : New freedom degree ADDOPS\n", __FILE__, __LINE__);
				hwres_heap_print(new_degree->target.addops.heap, NULL);

				{
					// Compute the total available resources for this State
					hwres_heap_t* avail_resources = hwres_heap_dup(state_dep->hwres_remain);
					hwres_heap_add_heap_avail_forFD(avail_resources, new_degree->target.addops.heap);

					bool b = hwres_heap_check_avail_forFD(avail_resources, dep_act->needed_hwres);
					if(b==false) printf("INTERNAL ERROR %s:%u : Inclusion error:\n", __FILE__, __LINE__);

					hwres_heap_free(avail_resources);
				}
				#endif

			}  // Scan the unscheduled Actions

		}  // Scan the states
	}  // Scan all basic blocks

	// Scan the freedom degrees and remove the ones that are flagged impossible.
	chain_list* new_list = NULL;
	foreach(list_freedom_degrees, ScanDegreeRef) {
		freedom_degree_t* degree = ScanDegreeRef->DATA;
		// Scan the impossible degrees
		int must_skip = 0;
		foreach(Implem->CORE.skipfd_addops, ScanSkip) {
			hwres_heap_t* skip_heap = ScanSkip->DATA;
			bool b = hwres_heap_check_need_included_forFD(skip_heap, degree->target.addops.heap);
			if(b==true) { must_skip = 1; break; }
		}
		if(must_skip!=0) {
			freedom_degree_free(degree);
			continue;
		}
		// Keep the degree
		new_list = addchain(new_list, degree);
	}
	freechain(list_freedom_degrees);
	list_freedom_degrees = new_list;

	// For each possible set of resources,
	// The possible time gain is set as the number of States where an action
	// can benefit it. Actions are considered only one time per basic block.
	foreach(list_freedom_degrees, ScanDegreeRef) {
		freedom_degree_t* ref_degree = ScanDegreeRef->DATA;

		double weight = 0;

		avl_p_foreach(&Implem->H->NODES[HIERARCHY_BB], scanBB) {
			hier_node_t* nodeBB = scanBB->data;
			schedule_t* sched = nodeBB->NODE.BB.sched.data;
			if(sched->state_dep_first==NULL) continue;

			// Get the hierarchy factor
			double factor = sched->node_bb->timing.factor;

			// Initialize the flags of the Actions.
			foreach(sched->act_dep_list, scanAct) {
				act_dep_t* ptr_dep = scanAct->DATA;
				ptr_dep->flag_sched = 0;
			}

			// Scan the States
			foreach(sched->state_sched_first, state_dep) {
				if(state_dep->acts_unscheduled==NULL) continue;

				// Compute the total available resources for this State
				hwres_heap_t* avail_resources = hwres_heap_dup(state_dep->hwres_remain);
				hwres_heap_add_heap_avail_forFD(avail_resources, ref_degree->target.addops.heap);

				// Scan the Actions. Finding one action is enough.
				unsigned found_nb = 0;
				foreach(state_dep->acts_unscheduled, scanAct) {
					act_dep_t* dep_act = scanAct->DATA;
					if(dep_act->flag_sched!=0) continue;
					// Check the resources...
					bool b = hwres_heap_check_avail_forFD(avail_resources, dep_act->needed_hwres);
					if(b==false) continue;
					// Action found. Flag it.
					dep_act->flag_sched = 1;
					found_nb ++;
					// Handle only one Action per State.
					break;
				}  // Scan the actions

				hwres_heap_free(avail_resources);

				unsigned nb1 = ChainList_Count(state_dep->acts);
				unsigned nb2 = ChainList_Count(state_dep->acts_unscheduled);

				weight += factor * (double)found_nb / GetMax(nb1, nb2);

			}  // Scan the states
		}  // Scan the basic blocks

		// Assign the new time weight to the freedom degree
		ref_degree->time_gain = weight;

		// Get the hardware cost
		ref_degree->resource_cost = FD_HwCost_HeapRes(Implem, ref_degree->target.addops.heap);

	}  // Scan the freedomp degrees


	#if 0  // Display the freedom degrees found
	foreach(list_freedom_degrees, scan_degree) {
		freedom_degree_t* degree = scan_degree->DATA;
		printf("--> New freedom degree : time %g\n", degree->time_gain);
		hwres_heap_print(degree->target.addops);
	}
	#endif

	return list_freedom_degrees;
}

chain_list* Sched_FreedomDegrees_Build_ScalarReplace(implem_t* Implem) {
	chain_list* list_degrees = NULL;

	// Scan the memory components
	avl_pp_foreach(&Implem->netlist.top->children, scanComp) {
		netlist_comp_t* comp = scanComp->data;
		if(comp->model!=NETLIST_COMP_MEMORY) continue;

		// Skip the freedom degrees already flagged as impossible
		if(ChainList_IsDataHere(Implem->CORE.skipfd_scalar, comp->name)==true) continue;

		netlist_memory_t* mem_data = comp->data;
		if(mem_data->direct_en==true) continue;

		if(mem_data->ports_wa_nb>0) {
			bool b = ChainNum_IsDataHere(Implem->CORE.freedom_degrees_skipped_ops, FD_SKIP_INLINE_RAM);
			if(b==true) continue;
			// There should be no initialization
			if(mem_data->init_vex!=NULL) {
				printf("WARNING %s:%d : Skipping array '%s' because it has an initialization.\n", __FILE__, __LINE__, comp->name);
				continue;
			}
		}
		else {
			bool b = ChainNum_IsDataHere(Implem->CORE.freedom_degrees_skipped_ops, FD_SKIP_INLINE_ROM);
			if(b==true) continue;
			// There should be an initialization
			if(mem_data->init_vex==NULL) {
				printf("WARNING %s:%d : Skipping ROM '%s' because it has no initialization.\n", __FILE__, __LINE__, comp->name);
				continue;
			}
		}

		int z = ArrayOccur_LittAddr(Implem, comp->name, NULL);
		if(z!=0) continue;

		// Add a freedom degree
		freedom_degree_t* new_degree = freedom_degree_new();
		new_degree->Implem = Implem;

		new_degree->type = FD_MEM_INLINE;
		new_degree->target.array.name = comp->name;
		list_degrees = addchain(list_degrees, new_degree);

		// To get an approximate time gain
		double time_weight = 0;

		// Scan all basic blocks to see what could be added
		avl_p_foreach(&Implem->H->NODES[HIERARCHY_BB], scanBB) {
			hier_node_t* nodeBB = scanBB->data;
			schedule_t* sched = nodeBB->NODE.BB.sched.data;
			// Scan all States
			foreach(sched->state_sched_first, state_dep) {
				if(state_dep->acts_unscheduled==NULL) continue;

				unsigned found_nb = 0;

				foreach(state_dep->acts_unscheduled, scanAct) {
					act_dep_t* dep_act = scanAct->DATA;
					if(dep_act->needed_hwres==NULL) continue;
					// Approximation : do not compute the missing resources
					// Only use what is required by the action
					hwres_heap_t* resources = dep_act->needed_hwres;
					hw_res_t* elt = hwres_heap_mem_find(resources, comp->name);
					if(elt==NULL) continue;
					found_nb ++;
				}  // Scan the unscheduled Actions

				// Handle the execution factor of this State

				unsigned nb1 = ChainList_Count(state_dep->acts);
				unsigned nb2 = ChainList_Count(state_dep->acts_unscheduled);

				time_weight += sched->node_bb->timing.factor * (double)found_nb / GetMax(nb1, nb2);

			}  // Scan the states
		}  // Scan all basic blocks

		// Set the possible time gain for this freedom degree
		new_degree->time_gain = time_weight;

		// Set an estimation of the cost
		new_degree->resource_cost = Techno_Netlist_Eval_Mem2Reg(Implem, comp->name);
	}  // Memory components

	return list_degrees;
}

chain_list* Sched_FreedomDegrees_Build_PPDirect(implem_t* Implem) {
	chain_list* list_degrees = NULL;

	// Scan the PP components
	avl_pp_foreach(&Implem->netlist.top->children, scanComp) {
		netlist_comp_t* comp = scanComp->data;
		if(comp->model!=NETLIST_COMP_PINGPONG_IN && comp->model!=NETLIST_COMP_PINGPONG_OUT) continue;

		// Skip the freedom degrees already flagged as impossible
		if(ChainList_IsDataHere(Implem->CORE.skipfd_scalar, comp->name)==true) continue;

		netlist_pingpong_t* pp_data = comp->data;
		if(pp_data->is_direct==true) continue;

		int z = ArrayOccur_LittAddr(Implem, comp->name, NULL);
		if(z!=0) continue;

		// Add a freedom degree
		freedom_degree_t* new_degree = freedom_degree_new();
		new_degree->Implem = Implem;

		new_degree->type = FD_PP_DIRECT;
		new_degree->target.array.name = comp->name;
		list_degrees = addchain(list_degrees, new_degree);

		// To get an approximate time gain
		double time_weight = 0;

		// Scan all basic blocks to see what could be added
		avl_p_foreach(&Implem->H->NODES[HIERARCHY_BB], scanBB) {
			hier_node_t* nodeBB = scanBB->data;
			schedule_t* sched = nodeBB->NODE.BB.sched.data;
			// Scan all States
			foreach(sched->state_sched_first, state_dep) {
				if(state_dep->acts_unscheduled==NULL) continue;

				unsigned found_nb = 0;

				foreach(state_dep->acts_unscheduled, scanAct) {
					act_dep_t* dep_act = scanAct->DATA;
					if(dep_act->needed_hwres==NULL) continue;
					// Approximation : do not compute the missing resources
					// Only use what is required by the action
					hw_res_t* elt = hwres_heap_mem_find(dep_act->needed_hwres, comp->name);
					if(elt==NULL) continue;
					found_nb ++;
				}  // Scan the unscheduled Actions

				// Handle the execution factor of this State

				unsigned nb1 = ChainList_Count(state_dep->acts);
				unsigned nb2 = ChainList_Count(state_dep->acts_unscheduled);

				time_weight += sched->node_bb->timing.factor * (double)found_nb / GetMax(nb1, nb2);

			}  // Scan the states
		}  // Scan all basic blocks

		// Set the possible time gain for this freedom degree
		new_degree->time_gain = time_weight;

		// Set an estimation of the cost
		new_degree->resource_cost = Techno_Netlist_Eval_Mem2Reg(Implem, comp->name);
	}  // Memory components

	return list_degrees;
}


// This is a utility function for other freedom degree types
// To get the latency after scheduling
// Note: the Implem is not used
double Sched_GetScheduledLatency_ForFD(implem_t* Implem, hier_node_t* nodeBB, hwres_heap_t* avail_resources) {

	// Free the scheduling data
	Hier_NodeBB_ClearSched(nodeBB);

	// Create the scheduling structure
	schedule_t* sched = sched_new();
	sched->node_bb = nodeBB;
	nodeBB->NODE.BB.sched.data = sched;

	// Initialize : build the dependency links and the register usage.
	sched_init_bb(sched);

	if(sched->act_dep_list==NULL) {
		// Scheduling is not applicable on this BB.
		return -1;
	}
	if(sched->state_nb_orig<2) {
		// Scheduling is not necessary on this BB.
		return nodeBB->timing.once.avg;
	}

	// Build the ranks
	sched_rank_bb(sched);

	// Launch the scheduling
	sched_process_bb(sched, avail_resources);

	// Compute the new latency
	double count = 0;
	foreach(sched->state_sched_first, state_dep) {
		double max = 0;
		foreach(state_dep->acts, scanAct) {
			act_dep_t* dep = scanAct->DATA;
			if(dep->action->nb_clk > max) max = dep->action->nb_clk;
		}
		count += max;
	}

	return count;
}



//================================================
// Command interpreter
//================================================

#include <command.h>

extern int Schedule_Command(implem_t* Implem, command_t* cmd_data) {
	const char* cmd = Command_PopParam(cmd_data);
	if(cmd==NULL) {
		printf("Error 'sched' : No command received. Try 'help'.\n");
		return -1;
	}

	if(strcmp(cmd, "help")==0) {
		printf(
			"Possible commands :\n"
			"  help             Display this help.\n"
			"  schedule         Compute and apply a new scheduling.\n"
			"                   Internal data is kept for later exploration.\n"
			"  resources-build  Build the heap of available resources for the scheduler.\n"
			"  resources-view   View the heap of available resources for the scheduler.\n"
			"  view-miss-hw     Explore the needed resources to improve the scheduling.\n"
			"Set parameters :\n"
			"  depend-dump <options>\n"
			"    file <file>    Disable data dependencies dump in a file.\n"
			"    no | off       Disable data dependencies dump [default].\n"
			"  use-vexalloc-share [<bool>]\n"
			"                   Enable usage of expensive VexAlloc sharing.\n"
			"                   Init=no, default=yes\n"
			"  use-expensive-compare [<bool>]\n"
			"                   Enable usage of expensive expression comparison.\n"
			"                   Init=no, default=yes\n"
		);
		return 0;
	}

	if(strcmp(cmd, "depend-dump")==0) {
		const char* param = Command_PopParam(cmd_data);
		if(param==NULL) {
			printf("Error 'sched', command '%s' : Missing parameter. Try 'help'.\n", cmd);
			return -1;
		}
		if(strcmp(param, "file")==0) {
			const char* filename = Command_PopParam(cmd_data);
			if(filename==NULL) {
				printf("Error 'sched', command '%s %s' : Missing parameter. Try 'help'.\n", cmd, param);
				return -1;
			}
			TestFree(depend_dump_filename);
			depend_dump_filename = strdup(filename);
		}
		else if(strcmp(param, "off")==0 || strcmp(param, "no")==0 || strcmp(param, "none")==0) {
			TestFreeNull(depend_dump_filename);
		}
		else {
			printf("Error 'sched', command '%s' : Unknown parameter '%s'. Try 'help'.\n", cmd, param);
			return -1;
		}
		return 0;
	}

	if(strcmp(cmd, "use-vexalloc-share")==0) {
		int z = Command_PopParam_YesNoDef_Verbose(cmd_data, true, cmd);
		if(z<0) return -1;
		sched_use_vexalloc_share = z;
		return 0;
	}

	if(strcmp(cmd, "use-expensive-compare")==0) {
		int z = Command_PopParam_YesNoDef_Verbose(cmd_data, true, cmd);
		if(z<0) return -1;
		hvex_expensive_cmp_en = z;
		return 0;
	}

	// The other commands require that the hierarchy exists

	bool error_found = false;
	if     (Implem==NULL)    error_found = true;
	else if(Implem->H==NULL) error_found = true;

	if(error_found==true) {
		printf("Error 'sched' : Command '%s' requires a design (or unknown command).\n", cmd);
		return -1;
	}

	// Processes

	else if(strcmp(cmd, "schedule")==0) {
		int algo = SCHED_ALGO_LIST;

		// Get algorithm
		do {
			const char* param = Command_PopParam(cmd_data);
			if(param==NULL) break;

			if(strcmp(param, "-asap")==0) {
				algo = SCHED_ALGO_ASAP;
			}
			else {
				printf("Error 'sched' : Command '%s' : Unknown parameter '%s'\n", cmd, param);
				return -1;
			}

		} while(1);

		// Only to know the execution time
		struct timespec oldtime;
		TimeSpec_GetReal(&oldtime);

		int z = Schedule(Implem, algo);

		// Display the execution time
		double diff = TimeSpec_DiffCurrReal_Double(&oldtime);
		if(Implem->env.globals->verbose.level>VERBOSE_SILENT)
			printf("Scheduling done in %g seconds.\n", diff);

		if(z < 0) return z;
		return 0;
	}

	else if(strcmp(cmd, "resources-build")==0) {
		return Resources_Available_Detect(Implem);
	}
	else if(strcmp(cmd, "resources-view")==0) {
		indent_t indent = Indent_Make(' ', 2, 0);
		Resources_Available_View(Implem, &indent);
	}

	else if(strcmp(cmd, "view-miss-hw")==0) {
		Sched_ViewMissingRes(Implem, NULL);
	}

	else {
		printf("Error 'sched' : Unknown command '%s'.\n", cmd);
		return -1;
	}

	return 0;
}


