
/* printfm functions: lots of calls to printf() in one
 *
 * Note: This is sort of re-implements printf().
 * Ideally, we would like to call vfprintf() iteratively until the format NULL is reached.
 * However, the man page for stdargs says that the argument pointer is undefined
 * after a call to a function that uses va_arg().
 *
 * Indeed on most platforms the argument pointer passed by value,
 * so there is no way to know what the called function did with it.
 */

#include <ctype.h>
#include <wchar.h>
#include <errno.h>

#include "printfm.h"


#if _BSD_SOURCE || _SVID_SOURCE

// Use the stdio *_unlocked functions for better efficiency
// FIXME Simply define a macro to add the suffix _unlocked when possible
int fprintfm(FILE* F, char* fmt, ...) {
	va_list ap;
	va_start(ap, fmt);

	int written_bytes = 0;

	flockfile(F);

	while(fmt!=NULL) {

		// Process the current format string
		do {

			// Search the '%' character
			char c = *(fmt++);
			if(c==0) break;
			if(c!='%') {
				int z = fputc_unlocked(c, F);
				if(z==EOF) { written_bytes = -1; goto CLEANUP; }
				written_bytes ++;
				continue;
			}

			c = *(fmt++);
			if(c==0) break;
			// A little check to avoid the unuseful parsing after
			if(c=='%') {
				int z = fputc_unlocked('%', F);
				if(z==EOF) { written_bytes = -1; goto CLEANUP; }
				written_bytes ++;
				continue;
			}

			// Here we are at the beginning of a format specification
			// This buffer will contain the entire format, including the '%'
			char fmt_loc[32];
			unsigned fmt_loc_nb = 0;
			fmt_loc[fmt_loc_nb++] = '%';

			// Buffer where data is written with sprintf() before being written in the destination file
			char buf[128];

			// A local utility function
			int str_write(char* str) {
				for(unsigned i=0; ; i++) {
					char c = str[i];
					if(c==0) return i;
					int z = fputc_unlocked(c, F);
					if(z==EOF) return -1;
				}
			}
			int wstr_write(wint_t* wstr) {
				int nbytes = 0;
				for(unsigned i=0; ; i++) {
					wint_t wc = wstr[i];
					if(wc==L'\0') return i;
					wcrtomb(buf, wc, NULL);
					int z = str_write(buf);
					if(z<0) return -1;
					nbytes += z;
				}
			}

			// The return value of fprintf()
			int printf_ret = 0;

			// The flag character
			if(c=='#' || c=='0' || c=='-' || c==' ' || c=='+' || c=='\'' || c=='I') {
				fmt_loc[fmt_loc_nb++] = c;
				c = *(fmt++);
				if(c==0) break;
			}

			// The field width
			while(isdigit(c)!=0) {
				fmt_loc[fmt_loc_nb++] = c;
				c = *(fmt++);
				if(c==0) break;
			}

			// The precision
			if(c=='.') {
				fmt_loc[fmt_loc_nb++] = c;
				do {
					c = *(fmt++);
					if(c==0) break;
					if(isdigit(c)==0) break;
					fmt_loc[fmt_loc_nb++] = c;
				}while(1);
			}

			// The length modifier
			char length_mod1 = 0;
			char length_mod2 = 0;
			if(c=='h' || c=='l' || c=='L' || c=='q' || c=='j' || c=='z' || c=='t') {
				length_mod1 = c;
				fmt_loc[fmt_loc_nb++] = c;
				c = *(fmt++);
				if(c==0) break;
				if( (length_mod1=='h' && c=='h') || (length_mod1=='l' && c=='l') ) {
					length_mod2 = c;
					fmt_loc[fmt_loc_nb++] = c;
					c = *(fmt++);
					if(c==0) break;
				}
			}

			// The conversion specifier
			// Integers
			if(c=='d' || c=='i' || c=='o' || c=='u' || c=='x' || c=='X') {
				fmt_loc[fmt_loc_nb++] = c;
				fmt_loc[fmt_loc_nb++] = 0;
				if(length_mod1=='l') {
					if(length_mod2=='l') {
						long long int val = va_arg(ap, long long int);
						sprintf(buf, fmt_loc, val);
						printf_ret = str_write(buf);
					}
					else {
						long int val = va_arg(ap, long int);
						sprintf(buf, fmt_loc, val);
						printf_ret = str_write(buf);
					}
				}
				else {
					int val = va_arg(ap, int);
					printf_ret = fprintf(F, fmt_loc, val);
				}
			}
			// Floating-point
			else if(c=='e' || c=='E' || c=='f' || c=='F' || c=='g' || c=='G'|| c=='a' || c=='A') {
				fmt_loc[fmt_loc_nb++] = c;
				fmt_loc[fmt_loc_nb++] = 0;
				if(length_mod1=='L') {
					long double val = va_arg(ap, long double);
					sprintf(buf, fmt_loc, val);
					printf_ret = str_write(buf);
				}
				else {
					double val = va_arg(ap, double);
					sprintf(buf, fmt_loc, val);
					printf_ret = str_write(buf);
				}
			}
			// Character
			else if(c=='c') {
				fmt_loc[fmt_loc_nb++] = c;
				fmt_loc[fmt_loc_nb++] = 0;
				if(length_mod1=='l') {
					wint_t val = va_arg(ap, wint_t);
					sprintf(buf, fmt_loc, val);
					printf_ret = str_write(buf);
				}
				else {
					unsigned char val = va_arg(ap, int);
					printf_ret = fputc_unlocked(val, F);
					if(printf_ret!=EOF) printf_ret = 1;
				}
			}
			// String
			else if(c=='s') {
				fmt_loc[fmt_loc_nb++] = c;
				fmt_loc[fmt_loc_nb++] = 0;
				char* val = va_arg(ap, char*);
				printf_ret = str_write(val);
			}
			// Stuff that shouldn't be used
			else if(c=='C') {
				fmt_loc[fmt_loc_nb++] = c;
				fmt_loc[fmt_loc_nb++] = 0;
				wint_t val = va_arg(ap, wint_t);
				sprintf(buf, fmt_loc, val);
				printf_ret = str_write(buf);
			}
			else if(c=='S') {
				fmt_loc[fmt_loc_nb++] = c;
				fmt_loc[fmt_loc_nb++] = 0;
				wint_t* val = va_arg(ap, wint_t*);
				printf_ret = wstr_write(val);
			}
			// Pointer
			else if(c=='p') {
				fmt_loc[fmt_loc_nb++] = c;
				fmt_loc[fmt_loc_nb++] = 0;
				void* val = va_arg(ap, void*);
				sprintf(buf, fmt_loc, val);
				printf_ret = str_write(buf);
			}
			// The number of bytes written so far
			else if(c=='n') {
				if(length_mod1=='h') {
					if(length_mod2=='h') {
						char *val = va_arg(ap, char*);
						*val = written_bytes;
					}
					else {
						short int *val = va_arg(ap, short int*);
						*val = written_bytes;
					}
				}
				else if(length_mod1=='l') {
					if(length_mod2=='l') {
						long long int *val = va_arg(ap, long long int*);
						*val = written_bytes;
					}
					else {
						long int *val = va_arg(ap, long int*);
						*val = written_bytes;
					}
				}
				else {
					int *val = va_arg(ap, int*);
					*val = written_bytes;
				}
			}
			// strerror(errno)
			else if(c=='m') {
				fmt_loc[fmt_loc_nb++] = c;
				fmt_loc[fmt_loc_nb++] = 0;
				printf_ret = str_write(strerror(errno));
			}
			// Write a '%' character
			else if(c=='%') {
				printf_ret = fputc_unlocked('%', F);
				if(printf_ret!=EOF) printf_ret = 1;
			}

			// Unrecognized format specification
			else {
				fmt_loc[fmt_loc_nb++] = c;
				fmt_loc[fmt_loc_nb++] = 0;
				printf_ret = str_write(fmt_loc);
			}

			// Handle the printf() return value
			if(printf_ret<0) { written_bytes = -1; goto CLEANUP; }
			written_bytes += printf_ret;

			// Debug: display the format
			//printf("\n### Format: %s ###\n", fmt_loc);

		} while(1);  // Loop to read the format string

		// Note: any unfinished format string gets discarded

		// Get the next format string
		fmt = va_arg(ap, char*);

	}  // Loop as long as there is a format string to process

	CLEANUP:

	funlockfile(F);

	va_end(ap);

	return written_bytes;
}

#else

int fprintfm(FILE* F, char* fmt, ...) {
	va_list ap;
	va_start(ap, fmt);

	int written_bytes = 0;

	while(fmt!=NULL) {

		// Process the current format string
		do {

			// Search the '%' character
			char c = *(fmt++);
			if(c==0) break;
			if(c!='%') {
				int z = fputc(c, F);
				if(z==EOF) return -1;
				written_bytes ++;
				continue;
			}

			c = *(fmt++);
			if(c==0) break;
			// A little check to avoid the unuseful parsing after
			if(c=='%') {
				int z = fputc('%', F);
				if(z==EOF) return -1;
				written_bytes ++;
				continue;
			}

			// Here we are at the beginning of a format specification
			// This buffer will contain the entire format, including the '%'
			char fmt_loc[32];
			unsigned fmt_loc_nb = 0;
			fmt_loc[fmt_loc_nb++] = '%';

			// The return value of fprintf()
			int printf_ret = 0;

			// The flag character
			if(c=='#' || c=='0' || c=='-' || c==' ' || c=='+' || c=='\'' || c=='I') {
				fmt_loc[fmt_loc_nb++] = c;
				c = *(fmt++);
				if(c==0) break;
			}

			// The field width
			while(isdigit(c)!=0) {
				fmt_loc[fmt_loc_nb++] = c;
				c = *(fmt++);
				if(c==0) break;
			}

			// The precision
			if(c=='.') {
				fmt_loc[fmt_loc_nb++] = c;
				do {
					c = *(fmt++);
					if(c==0) break;
					if(isdigit(c)==0) break;
					fmt_loc[fmt_loc_nb++] = c;
				}while(1);
			}

			// The length modifier
			char length_mod1 = 0;
			char length_mod2 = 0;
			if(c=='h' || c=='l' || c=='L' || c=='q' || c=='j' || c=='z' || c=='t') {
				length_mod1 = c;
				fmt_loc[fmt_loc_nb++] = c;
				c = *(fmt++);
				if(c==0) break;
				if( (length_mod1=='h' && c=='h') || (length_mod1=='l' && c=='l') ) {
					length_mod2 = c;
					fmt_loc[fmt_loc_nb++] = c;
					c = *(fmt++);
					if(c==0) break;
				}
			}

			// The conversion specifier
			// Integers
			if(c=='d' || c=='i' || c=='o' || c=='u' || c=='x' || c=='X') {
				fmt_loc[fmt_loc_nb++] = c;
				fmt_loc[fmt_loc_nb++] = 0;
				if(length_mod1=='l') {
					if(length_mod2=='l') {
						long long int val = va_arg(ap, long long int);
						printf_ret = fprintf(F, fmt_loc, val);
					}
					else {
						long int val = va_arg(ap, long int);
						printf_ret = fprintf(F, fmt_loc, val);
					}
				}
				else {
					int val = va_arg(ap, int);
					printf_ret = fprintf(F, fmt_loc, val);
				}
			}
			// Floating-point
			else if(c=='e' || c=='E' || c=='f' || c=='F' || c=='g' || c=='G'|| c=='a' || c=='A') {
				fmt_loc[fmt_loc_nb++] = c;
				fmt_loc[fmt_loc_nb++] = 0;
				if(length_mod1=='L') {
					long double val = va_arg(ap, long double);
					printf_ret = fprintf(F, fmt_loc, val);
				}
				else {
					double val = va_arg(ap, double);
					printf_ret = fprintf(F, fmt_loc, val);
				}
			}
			// Character
			else if(c=='c') {
				fmt_loc[fmt_loc_nb++] = c;
				fmt_loc[fmt_loc_nb++] = 0;
				char val = va_arg(ap, int);
				printf_ret = fprintf(F, fmt_loc, val);
			}
			// String
			else if(c=='s') {
				fmt_loc[fmt_loc_nb++] = c;
				fmt_loc[fmt_loc_nb++] = 0;
				char* val = va_arg(ap, char*);
				printf_ret = fprintf(F, fmt_loc, val);
			}
			// Stuff that shouldn't be used
			else if(c=='C') {
				fmt_loc[fmt_loc_nb++] = c;
				fmt_loc[fmt_loc_nb++] = 0;
				char val = va_arg(ap, int);
				printf_ret = fprintf(F, fmt_loc, val);
			}
			else if(c=='S') {
				fmt_loc[fmt_loc_nb++] = c;
				fmt_loc[fmt_loc_nb++] = 0;
				char* val = va_arg(ap, char*);
				printf_ret = fprintf(F, fmt_loc, val);
			}
			// Pointer
			else if(c=='p') {
				fmt_loc[fmt_loc_nb++] = c;
				fmt_loc[fmt_loc_nb++] = 0;
				void* val = va_arg(ap, void*);
				printf_ret = fprintf(F, fmt_loc, val);
			}
			// The number of bytes written so far
			else if(c=='n') {
				if(length_mod1=='h') {
					if(length_mod2=='h') {
						char *val = va_arg(ap, char*);
						*val = written_bytes;
					}
					else {
						short int *val = va_arg(ap, short int*);
						*val = written_bytes;
					}
				}
				else if(length_mod1=='l') {
					if(length_mod2=='l') {
						long long int *val = va_arg(ap, long long int*);
						*val = written_bytes;
					}
					else {
						long int *val = va_arg(ap, long int*);
						*val = written_bytes;
					}
				}
				else {
					int *val = va_arg(ap, int*);
					*val = written_bytes;
				}
			}
			// strerror(errno)
			else if(c=='m') {
				fmt_loc[fmt_loc_nb++] = c;
				fmt_loc[fmt_loc_nb++] = 0;
				printf_ret = fprintf(F, fmt_loc);
			}
			// Write a '%' character
			else if(c=='%') {
				fmt_loc[fmt_loc_nb++] = c;
				fmt_loc[fmt_loc_nb++] = 0;
				printf_ret = fprintf(F, fmt_loc);
			}

			// Unrecognized format specification
			else {
				fmt_loc[fmt_loc_nb++] = c;
				fmt_loc[fmt_loc_nb++] = 0;
				printf_ret = fprintf(F, "%s", fmt_loc);
			}

			// Handle the printf() return value
			if(printf_ret<0) return -1;
			written_bytes += printf_ret;

			// Debug: display the format
			//printf("\n### Format: %s ###\n", fmt_loc);

		} while(1);  // Loop to read the format string

		// Note: any unfinished format string gets discarded

		// Get the next format string
		fmt = va_arg(ap, char*);

	}  // Loop as long as there is a format string to process

	va_end(ap);

	return written_bytes;
}

#endif

