
#ifndef _PRINTFM_H_
#define _PRINTFM_H_


#include <stdio.h>
#include <stdarg.h>


// fprintf-like function that accepts an arbitrary number of format strings
// Note: the last format string must be NULL.
int fprintfm(FILE* F, char* fmt, ...);

#define printfm(fmt, ...) fprintfm(stdout, fmt, ##__VA_ARGS__)


#endif  // _PRINTFM_H_

