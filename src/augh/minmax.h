
#ifndef _MINMAX_H_
#define _MINMAX_H_

#include <stdio.h>  // For the print function


//======================================================================
// Structures
//======================================================================

typedef struct {
	double min;
	double avg;
	double max;
} minmax_t;


//======================================================================
// Functions
//======================================================================

// To simplify getting max and min values
#define GetMax(v0,v1) ((v0)>(v1) ? (v0) : (v1))
#define GetMin(v0,v1) ((v0)<(v1) ? (v0) : (v1))



static inline minmax_t minmax_make_cst(double d) {
	minmax_t t;
	t.min = d;
	t.avg = d;
	t.max = d;
	return t;
}

static inline minmax_t* minmax_set_cst(minmax_t *t, double d) {
	t->min = d;
	t->avg = d;
	t->max = d;
	return t;
}
static inline minmax_t* minmax_set_add(minmax_t *dest, minmax_t *src0, minmax_t *src1) {
	dest->min = src0->min + src1->min;
	dest->avg = src0->avg + src1->avg;
	dest->max = src0->max + src1->max;
	return dest;
}
static inline minmax_t* minmax_set_mult(minmax_t *dest, minmax_t *src, double m) {
	dest->min = src->min * m;
	dest->avg = src->avg * m;
	dest->max = src->max * m;
	return dest;
}
static inline minmax_t* minmax_set_avg2(minmax_t *dest, minmax_t *src0, minmax_t *src1) {
	dest->avg = (src0->avg + src1->avg) * 0.5;
	if(src0->min < src1->min) dest->min = src0->min;
	else dest->min = src1->min;
	if(src0->max > src1->max) dest->max = src0->max;
	else dest->max = src1->max;
	return dest;
}
static inline minmax_t* minmax_set_avg2_prob(minmax_t *dest, minmax_t *src0, double prob0, minmax_t *src1, double prob1) {
	dest->avg = (src0->avg * prob0) + (src1->avg * prob1);
	if(src0->min < src1->min) dest->min = src0->min;
	else dest->min = src1->min;
	if(src0->max > src1->max) dest->max = src0->max;
	else dest->max = src1->max;
	return dest;
}
static inline minmax_t* minmax_add_prob(minmax_t *dest, minmax_t *src, double prob) {
	dest->avg += src->avg * prob;
	if(src->min < dest->min) dest->min = src->min;
	if(src->max > dest->max) dest->max = src->max;
	return dest;
}
static inline minmax_t* minmax_add_cst_prob(minmax_t *dest, double val, double prob) {
	dest->avg += val * prob;
	if(val < dest->min) dest->min = val;
	if(val > dest->max) dest->max = val;
	return dest;
}

static inline minmax_t* minmax_add(minmax_t *dest, minmax_t *src) {
	dest->min += src->min;
	dest->avg += src->avg;
	dest->max += src->max;
	return dest;
}
static inline minmax_t* minmax_add2(minmax_t *dest, minmax_t src) {
	return minmax_add(dest, &src);
}
static inline minmax_t* minmax_mult(minmax_t *t, double m) {
	t->min *= m;
	t->avg *= m;
	t->max *= m;
	return t;
}
static inline minmax_t* minmax_addmult(minmax_t *dest, minmax_t *src, double m) {
	dest->min += src->min * m;
	dest->avg += src->avg * m;
	dest->max += src->max * m;
	return dest;
}

static inline void minmax_fprint(FILE* F, minmax_t *dest, char* prefix, char* suffix) {
	if(prefix!=NULL) fprintf(F, "%s", prefix);
	fprintf(F, "min %g / avg %g / max %g", dest->min, dest->avg, dest->max);
	if(suffix!=NULL) fprintf(F, "%s", suffix);
}
static inline void minmax_print(minmax_t *dest, char* prefix, char* suffix) {
	minmax_fprint(stdout, dest, prefix, suffix);
}

static inline void minmax_fprint_raw(FILE* F, minmax_t *dest) {
	fprintf(F, "%g %g %g", dest->min, dest->avg, dest->max);
}
static inline int  minmax_fscanf_raw(FILE* F, minmax_t *dest) {
	int z;
	z = fscanf(F, "%lf", &dest->min);
	if(z!=1) return __LINE__;
	z = fscanf(F, "%lf", &dest->avg);
	if(z!=1) return __LINE__;
	z = fscanf(F, "%lf", &dest->max);
	if(z!=1) return __LINE__;
	return 0;
}


#endif  // _MINMAX_H_

