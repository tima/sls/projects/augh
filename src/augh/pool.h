/* Generic pool of elements

Mandatory #defines :

POOL_prefix           The prefix of all names for structures and functions.
                      The type of the pool structure is <POOL_prefix>_t

POOL_data_t           The type of data stored into the list.
                      The pool functions use pointers to such a data type.

Optional #defines :

POOL_FUNC_ATTRIBUTES  Set custom attributes for all generated functions.
                      Default : static __attribute((__unused__))

POOL_ALLOC_SIZE       The number of elements of each allocation (default : 100).

POOL_STAT             Track the number of allocations and the number of available elements.

#defines for memory allocations :

POOL_ALLOC            Use a custom memory allocation function.
                      Default : the malloc() function from stdlib.h
                      Example : #define POOL_ALLOC(size) calloc(1, size)
                      Example : #define POOL_ALLOC(size) my_alloc_func(my_data, size)

POOL_FREE             Use a custom memory free function.
                      Default : the free() function from stdlib.h

#defines for pool instanciation :

POOL_INSTANCE         Instanciate and initialize an empty pool.

POOL_INSTANCE_ATTRIBUTES
                      The attributes of the instanciated pool.
                      Default : static

*/


// Include of standard headers

#if !defined(POOL_ALLOC) || !defined(POOL_FREE) || defined(POOL_DEBUG_MALLOC)
	#include <stdlib.h>
#endif

// Standard checks of defines

#ifndef POOL_prefix
	#error "POOL_prefix is undefined !"
#endif
#ifndef POOL_data_t
	#error "POOL_data_t is undefined !"
#endif

// Internal definitions

#define POOL_CONCAT(x,y)   x ## _ ## y
#define POOL_XCONCAT(x,y)  POOL_CONCAT(x,y)

#define POOL_t        POOL_XCONCAT(POOL_prefix, t)
#define POOL_elt_t    POOL_XCONCAT(POOL_prefix, elt_t)
#define POOL_array_t  POOL_XCONCAT(POOL_prefix, array_t)

#define POOL_alloc    POOL_XCONCAT(POOL_prefix, alloc)
#define POOL_free     POOL_XCONCAT(POOL_prefix, free)
#define POOL_init     POOL_XCONCAT(POOL_prefix, init)
#define POOL_term     POOL_XCONCAT(POOL_prefix, term)
#define POOL_reinit   POOL_XCONCAT(POOL_prefix, reinit)
#define POOL_pop      POOL_XCONCAT(POOL_prefix, pop)
#define POOL_push     POOL_XCONCAT(POOL_prefix, push)

#define POOL_get_avail  POOL_XCONCAT(POOL_prefix, get_avail)
#define POOL_get_alloc  POOL_XCONCAT(POOL_prefix, get_alloc)

#ifndef POOL_ALLOC
	#define POOL_ALLOC malloc
#endif
#ifndef POOL_FREE
	#define POOL_FREE free
#endif

#ifndef POOL_FUNC_ATTRIBUTES
	#define POOL_FUNC_ATTRIBUTES static __attribute((__unused__))
#endif


//==================================================
// Definition of structures
//==================================================

// Structure for the internal elements.

typedef union POOL_elt_t  POOL_elt_t;
union POOL_elt_t {
	POOL_data_t   data;
	POOL_elt_t*   next;
};

// Pool of nodes

#ifndef POOL_ALLOC_SIZE
#define POOL_ALLOC_SIZE 100
#endif

typedef struct POOL_array_t  POOL_array_t;
struct POOL_array_t {
	POOL_array_t*      next;
	POOL_elt_t         elements[POOL_ALLOC_SIZE];
};

typedef struct POOL_t  POOL_t;
struct POOL_t {
	POOL_elt_t*        avail;  // List of available nodes.
	POOL_array_t*      array;  // List of allocated arrays.
	#ifdef POOL_STAT
	unsigned           count_alloc;  // Number of allocations done
	unsigned           count_avail;  // Number of available nodes
	#endif
};


//==================================================
// Functions
//==================================================

// Initialize a pool
POOL_FUNC_ATTRIBUTES void POOL_init (POOL_t* pool) {
	pool->avail = NULL;
	pool->array = NULL;
	#ifdef POOL_STAT
	pool->count_alloc = 0;
	pool->count_avail = 0;
	#endif
	return;
}

// Terminate a pool
POOL_FUNC_ATTRIBUTES void POOL_term (POOL_t* pool) {
	POOL_array_t *cur = pool->array;
	while (cur != NULL) {
		POOL_array_t *tmp = cur;
		cur = cur->next;
		POOL_FREE(tmp);
	}
}

// Terminate and initialize again
POOL_FUNC_ATTRIBUTES void POOL_reinit (POOL_t* pool) {
	POOL_term(pool);
	POOL_init(pool);
}

// Allocation / free of a pool structure
POOL_FUNC_ATTRIBUTES POOL_t* POOL_alloc () {
	POOL_t* pool = POOL_ALLOC(sizeof(*pool));
	POOL_init(pool);
	return pool;
}
POOL_FUNC_ATTRIBUTES void POOL_free (POOL_t* pool) {
	POOL_term(pool);
	POOL_FREE(pool);
}

// Get a node from the pool
POOL_FUNC_ATTRIBUTES POOL_data_t* POOL_pop (POOL_t* pool) {
	POOL_elt_t* node = NULL;

	#ifdef POOL_DEBUG_MALLOC
	node = malloc(sizeof(*node));
	#else
	if(pool->avail==NULL) {
		// adding a node array to the pool
		POOL_array_t* array = POOL_ALLOC(sizeof(POOL_array_t));
		array->next = pool->array;
		pool->array = array;
		for(unsigned i=0; i<(POOL_ALLOC_SIZE-1); i++) {
			array->elements[i].next = &array->elements[i+1];
		}
		array->elements[POOL_ALLOC_SIZE-1].next = NULL;
		pool->avail = &array->elements[0];
		#ifdef POOL_STAT
		pool->count_alloc ++;
		pool->count_avail += POOL_ALLOC_SIZE;
		#endif
	}
	node = pool->avail;
	pool->avail = node->next;
	#endif

	#ifdef POOL_STAT
	pool->count_avail --;
	#endif

	return (POOL_data_t*)node;
}

// Put a node back in the pool
POOL_FUNC_ATTRIBUTES void POOL_push (POOL_t* pool, POOL_data_t* data) {
	POOL_elt_t* node = (POOL_elt_t*)data;

	#ifdef POOL_DEBUG_MALLOC
	free(node);
	#else
	node->next = pool->avail;
	pool->avail = node;
	#endif

	#ifdef POOL_STAT
	pool->count_avail ++;
	#endif
}

// Get the number of allocations done
POOL_FUNC_ATTRIBUTES unsigned POOL_get_alloc (POOL_t* pool) {
	#ifdef POOL_STAT
	return pool->count_alloc;
	#else
	unsigned count = 0;
	for(POOL_array_t* array=pool->array; array!=NULL; array=array->next) count++;
	return count;
	#endif
}

// Get the number of available elements
POOL_FUNC_ATTRIBUTES unsigned POOL_get_avail (POOL_t* pool) {
	#ifdef POOL_STAT
	return pool->count_avail;
	#else
	unsigned count = 0;
	for(POOL_elt_t* elt=pool->avail; elt!=NULL; elt=elt->next) count++;
	return count;
	#endif
}


// Instantiation of a pool

#ifdef POOL_INSTANCE

#ifndef POOL_INSTANCE_ATTRIBUTES
	#define POOL_INSTANCE_ATTRIBUTES static
#endif

POOL_INSTANCE_ATTRIBUTES POOL_t POOL_INSTANCE = {
	.avail = NULL,
	.array = NULL,
	#ifdef POOL_STAT
	.count_alloc = 0,
	.count_avail = 0,
	#endif
};

#endif


// Undefine the internal definitions

#undef POOL_CONCAT
#undef POOL_XCONCAT

#undef POOL_prefix
#undef POOL_data_t

#undef POOL_t
#undef POOL_elt_t
#undef POOL_array_t

#undef POOL_alloc
#undef POOL_free
#undef POOL_init
#undef POOL_term
#undef POOL_reinit
#undef POOL_pop
#undef POOL_push

#undef POOL_get_avail
#undef POOL_get_alloc

#undef POOL_ALLOC_SIZE
#undef POOL_FUNC_ATTRIB

#ifdef POOL_INSTANCE
	#undef POOL_INSTANCE
	#undef POOL_INSTANCE_ATTRIBUTES
#endif

#ifdef POOL_STAT
	#undef POOL_STAT
#endif

#ifdef POOL_ALLOC
	#undef POOL_ALLOC
#endif
#ifdef POOL_FREE
	#undef POOL_FREE
#endif


