
#ifndef _CHAIN_H_
#define _CHAIN_H_

#include <stdint.h>
#include <stdbool.h>

#include <mut.h>


//====================================================================
// Inline functions
//====================================================================

// All chain types functions

// To simplify writing FOR loops
#define foreach(list,iter) for(typeof(list) iter=list; iter!=NULL; iter=iter->NEXT)
#define foreach_f(list,iter,field) for(typeof(list) iter=list; iter!=NULL; iter=iter->field)


// Functions about the chain_list type

static inline chain_list* ChainList_Concat(chain_list* head, chain_list* tail) {
	return append(head, tail);
}
static inline chain_list* ChainList_Add_Tail(chain_list* chain, void* data) {
	return ChainList_Concat(chain, addchain(NULL, data));
}
static inline chain_list* ChainList_Add_NoNull(chain_list* chain, void* data) {
	if(data==NULL) return chain;
	return addchain(chain, data);
}
static inline chain_list* ChainList_Add_NoDup(chain_list* chain, void* data) {
	for(chain_list* scan=chain; scan!=NULL; scan=scan->NEXT) if(scan->DATA==data) return chain;
	return addchain(chain, data);
}
static inline chain_list* ChainList_Add_NoDupNull(chain_list* chain, void* data) {
	if(data==NULL) return chain;
	for(chain_list* scan=chain; scan!=NULL; scan=scan->NEXT) if(scan->DATA==data) return chain;
	return addchain(chain, data);
}
static inline bool ChainList_IsDataHere(chain_list* chain, void* data) {
	for(chain_list* scan=chain; scan!=NULL; scan=scan->NEXT) if(scan->DATA==data) return true;
	return false;
}
static inline chain_list* ChainList_Replace(chain_list* chain, void* orig, void* val) {
	for(chain_list* scan=chain; scan!=NULL; scan=scan->NEXT) if(scan->DATA==orig) { scan->DATA=val; break; }
	return chain;
}
static inline chain_list* ChainList_RemFirst(chain_list* chain) {
	chain_list* next = chain->NEXT;
	chain->NEXT = NULL;
	freechain(chain);
	return next;
}
static inline void* ChainList_GetIdx(chain_list* chain, unsigned idx) {
	unsigned i = 0;
	chain_list* scan = chain;
	while(scan!=NULL) {
		if(i==idx) return scan->DATA;
		scan = scan->NEXT;
		i++;
	}
	return NULL;
}
static inline void* ChainList_GetLastNode(chain_list* chain) {
	if(chain==NULL) return NULL;
	for( ; chain->NEXT!=NULL; chain=chain->NEXT) ;
	return chain;
}
static inline void* ChainList_GetLast(chain_list* chain) {
	if(chain==NULL) return NULL;
	for( ; chain->NEXT!=NULL; chain=chain->NEXT) ;
	return chain->DATA;
}

static inline unsigned ChainList_Count(chain_list* chain) {
	unsigned count = 0;
	for( ; chain!=NULL; chain=chain->NEXT) count++;
	return count;
}

// The destination list is modified.
chain_list* ChainList_Merge(chain_list* dest, chain_list* src);

// Allocates a new list
chain_list* ChainList_Unique(chain_list* list);

// deldatachain() does it, but this version returns the list if the data was not found (instead of NULL)
chain_list* ChainList_Remove(chain_list* list, void* data);


// ChainNum functions

static inline unsigned ChainNum_Count(num_list* list) {
	unsigned count = 0;
	foreach(list, scan) count++;
	return count;
}
static inline num_list* ChainNum_Dup(num_list* list) {
	num_list* new_list = NULL;
	foreach(list, scan) new_list = addnum(new_list, scan->DATA);
	return new_list;
}
static inline bool ChainNum_IsDataHere(num_list* list, long data) {
	foreach(list, scan) if(scan->DATA==data) return true;
	return false;
}
static inline num_list* ChainNum_Search(num_list* list, long data) {
	foreach(list, scan) if(scan->DATA==data) return scan;
	return NULL;
}
static inline num_list* ChainNum_Add_NoDup(num_list* list, long data) {
	if(ChainNum_IsDataHere(list, data)==true) return list;
	return addnum(list, data);
}

void ChainNum_Print_fbse(num_list* list, FILE* F, char* beg, char* sep, char* end);
static inline void ChainNum_Print(num_list* list) {
	ChainNum_Print_fbse(list, stdout, NULL, NULL, NULL);
}

// ChainPType functions

static inline unsigned ChainPType_Count(ptype_list* chain) {
	unsigned count = 0;
	for( ; chain!=NULL; chain=chain->NEXT) count++;
	return count;
}

ptype_list* ChainPType_Concat(ptype_list* head, ptype_list* tail);

static inline ptype_list* ChainPType_SearchData(ptype_list* head, void* data) {
	for( ; head!=NULL; head=head->NEXT) if(head->DATA==data) return head;
	return NULL;
}

static inline ptype_list* ChainPType_SearchType(ptype_list* head, long type) {
	for( ; head!=NULL; head=head->NEXT) if(head->TYPE==type) return head;
	return NULL;
}

ptype_list* ChainPTtpe_Remove_Element(ptype_list* list, ptype_list* elt);
ptype_list* ChainPTtpe_Remove_Data(ptype_list* list, void* data);
ptype_list* ChainPTtpe_Remove_Data_xorType(ptype_list* list, void* data, long flag);
ptype_list* ChainPType_Reverse(ptype_list* list);

// The input list is modified !
ptype_list* ChainPType_Sort_Inc(ptype_list* list);
ptype_list* ChainPType_Sort_Dec(ptype_list* list);

// Check inclusions
// The DATA is the object, TYPE is the amount.
// Return true is each tested DATA is present in the ref list, and the amount is enough.
bool ChainPType_CheckInclude(ptype_list* list_ref, ptype_list* list_tested);

// ChainBiType functions

static inline unsigned ChainBiType_Count(bitype_list* chain) {
	unsigned count = 0;
	for( ; chain!=NULL; chain=chain->NEXT) count++;
	return count;
}

bitype_list* ChainBiType_Concat(bitype_list* head, bitype_list* tail);

static inline bitype_list* ChainBiType_SearchType(bitype_list* chain, long type) {
	for( ; chain!=NULL; chain=chain->NEXT) if(chain->TYPE==type) return chain;
	return NULL;
}
static inline bitype_list* ChainBiType_SearchFrom(bitype_list* chain, void* data) {
	for( ; chain!=NULL; chain=chain->NEXT) if(chain->DATA_FROM==data) return chain;
	return NULL;
}
static inline bitype_list* ChainBiType_SearchTo(bitype_list* chain, void* data) {
	for( ; chain!=NULL; chain=chain->NEXT) if(chain->DATA_TO==data) return chain;
	return NULL;
}
static inline bitype_list* ChainBiType_SearchFromType(bitype_list* chain, int type, void* data) {
	for( ; chain!=NULL; chain=chain->NEXT) if(chain->TYPE==type && chain->DATA_FROM==data) return chain;
	return NULL;
}



//====================================================================
// Chain type : one pointer, one double
//====================================================================

// Definitions

typedef struct chain_pd_t  chain_pd_t;
struct chain_pd_t {
	void*        p;
	double       d;
	chain_pd_t*  NEXT;
};

// Functions

chain_pd_t* chain_pd_new();
void chain_pd_del(chain_pd_t* elt);

static inline chain_pd_t* chain_pd_make(void* p, double d) {
	chain_pd_t *elt = chain_pd_new();
	elt->p = p;
	elt->d = d;
	return elt;
}
static inline chain_pd_t* chain_pd_add(chain_pd_t* head, void* p, double d) {
	chain_pd_t *elt = chain_pd_make(p, d);
	elt->NEXT = head;
	return elt;
}
static inline chain_pd_t* chain_pd_dup(chain_pd_t* elt) {
	chain_pd_t *new_elt = chain_pd_new();
	new_elt->p = elt->p;
	new_elt->d = elt->d;
	return new_elt;
}

chain_pd_t* chain_pd_dup_list(chain_pd_t* elt);

static inline unsigned    chain_pd_count(chain_pd_t* list) {
	unsigned count = 0;
	foreach(list, elt) count++;
	return count;
}
static inline int         chain_pd_isthere_d(chain_pd_t* list, double d) {
	foreach(list, elt) if(elt->d==d) return 1;
	return 0;
}

// The input list is modified !
chain_pd_t* chain_pd_sort_inc(chain_pd_t* list);
chain_pd_t* chain_pd_sort_dec(chain_pd_t* list);



//===============================================
// All AVL trees
//===============================================

#define AVL_SHARED_DECL

#include "avl_alldecls.h"

// Some specific functions

void* avl_p_GetIdx(avl_p_tree_t* tree, unsigned idx);

int avl_pi_addflag(avl_pi_tree_t* tree, void* key, int flag);

int avl_pi_rmflag(avl_pi_tree_t* tree, void* key, int flag);



#endif  // _CHAIN_H_

