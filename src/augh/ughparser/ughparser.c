
// For asprintf() and canonicalize_file_name()
#define _GNU_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

// To load the ughparser lib
#include <dlfcn.h>

#include "../auto/auto.h"
#include "../auto/hierarchy.h"
#include "../auto/map.h"

// For the definitions of interface and buffer components
#include "../netlist/netlist.h"

#include "ughparser.h"



bool parse_verbose = false;



//======================================================================
// Load of a C file in Hier and Netlist graphs
//======================================================================

int Hier_Load_UghParser(implem_t* Implem, const char* name) {
	int error_code = 0;

	// Save the name of the input source file
	Implem->env.input_file = stralloc(name);

	// Load the source file
	if(Implem->env.globals->verbose.level>VERBOSE_SILENT) printf("Loading '%s'\n", name);

	int C_argc = 0;
	char* C_argv[128];

	// Default argument to gcc
	C_argv[C_argc++] = strdup( "-Wall" );

	C_argv[C_argc++] = strdup( "-DAUGH_SYNTHESIS" );

	// Add the system include dir relative to AUGH install location

	char* exepath = GetExePath_CutBin();
	if(exepath!=NULL) {
		char* fullarg = NULL;
		asprintf(&fullarg, "-I%s/include", exepath);
		if(fullarg!=NULL) {
			C_argv[C_argc++] = fullarg;
		}
	}

	// Extract user arguments
	foreach(Implem->env.cflags, scan) {
		char* C_parse = scan->DATA;
		while( ( C_argv[C_argc] = strtok(C_parse, " ") ) ) {
			C_argc++;
			// strtok: continue on current position
			C_parse = NULL;
		}
	}

	bool use_external_cpp = true;
	bool use_external_syntax_check = false;
	char* tmpfilename = NULL;

	if(use_external_cpp==true) {

		// Get a temporary filename
		char buf[100];
		sprintf(buf, "/tmp/augh_XXXXXX");
		int tmp_fd = mkstemp(buf);
		close(tmp_fd);
		tmpfilename = strdup(buf);

		// Count the full size of the command-line
		unsigned l = 25;
		for(unsigned i=0; i<C_argc; i++) l = l + 3 + strlen(C_argv[i]);
		l = l + 10;  // -Werror
		l = l + 5 + strlen(tmpfilename);  // The output in /tmp
		l = l + 3 + strlen(name);  // The input C file

		char cmds[l];
		char* cmds_scan = cmds;

		sprintf(cmds_scan, "gcc -E -fsyntax-only");

		for(unsigned i=0; i<C_argc; i++) {
			cmds_scan += strlen(cmds_scan);
			sprintf(cmds_scan, " %s", C_argv[i]);
		}
		cmds_scan += strlen(cmds_scan);
		sprintf(cmds_scan, " -Werror");
		cmds_scan += strlen(cmds_scan);
		sprintf(cmds_scan, " -o %s", tmpfilename);
		cmds_scan += strlen(cmds_scan);
		sprintf(cmds_scan, " %s", name);

		// Flush all open output streams
		fflush(NULL);

		dbgprintf("Executed command: %s\n", cmds);

		// Execute the external preprocessor
		int z = system(cmds);

		if(z!=0) {
			printf("ERROR : The external preprocesssor failed (code %d).\n", z);
			// Remove the tmp file
			remove(tmpfilename);
			free(tmpfilename);
			return __LINE__;
		}

		// Check syntax
		if(use_external_syntax_check==true) {
			// FIXME we can't check syntax that simply
			// This section is really for debug

			// Flush all open output streams
			fflush(NULL);

			cmds[5] = 'c';
			z = system(cmds);

			if(z!=0) {
				printf("ERROR : External syntx checking failed (code %d).\n", z);
				// Remove the tmp file
				remove(tmpfilename);
				free(tmpfilename);
				return __LINE__;
			}
		}

		// Use the generated file as new input for AUGH
		printf("INFO : Using the externally-preprocessed file as input: %s.\n", tmpfilename);
		name = tmpfilename;
	}

	// Flush all open output streams
	fflush(NULL);

	// Now we use a .so library that contains the UGH parser
	// The reason this is not hardcoded in AUGH is that this way,
	// it is correctly initialized for each source file to parse.

	// Dynamically load the ughparser lib
	void* ptrlib = dlopen("libaugh_ughparser.so", RTLD_LAZY | RTLD_LOCAL | RTLD_DEEPBIND);
	if(ptrlib==NULL) {
		printf("ERROR: Can't load the parser lib.\n");
		// Remove the tmp file, if any
		remove(tmpfilename);
		free(tmpfilename);
		return __LINE__;
	}

	int (*my_c2hier) (implem_t* Implem, const char* filename, int Argc, char **Argv ) = NULL;

	my_c2hier = dlsym(ptrlib, "C2Hier");
	if(my_c2hier==NULL) {
		printf("ERROR: Can't find the main function in the parser lib.\n");
		// Remove the tmp file, if any
		remove(tmpfilename);
		free(tmpfilename);
		return __LINE__;
	}

	// Load the C into the Hier
	int ret = my_c2hier(Implem, name, C_argc, C_argv);

	// Unload the UGH parser lib
	dlclose(ptrlib);

	// Remove the tmp file
	if(tmpfilename!=NULL) {
		remove(tmpfilename);
		free(tmpfilename);
	}

	if(ret!=0) {
		printf("ERROR: Failed to load the file.\n");
		error_code = ret;
		goto EXIT_POINT;
	}

	#ifndef NDEBUG  // Debug: Check integrity of the Hier graph
	Hier_Check_Integrity(Implem->H);
	#endif

	EXIT_POINT:

	// Free the C flags
	for(unsigned i=0; i<C_argc; i++) free(C_argv[i]);

	if(error_code!=0) {
		if(Implem->H!=NULL) {
			// Reset the main heap of nodes in Hier to avoid double free
			avl_p_reset(&Implem->H->NODES_ALL);
			// Free the rest
			Hier_Free(Implem->H);
			Implem->H = NULL;
		}
		Map_Netlist_Implem_Free(Implem);
	}

	return error_code;
}


