
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#include "../auto/auto.h"
#include "../auto/hierarchy.h"

#include "plugins.h"
#include "plugins_hier.h"



int Plugin_DeclBuiltinFunc(plugin_t* plugin, builtinfunc_t* builtin) {
	bool b = Hier_BuiltinFunc_Add(builtin);
	if(b==false) return __LINE__;

	builtin->plugin = plugin;
	avl_pp_add_overwrite(&plugin->builtin_funcs, builtin->func_name, builtin);

	return 0;
}


