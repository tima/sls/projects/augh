
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#include "../auto/auto.h"
#include "../netlist/netlist.h"

#include "plugins.h"
#include "plugins_netlist.h"



int Plugin_DeclCompModel(plugin_t* plugin, netlist_comp_model_t* model) {
	bool b = Netlist_CompModel_Declare(model);
	if(b==false) return __LINE__;

	model->plugin = plugin;
	avl_pp_add_overwrite(&plugin->comp_models, model->name, model);

	return 0;
}

int Plugin_DeclAccessModel(plugin_t* plugin, netlist_access_model_t* model) {
	bool b = Netlist_AccessModel_Declare(model);
	if(b==false) return __LINE__;

	model->plugin = plugin;
	avl_pp_add_overwrite(&plugin->access_models, model->name, model);

	return 0;
}

