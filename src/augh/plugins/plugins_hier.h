
#ifndef _PLUGINS_HIER_H_
#define _PLUGINS_HIER_H_

#include "plugins.h"
#include "../auto/hierarchy.h"
#include "../auto/hierarchy_build.h"


int Plugin_DeclBuiltinFunc(plugin_t* plugin, builtinfunc_t* builtin);


#endif  // _PLUGINS_HIER_H_

