
// For asprintf() and canonicalize_file_name()
#define _GNU_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <dlfcn.h>

#include "../auto/auto.h"
#include "../auto/command.h"
#include "../auto/hierarchy_build.h"
#include "plugins.h"



//=====================================================================
// General plugin structure
//=====================================================================

// Create a pool of type : builtin_func_t

#define POOL_prefix      pool_plugin
#define POOL_data_t      plugin_t
#define POOL_ALLOC_SIZE  100

#define POOL_INSTANCE             POOL_PLUGIN
#define POOL_INSTANCE_ATTRIBUTES  static

#include "../pool.h"

// Allocation wrappers

static plugin_t* Plugin_New() {
	plugin_t* plugin = pool_plugin_pop(&POOL_PLUGIN);
	memset(plugin, 0, sizeof(*plugin));
	plugin->is_registered = false;
	// Init hierarchy stuff
	avl_pp_init(&plugin->builtin_funcs);
	// Init netlist stuff
	avl_pp_init(&plugin->comp_models);
	avl_pp_init(&plugin->access_models);
	// Init techno stuff
	avl_pp_init(&plugin->technos);
	avl_pp_init(&plugin->fpgas);
	avl_pp_init(&plugin->boards);
	// The structure is now ready to use
	return plugin;
}
static void Plugin_Free(plugin_t* elt) {
	pool_plugin_push(&POOL_PLUGIN, elt);
}



//=====================================================================
// Functions local to this module
//=====================================================================

// Initialization of the plug-in module

static avl_pp_tree_t PLUGINS;

void Plugin_Init() {
	avl_pp_init(&PLUGINS);
}



static plugin_t* Plugin_Find(const char* name) {
	plugin_t* plugin = NULL;
	avl_pp_find_data(&PLUGINS, name, (void**)&plugin);
	return plugin;
}
static void Plugin_Register_nocheck(plugin_t* plugin) {
	avl_pp_add_overwrite(&PLUGINS, plugin->name, plugin);
	plugin->is_registered = true;
}
static void Plugin_FreeFull(plugin_t* plugin) {
	avl_pp_rem_key(&PLUGINS, plugin->name);
	if(plugin->path!=NULL) free(plugin->path);
	if(plugin->params!=NULL) free(plugin->params);
	if(plugin->callbacks.free!=NULL) plugin->callbacks.free(plugin);
	if(plugin->dl_handle!=NULL) dlclose(plugin->dl_handle);
	// Hierarchy stuff
	avl_pp_foreach(&plugin->builtin_funcs, scan) Hier_BuiltinFunc_Remove(scan->key);
	avl_pp_reset(&plugin->builtin_funcs);
	// Netlist stuff
	avl_pp_foreach(&plugin->comp_models, scan) Netlist_CompModel_Remove(scan->key);
	avl_pp_reset(&plugin->comp_models);
	avl_pp_foreach(&plugin->access_models, scan) Netlist_AccessModel_Remove(scan->key);
	avl_pp_reset(&plugin->access_models);

	// FIXMEEEE The structures are removed butnot freed
	// FIXMEEEE Missing removing of Techno stuff

	// The plugin structure
	Plugin_Free(plugin);
}



//=====================================================================
// Functions that can be called from plug-ins
//=====================================================================

void Plugin_PrintInfo(plugin_t* plugin) {
	printf("API Version .......... %s\n", plugin->api_version);
	printf("Name ................. %s\n", plugin->name);
	printf("Version .............. %s\n", plugin->version);
	printf("Library path ......... %s\n", plugin->path!=NULL ? plugin->path : "");
	printf("Parameters ........... %s\n", plugin->params!=NULL ? plugin->params : "");
	printf("Command interpreter .. %s\n", plugin->callbacks.interpreter!=NULL ? "yes" : "no");
	// Hierarchy stuff
	if(avl_pp_isempty(&plugin->builtin_funcs)==false) {
		printf("Builtin functions ....");
		avl_pp_foreach(&plugin->builtin_funcs, scan) printf(" %s", (char*)scan->key);
		printf("\n");
	}
	// Netlist stuff
	if(avl_pp_isempty(&plugin->comp_models)==false) {
		printf("Component models .....");
		avl_pp_foreach(&plugin->comp_models, scan) printf(" %s", (char*)scan->key);
		printf("\n");
	}
	if(avl_pp_isempty(&plugin->access_models)==false) {
		printf("Access models ........");
		avl_pp_foreach(&plugin->access_models, scan) printf(" %s", (char*)scan->key);
		printf("\n");
	}
	// Techno stuff
	if(avl_pp_isempty(&plugin->technos)==false) {
		printf("Technologies .........");
		avl_pp_foreach(&plugin->technos, scan) printf(" %s", (char*)scan->key);
		printf("\n");
	}
	if(avl_pp_isempty(&plugin->fpgas)==false) {
		printf("FPGA chips ...........");
		avl_pp_foreach(&plugin->fpgas, scan) printf(" %s", (char*)scan->key);
		printf("\n");
	}
	if(avl_pp_isempty(&plugin->boards)==false) {
		printf("FPGA boards ..........");
		avl_pp_foreach(&plugin->boards, scan) printf(" %s", (char*)scan->key);
		printf("\n");
	}
}

bool Plugin_IsNameDeclared(const char* name) {
	return avl_pp_isthere(&PLUGINS, namealloc(name));
}

int Plugin_Declare(plugin_t* plugin) {
	// Sanity checks
	if(plugin==NULL) {
		printf("Error %s:%u : NULL plugin structure\n", __FILE__, __LINE__);
		return __LINE__;
	}

	int fields_null_nb = 0;
	if(plugin->path==NULL)        fields_null_nb++;
	if(plugin->api_version==NULL) fields_null_nb++;
	if(plugin->name==NULL)        fields_null_nb++;
	if(plugin->version==NULL)     fields_null_nb++;

	if(fields_null_nb > 0) {
		printf("Error %s:%u : Some fields in plugin structure are NULL:\n", __FILE__, __LINE__);
		printf("  api_version .. %s\n", plugin->api_version);
		printf("  path ......... %s\n", plugin->path);
		printf("  name ......... %s\n", plugin->name);
		printf("  version ...... %s\n", plugin->version);
		return __LINE__;
	}

	// Check the API version
	if(strcmp(plugin->api_version, AUGH_PLUGIN_API_VERSION)!=0) {
		printf("Error: Incompatible API version in plugin path '%s'\n", plugin->path);
		printf("       AUGH API version is '%s', plugin API version is '%s'\n", AUGH_PLUGIN_API_VERSION, plugin->api_version);
		return __LINE__;
	}

	// Check the name
	if( Plugin_IsNameDeclared(plugin->name)==true ) {
		printf("Error: Name conflict for plugin path '%s'\n", plugin->path);
		printf("       The plugin name '%s' is already registered.\n", plugin->name);
		return __LINE__;
	}

	// Register the plugin
	Plugin_Register_nocheck(plugin);

	return 0;
}



//=====================================================================
// Loading plugins
//=====================================================================

#if 0  // Not used yet

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

static bool file_exists(char* name) {
	struct stat buffer;
	int status = stat(name, &buffer);
	if(status==0) return true;
	return false;
}

#endif

// Note: A path sent to dlopen() can begin by '/', '.' or '..' but not '~'
//   So this function replaces the '~' by the $HOME env. variable
// The flags are for dlopen()
static int Plugin_Load(const char* path, int dlopen_flags, char* commands, command_t* cmd_data) {

	// Create the plugin structure

	plugin_t* plugin = Plugin_New();
	int error_code = 0;

	// Analyze the file name given and try to load the plugin

	if(path[0]=='/' || path[0]=='.') {
		// Don't modify the given path

		plugin->dl_handle = dlopen(path, dlopen_flags);
		if(plugin->dl_handle==NULL) {
			printf("Error: Could not load the plugin '%s'.\n", path);
			printf("%s\n", dlerror());
			error_code = __LINE__;
			goto ERROR_CASE;
		}
		plugin->path = strdup(path);

	}
	else if(path[0]=='~') {
		// Prepend the home directory path

		char* homedir = getenv("HOME");
		if(homedir==NULL) {
			printf("Warning: The env. var. \"HOME\" is not set.\n");
			printf("         Can't expand '~' in the given path.\n");
			error_code = __LINE__;
			goto ERROR_CASE;
		}

		char fullpath[ strlen(homedir) + strlen(path) + 1];
		sprintf(fullpath, "%s/%s", homedir, path+1);
		printf("Searching for plugin path %s ...\n", fullpath);

		plugin->dl_handle = dlopen(fullpath, dlopen_flags);
		if(plugin->dl_handle==NULL) {
			printf("Error: Could not load the plugin '%s'.\n", path);
			printf("%s\n", dlerror());
			error_code = __LINE__;
			goto ERROR_CASE;
		}
		plugin->path = strdup(fullpath);

	}
	else {
		// FIXME subdirs could be handled, for example when expanding the env. var. "_"
		if(strchr(path, '/')!=NULL) {
			printf("Error: The given path is not absolute, it must not contain subdirectories.\n");
			error_code = __LINE__;
			goto ERROR_CASE;
		}

		unsigned path_len = strlen(path);
		bool is_so = false;

		if(path_len<=3) {}
		else {
			// Find the extension .so
			char* ptr_so = strstr(path, ".so");
			if(ptr_so!=NULL) {
				if(ptr_so==path) {
					printf("Error: Invalid plugin name.\n");
					error_code = __LINE__;
					goto ERROR_CASE;
				}
				is_so = true;
			}
		}

		char libnameso[path_len + 8];

		if(is_so==true) strcpy(libnameso, path);
		else {
			sprintf(libnameso, "lib%s.so", path);
			printf("Searching for plugin lib name %s ...\n", libnameso);
		}

		plugin->dl_handle = dlopen(libnameso, dlopen_flags);
		if(plugin->dl_handle!=NULL) {
			plugin->path = strdup(libnameso);
		}

		char* plugins_loc_dir = "lib/augh/plugins";

		// Try to prepend the environment variable (A)UGH_HOME
		if(plugin->dl_handle==NULL) {

			char* envaughhome = "AUGH_HOME";
			char* aughhomedir = getenv(envaughhome);

			if(aughhomedir!=NULL) {

				char* fullpath = NULL;

				if(aughhomedir[0]=='~') {
					if(aughhomedir[1]=='/') {
						char* homedir = getenv("HOME");
						if(homedir!=NULL) {
							asprintf(&fullpath, "%s/%s/%s/%s", homedir, aughhomedir+2, plugins_loc_dir, libnameso);
						}
						else {
							printf("Warning: The env. var. \"HOME\" is not set.\n");
							printf("         Can't expand '~' in the env. var. \"%s\".\n", envaughhome);
						}
					}
					else {
						printf("Error: Invalid beginning of env. var. \"%s\".\n", envaughhome);
					}
				}
				else {
					asprintf(&fullpath, "%s/%s/%s", aughhomedir, plugins_loc_dir, libnameso);
				}

				if(fullpath!=NULL) {
					printf("Trying path %s ...\n", fullpath);
					plugin->dl_handle = dlopen(fullpath, dlopen_flags);
					if(plugin->dl_handle==NULL) {
						printf("  Fail. Reason: %s\n", dlerror());
						free(fullpath);
					}
					else plugin->path = fullpath;
				}

			}
		}

		// Get the AUGH exec path and search relative to it
		if(plugin->dl_handle==NULL) {

			char* exepath = GetExePath_CutBin();
			if(exepath!=NULL) {

				char* fullpath = NULL;
				asprintf(&fullpath, "%s/%s/%s", exepath, plugins_loc_dir, libnameso);

				if(fullpath!=NULL) {
					printf("Trying path %s ...\n", fullpath);
					plugin->dl_handle = dlopen(fullpath, dlopen_flags);
					if(plugin->dl_handle==NULL) {
						printf("  Fail. Reason: %s\n", dlerror());
						free(fullpath);
					}
					else plugin->path = fullpath;
				}

			}
		}

	}

	if(plugin->dl_handle==NULL) {
		printf("Error: The plugin '%s' couldn't be loaded.\n", path);
		error_code = __LINE__;
		goto ERROR_CASE;
	}

	// Set the other parameters

	commands = SplitString_trim_beg(commands);
	if(commands!=NULL) plugin->params = strdup(commands);

	// Initialize plugin data

	plugin_init_t sym_init = dlsym(plugin->dl_handle, "plugin_init");
	if(sym_init==NULL) {
		printf("Warning: No init function in plugin '%s'\n", path);
	}
	else {
		int z = sym_init(plugin);
		if(z!=0) {
			printf("Error: Failed initialization of plugin '%s' (code %d\n", path, z);
			error_code = __LINE__;
			goto ERROR_CASE;
		}
	}

	if(plugin->is_registered==false) {
		printf("Error: No declaration of plugin '%s'\n", path);
		error_code = __LINE__;
		goto ERROR_CASE;
	}

	// Here the plugin is assumed to be inserted in the plugins base

	printf("INFO : Added plugin '%s' version '%s'.\n", plugin->name, plugin->version);

	// Normal exit
	return 0;

	// Error cases

	ERROR_CASE:

	Plugin_FreeFull(plugin);

	return error_code;
}

// Function supposed to be used for plugins specified in AUGH command-line
int Plugin_Load_cmdline(const char* path) {
	return Plugin_Load(path, RTLD_GLOBAL | RTLD_DEEPBIND | RTLD_NOW, NULL, NULL);
}



//=====================================================================
// Command interpreter
//=====================================================================

int Plugin_Command(implem_t* Implem, command_t* cmd_data) {
	const char* cmd = Command_PopParam(cmd_data);
	if(cmd==NULL) {
		printf("Error in Plugins module : No command received. Try 'help'.\n");
		return -1;
	}

	if(strcmp(cmd, "help")==0) {
		printf(
			"This is the module 'Plugins'.\n"
			"Possible commands :\n"
			"  help            Display this help.\n"
			"  load [options] <path> [plugin-params]\n"
			"    Load a plug-in, provide filesystem path.\n"
			"    The plugin-params are parsed by a callback declared by the plugin.\n"
			"    Available options:\n"
			"    -lazy         Add the flag LAZY to dlopen(). Can be useful for interdependant plugins.\n"
			"    -p | -path    Stop parsing of options. Enable to use a path beginning with '-'.\n"
			"  cmd <name> <command>\n"
			"      Send some commands to an embedded command interpreter\n"
			"  info [<name>]   Display information about plugins.\n"
		);
		return 0;
	}

	if(strcmp(cmd, "load")==0) {
		bool param_lazy = false;

		// Parse parameters
		char const * param = NULL;
		do {
			param = Command_PopParam(cmd_data);
			if(param==NULL) break;
			if(param[0]!='-') break;
			// Perform actions for the parameters
			if(strcmp(param, "-lazy")==0) param_lazy = true;
			else if(strcmp(param, "-p")==0 || strcmp(param, "-path")==0) {
				param = NULL;
				break;
			}
			else {
				printf("Error[plugins]: Command '%s': Unknown parameter '%s'.\n", cmd, param);
				return -1;
			}
		}while(1);

		// Get the path
		if(param==NULL) {
			param = Command_PopParam(cmd_data);
		}
		if(param==NULL) {
			printf("Error: No plugin path is given.\n");
			return -1;
		}

		int flags = RTLD_GLOBAL | RTLD_DEEPBIND;
		if(param_lazy==true) flags |= RTLD_LAZY; else flags |= RTLD_NOW;

		// Note: for dlopen flags, RTLD_LAZY could be useful when plugins have dependencies
		return Plugin_Load(param, flags, NULL, cmd_data);
	}
	else if(strcmp(cmd, "unload")==0) {
		const char* plugin_name = Command_PopParam_namealloc(cmd_data);
		if(plugin_name==NULL) {
			printf("Error[plugins]: Missing parameter for command '%s'.\n", cmd);
			return -1;
		}
		plugin_t* plugin = Plugin_Find(plugin_name);
		if(plugin==NULL) {
			printf("Error[plugins]: There is no plugin '%s'.\n", plugin_name);
			return -1;
		}
		Plugin_FreeFull(plugin);
		return 0;
	}

	else if(strcmp(cmd, "cmd")==0) {
		char* plugin_name = Command_PopParam_namealloc(cmd_data);
		if(plugin_name==NULL) {
			printf("Error[plugins]: Missing parameter for command '%s'.\n", cmd);
			return -1;
		}
		plugin_t* plugin = Plugin_Find(plugin_name);
		if(plugin==NULL) {
			printf("Error[plugins]: There is no plugin '%s'.\n", plugin_name);
			return -1;
		}
		if(plugin->callbacks.interpreter==NULL) {
			printf("Error[plugins]: The plugin '%s' has no declared command interpreter.\n", plugin_name);
			return -1;
		}
		return plugin->callbacks.interpreter(plugin, Implem, cmd_data);
	}

	else if(strcmp(cmd, "info")==0) {
		char* plugin_name = Command_PopParam_namealloc(cmd_data);
		if(plugin_name!=NULL) {
			plugin_t* plugin = Plugin_Find(plugin_name);
			if(plugin==NULL) {
				printf("Error[plugins]: There is no plugin '%s'.\n", plugin_name);
				return -1;
			}
			printf("\n");
			Plugin_PrintInfo(plugin);
			printf("\n");
		}
		else if(avl_pp_isempty(&PLUGINS)==true) {
			printf("No plugin is used.\n");
		}
		else {
			printf("There are %u registered plugins.\n", avl_pp_count(&PLUGINS));
			printf("\n");
			avl_pp_foreach(&PLUGINS, scanPlug) {
				plugin_t* plugin = scanPlug->data;
				Plugin_PrintInfo(plugin);
				printf("\n");
			}
		}
	}

	else {
		printf("Error in module 'Plugins': Unknown command '%s'.\n", cmd);
		return -1;
	}

	return 0;
}


