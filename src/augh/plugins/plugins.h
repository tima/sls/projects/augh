
#ifndef _PLUGINS_H_
#define _PLUGINS_H_


// Some useful type definitions
typedef struct plugin_t  plugin_t;

typedef int (*plugin_init_t)(plugin_t* plugin);
typedef int (*plugin_cb_p_t)(plugin_t* plugin);
typedef int (*plugin_cb_pi_t)(plugin_t* plugin, implem_t* Implem);
typedef int (*plugin_cb_pis_t)(plugin_t* plugin, implem_t* Implem, command_t* cmd_data);

// The data structure that defines contains all data for a plugin instance
struct plugin_t {

	// The AUGH version the plugin was compiled for
	char* api_version;

	// The plugin identification
	char* name;
	char* version;

	// The absolute (or relative) path of the loaded library
	char* path;
	// The loading-time parameters
	char* params;

	// This structure is private to dlopen()
	void* dl_handle;

	// Flag: the plugin structure is registered in AUGH list
	bool is_registered;

	// This is the plugin's private data. Usage is only optional.
	void* private_data;

	// The callback functions
	struct {

		// An optional free function
		plugin_cb_p_t free;
		// Declare a command interpreter
		plugin_cb_pis_t interpreter;

	} callbacks;

	// Declarations of hierarchy stuff
	avl_pp_tree_t builtin_funcs;

	// Declarations of netlist stuff
	avl_pp_tree_t comp_models;
	avl_pp_tree_t access_models;

	// Declarations of techno stuff
	avl_pp_tree_t technos;
	avl_pp_tree_t fpgas;
	avl_pp_tree_t boards;

};


// Plugin API version

#define AUGH_PLUGIN_API_VERSION  "0.1.0"


// Functions for AUGH only, not for plugins

void Plugin_Init();

bool Plugin_IsNameDeclared(const char* name);

int Plugin_Load_cmdline(const char* path);

int Plugin_Command(implem_t* Implem, command_t* cmd_data);



// API for plug-ins

__attribute__((warn_unused_result))
int Plugin_Declare(plugin_t* plugin);

/* Functions the plug-in is supposed to have declared:

int plugin_init(plugin_t* plugin)

Returned value:
  The plugin returns zero if OK.
  Otherwise the returned value is assumed to be an error code, which is
  displayed to the user, and the plugin data structure is freed.

FIXME The function name is too much similar to AUGH plugin module init function

*/

// The main function each plugin is supposed to declare
int plugin_init(plugin_t* plugin);



#endif  // _PLUGINS_H_


