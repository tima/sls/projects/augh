
#ifndef _PLUGINS_TECHNO_H_
#define _PLUGINS_TECHNO_H_

#include "plugins.h"
#include "../netlist/netlist.h"


int Plugin_DeclTechno(plugin_t* plugin, techno_t* techno);
int Plugin_DeclFPGA(plugin_t* plugin, fpga_model_t* fpga);
int Plugin_DeclBoard(plugin_t* plugin, board_t* board);


#endif  // _PLUGINS_TECHNO_H_

