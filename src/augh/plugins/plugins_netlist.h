
#ifndef _PLUGINS_NETLIST_H_
#define _PLUGINS_NETLIST_H_

#include "plugins.h"
#include "../netlist/netlist.h"


int Plugin_DeclCompModel(plugin_t* plugin, netlist_comp_model_t* model);
int Plugin_DeclAccessModel(plugin_t* plugin, netlist_access_model_t* model);


#endif  // _PLUGINS_NETLIST_H_

