
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#include "../auto/auto.h"
#include "../auto/techno.h"

#include "plugins.h"
#include "plugins_techno.h"



int Plugin_DeclTechno(plugin_t* plugin, techno_t* techno) {
	bool b = Techno_DeclareTechno(techno);
	if(b==false) return __LINE__;

	techno->plugin = plugin;
	avl_pp_add_overwrite(&plugin->technos, techno->name, techno);

	return 0;
}

int Plugin_DeclFPGA(plugin_t* plugin, fpga_model_t* fpga) {
	bool b = Techno_DeclareFPGA(fpga);
	if(b==false) return __LINE__;

	fpga->plugin = plugin;
	avl_pp_add_overwrite(&plugin->fpgas, fpga->name, fpga);

	return 0;
}

int Plugin_DeclBoard(plugin_t* plugin, board_t* board) {
	bool b = Techno_DeclareBoard(board);
	if(b==false) return __LINE__;

	board->plugin = plugin;
	avl_pp_add_overwrite(&plugin->boards, board->name, board);

	return 0;
}

