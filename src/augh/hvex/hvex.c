
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <stdbool.h>
#include <assert.h>

#include <stdarg.h>  // For creation functions

#include "hvex.h"
#include "hvex_calc.h"

#include "../auto/auto.h"  // For uint_bitsnb()



//==============================================
// HVEX models for built-in AUGH operations
//==============================================

hvex_model_t* HVEX_VECTOR = NULL;    // name[left, right]
hvex_model_t* HVEX_LITERAL = NULL;   // a constant value

hvex_model_t* HVEX_INDEX = NULL;     // name(index)[left, right]
hvex_model_t* HVEX_CONCAT = NULL;    // Concatenation, left to right
hvex_model_t* HVEX_RESIZE = NULL;    // Signed or unsigned extension
hvex_model_t* HVEX_REPEAT = NULL;    // Repeat a 1-bit operand

// Should only be used for parsing
hvex_model_t* HVEX_STRING = NULL;
hvex_model_t* HVEX_FUNCTION = NULL;  // func(arg, arg, ...)
hvex_model_t* HVEX_DIVQ = NULL;       // Integer division
hvex_model_t* HVEX_DIVR = NULL;       // Remainder of integer division

// Arithmetic operations
hvex_model_t* HVEX_ADD = NULL;
hvex_model_t* HVEX_SUB = NULL;
hvex_model_t* HVEX_NEG = NULL;
hvex_model_t* HVEX_MUL = NULL;

// Arithmetic operations with carry input
hvex_model_t* HVEX_ADDCIN = NULL;

// Comparisons
hvex_model_t* HVEX_EQ = NULL;
hvex_model_t* HVEX_NE = NULL;
hvex_model_t* HVEX_GT = NULL;
hvex_model_t* HVEX_GE = NULL;
hvex_model_t* HVEX_LT = NULL;
hvex_model_t* HVEX_LE = NULL;
hvex_model_t* HVEX_GTS = NULL;
hvex_model_t* HVEX_GES = NULL;
hvex_model_t* HVEX_LTS = NULL;
hvex_model_t* HVEX_LES = NULL;

// Shifts and rotations
hvex_model_t* HVEX_SHL = NULL;
hvex_model_t* HVEX_SHR = NULL;
hvex_model_t* HVEX_ROTL = NULL;
hvex_model_t* HVEX_ROTR = NULL;

// Logic operations
hvex_model_t* HVEX_NOT = NULL;
hvex_model_t* HVEX_AND = NULL;
hvex_model_t* HVEX_NAND = NULL;
hvex_model_t* HVEX_OR = NULL;
hvex_model_t* HVEX_NOR = NULL;
hvex_model_t* HVEX_XOR = NULL;
hvex_model_t* HVEX_NXOR = NULL;

// Assignments
hvex_model_t* HVEX_ASG = NULL;
hvex_model_t* HVEX_ASG_EN = NULL;
hvex_model_t* HVEX_ASG_ADDR = NULL;
hvex_model_t* HVEX_ASG_ADDR_EN = NULL;

// Binary MUX
// First operand is selection (N bits). Other 2^N operands must have same width.
hvex_model_t* HVEX_MUXB = NULL;
// Decoded MUX
// Operands are all MUXDECIN. Only one condition of operands is assumed to be 1.
hvex_model_t* HVEX_MUXDEC = NULL;
// First operand is condition, second operand is data
hvex_model_t* HVEX_MUXDECIN = NULL;

// Used only at parsing
hvex_model_t* HVEX_ARRAY = NULL;  // 2 operands



//==============================================
// Allocation stuff
//==============================================

hvex_model_t* hvex_model_new() {
	hvex_model_t* model = calloc(1, sizeof(*model));
	return model;
}
void hvex_model_free(hvex_model_t* model) {
	free(model);
}


#define POOL_prefix      pool_hvex
#define POOL_data_t      hvex_t
#define POOL_ALLOC_SIZE  1000

#define POOL_INSTANCE             POOL_HVEX
#define POOL_INSTANCE_ATTRIBUTES  static

#include "../pool.h"

// Allocation wrappers

hvex_t* hvex_new() {
	hvex_t* vex = pool_hvex_pop(&POOL_HVEX);
	memset(vex, 0, sizeof(*vex));
	vex->pad = '0';
	return vex;
}
static void hvex_free_raw(hvex_t* vex) {
	pool_hvex_push(&POOL_HVEX, vex);
}
void hvex_free_ops(hvex_t* vex) {
	hvex_t* vexop = vex->operands;
	while(vexop!=NULL) {
		hvex_t* nextop = vexop->next;
		hvex_free(vexop);
		vexop = nextop;
	}
	vex->operands = NULL;
}
void hvex_free(hvex_t* vex) {
	if(vex->operands!=NULL) hvex_free_ops(vex);
	if(vex->data!=NULL && vex->model->func_datafree!=NULL) vex->model->func_datafree(vex->data);
	hvex_free_raw(vex);
}



//==============================================
// Misc functions
//==============================================

// Count opernads
unsigned hvex_countops(hvex_t* vex) {
	unsigned count = 0;
	hvex_foreach(vex->operands, vexop) count++;
	return count;
}

// Adding operands
void hvex_addop_head(hvex_t* parent, hvex_t* child) {
	assert(child->prev==NULL);
	child->father = parent;
	child->father_type = HVEX_FATHER_HVEX;
	child->next = parent->operands;
	if(parent->operands!=NULL) parent->operands->prev = child;
	parent->operands = child;
}
void hvex_addop_tail(hvex_t* parent, hvex_t* child) {
	assert(child->next==NULL);
	child->father = parent;
	child->father_type = HVEX_FATHER_HVEX;
	if(parent->operands==NULL) parent->operands = child;
	else {
		hvex_t* lastop = parent->operands;
		while(lastop->next!=NULL) lastop = lastop->next;
		lastop->next = child;
		child->prev = lastop;
	}
}

// Replace one operand by one or several operands
void hvex_addmultop_before(hvex_t* oldop, hvex_t* newop) {
	// Set father
	hvex_foreach(newop, scanop) {
		scanop->father = oldop->father;
		scanop->father_type = HVEX_FATHER_HVEX;
	}
	// Find the last of the new operands
	hvex_t* lastop = newop;
	while(lastop->next!=NULL) lastop = lastop->next;
	// Perform link
	newop->prev = oldop->prev;
	lastop->next = oldop->next;
	if(newop->prev!=NULL) newop->prev->next = newop;
	else newop->father->operands = newop;
	oldop->prev = lastop;
}
void hvex_addmultop_after(hvex_t* oldop, hvex_t* newop) {
	// Set father
	hvex_foreach(newop, scanop) {
		scanop->father = oldop->father;
		scanop->father_type = HVEX_FATHER_HVEX;
	}
	// Find the last of the new operands
	hvex_t* lastop = newop;
	while(lastop->next!=NULL) lastop = lastop->next;
	// Perform link
	lastop->next = oldop->next;
	newop->prev = oldop;
	if(lastop->next!=NULL) lastop->next->prev = lastop;
	oldop->next = newop;
}
void hvex_addmultop_replace(hvex_t* oldop, hvex_t* newop) {
	// Set father
	hvex_foreach(newop, scanop) {
		scanop->father = oldop->father;
		scanop->father_type = HVEX_FATHER_HVEX;
	}
	// Find the last of the new operands
	hvex_t* lastop = newop;
	while(lastop->next!=NULL) lastop = lastop->next;
	// Perform link
	lastop->next = oldop->next;
	newop->prev = oldop->prev;
	if(lastop->next!=NULL) lastop->next->prev = lastop;
	if(newop->prev!=NULL) newop->prev->next = newop;
	else newop->father->operands = newop;
	// Free the old operand
	hvex_free(oldop);
}

// Clear data, operands and model. Keep the rest.
void hvex_clearinside(hvex_t* vex) {
	if(vex->data!=NULL && vex->model->func_datafree!=NULL) vex->model->func_datafree(vex->data);
	vex->model = NULL;
	vex->data = NULL;
	if(vex->operands!=NULL) hvex_free_ops(vex);
}

// The dest node is kept and modified, the new node is freed.
// Warning: the fields pad and father are kept. Both nodes must have same width.
void hvex_replace_free(hvex_t* destvex, hvex_t* srcvex) {
	assert(destvex->width==srcvex->width);
	if(destvex->data!=NULL && destvex->model->func_datafree!=NULL) destvex->model->func_datafree(destvex->data);
	destvex->model = srcvex->model;
	destvex->data = srcvex->data;
	// Handle indexes
	assert(destvex->width==srcvex->width);
	destvex->left = srcvex->left;
	destvex->right = srcvex->right;
	// Transfer the operands
	if(destvex->operands!=NULL) hvex_free_ops(destvex);
	destvex->operands = srcvex->operands;
	srcvex->operands = NULL;
	hvex_foreach(destvex->operands, vexop) {
		vexop->father = destvex;
		vexop->father_type = HVEX_FATHER_HVEX;
	}
	// Free the new node without taking care of what's inside
	hvex_free_raw(srcvex);
}
// Same as above except it resizes the source vex to the indexes of the dest vex
void hvex_replace_resize_free(hvex_t* destvex, hvex_t* srcvex) {
	if(destvex->right >= srcvex->width) {
		hvex_replace_byextend_free(destvex, srcvex);
		return;
	}
	if(destvex->left+1 != srcvex->width) hvex_resize(srcvex, destvex->left+1);
	if(destvex->right > 0) hvex_croprel(srcvex, 0, destvex->right);
	hvex_replace_free(destvex, srcvex);
}
// Replace by the left extension of srcvex (which is freed)
void hvex_replace_byextend_free(hvex_t* destvex, hvex_t* srcvex) {
	if(srcvex->pad!=0) {
		hvex_lit_setrepeat(destvex, srcvex->pad, destvex->width);
		hvex_free(srcvex);
	}
	else {
		if(srcvex->width > 1) hvex_croprel(srcvex, 0, srcvex->width - 1);
		hvex_addop_head(destvex, srcvex);
		destvex->model = HVEX_REPEAT;
		destvex->left -= destvex->right;
		destvex->right = 0;
	}
}

// Return zero if these are identical
int hvex_cmp(hvex_t* vex1, hvex_t* vex2) {
	if(vex1->model != vex2->model) return 1;
	if(vex1->left != vex2->left) return 1;
	if(vex1->right != vex2->right) return 1;
	if(vex1->model->func_datacmp!=NULL) {
		int z = vex1->model->func_datacmp(vex1->data, vex2->data);
		if(z!=0) return 1;
	}
	else if(vex1->data != vex2->data) return 1;
	if(vex1->pad != vex2->pad) return 1;
	// Compare operands
	hvex_t* op1 = vex1->operands;
	hvex_t* op2 = vex2->operands;
	while(op1!=NULL && op2!=NULL) {
		if(hvex_cmp(op1, op2)!=0) return 1;
		op1 = op1->next;
		op2 = op2->next;
	}
	if(op1!=NULL || op2!=NULL) return 1;
	// The nodes are identical
	return 0;
}

// Note: The parent node is assumed to also be an hvex node
static inline hvex_t* hvex_unlinkop_internal(hvex_t* vexop) {
	if(vexop->prev==NULL) vexop->father->operands = vexop->next;
	else vexop->prev->next = vexop->next;
	if(vexop->next!=NULL) vexop->next->prev = vexop->prev;
	return vexop;
}
hvex_t* hvex_unlinkop(hvex_t* vexop) {
	hvex_unlinkop_internal(vexop);
	vexop->father_type = HVEX_FATHER_NONE;
	vexop->father = NULL;
	vexop->prev = NULL;
	vexop->next = NULL;
	return vexop;
}
void hvex_remop(hvex_t* vexop) {
	hvex_unlinkop_internal(vexop);
	hvex_free(vexop);
}

// Duplicate a node and its operands. Fields father, prev and next are left NULL.
hvex_t* hvex_dup(hvex_t* vex) {
	if(vex==NULL) return NULL;

	hvex_t* newvex = hvex_new();
	newvex->width = vex->width;
	newvex->left = vex->left;
	newvex->right = vex->right;

	newvex->model = vex->model;
	if(vex->data!=NULL && vex->model->func_datadup!=NULL) newvex->data = vex->model->func_datadup(vex->data);
	else newvex->data = vex->data;

	newvex->pad = vex->pad;

	hvex_t* lastop = NULL;

	hvex_foreach(vex->operands, vexop) {
		hvex_t* newop = hvex_dup(vexop);
		newop->father = newvex;
		newop->father_type = HVEX_FATHER_HVEX;
		if(lastop!=NULL) { lastop->next = newop; newop->prev = lastop; }
		else newvex->operands = newop;
		lastop = newop;
	}

	return newvex;
}

// Return to top-most parent hvex node
hvex_t* hvex_top_parent(hvex_t* vex) {
	while(vex->father_type==HVEX_FATHER_HVEX && vex->father!=NULL) vex = vex->father;
	return vex;
}



//==============================================
// Creation of nodes
//==============================================

hvex_t* hvex_newvec(const char* name, unsigned left, unsigned right) {
	hvex_t* vex = hvex_new();
	vex->model = HVEX_VECTOR;
	vex->data = (void*)name;
	vex->left = left;
	vex->right = right;
	vex->width = left - right + 1;
	return vex;
}
hvex_t* hvex_newvec_bit(const char* name) {
	hvex_t* vex = hvex_new();
	vex->model = HVEX_VECTOR;
	vex->data = (void*)name;
	vex->width = 1;
	return vex;
}

hvex_t* hvex_newindex(const char* name, unsigned left, unsigned right, hvex_t* addr) {
	hvex_t* vex = hvex_new();
	vex->model = HVEX_INDEX;
	vex->data = (void*)name;
	vex->left = left;
	vex->right = right;
	vex->width = left - right + 1;
	hvex_addop_head(vex, addr);
	return vex;
}

hvex_t* hvex_newlit(const char* lit) {
	hvex_t* vex = hvex_new();
	vex->model = HVEX_LITERAL;
	vex->data = (void*)lit;
	vex->width = strlen(lit);
	vex->left = vex->width - 1;
	return vex;
}
hvex_t* hvex_newlit_repeat(char c, unsigned n) {
	hvex_t* vex = hvex_new();
	vex->model = HVEX_LITERAL;
	char buf[n+1];
	for(unsigned i=0; i<n; i++) buf[i] = c;
	buf[n] = 0;
	vex->data = stralloc(buf);
	vex->left = n - 1;
	vex->width = n;
	return vex;
}
hvex_t* hvex_newlit_bit(char c) {
	hvex_t* vex = hvex_new();
	vex->model = HVEX_LITERAL;
	char buf[2] = { c, 0 };
	vex->data = stralloc(buf);
	vex->width = 1;
	return vex;
}
hvex_t* hvex_newlit_int(int i, unsigned width, bool is_signed) {
	hvex_t* vex = hvex_new();
	vex->model = HVEX_LITERAL;

	vex->width = width;
	vex->left  = width - 1;
	vex->right = 0;

	char buf[width + 1];
	unsigned scan = 0;  // Index in buf
	unsigned mask = 0;

	// Write the sign extension bits
	if(width > sizeof(i) * 8) {
		mask = (unsigned)1 << (sizeof(i) * 8 - 1 );
		unsigned bit_extend_nb = width - sizeof(i) * 8;
		char bit_extend = (is_signed==false || (i & mask)==0) ? '0' : '1';
		for( ; scan < bit_extend_nb; scan++ ) buf[scan] = bit_extend;
	}
	else {
		mask = (unsigned)1 << (width - 1);
	}
	// Write the bits of the integer
	for( ; scan<width; scan++) {
		buf[scan] = (i & mask)==0 ? '0' : '1';
		i <<= 1;
	}

	buf[width] = 0;
	vex->data = stralloc(buf);
	if(is_signed==true) vex->pad = 0;

	return vex;
}

// The first elements of the array are the most significant
hvex_t* hvex_newlit_arrint32_be(const int* arr, unsigned nb, unsigned width, bool is_signed) {
	assert(nb > 0 && width > 0);

	hvex_t* vex = hvex_new();
	vex->model = HVEX_LITERAL;

	vex->width = width;
	vex->left  = width - 1;
	vex->right = 0;

	// Get buffer dimension and size of sign extension
	unsigned bufsize = 32 * nb;
	unsigned extsize = 0;
	if(width > bufsize) { extsize = width - bufsize; bufsize = width; }

	char buf[bufsize + 1];
	unsigned bufidx = 0;  // Index in buf

	// Write the sign extension bits
	if(extsize > 0) {
		char pad = (is_signed==false || (arr[0] & (1L << 31))==0) ? '0' : '1';
		for(unsigned i=0; i<extsize; i++) buf[bufidx++] = pad;
	}

	// Write the bits from the integers of the array
	for(unsigned i=0; i<nb; i++) {
		unsigned curval = arr[i];
		unsigned mask = 1L << 31;
		while(mask!=0) {
			buf[bufidx++] = (curval & mask)==0 ? '0' : '1';
			mask >>= 1;
		}
	}

	buf[bufsize] = 0;
	vex->data = namealloc(buf + bufsize - width);
	if(is_signed==true) vex->pad = 0;

	return vex;
}

hvex_t* hvex_newfunc(const char* name, unsigned width) {
	hvex_t* vex = hvex_new();
	vex->model = HVEX_FUNCTION;
	vex->data = (void*)name;
	vex->left = width - 1;
	vex->width = width;
	return vex;
}
hvex_t* hvex_newfunc_op1(const char* name, unsigned width, hvex_t* op) {
	hvex_t* vex = hvex_newfunc(name, width);
	hvex_addop_head(vex, op);
	return vex;
}
hvex_t* hvex_newfunc_op2(const char* name, unsigned width, hvex_t* op1, hvex_t* op2) {
	hvex_t* vex = hvex_newfunc(name, width);
	hvex_addop_head(vex, op2);
	hvex_addop_head(vex, op1);
	return vex;
}
// The last operand must be NULL
hvex_t* hvex_newfunc_opn(const char* name, unsigned width, ...) {
	va_list ap;
	va_start(ap, width);

	hvex_t* vex = hvex_newfunc(name, width);

	do {
		hvex_t* op = va_arg(ap, hvex_t*);
		if(op==NULL) break;
		hvex_addop_tail(vex, op);
	} while(1);

	va_end(ap);

	return vex;
}

hvex_t* hvex_newmodel(hvex_model_t* model, unsigned width) {
	hvex_t* vex = hvex_new();
	vex->model = model;
	vex->left = width - 1;
	vex->width = width;
	return vex;
}
hvex_t* hvex_newmodel_op1(hvex_model_t* model, unsigned width, hvex_t* op) {
	hvex_t* vex = hvex_newmodel(model, width);
	hvex_addop_head(vex, op);
	return vex;
}
hvex_t* hvex_newmodel_op2(hvex_model_t* model, unsigned width, hvex_t* op1, hvex_t* op2) {
	hvex_t* vex = hvex_newmodel(model, width);
	hvex_addop_head(vex, op2);
	hvex_addop_head(vex, op1);
	return vex;
}
// The last operand must be NULL
hvex_t* hvex_newmodel_opn(hvex_model_t* model, unsigned width, ...) {
	va_list ap;
	va_start(ap, width);

	hvex_t* vex = hvex_newmodel(model, width);

	do {
		hvex_t* op = va_arg(ap, hvex_t*);
		if(op==NULL) break;
		hvex_addop_tail(vex, op);
	} while(1);

	va_end(ap);

	return vex;
}

// Create a new node, move the content of the orig node to it, and reset the orig node
// Fields of the orig node guaranteed to be kept: indexes, width, pad
hvex_t* hvex_new_move(hvex_t* vex) {
	if(vex==NULL) return NULL;

	hvex_t* newvex = hvex_new();
	newvex->width = vex->width;
	newvex->left = vex->left;
	newvex->right = vex->right;

	newvex->model = vex->model;
	newvex->data = vex->data;
	newvex->pad = vex->pad;

	// Transfer the operands
	newvex->operands = vex->operands;
	hvex_foreach(newvex->operands, vexop) {
		vexop->father = newvex;
		vexop->father_type = HVEX_FATHER_HVEX;
	}

	// Reset the orig node
	vex->model = NULL;
	vex->operands = NULL;
	vex->data = NULL;

	return newvex;
}



//==============================================
// Checking
//==============================================

#define adderror(node, msg) \
	do { list_errors = addbitype(list_errors, __LINE__, node, msg); } while(0)

static char hvex_buferr[1024];
#define adderrorf(node, msg, ...) \
	do { sprintf(hvex_buferr, msg, ##__VA_ARGS__); adderror(node, stralloc(hvex_buferr)); } while(0)

static bool hvex_check_bit(char bit) {
	if(bit=='0') return true;
	if(bit=='1') return true;
	if(bit=='-') return true;
	return false;
}

static bitype_list* hvex_check_vec(hvex_t* vex) {
	char* name = hvex_vec_getname(vex);
	bitype_list* list_errors = NULL;

	if(name==NULL) adderror(vex, "Vector with no name");
	if(stralloc(name)!=name) adderror(vex, "Vector name not stralloc'd");
	if(vex->operands!=NULL) adderror(vex, "Vector can't have operands");

	return list_errors;
}
static bitype_list* hvex_check_lit(hvex_t* vex) {
	char* lit = hvex_lit_getval(vex);
	bitype_list* list_errors = NULL;

	if(lit==NULL) adderror(vex, "Literal with no data string");
	if(stralloc(lit)!=lit) adderror(vex, "Literal string not stralloc'd");
	if(vex->operands!=NULL) adderror(vex, "Literal can't have operands");

	if(vex->right!=0) adderrorf(vex, "Right index of literal is %u, should be zero", vex->right);

	if(lit!=NULL) {
		for(unsigned i=0; i<vex->width; i++) {
			if(hvex_check_bit(lit[i])==true) continue;
			adderror(vex, "Wrong bits in literal string");
			break;
		}
		if(strlen(lit)!=vex->width) adderrorf(vex, "Inconsistent literal width: str is %zu, field says %u", strlen(lit), vex->width);
	}

	return list_errors;
}

static bitype_list* hvex_check_index(hvex_t* vex) {
	char* name = hvex_index_getname(vex);
	bitype_list* list_errors = NULL;

	if(name==NULL) adderror(vex, "Index operation with no name");
	if(stralloc(name)!=name) adderror(vex, "Index name not stralloc'd");
	if(hvex_countops(vex)!=1) adderrorf(vex, "Number of operands of index is %u, should be 1", hvex_countops(vex));

	return list_errors;
}
static bitype_list* hvex_check_concat(hvex_t* vex) {
	bitype_list* list_errors = NULL;

	if(vex->right!=0) adderrorf(vex, "Right index of concat is %u, should be 0", vex->right);
	if(vex->operands==NULL) adderror(vex, "Concat operation with no operands");

	unsigned sumwidth = 0;
	hvex_foreach(vex->operands, vexop) sumwidth += vexop->width;
	if(sumwidth!=vex->width) adderrorf(vex, "Sum of widthes of operands of concat is %u, should be %u",  sumwidth, vex->width);

	return list_errors;
}
static bitype_list* hvex_check_repeat(hvex_t* vex) {
	bitype_list* list_errors = NULL;

	if(vex->right!=0) adderrorf(vex, "Right index of repeat is %u, should be 0", vex->right);
	if(hvex_countops(vex)!=1) adderrorf(vex, "Number of operands of repeat is %u, should be 1", hvex_countops(vex));

	hvex_foreach(vex->operands, vexop) {
		if(vexop->width!=1) adderrorf(vex, "Width of operand of repeat is %u, should be 1", vexop->width);
	}

	return list_errors;
}
static bitype_list* hvex_check_resize(hvex_t* vex) {
	bitype_list* list_errors = NULL;

	if(vex->right!=0) adderrorf(vex, "Right index of resize is %u, should be 0", vex->right);
	if(hvex_countops(vex)!=1) adderrorf(vex, "Number of operands of resize is %u, should be 1", hvex_countops(vex));

	return list_errors;
}

static bitype_list* hvex_check_string(hvex_t* vex) {
	char* str = vex->data;
	bitype_list* list_errors = NULL;

	if(vex->operands!=NULL) adderror(vex, "String can't have operands");
	if(vex->right!=0) adderrorf(vex, "Right index of string is %u, should be zero", vex->right);
	if(strlen(str)!=vex->width) adderrorf(vex, "Inconsistent string width: str is %zu, field says %u", strlen(str), vex->width);

	return list_errors;
}
static bitype_list* hvex_check_function(hvex_t* vex) {
	char* name = vex->data;
	bitype_list* list_errors = NULL;

	if(name==NULL) adderror(vex, "Function with no name");

	return list_errors;
}
static bitype_list* hvex_check_divqr(hvex_t* vex) {
	bitype_list* list_errors = NULL;

	if(hvex_countops(vex)!=2) adderrorf(vex, "Number of operands of model %s is %u, should be 2", vex->model->name, hvex_countops(vex));

	return list_errors;
}

static bitype_list* hvex_check_add(hvex_t* vex) {
	bitype_list* list_errors = NULL;

	if(vex->operands==NULL) adderror(vex, "Add with no operands");

	return list_errors;
}
static bitype_list* hvex_check_sub(hvex_t* vex) {
	bitype_list* list_errors = NULL;

	if(hvex_countops(vex)!=2) adderrorf(vex, "Number of operands of sub is %u, should be 2", hvex_countops(vex));

	return list_errors;
}
static bitype_list* hvex_check_neg(hvex_t* vex) {
	bitype_list* list_errors = NULL;

	if(hvex_countops(vex)!=1) adderrorf(vex, "Number of operands of sub is %u, should be 1", hvex_countops(vex));

	return list_errors;
}
static bitype_list* hvex_check_mul(hvex_t* vex) {
	bitype_list* list_errors = NULL;

	if(vex->operands==NULL) adderror(vex, "Mul with no operands");

	return list_errors;
}

static bitype_list* hvex_check_addcin(hvex_t* vex) {
	bitype_list* list_errors = NULL;

	if(hvex_countops(vex)!=3) adderrorf(vex, "Number of operands of addcin is %u, should be 3", hvex_countops(vex));
	else {
		hvex_t* op_cin = vex->operands->next->next;
		if(op_cin->width!=1) adderrorf(vex, "Width of CIN operand of addcin is %u, should be 1", op_cin->width);
	}

	return list_errors;
}

static bitype_list* hvex_check_eqs(hvex_t* vex) {
	bitype_list* list_errors = NULL;

	if(vex->width!=1) adderrorf(vex, "Width of model %s is %u, should be 1", vex->model->name, vex->width);
	if(vex->right!=0) adderrorf(vex, "Right index of model %s is %u, should be 0", vex->model->name, vex->right);
	if(hvex_countops(vex)<2) adderrorf(vex, "Number of operands of model %s is %u, should be >= 2", vex->model->name, hvex_countops(vex));

	return list_errors;
}
static bitype_list* hvex_check_ineqs(hvex_t* vex) {
	bitype_list* list_errors = NULL;

	if(vex->width!=1) adderrorf(vex, "Width of model %s is %u, should be 1", vex->model->name, vex->width);
	if(vex->right!=0) adderrorf(vex, "Right index of model %s is %u, should be 0", vex->model->name, vex->right);
	if(hvex_countops(vex)!=2) adderrorf(vex, "Number of operands of model %s is %u, should be 2", vex->model->name, hvex_countops(vex));

	return list_errors;
}

static bitype_list* hvex_check_shiftsrots(hvex_t* vex) {
	bitype_list* list_errors = NULL;

	if(hvex_countops(vex)!=2) adderrorf(vex, "Model %s has %u operands, should be 2", vex->model->name, hvex_countops(vex));

	return list_errors;
}

static bitype_list* hvex_check_not(hvex_t* vex) {
	bitype_list* list_errors = NULL;

	if(hvex_countops(vex)!=1) adderrorf(vex, "Model not has %u operands, should be 1", hvex_countops(vex));

	hvex_foreach(vex->operands, vexop) {
		if(vexop->width!=vex->width) adderrorf(vex, "Operand of not has width %u, vex is %u", vexop->width, vex->width);
	}

	return list_errors;
}
static bitype_list* hvex_check_logics(hvex_t* vex) {
	bitype_list* list_errors = NULL;

	if(vex->operands==NULL) adderrorf(vex, "Model %s has no operands", vex->model->name);

	hvex_foreach(vex->operands, vexop) {
		if(vexop->width!=vex->width) {
			adderrorf(vex, "Operand of model %s has width %u, vex is %u", vex->model->name, vexop->width, vex->width);
		}
	}

	return list_errors;
}

static bitype_list* hvex_check_asg(hvex_t* vex) {
	bitype_list* list_errors = NULL;

	if(hvex_countops(vex)!=2) adderrorf(vex, "ASG has %u operands, should be 2", hvex_countops(vex));
	else {
		hvex_t* vex_dest = hvex_asg_get_dest(vex);
		hvex_t* vex_expr = hvex_asg_get_expr(vex);
		if(vex_dest->model!=HVEX_VECTOR) adderrorf(vex, "Dest of ASG is model '%s', should be Vector", vex_dest->model->name);
		if(vex_dest->width != vex_expr->width) adderrorf(vex, "Dest and Expr of ASG have widthes %u and %u", vex_dest->width, vex_expr->width);
	}

	return list_errors;
}
static bitype_list* hvex_check_asg_en(hvex_t* vex) {
	bitype_list* list_errors = NULL;

	if(hvex_countops(vex)!=3) adderrorf(vex, "ASG_EN has %u operands, should be 3", hvex_countops(vex));
	else {
		hvex_t* vex_dest = hvex_asg_get_dest(vex);
		hvex_t* vex_expr = hvex_asg_get_expr(vex);
		hvex_t* vex_cond = hvex_asg_get_cond(vex);
		if(vex_dest->model!=HVEX_VECTOR) adderrorf(vex, "Dest of ASG_EN is model '%s', should be Vector", vex_dest->model->name);
		if(vex_dest->width != vex_expr->width) adderrorf(vex, "Dest and Expr of ASG_EN have widthes %u and %u", vex_dest->width, vex_expr->width);
		if(vex_cond->width!=1) adderrorf(vex, "Cond operand of ASG_EN has width %u, should be 1", vex_cond->width);
	}

	return list_errors;
}
static bitype_list* hvex_check_asg_addr(hvex_t* vex) {
	bitype_list* list_errors = NULL;

	if(hvex_countops(vex)!=3) adderrorf(vex, "ASG_ADDR has %u operands, should be 3", hvex_countops(vex));
	else {
		hvex_t* vex_dest = hvex_asg_get_dest(vex);
		hvex_t* vex_expr = hvex_asg_get_expr(vex);
		if(vex_dest->model!=HVEX_VECTOR) adderrorf(vex, "Dest of ASG_ADDR is model '%s', should be Vector", vex_dest->model->name);
		if(vex_dest->width != vex_expr->width) adderrorf(vex, "Dest and Expr of ASG_ADDR have widthes %u and %u", vex_dest->width, vex_expr->width);
	}

	return list_errors;
}
static bitype_list* hvex_check_asg_addr_en(hvex_t* vex) {
	bitype_list* list_errors = NULL;

	if(hvex_countops(vex)!=4) adderrorf(vex, "ASG_ADDR_EN has %u operands, should be 3", hvex_countops(vex));
	else {
		hvex_t* vex_dest = hvex_asg_get_dest(vex);
		hvex_t* vex_expr = hvex_asg_get_expr(vex);
		hvex_t* vex_cond = hvex_asg_get_cond(vex);
		if(vex_dest->model!=HVEX_VECTOR) adderrorf(vex, "Dest of ASG_ADDR_EN is model '%s', should be Vector", vex_dest->model->name);
		if(vex_dest->width != vex_expr->width) adderrorf(vex, "Dest and Expr of ASG_ADDR_EN have widthes %u and %u", vex_dest->width, vex_expr->width);
		if(vex_cond->width!=1) adderrorf(vex, "Cond operand of ASG_ADDR_EN has width %u, should be 1", vex_cond->width);
	}

	return list_errors;
}

static bitype_list* hvex_check_muxb(hvex_t* vex) {
	bitype_list* list_errors = NULL;

	hvex_t* vexop_sel = vex->operands;

	unsigned operands_nb = hvex_countops(vex);
	if(operands_nb==0) adderrorf(vex, "MUXB has no operands");
	else if(vexop_sel->width>=30) adderrorf(vex, "MUXB sel input is clearly too wide: %u", vexop_sel->width);
	else {
		operands_nb --;
		unsigned expected_ops = ((unsigned) 1) << vexop_sel->width;
		if(expected_ops != operands_nb) {
			adderrorf(vex, "MUXB Wrong number of operands: sel is %u bits, expect 2^%u = %u data operands but found %u",
				vexop_sel->width, vexop_sel->width, expected_ops, operands_nb
			);
		}
		unsigned wrong_width_nb = 0;
		hvex_foreach(vex->operands->next, vexop) {
			if(vexop->width!=vex->width) wrong_width_nb ++;
		}
		if(wrong_width_nb!=0) adderrorf(vex, "MUXB: %u data operands have wrong width", wrong_width_nb);
	}

	return list_errors;
}
static bitype_list* hvex_check_muxdec(hvex_t* vex) {
	bitype_list* list_errors = NULL;

	if(vex->operands==NULL) adderrorf(vex, "MUXDEC has no operands");

	unsigned wrong_width_nb = 0;
	unsigned wrong_model_nb = 0;
	hvex_foreach(vex->operands, vexop) {
		if(vexop->model!=HVEX_MUXDECIN) wrong_model_nb ++;
		if(vexop->width!=vex->width) wrong_width_nb ++;
	}
	if(wrong_width_nb!=0) adderrorf(vex, "MUXDEC: %u data operands have wrong width", wrong_width_nb);
	if(wrong_model_nb!=0) adderrorf(vex, "MUXDEC: %u data operands have wrong model", wrong_model_nb);

	return list_errors;
}
static bitype_list* hvex_check_muxdecin(hvex_t* vex) {
	bitype_list* list_errors = NULL;

	if(vex->father_type!=HVEX_FATHER_HVEX) {
		adderrorf(vex, "MUXDECIN has not HVEX parent");
	}
	else if(vex->father->model!=HVEX_MUXDEC) {
		adderrorf(vex, "MUXDECIN has father model '%s' instead of MUXDEC", vex->father->model->name);
	}

	if(hvex_countops(vex)!=2) adderrorf(vex, "MUXDECIN has %u operands, should be 2", hvex_countops(vex));
	else {
		hvex_t* vexop_cond = vex->operands;
		if(vexop_cond->width!=1) adderrorf(vex, "MUXDECIN: Condition operand has width %u, should be 1", vexop_cond->width);
		hvex_t* vexop_data = vex->operands->next;
		if(vexop_data->width!=vex->width) adderrorf(vex, "MUXDECIN: Data operand has width %u but vex is %u", vexop_data->width, vex->width);
	}

	return list_errors;
}



// Launch all checks recursively and return the error messages
static bitype_list* hvex_check_internal(hvex_t* vex, unsigned depth) {
	bitype_list* list_errors = NULL;

	// For debug: set a limit to detect recursive instantiation
	#ifndef NDEBUG
	assert(depth < 100);
	#endif

	// Check that a model is set
	if(vex->model==NULL) {
		adderrorf(vex, "Missing model");
	}

	// Check the width
	if(vex->width==0 || vex->left < vex->right || vex->width != vex->left - vex->right + 1) {
		adderrorf(vex, "Inconsistent indexes: w=%u l=%u r=%u", vex->width, vex->left, vex->right);
	}

	// Check the pad bit
	if(vex->pad!=0 && hvex_check_bit(vex->pad)==false) {
		adderrorf(vex, "Inconsistent pad: %02x", vex->pad);
	}

	// Check first operand
	if(vex->operands!=NULL && vex->operands->prev!=NULL) adderrorf(vex, "First operand %p has PREV", vex->operands);

	// Check links between operands
	hvex_foreach(vex->operands, vexop) {
		if(vexop->father!=vex) adderrorf(vex, "Operand %p has wrong father: %p", vexop, vexop->father);
		if(vexop->father_type!=HVEX_FATHER_HVEX) adderrorf(vex, "Operand %p has wrong father type: %u", vexop, vexop->father_type);
		if(vexop->prev==NULL && vex->operands!=vexop) adderrorf(vex, "Operand %p has no PREV but is not first operand", vexop);
		if(vexop->prev!=NULL && vexop->prev->next!=vexop) adderrorf(vex, "Operand %p: Wrong prev->next: %p", vexop, vexop->prev->next);
		if(vexop->next!=NULL && vexop->next->prev!=vexop) adderrorf(vex, "Operand %p: Wrong next->prev: %p", vexop, vexop->next->prev);
	}

	// Check the operands
	hvex_foreach(vex->operands, vexop) {
		bitype_list* oplist_errors = hvex_check_internal(vexop, depth+1);
		list_errors = ChainBiType_Concat(oplist_errors, list_errors);
	}

	// Check the node itself
	if(vex->model!=NULL && vex->model->func_check!=NULL) {
		bitype_list* modlist_errors = vex->model->func_check(vex);
		list_errors = ChainBiType_Concat(modlist_errors, list_errors);
	}

	// Return the full list
	return list_errors;
}

// Main check function: Display errors and abort if any
void hvex_checkabort(hvex_t* vex) {
	// Get the errors
	bitype_list* list_errors = hvex_check_internal(vex, 0);
	// Display errors
	if(list_errors!=NULL) {
		unsigned count = 0;
		foreach(list_errors, scan) {
			printf("HVEX ERROR node %p: %s (code %lu)\n", scan->DATA_FROM, (char*)scan->DATA_TO, scan->TYPE);
			count++;
		}
		printf("Errors found: %u\n", count);
		// Note: the list is intentionally not freed to enable manual scan for debug
		abort();
	}
}



//==============================================
// Printing
//==============================================

// After each print, this flag must come reset to false
static bool hvex_fprint_flag_bounds = false;

void hvex_fprint_boundsonly_raw(FILE* F, hvex_t* vex, char beg, char end) {
	char c = vex->pad;
	if(c==0) c = 's';
	else if(c=='0') c = 'u';
	fprintf(F, "%c%u:%u:%u:%c%c", beg, vex->left, vex->right, vex->width, c, end);
}
void hvex_fprint_boundsonly_brk(FILE* F, hvex_t* vex) {
	hvex_fprint_boundsonly_raw(F, vex, '{', '}');
}
void hvex_fprint_boundsonly_par(FILE* F, hvex_t* vex) {
	hvex_fprint_boundsonly_raw(F, vex, '(', ')');
}

// This is the function to use when recursively printing
void hvex_fprint(FILE* F, hvex_t* vex) {
	assert(vex!=NULL);
	if(hvex_fprint_flag_bounds==true) hvex_fprint_boundsonly_brk(F, vex);
	if(vex->model->func_fprint!=NULL) vex->model->func_fprint(F, vex);
	else {
		char buf[strlen(vex->data) + 20];
		sprintf(buf, "NOPRINTFUNC##%s", (char*)vex->model->name);
		hvex_fprint_opbefore(F, buf, vex);
	}
}
// Print all bounds and add a newline after all is printed
void hvex_fprint_bn(FILE* F, hvex_t* vex) {
	hvex_fprint_flag_bounds = true;
	hvex_fprint(F, vex);
	hvex_fprint_flag_bounds = false;
	fprintf(F, "\n");
}
void hvex_fprint_b(FILE* F, hvex_t* vex) {
	hvex_fprint_flag_bounds = true;
	hvex_fprint(F, vex);
	hvex_fprint_flag_bounds = false;
}
// Shortcuts to print to standard output
void hvex_print(hvex_t* vex) {
	hvex_fprint(stdout, vex);
}
void hvex_print_bn(hvex_t* vex) {
	hvex_fprint_bn(stdout, vex);
}
void hvex_print_b(hvex_t* vex) {
	hvex_fprint_b(stdout, vex);
}

// Print stuff like that for CONCAT: op1 & op2 & op3
void hvex_fprint_opbetween(FILE* F, const char* opstr, hvex_t* vex) {
	unsigned nb = 0;
	fprintf(F, "(");
	hvex_foreach(vex->operands, scanvex) {
		if(nb>0) fprintf(F, " %s ", opstr);
		hvex_fprint(F, scanvex);
		nb++;
	}
	fprintf(F, ")");
}
// Print stuff like that for AND: and(op1, op2, op3)
void hvex_fprint_opbefore(FILE* F, const char* opstr, hvex_t* vex) {
	unsigned nb = 0;
	fprintf(F, "%s(", opstr);
	hvex_foreach(vex->operands, scanvex) {
		if(nb>0) fprintf(F, ", ");
		hvex_fprint(F, scanvex);
		nb++;
	}
	fprintf(F, ")");
}

// Generic callback functions for models

void hvex_fprint_opbetween_cb(FILE* F, hvex_t* vex) {
	char* sym = vex->model->sym_fprint!=NULL ? vex->model->sym_fprint : vex->model->name;
	hvex_fprint_opbetween(F, sym, vex);
}
void hvex_fprint_opbefore_cb(FILE* F, hvex_t* vex) {
	char* sym = vex->model->sym_fprint!=NULL ? vex->model->sym_fprint : vex->model->name;
	hvex_fprint_opbefore(F, sym, vex);
}

// Callback functions for specific models

static void hvex_fprint_vector(FILE* F, hvex_t* vex) {
	fprintf(F, "%s", (char*)vex->data);
	hvex_fprint_boundsonly_par(F, vex);
}
static void hvex_fprint_literal(FILE* F, hvex_t* vex) {
	fprintf(F, "\"%s\"", (char*)vex->data);
	hvex_fprint_boundsonly_par(F, vex);
}

static void hvex_fprint_index(FILE* F, hvex_t* vex) {
	fprintf(F, "%s", (char*)vex->data);
	hvex_fprint_boundsonly_par(F, vex);
	fprintf(F, "[");
	hvex_fprint(F, vex->operands);
	fprintf(F, "]");
}
static void hvex_fprint_resize(FILE* F, hvex_t* vex) {
	fprintf(F, "resize(%u->%u, ", vex->operands->width, vex->width);
	hvex_fprint(F, vex->operands);
	fprintf(F, ")");
}
static void hvex_fprint_repeat(FILE* F, hvex_t* vex) {
	fprintf(F, "repeat(%u, ", vex->width);
	hvex_fprint(F, vex->operands);
	fprintf(F, ")");
}

static void hvex_fprint_string(FILE* F, hvex_t* vex) {
	fprintf(F, "string##\"%s\"", (char*)vex->data);
}
static void hvex_fprint_function(FILE* F, hvex_t* vex) {
	char buf[strlen(vex->data) + 20];
	sprintf(buf, "function##%s", (char*)vex->data);
	hvex_fprint_opbefore(F, buf, vex);
}

static void hvex_fprint_asg(FILE* F, hvex_t* vex) {
	hvex_fprint(F, vex->operands);
	fprintf(F, " = ");
	hvex_fprint(F, vex->operands->next);
}
static void hvex_fprint_asg_en(FILE* F, hvex_t* vex) {
	hvex_fprint(F, vex->operands);
	fprintf(F, " = ");
	hvex_fprint(F, vex->operands->next);
	fprintf(F, " COND ");
	hvex_fprint(F, vex->operands->next->next);
}
static void hvex_fprint_asg_addr(FILE* F, hvex_t* vex) {
	fprintf(F, "%s", (char*)vex->operands->data);
	hvex_fprint_boundsonly_par(F, vex);
	fprintf(F, "[");
	hvex_fprint(F, vex->operands->next->next);
	fprintf(F, "]");
	fprintf(F, " = ");
	hvex_fprint(F, vex->operands->next);
}
static void hvex_fprint_asg_addr_en(FILE* F, hvex_t* vex) {
	fprintf(F, "%s", (char*)vex->operands->data);
	hvex_fprint_boundsonly_par(F, vex);
	fprintf(F, "[");
	hvex_fprint(F, vex->operands->next->next);
	fprintf(F, "]");
	fprintf(F, " = ");
	hvex_fprint(F, vex->operands->next);
	fprintf(F, " COND ");
	hvex_fprint(F, vex->operands->next->next->next);
}

static void hvex_fprint_array(FILE* F, hvex_t* vex) {
	fprintf(F, "ARRAY ");
	hvex_fprint(F, vex->operands);
	fprintf(F, ", ");
	hvex_fprint(F, vex->operands->next);
}



//==============================================
// Cropping, resizing
//==============================================

// Note: VECTOR model doesn't need a special cropping function
static void hvex_croprel_literal(hvex_t* vex, unsigned left, unsigned right) {
	char* lit = hvex_lit_getval(vex);
	if(right==0) {
		vex->data = stralloc(lit + left);
	}
	else {
		char buf[vex->width + 1];
		strcpy(buf, lit);
		buf[vex->width - right] = 0;
		vex->data = stralloc(buf + left);
	}
	vex->width -= left + right;
	vex->left = vex->width - 1;
	vex->right = 0;
}

// Note: INDEX model doesn't need a special cropping function
static void hvex_croprel_concat(hvex_t* vex, unsigned left, unsigned right) {

	// Crop right
	if(right > 0) {
		hvex_t* vexop = vex->operands;
		while(vexop->next!=NULL) vexop = vexop->next;
		unsigned cur_right = right;
		while(vexop->width <= cur_right) {
			cur_right -= vexop->width;
			hvex_t* vexop_prev = vexop->prev;
			if(vexop_prev!=NULL) vexop_prev->next = NULL;
			hvex_free(vexop);
			vexop = vexop_prev;
		}
		if(cur_right > 0) hvex_croprel(vexop, 0, cur_right);
	}

	// Crop left
	if(left > 0) {
		unsigned cur_left = left;
		while(vex->operands->width <= cur_left) {
			cur_left -= vex->operands->width;
			hvex_t* vexop_next = vex->operands->next;
			if(vexop_next!=NULL) vexop_next->prev = NULL;
			hvex_free(vex->operands);
			vex->operands = vexop_next;
		}
		if(cur_left > 0) hvex_croprel(vex->operands, cur_left, 0);
	}

	// Update width
	vex->width -= left + right;
	vex->left = vex->width - 1;
	assert(vex->right==0);
}
// Note: RESIZE model doesn't need a special cropping function (handled in simp)
static void hvex_croprel_repeat(hvex_t* vex, unsigned left, unsigned right) {
	vex->width -= left + right;
	vex->left = vex->width - 1;
	vex->right = 0;
	if(vex->width==1) {
		// This is no longer a REPEAT operation
		hvex_t* vexop = vex->operands;
		vex->operands = NULL;
		hvex_replace_free(vex, vexop);
	}
}

// Note: STRING model can't have a cropping function
// Note: FUNCTION model doesn't need a special cropping function
// Note: DIVQ, DIVR models don't need a special cropping function

// Note: ADD, SUB, NEG, MUL models don't need a special cropping function (handled in simp)

// Note: Comparison models can't have a cropping function

// Note: SHL, SHR, ROTL, ROTR models don't need a special cropping function (handled in simp)

static void hvex_croprel_logic_all(hvex_t* vex, unsigned left, unsigned right) {
	// Update indexes
	vex->right = 0;
	vex->left -= left + right;
	vex->width -= left + right;
	// Crop the operands
	hvex_foreach(vex->operands, vexop) hvex_croprel(vexop, left, right);
	// Launch simplification
	hvex_simp_noops(vex);
}

// Note: ASG, ASG_EN models can't have a cropping function

static void hvex_croprel_muxb(hvex_t* vex, unsigned left, unsigned right) {
	// Update indexes
	assert(vex->right==0);
	vex->width -= left + right;
	vex->left = vex->width - 1;
	// Crop the data operands, don't touch the selection operand
	hvex_foreach(vex->operands->next, vexop) {
		hvex_croprel(vexop, left, right);
	}
}

static void hvex_croprel_muxdec(hvex_t* vex, unsigned left, unsigned right) {
	// Update indexes
	assert(vex->right==0);
	vex->width -= left + right;
	vex->left = vex->width - 1;
	// Crop the data operands, don't touch the condition operands
	hvex_foreach(vex->operands, vexop) {
		assert(vexop->model==HVEX_MUXDECIN);
		// Also update the indexes of the operand
		vexop->width = vex->width;
		vexop->left  = vex->left;
		vexop->right = vex->right;
		// Crop the data operand
		hvex_t* vexop_data = vexop->operands->next;
		hvex_croprel(vexop_data, left, right);
	}
}

// A phony cropping function for the models who can't have cropping function
static void hvex_croprel_error_cantcrop(hvex_t* vex, unsigned left, unsigned right) {
	printf("Error: HVEX model '%s' can't have a cropping function.\n", vex->model->name);
	abort();
}

// Main cropping functions
void hvex_croprel(hvex_t* vex, unsigned left, unsigned right) {
	if(left==0 && right==0) return;
	// Note: Individually check left and right to detect overflows that would passe with just checking left + right
	assert(left < vex->width && right < vex->width && left + right < vex->width);
	// If the model declares a dedicated cropping function, use it
	if(vex->model->func_croprel!=NULL) {
		vex->model->func_croprel(vex, left, right);
	}
	// Generic cropping method:
	// Update indexes and use the simplification callback
	else {
		vex->right += right;
		vex->left  -= left;
		vex->width -= left + right;
		hvex_simp_noops(vex);
	}
}

// Perform signed/unsigned extension. Add the specified number of bits.
void hvex_extend(hvex_t* vex, unsigned bits_nb) {
	if(bits_nb==0) return;

	if(vex->model==HVEX_LITERAL) {
		char* lit = vex->data;
		char pad = vex->pad;
		if(pad==0) pad = lit[0];
		char buf[vex->width + bits_nb + 1];
		for(unsigned i=0; i<bits_nb; i++) buf[i] = pad;
		for(unsigned i=0; i<vex->width; i++) buf[bits_nb + i] = lit[i];
		buf[vex->width + bits_nb] = 0;
		vex->data = stralloc(buf);
		vex->width += bits_nb;
		vex->left += bits_nb;
		return;
	}

	else if(vex->model==HVEX_RESIZE) {
		if(vex->pad!=0) {
			// Concat some literal bits
			hvex_addop_head(vex, hvex_new_move(vex));
			hvex_addop_head(vex, hvex_newlit_repeat(vex->pad, bits_nb));
			vex->model = HVEX_CONCAT;
			// Adjust indexes
			vex->width += bits_nb;
			vex->left = vex->width - 1;
			vex->right = 0;
			// Another simplification
			hvex_simp_noops(vex);
			return;
		}
		if(vex->operands->pad==0) {
			// Here we want sign extension of a sign extension, so it's simple
			vex->width += bits_nb;
			vex->left += bits_nb;
			return;
		}
	}

	else if(vex->model==HVEX_REPEAT) {
		if(hvex_is_signed(vex)==true) {
			// Just modify the indexes, this is still a sign extension
			vex->width += bits_nb;
			vex->left += bits_nb;
		}
		else {
			// Concat some literal bits
			hvex_addop_head(vex, hvex_new_move(vex));
			hvex_addop_head(vex, hvex_newlit_repeat(vex->pad, bits_nb));
			vex->model = HVEX_CONCAT;
			// Adjust indexes
			vex->width += bits_nb;
			vex->left = vex->width - 1;
			vex->right = 0;
			// Another simplification
			hvex_simp_noops(vex);
		}
		return;
	}

	// Default case: Create a model RESIZE
	hvex_addop_head(vex, hvex_new_move(vex));
	vex->model = HVEX_RESIZE;
	vex->width += bits_nb;
	vex->left = vex->width - 1;
	vex->right = 0;
	// And a last simplification pass
	hvex_simp_noops(vex);
}
// Crop or extend from the left side. The given width is the wanted width.
void hvex_resize(hvex_t* vex, unsigned width) {
	if(vex->width==width) return;
	if(vex->width > width) {
		hvex_croprel(vex, vex->width - width, 0);
	}
	else hvex_extend(vex, width - vex->width);
}

// Return the number of bits that can be cropped left
// Important: assume the vex is simplified
unsigned hvex_shrinks_getcrop(hvex_t* vex) {
	if(vex->pad!=0) return 0;

	if(vex->model==HVEX_LITERAL) {
		char* lit = vex->data;
		unsigned count = 0;
		while(lit[0]==lit[1]) { lit++; count++; }
		return count;
	}

	else if(vex->model==HVEX_CONCAT) {
		// Note: only the first operand needs to be checked because all is supposed to already simplified
		return hvex_shrinks_getcrop(vex->operands);
	}

	else if(vex->model==HVEX_RESIZE) {
		hvex_t* vexop = vex->operands;
		if(vexop->pad!=0) return 0;
		return vex->width - vexop->width;
	}

	else if(vex->model==HVEX_REPEAT) {
		return vex->width - 1;
	}

	return 0;
}
// Crop left if possible
unsigned hvex_shrinks(hvex_t* vex) {
	if(vex->width < 2) return 0;

	unsigned cropwidth = hvex_shrinks_getcrop(vex);
	if(cropwidth==0) return 0;

	if(cropwidth==vex->width) cropwidth--;
	if(cropwidth==0) return 0;

	// Crop
	hvex_croprel(vex, cropwidth, 0);

	return cropwidth;
}

// FIXME For CONCAT, shrinking could be done iteratively, when some operands are removed
//   Or no, because the vex is supposed to be already simplified before shrinking?
// => Missing explanations and specifications.

// Return the number of bits that can be cropped left for extension
// with a given char: '0', '1', '-' (includes unsigned extension)
// Important: assume the vex is simplified
unsigned hvex_shrinku_getcrop(hvex_t* vex, char bit) {
	if(vex->pad!=bit && vex->pad!='-') return 0;

	if(vex->model==HVEX_LITERAL) {
		char* lit = hvex_lit_getval(vex);
		// Count the number of leading bits that can be cropped
		unsigned nbcrop = 0;
		for(unsigned i=0; i<vex->width; i++) {
			if(lit[i]=='-' || lit[i]==bit) nbcrop++;
			else break;
		}
		return nbcrop;
	}

	else if(vex->model==HVEX_CONCAT) {
		// Note: only the first operand needs to be checked because all is supposed to already simplified
		return hvex_shrinku_getcrop(vex->operands, bit);
	}

	else if(vex->model==HVEX_RESIZE) {
		hvex_t* vexop = vex->operands;
		if(vexop->pad!=bit && vexop->pad!='-') return 0;
		return vex->width - vexop->width;
	}

	return 0;
}
// Crop left if possible
unsigned hvex_shrinku(hvex_t* vex) {
	if(vex->width < 2) return 0;

	// Get the crop width
	unsigned cropwidth = hvex_shrinku_getcrop(vex, '0');
	if(cropwidth==0) return 0;

	if(cropwidth==vex->width) cropwidth--;
	if(cropwidth==0) return 0;

	// Crop
	hvex_croprel(vex, cropwidth, 0);

	return cropwidth;
}

// Crop left if possible
unsigned hvex_shrink(hvex_t* vex) {
	if(vex->width < 2) return 0;

	unsigned cropwidth = 0;
	if(vex->pad==0) cropwidth = hvex_shrinks_getcrop(vex);
	else cropwidth = hvex_shrinku_getcrop(vex, vex->pad);

	if(cropwidth==vex->width) cropwidth--;
	if(cropwidth==0) return 0;

	// Crop
	hvex_croprel(vex, cropwidth, 0);

	return cropwidth;
}



//==============================================
// Simplification
//==============================================

// Note: VECTOR model doesn't need a simplification function
// Note: LITERAL model doesn't need a simplification function

// Note: INDEX model doesn't need a simplification function
static unsigned hvex_simp_concat(hvex_t* vex) {
	unsigned count = 0;

	assert(vex->right == 0);

	hvex_t* vexop = vex->operands;
	while(vexop!=NULL) {

		// Merge operands that are LITERAL
		if(vexop->model==HVEX_LITERAL && vexop->prev!=NULL) {
			hvex_t* prevop = vexop->prev;
			if(prevop->model==HVEX_LITERAL) {
				assert(strlen(prevop->data)==prevop->width && strlen(vexop->data)==vexop->width);
				// Keep the current operand and remove the previous one
				char buf[prevop->width + vexop->width + 1];
				sprintf(buf, "%s%s", (char*)prevop->data, (char*)vexop->data);
				vexop->data = stralloc(buf);
				vexop->width += prevop->width;
				vexop->left = vexop->width - 1;
				hvex_remop(prevop);
				// Increment the number of things done
				count++;
			}
			goto NEXTOP;
		}

		// Flatten operands that are CONCAT
		else if(vexop->model==HVEX_CONCAT) {
			hvex_t* operands = vexop->operands;
			vexop->operands = NULL;
			hvex_addmultop_replace(vexop, operands);
			vexop = operands;
			// Increment the number of things done
			count++;
			continue;
		}

		// Merge VECTOR operands from the same symbol and that are contiguous
		else if(vexop->model==HVEX_VECTOR && vexop->prev!=NULL) {
			hvex_t* prevop = vexop->prev;
			if(prevop->model==HVEX_VECTOR && vexop->data==prevop->data && vexop->left+1==prevop->right) {
				// Keep the current operand and remove the previous one
				vexop->left = prevop->left;
				vexop->width += prevop->width;
				hvex_remop(prevop);
				// Increment the number of things done
				count++;
			}
			goto NEXTOP;
		}

		// Merge INDEX operands from the same symbol, with the same address, and that are contiguous
		else if(vexop->model==HVEX_INDEX && vexop->prev!=NULL) {
			hvex_t* prevop = vexop->prev;
			if(prevop->model==HVEX_INDEX && vexop->data==prevop->data && vexop->left+1==prevop->right) {
				if(hvex_cmp(vexop->operands, prevop->operands)==0) {
					// Keep the current operand and remove the previous one
					vexop->left = prevop->left;
					vexop->width += prevop->width;
					hvex_remop(prevop);
					// Increment the number of things done
					count++;
				}
			}
			goto NEXTOP;
		}

		// FIXME TODO Here, add the expensive optimizations present in old Vexmisc

		NEXTOP:
		vexop = vexop->next;
	}

	// If there is only one operand, the CONCAT operation is no longer needed
	if(vex->operands->next==NULL) {
		hvex_t* vexop = vex->operands;
		vex->operands = NULL;
		hvex_replace_free(vex, vexop);
		// Increment the number of things done
		count++;
	}

	// FIXME TODO Merge REPEAT and RESIZE (expensive operation?)
	// FIXME TODO Merge operands to create RESIZE (expensive operation?)
	// FIXME TODO Merge operands to create REPEAT

	return count;
}
static unsigned hvex_simp_resize(hvex_t* vex) {
	hvex_t* vexop = vex->operands;
	unsigned count = 0;

	// Note: Don't assume right=0 because this function is called for cropping

	// Check if the vex result is out of bounds, replace by a repeat of the sign bit
	if(vex->right > 0) {
		if(hvex_is_signed(vexop)==true) {
			if(vex->right >= vexop->width - 1) {
				// Replace by a REPEAT of the sign bit
				hvex_croprel(vexop, 0, vexop->width - 1);
				vex->model = HVEX_REPEAT;
				vex->left -= vex->right;
				vex->right = 0;
				return 1;
			}
		}
		else {
			if(vex->right > vexop->width - 1) {
				// Replace by a literal
				char pad = vexop->pad;
				hvex_free_ops(vex);
				hvex_lit_setrepeat(vex, pad, vex->width);
				return 1;
			}
		}
	}

	// Check if it is more a crop than a resize
	//   |  resize  |
	// |    operand    |
	if(vex->left <= vexop->width - 1) {
		// Replace by the cropped operand
		hvex_croprel(vexop, vexop->width-1 - vex->left, vex->right);
		vex->operands = NULL;
		hvex_replace_free(vex, vexop);
		return 1;
	}

	// Crop operand so that right = 0
	if(vex->right > 0) {
		hvex_croprel(vexop, 0, vex->right);
		vex->left -= vex->right;
		vex->right = 0;
		// increment the number of things done
		count ++;
	}

	// From here we have vex->right = 0, and still vex->width > vexop->width
	unsigned bits_nb = vex->width - vexop->width;

	// Handle when the operand is literal
	char pad = vexop->pad;
	if(vexop->model==HVEX_LITERAL) {
		char* lit = hvex_lit_getval(vexop);
		if(pad==0) pad = lit[0];
		char buf[vex->width + 1];
		for(unsigned i=0; i<bits_nb; i++) buf[i] = pad;
		for(unsigned i=0; i<vexop->width; i++) buf[bits_nb + i] = lit[i];
		buf[vex->width] = 0;
		// Change the current model to LITERAL
		hvex_free_ops(vex);
		vex->model = HVEX_LITERAL;
		hvex_lit_setval(vex, stralloc(buf));
		return 1;
	}

	// From here the operand is not literal.

	// Handle the case where the padding char is known.
	if(pad!=0) {
		// Concatenate some padding bits
		hvex_addop_head(vex, hvex_newlit_repeat(pad, bits_nb));
		vex->model = HVEX_CONCAT;
		// Another little simplification
		hvex_simp_noops(vex);
		return 1;
	}

	// From here the operand is not literal, and it is a signed extension.

	if(vexop->model==HVEX_RESIZE) {
		// We have a vex=resize(vexop=resize(op2)) and both op2 and the second resize have sign extension
		// Remove the second resize and op2 becomes the operand of the first resize
		hvex_t* vexop2 = hvex_unlinkop(vexop->operands);
		assert(vexop2->pad==0);  // It should already have been simplified
		hvex_remop(vexop);
		hvex_addop_head(vex, vexop2);
		return 1;
	}

	else if(vexop->model==HVEX_REPEAT) {
		// We have a resize(repeat(op2)) and the repeat is a sign extension
		// Remove the repeat and op2 becomes the operand of the first resize
		hvex_t* vexop2 = hvex_unlinkop(vexop->operands);
		hvex_remop(vexop);
		hvex_addop_head(vex, vexop2);
		// Another little simplification
		hvex_simp_noops(vex);
		return 1;
	}

	return count;
}
static unsigned hvex_simp_repeat(hvex_t* vex) {
	hvex_t* vexop = vex->operands;

	if(vex->width==1) {
		vex->operands = NULL;
		hvex_replace_free(vex, vexop);
		return 1;
	}

	if(vexop->model==HVEX_LITERAL) {
		char* lit = hvex_lit_getval(vexop);
		hvex_free_ops(vex);
		hvex_lit_setrepeat(vex, lit[0], vex->width);
		return 0;
	}

	return 0;
}

// Note: STRING model doesn't need a simplification function
// Note: FUNCTION model doesn't need a simplification function

// Return the expression: A[31] and (A[3-0] != 0)
// Or, return NULL if the input Vex is unsigned
// This is used to simplify DIV Q/R when data is signed
static hvex_t* hvex_simp_divqr_specialsign(hvex_t* dataop, unsigned nb_zero) {
	hvex_t* addbit = NULL;
	if(dataop->pad=='1') {
		if(dataop->width < nb_zero) addbit = hvex_newlit_bit('1');
		else {
			hvex_t* dataright = hvex_dup(dataop);
			hvex_croprel(dataright, dataop->width - nb_zero, 0);
			addbit = hvex_newmodel_op2(HVEX_NE, 1, dataright, hvex_newlit_repeat('0', nb_zero));
		}
	}
	else if(dataop->pad==0) {
		hvex_t* dataright = hvex_dup(dataop);
		hvex_resize(dataright, nb_zero);
		hvex_t* vex_ne = hvex_newmodel_op2(HVEX_NE, 1, dataright, hvex_newlit_repeat('0', nb_zero));
		hvex_t* vex_msb = hvex_dup(dataop);
		hvex_croprel(vex_msb, 0, vex_msb->width-1);
		addbit = hvex_newmodel_op2(HVEX_AND, 1, vex_msb, vex_ne);
	}
	return addbit;
}
static unsigned hvex_simp_divqr_right0(hvex_t* vex) {
	hvex_t* dataop = vex->operands;
	hvex_t* divop = vex->operands->next;

	// IMPORTANT:
	// - Before entering this function, it must already be known that data!=0 and div!=0
	// - This function doesn't re-launch simplify to avoid infinite recursion

	// Handle when dividend (here data) and divisor (even signed) have '0' at right, simplify them
	unsigned dataop_nbz = hvex_getnbrightbit(dataop, '0', true);
	unsigned divop_nbz = hvex_getnbrightbit(divop, '0', true);

	unsigned nbz = GetMin(dataop_nbz, divop_nbz);

	if(nbz==0) return 0;

	// Here, the data and the divisor can both be cropped right.
	hvex_croprel(dataop, 0, nbz);
	hvex_croprel(divop, 0, nbz);

	// For DIVR:         data = div * quo + rem
	// If multiple of 8: data2 * 8 = div2 * 8 * quo + rem
	// Hence rem is a multiple of 8. So, the right zeroes must be concat to right of result.

	if(vex->model==HVEX_DIVR) {
		// Append the zeroes at right of result
		hvex_t* newlit = hvex_newlit_repeat('0', nbz);
		hvex_t* newvex = hvex_new_move(vex);
		newvex->width -= nbz;
		newvex->left -= nbz;
		// Set model to concat
		vex->model = HVEX_CONCAT;
		vex->left -= vex->right;
		vex->right = 0;
		// Add operands
		hvex_addop_head(vex, newlit);
		hvex_addop_head(vex, newvex);
	}

	return 1;
}
static unsigned hvex_simp_divqr(hvex_t* vex) {
	hvex_t* dataop = vex->operands;
	hvex_t* divop = vex->operands->next;
	unsigned count = 0;

	// Shrink the operands
	count += hvex_shrink(dataop);
	count += hvex_shrink(divop);

	// Crop to the width of the data
	// FIXME TODO Handle similar simplifications when vex or operands are signed
	if(hvex_is_unsigned(vex)==true && hvex_is_unsigned(dataop)==true && hvex_is_unsigned(divop)==true) {
		// If the result is out of bounds, the result is zero
		if(vex->right >= dataop->width) {
			hvex_free_ops(vex);
			hvex_lit_setrepeat(vex, '0', vex->width);
			return 1;
		}

		// FIXME TODO: if data is a power of 2, result width = data width - 1 in these conditions:
		// for DIVR, because the result is always < data,
		// for DIVQ, if we can guarantee divisor > 1, because then result < data

		// The result can't be wider than the data operand
		if(vex->left+1 > dataop->width) {
			count++;
			unsigned bits_nb = vex->left+1 - dataop->width;
			// Concat some zeroes at left of result
			hvex_t* newlit = hvex_newlit_repeat('0', bits_nb);
			hvex_t* newvex = hvex_new_move(vex);
			newvex->width -= bits_nb;
			newvex->left -= bits_nb;
			// The original VEX becomes a CONCAT
			vex->model = HVEX_CONCAT;
			hvex_set_unsigned(vex);
			hvex_addop_head(vex, newvex);
			hvex_addop_head(vex, newlit);
			// Simplify again
			hvex_simp(vex);
			return 1;
		}
	}

	// Check if dataop is zero: result is zero
	if(dataop->pad!='1' && dataop->model==HVEX_LITERAL) {
		if(hvex_lit_getsamebit(dataop)=='0') {
			// Replace the vex by all '0'
			hvex_free_ops(vex);
			hvex_lit_setrepeat(vex, '0', vex->width);
			return 1;
		}
	}

	// Handle last-resort simplifications when the divisor is not literal
	if(divop->model!=HVEX_LITERAL) {
		unsigned z = hvex_simp_divqr_right0(vex);
		if(z!=0) {
			count += hvex_simp(vex);
			count += z;
		}
		return count;
	}

	// From here we know the divisor is a literal

	// Replace the bits '-' by '0'
	count += hvex_lit_replace_dc(divop, '0');
	char* lit = hvex_lit_getval(divop);

	// Count the number of right '0' and the number of '1' to get the corresponding power of 2
	unsigned nb_zero = 0;
	unsigned nb_one = 0;
	for(int i=divop->width-1; i>=0; i--) {
		if(lit[i]=='0') { if(nb_one==0) nb_zero++; }
		else { nb_one ++; if(nb_one > 1) break; }
	}

	// Detect division by zero
	if(nb_one==0) {
		printf("ERROR: Your design contains a division by zero.\n");
		abort();
	}

	// Handle when the divisor is a power of 2
	// Follow the C standard for signedness of remainder: same sign as dividend
	// So if if the data is signed and negative, and not multiple of the divisor, then add 1 to the quotient
	// Possible formulas:
	//   (1)  (A + repeat(4, A[31])) >> 4
	//   (2)  (A >> 4) + (A[31] and (A[3-0] != 0))
	// Here we implement the formula 2.
	// For the remainder, the formula is:
	//   repeat(28, bit) & A[3-0]  where  bit = A[31] and (A[3-0] != 0)
	if(nb_one==1) {
		hvex_remop(divop);
		if(vex->model==HVEX_DIVQ) {
			if(nb_zero==0) {
				// Division by 1: the quotient is the data
				hvex_unlinkop(dataop);
				hvex_resize(dataop, vex->left+1);
				hvex_croprel(dataop, 0, vex->right);
				hvex_replace_free(vex, dataop);
			}
			else {
				// Get the correction to apply when the data is signed
				hvex_t* addbit = hvex_simp_divqr_specialsign(dataop, nb_zero);
				// Shift the data operand
				hvex_unlinkop(dataop);
				hvex_resize(dataop, vex->left+1 + nb_zero);
				hvex_croprel(dataop, 0, nb_zero + vex->right);
				// Create an ADD operation for the correction about signed numbers
				if(addbit==NULL) {
					hvex_replace_free(vex, dataop);
				}
				else {
					hvex_t* vex_add = hvex_newmodel_op2(HVEX_ADD, vex->width, dataop, addbit);
					hvex_replace_free(vex, vex_add);
				}
				// The vex has profoundly changed, simplify again
				hvex_simp(vex);
			}
		}
		else if(vex->model==HVEX_DIVR) {
			if(nb_zero==0) {
				// Division by 1: the remainder is 0
				hvex_free_ops(vex);
				hvex_lit_setrepeat(vex, '0', vex->width);
			}
			else {
				// The result is a selection of bits
				hvex_unlinkop(dataop);
				if(nb_zero >= dataop->width) {
					hvex_resize(dataop, vex->left+1);
					hvex_croprel(dataop, 0, vex->right);
					hvex_replace_free(vex, dataop);
				}
				else {
					// Get the correction to apply when data is signed
					hvex_t* addbit = hvex_simp_divqr_specialsign(dataop, nb_zero);
					// Replace
					if(addbit==NULL) {
						hvex_set_unsigned(dataop);
						hvex_resize(dataop, nb_zero);
						hvex_resize(dataop, vex->left+1);
						hvex_croprel(dataop, 0, vex->right);
						hvex_replace_free(vex, dataop);
					}
					else {
						unsigned datawidth = dataop->width;
						hvex_resize(dataop, nb_zero);
						hvex_t* vex_concat = hvex_newmodel_op2(HVEX_CONCAT, datawidth,
							hvex_newmodel_op1(HVEX_REPEAT, datawidth - nb_zero, addbit), dataop
						);
						hvex_resize(vex_concat, vex->left+1);
						hvex_croprel(vex_concat, 0, vex->right);
						hvex_replace_free(vex, vex_concat);
					}
				}
				// The vex has profoundly changed, simplify again
				hvex_simp(vex);
			}
		}
		else {
			abort();
		}
		return 1;
	}

	// Checks on the data operand
	if(dataop->model!=HVEX_LITERAL || (dataop->pad!='0' && dataop->pad!='-')) {
		return count + hvex_simp_divqr_right0(vex);
	}

	// From here we know that both the dividend (data) and the divisor are unsigned literals
	// So we can compute the result

	// Replace the bits '-' by '0'
	hvex_lit_replace_dc(dataop, '0');

	// Shrink again, in case there were bits '-'
	hvex_shrink(dataop);
	hvex_shrink(divop);

	char* litdata = hvex_lit_getval(dataop);
	char* litdiv = hvex_lit_getval(divop);

	// Handle the situation where divisor > data
	int z = hvex_calc_cmpu(divop->width, litdiv, dataop->width, litdata);
	assert(z!='-');
	if(z>0) {
		// The divisor is greater than the dividend (data)
		if(vex->model==HVEX_DIVQ) {
			// The quotient is zero
			hvex_unlinkop(dataop);
			hvex_free_ops(vex);
			hvex_lit_setrepeat(vex, '0', vex->width);
		}
		else if(vex->model==HVEX_DIVR) {
			// The remainder is the data. Transform the node into RESIZE.
			hvex_remop(divop);
			vex->model = HVEX_RESIZE;
			vex->left = vex->width - 1;
			vex->right = 0;
		}
		else {
			abort();
		}
		// Simplify the result as it has profoundly changed
		hvex_simp_noops(vex);
		return 1;
	}

	// Here divisor <= data. Iteratively subtract the divisor (shifted).

	// A copy of the data, to work into. It will contain the remainder.
	char bufdata[dataop->width + 1];
	hvex_calc_fillbuf(dataop->width, bufdata, dataop->width, litdata, '0');
	bufdata[dataop->width] = 0;
	// A buffer to hold the quotient, initialized to zero
	char bufquo[dataop->width + 1];
	hvex_calc_fillrepeat(dataop->width, bufquo, '0');
	bufquo[dataop->width] = 0;

	// Get the difference of bit width of the data and the divisor
	assert(dataop->width >= divop->width);
	unsigned diffbits = dataop->width - divop->width;

	// Launch iterative subtraction
	for(int i=diffbits; i>=0; i--) {
		// Consider the divisor, shifted left by i bits
		// Check whether the data, cropped right by i bits, is >= shifted divisor
		int z = hvex_calc_cmpu(divop->width, bufdata, divop->width, litdiv);
		assert(z!='-');
		if(z>=0) {
			// The part of data is >= divisor. Subtract.
			hvex_calc_sub(divop->width, bufdata, divop->width, litdiv, '0', '0');
			// Add '1' (shifted) to the quotient.
			bufquo[divop->width - i - 1] = '1';
		}
	}

	// A last check for debug
	assert(hvex_calc_cmpu(divop->width, bufdata, divop->width, litdiv) < 0);

	// Here we have the results
	// Keep the data operand, modify its content. The vex becomes a RESIZE.
	hvex_remop(divop);
	vex->model = HVEX_RESIZE;
	vex->left = vex->width - 1;
	vex->right = 0;
	if(vex->model==HVEX_DIVQ) {
		hvex_lit_setval(dataop, stralloc(bufquo));
	}
	else if(vex->model==HVEX_DIVR) {
		hvex_lit_setval(dataop, stralloc(bufdata));
	}
	else {
		abort();
	}
	// Simplify the result as it has profoundly changed
	hvex_simp_noops(vex);

	return 1;
}

static unsigned hvex_simp_add(hvex_t* vex) {
	unsigned count = 0;

	// Crop & shrink operands
	hvex_foreach(vex->operands, vexop) {
		if(vexop->width-1 > vex->left) {
			hvex_croprel(vexop, vexop->width-1 - vex->left, 0);
			count++;
		}
		count += hvex_shrink(vexop);
	}

	// List all literal operands, add their value
	chain_list* list_litops = NULL;
	hvex_t* litop = NULL;
	hvex_foreach(vex->operands, vexop) {
		if(vexop->model==HVEX_LITERAL) list_litops = addchain(list_litops, vexop);
	}
	unsigned nblit = ChainList_Count(list_litops);
	if(nblit == 1) {
		litop = list_litops->DATA;
	}
	else if(nblit >= 2) {
		unsigned bufsize = vex->left + 1;
		// Fill the array "res" with all '0'
		char res[bufsize + 1];
		hvex_calc_fillrepeat(bufsize, res, '0');
		res[bufsize] = 0;
		// Add all literal operands to the literal "res"
		foreach(list_litops, scanop) {
			hvex_t* vexop = scanop->DATA;
			hvex_calc_add(bufsize, res, vexop->width, vexop->data, vexop->pad, '0');
			// Free the operand, keep the first one
			if(litop==NULL) litop = vexop;
			else hvex_remop(vexop);
		}
		// Modify the operand that was kept
		litop->width = bufsize;
		litop->left = bufsize - 1;
		litop->right = 0;
		litop->data = stralloc(res);
		// Increment the number of things done
		count++;
	}
	freechain(list_litops);

	// If the literal operand is zero, remove it (only if there are other operands)
	if(litop!=NULL && vex->operands->next!=NULL && hvex_lit_iszero(litop)==true) {
		hvex_remop(litop);
		litop = NULL;
		count++;
	}

	// If only one operand remains, replace the vex by that operand
	if(vex->operands->next==NULL) {
		hvex_t* vexop = vex->operands;
		vex->operands = NULL;
		hvex_replace_resize_free(vex, vexop);
		// Simplify again
		hvex_simp(vex);
		return 1;
	}

	// Detect when all operands have some '0' at the right
	// Detect when one operand has less '0' than the others
	hvex_t* remain_op = NULL;
	unsigned all_right_zero = 0;
	unsigned rem_right_zero = 0;
	hvex_foreach(vex->operands, vexop) {
		unsigned nb = hvex_getnbrightbit(vexop, '0', false);

		// Handle when there is already a declared special operand (the one with fewer '0' than others)
		if(remain_op!=NULL) {
			if(nb > rem_right_zero) {
				all_right_zero = all_right_zero==0 ? nb : GetMin(all_right_zero, nb);
			}
			else if(nb < rem_right_zero) {
				// The special operand is moved into the category "others", with higher nb of '0'
				all_right_zero = rem_right_zero;
				// The special operand is now this one
				rem_right_zero = nb;
				remain_op = vexop;
			}
			else {
				// Same number of '0' as the special operand
				if(rem_right_zero==0) break;
				// The two operands are moved into the category "others"
				all_right_zero = nb;
				remain_op = NULL;
				rem_right_zero = 0;
			}
			continue;
		}

		// Here there is no special operand
		// Only one thing to do: the operand becomes the special operand in 2 situations :
		//   it is the first scanned operand, or its nb of '0' is lower than other operands
		if(all_right_zero==0 || nb < all_right_zero) {
			rem_right_zero = nb;
			remain_op = vexop;
			continue;
		}

	}  // Scan operands
	if(all_right_zero > 0) {
		// All operands can be cropped right by the number of '0' in the special operand
		// All non-special operands can be cropped more, that involves a CONCAT operation
		assert(all_right_zero < vex->left+1);

		// For debug
		//dbgprintf("Vex %p  -  ", vex); hvex_print_bn(vex);

		// Save original right index because a CONCAT with right>0 is not valid
		unsigned orig_right = vex->right;
		vex->right = 0;
		vex->width = vex->left + 1;
		// Move the original ADD into another vex
		hvex_t* old_add = hvex_new_move(vex);
		// The original vex becomes a CONCAT
		vex->model = HVEX_CONCAT;

		// Handle the special operand
		if(remain_op!=NULL) {
			if(rem_right_zero > 0) hvex_addop_head(vex, hvex_newlit_repeat('0', rem_right_zero));
			hvex_t* rem_dup = hvex_dup(remain_op);
			hvex_croprel(rem_dup, 0, rem_right_zero);
			hvex_resize(rem_dup, all_right_zero - rem_right_zero);
			hvex_addop_head(vex, rem_dup);
		}
		else {
			hvex_addop_head(vex, hvex_newlit_repeat('0', all_right_zero));
		}

		// Now force crop right of all operands of the ADD, and crop left the ADD itself
		hvex_foreach(old_add->operands, vexop) {
			if(vexop->width <= all_right_zero) hvex_resize(vexop, all_right_zero+1);
			hvex_croprel(vexop, 0, all_right_zero);
		}
		old_add->width -= all_right_zero;
		old_add->left -= all_right_zero;
		// And add the ADD as first operand
		hvex_addop_head(vex, old_add);

		// Update the indexes of the CONCAT
		if(orig_right > 0) hvex_croprel(vex, 0, orig_right);
		// Just launch a final simplification
		hvex_simp(vex);

		// For debug
		//hvex_checkabort(vex);

		return 1;
	}

	// Detect when conversion to ADDCIN is possible
	// List all operands, and the operands of those that are ADD
	// If one of these operands has width 1, and an appropriate signedness, use it as carry input
	// FIXME TODO Handle when the number of operands is arbitrary
	if(hvex_countops(vex)==2) {
		chain_list* list_opsadd = NULL;
		hvex_t* opAdd = NULL;

		if(vex->operands->model==HVEX_ADD) {
			opAdd = vex->operands;
			list_opsadd = addchain(list_opsadd, vex->operands->next);
		}
		else if(vex->operands->next->model==HVEX_ADD) {
			opAdd = vex->operands->next;
			list_opsadd = addchain(list_opsadd, vex->operands);
		}

		if(opAdd!=NULL && hvex_countops(opAdd)==2 && opAdd->width==vex->width && opAdd->right==0) {
			list_opsadd = addchain(list_opsadd, opAdd->operands);
			list_opsadd = addchain(list_opsadd, opAdd->operands->next);
		}
		else { freechain(list_opsadd); list_opsadd = NULL; }

		if(list_opsadd!=NULL) {
			hvex_t* opCin = NULL;
			foreach(list_opsadd, scan) {
				hvex_t* op = scan->DATA;
				if(op->width!=1 || op->pad==0 || op->pad=='1') continue;
				if(op->model==HVEX_LITERAL) { opCin = op; break; }
				if(opCin==NULL) opCin = op;
			}
			if(opCin!=NULL) {
				// Here a conversion to ADDCIN is possible
				// Unlink all operands, free the operand ADD
				foreach(list_opsadd, scan) hvex_unlinkop(scan->DATA);
				assert(opAdd->operands==NULL);
				hvex_remop(opAdd);
				assert(vex->operands==NULL);
				foreach(list_opsadd, scan) {
					hvex_t* op = scan->DATA;
					if(op==opCin) hvex_addop_tail(vex, op);
					else hvex_addop_head(vex, op);
				}
				// Replace the original model by ADDCIN and re-simplify
				vex->model = HVEX_ADDCIN;
				count += 1 + hvex_simp_noops(vex);
			}
		}

		// Clean
		freechain(list_opsadd);
	}

	return count;
}
static unsigned hvex_simp_sub(hvex_t* vex) {
	unsigned count = 0;

	// Crop & shrink operands
	hvex_foreach(vex->operands, vexop) {
		if(vexop->width-1 > vex->left) {
			hvex_croprel(vexop, vexop->width-1 - vex->left, 0);
			count++;
		}
		count += hvex_shrink(vexop);
	}

	hvex_t* vexop1 = vex->operands;
	hvex_t* vexop2 = vex->operands->next;

	// If operand is literal, just compute the result
	if(vexop1->model==HVEX_LITERAL && vexop2->model==HVEX_LITERAL) {
		char* lit1 = vexop1->data;
		char* lit2 = vexop2->data;
		unsigned bufsize = vex->left + 1;
		// Fill the array "res" with the first operand (extended)
		char res[bufsize + 1];
		hvex_calc_fillbuf(bufsize, res, vexop1->width, lit1, vexop1->pad);
		res[bufsize] = 0;
		// Compute the result in the array "res"
		hvex_calc_sub(bufsize, res, vexop2->width, lit2, vexop2->pad, '0');
		// Update indexes
		hvex_remop(vexop2);
		hvex_unlinkop(vexop1);
		vexop1->width = bufsize;
		vexop1->left = bufsize - 1;
		vexop1->right = 0;
		vexop1->data = stralloc(res);
		// Replace
		hvex_replace_resize_free(vex, vexop1);
		return 1;
	}

	// If the first operand is zero, remove it and change the operation to NEG
	if(hvex_iszero(vexop1)) {
		hvex_remop(vexop1);
		vex->model = HVEX_NEG;
		// Simplify again
		hvex_simp_noops(vex);
		return 1;
	}

	// If the second operand is zero, the result is the first operand
	if(hvex_iszero(vexop2)) {
		hvex_unlinkop(vexop1);
		hvex_free_ops(vex);
		hvex_replace_resize_free(vex, vexop1);
		// Simplify again
		hvex_simp(vex);
		return 1;
	}

	return count;
}
static unsigned hvex_simp_neg(hvex_t* vex) {
	hvex_t* vexop = vex->operands;
	unsigned count = 0;

	// Crop operand if needed
	if(vexop->width-1 > vex->left) {
		hvex_croprel(vexop, vexop->width-1 - vex->left, 0);
		count ++;
	}
	count += hvex_shrink(vexop);

	// If operand is literal, just compute the result
	if(vexop->model==HVEX_LITERAL) {
		char* lit = vexop->data;
		unsigned bufsize = vex->left + 1;
		// Fill the array "res" with all '0'
		char res[bufsize + 1];
		hvex_calc_fillrepeat(bufsize, res, '0');
		res[bufsize] = 0;
		// Compute the result in the array "res"
		hvex_calc_sub(bufsize, res, vexop->width, lit, vexop->pad, '0');
		// Update indexes
		vexop->width = bufsize;
		vexop->left = bufsize - 1;
		vexop->right = 0;
		vexop->data = stralloc(res);
		// Replace
		vex->operands = NULL;
		hvex_replace_resize_free(vex, vexop);
		return 1;
	}

	return count;
}
static unsigned hvex_simp_mul(hvex_t* vex) {
	unsigned count = 0;

	// If the result is only the MSB, it is a selection of the sign
	// Replace the multiplication by a XOR of the MSB of the operands
	if(vex->width==1) {
		unsigned totalwidth = 0;
		bool allsigned = true;
		hvex_foreach(vex->operands, vexop) {
			if(vexop->pad!=0) { allsigned = false; break; }
			totalwidth += vexop->width;
		}
		if(allsigned==true && vex->right == totalwidth-1) {
			vex->model = HVEX_XOR;
			// Crop the operands
			hvex_foreach(vex->operands, vexop) hvex_croprel(vexop, 0, vexop->width - 1);
			// Simplify again as the vex has changed
			hvex_simp(vex);
			return 1;
		}
	}

	// Crop & shrink operands
	hvex_foreach(vex->operands, vexop) {
		if(vexop->width-1 > vex->left) {
			hvex_croprel(vexop, vexop->width-1 - vex->left, 0);
			count++;
		}
		count += hvex_shrink(vexop);
	}

	// List the literal operands
	chain_list* list_litops = NULL;
	hvex_foreach(vex->operands, vexop) {
		if(vexop->model==HVEX_LITERAL) list_litops = addchain(list_litops, vexop);
	}
	// Compute the value of all literal operands
	hvex_t* litop = NULL;
	unsigned nblit = ChainList_Count(list_litops);
	if(nblit == 1) {
		litop = list_litops->DATA;
	}
	else if(nblit >= 2) {
		unsigned bufsize = vex->left + 1;

		// Create an array to hold the result, initialize to all '0' for debug
		char res[bufsize + 1];
		hvex_calc_fillrepeat(bufsize, res, '0');
		res[bufsize] = 0;
		// Create another result buffer, initialize to all '0' for debug
		char resbuf[bufsize + 1];
		hvex_calc_fillrepeat(bufsize, resbuf, '0');
		resbuf[bufsize] = 0;
		// Create a buffer to hold operands + sign extension, initialize to all '0' for debug
		char opbuf[bufsize + 1];
		hvex_calc_fillrepeat(bufsize, opbuf, '0');
		opbuf[bufsize] = 0;
		// Create a buffer to hold all '-' in case there are '-' bits on operands
		char dcbuf[bufsize + 1];
		hvex_calc_fillrepeat(bufsize, dcbuf, '-');
		dcbuf[bufsize] = 0;

		// Fill the result buffer with the first operand
		litop = list_litops->DATA;
		hvex_calc_fillbuf(bufsize, res, litop->width, litop->data, litop->pad);

		// Multiply the literal "res" by all other literal operands
		foreach(list_litops->NEXT, scanop) {
			hvex_t* vexop = scanop->DATA;
			char* lit = hvex_lit_getval(vexop);

			// Copy the operand in a buffer to apply sign extension if needed
			hvex_calc_fillbuf(bufsize, opbuf, vexop->width, lit, vexop->pad);
			// Set the next result to all zero
			hvex_calc_fillrepeat(bufsize, resbuf, '0');

			// Perform the multiplication
			for(unsigned i=0; i<bufsize; i++) {
				char bit = opbuf[i];
				if(bit=='0') continue;

				if(bit=='1') {  // Add the previous result, shifted
					hvex_calc_add(i+1, resbuf, bufsize, res, '0', '0');
				}
				else {  // The bit is '-', add "don't care" bits
					hvex_calc_add(i+1, resbuf, bufsize, dcbuf, '-', '0');
				}

			}  // Scan all bits of the operand

			// Copy the new result in the main result buffer
			strcpy(res, resbuf);

			// Free the operand
			hvex_remop(vexop);
		}  // Scan all other literal operands

		// Modify the operand that was kept
		litop->width = bufsize;
		litop->left = bufsize - 1;
		litop->right = 0;
		hvex_lit_setval(litop, stralloc(res));
		// Increment the number of things done
		count++;
	}
	freechain(list_litops);

	// If litop = 0, the result is 0.
	if(litop!=NULL && hvex_lit_iszero(litop)==true) {
		// The operand is assumed to be 0. The result is zero.
		hvex_free_ops(vex);
		hvex_lit_setrepeat(vex, '0', vex->width);
		return 1;
	}

	// Scan all operands, count the total number of '0' at the right
	//   Crop them from the operands and concat them to the result
	unsigned nbright0 = 0;
	hvex_foreach(vex->operands, vexop) {
		unsigned local_nb = hvex_getnbrightbit(vexop, '0', true);
		assert(local_nb < vexop->width);
		if(local_nb > 0) {
			hvex_croprel(vexop, 0, local_nb);
			nbright0 += local_nb;
		}
	}
	if(nbright0 > 0) {
		// If all '0' lead beyond the cropping left of the result, the vex becomes all '0'
		if(nbright0 >= vex->left + 1) {
			hvex_free_ops(vex);
			hvex_lit_setrepeat(vex, '0', vex->width);
			return 1;
		}
		// If all '0' remain inside the cropping right of the result, just crop more the vex
		if(nbright0 <= vex->right) {
			vex->left -= nbright0;
			vex->right -= nbright0;
			count ++;
			// Simplify again: maybe some operands can be cropped
			hvex_simp_noops(vex);
			return count;
		}
		// Here we have nbright0 > vex->right
		// Count the number of '0' to actually concat
		if(vex->right > 0) {
			nbright0 -= vex->right;
			vex->width -= nbright0;
			vex->left -= vex->right + nbright0;
			vex->right = 0;
		}
		// Move the MUL into another structure, so it can become an operand
		hvex_t* newmul = hvex_new_move(vex);
		// Change the vex into a CONCAT. Keep the width.
		vex->model = HVEX_CONCAT;
		vex->right = 0;
		vex->left = vex->width - 1;
		// Modify the indexes of the MUL
		newmul->width -= nbright0;
		newmul->left -= nbright0;
		// Add operands
		hvex_t* litcat = hvex_newlit_repeat('0', nbright0);
		hvex_addop_head(vex, litcat);
		hvex_addop_head(vex, newmul);
		// Simplify again
		hvex_simp(vex);
		return 1;
	}

	// Simplifications based on the value of the literal operand
	if(litop!=NULL) {
		// If litop = 1, remove it. ONLY if there is more than 1 operand (the rest is handled just after)
		if(vex->operands->next!=NULL && hvex_lit_isone(litop)==true) {
			hvex_remop(litop);
			litop = NULL;
			count++;
		}
		#if 0  // FIXME This is disabled because it adds needed operator SUB, which makes the scheduler fail
		// If litop = -1 and there are 2 operands, the operation becomes -op.
		else if(hvex_is_signed(litop)==true && hvex_lit_checkfitint(litop)==true && hvex_lit_evalint(litop)==-1 && hvex_countops(vex)==2) {
			// Remove the literal operand and replace the vex operation by NEG
			hvex_remop(litop);
			vex->model = HVEX_NEG;
			// One last simplification
			count ++;
			count += hvex_simp_noops(vex);
			return count;
		}
		#endif
	}

	// If only one operand remains, the result is that operand
	if(vex->operands->next==NULL) {
		hvex_t* vexop = vex->operands;
		vex->operands = NULL;
		hvex_replace_resize_free(vex, vexop);
		// Simplify again because the vex has changed
		hvex_simp(vex);
		return 1;
	}

	return count;
}

static unsigned hvex_simp_addcin(hvex_t* vex) {
	unsigned count = 0;

	// Crop & shrink operands
	hvex_foreach(vex->operands, vexop) {
		if(vexop->width-1 > vex->left) {
			hvex_croprel(vexop, vexop->width-1 - vex->left, 0);
			count++;
		}
		count += hvex_shrink(vexop);
	}

	// If the carry input is zero, remove it and convert the ADDCIN into a normal ADD
	hvex_t* op_cin = vex->operands->next->next;
	assert(op_cin!=NULL && op_cin->width==1);

	if(op_cin->model==HVEX_LITERAL) {
		char* lit = hvex_lit_getval(op_cin);
		char c = lit[0];
		if(c=='0' || c=='-') {
			hvex_remop(op_cin);
			vex->model = HVEX_ADD;
			count += hvex_simp_noops(vex);
			return count + 1;
		}
	}

	// If 2 operands are literal, simply convert that into a normal ADD operation
	unsigned countlit = 0;
	hvex_foreach(vex->operands, vexop) {
		if(vexop->model==HVEX_LITERAL) countlit ++;
	}
	if(countlit >= 2) {
		vex->model = HVEX_ADD;
		count += hvex_simp_noops(vex);
		return count + 1;
	}

	// FIXME If one of the operands have a zero at the right, merge with the carry input
	// FIXME Crop the Vex and do sign extension according to max operand size + operand sign

	return count;
}

static unsigned hvex_simp_eqs(hvex_t* vex) {
	bool is_ne = false;
	if(vex->model==HVEX_NE) is_ne = true;

	// Find the max width of the operands
	unsigned maxwidth = 0;
	bool alllit = true;
	char* fulllit = NULL;
	hvex_foreach(vex->operands, vexop) {
		if(vexop->model==HVEX_LITERAL) {
			if(vexop->width > maxwidth) {
				maxwidth = vexop->width;
				fulllit = hvex_lit_getval(vexop);
			}
		}
		else alllit = false;
	}

	if(alllit==true) {
		bool flag_dc = false;  // If there is uncertainty with '-', set this
		char res = 0;

		hvex_foreach(vex->operands, vexop) {
			char* lit = hvex_lit_getval(vexop);
			if(lit==fulllit) continue;

			// Build a local version of the literal
			char locbuf[maxwidth];
			if(vexop->width < maxwidth) {
				unsigned begidx = 0;
				char pad = vexop->pad;
				if(pad==0) pad = lit[0];
				for(begidx=0; begidx<(maxwidth - vexop->width); begidx++) locbuf[begidx] = pad;
				for(unsigned i=0; i<vexop->width; i++) locbuf[begidx + i] = lit[i];
				lit = locbuf;
			}

			// Compare with the max-length operand already found
			bool flag_ne = false;
			for(unsigned i=0; i<maxwidth; i++) {
				char b1 = fulllit[i];
				char b2 = lit[i];
				if(b1=='-' || b2=='-') flag_dc = true;
				else if(b1!=b2) { flag_ne = true; break; }
			}

			// If a conclusion is definitely known, replace the vex by the result
			if(flag_ne==true) {
				res = is_ne==true ? '1' : '0';
				break;
			}

		}  // Scan operands

		// Build the result bit
		if(res==0) {
			// Here we found no definite conflict.
			// So either the operands are either all equal, or there was a "don't care" bit somewhere.
			if(flag_dc==true) res = '-';
			else res = is_ne==true ? '0' : '1';
		}

		// Replace the vex by the result bit
		hvex_free_ops(vex);
		hvex_lit_setrepeat(vex, res, 1);
		return 1;
	}  // End of case where all operands are literal

	// Move the comparison inside a CONCAT operand
	// This is to handle situations like this: ("000" & stuff) = ("000" & stuff2)
	hvex_t* catop = NULL;
	hvex_foreach(vex->operands, vexop) {
		if(vexop->model==HVEX_CONCAT) { catop = vexop; break; }
	}
	if(catop!=NULL) {
		// Replace the VEX by a (N)AND operation
		hvex_t* oldvex = hvex_new_move(vex);
		vex->model = oldvex->model==HVEX_EQ ? HVEX_AND : HVEX_NAND;
		vex->right = 0;
		vex->left  = 0;
		vex->width = 1;
		// Fill the new CONCAT with the cropped operands. Scan right to left for more efficiency.
		unsigned crop_right = 0;
		hvex_t* curop = hvex_last(catop->operands);
		while(curop!=NULL) {
			hvex_t* new_oper_op = hvex_newmodel(HVEX_EQ, 1);
			hvex_foreach(oldvex->operands, vexop) {
				hvex_t* dupop = hvex_new_croprel(
					hvex_dup(vexop),
					vexop->width - curop->width - crop_right,
					crop_right
				);
				hvex_addop_head(new_oper_op, dupop);
			}
			hvex_addop_head(vex, new_oper_op);
			crop_right += curop->width;
			curop = curop->prev;
		}
		// Free the old VEX
		hvex_free(oldvex);
		// Launch a final simplification
		return 1 + hvex_simp(vex);
	}

	return 0;
}

static unsigned hvex_simp_ineqs(hvex_t* vex) {
	hvex_t* dataop1 = vex->operands;
	hvex_t* dataop2 = vex->operands->next;

	bool is_gt = false;
	bool is_eq = false;
	bool is_sign = false;

	if     (vex->model==HVEX_GT)  { is_gt = true;  is_eq = false; is_sign = false; }
	else if(vex->model==HVEX_GE)  { is_gt = true;  is_eq = true;  is_sign = false; }
	else if(vex->model==HVEX_LT)  { is_gt = false; is_eq = false; is_sign = false; }
	else if(vex->model==HVEX_LE)  { is_gt = false; is_eq = true;  is_sign = false; }
	else if(vex->model==HVEX_GTS) { is_gt = true;  is_eq = false; is_sign = true; }
	else if(vex->model==HVEX_GES) { is_gt = true;  is_eq = true;  is_sign = true; }
	else if(vex->model==HVEX_LTS) { is_gt = false; is_eq = false; is_sign = true; }
	else if(vex->model==HVEX_LES) { is_gt = false; is_eq = true;  is_sign = true; }
	else abort();

	unsigned maxwidth = GetMax(dataop1->width, dataop2->width);

	// Handle the case where both operands are literal
	if(dataop1->model==HVEX_LITERAL && dataop2->model==HVEX_LITERAL) goto BOTHLIT;

	// WARNING: Handling of all other cases is currently disabled
	// It's not advised to activate these transformations in AUGH, not currently
	// because it prevents AUGH to recegnize the actual operation done
	// So it can't detect the iteration bounds of loops, unroll loops, estimate the design execution time...
	return 0;

	// Simplify comparisons with zero
	hvex_t* litop = NULL;
	hvex_t* otherop = NULL;

	if(dataop1->model==HVEX_LITERAL) {
		litop = dataop1;
		otherop = dataop2;
		// Inverse the comparison so the zero operand appears to be the second one
		is_gt = is_gt==true ? false : true;
	}
	if(dataop2->model==HVEX_LITERAL) {
		litop = dataop2;
		otherop = dataop1;
	}

	// Here no operand is literal, get out
	if(litop==NULL) return 0;

	// Try to match the literal with the form "00011", count the number of 0 and 1
	char* lit = hvex_lit_getval(litop);
	unsigned nboner = 0;
	unsigned nbzerol = 0;
	bool is_applicable = true;

	for(unsigned i=0; i<litop->width; i++) {
		char c = lit[litop->width - i - 1];
		if(c=='0') nbzerol++;
		else if(c=='1') {
			if(nbzerol > 0) { is_applicable = false; break; }
			nboner++;
		}
		else {
			is_applicable = false;
			break;
		}
	}

	if(is_applicable==false) return 0;

	// Normalize values to the width of the data operand, only if it is wider
	if(litop->width < maxwidth) {
		if(nbzerol > 0) {
			if(litop->pad=='1') return 0;  // Not applicable
			nbzerol += maxwidth - litop->width;
		}
		else {
			if(litop->pad=='1') {
				if(nbzerol > 0) return 0;  // Not applicable
				nboner += maxwidth - litop->width;
			}
			else nbzerol += maxwidth - litop->width;
		}
	}

	assert(nbzerol + nboner == maxwidth);

	// Handle when the literal operand is zero
	if(nboner==0) goto LITZERO;
	// Handle when the literal operand is full of 1
	if(nbzerol==0) goto LITONES;

	// For debug: disable handling of this case
	//return 0;

	// a <  0011  =>  a[31-2]==00 and a[1-0]!=11 if unsigned, a[31] or ( a[30-2]==00 and a[1-0]!=11 ) if signed
	// a <= 0011  =>  a[31-2]==00 if unsigned, a[31] or a[30-2]==00 if signed
	// a >  0011  =>  a[31-2]!=00 if unsigned, not(a[31]) and a[30-2]!=00  if signed
	// a >= 0011  =>  a[31-2]!=00 or a[1-0]==11 if unsigned, not(a[31]) and ( a[30-2]!=00 or a[1-0]==11 ) if signed

	// Implement the transformation for < and <=,
	// then negate the final operation model to obtain > and >=

	hvex_unlinkop(otherop);
	hvex_clearinside(vex);
	hvex_resize(otherop, maxwidth);

	// Handle < and >= (do the < style, then negate if needed)
	if( (is_gt==false && is_eq==false) || (is_gt==true && is_eq==true) ) {
		if(is_sign==false) {
			// a < 0011 (unsigned), replace by: a[31-2]==00 and a[1-0]!=11

			// Build the EQ operand
			hvex_t* vexeq_data = hvex_dup(otherop);
			hvex_resize(vexeq_data, nboner);
			hvex_croprel(vexeq_data, 0, nboner);
			hvex_t* vexeq_lit = hvex_newlit_repeat('0', vexeq_data->width);
			hvex_t* vexeq = hvex_newmodel_op2(HVEX_EQ, 1, vexeq_data, vexeq_lit);

			// Build the NE operand
			hvex_t* vexne_data = otherop;
			hvex_croprel(vexne_data, 0, vexne_data->width - nboner);
			hvex_t* vexne_lit = hvex_newlit_repeat('1', vexne_data->width);
			hvex_t* vexne = hvex_newmodel_op2(HVEX_NE, 1, vexne_data, vexne_lit);

			hvex_addop_head(vex, vexne);
			hvex_addop_head(vex, vexeq);

			// Set the top-level operation (N)AND
			if(is_gt==false) vex->model = HVEX_AND;
			else vex->model = HVEX_NAND;
		}
		else {
			if(nbzerol == 1) {
				// If there is only one zero: a < 011 (signed), replace by: a[1-0]!=11

				hvex_croprel(otherop, nbzerol, 0);
				hvex_t* vexzero = hvex_newlit_repeat('0', nbzerol);

				// Build the final OR expression
				hvex_addop_head(vex, otherop);
				hvex_addop_head(vex, vexzero);

				// Set the top-level operation EQ or NE
				if(is_gt==false) vex->model = HVEX_NE;
				else vex->model = HVEX_EQ;
			}
			else {
				// a < 0011 (signed), replace by: a[31] or ( a[30-2]==00 and a[1-0]!=11 )

				// Build the EQ operand
				hvex_t* vexeq_data = hvex_dup(otherop);
				hvex_croprel(vexeq_data, 1, nboner);
				hvex_t* vexeq_lit = hvex_newlit_repeat('0', vexeq_data->width);
				hvex_t* vexeq = hvex_newmodel_op2(HVEX_EQ, 1, vexeq_data, vexeq_lit);

				// Build the NE operand
				hvex_t* vexne_data = otherop;
				hvex_croprel(vexne_data, vexne_data->width - nboner, 0);
				hvex_t* vexne_lit = hvex_newlit_repeat('1', vexne_data->width);
				hvex_t* vexne = hvex_newmodel_op2(HVEX_NE, 1, vexne_data, vexne_lit);

				// Build the AND operand
				hvex_t* vexand = hvex_newmodel_op2(HVEX_AND, 1, vexeq, vexne);

				// Build the sign bit operand
				hvex_t* vexsign = otherop;
				hvex_croprel(vexsign, 0, vexsign->width-1);

				// Build the final OR expression
				hvex_addop_head(vex, vexand);
				hvex_addop_head(vex, vexsign);

				// Set the top-level operation (N)OR
				if(is_gt==false) vex->model = HVEX_OR;
				else vex->model = HVEX_NOR;
			}
		}
	}
	// Handle > and <= (do the <= style, then negate if needed)
	else {
		if(is_sign==false) {
			// a <= 0011 (unsigned), replace by: a[31-2]==00

			hvex_croprel(otherop, 0, nboner);
			hvex_t* vexzero = hvex_newlit_repeat('0', nbzerol);

			// Build the final OR expression
			hvex_addop_head(vex, otherop);
			hvex_addop_head(vex, vexzero);

			// Set the top-level operation EQ or NE
			if(is_gt==false) vex->model = HVEX_EQ;
			else vex->model = HVEX_NE;
		}
		else {
			if(nbzerol == 1) {
				// If there is only one zero: a <= 011 (signed), always true
				if(is_gt==true) hvex_lit_setrepeat(vex, '1', 1);
				else hvex_lit_setrepeat(vex, '0', 1);
			}
			else {
				// a <= 0011 (signed), replace by: a[31] or a[30-2]==00

				// Build the EQ operand
				hvex_t* vexeq_data = hvex_dup(otherop);
				hvex_croprel(vexeq_data, 1, nboner);
				hvex_t* vexeq_lit = hvex_newlit_repeat('0', vexeq_data->width);
				hvex_t* vexeq = hvex_newmodel_op2(HVEX_EQ, 1, vexeq_data, vexeq_lit);

				// Build the sign bit operand
				hvex_t* vexsign = otherop;
				hvex_croprel(vexsign, 0, vexsign->width-1);

				// Build the final OR expression
				hvex_addop_head(vex, vexeq);
				hvex_addop_head(vex, vexsign);

				// Set the top-level operation (N)OR
				if(is_gt==false) vex->model = HVEX_OR;
				else vex->model = HVEX_NOR;
			}
		}
	}

	return 1 + hvex_simp(vex);

	// Handle the comparisons with a literal full of 1

	LITONES:

	// For debug: disable handling of this case
	//return 0;

	// a <  1111  =>  a!=1111 if unsigned, a[31] and a[30-0]!=1111 if signed
	// a <= 1111  =>  1 if unsigned, a[31] if signed
	// a >  1111  =>  0 if unsigned, not(a[31]) if signed
	// a >= 1111  =>  a==1111 if unsigned, not(a[31]) or a==1111 if signed

	if(is_gt==false && is_eq==false) {
		if(is_sign==false) {
			// Replace by: a!=1111, just change the operation
			vex->model = HVEX_NE;
		}
		else {
			if(maxwidth==1) {
				// Replace by the literal 0
				hvex_clearinside(vex);
				hvex_lit_setrepeat(vex, '0', 1);
			}
			else {
				// Replace by: a[31] and a[30-0]!=1111
				hvex_t* vexbit = hvex_dup(otherop);
				hvex_resize(vexbit, maxwidth);
				hvex_croprel(vexbit, 0, vexbit->width-1);
				// Create the NE operation
				if(otherop->width==maxwidth) hvex_croprel(otherop, 1, 0);
				if(litop->width==maxwidth) hvex_croprel(litop, 1, 0);
				hvex_t* vexne = hvex_new_move(vex);
				vexne->model = HVEX_NE;
				// Change the original operation to AND
				vex->model = HVEX_AND;
				hvex_addop_head(vex, vexbit);
				hvex_addop_head(vex, vexne);
			}
		}
	}
	else if(is_gt==false && is_eq==true) {
		if(is_sign==false) {
			// Replace by the literal 1
			hvex_clearinside(vex);
			hvex_lit_setrepeat(vex, '1', 1);
		}
		else {
			// Replace by: a[31]
			hvex_unlinkop(otherop);
			hvex_clearinside(vex);
			hvex_resize(otherop, maxwidth);
			hvex_croprel(otherop, 0, otherop->width-1);
			hvex_replace_free(vex, otherop);
		}
	}
	else if(is_gt==true && is_eq==false) {
		if(is_sign==false) {
			// Replace by the literal 0
			hvex_clearinside(vex);
			hvex_lit_setrepeat(vex, '0', 1);
		}
		else {
			// Replace by: not(a[31])
			hvex_unlinkop(otherop);
			hvex_clearinside(vex);
			hvex_resize(otherop, maxwidth);
			hvex_croprel(otherop, 0, otherop->width-1);
			hvex_replace_free(vex, hvex_newnot(otherop));
		}
	}
	if(is_gt==true && is_eq==true) {
		if(is_sign==false) {
			// Replace by; a==1111, just change the operation
			vex->model = HVEX_EQ;
		}
		else {
			// Replace by: not(a[31]) or a==1111
			if(maxwidth==1) {
				// Replace by the literal 1
				hvex_clearinside(vex);
				hvex_lit_setrepeat(vex, '1', 1);
			}
			else {
				hvex_t* vexbit = hvex_dup(otherop);
				hvex_resize(vexbit, maxwidth);
				hvex_croprel(vexbit, 0, vexbit->width-1);
				vexbit = hvex_newnot(vexbit);
				// Create the NE operation
				if(otherop->width==maxwidth) hvex_croprel(otherop, 1, 0);
				if(litop->width==maxwidth) hvex_croprel(litop, 1, 0);
				hvex_t* vexeq = hvex_new_move(vex);
				vexeq->model = HVEX_EQ;
				// Change the original operation to OR
				vex->model = HVEX_OR;
				hvex_addop_head(vex, vexbit);
				hvex_addop_head(vex, vexeq);
			}
		}
	}

	return 1 + hvex_simp(vex);

	// Handle the comparisons with zero

	LITZERO:

	// a<0   =>   a[31] if signed, '0' if unsigned
	// a<=0  =>   a[31] OR a==0 if signed, a==0 if unsigned
	// a>0   =>   not(a[31] OR a==0) if signed, not(a==0) if unsigned
	// a>=0  =>   not(a[31]) if signed, '1' if unsigned

	if(is_gt==false && is_eq==false) {
		if(is_sign==false) {
			// Replace by the literal '0'
			hvex_clearinside(vex);
			hvex_lit_setrepeat(vex, '0', vex->width);
		}
		else {
			// Replace by the sign bit of the data operand
			hvex_unlinkop(otherop);
			hvex_clearinside(vex);
			hvex_resize(otherop, maxwidth);
			hvex_croprel(otherop, 0, otherop->width-1);
			hvex_replace_free(vex, otherop);
		}
	}
	else if(is_gt==false && is_eq==true) {
		if(is_sign==false) {
			// Replace by: a==0
			vex->model = HVEX_EQ;
			hvex_simp_noops(vex);
		}
		else {
			if(maxwidth==1) {
				// Replace by the literal 1
				hvex_clearinside(vex);
				hvex_lit_setrepeat(vex, '1', 1);
			}
			else {
				// Replace by: a[31] OR a==0
				hvex_t* dupop = hvex_dup(otherop);
				hvex_resize(dupop, maxwidth);
				hvex_croprel(dupop, 0, dupop->width - 1);
				// Create the EQ operation
				if(otherop->width==maxwidth) hvex_croprel(otherop, 1, 0);
				if(litop->width==maxwidth) hvex_croprel(litop, 1, 0);
				hvex_t* newop = hvex_new_move(vex);
				newop->model = HVEX_EQ;
				// Convert the original operation into OR
				vex->model = HVEX_OR;
				hvex_addop_head(vex, newop);
				hvex_addop_head(vex, dupop);
				hvex_simp(vex);
			}
		}
	}
	else if(is_gt==true && is_eq==false) {
		if(is_sign==false) {
			// Replace by: not(a==0)
			vex->model = HVEX_NE;
			hvex_simp_noops(vex);
		}
		else {
			if(maxwidth==1) {
				// Replace by the literal 1
				hvex_clearinside(vex);
				hvex_lit_setrepeat(vex, '1', 1);
			}
			else {
				// Replace by: not( a[31] OR a==0 )
				hvex_t* dupop = hvex_dup(otherop);
				hvex_resize(dupop, maxwidth);
				hvex_croprel(dupop, 0, dupop->width - 1);
				// Create the EQ operation
				if(otherop->width==maxwidth) hvex_croprel(otherop, 1, 0);
				if(litop->width==maxwidth) hvex_croprel(litop, 1, 0);
				hvex_t* newop = hvex_new_move(vex);
				newop->model = HVEX_EQ;
				// Convert the original operation into NOR
				vex->model = HVEX_NOR;
				hvex_addop_head(vex, newop);
				hvex_addop_head(vex, dupop);
				hvex_simp(vex);
			}
		}
	}
	if(is_gt==true && is_eq==true) {
		if(is_sign==false) {
			// Replace by the literal '1'
			hvex_clearinside(vex);
			hvex_lit_setrepeat(vex, '1', vex->width);
		}
		else {
			// Replace by the NOT of the sign bit of the data operand
			hvex_unlinkop(otherop);
			hvex_clearinside(vex);
			hvex_resize(otherop, maxwidth);
			hvex_croprel(otherop, 0, otherop->width - 1);
			otherop = hvex_newnot(otherop);
			hvex_replace_free(vex, otherop);
		}
	}

	// Something has changed, so return >0
	return 1 + hvex_simp(vex);

	// Here begins the case where both operands are literal

	// Note: This is declared before the label to avoid compiler failure
	char* lit1;
	char* lit2;

	BOTHLIT:

	lit1 = hvex_lit_getval(dataop1);
	lit2 = hvex_lit_getval(dataop2);

	// Resize the shortest operand if needed
	char buf[maxwidth];
	if(dataop1->width < dataop2->width) {
		hvex_calc_fillbuf(maxwidth, buf, dataop1->width, lit1, dataop1->pad);
		lit1 = buf;
	}
	else if(dataop2->width < dataop1->width) {
		hvex_calc_fillbuf(maxwidth, buf, dataop2->width, lit2, dataop2->pad);
		lit2 = buf;
	}

	// Now the literals have same width. Compare them.
	bool is_neg = false;
	char res = 0;  // When it's non-zero, computing is finished
	unsigned idx = 0;  // To skip the sign bit
	if(is_sign==true) {
		char c1 = lit1[0];
		char c2 = lit2[0];
		// FIXME Handle when there is one '-' but the result should be '0' or '1':
		//   examples: -111 > 0001, -111 >= 0111
		if(c1=='-' || c2=='-') { res = '-'; goto HAVERES; }
		if(c1==c2) { if(c1=='1') is_neg = true; idx = 1; }
		else {
			// Here one bit is '0', the other is '1'
			if(c1=='0') res = is_gt==true ? '1' : '0';
			else        res = is_gt==true ? '0' : '1';
			goto HAVERES;
		}
	}
	for(unsigned i=idx; i<maxwidth; i++) {
		char c1 = lit1[i];
		char c2 = lit2[i];
		if(c1=='-' || c2=='-') { res = '-'; break; }
		if(c1==c2) continue;
		// Here one of the bits is '0', the other is '1'
		assert( (c1=='1' && c2=='0') || (c1=='0' && c2=='1'));
		if(c1=='0') {
			// c1=='0' && c2=='1'
			if(is_neg==false) res = is_gt==true ? '0' : '1';
			else              res = is_gt==true ? '1' : '0';
		}
		else {
			// c1=='1' && c2=='0'
			if(is_neg==false) res = is_gt==true ? '1' : '0';
			else              res = is_gt==true ? '0' : '1';
		}
		break;
	}  // Scan the bits

	HAVERES:

	if(res==0) {
		// The operands are equal
		if(is_eq==true) res = '1'; else res = '0';
	}

	// Replace the vex by the result
	hvex_free_ops(vex);
	hvex_lit_setrepeat(vex, res, 1);

	return 1;
}

static unsigned hvex_simp_shl(hvex_t* vex) {
	hvex_t* dataop = vex->operands;
	hvex_t* shiftop = vex->operands->next;
	unsigned count = 0;

	// Crop & shrink operands
	assert(hvex_is_unsigned(shiftop)==true);
	count += hvex_shrink(dataop);
	count += hvex_shrinku(shiftop);

	// Left crop if possible
	if(dataop->width-1 > vex->left) {
		hvex_croprel(dataop, dataop->width-1 - vex->left, 0);
		count++;
		count += hvex_shrink(dataop);
	}

	// Handle when the shift value is literal
	if(shiftop->model==HVEX_LITERAL) {
		// If there are bits '-' replace them by '0' and shrink again
		hvex_lit_replace_dc(shiftop, '0');
		hvex_shrinku(shiftop);

		if(hvex_lit_checkfitint(shiftop)==true) {
			// Get the unsigned shift value
			unsigned shift_value = hvex_lit_evalint(shiftop);
			// Handle when the shift is zero:
			// the vex becomes the data operand, resized and cropped
			if(shift_value==0) {
				hvex_remop(shiftop);
				hvex_unlinkop(dataop);
				hvex_resize(dataop, vex->left+1);
				hvex_croprel(dataop, 0, vex->right);
				hvex_replace_free(vex, dataop);
				return 1 + hvex_simp(vex);
			}
			// Replace the shift by a crop and a concat with bits '0'
			// The shift operation becomes a CONCAT
			if(shift_value < vex->left+1) {
				unsigned orig_left  = vex->left;
				unsigned orig_right = vex->right;
				vex->model = HVEX_CONCAT;
				vex->right = 0;
				vex->width = dataop->width + shift_value;
				vex->left = vex->width - 1;
				// The shift operand becomes the literal '0'
				hvex_lit_setrepeat(shiftop, '0', shift_value);
				// Resize the resulting CONCAT and crop right
				hvex_resize(vex, orig_left+1);
				hvex_croprel(vex, 0, orig_right);
				// And another simplification
				return 1 + hvex_simp(vex);
			}
		}

		// Here we know the shift value is higher than the data width
		// Replace the entire vex by all '0'
		hvex_free_ops(vex);
		hvex_lit_setrepeat(vex, '0', vex->width);

		return 1;
	}

	return count;
}
static unsigned hvex_simp_shr(hvex_t* vex) {
	hvex_t* dataop = vex->operands;
	hvex_t* shiftop = vex->operands->next;
	unsigned count = 0;

	// Crop & shrink operands
	assert(hvex_is_unsigned(shiftop)==true);
	count += hvex_shrink(dataop);
	count += hvex_shrinku(shiftop);

	// Right crop if possible
	if(vex->right > 0) {
		hvex_croprel(dataop, 0, vex->right);
		vex->left -= vex->right;
		vex->right = 0;
		count ++;
		// Shrink again
		count += hvex_shrink(dataop);
	}

	// Handle when the shift value is literal
	if(shiftop->model==HVEX_LITERAL) {
		// If there are bits '-' replace them by '0' and shrink again
		hvex_lit_replace_dc(shiftop, '0');
		hvex_shrink(shiftop);

		if(hvex_lit_checkfitint(shiftop)==true) {
			// Get the unsigned value
			unsigned shift_value = hvex_lit_evalint(shiftop);
			// Separately handle small shift values
			if(shift_value < dataop->width - vex->right) {
				hvex_remop(shiftop);
				hvex_unlinkop(dataop);
				// Replace the shift by a crop and a resize of the data operand
				//      |     dataop     |
				//            | shifted dataop |
				// |     result    |
				hvex_croprel(dataop, 0, vex->right + shift_value);
				hvex_resize(dataop, vex->width);
				hvex_replace_free(vex, dataop);
				// Simplify again
				hvex_simp_noops(vex);
				return 1;
			}
		}

		// Here we know the shift value is higher than the data width
		// Remove the shift operand
		hvex_remop(shiftop);
		hvex_unlinkop(dataop);
		// Replace by the sign extension of the data operand
		hvex_replace_byextend_free(vex, dataop);

		return 1;
	}

	// Move the data sign extension out of the shifter
	if(dataop->width < vex->left+1) {
		unsigned bits_nb = vex->left+1 - dataop->width;
		vex->left -= bits_nb;
		vex->width -= bits_nb;
		char save_pad = vex->pad;
		vex->pad = dataop->pad;
		hvex_resize(vex, vex->width + bits_nb);
		vex->pad = save_pad;
		return 1 + hvex_simp_noops(vex);
	}

	return count;
}
// FIXME Simplify this code
static unsigned hvex_simp_rots(hvex_t* vex) {
	hvex_t* dataop = vex->operands;
	hvex_t* shiftop = vex->operands->next;
	unsigned count = 0;

	// Shrink the shift operand
	assert(hvex_is_unsigned(shiftop)==true);
	count += hvex_shrinku(shiftop);

	// Handle when the shift value is literal
	if(shiftop->model==HVEX_LITERAL) {
		// If there are bits '-' replace them by '0' and shrink again
		count += hvex_lit_replace_dc(shiftop, '0');
		count += hvex_shrinku(shiftop);
		unsigned shift_value = 0;

		// Unlink the shift operand
		hvex_unlinkop(shiftop);

		// Handle when the shift value can't fit in an integer (situation a bit extreme, isn't it?)
		if(hvex_lit_checkfitint(shiftop)==false) {
			// Create a DIVR operation
			hvex_t* tmp_val = hvex_newlit_int(dataop->width, sizeof(unsigned)*8, false);
			hvex_t* tmp_divr = hvex_newmodel_op2(HVEX_DIVR, sizeof(unsigned)*8, shiftop, tmp_val);
			// Simplify the DIVR
			hvex_simp_noops(tmp_divr);
			assert(tmp_divr->model == HVEX_LITERAL);
			assert(hvex_lit_checkfitint(tmp_divr)==true);
			// Free the temp stuff
			shiftop = tmp_divr;
		}

		// Get the shift value
		shift_value = hvex_lit_evalint(shiftop);
		shift_value %= dataop->width;

		// Free the shift operand. Note: it is already unlinked from the vex.
		hvex_free(shiftop);

		// ROTR is ROTL with a modification of the shift value
		// This simp function works the ROTL style
		if(vex->model==HVEX_ROTL) {}
		else if(vex->model==HVEX_ROTR) {
			if(shift_value > 0) shift_value = dataop->width - shift_value;
		}
		else abort();

		// Minor check for debug
		assert(shift_value < dataop->width);

		// Handle when the rotation lets the operand unchanged
		// Also when the shift value is small
		if(shift_value <= vex->right) {
			// |                  data operand                 |
			// |   |<- result selection ->|   |<- shift value->|
			// Replace the vex by the cropped data operand
			hvex_unlinkop(dataop);
			hvex_cropabs(dataop, vex->left - shift_value, vex->right - shift_value);
			hvex_replace_free(vex, dataop);
			hvex_simp_noops(vex);
		}
		else if(shift_value >= vex->left + 1) {
			// |                  data operand                 |
			// |       |<-          shift value              ->|
			// |                |<-   result selection   ->|   |
			// Replace the vex by the cropped data operand
			hvex_unlinkop(dataop);
			unsigned i = vex->left + 1 - shift_value;
			hvex_cropabs(dataop, vex->left - i, vex->right - i);
			hvex_replace_free(vex, dataop);
			hvex_simp_noops(vex);
		}
		else {
			// Save the old data width
			unsigned data_width = dataop->width;
			// Replace the vex by a concat of 2 cropped data operands
			hvex_t* dataop_left = hvex_dup(dataop);
			hvex_addop_head(vex, dataop_left);
			// Crop the operands
			hvex_croprel(dataop_left, shift_value, 0);
			hvex_croprel(dataop, 0, dataop->width - shift_value);
			// The shift operation becomes a CONCAT
			vex->model = HVEX_CONCAT;
			// Properly apply cropping
			if(vex->left != data_width - 1 || vex->right > 0) {
				unsigned want_left = vex->left;
				unsigned want_right = vex->right;
				vex->width = data_width;
				vex->left = data_width - 1;
				vex->right = 0;
				hvex_cropabs(vex, want_left, want_right);
			}
			// Simplify again
			hvex_simp_noops(dataop);
			hvex_simp_noops(dataop_left);
			hvex_simp_noops(vex);
		}

		return 1;
	}  // Case where shift is literal

	return count;
}

static unsigned hvex_simp_not(hvex_t* vex) {
	hvex_t* vexop = vex->operands;

	// Note: this function assumes vex->right=0 and that all operands have proper width

	// If operand is literal, just compute the result
	if(vexop->model==HVEX_LITERAL) {
		char* lit = vexop->data;
		char res[vex->width + 1];
		for(unsigned i=0; i<vex->width; i++) res[i] = hvex_notbit(lit[i]);
		res[vex->width] = 0;
		hvex_lit_setval(vexop, stralloc(res));
		// Replace the main node by the literal
		hvex_unlinkop(vexop);
		hvex_replace_free(vex, vexop);
		return 1;
	}

	// Handle not(not(stuff)): replace all by just "stuff"
	else if(vexop->model==HVEX_NOT) {
		hvex_t* vexop2 = vexop->operands;
		hvex_unlinkop(vexop);
		hvex_replace_free(vex, vexop2);
		return 1;
	}

	else if(vexop->model==HVEX_RESIZE) {
		// Note: If the operand is not signed, then the padding should be inverted
		// Note: Intentionally only handle not() of signed operand.
		// Because otherwise, it is assumed that the operand have been simplified and the extension has been replaced by a LITERAL.
		if(vexop->pad!=0) return 0;
		// Apply the NOT on the operand, in-place, and re-simplify it
		hvex_not(vexop->operands);
		hvex_simp_noops(vexop->operands);
		// Replace the top NOT operation
		hvex_unlinkop(vexop);
		hvex_replace_free(vex, vexop);
		return 1;
	}

	// Swap the NOT operation and the top operation
	// Example: not(repeat(stuff)) becomes repeat(not(stuff))
	else if(vexop->model==HVEX_CONCAT || vexop->model==HVEX_REPEAT) {
		// Apply the NOT on the operands, in-place, and re-simplify them
		hvex_foreach(vexop->operands, vexop2) {
			hvex_not(vexop2);
			hvex_simp_noops(vexop2);
		}
		// Replace the top NOT operation
		hvex_unlinkop(vexop);
		hvex_replace_free(vex, vexop);
		return 1;
	}

	// If there is a registered opposite model, set it
	if(vexop->model->model_not!=NULL) {
		hvex_unlinkop(vexop);
		vexop->model = vexop->model->model_not;
		hvex_replace_free(vex, vexop);
		return 1;
	}

	return 0;
}

// A utility function for simplification of logics: replace the vex by its unique operand
static unsigned hvex_simp_logics_oneop(hvex_t* vex) {
	assert(vex->operands!=NULL && vex->operands->next==NULL);
	unsigned count = 0;

	// Unlink the operand
	hvex_t* vexop = vex->operands;
	vex->operands = NULL;

	// Apply the NOT operation if needed
	if(vex->model==HVEX_NAND || vex->model==HVEX_NOR || vex->model==HVEX_NXOR) {
		hvex_not(vexop);
		count += hvex_simp_noops(vexop);
	}

	// Replace the main vex by the operand
	hvex_replace_free(vex, vexop);
	count++;

	return count;
}
// Main simplification function or logics
static unsigned hvex_simp_logics(hvex_t* vex) {
	unsigned count = 0;

	// Note: this function assumes vex->right=0 and that all operands have proper width
	assert(vex->right==0);

	// Merge literal operands
	hvex_t* litop = NULL;
	char bufres[vex->width + 1];
	hvex_t* vexop = vex->operands;
	while(vexop!=NULL) {
		if(vexop->model!=HVEX_LITERAL) { vexop = vexop->next; continue; }
		// Save the first literal operand
		if(litop==NULL) {
			litop = vexop;
			vexop = vexop->next;
			continue;
		}
		// Merge the current operand in the first literal operand
		if     (vex->model==HVEX_AND)  hvex_calc_and(vex->width, litop->data, vexop->data, bufres);
		else if(vex->model==HVEX_NAND) hvex_calc_and(vex->width, litop->data, vexop->data, bufres);
		else if(vex->model==HVEX_OR)   hvex_calc_or (vex->width, litop->data, vexop->data, bufres);
		else if(vex->model==HVEX_NOR)  hvex_calc_or (vex->width, litop->data, vexop->data, bufres);
		else if(vex->model==HVEX_XOR)  hvex_calc_xor(vex->width, litop->data, vexop->data, bufres);
		else if(vex->model==HVEX_NXOR) hvex_calc_xor(vex->width, litop->data, vexop->data, bufres);
		else abort();
		bufres[vex->width] = 0;
		litop->data = stralloc(bufres);
		// Increment ocunt of things done
		count++;
		// Remove the current operand
		hvex_t* nextop = vexop->next;
		hvex_remop(vexop);
		vexop = nextop;
	}

	// If only one operand remains, replace the vex
	if(vex->operands->next==NULL) {
		return count + hvex_simp_logics_oneop(vex);
	}

	// Simplification when the literal is one repeated bit
	// It may be unuseful (remove it) or it may inhibit all other operands (replace the entire Vex)
	if(litop!=NULL) {

		// Note about how the bit '-' is handled:
		// Assume it absorbs all other operands for highest simplification (e.g. '1' for OR, '0' for AND)
		char bit_dc = 0;
		// An input bit that can be removed
		char bit_rem = 0;
		// An 'inhibitor' bit and the result value when present
		char bit_inh = 0;
		char bit_inh_res = 0;

		if     (vex->model==HVEX_OR)   { bit_dc = '1'; bit_rem = '0'; bit_inh = '1'; bit_inh_res = '1'; }
		else if(vex->model==HVEX_NOR)  { bit_dc = '1'; bit_rem = '0'; bit_inh = '1'; bit_inh_res = '0'; }
		else if(vex->model==HVEX_AND)  { bit_dc = '0'; bit_rem = '1'; bit_inh = '0'; bit_inh_res = '0'; }
		else if(vex->model==HVEX_NAND) { bit_dc = '0'; bit_rem = '1'; bit_inh = '0'; bit_inh_res = '1'; }
		else if(vex->model==HVEX_XOR)  { bit_dc = '0'; bit_rem = '0'; }
		else if(vex->model==HVEX_NXOR) { bit_dc = '0'; bit_rem = '0'; }
		else abort();
		count += hvex_lit_replace_dc(litop, bit_dc);

		// Check if the literal is all one bit
		char* lit = hvex_lit_getval(litop);
		char samebit = lit[0];
		for(unsigned i=1; i<vex->width; i++) if(lit[i]!=samebit) { samebit = 0; break; }

		// Unconditionally remove the literal operand
		hvex_remop(litop);
		litop = NULL;
		count++;

		// Handle when the literal is just one repeated bit
		if(samebit!=0) {

			if(samebit==bit_rem) {
				// Do nothing, the input is simply removed
			}
			else if(samebit==bit_inh) {
				// This is the 'inhibitor' bit, replace by the appropriate bit
				hvex_clearinside(vex);
				hvex_lit_setrepeat(vex, bit_inh_res, vex->width);
				count++;
				return count;
			}
			else {
				// Here we must be in the case of XOR/NXOR and it's the bit '1'
				assert(vex->model==HVEX_XOR || vex->model==HVEX_NXOR);
				assert(samebit=='1');
				// Invert the operation model
				assert(vex->model->model_not!=NULL);
				vex->model = vex->model->model_not;
				hvex_simp_noops(vex);
				return count + 1;
			}

		}  // Case where the literal is just one repeated bit

		// Handle when the literal is not just one repeated bit
		else {

			// Split the Vex according to the different parts of the literal
			hvex_t* vex_move = hvex_new_move(vex);
			vex->model = HVEX_CONCAT;

			unsigned cur_right = 0;
			unsigned cur_left = 0;

			// Iteratively get the different parts of the literal and create operands for the CONCAT
			// Process from right to left
			do {

				// Get the width of the current part
				char bit = lit[vex->width - 1 - cur_right];
				do {
					if(cur_left == vex->width - 1) break;
					if(lit[vex->width - 1 - (cur_left+1)] != bit) break;
					cur_left ++;
				} while(1);

				// Generate one part
				hvex_t* cur_part = hvex_dup(vex_move);
				// Add another operand: the partial literal
				// Note: We can't do this after cropping the operand because it may no longer be a logic operation
				hvex_addop_head(cur_part, hvex_newlit_repeat(bit, cur_part->width));

				// Crop and simplify the new part
				hvex_croprel(cur_part, vex->width - 1 - cur_left, cur_right);
				count += hvex_simp_noops(cur_part);

				// Add this part as operand to the replacement CONCAT
				hvex_addop_head(vex, cur_part);

				// Increment indexes for next part
				if(cur_left == vex->width - 1)  break;
				cur_left ++;
				cur_right = cur_left;

			} while(1);

			// Clean
			hvex_free(vex_move);

			// Final simplification
			count += hvex_simp_noops(vex);
			return count + 1;
		}  // Case where the literal is not just one repeated bit

	}  // Case where there is a literal operand

	// If only one operand remains, replace the vex
	if(vex->operands->next==NULL) {
		return count + hvex_simp_logics_oneop(vex);
	}

	// Remove identical operands
	vexop = vex->operands;
	while(vexop!=NULL) {
		bool remthisop = false;

		hvex_t* vexop2 = vexop->next;
		while(vexop2!=NULL) {
			int z = hvex_cmp(vexop, vexop2);
			if(z!=0) { vexop2 = vexop2->next; continue; }
			// Here the operands are identical
			// Always remove the second one
			hvex_remop(vexop2);
			count++;
			// If model os XOR or NXOR, also remove the first operand
			if(vex->model==HVEX_XOR || vex->model==HVEX_NXOR) {
				remthisop = true;
				break;
			}
		}

		hvex_t* nextop = vexop->next;
		if(remthisop==true) {
			if(vex->operands->next==NULL) {
				// Avoid the situation where there is no operand, replace
				hvex_free_ops(vex);
				// Set the new literal
				char replacebit = '0';
				if(vex->model==HVEX_XOR) replacebit = '0';
				else if(vex->model==HVEX_NXOR) replacebit = '1';
				else abort();
				hvex_lit_setrepeat(vex, replacebit, vex->width);
				return count;
			}
			hvex_remop(vexop);
		}

		// Next operand
		vexop = nextop;
	}

	// If only one operand remains, replace the vex
	if(vex->operands->next==NULL) {
		return count + hvex_simp_logics_oneop(vex);
	}

	// Get the max number of "inhibitor" bits from left or right of operands
	char bit_inh = 0;
	char bit_res = 0;
	bool can_apply = false;
	if(vex->model==HVEX_AND) {
		bit_inh = '0';
		bit_res = '0';
		can_apply = true;
	}
	else if(vex->model==HVEX_OR) {
		bit_inh = '1';
		bit_res = '1';
		can_apply = true;
	}
	else if(vex->model==HVEX_NAND) {
		bit_inh = '0';
		bit_res = '1';
		can_apply = true;
	}
	else if(vex->model==HVEX_NOR) {
		bit_inh = '1';
		bit_res = '0';
		can_apply = true;
	}
	if(can_apply==true) {
		unsigned bits_nb_left = 0;
		unsigned bits_nb_right = 0;
		// Get the max numbers of inhibitor bits, from left and right
		hvex_foreach(vex->operands, vexOp) {
			unsigned loc_bits_left  = hvex_getnbleftbit(vexOp, bit_inh, true);
			unsigned loc_bits_right = hvex_getnbrightbit(vexOp, bit_inh, true);
			bits_nb_left  = GetMax(bits_nb_left,  loc_bits_left);
			bits_nb_right = GetMax(bits_nb_right, loc_bits_right);
		}
		// Handle when the entire Vex can be replaced by a literal
		if(bits_nb_left >= vex->width || bits_nb_right >= vex->width) {
			hvex_clearinside(vex);
			hvex_lit_setrepeat(vex, vex->width, bit_res);
			return 1;
		}
		// Partial replacement
		if(bits_nb_left > 0 || bits_nb_right > 0) {
			assert(bits_nb_left + bits_nb_right < vex->width);
			hvex_t* vex_move = hvex_new_move(vex);
			hvex_croprel(vex_move, bits_nb_left, bits_nb_right);
			vex->model = HVEX_CONCAT;
			if(bits_nb_right > 0) hvex_addop_head(vex, hvex_newlit_repeat(bit_res, bits_nb_right));
			hvex_addop_head(vex, vex_move);
			if(bits_nb_left > 0) hvex_addop_head(vex, hvex_newlit_repeat(bit_res, bits_nb_left));
			return 1 + hvex_simp_noops(vex);
		}
	}

	// Move the logic operation inside a CONCAT operand
	// This is to handle situations like this: (stuff & "000") OR ("000" & stuff2)
	hvex_t* catop = NULL;
	hvex_foreach(vex->operands, vexop) {
		if(vexop->model==HVEX_CONCAT) { catop = vexop; break; }
	}
	if(catop!=NULL) {
		// Replace the VEX by a CONCAT operation
		hvex_t* oldvex = hvex_new_move(vex);
		vex->model = HVEX_CONCAT;
		// Fill the new CONCAT with the cropped operands. Scan right to left for more efficiency.
		unsigned crop_right = 0;
		hvex_t* curop = hvex_last(catop->operands);
		while(curop!=NULL) {
			hvex_t* new_oper_op = hvex_newmodel(oldvex->model, curop->width);
			hvex_foreach(oldvex->operands, vexop) {
				hvex_t* dupop = hvex_new_croprel(
					hvex_dup(vexop),
					vexop->width - curop->width - crop_right,
					crop_right
				);
				hvex_addop_head(new_oper_op, dupop);
			}
			hvex_addop_head(vex, new_oper_op);
			crop_right += curop->width;
			curop = curop->prev;
		}
		// Free the old VEX
		hvex_free(oldvex);
		// Launch a final simplification
		return 1 + hvex_simp(vex);
	}

	// Move the logic operation inside a RESIZE operand
	// This is to handle situations like this: "000000" OR (resize+3 of stuff2)
	//   and to later get at: "000" OR stuff2
	// FIXME: Unuseful in this form? Do it only if another operand is LITERAL, or REPEAT?
	#if 1

	hvex_t* resizeop = NULL;
	hvex_foreach(vex->operands, vexop) {
		if(vexop->model==HVEX_RESIZE) { catop = vexop; break; }
	}
	if(resizeop!=NULL) {
		assert(resizeop->left+1 > resizeop->operands->width && resizeop->right==0);
		// Replace the VEX by a CONCAT operation
		hvex_t* oldvex = hvex_new_move(vex);
		vex->model = HVEX_CONCAT;
		// Fill the new CONCAT with the cropped operands. Scan right to left for more efficiency.
		unsigned cur_crop_right = 0;
		unsigned cur_width = resizeop->operands->width;
		// Little loop that iterates 2 times because there will be only 2 operands for the CONCAT
		for(unsigned i=0; i<2; i++) {
			hvex_t* new_oper_op = hvex_newmodel(oldvex->model, cur_width);
			hvex_foreach(oldvex->operands, vexop) {
				hvex_t* dupop = hvex_new_croprel(
					hvex_dup(vexop),
					vexop->width - cur_width - cur_crop_right,
					cur_crop_right
				);
				hvex_addop_head(new_oper_op, dupop);
			}
			hvex_addop_head(vex, new_oper_op);
			// Update wanted indexes for the other, leftmost operand
			cur_crop_right = oldvex->width - resizeop->operands->width;
			cur_width = resizeop->operands->width;
		}
		// Free the old VEX
		hvex_free(oldvex);
		// Launch a final simplification
		return 1 + hvex_simp(vex);
	}

	#endif

	// When 2 operands are REPEAT, move the logic operation inside
	unsigned repeatops_nb = 0;
	hvex_foreach(vex->operands, vexop) {
		if(vexop->model==HVEX_REPEAT) repeatops_nb++;
	}
	if(repeatops_nb >= 2) {
		// Build the new, small, logic operation
		hvex_t* newvex = hvex_newmodel(vex->model, 1);
		hvex_foreach(vex->operands, vexop) {
			if(vexop->model!=HVEX_REPEAT) continue;
			hvex_addop_head(newvex, hvex_unlinkop(vexop->operands));
			hvex_remop(vexop);
		}
		// A small simplification
		count += hvex_simp_noops(newvex);
		// Insert the new REPEAT operation in the original VEX node
		if(vex->operands==NULL) {
			vex->model = HVEX_REPEAT;
			hvex_addop_head(vex, newvex);
		}
		else {
			hvex_t* newrepeat = hvex_newmodel_op1(HVEX_REPEAT, vex->width, newvex);
			hvex_addop_head(vex, newrepeat);
		}
		// Launch a final simplification
		return count + hvex_simp_noops(vex);
	}

	return count;
}

static unsigned hvex_simp_asg(hvex_t* vex) {
	unsigned count = 0;

	// ASG_EN and ASG_ADDR_EN models: simplify the condition
	if(vex->model==HVEX_ASG_EN || vex->model==HVEX_ASG_ADDR_EN) {
		hvex_t* vex_cond = hvex_asg_get_cond(vex);
		// Note: the cond is supposed to be simplified already

		if(vex_cond->model==HVEX_LITERAL) {
			char* lit = hvex_lit_getval(vex_cond);
			char c = lit[vex_cond->width-1];
			if(c=='0') {
				// Nothing to do, the Action will be removed
			}
			else if(c=='1') {
				// Remove the condition
				hvex_remop(vex_cond);
				if(vex->model==HVEX_ASG_EN) vex->model = HVEX_ASG;
				else if(vex->model==HVEX_ASG_ADDR_EN) vex->model = HVEX_ASG_ADDR;
				else {
					abort();
				}
				count++;
			}
			else {
				// Cond = '-', replace by '0' so the Action is later removed
				hvex_lit_setval(vex_cond, stralloc("0"));
				count++;
			}
		}  // Cond is literal

	}  // End simplification of condition

	// Note: Nothing to simplify for models ASG and ASG_ADDR

	return count;
}

static unsigned hvex_simp_muxb(hvex_t* vex) {
	hvex_t* selop = vex->operands;

	if(selop->model==HVEX_LITERAL) {
		// Force the selection operand as unsigned
		hvex_set_unsigned(selop);
		// Replace the VEX by the right data operand
		unsigned selval = hvex_lit_evalint(selop);
		hvex_t* dataop = vex->operands->next;
		for(unsigned i=0; i<selval; i++) dataop = dataop->next;
		assert(dataop!=NULL);
		// Unlink the data operand
		hvex_unlinkop(dataop);
		hvex_replace_free(vex, dataop);
		return 1;
	}

	return 0;
}

static unsigned hvex_simp_muxdec(hvex_t* vex) {
	unsigned count = 0;

	// If an operand has condition 1, replace the vex by this data operand
	// Remove all operands whose condition is zero
	// Remove operands whose data is all zero

	hvex_t* vexop_cond1 = NULL;
	chain_list* listops_rem = NULL;

	hvex_foreach(vex->operands, vexop) {
		assert(vexop->model==HVEX_MUXDECIN);
		hvex_t* vexop_cond = vexop->operands;
		hvex_t* vexop_data = vexop->operands->next;
		if(vexop_cond->model==HVEX_LITERAL) {
			char* lit = hvex_lit_getval(vexop_cond);
			if(lit[0]=='1') { vexop_cond1 = vexop; break; }
			else listops_rem = addchain(listops_rem, vexop);
		}
		else if(vexop_data->model==HVEX_LITERAL) {
			if(hvex_lit_iszero(vexop_data)==true) {
				listops_rem = addchain(listops_rem, vexop);
			}
		}
	}

	if(vexop_cond1!=NULL) {
		// Free the list of operands to remove
		freechain(listops_rem);
		// Extract the replacement data operand
		hvex_t* vexop_data = vexop_cond1->operands->next;
		hvex_unlinkop(vexop_data);
		// Replace the vex
		hvex_replace_free(vex, vexop_data);
		return 1;
	}

	// Remove the unneded operands
	foreach(listops_rem, scan) {
		hvex_t* vexop = scan->DATA;
		hvex_remop(vexop);
		count++;
	}
	freechain(listops_rem);

	// If no operand remains, replace by all zero
	if(vex->operands==NULL) {
		hvex_clearinside(vex);
		hvex_lit_setrepeat(vex, '0', vex->width);
		return 1;
	}

	// TODO Simp when only 2 operands and tests are opposite: convert to MUXB
	// TODO Simp when muxdec(muxdec) and tests are guaranteed to be incompatible => merge in parent MUXDEC
	// TODO Simp when operands are REPEAT
	// TODO Simp when only 1 operand, width 1: convert to AND

	return count;
}


// Don't simplify operands recursively.
// Useful for fast re-simplification inside simplification and cropping callbacks.
// FIXME should be inline?
unsigned hvex_simp_noops(hvex_t* vex) {
	unsigned count = 0;

	// For expensive debug
	//dbgprintf("Vex %p  -  ", vex); hvex_print_bn(vex);

	if(vex->model->func_simp!=NULL) count = vex->model->func_simp(vex);

	// For expensive debug
	//dbgprintf("Vex %p  -  ", vex); hvex_print_bn(vex);
	//hvex_checkabort(vex);

	return count;
}

// Main simplification function
unsigned hvex_simp(hvex_t* vex) {
	unsigned count = 0;
	assert(vex->model!=NULL);

	// For expensive debug
	//dbgprintf("Vex %p  -  ", vex); hvex_print_bn(vex);

	hvex_foreach(vex->operands, vexop) count += hvex_simp(vexop);
	if(vex->model->func_simp!=NULL) count += vex->model->func_simp(vex);

	// For expensive debug
	//dbgprintf("Vex %p  -  ", vex); hvex_print_bn(vex);
	//hvex_checkabort(vex);

	return count;
}



//==============================================
// Model-specific functions
//==============================================

// Check if all bits are either '0' or '1'
bool hvex_lit_allknown(hvex_t* vex) {
	assert(vex->model==HVEX_LITERAL);

	char* lit = hvex_lit_getval(vex);
	for(unsigned i=0; i<vex->width; i++) {
		char c = lit[i];
		if(c!='0' && c!='1') return false;
	}

	return true;
}

// Replace '-' by the specified bit. Return the number of bits replaced.
unsigned hvex_lit_replace_dc(hvex_t* vex, char bit) {
	assert(vex->model==HVEX_LITERAL);

	char* lit = hvex_lit_getval(vex);
	unsigned count = 0;
	char buf[vex->width + 1];

	for(unsigned i=0; i<vex->width; i++) {
		char c = lit[i];
		if(c=='-') { c = bit; count++; }
		buf[i] = c;
	}

	if(count>0) {
		buf[vex->width] = 0;
		hvex_lit_setval(vex, stralloc(buf));
	}

	return count;
}

// Evaluation of LITERAL as integer
int hvex_lit_evalint(hvex_t* vex) {
	assert(vex->model==HVEX_LITERAL);

	char* lit = vex->data;

	int val = 0;
	if( (vex->pad==0 && lit[0]=='1') || vex->pad=='1' ) val = -1;

	for(unsigned i=0; i<vex->width; i++) {
		val = (val << 1) | (lit[i]=='1' ? 1 : 0);
	}

	return val;
}

// Set a node content as LITERAL
void hvex_lit_setrepeat(hvex_t* vex, char c, unsigned n) {
	vex->model = HVEX_LITERAL;
	vex->data = stralloc_repeat(c, n);
	vex->width = n;
	vex->left = n - 1;
	vex->right = 0;
}

// Return 0 if not all same bit
char hvex_lit_getsamebit(hvex_t* vex) {
	char* lit = hvex_lit_getval(vex);
	char samebit = lit[0];
	for(unsigned i=1; i<vex->width; i++) if(lit[i]!=samebit) return 0;
	return samebit;
}

// Count the number of bits '0' or '1' from the left
unsigned hvex_lit_getnbleftbit(hvex_t* vex, char bit, bool with_dc) {
	assert(vex->model==HVEX_LITERAL);
	char* lit = hvex_lit_getval(vex);
	unsigned count = 0;
	for(unsigned i=0; i<vex->width; i++) {
		if(lit[i]!=bit && (with_dc==false || lit[i]!='-')) break;
		count++;
	}
	return count;
}
unsigned hvex_getnbleftbit(hvex_t* vex, char bit, bool with_dc) {

	if(vex->model==HVEX_LITERAL) {
		return hvex_lit_getnbleftbit(vex, bit, with_dc);
	}

	else if(vex->model==HVEX_CONCAT) {
		unsigned count = 0;
		count = hvex_getnbleftbit(vex->operands, bit, with_dc);
		if(count==vex->operands->width && vex->operands->next!=NULL) {
			// In case it is a RESIZE
			count += hvex_getnbleftbit(vex->operands->next, bit, with_dc);
		}
		return count;
	}

	// Note: models REPEAT and RESIZE are intentionally not handled
	// Because it is assumed that they have already been simplified, so there is a LITERAL at left

	return 0;
}

// Count the number of bits '0' or '1' from the right
unsigned hvex_lit_getnbrightbit(hvex_t* vex, char bit, bool with_dc) {
	assert(vex->model==HVEX_LITERAL);
	char* lit = hvex_lit_getval(vex);
	unsigned count = 0;
	for(int i=vex->width - 1; i>=0; i--) {
		if(lit[i]!=bit && (with_dc==false || lit[i]!='-')) break;
		count++;
	}
	return count;
}
unsigned hvex_getnbrightbit(hvex_t* vex, char bit, bool with_dc) {

	if(vex->model==HVEX_LITERAL) {
		return hvex_lit_getnbrightbit(vex, bit, with_dc);
	}

	else if(vex->model==HVEX_CONCAT) {
		unsigned count = 0;
		hvex_t* lastop = hvex_last(vex->operands);
		count = hvex_getnbrightbit(lastop, bit, with_dc);
		if(count==lastop->width && lastop->prev!=NULL) {
			// In case it is a RESIZE
			count += hvex_getnbrightbit(lastop->prev, bit, with_dc);
		}
		return count;
	}

	else if(vex->model==HVEX_RESIZE) {
		// Note: assume it has already been simplified: can't know the sign extension bit
		return hvex_getnbrightbit(vex->operands, bit, with_dc);
	}

	// Note: model REPEAT is intentionally not handled
	// Because it is assumed that it has already been simplified and replaced by a literal

	return 0;
}

// Concat a Vex to another Vex, in-place
void hvex_catleft(hvex_t* vex, hvex_t* op) {
	// Ensure the model is CONCAT
	if(vex->model!=HVEX_CONCAT) {
		hvex_t* moveop = hvex_new_move(vex);
		hvex_addop_head(vex, moveop);
		vex->model = HVEX_CONCAT;
		vex->left = vex->width - 1;
		vex->right = 0;
	}
	// Here the model is CONCAT. Add the operand.
	hvex_addop_head(vex, op);
	vex->width += op->width;
	vex->left += op->width;
}
void hvex_catright(hvex_t* vex, hvex_t* op) {
	// Ensure the model is CONCAT
	if(vex->model!=HVEX_CONCAT) {
		hvex_t* moveop = hvex_new_move(vex);
		hvex_addop_head(vex, moveop);
		vex->model = HVEX_CONCAT;
		vex->left = vex->width - 1;
		vex->right = 0;
	}
	// Here the model is CONCAT. Append the operand.
	hvex_addop_tail(vex, op);
	vex->width += op->width;
	vex->left += op->width;
}

// FIXME These functions should not handle '-' => this should be separate function
//   Also, most of the time we don't want the sign char to be tested

// Check if the literal value is 0
bool hvex_lit_iszero(hvex_t* vex) {
	if(vex->pad=='1') return false;
	char* lit = hvex_lit_getval(vex);
	for(unsigned i=0; i<vex->width; i++) if(lit[i]!='0' && lit[i]!='-') return false;
	return true;
}
// Check if the literal value is 1
bool hvex_lit_isone(hvex_t* vex) {
	char* lit = hvex_lit_getval(vex);
	if(vex->pad=='1' || (vex->pad==0 && vex->width>0 && lit[0]=='1')) return false;
	if(lit[vex->width-1]!='1') return false;
	for(unsigned i=0; i<vex->width-1; i++) if(lit[i]!='0' && lit[i]!='-') return false;
	return true;;
}

// Return the opposite bit
char hvex_notbit(char bit) {
	if(bit=='0') return '1';
	if(bit=='1') return '0';
	return bit;
}

// The node becomes the operand of a NOT operation
void hvex_not(hvex_t* vex) {
	// Shortcut
	if(vex->model->model_not!=NULL) {
		vex->model = vex->model->model_not;
		return;
	}
	// Move the vex content to a new node
	hvex_t* vexop = hvex_new_move(vex);
	// Create the NOT operation (note: indexes and padding are kept)
	vex->model = HVEX_NOT;
	hvex_addop_head(vex, vexop);
}
// This function may be faster
// The input vex may not be kept. It must NOT be an operand of another vex already.
hvex_t* hvex_newnot(hvex_t* vex) {
	// Shortcut
	if(vex->model->model_not!=NULL) {
		vex->model = vex->model->model_not;
		return vex;
	}
	// Create a new NOT operation
	hvex_t* vexnot = hvex_newmodel_op1(HVEX_NOT, vex->width, vex);
	vexnot->pad = vex->pad;
	return vexnot;
}

// Set the min right and max left in first operand
void hvex_set_minmax(hvex_t* vex1, hvex_t* vex2) {
	if(vex2->left  > vex1->left)  vex1->left  = vex2->left;
	if(vex2->right < vex1->right) vex1->right = vex2->right;
	vex1->width = vex1->left - vex1->right + 1;
}

// Return the MSB bit, if known: '0', '1', '-', or 0 if not applicable
char hvex_get_msb(hvex_t* vex) {
	if(vex->model==HVEX_LITERAL) {
		return hvex_lit_getval(vex)[1];
	}
	else if(vex->model==HVEX_CONCAT) {
		return hvex_get_msb(vex->operands);
	}
	return 0;
}

// Build ASG operations
hvex_t* hvex_asg_make(hvex_t* vex_dest, hvex_t* vex_addr, hvex_t* vex_expr, hvex_t* vex_en) {
	hvex_t* Expr = hvex_new();
	Expr->right = 0;
	Expr->left  = vex_dest->width - 1;
	Expr->width = vex_dest->width;

	if(vex_addr==NULL) {
		if(vex_en==NULL) Expr->model = HVEX_ASG;
		else             Expr->model = HVEX_ASG_EN;
	}
	else {
		if(vex_en==NULL) Expr->model = HVEX_ASG_ADDR;
		else             Expr->model = HVEX_ASG_ADDR_EN;
	}

	if(vex_en!=NULL) hvex_addop_head(Expr, vex_en);
	if(vex_addr!=NULL) hvex_addop_head(Expr, vex_addr);
	hvex_addop_head(Expr, vex_expr);
	hvex_addop_head(Expr, vex_dest);

	return Expr;
}

// Add an extra condition to an assignment, merge with existing conditions with and ADND operation
void hvex_asg_andcond(hvex_t* vex, hvex_t* andcond) {
	assert(andcond!=NULL);

	if(vex->model == HVEX_ASG) {
		hvex_addop_tail(vex, andcond);
		vex->model = HVEX_ASG_EN;
	}
	else if(vex->model == HVEX_ASG_ADDR) {
		hvex_addop_tail(vex, andcond);
		vex->model = HVEX_ASG_ADDR_EN;
	}
	else if(vex->model == HVEX_ASG_EN || vex->model == HVEX_ASG_ADDR_EN){
		// Get the existing condition
		hvex_t* oldcond = hvex_asg_get_cond(vex);
		assert(oldcond!=NULL);
		// The condition becomes an AND operation
		if(oldcond->model!=HVEX_AND) {
			hvex_t* opmove = hvex_new_move(oldcond);
			oldcond->model = HVEX_AND;
			hvex_addop_head(oldcond, opmove);
		}
		// Add the new condition to the AND operation
		hvex_addop_head(oldcond, andcond);
	}

	else {
		abort();
	}
}

bool hvex_model_is_asg(hvex_model_t* model) {
	if(model==HVEX_ASG || model==HVEX_ASG_EN || model==HVEX_ASG_ADDR || model==HVEX_ASG_ADDR_EN) return true;
	return false;
}

bool hvex_model_is_ineq(hvex_model_t* model) {
	if(
		model==HVEX_LT  || model==HVEX_GT  || model==HVEX_LE  || model==HVEX_GE ||
		model==HVEX_LTS || model==HVEX_GTS || model==HVEX_LES || model==HVEX_GES
	) return true;
	return false;
}

hvex_model_t* hvex_model_reverse_ineq(hvex_model_t* model) {
	if     (model==HVEX_LT)  return HVEX_GT;
	else if(model==HVEX_GT)  return HVEX_LT;
	else if(model==HVEX_LE)  return HVEX_GE;
	else if(model==HVEX_GE)  return HVEX_LE;
	else if(model==HVEX_LTS) return HVEX_GTS;
	else if(model==HVEX_GTS) return HVEX_LTS;
	else if(model==HVEX_LES) return HVEX_GES;
	else if(model==HVEX_GES) return HVEX_LES;
	return NULL;
}

bool hvex_model_is_ineq_or_eq(hvex_model_t* model) {
	if(model==HVEX_LE || model==HVEX_GE || model==HVEX_LES || model==HVEX_GES) return true;
	return false;
}
bool hvex_model_is_ineq_signed(hvex_model_t* model) {
	if(model==HVEX_LTS || model==HVEX_GTS || model==HVEX_LES || model==HVEX_GES) return true;
	return false;
}
bool hvex_model_is_ineq_lower(hvex_model_t* model) {
	if(model==HVEX_LT || model==HVEX_LE || model==HVEX_LTS || model==HVEX_LES) return true;
	return false;
}
bool hvex_model_is_ineq_upper(hvex_model_t* model) {
	if(model==HVEX_GT || model==HVEX_GE || model==HVEX_GTS || model==HVEX_GES) return true;
	return false;
}



//==============================================
// Initialization of the HVEX module
//==============================================

void hvex_init() {

	hvex_model_t* model = NULL;

	model = hvex_model_new();
	model->name = namealloc("vector");
	model->func_croprel = NULL;
	model->func_fprint = hvex_fprint_vector;
	model->func_check = hvex_check_vec;
	HVEX_VECTOR = model;

	model = hvex_model_new();
	model->name = namealloc("literal");
	model->func_croprel = hvex_croprel_literal;
	model->func_fprint = hvex_fprint_literal;
	model->func_check = hvex_check_lit;
	HVEX_LITERAL = model;

	model = hvex_model_new();
	model->name = namealloc("index");
	model->func_croprel = NULL;
	model->func_fprint = hvex_fprint_index;
	model->func_check = hvex_check_index;
	HVEX_INDEX = model;

	model = hvex_model_new();
	model->name = namealloc("concat");
	model->func_croprel = hvex_croprel_concat;
	model->func_simp = hvex_simp_concat;
	model->func_fprint = hvex_fprint_opbetween_cb;
	model->sym_fprint = namealloc("&");
	model->func_check = hvex_check_concat;
	HVEX_CONCAT = model;

	model = hvex_model_new();
	model->name = namealloc("resize");
	model->func_croprel = NULL;
	model->func_simp = hvex_simp_resize;
	model->func_fprint = hvex_fprint_resize;
	model->func_check = hvex_check_resize;
	HVEX_RESIZE = model;

	model = hvex_model_new();
	model->name = namealloc("repeat");
	model->func_croprel = hvex_croprel_repeat;
	model->func_simp = hvex_simp_repeat;
	model->func_fprint = hvex_fprint_repeat;
	model->func_check = hvex_check_repeat;
	HVEX_REPEAT = model;

	// Should only be used for parsing

	model = hvex_model_new();
	model->name = namealloc("string");
	model->func_croprel = hvex_croprel_error_cantcrop;
	model->func_fprint = hvex_fprint_string;
	model->func_check = hvex_check_string;
	HVEX_STRING = model;

	model = hvex_model_new();
	model->name = namealloc("function");
	model->func_croprel = NULL;
	model->func_fprint = hvex_fprint_function;
	model->func_check = hvex_check_function;
	HVEX_FUNCTION = model;

	model = hvex_model_new();
	model->name = namealloc("divq");
	model->func_croprel = NULL;
	model->func_simp = hvex_simp_divqr;
	model->func_fprint = hvex_fprint_opbetween_cb;
	model->sym_fprint = namealloc("/");
	model->func_check = hvex_check_divqr;
	HVEX_DIVQ = model;

	model = hvex_model_new();
	model->name = namealloc("divr");
	model->func_croprel = NULL;
	model->func_simp = hvex_simp_divqr;
	model->func_fprint = hvex_fprint_opbetween_cb;
	model->sym_fprint = namealloc("%");
	model->func_check = hvex_check_divqr;
	HVEX_DIVR = model;

	// Arithmetic operations

	model = hvex_model_new();
	model->name = namealloc("add");
	model->func_croprel = NULL;
	model->func_simp = hvex_simp_add;
	model->func_fprint = hvex_fprint_opbetween_cb;
	model->sym_fprint = namealloc("+");
	model->func_check = hvex_check_add;
	HVEX_ADD = model;

	model = hvex_model_new();
	model->name = namealloc("sub");
	model->func_croprel = NULL;
	model->func_simp = hvex_simp_sub;
	model->func_fprint = hvex_fprint_opbetween_cb;
	model->sym_fprint = namealloc("-");
	model->func_check = hvex_check_sub;
	HVEX_SUB = model;

	model = hvex_model_new();
	model->name = namealloc("neg");
	model->func_croprel = NULL;
	model->func_simp = hvex_simp_neg;
	model->func_fprint = hvex_fprint_opbefore_cb;
	model->sym_fprint = namealloc("-");
	model->func_check = hvex_check_neg;
	HVEX_NEG = model;

	model = hvex_model_new();
	model->name = namealloc("mul");
	model->func_croprel = NULL;
	model->func_simp = hvex_simp_mul;
	model->func_fprint = hvex_fprint_opbetween_cb;
	model->sym_fprint = namealloc("*");
	model->func_check = hvex_check_mul;
	HVEX_MUL = model;

	// Arithmetic operations with carry input

	model = hvex_model_new();
	model->name = namealloc("addcin");
	model->func_croprel = NULL;
	model->func_simp = hvex_simp_addcin;
	model->func_fprint = hvex_fprint_opbefore_cb;
	model->func_check = hvex_check_addcin;
	HVEX_ADDCIN = model;

	// Comparisons

	model = hvex_model_new();
	model->name = namealloc("eq");
	model->func_croprel = hvex_croprel_error_cantcrop;
	model->func_simp = hvex_simp_eqs;
	model->func_fprint = hvex_fprint_opbetween_cb;
	model->sym_fprint = namealloc("==");
	model->func_check = hvex_check_eqs;
	HVEX_EQ = model;

	model = hvex_model_new();
	model->name = namealloc("ne");
	model->func_croprel = hvex_croprel_error_cantcrop;
	model->func_simp = hvex_simp_eqs;
	model->func_fprint = hvex_fprint_opbetween_cb;
	model->sym_fprint = namealloc("!=");
	model->func_check = hvex_check_eqs;
	HVEX_NE = model;

	// Inequalities

	model = hvex_model_new();
	model->name = namealloc("gt");
	model->func_croprel = hvex_croprel_error_cantcrop;
	model->func_simp = hvex_simp_ineqs;
	model->func_fprint = hvex_fprint_opbetween_cb;
	model->sym_fprint = namealloc(">");
	model->func_check = hvex_check_ineqs;
	HVEX_GT = model;

	model = hvex_model_new();
	model->name = namealloc("ge");
	model->func_croprel = hvex_croprel_error_cantcrop;
	model->func_simp = hvex_simp_ineqs;
	model->func_fprint = hvex_fprint_opbetween_cb;
	model->sym_fprint = namealloc(">=");
	model->func_check = hvex_check_ineqs;
	HVEX_GE = model;

	model = hvex_model_new();
	model->name = namealloc("lt");
	model->func_croprel = hvex_croprel_error_cantcrop;
	model->func_simp = hvex_simp_ineqs;
	model->func_fprint = hvex_fprint_opbetween_cb;
	model->sym_fprint = namealloc("<");
	model->func_check = hvex_check_ineqs;
	HVEX_LT = model;

	model = hvex_model_new();
	model->name = namealloc("le");
	model->func_croprel = hvex_croprel_error_cantcrop;
	model->func_simp = hvex_simp_ineqs;
	model->func_fprint = hvex_fprint_opbetween_cb;
	model->sym_fprint = namealloc("<=");
	model->func_check = hvex_check_ineqs;
	HVEX_LE = model;

	model = hvex_model_new();
	model->name = namealloc("gts");
	model->func_croprel = hvex_croprel_error_cantcrop;
	model->func_simp = hvex_simp_ineqs;
	model->func_fprint = hvex_fprint_opbetween_cb;
	model->sym_fprint = namealloc(">s");
	model->func_check = hvex_check_ineqs;
	HVEX_GTS = model;

	model = hvex_model_new();
	model->name = namealloc("ges");
	model->func_croprel = hvex_croprel_error_cantcrop;
	model->func_simp = hvex_simp_ineqs;
	model->func_fprint = hvex_fprint_opbetween_cb;
	model->sym_fprint = namealloc(">=s");
	model->func_check = hvex_check_ineqs;
	HVEX_GES = model;

	model = hvex_model_new();
	model->name = namealloc("lts");
	model->func_croprel = hvex_croprel_error_cantcrop;
	model->func_simp = hvex_simp_ineqs;
	model->func_fprint = hvex_fprint_opbetween_cb;
	model->sym_fprint = namealloc("<s");
	model->func_check = hvex_check_ineqs;
	HVEX_LTS = model;

	model = hvex_model_new();
	model->name = namealloc("les");
	model->func_croprel = hvex_croprel_error_cantcrop;
	model->func_simp = hvex_simp_ineqs;
	model->func_fprint = hvex_fprint_opbetween_cb;
	model->sym_fprint = namealloc("<=s");
	model->func_check = hvex_check_ineqs;
	HVEX_LES = model;

	// Shifts and rotations

	model = hvex_model_new();
	model->name = namealloc("shl");
	model->func_croprel = NULL;
	model->func_simp = hvex_simp_shl;
	model->func_fprint = hvex_fprint_opbetween_cb;
	model->sym_fprint = namealloc("<<");
	model->func_check = hvex_check_shiftsrots;
	HVEX_SHL = model;

	model = hvex_model_new();
	model->name = namealloc("shr");
	model->func_croprel = NULL;
	model->func_simp = hvex_simp_shr;
	model->func_fprint = hvex_fprint_opbetween_cb;
	model->sym_fprint = namealloc(">>");
	model->func_check = hvex_check_shiftsrots;
	HVEX_SHR = model;

	model = hvex_model_new();
	model->name = namealloc("rotl");
	model->func_croprel = NULL;
	model->func_simp = hvex_simp_rots;
	model->func_fprint = hvex_fprint_opbefore_cb;
	model->func_check = hvex_check_shiftsrots;
	HVEX_ROTL = model;

	model = hvex_model_new();
	model->name = namealloc("rotr");
	model->func_croprel = NULL;
	model->func_simp = hvex_simp_rots;
	model->func_fprint = hvex_fprint_opbefore_cb;
	model->func_check = hvex_check_shiftsrots;
	HVEX_ROTR = model;

	// Logic operations

	model = hvex_model_new();
	model->name = namealloc("not");
	model->func_croprel = hvex_croprel_logic_all;
	model->func_simp = hvex_simp_not;
	model->func_fprint = hvex_fprint_opbefore_cb;
	model->func_check = hvex_check_not;
	HVEX_NOT = model;

	model = hvex_model_new();
	model->name = namealloc("and");
	model->func_croprel = hvex_croprel_logic_all;
	model->func_simp = hvex_simp_logics;
	model->func_fprint = hvex_fprint_opbetween_cb;
	model->func_check = hvex_check_logics;
	HVEX_AND = model;

	model = hvex_model_new();
	model->name = namealloc("nand");
	model->func_croprel = hvex_croprel_logic_all;
	model->func_simp = hvex_simp_logics;
	model->func_fprint = hvex_fprint_opbefore_cb;
	model->func_check = hvex_check_logics;
	HVEX_NAND = model;

	model = hvex_model_new();
	model->name = namealloc("or");
	model->func_croprel = hvex_croprel_logic_all;
	model->func_simp = hvex_simp_logics;
	model->func_fprint = hvex_fprint_opbetween_cb;
	model->func_check = hvex_check_logics;
	HVEX_OR = model;

	model = hvex_model_new();
	model->name = namealloc("nor");
	model->func_croprel = hvex_croprel_logic_all;
	model->func_simp = hvex_simp_logics;
	model->func_fprint = hvex_fprint_opbefore_cb;
	model->func_check = hvex_check_logics;
	HVEX_NOR = model;

	model = hvex_model_new();
	model->name = namealloc("xor");
	model->func_croprel = hvex_croprel_logic_all;
	model->func_simp = hvex_simp_logics;
	model->func_fprint = hvex_fprint_opbetween_cb;
	model->func_check = hvex_check_logics;
	HVEX_XOR = model;

	model = hvex_model_new();
	model->name = namealloc("nxor");
	model->func_croprel = hvex_croprel_logic_all;
	model->func_simp = hvex_simp_logics;
	model->func_fprint = hvex_fprint_opbefore_cb;
	model->func_check = hvex_check_logics;
	HVEX_NXOR = model;

	// Assignments

	model = hvex_model_new();
	model->name = namealloc("asg");
	model->func_croprel = hvex_croprel_error_cantcrop;
	model->func_simp = hvex_simp_asg;
	model->func_fprint = hvex_fprint_asg;
	model->func_check = hvex_check_asg;
	HVEX_ASG = model;

	model = hvex_model_new();
	model->name = namealloc("asg_en");
	model->func_croprel = hvex_croprel_error_cantcrop;
	model->func_simp = hvex_simp_asg;
	model->func_fprint = hvex_fprint_asg_en;
	model->func_check = hvex_check_asg_en;
	HVEX_ASG_EN = model;

	model = hvex_model_new();
	model->name = namealloc("asg_addr");
	model->func_croprel = hvex_croprel_error_cantcrop;
	model->func_simp = hvex_simp_asg;
	model->func_fprint = hvex_fprint_asg_addr;
	model->func_check = hvex_check_asg_addr;
	HVEX_ASG_ADDR = model;

	model = hvex_model_new();
	model->name = namealloc("asg_addr_en");
	model->func_croprel = hvex_croprel_error_cantcrop;
	model->func_simp = hvex_simp_asg;
	model->func_fprint = hvex_fprint_asg_addr_en;
	model->func_check = hvex_check_asg_addr_en;
	HVEX_ASG_ADDR_EN = model;

	// Mux-like operations

	model = hvex_model_new();
	model->name = namealloc("muxb");
	model->func_croprel = hvex_croprel_muxb;
	model->func_simp = hvex_simp_muxb;
	model->func_fprint = hvex_fprint_opbefore_cb;
	model->func_check = hvex_check_muxb;
	HVEX_MUXB = model;

	model = hvex_model_new();
	model->name = namealloc("muxdec");
	model->func_croprel = hvex_croprel_muxdec;
	model->func_simp = hvex_simp_muxdec;
	model->func_fprint = hvex_fprint_opbefore_cb;
	model->func_check = hvex_check_muxdec;
	HVEX_MUXDEC = model;

	model = hvex_model_new();
	model->name = namealloc("muxdecin");
	model->func_croprel = hvex_croprel_error_cantcrop;  // Crop must be from parent MUXDEC
	model->func_simp = NULL;
	model->func_fprint = hvex_fprint_opbefore_cb;
	model->func_check = hvex_check_muxdecin;
	HVEX_MUXDECIN = model;

	// Stuff used only at parsing

	model = hvex_model_new();
	model->name = namealloc("array");
	model->func_croprel = hvex_croprel_error_cantcrop;
	model->func_fprint = hvex_fprint_array;
	HVEX_ARRAY = model;

	// Now, set the opposite models

	HVEX_EQ->model_not = HVEX_NE;
	HVEX_NE->model_not = HVEX_EQ;

	HVEX_GT->model_not = HVEX_LE;
	HVEX_GE->model_not = HVEX_LT;
	HVEX_LT->model_not = HVEX_GE;
	HVEX_LE->model_not = HVEX_GT;
	HVEX_GTS->model_not = HVEX_LES;
	HVEX_GES->model_not = HVEX_LTS;
	HVEX_LTS->model_not = HVEX_GES;
	HVEX_LES->model_not = HVEX_GTS;

	HVEX_AND ->model_not = HVEX_NAND;
	HVEX_NAND->model_not = HVEX_AND;
	HVEX_OR  ->model_not = HVEX_NOR;
	HVEX_NOR ->model_not = HVEX_OR;
	HVEX_XOR ->model_not = HVEX_NXOR;
	HVEX_NXOR->model_not = HVEX_XOR;

}


