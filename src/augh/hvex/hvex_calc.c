
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <stdbool.h>
#include <assert.h>

#include "hvex.h"

#include "../auto/auto.h"  // For GetMin(), GetMax(), Swap()


// A useful little function to fill a buffer from an existing literal
void hvex_calc_fillbuf(unsigned widthD, char* dest, unsigned widthS, char* src, char pad) {
	// Write the input literal
	if(widthS>0) {
		unsigned bitsnb = GetMin(widthD, widthS);  // In case source larger than destination
		char* ptrD = dest + widthD - 1;
		char* ptrS = src + widthS - 1;
		for(unsigned i=0; i<bitsnb; i++) {
			*ptrD = *ptrS;
			ptrD--;
			ptrS--;
		}
	}
	// In case destination is larger than source
	if(widthD > widthS) {
		unsigned bitsnb = widthD - widthS;
		if(pad==0) pad = src[0];  // Get sign extension of source
		for(unsigned i=0; i<bitsnb; i++) dest[i] = pad;
	}
}

// Add B to A, overwrite A. Return the carry out.
// inB may be NULL or larger than inA.
char hvex_calc_add(unsigned widthA, char* inA, unsigned widthB, char* inB, char padB, char inC) {
	char c = inC;  // The carry

	// Local utility function to add 1 bit.
	// Return the result, reads and updates the carry.
	char loc_add(char a, char b) {
		char r = '0';

		unsigned nb0 = 0;
		unsigned nb1 = 0;
		unsigned nbdc = 0;
		if(a=='0') nb0++; else if(a=='1') nb1++; else nbdc++;
		if(b=='0') nb0++; else if(b=='1') nb1++; else nbdc++;
		if(c=='0') nb0++; else if(c=='1') nb1++; else nbdc++;

		// Handle when there is at least one '-'
		if(nbdc>0) {
			if(nb0==2) c = '0';       // two '0' and one '-'
			else if(nb1==2) c = '1';  // two '1' and one '-'
			else c = '-';
			r = '-';
		}

		// Handle when there is at least two '1' (carry always '1')
		else if(nb1>=2) {
			if(nb1==3) r = '1';
			else if(nb0>0) r = '0';
			else r = '-';
			c = '1';
		}

		// Handle when there is at least two '0' (carry always '0')
		else {
			// All bits are '0', nothing to do, results are already set to '0'.
			if(nb1==0) {}
			// Last situation: one '1' and two '0'
			else r = '1';
			c = '0';
		}

		return r;
	}

	// Add inB
	if(widthB>0) {
		unsigned bitsnb = GetMin(widthA, widthB);  // In case inB larger than inA
		char* ptrA = inA + widthA - 1;
		char* ptrB = inB + widthB - 1;
		for(unsigned i=0; i<bitsnb; i++) {
			*ptrA = loc_add(*ptrA, *ptrB);
			ptrA--;
			ptrB--;
		}
	}

	// In case inA larger than inB
	if(widthA > widthB) {
		unsigned bitsnb = widthA - widthB;
		char* ptrA = inA + bitsnb - 1;
		if(padB==0) padB = inB[0];  // Get sign extension of inB
		for(unsigned i=0; i<bitsnb; i++) {
			*ptrA = loc_add(*ptrA, padB);
			ptrA--;
		}
	}

	return c;
}

// Subtract B from A, overwrite A. Return the carry out.
// inB may be NULL or larger than inA.
char hvex_calc_sub(unsigned widthA, char* inA, unsigned widthB, char* inB, char padB, char inC) {
	char c = inC;  // The carry

	// Local utility function to subtract 1 bit.
	// Return the result, reads and updates the carry.
	char loc_sub(char a, char b) {
		char r = '0';

		if(a=='0') {
			if(b=='0') {
				// c = '0' or '1' or '-' the result is: c unchanged and r = c
				r = c;
			}
			else if(b=='1') {
				r = hvex_notbit(c);
				c = '1';
			}
			else {  // b = '-'
				if(c=='1') {}  // c is unchanged
				else c = '-';
				r = '-';
			}
		}

		else if(a=='1') {
			if(b=='0' || c=='0') {
				r = hvex_notbit(b!='0' ? b : c);
				c = '0';
			}
			else if(b==c) {  // both '1' or both '-'
				r = b;
				c = b;
			}
			else {  // one is '1', the other is '-'
				r = '-';
				c = '-';
			}
		}

		else {  // a = '-'
			// b and c are '0' => c = '0'
			// b and c are '1' => c = '1'
			// Otherwise, c = '-'
			if(b==c) {}  // c is unchanged
			else c = '-';
			r = '-';
		}

		return r;
	}

	// Subtract inB
	if(widthB>0) {
		unsigned bitsnb = GetMin(widthA, widthB);  // In case inB larger than inA
		char* ptrA = inA + widthA - 1;
		char* ptrB = inB + widthB - 1;
		for(unsigned i=0; i<bitsnb; i++) {
			*ptrA = loc_sub(*ptrA, *ptrB);
			ptrA--;
			ptrB--;
		}
	}

	// In case inA larger than inB
	if(widthA > widthB) {
		unsigned bitsnb = widthA - widthB;
		char* ptrA = inA + bitsnb - 1;
		if(padB==0) padB = inB[0];  // Get sign extension of inB
		for(unsigned i=0; i<bitsnb; i++) {
			*ptrA = loc_sub(*ptrA, padB);
			ptrA--;
		}
	}

	return c;
}

// Logic operations, width of the operands and of the result must be identical
void hvex_calc_not(unsigned width, char* in, char* res) {
	for(unsigned i=0; i<width; i++) {
		char a = in[i];
		char r = '0';

		if(a=='0') r = '1';
		else if(a=='1') r = '0';
		else r = '-';

		res[i] = r;
	}  // Loop on the bits to compute
}
void hvex_calc_and(unsigned width, char* inA, char* inB, char* res) {
	for(unsigned i=0; i<width; i++) {
		char a = inA[i];
		char b = inB[i];
		char r = '0';

		if(a=='0' || b=='0') r = '0';
		else if(a=='-' || b=='-') r = '-';
		else r = '1';  // Both inputs are '1'

		res[i] = r;
	}  // Loop on the bits to compute
}
void hvex_calc_nand(unsigned width, char* inA, char* inB, char* res) {
	for(unsigned i=0; i<width; i++) {
		char a = inA[i];
		char b = inB[i];
		char r = '0';

		if(a=='0' || b=='0') r = '1';
		else if(a=='-' || b=='-') r = '-';
		else r = '0';  // Both inputs are '1'

		res[i] = r;
	}  // Loop on the bits to compute
}
void hvex_calc_or(unsigned width, char* inA, char* inB, char* res) {
	for(unsigned i=0; i<width; i++) {
		char a = inA[i];
		char b = inB[i];
		char r = '0';

		if(a=='1' || b=='1') r = '1';
		else if(a=='-' || b=='-') r = '-';
		else r = '0';  // Both inputs are '0'

		res[i] = r;
	}  // Loop on the bits to compute
}
void hvex_calc_nor(unsigned width, char* inA, char* inB, char* res) {
	for(unsigned i=0; i<width; i++) {
		char a = inA[i];
		char b = inB[i];
		char r = '0';

		if(a=='1' || b=='1') r = '0';
		else if(a=='-' || b=='-') r = '-';
		else r = '1';  // Both inputs are '0'

		res[i] = r;
	}  // Loop on the bits to compute
}
void hvex_calc_xor(unsigned width, char* inA, char* inB, char* res) {
	for(unsigned i=0; i<width; i++) {
		char a = inA[i];
		char b = inB[i];
		char r = '0';

		if(a=='-' || b=='-') r = '-';
		else if(a==b) r = '0';
		else r = '1';

		res[i] = r;
	}  // Loop on the bits to compute
}
void hvex_calc_nxor(unsigned width, char* inA, char* inB, char* res) {
	for(unsigned i=0; i<width; i++) {
		char a = inA[i];
		char b = inB[i];
		char r = '0';

		if(a=='-' || b=='-') r = '-';
		else if(a==b) r = '1';
		else r = '0';

		res[i] = r;
	}  // Loop on the bits to compute
}

// Compare two literals
// Return -1 if A<B, 0 if A=B, 1 if A>B, '-' if a relevant '-' bit is found
int hvex_calc_cmpu(unsigned widthA, char* inA, unsigned widthB, char* inB) {
	int swapvalue = 1;

	if(widthB > widthA) {
		Swap(inA, inB);
		Swap(widthA, widthB);
		swapvalue = -1;
	}

	unsigned extraA = widthA - widthB;

	if(extraA > 0) {
		for(unsigned i=0; i<extraA; i++) {
			char a = *inA;
			if(a=='-') return '-';
			if(a=='1') return swapvalue;
			inA ++;
		}
	}

	for(unsigned i=0; i<widthB; i++) {
		char a = *inA;
		char b = *inB;
		assert(*inA!=0 && *inB!=0);
		if(a=='-' || b=='-') return '-';
		if(a=='1' && b=='0') return swapvalue;
		if(a=='0' && b=='1') return -swapvalue;
		inA ++;
		inB ++;
	}

	assert(*inA==0 && *inB==0);

	return 0;
}


