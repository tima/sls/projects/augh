
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <stdbool.h>

#include "hvex_misc.h"



//================================================
// Getting occurrences of Vex nodes
//================================================

// Return the matching VEX (a list of hvex_t*)
// FIXME The difference between this function and the others is not clear, and surely unnecessary
chain_list* hvex_SearchSym_AddList(chain_list* list, hvex_t* Expr, const char* name) {
	if(Expr==NULL) return list;

	// Scan all operands
	hvex_foreach(Expr->operands, VexOp) list = hvex_SearchSym_AddList(list, VexOp, name);

	// Consider local node
	if(Expr->model==HVEX_VECTOR || Expr->model==HVEX_INDEX) {
		if(name==NULL || hvex_vecidx_getname(Expr)==name) list = addchain(list, Expr);
	}

	return list;
}
chain_list* hvex_SearchVector_AddList(chain_list* list, hvex_t* Expr, const char* name) {
	if(Expr==NULL) return list;

	// Scan all operands
	hvex_foreach(Expr->operands, VexOp) list = hvex_SearchVector_AddList(list, VexOp, name);

	// Consider local node
	if(Expr->model==HVEX_VECTOR) {
		if(name==NULL || hvex_vec_getname(Expr)==name) list = addchain(list, Expr);
	}

	return list;
}
chain_list* hvex_SearchArray_AddList(chain_list* list, hvex_t* Expr, const char* name) {
	if(Expr==NULL) return list;

	// Scan operands
	hvex_foreach(Expr->operands, VexOp) list = hvex_SearchArray_AddList(list, VexOp, name);

	// Consider the current node
	if(Expr->model==HVEX_INDEX) {
		if(name==NULL || hvex_vecidx_getname(Expr)==name) list = addchain(list, Expr);
	}
	else if(Expr->model==HVEX_ASG_ADDR || Expr->model==HVEX_ASG_ADDR_EN) {
		if(name==NULL || hvex_vecidx_getname(Expr->operands)==name) list = addchain(list, Expr);
	}

	return list;
}

chain_list* hvex_SearchVectorIdx_AddList(chain_list* list, hvex_t* Expr, hvex_t* vex) {
	if(Expr==NULL) return list;

	// Scan all operands
	hvex_foreach(Expr->operands, VexOp) list = hvex_SearchVectorIdx_AddList(list, VexOp, vex);

	// Consider local node
	if(Expr->model==HVEX_VECTOR) {
		if(hvex_vec_getname(Expr)==hvex_vec_getname(vex)) {
			if(hvex_check_overlap(Expr, vex)==true) list = addchain(list, Expr);
		}
	}

	return list;
}

// Store a chain_list* of hvex_t*
void hvex_SearchAllSyms_tree(hvex_t* Expr, avl_pp_tree_t* tree, bool do_variables, bool do_arrays) {
	if(Expr==NULL) return;

	// A local utility function
	void add_tree(char* name) {
		if(tree==NULL) return;
		avl_pp_node_t* pp_node = NULL;
		bool b = avl_pp_add(tree, name, &pp_node);
		if(b==true) pp_node->data = NULL;
		pp_node->data = addchain(pp_node->data, Expr);
	}

	// Detect variables
	if(Expr->model==HVEX_VECTOR) {
		if(do_variables==true) {
			char* name = hvex_vec_getname(Expr);
			add_tree(name);
		}
		return;
	}

	else if(Expr->model==HVEX_INDEX) {
		if(do_arrays==true) {
			char* name = hvex_index_getname(Expr);
			add_tree(name);
		}
		hvex_SearchAllSyms_tree(Expr->operands, tree, do_variables, do_arrays);
	}

	else if(Expr->model==HVEX_ASG || Expr->model==HVEX_ASG_EN) {
		if(do_variables==true) {
			char* name = hvex_vec_getname(Expr->operands);
			add_tree(name);
		}
		hvex_SearchAllSyms_tree(Expr->operands, tree, do_variables, do_arrays);
		hvex_SearchAllSyms_tree(Expr->operands->next, tree, do_variables, do_arrays);
	}
	else if(Expr->model==HVEX_ASG_ADDR || Expr->model==HVEX_ASG_ADDR_EN) {
		if(do_arrays==true) {
			char* name = hvex_vec_getname(Expr->operands);
			add_tree(name);
		}
		hvex_SearchAllSyms_tree(Expr->operands, tree, do_variables, do_arrays);
		hvex_SearchAllSyms_tree(Expr->operands->next, tree, do_variables, do_arrays);
		hvex_SearchAllSyms_tree(Expr->operands->next->next, tree, do_variables, do_arrays);
	}

	else {
		hvex_foreach(Expr->operands, VexOp) hvex_SearchAllSyms_tree(VexOp, tree, do_variables, do_arrays);
	}

	return;
}

// Store symbol names
void hvex_GetSymbols_tree(hvex_t* Expr, avl_p_tree_t* tree_r, avl_p_tree_t* tree_w) {
	if(Expr==NULL) return;

	// Detect variables
	if(Expr->model==HVEX_VECTOR) {
		char* name = hvex_vec_getname(Expr);
		if(tree_r!=NULL) avl_p_add_overwrite(tree_r, name, name);
	}
	// Detect arrays
	else if(Expr->model==HVEX_INDEX) {
		char* name = hvex_index_getname(Expr);
		if(tree_r!=NULL) avl_p_add_overwrite(tree_r, name, name);
		hvex_GetSymbols_tree(Expr->operands, tree_r, tree_w);
	}

	// Detect write to variables and arrays
	else if(hvex_model_is_asg(Expr->model)==true) {
		char* name = hvex_asg_get_destname(Expr);
		if(tree_w!=NULL) avl_p_add_overwrite(tree_w, name, name);
		hvex_GetSymbols_tree(hvex_asg_get_expr(Expr), tree_r, tree_w);
		hvex_GetSymbols_tree(hvex_asg_get_addr(Expr), tree_r, tree_w);
		hvex_GetSymbols_tree(hvex_asg_get_cond(Expr), tree_r, tree_w);
	}

	// Generic case
	else {
		hvex_foreach(Expr->operands, VexOp) hvex_GetSymbols_tree(VexOp, tree_r, tree_w);
	}
}

// Get the Vex in the tree
hvex_t* hvex_treevex_get(avl_pp_tree_t* tree, char* name) {
	avl_pp_node_t* pp_node = NULL;
	bool b = avl_pp_find(tree, name, &pp_node);
	if(b==true) return pp_node->data;
	return NULL;
}
// Add a symbol in the tree as HVEX_VECTOR node, keep only min/max indexes
void hvex_treevex_addminmax(avl_pp_tree_t* tree, char* name, hvex_t* Expr) {
	avl_pp_node_t* pp_node = NULL;
	bool b = avl_pp_add(tree, name, &pp_node);
	if(b==true) pp_node->data = hvex_newvec(name, Expr->left, Expr->right);
	else {
		hvex_t* vex = pp_node->data;
		hvex_set_minmax(vex, Expr);
	}
}
// Clear a tree of Vex expressions
void hvex_treevex_reset(avl_pp_tree_t* tree) {
	avl_pp_foreach(tree, scan) hvex_free(scan->data);
	avl_pp_reset(tree);
}
void hvex_treevex_clear(avl_pp_tree_t* tree) {
	avl_pp_foreach(tree, scan) hvex_free(scan->data);
}

// Store one VECTOR per symbol name, set leftmost and rightmost indexes. Trees can be NULL.
void hvex_GetAllSymbols_treevex(hvex_t* Expr, avl_pp_tree_t* tree_r, avl_pp_tree_t* tree_w) {
	if(Expr==NULL) return;

	// Detect variables
	if(Expr->model==HVEX_VECTOR || Expr->model==HVEX_INDEX) {
		if(tree_r!=NULL) {
			char* name = hvex_vec_getname(Expr);
			hvex_treevex_addminmax(tree_r, name, Expr);
		}
		// Scan arguments, in case it is INDEX
		hvex_foreach(Expr->operands, VexOp) hvex_GetAllSymbols_treevex(VexOp, tree_r, tree_w);
	}

	// Detect arrays
	else if(hvex_model_is_asg(Expr->model)==true) {
		if(tree_w!=NULL) {
			hvex_t* vex_dest = hvex_asg_get_dest(Expr);
			char* name = hvex_vecidx_getname(vex_dest);
			assert(name!=NULL);
			hvex_treevex_addminmax(tree_w, name, vex_dest);
		}
		// Process other ASG operands
		hvex_GetAllSymbols_treevex(hvex_asg_get_addr(Expr), tree_r, tree_w);
		hvex_GetAllSymbols_treevex(hvex_asg_get_expr(Expr), tree_r, tree_w);
		hvex_GetAllSymbols_treevex(hvex_asg_get_cond(Expr), tree_r, tree_w);
	}

	// Generic case
	else {
		hvex_foreach(Expr->operands, VexOp) hvex_GetAllSymbols_treevex(VexOp, tree_r, tree_w);
	}
}

// Store one HVEX_VECTOR per symbol name, set leftmost and rightmost indexes
void hvex_SearchVectors_treevex(hvex_t* Expr, avl_pp_tree_t* tree) {
	if(Expr==NULL) return;

	// Scan all operands
	hvex_foreach(Expr->operands, VexOp) hvex_SearchVectors_treevex(VexOp, tree);

	// Detect variables
	if(Expr->model==HVEX_VECTOR) {
		hvex_treevex_addminmax(tree, hvex_vec_getname(Expr), Expr);
		return;
	}
}



//================================================
// Miscellaneous
//================================================

// Return the VEX: test AND expr
// The test is assumed to be 1-bit large.
// Important: the input VEX are not duplicated.
hvex_t* hvex_ExprAndTest(hvex_t* expr, hvex_t* test) {
	if(expr->width>1) test = hvex_newmodel_op1(HVEX_REPEAT, expr->width, test);
	return hvex_newmodel_op2(HVEX_AND, expr->width, test, expr);
}

// Default result is false (the vex is read)
bool hvex_IsWriteOperation(hvex_t* vex) {
	if(vex->model==HVEX_VECTOR) {
		// Get the father vex to check whether it's an assignment
		if(hvex_is_father_hvex(vex)==false) return false;
		hvex_t* vexfather = vex->father;
		if(vexfather==NULL) return false;
		if(
			vexfather->model!=HVEX_ASG && vexfather->model!=HVEX_ASG_EN &&
			vexfather->model!=HVEX_ASG_ADDR && vexfather->model!=HVEX_ASG_ADDR_EN
		) return false;
		// Here the father vex is an assignment operation. Check whether the dest is the current vex.
		if(hvex_asg_get_dest(vexfather)==vex) return true;
		return false;
	}
	else if(vex->model==HVEX_INDEX) return false;
	else if(vex->model==HVEX_ASG || vex->model==HVEX_ASG_EN) return true;
	else if(vex->model==HVEX_ASG_ADDR || vex->model==HVEX_ASG_ADDR_EN) return true;
	return false;
}

// Note: Inputs are not modified.
// Modify the input Expr so that the indexes included in prev_dest are replaced
//   with the corresponding sub-expression from prev_expr
// expr and prev_dest must be HVEX_VECTOR nodes with the same symbol
hvex_t* hvex_ReplaceSubExpr_FromPrevExpr_dup(hvex_t* Expr, hvex_t* prev_dest, hvex_t* prev_expr) {
	assert(Expr->model==HVEX_VECTOR && prev_dest->model==HVEX_VECTOR && hvex_vec_getname(Expr)==hvex_vec_getname(prev_dest));

	// Check overlap
	if(hvex_check_overlap(Expr, prev_dest)==false) return hvex_dup(Expr);

	hvex_t* new_expr = hvex_dup(prev_expr);

	// Crop the prev_expr so it includes only the range in Expr:
	//     | Expr |
	//  | prev_dest |
	unsigned crop_left = 0;
	unsigned crop_right = 0;
	if(Expr->left < prev_dest->left) crop_left = prev_dest->left - Expr->left;
	if(Expr->right > prev_dest->right) crop_right = Expr->right - prev_dest->right;

	if(crop_left > 0 || crop_right > 0) hvex_croprel(new_expr, crop_left, crop_right);

	// Handle when there is no CONCAT to create
	if(hvex_check_inclusion(prev_dest, Expr)==true) {
		hvex_sign_copy(new_expr, Expr);
		return new_expr;
	}

	// Here we will have at least two operands
	// The final operands to a CONCAT operation
	hvex_t* vex_concat = hvex_newmodel(HVEX_CONCAT, Expr->width);
	hvex_sign_copy(vex_concat, Expr);

	// Handle the right side
	if(Expr->right < prev_dest->right) {
		//  ...   Expr      |
		//  ... prev_dest |
		hvex_t* new_op = hvex_new_cropabs(hvex_dup(Expr), prev_dest->right-1, Expr->right);
		hvex_addop_head(vex_concat, new_op);
	}

	// Handle the center section
	hvex_addop_head(vex_concat, new_expr);

	// Handle the left side
	if(Expr->left > prev_dest->left) {
		// |    Expr     ...
		//   | prev_dest ...
		hvex_t* new_op = hvex_new_cropabs(hvex_dup(Expr), Expr->left, prev_dest->left+1);
		hvex_addop_head(vex_concat, new_op);
	}

	return vex_concat;
}
// Note: the input Expr is modified/freed inside this function
// Other input arguments are not modified
void hvex_ReplaceSubExpr_FromPrevExpr(hvex_t* Expr, hvex_t* prev_dest, hvex_t* prev_expr) {
	hvex_t* new_expr = hvex_ReplaceSubExpr_FromPrevExpr_dup(Expr, prev_dest, prev_expr);
	hvex_replace_free(Expr, new_expr);
}
// Replace all occurrences, even in sub-expressions
void hvex_ReplaceSubExpr_FromPrevExpr_All(hvex_t* Expr, hvex_t* prev_dest, hvex_t* prev_expr) {
	assert(prev_dest->model==HVEX_VECTOR);
	char* name = hvex_vec_getname(prev_dest);

	if(Expr->model==HVEX_VECTOR) {
		char* locname = hvex_vec_getname(Expr);
		if(locname==name) {
			hvex_ReplaceSubExpr_FromPrevExpr(Expr, prev_dest, prev_expr);
		}
		return;
	}

	hvex_foreach(Expr->operands, VexOp) {
		hvex_ReplaceSubExpr_FromPrevExpr_All(VexOp, prev_dest, prev_expr);
	}
}

// Important: expr is not duplicated, the other inputs are duplicated.
// The extensions, left and right, are built with the specified symbol and address (if not null)
// The extensions correspond to the difference of indexes between wanted_dest and this_dest
hvex_t* hvex_Resize_CatSym_FromVex(hvex_t* expr, hvex_t* this_dest, hvex_t* wanted_dest, const char* name, hvex_t* addr) {
	// In case indexes don't overlap
	if(hvex_check_overlap(wanted_dest, this_dest)==false) {
		return hvex_ReadSym(name, hvex_dup(addr), wanted_dest->left, wanted_dest->right);
	}
	// Crop
	unsigned crop_left  = wanted_dest->left  < this_dest->left  ? this_dest->left    - wanted_dest->left : 0;
	unsigned crop_right = wanted_dest->right > this_dest->right ? wanted_dest->right - this_dest->right : 0;
	if(crop_left>0 || crop_right>0) {
		hvex_croprel(expr, crop_left, crop_right);
	}
	// Extend
	if(wanted_dest->left > this_dest->left || wanted_dest->right < this_dest->right) {
		hvex_t* this_cat = hvex_newmodel(HVEX_CONCAT, wanted_dest->width);
		if(wanted_dest->right < this_dest->right) {
			hvex_t* this_elt = hvex_ReadSym(name, hvex_dup(addr), this_dest->right-1, wanted_dest->right);
			hvex_addop_head(this_cat, this_elt);
		}
		hvex_addop_head(this_cat, expr);
		if(wanted_dest->left > this_dest->left) {
			hvex_t* this_elt = hvex_ReadSym(name, hvex_dup(addr), wanted_dest->left, this_dest->left+1);
			hvex_addop_head(this_cat, this_elt);
		}
		expr = this_cat;
	}
	return expr;
}

// Warning: The address, if not NULL, is not duplicated.
hvex_t* hvex_ReadSym(const char* name, hvex_t* addr, unsigned left, unsigned right) {
	hvex_t* vex = hvex_newvec(name, left, right);
	if(addr==NULL) return vex;
	vex->model = HVEX_INDEX;
	hvex_addop_head(vex, addr);
	return vex;
}

hvex_t* hvex_DupFullDest(hvex_t* Expr) {
	hvex_t* vex_dest = hvex_asg_get_dest(Expr);
	hvex_t* vex_addr = hvex_asg_get_addr(Expr);

	if(vex_dest!=NULL) {
		if(vex_addr!=NULL) {
			assert(vex_dest->model==HVEX_VECTOR);
			vex_dest = hvex_dup(vex_dest);
			vex_dest->model = HVEX_INDEX;
			hvex_addop_head(vex_dest, hvex_dup(vex_addr));
			return vex_dest;
		}
		else return hvex_dup(vex_dest);
	}

	return NULL;
}



//================================================
// Extended comparison of Vex
//================================================

bool hvex_expensive_cmp_en = false;

// Define the return values of the comparison functions
#define HVEX_COMPARE_EQUAL    0  // Expressions are always aqual
#define HVEX_COMPARE_DIFF     1  // Expressions are guaranteed to always be different
#define HVEX_COMPARE_MAYBE   -1  // There exists a case where expressions are equal, guaranteed
#define HVEX_COMPARE_NOAPPL  -2  // The function can't conclude, need to launch other check functions

// Check that all values of that Vex can be computed as literals
static bool hvex_DataLiteral_Check(hvex_t* vex) {
	if(vex->model==HVEX_LITERAL) return true;
	else if(vex->model==HVEX_CONCAT || vex->model==HVEX_REPEAT || vex->model==HVEX_RESIZE) {
		hvex_foreach(vex->operands, vexOp) {
			bool b = hvex_DataLiteral_Check(vexOp);
			if(b==false) return false;
		}
	}
	else if(vex->model==HVEX_MUXB) {
		hvex_foreach(vex->operands->next, vexOp) {
			bool b = hvex_DataLiteral_Check(vexOp);
			if(b==false) return false;
		}
	}
	else if(vex->model==HVEX_MUXDEC) {
		hvex_foreach(vex->operands, vexOp) {
			assert(vexOp->model==HVEX_MUXDECIN);
			bool b = hvex_DataLiteral_Check(vexOp->operands->next);
			if(b==false) return false;
		}
	}
	return false;
}

// Algorithm:
// Fill the tree all possible values that this Vex can take
// Store the namealloc'd image of all literal strings
static void hvex_DataLiteral_AllAddTree(hvex_t* vex, avl_p_tree_t* tree) {

	if(vex->model==HVEX_LITERAL) {
		char* lit = hvex_lit_getval(vex);
		avl_p_add_overwrite(tree, lit, lit);
	}

	else if(vex->model==HVEX_CONCAT) {
		avl_p_tree_t tree_cur;
		avl_p_tree_t tree_op;
		avl_p_init(&tree_cur);
		avl_p_init(&tree_op);
		char buf[vex->width+1];
		unsigned offset = 0;
		// Separately handle the first operand
		hvex_DataLiteral_AllAddTree(vex->operands, tree);
		// Iteratively compute the list of all combinations of operand values
		hvex_foreach(vex->operands->next, vexOp) {
			hvex_DataLiteral_AllAddTree(vexOp, &tree_op);
			// Prepare the tree contents
			avl_p_dup(&tree_cur, tree);
			avl_p_reset(tree);
			// Append all versions of the operand to the previously-built literals
			avl_p_foreach(&tree_cur, scanCurLit) {
				strcpy(buf, scanCurLit->data);
				avl_p_foreach(&tree_op, scanCurOp) {
					strcpy(buf+offset, scanCurOp->data);
					buf[offset+vexOp->width] = 0;
					char* lit = namealloc(buf);
					avl_p_add_overwrite(tree, lit, lit);
				}
			}
		}
		// Clean
		avl_p_reset(&tree_cur);
		avl_p_reset(&tree_op);
	}

	else if(vex->model==HVEX_REPEAT) {
		avl_p_tree_t tree_op;
		avl_p_init(&tree_op);
		char buf[vex->width+1];
		// Get all version of the operand
		hvex_DataLiteral_AllAddTree(vex->operands, &tree_op);
		// Create all full version
		avl_p_foreach(&tree_op, scanOp) {
			char* lit = scanOp->data;
			for(unsigned i=0; i<vex->width; i++) buf[i] = lit[0];
			buf[vex->width] = 0;
			lit = namealloc(buf);
			avl_p_add_overwrite(tree, lit, lit);
		}
		// Clean
		avl_p_reset(&tree_op);
	}

	else if(vex->model==HVEX_RESIZE) {
		avl_p_tree_t tree_op;
		avl_p_init(&tree_op);
		char buf[vex->width+1];
		// Get all version of the operand
		hvex_DataLiteral_AllAddTree(vex->operands, &tree_op);
		// Create all full version
		avl_p_foreach(&tree_op, scanOp) {
			char* lit = scanOp->data;
			char pad = vex->pad==0 ? lit[0] : vex->pad;
			for(unsigned i=0; i<vex->width-1; i++) buf[i] = pad;
			buf[vex->width-1] = lit[0];
			buf[vex->width] = 0;
			lit = namealloc(buf);
			avl_p_add_overwrite(tree, lit, lit);
		}
		// Clean
		avl_p_reset(&tree_op);
	}

	else if(vex->model==HVEX_MUXB) {
		avl_p_tree_t tree_op;
		avl_p_init(&tree_op);
		// Merge all versions of the operands
		hvex_foreach(vex->operands->next, vexOp) {
			// Get all versions of the operands
			hvex_DataLiteral_AllAddTree(vexOp, &tree_op);
			// Merge
			avl_p_foreach(&tree_op, scanCurOp) {
				char* lit = scanCurOp->data;
				avl_p_add_overwrite(tree, lit, lit);
			}
			avl_p_reset(&tree_op);
		}
	}

	else if(vex->model==HVEX_MUXDEC) {
		avl_p_tree_t tree_op;
		avl_p_init(&tree_op);
		// Merge all versions of the operands
		hvex_foreach(vex->operands, vexOp) {
			hvex_t* dataop = vexOp->operands->next;
			// Get all versions of the operands
			hvex_DataLiteral_AllAddTree(dataop, &tree_op);
			// Merge
			avl_p_foreach(&tree_op, scanCurOp) {
				char* lit = scanCurOp->data;
				avl_p_add_overwrite(tree, lit, lit);
			}
			avl_p_reset(&tree_op);
		}
	}

	else {
		abort();
	}

}

// Check collision of values in the trees
static bool hvex_DataLiteral_CheckCollisions(avl_p_tree_t* tree1, avl_p_tree_t* tree2) {
	avl_p_foreach(tree1, scan) {
		char* lit = scan->data;
		if(avl_p_isthere(tree2, lit)==true) return true;
	}
	return false;
}

// Expensive comparison
// Handle 2 expressions of the form: <common> +/- literals
// Return value : 0 if equal, >0 if different, <0 if unknown.
static int hvex_CompareExp_AddSub_CommonOp(hvex_t* vex1, hvex_t* vex2) {
	// Misc checks
	if(vex1->model != vex2->model) return HVEX_COMPARE_NOAPPL;
	if(vex1->model != HVEX_ADD && vex1->model != HVEX_SUB) return HVEX_COMPARE_NOAPPL;
	if(hvex_countops(vex1)!=2 || hvex_countops(vex2)!=2) return HVEX_COMPARE_NOAPPL;

	// Get the common operator and the candidate "literals"
	hvex_t* vex1_op1 = vex1->operands;
	hvex_t* vex1_op2 = vex1->operands->next;
	hvex_t* vex2_op1 = vex2->operands;
	hvex_t* vex2_op2 = vex2->operands->next;

	hvex_t* litop1 = NULL;
	hvex_t* litop2 = NULL;

	if(hvex_cmp(vex1_op1, vex2_op1)==0) {
		litop1 = vex1_op2;
		litop2 = vex2_op2;
	}
	else if(hvex_cmp(vex1_op1, vex2_op2)==0) {
		litop1 = vex1_op2;
		litop2 = vex2_op1;
	}
	else if(hvex_cmp(vex1_op2, vex2_op1)==0) {
		litop1 = vex1_op1;
		litop2 = vex2_op2;
	}
	else if(hvex_cmp(vex1_op2, vex2_op2)==0) {
		litop1 = vex1_op1;
		litop2 = vex2_op1;
	}
	else {
		//dbgprintf("No common operand found\n");
		return HVEX_COMPARE_NOAPPL;
	}

	// Check that both "literal" operands can be correctly evaluated
	bool b;
	b = hvex_DataLiteral_Check(litop1);
	if(b==false) return HVEX_COMPARE_NOAPPL;
	b = hvex_DataLiteral_Check(litop2);
	if(b==false) return HVEX_COMPARE_NOAPPL;

	// Now get all possible values of both "literal operands"
	avl_p_tree_t tree_val1;
	avl_p_tree_t tree_val2;
	avl_p_init(&tree_val1);
	avl_p_init(&tree_val2);

	hvex_DataLiteral_AllAddTree(litop1, &tree_val1);
	hvex_DataLiteral_AllAddTree(litop2, &tree_val2);

	#if 0
	dbgprintf("Literal values from function %s:\n", __func__);
	avl_p_foreach(&tree_val1, scan) dbgprintf("val1: %s\n", (char*)scan->data);
	avl_p_foreach(&tree_val2, scan) dbgprintf("val2: %s\n", (char*)scan->data);
	#endif

	// Check collisions
	bool collision = hvex_DataLiteral_CheckCollisions(&tree_val1, &tree_val2);

	avl_p_reset(&tree_val1);
	avl_p_reset(&tree_val2);

	if(collision==true) return HVEX_COMPARE_MAYBE;
	return HVEX_COMPARE_DIFF;
}

// Expensive comparison
// Here we handle only one vex "vex" and another "vex +/- op"
// Return value : 0 if equal, >0 if different, <0 if unknown.
static int hvex_CompareExp_AddSub_OneIsOp(hvex_t* possible_add, hvex_t* possible_common) {
	// Paranioa checks
	if(possible_add->model!=HVEX_ADD && possible_add->model!=HVEX_SUB) return HVEX_COMPARE_NOAPPL;
	if(hvex_countops(possible_add)!=2) return HVEX_COMPARE_NOAPPL;

	hvex_t* other_op = NULL;

	// Find the other operand of the add/sub
	if(hvex_cmp(possible_add->operands, possible_common)==0) {
		other_op = possible_add->operands->next;
	}
	else if(hvex_cmp(possible_add->operands->next, possible_common)==0) {
		other_op = possible_add->operands;
	}
	if(other_op==NULL) return HVEX_COMPARE_NOAPPL;

	// Try to get all values the other operand can take
	avl_p_tree_t tree_val;
	avl_p_init(&tree_val);

	hvex_DataLiteral_AllAddTree(other_op, &tree_val);

	#if 0
	dbgprintf("Literal values from function %s:\n", __func__);
	avl_p_foreach(&tree_val, scan) dbgprintf("val: %s\n", (char*)scan->data);
	#endif

	// Check collision: only check if the value zero is in the tree
	char* value_zero = stralloc_repeat('0', other_op->width);
	bool found = avl_p_isthere(&tree_val, value_zero);
	unsigned nbval = avl_p_count(&tree_val);

	avl_p_reset(&tree_val);

	if(found==true) {
		if(nbval==1) return HVEX_COMPARE_EQUAL;
		else return HVEX_COMPARE_MAYBE;
	}
	return HVEX_COMPARE_DIFF;
}

// Expensive comparison
// Here we handle only one vex "vex" and another is MUXB where each data operand is "vex +/- op"
// Return value : 0 if equal, >0 if different, <0 if unknown.
static int hvex_CompareExp_MuxB_OneIsOp(hvex_t* possible_mux, hvex_t* possible_common) {
	// Paranioa checks
	if(possible_mux->model!=HVEX_MUXB) return HVEX_COMPARE_NOAPPL;

	hvex_foreach(possible_mux->operands->next, vexOp) {
		int z = hvex_Compare(vexOp, possible_common);
		if(z != HVEX_COMPARE_DIFF) return HVEX_COMPARE_NOAPPL;
	}

	#if 0
	dbgprintf("Guaranteed different!\n");
	#endif

	return HVEX_COMPARE_DIFF;
}
// Same but with MUXDEC
static int hvex_CompareExp_MuxDec_OneIsOp(hvex_t* possible_mux, hvex_t* possible_common) {
	// Paranioa checks
	if(possible_mux->model!=HVEX_MUXDEC) return HVEX_COMPARE_NOAPPL;

	hvex_foreach(possible_mux->operands, vexOp) {
		assert(vexOp->model==HVEX_MUXDECIN);
		int z = hvex_Compare(vexOp->operands->next, possible_common);
		if(z != HVEX_COMPARE_DIFF) return HVEX_COMPARE_NOAPPL;
	}

	#if 0
	dbgprintf("Guaranteed different!\n");
	#endif

	return HVEX_COMPARE_DIFF;
}

// Non-expensive comparison
// Here we handle only one vex "vex" and another "vex +/- cst"
// So one vex should be an operand of the other.
static int hvex_Compare_AddSub_OneIsOp(hvex_t* possible_add, hvex_t* possible_common) {
	// Paranioa checks
	if(possible_add->model!=HVEX_ADD && possible_add->model!=HVEX_SUB) return HVEX_COMPARE_NOAPPL;

	hvex_t* vex_common = NULL;
	hvex_t* vex_cst = NULL;

	// Try the first VEX as the common VEX
	hvex_foreach(possible_add->operands, vex_op) {
		if(vex_op->model==HVEX_LITERAL) {
			if(vex_cst!=NULL) return HVEX_COMPARE_NOAPPL;
			if(hvex_is_signed(vex_op)==true) return HVEX_COMPARE_NOAPPL;
			vex_cst = vex_op;
		}
		else {
			if(vex_common!=NULL) return HVEX_COMPARE_NOAPPL;
			if(hvex_cmp(vex_op, possible_common)!=0) return HVEX_COMPARE_NOAPPL;
			vex_common = vex_op;
		}
	}  // Scan the operands of the ADD expression
	if(vex_cst==NULL || vex_common==NULL) return HVEX_COMPARE_NOAPPL;
	// FIXME All signed extensions could actually be handled
	if(hvex_is_unsigned(vex_cst)==false) return HVEX_COMPARE_NOAPPL;

	// Get the position of the most and least significant bits. Scan left-to-right.
	signed int msb_pos = -1;
	signed int lsb_pos = -1;
	char* ptr = hvex_lit_getval(vex_cst);
	for( ; (*ptr)!=0; ptr++) {
		if(*ptr=='0') {
			if(msb_pos>=0) msb_pos++;
			if(lsb_pos>=0) lsb_pos++;
			continue;
		}
		else {
			if(msb_pos<0) msb_pos = 0; else msb_pos++;
			lsb_pos = 0;
		}
	}

	// Operation with zero : Same expressions.
	if(lsb_pos<0) return HVEX_COMPARE_EQUAL;
	// Overflow but still same result.
	if(lsb_pos>=vex_common->width) return HVEX_COMPARE_EQUAL;
	// The two expressions are definitely different.
	if(lsb_pos<vex_common->width) return HVEX_COMPARE_DIFF;

	// Unknown... (never happens here but this is for code genericity)
	return HVEX_COMPARE_NOAPPL;
}

// Special Vex comparison function
// It tries to determine whether the two Vex are guaranteed to always have dirrerent values
// Return value: 0 if equal, >0 if different, <0 if unknown.
int hvex_Compare(hvex_t* expr0, hvex_t* expr1) {

	// Get rid with the case where expressions are equal
	if(hvex_cmp(expr0, expr1)==0) return HVEX_COMPARE_EQUAL;

	// FIXME Different width is still OK if both are literals
	if(expr0->width!=expr1->width) return HVEX_COMPARE_NOAPPL;

	if(hvex_expensive_cmp_en==true) {
		int z;

		// Try both vex as: <common> +/- lit
		z = hvex_CompareExp_AddSub_CommonOp(expr0, expr1);
		if(z != HVEX_COMPARE_NOAPPL) return z;

		// Try when one expression is an operand of the other, which should be ADD/SUB
		z = hvex_CompareExp_AddSub_OneIsOp(expr0, expr1);
		if(z != HVEX_COMPARE_NOAPPL) return z;
		z = hvex_CompareExp_AddSub_OneIsOp(expr1, expr0);
		if(z != HVEX_COMPARE_NOAPPL) return z;

		// Try when one expression is an operand of the other, which should be MUXB
		z = hvex_CompareExp_MuxB_OneIsOp(expr0, expr1);
		if(z != HVEX_COMPARE_NOAPPL) return z;
		z = hvex_CompareExp_MuxB_OneIsOp(expr1, expr0);
		if(z != HVEX_COMPARE_NOAPPL) return z;
		// Same with MUXDEC
		z = hvex_CompareExp_MuxDec_OneIsOp(expr0, expr1);
		if(z != HVEX_COMPARE_NOAPPL) return z;
		z = hvex_CompareExp_MuxDec_OneIsOp(expr1, expr0);
		if(z != HVEX_COMPARE_NOAPPL) return z;
	}

	// Here we handle only one vex "vex" and another "vex +/- cst"
	// So one vex should be an operand of the other.
	int z;
	z = hvex_Compare_AddSub_OneIsOp(expr0, expr1);
	if(z != HVEX_COMPARE_NOAPPL) return z;
	z = hvex_Compare_AddSub_OneIsOp(expr1, expr0);
	if(z != HVEX_COMPARE_NOAPPL) return z;

	// Handle all situations where the two VEX have same model
	if(expr0->model!=expr1->model) return HVEX_COMPARE_NOAPPL;
	hvex_model_t* model = expr0->model;

	if(model==HVEX_LITERAL) {
		// Check sign extension
		// FIXME Often if the 2 literals have same size, when they re operands we don't care of the sign...
		if(expr0->pad != expr1->pad) return HVEX_COMPARE_NOAPPL;
		// Check the common width
		char* val0 = hvex_lit_getval(expr0);
		char* val1 = hvex_lit_getval(expr1);
		unsigned minwidth = 0;
		unsigned extwidth = 0;
		char* largerval = NULL;
		char* shorterval = NULL;
		if(expr0->width > expr1->width) {
			minwidth = expr1->width;
			extwidth = expr0->width - expr1->width;
			largerval = val0;
			shorterval = val1;
		}
		else {
			minwidth = expr0->width;
			extwidth = expr1->width - expr0->width;
			largerval = val1;
			shorterval = val0;
		}
		if(strcmp(val0 + expr0->width - minwidth, val1 + expr1->width - minwidth)!=0) return HVEX_COMPARE_DIFF;
		if(extwidth==0) return HVEX_COMPARE_EQUAL;
		// Check the sign extension
		char pad = expr0->pad==0 ? shorterval[0] : expr0->pad;
		for(unsigned i=0; i<extwidth; i++) if(largerval[i] != pad) return HVEX_COMPARE_DIFF;
		return HVEX_COMPARE_EQUAL;
	}

	else if(model==HVEX_VECTOR || model==HVEX_INDEX) {
		return HVEX_COMPARE_MAYBE;
	}

	else if(model==HVEX_CONCAT) {
		if(hvex_countops(expr0)!=hvex_countops(expr1)) return HVEX_COMPARE_NOAPPL;

		hvex_t* operands0 = expr0->operands;
		hvex_t* operands1 = expr1->operands;

		while(operands0!=NULL && operands1!=NULL) {
			int z = hvex_Compare(operands0, operands1);
			if(z==HVEX_COMPARE_DIFF) return HVEX_COMPARE_DIFF;
			if(z!=HVEX_COMPARE_EQUAL) return HVEX_COMPARE_NOAPPL;
			// Next operands
			operands0 = operands0->next;
			operands1 = operands1->next;
		}

		if(operands1!=NULL || operands0!=NULL) return HVEX_COMPARE_NOAPPL;  // Paranoia: Not the same number of operands
		return HVEX_COMPARE_EQUAL;
	}

	// Here it is unknown whether the two expressions can have equal or different values.
	return HVEX_COMPARE_NOAPPL;
}



//================================================
// Lists of Vex expressions
//================================================

void VexList_Free(chain_list* chain) {
	foreach(chain, scan) hvex_free(scan->DATA);
	freechain(chain);
}
chain_list* VexList_Dup(chain_list* chain) {
	chain_list* newchain = dupchain(chain);
	foreach(newchain, scan) scan->DATA = hvex_dup(scan->DATA);
	return newchain;
}

bool VexList_IsHere_SameOper(chain_list* list, hvex_t* data) {
	// FIXME Missing handling of commutativity of operands, etc
	foreach(list, scan) if(hvex_cmp(scan->DATA, data)==0) return true;
	return false;
}

chain_list* VexList_Merge_Dup(chain_list* dest, chain_list* src) {
	foreach(src, scan) {
		if(VexList_IsHere_SameOper(dest, scan->DATA)==false) {
			dest = addchain(dest, hvex_dup(scan->DATA));
		}
	}
	return dest;
}

// Return true if the vex was found and removed
static bool VexList_DelIfHere_SameOper_internal(chain_list** plist, hvex_t* expr, bool vexfree) {
	chain_list* list = *plist;

	chain_list* scan_prev = NULL;
	foreach(list, scan) {
		hvex_t* expr_list = scan->DATA;

		// Comparison
		int b = hvex_cmp(expr_list, expr);  // FIXME it doesn't handle operands commutativity, etc
		if(b!=0) { scan_prev = scan; continue; }

		// Removal
		chain_list* scan_next = scan->NEXT;
		scan->NEXT = NULL;
		if(vexfree==true) hvex_free(expr_list);
		freechain(scan);
		if(scan_prev==NULL) *plist = scan_next;
		else scan_prev->NEXT = scan_next;

		return true;
	}

	return false;
}
// Return >0 if elements in list0 are not in list1 (checked first)
//        <0 if elements in list1 are not in list0
int VexList_Compare_SameOper(chain_list* list0, chain_list* list1) {
	// Work on a duplicate of list1
	chain_list* duplist1 = dupchain(list1);

	// One by one, scan list0, find expr and delete them in duplist1
	foreach(list0, scan0) {
		hvex_t* expr0 = scan0->DATA;
		bool b = VexList_DelIfHere_SameOper_internal(&duplist1, expr0, false);
		if(b==false) { freechain(duplist1); return 1; }
	}

	// If duplist1 is not empty, it contains more elements than list0
	if(duplist1!=NULL) {
		freechain(duplist1);
		return -1;
	}

	return 0;
}



//================================================
// Print a VHDL condition
//================================================

void hvex_PrintVHDL_Width1(FILE* F, hvex_t* Expr, bool withpar) {
	assert(Expr->width==1);
	if(withpar==true) fprintf(F, "(");

	if(Expr->model==HVEX_LITERAL) {
		fprintf(F, "'%c'", hvex_lit_getval(Expr)[0]);
	}
	else if(Expr->model==HVEX_VECTOR) {
		fprintf(F, "%s", hvex_vec_getname(Expr));
	}

	else if(Expr->model==HVEX_NOT) {
		fprintf(F, "not ");
		hvex_PrintVHDL_Width1(F, Expr->operands, true);
	}

	else if(
		Expr->model==HVEX_AND || Expr->model==HVEX_OR || Expr->model==HVEX_XOR ||
		Expr->model==HVEX_NAND || Expr->model==HVEX_NOR || Expr->model==HVEX_NXOR
	) {
		bool do_not = false;
		char* print_oper = NULL;
		if(Expr->model==HVEX_NAND || Expr->model==HVEX_NOR || Expr->model==HVEX_NXOR) do_not = true;

		if     (Expr->model==HVEX_AND || Expr->model==HVEX_NAND) print_oper = "and";
		else if(Expr->model==HVEX_OR  || Expr->model==HVEX_NOR)  print_oper = "or";
		else if(Expr->model==HVEX_XOR || Expr->model==HVEX_NXOR) print_oper = "xor";
		else abort();

		if(do_not==true) fprintf(F, "not(");
		unsigned nb = 0;
		hvex_foreach(Expr->operands, VexOp) {
			if(nb > 0) fprintf(F, " %s ", print_oper);
			hvex_PrintVHDL_Width1(F, VexOp, true);
			nb ++;
		}
		if(do_not==true) fprintf(F, ")");
	}

	else if(Expr->model==HVEX_EQ) {
		unsigned nb = 0;
		// Either all bits are at '1'...
		fprintf(F, "(");
		nb = 0;
		hvex_foreach(Expr->operands, VexOp) {
			if(nb > 0) fprintf(F, " and ");
			hvex_PrintVHDL_Width1(F, VexOp, true);
			nb ++;
		}
		// ... or all bits are at '0'
		fprintf(F, ") or (");
		nb = 0;
		hvex_foreach(Expr->operands, VexOp) {
			if(nb > 0) fprintf(F, " and ");
			fprintf(F, "not ");
			hvex_PrintVHDL_Width1(F, VexOp, true);
			nb ++;
		}
		fprintf(F, ")");
	}
	else if(Expr->model==HVEX_NE) {
		unsigned nb = 0;
		fprintf(F, "not(");
		nb = 0;
		hvex_foreach(Expr->operands, VexOp) {
			if(nb > 0) fprintf(F, " and ");
			hvex_PrintVHDL_Width1(F, VexOp, true);
			nb ++;
		}
		fprintf(F, ") and not(");
		nb = 0;
		hvex_foreach(Expr->operands, VexOp) {
			if(nb > 0) fprintf(F, " and ");
			fprintf(F, "not ");
			hvex_PrintVHDL_Width1(F, VexOp, true);
			nb ++;
		}
		fprintf(F, ")");
	}

	else {
		errprintfa("Can't print HVEX mode '%s' as one-line VHDL\n", Expr->model->name);
	}

	if(withpar==true) fprintf(F, ")");
}
void hvex_PrintVHDL_cond(FILE* F, hvex_t* Expr, bool withpar) {
	assert(Expr->width==1);
	if(withpar==true) fprintf(F, "(");
	hvex_PrintVHDL_Width1(F, Expr, true);
	fprintf(F, " = '1'");
	if(withpar==true) fprintf(F, ")");
}


