
#ifndef _HVEX_MISC_H_
#define _HVEX_MISC_H_

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>

#include <assert.h>

#include "../auto/auto.h"


extern bool hvex_expensive_cmp_en;

// Define the return values of the special comparison functions
#define HVEX_COMPARE_EQUAL    0  // Expressions are always aqual
#define HVEX_COMPARE_DIFF     1  // Expressions are guaranteed to always be different
#define HVEX_COMPARE_MAYBE   -1  // There exists a case where expressions are equal, guaranteed
#define HVEX_COMPARE_NOAPPL  -2  // The function can't conclude, need to launch other check functions


chain_list* hvex_SearchSym_AddList(chain_list* list, hvex_t* Expr, const char* name);
chain_list* hvex_SearchVector_AddList(chain_list* list, hvex_t* Expr, const char* name);
chain_list* hvex_SearchArray_AddList(chain_list* list, hvex_t* Expr, const char* name);

chain_list* hvex_SearchVectorIdx_AddList(chain_list* list, hvex_t* Expr, hvex_t* vex);

void hvex_SearchAllSyms_tree(hvex_t* Expr, avl_pp_tree_t* tree, bool do_variables, bool do_arrays);

void hvex_GetSymbols_tree(hvex_t* Expr, avl_p_tree_t* tree_r, avl_p_tree_t* tree_w);
void hvex_GetAllSymbols_treevex(hvex_t* Expr, avl_pp_tree_t* tree_r, avl_pp_tree_t* tree_w);

hvex_t* hvex_treevex_get(avl_pp_tree_t* tree, char* name);
void hvex_SearchVectors_treevex(hvex_t* Expr, avl_pp_tree_t* tree);
void hvex_treevex_reset(avl_pp_tree_t* tree);
void hvex_treevex_clear(avl_pp_tree_t* tree);

hvex_t* hvex_ExprAndTest(hvex_t* expr, hvex_t* test);

bool hvex_IsWriteOperation(hvex_t* vex);

hvex_t* hvex_ReplaceSubExpr_FromPrevExpr_dup(hvex_t* Expr, hvex_t* prev_dest, hvex_t* prev_expr);
void hvex_ReplaceSubExpr_FromPrevExpr(hvex_t* Expr, hvex_t* prev_dest, hvex_t* prev_expr);
void hvex_ReplaceSubExpr_FromPrevExpr_All(hvex_t* Expr, hvex_t* prev_dest, hvex_t* prev_expr);

hvex_t* hvex_Resize_CatSym_FromVex(hvex_t* expr, hvex_t* this_dest, hvex_t* wanted_dest, const char* name, hvex_t* addr);

hvex_t* hvex_ReadSym(const char* name, hvex_t* addr, unsigned left, unsigned right);
hvex_t* hvex_DupFullDest(hvex_t* Expr);

int hvex_Compare(hvex_t* expr0, hvex_t* expr1);

void VexList_Free(chain_list* chain);
chain_list* VexList_Dup(chain_list* chain);

bool VexList_IsHere_SameOper(chain_list* list, hvex_t* data);
chain_list* VexList_Merge_Dup(chain_list* dest, chain_list* src);

int VexList_Compare_SameOper(chain_list* list0, chain_list* list1);

void hvex_PrintVHDL_Width1(FILE* F, hvex_t* Expr, bool withpar);
void hvex_PrintVHDL_cond(FILE* F, hvex_t* Expr, bool withpar);


#endif  // _HVEX_MISC_H_

