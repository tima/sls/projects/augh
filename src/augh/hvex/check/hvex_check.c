
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "../hvex.h"

#include <check.h>



START_TEST (lit_add_u16u16_u32) {
	unsigned v1 = 0xABCD;
	unsigned v2 = 0xEF01;
	hvex_t* vex = hvex_newmodel_op2(HVEX_ADD, 32,
		hvex_newlit_int(v1, 16, false),
		hvex_newlit_int(v2, 16, false)
	);
	hvex_simp(vex);
	fail_if(vex->model!=HVEX_LITERAL, "Result must be literal");
	fail_if(
		hvex_lit_evalint(vex) != v1 + v2,
		"Wrong result after simplification: 0x%04x + 0x%04x = 0x%08x, obtained 0x%08x", v1, v2, v1+v2, hvex_lit_evalint(vex)
	);
	hvex_free(vex);
}
END_TEST

START_TEST (lit_sub_u16u16_u32) {
	unsigned v1 = 0xABCD;
	unsigned v2 = 0xEF01;
	hvex_t* vex = hvex_newmodel_op2(HVEX_SUB, 32,
		hvex_newlit_int(v1, 16, false),
		hvex_newlit_int(v2, 16, false)
	);
	hvex_simp(vex);
	fail_if(vex->model!=HVEX_LITERAL, "Result must be literal");
	fail_if(
		hvex_lit_evalint(vex) != v1 - v2,
		"Wrong result after simplification: 0x%04x - 0x%04x = 0x%08x, obtained 0x%08x", v1, v2, v1-v2, hvex_lit_evalint(vex)
	);
	hvex_free(vex);
}
END_TEST

START_TEST (lit_mul_u8u8_u32) {
	unsigned v1 = 0xFF;
	unsigned v2 = 0xFF;
	hvex_t* vex = hvex_newmodel_op2(HVEX_MUL, 32,
		hvex_newlit_int(v1, 8, false),
		hvex_newlit_int(v2, 8, false)
	);
	hvex_simp(vex);
	fail_if(vex->model!=HVEX_LITERAL, "Result must be literal");
	fail_if(
		hvex_lit_evalint(vex) != v1 * v2,
		"Wrong result after simplification: 0x%02x * 0x%02x = 0x%08x, obtained 0x%08x", v1, v2, v1 * v2, hvex_lit_evalint(vex)
	);
	hvex_free(vex);
}
END_TEST

START_TEST (lit_rem_u8u8_u32) {
	unsigned v1 = 2;
	unsigned v2 = 4;
	hvex_t* vex = hvex_newmodel_op2(HVEX_DIVR, 32,
		hvex_newlit_int(v1, 8, false),
		hvex_newlit_int(v2, 8, false)
	);
	hvex_simp(vex);
	fail_if(vex->model!=HVEX_LITERAL, "Result must be literal");
	fail_if(
		hvex_lit_evalint(vex) != v1 % v2,
		"Wrong result after simplification: 0x%02x % 0x%02x = 0x%08x, obtained 0x%08x", v1, v2, v1 % v2, hvex_lit_evalint(vex)
	);
	hvex_free(vex);
}
END_TEST



Suite *hvex_check_build_suite() {
	Suite *s = suite_create("Suite");
	TCase *tc_tests = tcase_create("Tests");

	tcase_add_test(tc_tests, lit_add_u16u16_u32);
	tcase_add_test(tc_tests, lit_sub_u16u16_u32);
	tcase_add_test(tc_tests, lit_mul_u8u8_u32);
	tcase_add_test(tc_tests, lit_rem_u8u8_u32);

	suite_add_tcase (s, tc_tests);
	return s;
}

// For declaration of aughlib_init()
#include "../../auto/auto.h"

int main() {

	// Initialize the AUGH lib
	aughlib_init();

	// Begin checks

	Suite *s = hvex_check_build_suite();
	SRunner *sr = srunner_create(s);

	srunner_run_all(sr, CK_NORMAL);
	int errors_nb = srunner_ntests_failed(sr);

	srunner_free(sr);

	return (errors_nb == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}


