
/* Specification

The HVEX structures represent operations.
The set of available operations is extensible: adding HVEX models is possible.

One HVEX element is a structure of type hvex_t.
An HVEX model is a structure of type hvex_model_t.
Model-specific data (if any) can be stored in the field "data".

Each element can have operands.
Operands are linked as a double-chained list to ease replacement/simplification.
Each of these operands shall have its field "father" set to its parent element.
All element modifications are done in-place and the main element structure is kept.

For some models, the right index shall always be zero:
	LITERAL CONCAT RESIZE REPEAT
	NOT AND NAND OR NOR XOR NXOR
	IFTHEN

All operands of logic operations must have the width of the result:
	NOT AND NAND OR NOR XOR NXOR

The width of the operands of these models don't need to have same width:
	ADD SUB MUL
	GT GE LT LE GTS GES LTS LES

These models can have an arbitrary number of operands (>= 2):
	CONCAT
	ADD MUL
	EQ NE
	AND NAND OR NOR XOR NXOR

The data string of LITERAL must be namealloc()'d
The name string of VECTOR and INDEX must be namealloc()'d

About shifts and rotations: the data operand width can be anything

About IFTHEN: 2 operands: first is a condition (1 bit), second is the data.
If the condition is '0', the result is all '0', alse it is the data operand.

*/

#ifndef _HVEX_H_
#define _HVEX_H_

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>

#include <assert.h>

#include <mut.h>  // For declaration of bitype_list



//==============================================
// Definition of structures
//==============================================

// The main node type
typedef struct hvex_model_t  hvex_model_t;
typedef struct hvex_t  hvex_t;

// This structure represents an operation model
struct hvex_model_t {
	// A unique name, given by HLS tool or a plugin
	char* name;

	// Callbacks hvex-specific
	void*   (*func_datadup)(void* data);
	void    (*func_datafree)(void* data);
	int     (*func_datacmp)(void* data1, void* data2);
	void    (*func_croprel)(hvex_t* v, unsigned left, unsigned right);
	unsigned (*func_simp)(hvex_t* v);  // Return the number of things done
	void    (*func_fprint)(FILE* F, hvex_t* v);
	char*   sym_fprint;  // Example: "+", "&", "and"...

	// Check callback
	// TYPE = code, DATA_FROM = hvex_t*, DATA_TO = stralloc'd message
	bitype_list* (*func_check)(hvex_t* v);

	// For simplifications: The opposite model, if applicable
	hvex_model_t* model_not;

	// Callbacks mapper-specific
	//   map_data is map_netlist_data_t*
	//   elt is hvex_dico_elt_t* elt
	//   return map_netlist_datapath_t*
	// FIXME This should be in a HLS-tool-specific structure
	// FIXME Add a callback to list needed resources
	void* (*cb_map)(void* map_data, void* elt);
	// Callback to detect components
	//   hwres_data is hwres_scanvex_t*
	//   ref is hvex_dico_ref_t*
	void (*cb_needed_res_hvex)(hvex_t* vex, void* hwres_data);
	void (*cb_needed_res_vexalloc)(void* ref, void* hwres_data);

	// To track the plugin that declared this model
	void* plugin;
	// For generic HVEX models, may need some descriptive data
	void* data;
};

// The main node type. This structure represents an operation.
struct hvex_t {
	// Fields related to the hierarchy of elements
	hvex_t* father;       // The parent node
	hvex_t* operands;     // Pointer to first operand (often left-most operand)
	// Double-linked list in the operands of the father. Prev is left, next is right.
	hvex_t *prev, *next;

	// Type and local data
	hvex_model_t* model;
	void*         data;

	// The indexes (always 'downto' style: left is the most significant bit)
	unsigned left, right, width;

	// Sign extension ('0', '1', '-' or 0 for signed extension)
	char pad;
	// Default: 0 (unset), 1 (HVEX node). Otherwise: a user-specific value
	uint8_t father_type;
};

#define HVEX_FATHER_NONE  0
#define HVEX_FATHER_HVEX  1



//==============================================
// Useful macros
//==============================================

#define hvex_is_signed(vex)    ((vex)->pad==0 ? true : false)
#define hvex_is_unsigned(vex)  ((vex)->pad=='0' || (vex)->pad=='-' ? true : false)
#define hvex_set_signed(vex)   do { (vex)->pad = 0; } while(0)
#define hvex_set_unsigned(vex) do { (vex)->pad = '0'; } while(0)
#define hvex_sign_copy(dest, src) do { (dest)->pad = (src)->pad; } while(0)

#define hvex_is_father_hvex(vex) ((vex)->father_type==HVEX_FATHER_HVEX ? true : false)

#define hvex_foreach(vex, iter) for(hvex_t* iter=vex; iter!=NULL; iter=iter->next)



//==============================================
// HVEX models for built-in AUGH operations
//==============================================

extern hvex_model_t* HVEX_VECTOR;    // name[left, right]
extern hvex_model_t* HVEX_LITERAL;   // a constant value

extern hvex_model_t* HVEX_INDEX;     // name(index)[left, right]
extern hvex_model_t* HVEX_CONCAT;    // Concatenation, left to right
extern hvex_model_t* HVEX_RESIZE;    // Signed or unsigned extension
extern hvex_model_t* HVEX_REPEAT;    // Repeat a 1-bit operand

// Should only be used for parsing
extern hvex_model_t* HVEX_STRING;
extern hvex_model_t* HVEX_FUNCTION;  // func(arg, arg, ...)
extern hvex_model_t* HVEX_DIVQ;      // Quotient of integer division
extern hvex_model_t* HVEX_DIVR;      // Remainder of integer division

// Arithmetic operations
extern hvex_model_t* HVEX_ADD;
extern hvex_model_t* HVEX_SUB;
extern hvex_model_t* HVEX_NEG;  // Only one operand
extern hvex_model_t* HVEX_MUL;

// Arithmetic operations with carry input
extern hvex_model_t* HVEX_ADDCIN;

// Comparisons
extern hvex_model_t* HVEX_EQ;
extern hvex_model_t* HVEX_NE;
extern hvex_model_t* HVEX_GT;
extern hvex_model_t* HVEX_GE;
extern hvex_model_t* HVEX_LT;
extern hvex_model_t* HVEX_LE;
extern hvex_model_t* HVEX_GTS;
extern hvex_model_t* HVEX_GES;
extern hvex_model_t* HVEX_LTS;
extern hvex_model_t* HVEX_LES;

// Shifts and rotations. Operands: data, shift
extern hvex_model_t* HVEX_SHL;
extern hvex_model_t* HVEX_SHR;
extern hvex_model_t* HVEX_ROTL;
extern hvex_model_t* HVEX_ROTR;

// Logic operations
extern hvex_model_t* HVEX_NOT;  // Only one operand
extern hvex_model_t* HVEX_AND;
extern hvex_model_t* HVEX_NAND;
extern hvex_model_t* HVEX_OR;
extern hvex_model_t* HVEX_NOR;
extern hvex_model_t* HVEX_XOR;
extern hvex_model_t* HVEX_NXOR;

// Assignments. Operands: destination, data
extern hvex_model_t* HVEX_ASG;
extern hvex_model_t* HVEX_ASG_EN;       // One more operand: the enable signal (active at '1')
extern hvex_model_t* HVEX_ASG_ADDR;     // One more operand: the address
extern hvex_model_t* HVEX_ASG_ADDR_EN;  // Two more operands: the address, the enabale signal (active at '1')

// Binary MUX
// First operand is selection (N bits). Other 2^N operands must have same width.
extern hvex_model_t* HVEX_MUXB;
// Decoded MUX
// Operands are all MUXDECIN. Only one condition of operands is assumed to be 1.
extern hvex_model_t* HVEX_MUXDEC;
// First operand is condition, second operand is data
extern hvex_model_t* HVEX_MUXDECIN;

// Used only at parsing
extern hvex_model_t* HVEX_ARRAY;  // For array declarations




//==============================================
// Allocation functions
//==============================================

hvex_model_t* hvex_model_new();
void hvex_model_free(hvex_model_t* model);

hvex_t* hvex_new();
void hvex_free_ops(hvex_t* vex);
void hvex_free(hvex_t* v);



//==============================================
// Miscellaneous functions
//==============================================

unsigned hvex_countops(hvex_t* vex);

static inline hvex_t* hvex_last(hvex_t* vex) {
	while(vex->next!=NULL) vex = vex->next;
	return vex;
}

void hvex_addop_head(hvex_t* parent, hvex_t* child);
void hvex_addop_tail(hvex_t* parent, hvex_t* child);

void hvex_addmultop_before(hvex_t* oldop, hvex_t* newop);
void hvex_addmultop_after(hvex_t* oldop, hvex_t* newop);
void hvex_addmultop_replace(hvex_t* oldop, hvex_t* newop);

void hvex_clearinside(hvex_t* vex);  // Remove data, operands, model. Keep the rest.

void hvex_replace_free(hvex_t* destvex, hvex_t* srcvex);
void hvex_replace_resize_free(hvex_t* destvex, hvex_t* srcvex);
void hvex_replace_byextend_free(hvex_t* destvex, hvex_t* srcvex);

int  hvex_cmp(hvex_t* vex1, hvex_t* vex2);

hvex_t* hvex_unlinkop(hvex_t* vexop);
void hvex_remop(hvex_t* vexop);

hvex_t* hvex_dup(hvex_t* vex);

hvex_t* hvex_top_parent(hvex_t* vex);



//==============================================
// Creation of nodes
//==============================================

hvex_t* hvex_newvec(const char* name, unsigned left, unsigned right);
hvex_t* hvex_newvec_bit(const char* name);

hvex_t* hvex_newindex(const char* name, unsigned left, unsigned right, hvex_t* addr);

hvex_t* hvex_newlit(const char* lit);
hvex_t* hvex_newlit_repeat(char c, unsigned n);
hvex_t* hvex_newlit_bit(char c);
hvex_t* hvex_newlit_int(int i, unsigned width, bool is_signed);
hvex_t* hvex_newlit_arrint32_be(const int* arr, unsigned nb, unsigned width, bool is_signed);

hvex_t* hvex_newfunc(const char* name, unsigned width);
hvex_t* hvex_newfunc_op1(const char* name, unsigned width, hvex_t* op);
hvex_t* hvex_newfunc_op2(const char* name, unsigned width, hvex_t* op1, hvex_t* op2);
hvex_t* hvex_newfunc_opn(const char* name, unsigned width, ...);

hvex_t* hvex_newmodel(hvex_model_t* model, unsigned width);
hvex_t* hvex_newmodel_op1(hvex_model_t* model, unsigned width, hvex_t* op);
hvex_t* hvex_newmodel_op2(hvex_model_t* model, unsigned width, hvex_t* op1, hvex_t* op2);
hvex_t* hvex_newmodel_opn(hvex_model_t* model, unsigned width, ...);

hvex_t* hvex_new_move(hvex_t* vex);



//==============================================
// Printing
//==============================================

void hvex_fprint_boundsonly_raw(FILE* F, hvex_t* vex, char beg, char end);
void hvex_fprint_boundsonly_brk(FILE* F, hvex_t* vex);
void hvex_fprint_boundsonly_par(FILE* F, hvex_t* vex);

void hvex_fprint(FILE* F, hvex_t* vex);
void hvex_fprint_bn(FILE* F, hvex_t* vex);
void hvex_fprint_b(FILE* F, hvex_t* vex);

void hvex_print(hvex_t* vex);
void hvex_print_bn(hvex_t* vex);
void hvex_print_b(hvex_t* vex);

void hvex_fprint_opbetween(FILE* F, const char* opstr, hvex_t* vex);
void hvex_fprint_opbefore(FILE* F, const char* opstr, hvex_t* vex);

void hvex_fprint_opbetween_cb(FILE* F, hvex_t* vex);
void hvex_fprint_opbefore_cb(FILE* F, hvex_t* vex);



//==============================================
// Cropping, simplification
//==============================================

void hvex_croprel(hvex_t* vex, unsigned left, unsigned right);

static inline void hvex_cropabs(hvex_t* vex, unsigned left, unsigned right) {
	assert(left >= right && left <= vex->left && right >= vex->right);
	hvex_croprel(vex, vex->left - left, right - vex->right);
}

static inline hvex_t* hvex_new_croprel(hvex_t* vex, unsigned left, unsigned right) {
	hvex_croprel(vex, left, right);
	return vex;
}
static inline hvex_t* hvex_new_cropabs(hvex_t* vex, unsigned left, unsigned right) {
	hvex_cropabs(vex, left, right);
	return vex;
}

void hvex_extend(hvex_t* vex, unsigned bits_nb);
void hvex_resize(hvex_t* vex, unsigned width);

static inline hvex_t* hvex_new_extend(hvex_t* vex, unsigned bits_nb) {
	hvex_extend(vex, bits_nb);
	return vex;
}
static inline hvex_t* hvex_new_resize(hvex_t* vex, unsigned width) {
	hvex_resize(vex, width);
	return vex;
}

unsigned hvex_shrinks_getcrop(hvex_t* vex);
unsigned hvex_shrinku_getcrop(hvex_t* vex, char bit);

unsigned hvex_shrinks(hvex_t* vex);
unsigned hvex_shrinku(hvex_t* vex);
unsigned hvex_shrink(hvex_t* vex);

unsigned hvex_simp_noops(hvex_t* vex);
unsigned hvex_simp(hvex_t* vex);

static inline hvex_t* hvex_new_simp(hvex_t* vex) {
	hvex_simp(vex);
	return vex;
}



//==============================================
// Check and debug functions
//==============================================

void hvex_checkabort(hvex_t* vex);



//==============================================
// Model-specific functions
//==============================================

bool hvex_lit_allknown(hvex_t* vex);
unsigned hvex_lit_replace_dc(hvex_t* vex, char bit);

static inline bool hvex_lit_checkfitint(hvex_t* vex) {
	return vex->width <= sizeof(int) * 8 ? true : false;
}
int hvex_lit_evalint(hvex_t* vex);

void hvex_lit_setrepeat(hvex_t* vex, char c, unsigned n);
char hvex_lit_getsamebit(hvex_t* vex);

unsigned hvex_lit_getnbleftbit(hvex_t* vex, char bit, bool with_dc);
unsigned hvex_getnbleftbit(hvex_t* vex, char bit, bool with_dc);

unsigned hvex_lit_getnbrightbit(hvex_t* vex, char bit, bool with_dc);
unsigned hvex_getnbrightbit(hvex_t* vex, char bit, bool with_dc);

void hvex_catleft(hvex_t* vex, hvex_t* op);
void hvex_catright(hvex_t* vex, hvex_t* op);

bool hvex_lit_iszero(hvex_t* vex);
static inline bool hvex_iszero(hvex_t* vex) {
	if(vex->model!=HVEX_LITERAL) return false;
	return hvex_lit_iszero(vex);
}
bool hvex_lit_isone(hvex_t* vex);
static inline bool hvex_isone(hvex_t* vex) {
	if(vex->model!=HVEX_LITERAL) return false;
	return hvex_lit_isone(vex);
}

char hvex_notbit(char bit);

void hvex_not(hvex_t* vex);
hvex_t* hvex_newnot(hvex_t* vex);

static inline char* hvex_lit_getval(hvex_t* vex) {
	return (char*)vex->data;
}
static inline void hvex_lit_setval(hvex_t* vex, char* val) {
	vex->data = val;
}

static inline char* hvex_vec_getname(hvex_t* vex) {
	return (char*)vex->data;
}
static inline void hvex_vec_setname(hvex_t* vex, char* name) {
	vex->data = name;
}

static inline char* hvex_index_getname(hvex_t* vex) {
	return (char*)vex->data;
}
static inline void hvex_index_setname(hvex_t* vex, char* name) {
	vex->data = name;
}
static inline hvex_t* hvex_index_get_addr(hvex_t* vex) {
	return vex->operands;
}

static inline char* hvex_vecidx_getname(hvex_t* vex) {
	assert(vex->model==HVEX_VECTOR || vex->model==HVEX_INDEX);
	return (char*)vex->data;
}
static inline void hvex_vecidx_setname(hvex_t* vex, char* name) {
	assert(vex->model==HVEX_VECTOR || vex->model==HVEX_INDEX);
	vex->data = name;
}

static inline char* hvex_func_getname(hvex_t* vex) {
	return (char*)vex->data;
}
static inline void hvex_func_setname(hvex_t* vex, char* name) {
	vex->data = name;
}

static inline bool hvex_check_sameidx_vexidx(hvex_t* vex, unsigned left, unsigned right) {
	return (vex->left == left && vex->right == right) ? true : false;
}
static inline bool hvex_check_overlap_vexidx(hvex_t* vex, unsigned left, unsigned right) {
	return (vex->right > left || vex->left < right) ? false : true;
}
static inline bool hvex_check_inclusion_largevexidx(hvex_t* vex_large, unsigned left, unsigned right) {
	return (vex_large->left >= left && vex_large->right <= right) ? true : false;
}
static inline bool hvex_check_inclusion_shortvexidx(hvex_t* vex_short, unsigned left, unsigned right) {
	return (left >= vex_short->left && right <= vex_short->right) ? true : false;
}

static inline bool hvex_check_sameidx(hvex_t* vex1, hvex_t* vex2) {
	return (vex1->left == vex2->left && vex1->right == vex2->right) ? true : false;
}
static inline bool hvex_check_overlap(hvex_t* vex1, hvex_t* vex2) {
	return (vex1->right > vex2->left || vex1->left < vex2->right) ? false : true;
}
static inline bool hvex_check_inclusion(hvex_t* vex_large, hvex_t* vex_short) {
	return (vex_large->left >= vex_short->left && vex_large->right <= vex_short->right) ? true : false;
}

void hvex_set_minmax(hvex_t* vex1, hvex_t* vex2);

char hvex_get_msb(hvex_t* vex);

hvex_t* hvex_asg_make(hvex_t* vex_dest, hvex_t* vex_addr, hvex_t* vex_expr, hvex_t* vex_en);

static inline hvex_t* hvex_asg_get_dest(hvex_t* Expr) {
	if(Expr->model==HVEX_ASG || Expr->model==HVEX_ASG_EN || Expr->model==HVEX_ASG_ADDR || Expr->model==HVEX_ASG_ADDR_EN) {
		return Expr->operands;
	}
	return NULL;
}
static inline hvex_t* hvex_asg_get_expr(hvex_t* Expr) {
	if(Expr->model==HVEX_ASG || Expr->model==HVEX_ASG_EN || Expr->model==HVEX_ASG_ADDR || Expr->model==HVEX_ASG_ADDR_EN) {
		return Expr->operands->next;
	}
	return NULL;
}
static inline hvex_t* hvex_asg_get_addr(hvex_t* Expr) {
	if(Expr->model==HVEX_ASG_ADDR || Expr->model==HVEX_ASG_ADDR_EN) {
		return Expr->operands->next->next;
	}
	return NULL;
}
static inline hvex_t* hvex_asg_get_cond(hvex_t* Expr) {
	if(Expr->model==HVEX_ASG_EN) return Expr->operands->next->next;
	else if(Expr->model==HVEX_ASG_ADDR_EN) return Expr->operands->next->next->next;
	return NULL;
}

static inline char* hvex_asg_get_destname(hvex_t* Expr) {
	if(Expr->model==HVEX_ASG || Expr->model==HVEX_ASG_EN || Expr->model==HVEX_ASG_ADDR || Expr->model==HVEX_ASG_ADDR_EN) {
		return hvex_vec_getname(Expr->operands);
	}
	return NULL;
}

void hvex_asg_andcond(hvex_t* vex, hvex_t* andcond);

bool hvex_model_is_asg(hvex_model_t* model);

bool hvex_model_is_ineq(hvex_model_t* model);
hvex_model_t* hvex_model_reverse_ineq(hvex_model_t* model);
bool hvex_model_is_ineq_or_eq(hvex_model_t* model);
bool hvex_model_is_ineq_signed(hvex_model_t* model);
bool hvex_model_is_ineq_lower(hvex_model_t* model);
bool hvex_model_is_ineq_upper(hvex_model_t* model);



//==============================================
// Init
//==============================================

void hvex_init();



#endif  // _HVEX_H_

