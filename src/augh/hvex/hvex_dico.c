
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <stdbool.h>

#include "../auto/auto.h"
#include "hvex_dico.h"



// Create a pool of type : vex_alloc_elt_t

#define POOL_prefix      pool_hvex_dico_elt
#define POOL_data_t      hvex_dico_elt_t
#define POOL_ALLOC_SIZE  1000

#define POOL_INSTANCE             POOL_HVEX_DICO_ELT
#define POOL_INSTANCE_ATTRIBUTES  static

#include "../pool.h"

// Create a pool of type : vex_alloc_ref_t

#define POOL_prefix      pool_hvex_dico_ref
#define POOL_data_t      hvex_dico_ref_t
#define POOL_ALLOC_SIZE  1000

#define POOL_INSTANCE             POOL_HVEX_DICO_REF
#define POOL_INSTANCE_ATTRIBUTES  static

#include "../pool.h"

// Allocation wrappers

static hvex_dico_ref_t* VexAlloc_Ref_New() {
	hvex_dico_ref_t* elt = pool_hvex_dico_ref_pop(&POOL_HVEX_DICO_REF);
	memset(elt, 0, sizeof(*elt));
	return elt;
}
static void VexAlloc_Ref_Free(hvex_dico_ref_t* elt) {
	pool_hvex_dico_ref_push(&POOL_HVEX_DICO_REF, elt);
}

static hvex_dico_elt_t* VexAlloc_Elt_New() {
	hvex_dico_elt_t* elt = pool_hvex_dico_elt_pop(&POOL_HVEX_DICO_ELT);
	memset(elt, 0, sizeof(*elt));
	return elt;
}
static void VexAlloc_Elt_Free(hvex_dico_elt_t* vexalloc_elt) {
	freechain(vexalloc_elt->args);
	foreach(vexalloc_elt->refs, scanRef) VexAlloc_Ref_Free(scanRef->DATA);
	freechain(vexalloc_elt->refs);
	pool_hvex_dico_elt_push(&POOL_HVEX_DICO_ELT, vexalloc_elt);
}

// The main dictionary structure

void VexAlloc_Init(hvex_dico_t* vexalloc) {
	avl_pp_init(&vexalloc->elt_lit);
	avl_pp_init(&vexalloc->elt_sym);
	avl_pp_init(&vexalloc->elt_others);
	avl_p_init(&vexalloc->all_elts);
}
void VexAlloc_Clear(hvex_dico_t* vexalloc) {
	avl_pp_reset(&vexalloc->elt_lit);
	avl_pp_reset(&vexalloc->elt_sym);
	avl_pp_reset(&vexalloc->elt_others);

	avl_p_foreach(&vexalloc->all_elts, scan) VexAlloc_Elt_Free(scan->data);
	avl_p_reset(&vexalloc->all_elts);
}


/* WARNING IMPORTANT

This module handles operations whose order of operands is not relevant:
	VEX_AND, VEX_NAND, VEX_OR, VEX_NOR, VEX_XOR, VEX_NXOR
	VEX_ADD, VEX_MUL, VEX_MUL_S
	VEX_EQ, VEX_NE

It handles when the result of an operation is used several times at different widthes
	ex : sig(63..0) = a * b;  lo = sig(31..0); hi = sig(63..32);

This function will NOT attempt to detect these situations:

- When re-ordering the operands gives another operation
  VEX_GT, VEX_LE, etc

- When inverting the output corresponds to another function
  VEX_AND, VEX_NAND, VEX_OR, VEX_NOR, VEX_XOR, VEX_NXOR
  VEX_EQ, VEX_NE
  VEX_GT, VEX_LE, etc

FIXME Storing Operations and Functions as a chained list is not efficient.
The storage structure for Oper and Func elements should be a tree storing the pointers to the operands,
each operand then linking to the possible next operands.

  tree -> op1-1 --------+-> next operand (another tree)
          op1-2 ...     +-> list of Oper/Func Elt structures that use these operands
          op1-3 ...

An operand is represented by a special structure containing :
  - an integer: the rank in the list of operands (begins at 1 so it's the number of operands)
  - a pointer to the previous operand
  - a tree to store the next operands
  - a tree for the operations/Functions that use these operands
That way, the list of operands is no longer stored into the VexAlloc_Elt structure
and it is shared.

Would it be interesting in any way ?
Matching a list of commutative operands is still heavy.
To alleviate this, elements should first be sorted by number of operands,
then all is stored in chained lists of allocated operands (sorted by pointer value).
Matching is then O(n*log(n))

*/

static bool VexAlloc_Match_ListOper(hvex_dico_t* vexalloc, chain_list* opers0, chain_list* opers1) {
	while(opers0!=NULL && opers1!=NULL) {
		if(opers0->DATA != opers1->DATA) return false;
		// Next operands
		opers0 = opers0->NEXT;
		opers1 = opers1->NEXT;
	}
	if(opers0!=NULL || opers1!=NULL) return false;
	return true;
}
static bool VexAlloc_Match_ListOper_Commutative(hvex_dico_t* vexalloc, chain_list* opers0, chain_list* opers1) {
	// WARNING this check is done too many times
	if( ChainList_Count(opers0) != ChainList_Count(opers1) ) return false;
	chain_list* list1 = NULL;
	// Link the elements of opers1 in another list
	foreach(opers1, scan) list1 = addchain(list1, scan);
	// Scan correspondences for elements of list0
	foreach(opers0, elt0) {
		bool found = false;
		foreach(list1, scan1) {
			ptype_list* elt1 = scan1->DATA;
			if(elt0->DATA != elt1->DATA) continue;
			list1 = ChainList_Remove(list1, elt1);
			found = true;
			break;
		}
		if(found==false) {
			freechain(list1);
			return false;
		}
	}
	freechain(list1);
	return true;
}

bool VexAlloc_Add_raw(hvex_dico_t* vexalloc, hvex_t* Expr, hvex_dico_ref_t** ExprAlloc) {
	if(Expr==NULL) {
		if(ExprAlloc!=NULL) *ExprAlloc = NULL;
		return false;
	}

	assert(Expr->model!=HVEX_FUNCTION);

	if(Expr->model==HVEX_LITERAL) {
		char* value = hvex_lit_getval(Expr);
		char buffer[Expr->width + 1];
		strcpy(buffer, value);

		// Shrink the literal to what is strictly necessary for sign extension
		value = buffer;
		unsigned width = Expr->width;
		if(hvex_is_signed(Expr)==true) {
			// Remove duplicate bits on the left
			while(width>=2 && value[0]==value[1]) { value++; width--; }
		}
		else {
			// Remove all zeros on the left
			while(width>=2 && value[0]=='0') { value++; width--; }
		}
		value = namealloc(value);

		// Get the node where the expression is supposed to be stored.
		avl_pp_node_t* avl_node = NULL;
		bool b = avl_pp_add(&vexalloc->elt_lit, value, &avl_node);
		if(b==true) {
			// Create a new node
			//dbgprintf("Adding literal '%s' in the dictionary.\n", value);
			hvex_dico_elt_t* elt = VexAlloc_Elt_New();
			avl_node->data = elt;
			elt->model = HVEX_LITERAL;
			elt->data = value;
			elt->width = width;
			elt->left = width-1;
			elt->right = 0;
			avl_p_add_overwrite(&vexalloc->all_elts, elt, elt);
		}
		hvex_dico_elt_t* elt = avl_node->data;

		// Search for an existing matching reference
		// Note: All references grab the full width of the element.
		char want_extend = Expr->pad;
		foreach(elt->refs, scanRef) {
			hvex_dico_ref_t* ref = scanRef->DATA;
			if(ref->extend!=want_extend) continue;
			if(ref->width!=Expr->width) continue;
			if(ExprAlloc!=NULL) *ExprAlloc = ref;
			return false;  // Nothing was added
		}

		// Create a new reference
		//dbgprintf("Adding ref to literal '%s' in the dictionary.\n", value);
		hvex_dico_ref_t* ref = VexAlloc_Ref_New();
		elt->refs = addchain(elt->refs, ref);

		ref->width = Expr->width;
		ref->extend = want_extend;
		ref->ref.elt = elt;
		ref->ref.width = elt->width;
		ref->ref.left = elt->left;
		ref->ref.right = elt->right;

		if(ExprAlloc!=NULL) *ExprAlloc = ref;
		return true;  // The reference is a new one
	}

	else if(Expr->model==HVEX_VECTOR) {
		char* name = hvex_vec_getname(Expr);

		// Get the node where the expression is supposed to be stored.
		avl_pp_node_t* avl_node = NULL;
		bool b = avl_pp_add(&vexalloc->elt_sym, name, &avl_node);
		if(b==true) {
			//dbgprintf("Adding symbol '%s' (%u,%u:%u) in the dictionary.\n", name, Expr->LEFT, Expr->RIGHT, Expr->WIDTH);
			hvex_dico_elt_t* elt = VexAlloc_Elt_New();
			avl_node->data = elt;
			elt->model = HVEX_VECTOR;
			elt->data = name;
			elt->width = Expr->width;
			elt->left = Expr->left;
			elt->right = Expr->right;
			avl_p_add_overwrite(&vexalloc->all_elts, elt, elt);
		}
		hvex_dico_elt_t* elt = avl_node->data;

		// Search for an existing matching reference
		char want_extend = Expr->pad;
		foreach(elt->refs, scanRef) {
			hvex_dico_ref_t* ref = scanRef->DATA;
			if(ref->extend!=want_extend) continue;
			if(ref->width!=Expr->width) continue;
			if(ref->ref.left!=Expr->left) continue;
			if(ref->ref.right!=Expr->right) continue;
			if(ExprAlloc!=NULL) *ExprAlloc = ref;
			return false;  // Nothing was added
		}

		// Create a new reference
		//dbgprintf("Adding ref to symbol '%s' (%u,%u:%u) in the dictionary.\n", name, Expr->LEFT, Expr->RIGHT, Expr->WIDTH);
		hvex_dico_ref_t* ref = VexAlloc_Ref_New();
		elt->refs = addchain(elt->refs, ref);

		ref->width = Expr->width;
		ref->extend = want_extend;
		ref->ref.elt = elt;
		ref->ref.width = Expr->width;
		ref->ref.left = Expr->left;
		ref->ref.right = Expr->right;

		if(Expr->width > 0) {
			// Update the element details if needed
			if(ref->ref.left > elt->left) {
				elt->left = ref->ref.left;
				elt->width = elt->left - elt->right + 1;
			}
			if(ref->ref.right < elt->right) {
				elt->right = ref->ref.right;
				elt->width = elt->left - elt->right + 1;
			}
		}

		if(ExprAlloc!=NULL) *ExprAlloc = ref;
		return true;  // The reference is a new one
	}

	else {
		//dbgprintf("Model %s beginning VexAlloc...\n", Expr->model);

		// Build/Allocate the list of operands
		chain_list* list_ops = NULL;
		bool list_ops_is_new = false;

		// Handle special operations
		if(Expr->model==HVEX_INDEX) {
			hvex_dico_ref_t* alloc_op;
			bool b;
			// The index expr
			b = VexAlloc_Add_raw(vexalloc, Expr->operands, &alloc_op);
			if(b==true) list_ops_is_new = true;
			list_ops = addchain(list_ops, alloc_op);
			// The name
			hvex_t* tmp_expr = hvex_newvec_bit(hvex_index_getname(Expr));
			b = VexAlloc_Add_raw(vexalloc, tmp_expr, &alloc_op);
			if(b==true) list_ops_is_new = true;
			list_ops = addchain(list_ops, alloc_op);
			hvex_free(tmp_expr);
		}
		else if(hvex_model_is_asg(Expr->model)==true) {
			hvex_dico_ref_t* alloc_op;
			bool b;
			// The dest name
			hvex_t* tmp_expr = hvex_newvec_bit(hvex_vec_getname(Expr->operands));
			b = VexAlloc_Add_raw(vexalloc, tmp_expr, &alloc_op);
			if(b==true) list_ops_is_new = true;
			//dbgprintf("Oper %lu: Dest new: %u\n", oper, b);
			list_ops = addchain(list_ops, alloc_op);
			hvex_free(tmp_expr);
			// Exp, Addr and Cond
			hvex_foreach(Expr->operands->next, VexOp) {
				b = VexAlloc_Add_raw(vexalloc, VexOp, &alloc_op);
				if(b==true) list_ops_is_new = true;
				//dbgprintf("Oper %lu: Cond new: %u\n", oper, b);
				list_ops = addchain(list_ops, alloc_op);
			}
			// Put the list back in the right order
			list_ops = reverse(list_ops);
		}
		else {
			// Generic operation
			hvex_foreach(Expr->operands, VexOp) {
				hvex_dico_ref_t* alloc_op;
				bool b = VexAlloc_Add_raw(vexalloc, VexOp, &alloc_op);
				if(b==true) list_ops_is_new = true;
				list_ops = addchain(list_ops, alloc_op);
			}
			list_ops = reverse(list_ops);
		}
		unsigned list_ops_nb = ChainList_Count(list_ops);

		// Get the node where the expression is supposed to be stored.
		avl_pp_node_t* avl_node = NULL;
		bool b = avl_pp_add(&vexalloc->elt_others, Expr->model->name, &avl_node);
		if(b==true) {
			avl_node->data = NULL;
		}

		// Search an existing element
		hvex_dico_elt_t* elt = NULL;
		if(list_ops_is_new==false) {
			//dbgprintf("Oper %lu: List of ops is reused\n", oper);
			chain_list* list = avl_node->data;

			// Handle when re-ordering operands: logic, add, mul, eq, ne
			bool allow_reorder = false;
			if(
				Expr->model==HVEX_ADD || Expr->model==HVEX_MUL ||
				Expr->model==HVEX_EQ || Expr->model==HVEX_NE ||
				Expr->model==HVEX_AND || Expr->model==HVEX_NAND ||
				Expr->model==HVEX_OR || Expr->model==HVEX_NOR ||
				Expr->model==HVEX_XOR || Expr->model==HVEX_NXOR
			) allow_reorder = true;

			foreach(list, scanElt) {
				hvex_dico_elt_t* scan_elt = scanElt->DATA;
				// The interest is only the operands. Compare them.
				if(scan_elt->args_nb != list_ops_nb) continue;
				bool b = false;
				if(allow_reorder==true) b = VexAlloc_Match_ListOper_Commutative(vexalloc, scan_elt->args, list_ops);
				else b = VexAlloc_Match_ListOper(vexalloc, scan_elt->args, list_ops);
				if(b==true) {
					// Matching expression found
					freechain(list_ops);
					elt = scan_elt;
					break;
				}
			}

		}

		// Create a new element
		if(elt==NULL) {
			//dbgprintf("Oper %lu: Creating new element\n", oper);
			elt = VexAlloc_Elt_New();
			avl_node->data = addchain(avl_node->data, elt);
			elt->model = Expr->model;
			elt->width = Expr->width;
			elt->left = Expr->left;
			elt->right = Expr->right;
			elt->args_nb = list_ops_nb;
			elt->args = list_ops;
			avl_p_add_overwrite(&vexalloc->all_elts, elt, elt);
			// Handle operations whose result RIGHT should always be zero
			if(Expr->model==HVEX_ADD || Expr->model==HVEX_SUB || Expr->model==HVEX_MUL) {
				if(elt->right>0) {
					elt->width += elt->right;
					elt->right = 0;
				}
			}
		}

		// Search for an existing matching reference
		char want_extend = Expr->pad;
		foreach(elt->refs, scanRef) {
			hvex_dico_ref_t* ref = scanRef->DATA;
			if(ref->extend!=want_extend) continue;
			if(ref->width!=Expr->width) continue;
			if(ref->ref.left!=Expr->left) continue;
			if(ref->ref.right!=Expr->right) continue;
			if(ExprAlloc!=NULL) *ExprAlloc = ref;
			return false;  // Nothing was added
		}

		// Create a new reference
		//dbgprintf("Oper %lu: Creating new ref\n", oper);
		hvex_dico_ref_t* ref = VexAlloc_Ref_New();
		elt->refs = addchain(elt->refs, ref);

		ref->width = Expr->width;
		ref->extend = want_extend;
		ref->ref.elt = elt;
		ref->ref.width = Expr->width;
		ref->ref.left = Expr->left;
		ref->ref.right = Expr->right;

		// Update the element details if needed
		if(ref->ref.left > elt->left) {
			elt->left = ref->ref.left;
			elt->width = elt->left - elt->right + 1;
		}
		if(ref->ref.right < elt->right) {
			elt->right = ref->ref.right;
			elt->width = elt->left - elt->right + 1;
		}

		if(ExprAlloc!=NULL) *ExprAlloc = ref;
		return true;  // The reference is a new one
	}

	// Error
	errprintfa("This state should not be reachable.\n");

	return false;
}
hvex_dico_ref_t* VexAlloc_Add(hvex_dico_t* vexalloc, hvex_t* Expr) {
	hvex_dico_ref_t* ExprRef;

	#if 1
	VexAlloc_Add_raw(vexalloc, Expr, &ExprRef);
	#endif

	#if 0
	bool b = VexAlloc_Add_raw(vexalloc, Expr, &ExprRef);
	if(ExprRef!=NULL) {
		dbgprintf("VEXALLOC %u : ", b);
		VexAlloc_Print_Ref_be(ExprRef, NULL, "\n");
	}
	#endif

	return ExprRef;
}

unsigned VexAlloc_Lit2Uint(hvex_dico_elt_t* Expr) {
	unsigned value = 0;
	for(char* ptr = Expr->data; (*ptr)!=0; ptr++) {
		value <<= 1;
		if(*ptr=='1') value |= 0x01;
	}
	return value;
}

// Return zero if unknown
char VexAllocElt_GetLeftBit_Char(hvex_dico_elt_t* Expr) {
	if(Expr->model==HVEX_LITERAL) {
		return ((char*)Expr->data)[0];
	}
	if(Expr->model==HVEX_CONCAT) return VexAllocRef_GetLeftBit_Char(Expr->args->DATA);
	return 0;
}
char VexAllocRef_GetLeftBit_Char(hvex_dico_ref_t* ref) {
	if(ref->width > ref->ref.width) return ref->extend;
	return VexAllocElt_GetLeftBit_Char(ref->ref.elt);
}

hvex_t* VexAlloc_GenVex_Elt(hvex_dico_elt_t* vexalloc) {
	if(vexalloc==NULL) return NULL;

	hvex_t* Expr = NULL;

	void gen_vex_operand(hvex_t* Expr, chain_list* args) {
		chain_list* list = NULL;
		foreach(args, scanArg) {
			hvex_dico_ref_t* ref = scanArg->DATA;
			hvex_t* arg = VexAlloc_GenVex_Ref(ref);
			list = addchain(list, arg);
		}
		foreach(list, scan) {
			hvex_t* VexOp = scan->DATA;
			hvex_addop_head(Expr, VexOp);
		}
		freechain(list);
	}

	if(vexalloc->model==HVEX_LITERAL) {
		char* value = vexalloc->data;
		Expr = hvex_newlit(value);
	}
	else if(vexalloc->model==HVEX_VECTOR) {
		Expr = hvex_newvec(vexalloc->data, vexalloc->left, vexalloc->right);
	}
	else if(vexalloc->model==HVEX_INDEX) {
		Expr = hvex_newmodel(vexalloc->model, vexalloc->width);
		Expr->left = vexalloc->left;
		Expr->right = vexalloc->right;
		gen_vex_operand(Expr, vexalloc->args);
		// Fix the name
		hvex_t* VexOp = Expr->operands;
		hvex_unlinkop(VexOp);
		hvex_index_setname(Expr, hvex_vec_getname(VexOp));
		hvex_free(VexOp);
	}
	else {
		Expr = hvex_newmodel(vexalloc->model, vexalloc->width);
		Expr->left = vexalloc->left;
		Expr->right = vexalloc->right;
		gen_vex_operand(Expr, vexalloc->args);
	}

	assert(Expr->model!=HVEX_FUNCTION);

	return Expr;
}
hvex_t* VexAlloc_GenVex_Ref(hvex_dico_ref_t* ref) {
	if(ref==NULL) return NULL;

	hvex_t* Expr = VexAlloc_GenVex_Elt(ref->ref.elt);
	unsigned crop_left = ref->ref.elt->left - ref->ref.left;
	unsigned crop_right = ref->ref.right - ref->ref.elt->right;
	// FIXME Should use cropabs directly?
	if(crop_left>0 || crop_right>0) {
		hvex_croprel(Expr, crop_left, crop_right);
	}

	Expr->pad = ref->extend;

	if(ref->width > ref->ref.width) {
		hvex_resize(Expr, ref->width);
	}

	return Expr;
}

void VexAlloc_Print_Elt_be(hvex_dico_elt_t* elt, const char* beg, const char* end) {
	hvex_t* vex = VexAlloc_GenVex_Elt(elt);
	if(beg!=NULL) printf(beg);
	hvex_print_b(vex);
	if(end!=NULL) printf(end);
	hvex_free(vex);
}
void VexAlloc_Print_Ref_be(hvex_dico_ref_t* ref, const char* beg, const char* end) {
	hvex_t* vex = VexAlloc_GenVex_Ref(ref);
	if(beg!=NULL) printf(beg);
	hvex_print_b(vex);
	if(end!=NULL) printf(end);
	hvex_free(vex);
}



// Check depth in the dictionary (this mainly checks cycles in the tree)

void VexAlloc_CheckAbort_Elt(hvex_dico_elt_t* elt, unsigned level) {
	if(level >= 50) abort();
	foreach(elt->args, scan) {
		hvex_dico_ref_t* ref = scan->DATA;
		VexAlloc_CheckAbort_Elt(ref->ref.elt, level+1);
	}
}
void VexAlloc_CheckAbort(hvex_dico_t* dico) {
	avl_p_foreach(&dico->all_elts, scan) {
		VexAlloc_CheckAbort_Elt(scan->data, 0);
	}
}

// Apply operator sharing in the dictionary

// FIXME the current implementation is very slow
// There are too many vexalloc->vex conversions and VexSlice and VexSimplify calls
// Also, it has a very low genericity
// It only checks cropping LEFT, not right, and only works if the cropped result has the same OPER value
//   => this won't be the case in general
int VexAlloc_Share_Expensive_ipnode(hvex_dico_t* vexalloc, avl_pp_node_t* pp_node) {
	unsigned count = 0;

	chain_list* list = pp_node->data;

	// to enable debug on this function specifically
	//#define DBGSIMP

	#ifdef DBGSIMP  // Display for debug
	dbgprintf("Entering sharing for hvex model '%s'\n", (char*)pp_node->key);
	dbgprintf("  There are %u elements\n", ChainList_Count(list));
	#endif

	// THe list of remaining elements
	chain_list* kept_elts = NULL;

	// Scan all elements
	foreach(list, scan1) {
		hvex_dico_elt_t* elt1 = scan1->DATA;
		if(elt1==NULL) continue;  // In case it was already deleted

		// Scan all other elements
		// If sharing is detected, the elt2 is freed and scan2->DATA is made NULL
		foreach(scan1->NEXT, scan2) {
			hvex_dico_elt_t* elt2 = scan2->DATA;
			if(elt2==NULL) continue;  // In case it was already deleted
			// If they have same size, we know they are already different
			if(elt2->left==elt1->left) continue;
			// Check inclusion
			hvex_dico_elt_t* elt_long = elt1;
			hvex_dico_elt_t* elt_short = elt2;
			if(elt_long->left < elt_short->left) Swap(elt_long, elt_short);
			hvex_t* vex_long = VexAlloc_GenVex_Elt(elt_long);
			hvex_t* vex_short = VexAlloc_GenVex_Elt(elt_short);
			#ifdef DBGSIMP  // Display for debug
			dbgprintf("Comparing VEX: "); hvex_print_bn(vex_long);
			dbgprintf("         with: "); hvex_print_bn(vex_short);
			#endif
			hvex_croprel(vex_long, elt_long->left - elt_short->left, 0);
			hvex_simp(vex_long);
			hvex_simp(vex_short);
			#ifdef DBGSIMP  // Display for debug
			dbgprintf("  Cropped VEX: "); hvex_print_bn(vex_long);
			dbgprintf("  Cropped VEX: "); hvex_print_bn(vex_short);
			#endif
			if(hvex_cmp(vex_long, vex_short)==0) {
				#ifdef DBGSIMP  // Display for debug
				dbgprintf("       Result: SHARE\n");
				#endif
				// These expressions are identical. Jackpot!
				if(elt1->left < elt2->left) {
					// Move elt2 in place of elt1
					Swap(scan1->DATA, scan2->DATA);
					Swap(elt1, elt2);
				}
				// Move all references from elt2 to elt1
				foreach(elt2->refs, scanRef) {
					hvex_dico_ref_t* ref = scanRef->DATA;
					ref->ref.elt = elt1;
					elt1->refs = addchain(elt1->refs, ref);
				}
				freechain(elt2->refs);
				elt2->refs = NULL;

				// FIXME The element should not be removed this abruptly
				// The shorter element may be a part of the representation of the larger element
				// Example:
				//   the 5-bits result of   : {1:0:2:s}(i(1:0:2:s) + "11"(1:0:2:u)) + {4:0:5:s}(j(2:0:3:s) & "00"(1:0:2:u))
				//   and the 2-bits element : {1:0:2:s}(i(1:0:2:s) + "11"(1:0:2:u))

				// Remove elt2 from the dictionary
				VexAlloc_Elt_Free(elt2);
				scan2->DATA = NULL;
				avl_p_rem_key(&vexalloc->all_elts, elt2);
				// Increment the count of things shared
				count++;

				// For debug
				//VexAlloc_CheckAbort(vexalloc);
			}
			else {
				#ifdef DBGSIMP  // Display for debug
				dbgprintf("       Result: Keep\n");
				#endif
			}
			// Free the temporary VEX expressions
			hvex_free(vex_long);
			hvex_free(vex_short);
		}  // Scan2

		kept_elts = addchain(kept_elts, elt1);  // This element will be kept
	}  // Scan1

	// Replace the original list of Elt
	freechain(list);
	pp_node->data = kept_elts;

	#ifdef DBGSIMP
		#undef DBGSIMP
	#endif

	return count;
}

void VexAlloc_Share_Expensive(hvex_dico_t* vexalloc) {
	avl_pp_node_t* pp_node = NULL;
	bool b;

	b = avl_pp_find(&vexalloc->elt_others, HVEX_ADD->name, &pp_node);
	if(b==true) {
		VexAlloc_Share_Expensive_ipnode(vexalloc, pp_node);
	}

	b = avl_pp_find(&vexalloc->elt_others, HVEX_SUB->name, &pp_node);
	if(b==true) {
		VexAlloc_Share_Expensive_ipnode(vexalloc, pp_node);
	}

	b = avl_pp_find(&vexalloc->elt_others, HVEX_MUL->name, &pp_node);
	if(b==true) {
		VexAlloc_Share_Expensive_ipnode(vexalloc, pp_node);
	}

}


