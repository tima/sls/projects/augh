
#ifndef _HVEX_CALC_H_
#define _HVEX_CALC_H_


void hvex_calc_fillbuf(unsigned widthD, char* dest, unsigned widthS, char* src, char pad);
static inline void hvex_calc_fillrepeat(unsigned width, char* dest, char bit) {
	for(unsigned i=0; i<width; i++) dest[i] = bit;
}

char hvex_calc_add(unsigned widthA, char* inA, unsigned widthB, char* inB, char padB, char inC);
char hvex_calc_sub(unsigned widthA, char* inA, unsigned widthB, char* inB, char padB, char inC);

void hvex_calc_not(unsigned width, char* in, char* res);
void hvex_calc_and(unsigned width, char* inA, char* inB, char* res);
void hvex_calc_nand(unsigned width, char* inA, char* inB, char* res);
void hvex_calc_or(unsigned width, char* inA, char* inB, char* res);
void hvex_calc_nor(unsigned width, char* inA, char* inB, char* res);
void hvex_calc_xor(unsigned width, char* inA, char* inB, char* res);
void hvex_calc_nxor(unsigned width, char* inA, char* inB, char* res);

// Return -1 if A<B, 0 if A=B, 1 if A>B, '-' if a relevant '-' bit is found
int hvex_calc_cmpu(unsigned widthA, char* inA, unsigned widthB, char* inB);


#endif  // _HVEX_CALC_H_

