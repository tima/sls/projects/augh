
#ifndef _HVEX_DICO_H_
#define _HVEX_DICO_H_

#include <stdbool.h>

#include "hvex.h"



// The special structure representing an element in the dictionary of expressions.
typedef struct hvex_dico_elt_t  hvex_dico_elt_t;
typedef struct hvex_dico_ref_t  hvex_dico_ref_t;
typedef struct hvex_dico_t      hvex_dico_t;

// The structure linking to an element stored into the dictionary
// If width/indexes are not considered, set to zero
struct hvex_dico_ref_t {
	// Details of the resulting element
	// Extension is only on the left
	unsigned width;
	char     extend;  // Also indicates signedness
	// The referenced element and the section of result that is reused
	struct {
		hvex_dico_elt_t* elt;
		unsigned width;
		unsigned left;
		unsigned right;
	} ref;
};

// An element stored into the dictionary
// The width is the maximum that can be obtained from the symbol/operation/function
// Any reference wanting more will do (un)signed extension.
struct hvex_dico_elt_t {
	hvex_model_t* model;

	unsigned width;
	unsigned left;
	unsigned right;

	void*       data;
	unsigned    args_nb;
	chain_list* args;

	// List of all references. Contains vex_alloc_ref_t*
	chain_list* refs;
};

// The main dictionary structure
struct hvex_dico_t {
	// All elements
	avl_pp_tree_t elt_lit;     // Directly contains the elements
	avl_pp_tree_t elt_sym;     // One element per symbol. Maximum width of all references.
	avl_pp_tree_t elt_others;  // Other models. Key = model name, data = a chain_list* of elements
	// All elements in the dictionary
	avl_p_tree_t all_elts;
};


void VexAlloc_Init(hvex_dico_t* dico);
void VexAlloc_Clear(hvex_dico_t* dico);

bool VexAlloc_Add_raw(hvex_dico_t* dico, hvex_t* Expr, hvex_dico_ref_t** pRef);
hvex_dico_ref_t* VexAlloc_Add(hvex_dico_t* dico, hvex_t* Expr);

unsigned VexAlloc_Lit2Uint(hvex_dico_elt_t* Expr);

char VexAllocElt_GetLeftBit_Char(hvex_dico_elt_t* elt);
char VexAllocRef_GetLeftBit_Char(hvex_dico_ref_t* ref);

hvex_t* VexAlloc_GenVex_Elt(hvex_dico_elt_t* elt);
hvex_t* VexAlloc_GenVex_Ref(hvex_dico_ref_t* ref);

void VexAlloc_Print_Elt_be(hvex_dico_elt_t* elt, const char* beg, const char* end);
void VexAlloc_Print_Ref_be(hvex_dico_ref_t* ref, const char* beg, const char* end);

void VexAlloc_Share_Expensive(hvex_dico_t* dico);



#endif // _HVEX_DICO_H_

