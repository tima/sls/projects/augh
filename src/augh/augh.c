
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "../../config.h"

#include "auto/auto.h"
#include "auto/techno.h"
#include "plugins/plugins.h"
#include "auto/command.h"


#define AUGHRC ".aughrc"


// Print the program version
static void PrintDesc(FILE* F) {
	fprintfm(F,
		"This is %s\n", PACKAGE_STRING,
		"High-level synthesis of C programs.\n",
		NULL
	);
}
static void PrintVersion(FILE* F) {
	fprintfm(F,
		"%s\n", PACKAGE_STRING,
		"This is free software; see the source for copying conditions. There is NO\n"
		"warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.\n",
		NULL
	);
}



//============================================
// Command-line parameters
//============================================

// Launch the command interpreter
static bool  enable_cmd = false;
static char* script_file = NULL;

// Print the program usage
static void PrintUsage_Full(char** argv) {
	PrintDesc(stdout);
	printfm(
		"\n"
		"Syntax: %s <parameters> <cfile>\n", argv[0],
		"\n"
		"Parameters:\n"
		"  --version           Print the raw version number (%s) and exits.\n", PACKAGE_VERSION,
		"  --help, -h, -u      Display this help message and exits.\n"
		"  -v                  Verbose\n"
		"  -V                  Very verbose\n"
		"  -s                  Silent [default]\n"
		"  -ex <commands>      Execute a command with the command interpreter.\n"
		"  -source <filename>  Execute commands from the specified file.\n"
		"  -plugin, -p <name>  Load a plugin.\n"
		"\n"
		"Specify input design:\n"
		"  -load <filename>    Name of the C source file to load.\n"
		"  -cflags <flags>     Flags for the C preprocessor.\n"
		"  -cflags+, -cflags-add <flags>\n"
		"                      Add flags for the C preprocessor.\n"
		"  -I, -D              Same meaning and syntax than GCC.\n"
		"  -inline-all         Ask to inline all function calls.\n"
		"\n"
		"Parameters for the synthesis flow:\n"
		"  -i                  Interactive mode, launches an embedded command interpreter.\n"
		"  -script <filename>  Launch the command interpreter, fetching the commands from a script file.\n"
		"  -vhdl-dir <dirname> Directory where VHDL files are saved (default: %s.\n", DEFAULT_VHDL_DIR,
		"  -vhdl-prefix <prefix>\n"
		"                      A prefix for all VHDL component names and instance names.\n"
		"  -board <board-name> Select an FPGA board to do synthesis on.\n"
		"  -board-fpga <board-name> <fpga-name>\n"
		"                      For multi-FPGA boards, select a particular FPGA.\n"
		"  -board-clock <clock-name> \n"
		"                      Select a clock source on the selected board.\n"
		"  -techno <name>      Set the technology.\n"
		"  -chip <name>        Use the hardware resource limits for a particular chip name.\n"
		"  -speed <name>       Use the specified technology speed grade.\n"
		"  -pkg <name>         Use the specified FPGA package.\n"
		"  -chip-ratio <val>   Set the ratio of the chip resources to use (0 to 1 or in percent).\n"
		"  -hwlim <string>     Set limits for resource utilization. Example: 'lut6:1000/dff:500'\n"
		"  -freq <value>       Set the target frequency. Examples: '100M', '0.25G', '233.3333M', '233.3333e6'\n"
		"  -reset-level <value>\n"
		"                      Set the reset active level: 0 or 1\n"
		"\n"
		"Parameters for optimization level:\n"
		"  -O0                 Only transcript to VHDL.\n"
		"  -O1                 Simplify and schedule design.\n"
		"  -O2                 Launch fast elaboration.\n"
		"  -O3                 Launch precise elaboration, use more powerful simplification functions.\n"
		"\n"
		"Parameters for the circuit to generate:\n"
		"  -entity <name>      Name of the generated top-level VHDL entity [%s].\n", DEFAULT_ENTITY,
		"  -no-start           Don't add a wait-on-start loop after circuit reset.\n"
		//"  -top-func           Specify the name of the function to use as top-level.\n"
		//"  -inf-loop           Insert the circuit in an infinite loop.\n"
		//"                      Otherwise, after each run of the main function,\n"
		//"                      the circuit returns to the reset state.\n"
		"Parameters for the generation flow:\n"
		"  -no-vhdl            Skip generation of VHDL files\n"
		"  -no-synth-prj       Skip generation of back-end synthesis project files\n"
		"  -c                  Don't launch back-end logic synthesis operations\n"
		"\n"
		"Information about embedded data and calibration:\n"
		"  info <topic>\n"
		"  Available topics:\n"
		"    technos           Display all available technologies.\n"
		"    technos-data      Display all available technologies, with all details.\n"
		"    techno <name>     Display details for one technology.\n"
		"    chips             Display all available chips.\n"
		"    chips-data        Display all available chips, with all details.\n"
		"    chip <name>       Display details for one chip.\n"
		, NULL
	);
}

// Parse the command-line parameters
static void read_params(int argc, char** argv, implem_t* Implem, augh_globals_t* params) {

	for(unsigned argidx=1; argidx<argc; argidx++) {
		char* arg = argv[argidx];

		// A little utility function
		void check_wantargnb(unsigned nb) {
			unsigned extra = argc - 1 - argidx;
			if(extra < nb) {
				printf("Error: The command-line parameter '%s' expects %u extra parameter(s) but only %u are given.\n", arg, nb, extra);
				exit(EXIT_FAILURE);
			}
		}

		if(strcmp(arg, "--version")==0) {
			PrintVersion(stdout);
			exit(EXIT_SUCCESS);
		}
		if(strcmp(arg, "-h")==0 || strcmp(arg, "-u")==0 || strcmp(arg, "--help")==0) {
			PrintUsage_Full(argv);
			exit(EXIT_SUCCESS);
		}

		// Getting info about internal data and calibrations
		if(strcmp(arg, "info")==0) {
			check_wantargnb(1);
			argidx++;
			char* topic = argv[argidx];
			// Info topics
			if(strcmp(topic, "technos")==0) {
				Techno_DispAllTechno(NULL);
			}
			else if(strcmp(topic, "technos-data")==0) {
				Techno_DispAllTechnoData(NULL);
			}
			else if(strcmp(topic, "techno")==0) {
				argidx++;
				if(argidx>=argc) {
					printf("ERROR: Missing techno name.\n");
					exit(EXIT_FAILURE);
				}
				else {
					char* techno_name = argv[argidx];
					int z = Techno_DispTechnoData_user(techno_name, NULL);
					if(z!=0) exit(EXIT_FAILURE);
				}
			}
			else if(strcmp(topic, "chips")==0) {
				Techno_DispAllChips(NULL);
			}
			else if(strcmp(topic, "chips-data")==0) {
				Techno_DispAllChipsData(NULL);
			}
			else if(strcmp(topic, "chip")==0) {
				argidx++;
				if(argidx>=argc) {
					printf("ERROR: Missing chip name.\n");
					exit(EXIT_FAILURE);
				}
				else {
					char* chip_name = argv[argidx];
					int z = Techno_DispChipData_user(chip_name, NULL);
					if(z!=0) exit(EXIT_FAILURE);
				}
			}
			else {
				printf("ERROR: Unknown topic '%s'.\n", topic);
				exit(EXIT_FAILURE);
			}
			exit(EXIT_SUCCESS);
		}

		// Execute commands with the command interpreter
		else if(strcmp(arg, "-ex")==0) {
			check_wantargnb(1);
			argidx++;
			int z = AughCmd_Exec_str(Implem, argv[argidx]);
			if(z!=0) exit(EXIT_FAILURE);
		}
		else if(strcmp(arg, "-source")==0) {
			check_wantargnb(1);
			argidx++;
			int z = AughCmd_Script(Implem, argv[argidx]);
			if(z!=0) exit(EXIT_FAILURE);
		}

		// Verbosity levels
		else if(strcmp(arg, "-s")==0) {
			params->verbose.level = VERBOSE_SILENT;
			params->verbose.silent = true;
			params->verbose.lvl1 = false;
			params->verbose.lvl2 = false;
		}
		else if(strcmp(arg, "-v")==0) {
			params->verbose.level = VERBOSE_LEVEL1;
			params->verbose.silent = false;
			params->verbose.lvl1 = true;
			params->verbose.lvl2 = false;
		}
		else if(strcmp(arg, "-V")==0 || strcmp(arg, "-vv")==0) {
			params->verbose.level = VERBOSE_LEVEL2;
			params->verbose.silent = false;
			params->verbose.lvl1 = false;
			params->verbose.lvl2 = true;
		}

		else if(strcmp(arg, "-plugin")==0 || strcmp(arg, "-p")==0) {
			check_wantargnb(1);

			argidx++;
			char* plugin_name = argv[argidx];

			int z = Plugin_Load_cmdline(plugin_name);
			if(z!=0) {
				printf("ERROR: Loading plugin '%s' failed (code %d).\n", plugin_name, z);
				exit(EXIT_FAILURE);
			}
		}

		else if(strcmp(arg, "-load")==0) {
			check_wantargnb(1);
			if(Implem->env.input_file!=NULL) {
				printf("ERROR: Only one source file can be specified.\n");
				exit(EXIT_FAILURE);
			}
			argidx++;
			Implem->env.input_file = argv[argidx];
		}
		else if(strcmp(arg, "-cflags")==0) {
			check_wantargnb(1);
			argidx++;
			freechain(Implem->env.cflags);
			Implem->env.cflags = addchain(NULL, argv[argidx]);
		}
		else if(strcmp(arg, "-cflags+")==0 || strcmp(arg, "-cflags-add")==0) {
			check_wantargnb(1);
			argidx++;
			Implem->env.cflags = ChainList_Add_Tail(Implem->env.cflags, argv[argidx]);
		}
		else if(strcmp(arg, "-inline-all")==0) {
			Implem->env.inline_all = true;
		}

		else if(strcmp(arg, "-vhdl-dir")==0) {
			check_wantargnb(1);
			argidx++;
			Implem->env.vhdl_dir = argv[argidx];
		}
		else if(strcmp(arg, "-vhdl-prefix")==0) {
			check_wantargnb(1);
			argidx++;
			Implem->env.vhdl_prefix = argv[argidx];
		}

		else if(strcmp(arg, "-i")==0) {
			enable_cmd = true;
		}

		else if(strcmp(arg, "-script")==0) {
			check_wantargnb(1);
			argidx++;
			script_file = argv[argidx];
		}
		else if(strcmp(arg, "-board")==0) {
			check_wantargnb(1);
			argidx++;
			// Get the specified board
			char* board_name = namealloc(argv[argidx]);
			board_t* board = Techno_GetBoard(board_name);
			if(board==NULL) {
				printf("ERROR: Unknown board '%s'.\n", board_name);
				exit(EXIT_FAILURE);
			}
			// Set the new board
			if(avl_pp_count(&board->fpgas)!=1) {
				printf("ERROR: Only 1-FPGA boards are handled.\n");
				exit(EXIT_FAILURE);
			}
			board_fpga_t* board_fpga = avl_pp_root_data(&board->fpgas);
			Techno_SetBoardFpga(Implem, board_fpga);
		}
		else if(strcmp(arg, "-board-fpga")==0) {
			check_wantargnb(2);
			argidx++;
			// Get the specified board
			char* board_name = namealloc(argv[argidx]);
			board_t* board = Techno_GetBoard(board_name);
			if(board==NULL) {
				printf("ERROR: Unknown board '%s'.\n", board_name);
				exit(EXIT_FAILURE);
			}
			// Get the specified FPGA
			argidx++;
			char* fpga_name = namealloc(argv[argidx]);
			board_fpga_t* board_fpga = NULL;
			avl_pp_find_data(&board->fpgas, namealloc(fpga_name), (void**)&board_fpga);
			if(board_fpga==NULL) {
				printf("ERROR : The FPGA '%s' on board '%s' was not found.\n", fpga_name, board_name);
				exit(EXIT_FAILURE);
			}
			Techno_SetBoardFpga(Implem, board_fpga);
		}
		else if(strcmp(arg, "-board-clock")==0) {
			check_wantargnb(1);
			argidx++;
			char* name = namealloc(argv[argidx]);
			int z = Techno_SetBoardClock_FromName(Implem, name);
			if(z!=0) exit(EXIT_FAILURE);
		}
		else if(strcmp(arg, "-techno")==0) {
			check_wantargnb(1);
			argidx++;
			// Get the specified techno
			char* newtechno_name = namealloc(argv[argidx]);
			techno_t* newtechno = Techno_GetTechno(newtechno_name);
			if(newtechno==NULL) {
				printf("ERROR: Unknown technology '%s'.\n", newtechno_name);
				exit(EXIT_FAILURE);
			}
			// Set the new techno
			if(Implem->synth_target->techno!=NULL && newtechno!=Implem->synth_target->techno) {
				printf("Note: Overriding previous/default techno '%s' by '%s'.\n", Implem->synth_target->techno->name, newtechno->name);
			}
			Techno_SynthTarget_SetTechno(Implem->synth_target, newtechno);
		}
		else if(strcmp(arg, "-speed")==0) {
			if(Implem->synth_target->techno==NULL) {
				printf("ERROR: A target technology must be set for '%s'.\n", arg);
				exit(EXIT_FAILURE);
			}
			check_wantargnb(1);
			argidx++;
			// Get the specified speed grade
			char* name = namealloc(argv[argidx]);
			techno_timing_t* speed = Techno_GetSpeed(Implem->synth_target->techno, name);
			if(speed==NULL) {
				printf("ERROR : The speed grade '%s' was not found.\n", name);
				exit(EXIT_FAILURE);
			}
			Techno_SynthTarget_SetTechnoSpeed(Implem->synth_target, speed);
		}
		else if(strcmp(arg, "-pkg")==0 || strcmp(arg, "-package")==0) {
			if(Implem->synth_target->model==NULL) {
				printf("ERROR: A target FPGA must be set for '%s'.\n", arg);
				exit(EXIT_FAILURE);
			}
			check_wantargnb(1);
			argidx++;
			// Get the specified package
			char* name = namealloc(argv[argidx]);
			fpga_pkg_model_t* pkg = Techno_FPGA_GetPkg(Implem->synth_target->model, name);
			if(pkg==NULL) {
				printf("ERROR : The package '%s' was not found.\n", name);
				exit(EXIT_FAILURE);
			}
			Implem->synth_target->package = pkg;
		}
		else if(strcmp(arg, "-chip")==0) {
			check_wantargnb(1);
			argidx++;
			// Get the specified chip model
			char* newchip_name = namealloc(argv[argidx]);
			fpga_model_t* newchip = Techno_GetFPGA(newchip_name);
			if(newchip==NULL) {
				printf("ERROR: Unknown FPGA model '%s'.\n", newchip_name);
				exit(EXIT_FAILURE);
			}
			// Set the new FPGA model
			if(Implem->synth_target->model!=NULL && newchip!=Implem->synth_target->model) {
				printf("Note: Overriding previous/default FPGA model '%s' by '%s'.\n", Implem->synth_target->model->name, newchip->name);
			}
			Techno_SynthTarget_SetChipModel(Implem->synth_target, newchip);
		}
		else if(strcmp(arg, "-chip-ratio")==0) {
			check_wantargnb(1);
			argidx++;
			double ratio = 1;
			sscanf(argv[argidx], "%lf", &ratio);
			if(argv[argidx][strlen(argv[argidx])-1]=='%') {
				ratio /= 100;
			}
			if(ratio<=0) {
				printf("ERROR : Invalid chip usage ratio value.\n");
				exit(EXIT_FAILURE);
			}
			if(ratio>1) {
				printf("Warning : Chip usage ratio is above 100%%.\n");
			}
			Implem->synth_target->model_ratio = ratio;
		}

		else if(strcmp(arg, "-hwlim")==0) {
			check_wantargnb(1);
			argidx++;
			freeptype(Implem->synth_target->resources);
			Implem->synth_target->resources = augh_read_hwlimits(Implem->synth_target->techno, argv[argidx]);
			if(Implem->synth_target->resources==NULL) {
				printf("ERROR Could not read the resource limits.\n");
				exit(EXIT_FAILURE);
			}
			Implem->synth_target->usage_is_default = false;
		}
		else if(strcmp(arg, "-freq")==0) {
			check_wantargnb(1);
			argidx++;
			double val = atof_multiplier(argv[argidx]);
			if(val<=0) exit(EXIT_FAILURE);
			Techno_SynthTarget_SetFrequency(Implem->synth_target, val);
		}
		else if(strcmp(arg, "-reset-level")==0) {
			check_wantargnb(1);
			argidx++;
			char* level_str = argv[argidx];
			if(strcmp(level_str, "0")==0 || strcmp(level_str, "'0'")==0) {
				Implem->synth_target->reset_active_state = '0';
			}
			else if(strcmp(level_str, "1")==0 || strcmp(level_str, "'1'")==0) {
				Implem->synth_target->reset_active_state = '1';
			}
			else {
				printf("ERROR: Invalid reset level value '%s'. '0' or '1' expected.\n", level_str);
				exit(EXIT_FAILURE);
			}
		}

		else if(strcmp(arg, "-entity")==0) {
			check_wantargnb(1);
			argidx++;
			char* name = namealloc(argv[argidx]);
			// Remove the name specified in globals->implem_models
			avl_pp_rem_key(&Implem->env.globals->implem_models, Implem->netlist.top->name);
			// Rename the top-level entity
			Implem->env.entity = name;
			Implem->netlist.top->name = name;
			Implem->compmod.model_name = name;
			// Save the new name in globals->implem_models
			avl_pp_add_overwrite(&Implem->env.globals->implem_models, name, Implem);
		}
		else if(strcmp(arg, "-no-start")==0) {
			Implem->env.wait_on_start = false;
		}

		else if(strcmp(arg, "-no-vhdl")==0) {
			params->gen_vhdl = false;
		}
		else if(strcmp(arg, "-no-synth-prj")==0) {
			params->gen_synth_proj = false;
		}
		else if(strcmp(arg, "-c")==0) {
			params->stop_vhdl = true;
		}

		else if(arg[0]=='-' && (arg[1]=='I' || arg[1]=='D')) {
			char* cflags = NULL;
			if(arg[2]!=0) cflags = arg;
			else {
				if(argidx>=argc) {
					printf("ERROR: Missing parameter for '%s'.\n", arg);
					exit(EXIT_FAILURE);
				}
				argidx++;
				char buf[3 + strlen(argv[argidx]) + 1];
				sprintf(buf, "%s %s", arg, argv[argidx]);
				cflags = stralloc(buf);
			}
			if(cflags!=NULL) Implem->env.cflags = ChainList_Add_Tail(Implem->env.cflags, cflags);
		}

		else if(arg[0]=='-' && arg[1]=='O') {
			unsigned level = 0;
			char c = arg[2];
			if     (c=='0') level = 0;
			else if(c=='1') level = 1;
			else if(c=='2') level = 2;
			else if(c=='3') level = 3;
			else {
				printf("ERROR: Unknown optimization parameter '%s'.\n", arg);
				exit(EXIT_FAILURE);
			}
			OptimLevel_Set(params, level);
		}

		else if(arg[0]=='-') {
			printf("ERROR: Unknown parameter '%s'.\n", arg);
			exit(EXIT_FAILURE);
		}

		// This is the source file to load
		else {
			if(Implem->env.input_file!=NULL) {
				printf("ERROR: Taking parameter '%s' as an input source file but '%s' was already given.\n", arg, Implem->env.input_file);
				exit(EXIT_FAILURE);
			}
			Implem->env.input_file = arg;
		}

	}  // Scan the parameters

}



//============================================
// Main AUGH function
//============================================

int main(int argc, char** argv) {

	//====================================================================
	// Initialization
	//====================================================================

	// Initialize the AUGH lib
	aughlib_init();

	augh_globals_t* globals = augh_globals_new();

	// Some plugins may need this to initialize
	globals->argv0 = argv[0];

	// Create the top-level Implem
	implem_t* Implem = augh_implem_new(globals);
	// Index in the list of component models
	avl_pp_add_overwrite(&Implem->env.globals->implem_models, Implem->netlist.top->name, Implem);
	globals->top_implem = Implem;

	//====================================================================
	// Loading rc-files
	//====================================================================

	bool env_aughrc_skip = false;
	if( getenv("AUGHRC_SKIP")!=NULL ) env_aughrc_skip = true;

	if(env_aughrc_skip==false) {
		char* env_aughrc = getenv("AUGHRC");
		if(env_aughrc==NULL) env_aughrc = AUGHRC;

		bool b = AughCmd_Loadrc(env_aughrc, Implem);

		if(b==false) {
			// Try the rc-file in home directory
			char* env_home = getenv("HOME");
			if(env_home!=NULL) {
				char buffer[strlen(env_home) + 2 + strlen(AUGHRC)];
				sprintf(buffer, "%s/%s", env_home, AUGHRC);
				AughCmd_Loadrc(buffer, Implem);
			}
		}
	}

	//====================================================================
	// Parsing command-line parameters
	//====================================================================

	read_params(argc, argv, Implem, globals);

	if(Implem->synth_target->model!=NULL && Implem->synth_target->usage_is_default==true) {
		double ratio = Implem->synth_target->model_ratio;
		if(ratio<=0) {
			ratio = 0.8;
			printf("WARNING : Applying a default chip usage ratio of %lf.\n", ratio);
			Implem->synth_target->model_ratio = ratio;
			Implem->synth_target->usage_is_default = false;
		}
		foreach(Implem->synth_target->resources, scan) {
			scan->TYPE = ratio * scan->TYPE;
		}
	}

	if(globals->verbose.level > VERBOSE_SILENT) PrintDesc(stdout);

	// Diaplay some info
	if(globals->verbose.level > VERBOSE_SILENT) {
		if(Implem->env.cflags!=NULL) {
			printf("C preprocessor flags:\n");
			foreach(Implem->env.cflags, scan) printf("  %s\n", (char*)scan->DATA);
		}
		if(Implem->synth_target->techno!=NULL) {
			printf("Chosen technology : %s\n", Implem->synth_target->techno->name);
		}
		if(Implem->synth_target->timing!=NULL) {
			printf("Speed grade : %s\n", Implem->synth_target->timing->name);
		}
		if(Implem->synth_target->model!=NULL) {
			printf("Chosen chip : %s\n", Implem->synth_target->model->name);
		}
		if(Implem->synth_target->package!=NULL) {
			printf("Package : %s\n", Implem->synth_target->package->name);
		}
		if(Implem->synth_target->board_fpga!=NULL) {
			printf("  (name '%s' on board '%s')\n", Implem->synth_target->board_fpga->name, Implem->synth_target->board_fpga->board->name);
		}
		if(Implem->synth_target->resources!=NULL) {
			printf("Resource limits : ");
			foreach(Implem->synth_target->resources, scan) {
				if(scan!=Implem->synth_target->resources) printf(", ");
				printf("%s:%ld", (char*)scan->DATA, scan->TYPE);
			}
			printf("\n");
		}
		printf("Optimization level: %u\n", globals->optim_level);
	}

	//====================================================================
	// Command interpreter and scripts
	//====================================================================

	int return_val = 0;

	if(enable_cmd==true) {
		return_val = AughCmd_Interactive(Implem);
	}
	else if(script_file!=NULL) {
		return_val = AughCmd_Script(Implem, script_file);
	}
	else {
		return_val = augh_automatic(Implem);
	}

	// Free the data
	augh_globals_freechildren(globals);
	augh_globals_free(globals);

	// Free data from libraries
	aughlib_clear();

	if(return_val!=0) return EXIT_FAILURE;
	return EXIT_SUCCESS;
}

