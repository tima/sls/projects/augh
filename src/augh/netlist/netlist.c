
/* Description

The module "netlist" contains data structures representing a netlist graph.
- Hierarchy of components
- Fast access to ports
- Only Input and Output ports (no inout)
- Netlist duplication functionality

Embedded component types:
FSM, multiplexer, register, signal, multi-port memory,
adder, subtracter, multiplier, shifter, rotator,
logic operators, comparator...

*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <ctype.h>

#include "../auto/techno.h"
#include "netlist.h"
#include "netlist_comps.h"
#include "netlist_access.h"
#include "netlist_vhdl.h"



//===================================================
// Allocation
//===================================================

// Create a pool of type : netlist_comp_model_t

#define POOL_prefix      pool_netlist_comp_model
#define POOL_data_t      netlist_comp_model_t
#define POOL_ALLOC_SIZE  100

#define POOL_INSTANCE             POOL_NETLIST_COMP_MODEL
#define POOL_INSTANCE_ATTRIBUTES  static

#include "../pool.h"

// Allocation wrappers

netlist_comp_model_t* Netlist_CompModel_New() {
	netlist_comp_model_t* model = pool_netlist_comp_model_pop(&POOL_NETLIST_COMP_MODEL);
	memset(model, 0, sizeof(*model));
	model->func_gen_vhdl  = Netlist_Comp_GenVHDL_Default;
	model->func_eval_size = Netlist_EvalSize_Default;
	model->inlvhdl_en = false;
	return model;
}
void Netlist_CompModel_Free(netlist_comp_model_t* model) {
	pool_netlist_comp_model_push(&POOL_NETLIST_COMP_MODEL, model);
}


// Create a pool of type : netlist_access_model_t

#define POOL_prefix      pool_netlist_access_model
#define POOL_data_t      netlist_access_model_t
#define POOL_ALLOC_SIZE  100

#define POOL_INSTANCE             POOL_NETLIST_ACCESS_MODEL
#define POOL_INSTANCE_ATTRIBUTES  static

#include "../pool.h"

// Allocation wrappers

netlist_access_model_t* Netlist_AccessModel_New() {
	netlist_access_model_t* model = pool_netlist_access_model_pop(&POOL_NETLIST_ACCESS_MODEL);
	memset(model, 0, sizeof(*model));
	avl_pi_init(&model->ports_name2offset);
	avl_ip_init(&model->ports_offset2name);
	return model;
}
void Netlist_AccessModel_Free(netlist_access_model_t* model) {
	pool_netlist_access_model_push(&POOL_NETLIST_ACCESS_MODEL, model);
}


// Create a pool of type : netlist_access_t

#define POOL_prefix      pool_netlist_access
#define POOL_data_t      netlist_access_t
#define POOL_ALLOC_SIZE  1000

#define POOL_INSTANCE             POOL_NETLIST_ACCESS
#define POOL_INSTANCE_ATTRIBUTES  static

#include "../pool.h"

// Allocation wrappers

netlist_access_t* Netlist_Access_New(netlist_access_model_t* model) {
	netlist_access_t* access = pool_netlist_access_pop(&POOL_NETLIST_ACCESS);
	memset(access, 0, sizeof(*access));
	access->model = model;

	// Create the private data
	if(model->data_size > 0) {
		access->data = calloc(1, model->data_size);
	}

	return access;
}

void Netlist_Access_Free(netlist_access_t* access) {
	// Free the private data
	if(access->data != NULL) {
		if(access->model->func_free_data != NULL) {
			access->model->func_free_data(access);
		}
		else {
			free(access->data);
		}
	}

	// Unlink from the parent component
	if(access->component!=NULL) {
		avl_pp_rem_key(&access->component->accesses, access->name);
	}

	// Free the structure
	pool_netlist_access_push(&POOL_NETLIST_ACCESS, access);
}


// Create a pool of type : netlist_comp_t

#define POOL_prefix      pool_netlist_comp
#define POOL_data_t      netlist_comp_t
#define POOL_ALLOC_SIZE  1000

#define POOL_INSTANCE             POOL_NETLIST_COMP
#define POOL_INSTANCE_ATTRIBUTES  static

#include "../pool.h"

// Allocation wrappers

netlist_comp_t* Netlist_Comp_New(netlist_comp_model_t* model) {
	netlist_comp_t* comp = pool_netlist_comp_pop(&POOL_NETLIST_COMP);
	memset(comp, 0, sizeof(*comp));
	comp->model = model;

	avl_pp_init(&comp->ports);
	avl_pp_init(&comp->children);
	avl_pp_init(&comp->accesses);

	// Create the private data
	if(model->data_size > 0) {
		comp->data = calloc(1, model->data_size);
	}

	return comp;
}

void Netlist_Comp_Free(netlist_comp_t* comp) {
	// Free the special data
	if(comp->model->func_free_data!=NULL) {
		comp->model->func_free_data(comp);
	}

	// Unlink from the parent component
	if(comp->father!=NULL) {
		avl_pp_rem_key(&comp->father->children, comp->name);
	}

	// Free the child components
	do {
		avl_pp_node_t* avl_node = avl_pp_first(&comp->children);
		if(avl_node==NULL) break;
		netlist_comp_t* child = avl_node->data;
		Netlist_Comp_Free(child);
	} while(1);

	// Free the accesses
	do {
		avl_pp_node_t* avl_node = avl_pp_first(&comp->accesses);
		if(avl_node==NULL) break;
		netlist_access_t* access = avl_node->data;
		Netlist_Access_Free(access);
	} while(1);

	// Free the ports
	do {
		avl_pp_node_t* avl_node = avl_pp_first(&comp->ports);
		if(avl_node==NULL) break;
		netlist_port_t* port = avl_node->data;
		Netlist_Port_Free(port);
	} while(1);

	// Free the structures
	avl_pp_reset(&comp->ports);
	avl_pp_reset(&comp->children);
	avl_pp_reset(&comp->accesses);
	pool_netlist_comp_push(&POOL_NETLIST_COMP, comp);
}


// Create a pool of type : netlist_port_t

#define POOL_prefix      pool_netlist_port
#define POOL_data_t      netlist_port_t
#define POOL_ALLOC_SIZE  1000

#define POOL_INSTANCE             POOL_NETLIST_PORT
#define POOL_INSTANCE_ATTRIBUTES  static

#include "../pool.h"

// Allocation wrappers

netlist_port_t* Netlist_Port_New() {
	netlist_port_t* port = pool_netlist_port_pop(&POOL_NETLIST_PORT);
	memset(port, 0, sizeof(*port));
	return port;
}

// Note: It is the responsibility of the user to check that a deleted port is referenced nowhere.
void Netlist_Port_Free(netlist_port_t* port) {
	// Free the connections
	Netlist_ValCat_FreeList(port->source);
	if(port->target_ports!=NULL) {
		avl_p_reset(port->target_ports);
		free(port->target_ports);
	}

	// Unlink from the parent component
	if(port->component!=NULL) {
		avl_pp_rem_key(&port->component->ports, port->name);
	}

	// Free the structure
	pool_netlist_port_push(&POOL_NETLIST_PORT, port);
}


// Create a pool of type : netlist_val_cat_t

#define POOL_prefix      pool_netlist_val_cat
#define POOL_data_t      netlist_val_cat_t
#define POOL_ALLOC_SIZE  1000

#define POOL_INSTANCE             POOL_NETLIST_VAL_CAT
#define POOL_INSTANCE_ATTRIBUTES  static

#include "../pool.h"

// Allocation wrappers

netlist_val_cat_t* Netlist_ValCat_New() {
	netlist_val_cat_t* elt = pool_netlist_val_cat_pop(&POOL_NETLIST_VAL_CAT);
	memset(elt, 0, sizeof(*elt));
	return elt;
}
void Netlist_ValCat_Free(netlist_val_cat_t* elt) {
	pool_netlist_val_cat_push(&POOL_NETLIST_VAL_CAT, elt);
}



//===================================================
// Initialization of the library
//===================================================

// All component models, to ensure unicity.
static avl_pp_tree_t NETLIST_COMPONENT_MODELS;
// All access models, to ensure unicity.
static avl_pp_tree_t NETLIST_ACCESS_MODELS;

avl_pp_tree_t* Netlist_CompModel_GetTree() {
	return &NETLIST_COMPONENT_MODELS;
}
avl_pp_tree_t* Netlist_AccessModel_GetTree() {
	return &NETLIST_ACCESS_MODELS;
}

// Functions to register a new identifier

bool Netlist_CompModel_IsRegistered(const char* name) {
	return avl_pp_isthere(&NETLIST_COMPONENT_MODELS, name);
}
bool Netlist_AccessModel_IsRegistered(const char* name) {
	return avl_pp_isthere(&NETLIST_ACCESS_MODELS, name);
}

bool Netlist_CompModel_Declare(netlist_comp_model_t* model) {
	return avl_pp_add_keep(&NETLIST_COMPONENT_MODELS, model->name, model);
}
netlist_comp_model_t* Netlist_CompModel_Get(const char* name) {
	netlist_comp_model_t* model = NULL;
	avl_pp_find_data(&NETLIST_COMPONENT_MODELS, name, (void**)&model);
	return model;
}
void Netlist_CompModel_Remove(const char* name) {
	avl_pp_rem_key(&NETLIST_COMPONENT_MODELS, name);
}

bool Netlist_AccessModel_Declare(netlist_access_model_t* model) {
	return avl_pp_add_keep(&NETLIST_ACCESS_MODELS, model->name, model);
}
netlist_access_model_t* Netlist_AccessModel_Get(const char* name) {
	netlist_access_model_t* access = NULL;
	avl_pp_find_data(&NETLIST_ACCESS_MODELS, name, (void**)&access);
	return access;
}
void Netlist_AccessModel_Remove(const char* name) {
	avl_pp_rem_key(&NETLIST_ACCESS_MODELS, name);
}

// General initialization
void Netlist_Init() {

	avl_pp_init(&NETLIST_COMPONENT_MODELS);
	avl_pp_init(&NETLIST_ACCESS_MODELS);

	// Initialize the declarations of models

	// Component models
	netlist_comp_model_t* comp_model;

	comp_model = Netlist_Comp_Top_GetModel();
	Netlist_CompModel_Declare(comp_model);

	comp_model = Netlist_Comp_Fsm_GetModel();
	Netlist_CompModel_Declare(comp_model);

	comp_model = Netlist_Comp_Mux_GetModel();
	Netlist_CompModel_Declare(comp_model);
	comp_model = Netlist_MuxBin_GetModel();
	Netlist_CompModel_Declare(comp_model);

	comp_model = Netlist_Comp_Sig_GetModel();
	Netlist_CompModel_Declare(comp_model);

	comp_model = Netlist_Comp_Reg_GetModel();
	Netlist_CompModel_Declare(comp_model);

	comp_model = Netlist_Comp_Mem_GetModel();
	Netlist_CompModel_Declare(comp_model);

	comp_model = Netlist_Add_GetModel();
	Netlist_CompModel_Declare(comp_model);
	comp_model->flags |= NETLIST_COMPMOD_FLAG_CANSHARE;
	comp_model = Netlist_Sub_GetModel();
	Netlist_CompModel_Declare(comp_model);
	comp_model->flags |= NETLIST_COMPMOD_FLAG_CANSHARE;

	comp_model = Netlist_Mul_GetModel();
	Netlist_CompModel_Declare(comp_model);
	comp_model->flags |= NETLIST_COMPMOD_FLAG_CANSHARE;

	comp_model = Netlist_DivQR_GetModel();
	Netlist_CompModel_Declare(comp_model);
	comp_model->flags |= NETLIST_COMPMOD_FLAG_CANSHARE;

	comp_model = Netlist_ShL_GetModel();
	Netlist_CompModel_Declare(comp_model);
	comp_model->flags |= NETLIST_COMPMOD_FLAG_CANSHARE;
	comp_model = Netlist_ShR_GetModel();
	Netlist_CompModel_Declare(comp_model);
	comp_model->flags |= NETLIST_COMPMOD_FLAG_CANSHARE;
	comp_model = Netlist_RotL_GetModel();
	Netlist_CompModel_Declare(comp_model);
	comp_model->flags |= NETLIST_COMPMOD_FLAG_CANSHARE;
	comp_model = Netlist_RotR_GetModel();
	Netlist_CompModel_Declare(comp_model);
	comp_model->flags |= NETLIST_COMPMOD_FLAG_CANSHARE;

	comp_model = Netlist_Cmp_GetModel();
	Netlist_CompModel_Declare(comp_model);

	comp_model = Netlist_And_GetModel();
	Netlist_CompModel_Declare(comp_model);
	comp_model = Netlist_Nand_GetModel();
	Netlist_CompModel_Declare(comp_model);
	comp_model = Netlist_Or_GetModel();
	Netlist_CompModel_Declare(comp_model);
	comp_model = Netlist_Nor_GetModel();
	Netlist_CompModel_Declare(comp_model);
	comp_model = Netlist_Xor_GetModel();
	Netlist_CompModel_Declare(comp_model);
	comp_model = Netlist_Nxor_GetModel();
	Netlist_CompModel_Declare(comp_model);
	comp_model = Netlist_Not_GetModel();
	Netlist_CompModel_Declare(comp_model);

	comp_model = Netlist_ClkDiv_GetModel();
	Netlist_CompModel_Declare(comp_model);

	comp_model = Netlist_Fifo_GetModel();
	Netlist_CompModel_Declare(comp_model);

	comp_model = Netlist_Comp_PingPong_In_GetModel();
	Netlist_CompModel_Declare(comp_model);
	comp_model = Netlist_Comp_PingPong_Out_GetModel();
	Netlist_CompModel_Declare(comp_model);

	comp_model = Netlist_Comp_Uart_GetModel();
	Netlist_CompModel_Declare(comp_model);
	comp_model = Netlist_Comp_Uart_Rx_GetModel();
	Netlist_CompModel_Declare(comp_model);
	comp_model = Netlist_Comp_Uart_Tx_GetModel();
	Netlist_CompModel_Declare(comp_model);

	comp_model = Netlist_Comp_DiffClock_GetModel();
	Netlist_CompModel_Declare(comp_model);

	// Access models
	netlist_access_model_t* access_model;

	access_model = Netlist_Access_Clock_GetModel();
	Netlist_AccessModel_Declare(access_model);
	access_model = Netlist_Access_DiffClock_GetModel();
	Netlist_AccessModel_Declare(access_model);

	access_model = Netlist_Access_Reset_GetModel();
	Netlist_AccessModel_Declare(access_model);

	access_model = Netlist_Access_Start_GetModel();
	Netlist_AccessModel_Declare(access_model);

	access_model = Netlist_Access_WireIn_GetModel();
	Netlist_AccessModel_Declare(access_model);
	access_model = Netlist_Access_WireOut_GetModel();
	Netlist_AccessModel_Declare(access_model);

	access_model = Netlist_Access_FifoIn_GetModel();
	Netlist_AccessModel_Declare(access_model);
	access_model = Netlist_Access_FifoOut_GetModel();
	Netlist_AccessModel_Declare(access_model);

	access_model = Netlist_Access_MuxIn_GetModel();
	Netlist_AccessModel_Declare(access_model);

	access_model = Netlist_Access_MemRA_GetModel();
	Netlist_AccessModel_Declare(access_model);
	access_model = Netlist_Access_MemWA_GetModel();
	Netlist_AccessModel_Declare(access_model);
	access_model = Netlist_Access_MemD_GetModel();
	Netlist_AccessModel_Declare(access_model);
	access_model = Netlist_Access_MemCam_GetModel();
	Netlist_AccessModel_Declare(access_model);

	access_model = Netlist_Access_MemRA_Ext_GetModel();
	Netlist_AccessModel_Declare(access_model);
	access_model = Netlist_Access_MemWA_Ext_GetModel();
	Netlist_AccessModel_Declare(access_model);

	access_model = Netlist_Access_Uart_GetModel();
	Netlist_AccessModel_Declare(access_model);
	access_model = Netlist_Access_Uart_Rx_GetModel();
	Netlist_AccessModel_Declare(access_model);
	access_model = Netlist_Access_Uart_Tx_GetModel();
	Netlist_AccessModel_Declare(access_model);

}



//===================================================
// Miscellaneous functions
//===================================================

bool Netlist_IsBit10u(char c) {
	if(c=='0' || c=='1' || c=='-') return true;
	return false;
}
bool Netlist_IsBit10(char c) {
	if(c=='0' || c=='1') return true;
	return false;
}
char Netlist_EvalBit_Not(char c) {
	if(c=='0') return '1';
	if(c=='1') return '0';
	return '-';
}

// Function that can be used to set a default in component models
// Return NULL (no cost)
ptype_list* Netlist_EvalSize_Default(void* synth_target, netlist_comp_t* comp) {
	printf("WARNING: The default size eval function shouldn't be used (comp '%s' model '%s')\n", comp->name, comp->model->name);
	return NULL;
}



//===================================================
// Miscellaneous creation/search functions
//===================================================

netlist_comp_t* Netlist_Comp_GetChild(netlist_comp_t* comp, const char* name) {
	netlist_comp_t* child = NULL;
	avl_pp_find_data(&comp->children, name, (void**)&child);
	return child;
}
void Netlist_Comp_SetChild(netlist_comp_t* comp, netlist_comp_t* child) {
	avl_pp_add_overwrite(&comp->children, child->name, child);
	child->father = comp;
}
void Netlist_Comp_UnlinkChild(netlist_comp_t* comp) {
	avl_pp_rem_key(&comp->father->children, comp->name);
	comp->father = NULL;
}

netlist_port_t* Netlist_Comp_GetPort(netlist_comp_t* comp, const char* name) {
	netlist_port_t* port;
	bool b = avl_pp_find_data(&comp->ports, name, (void**)&port);
	if(b==true) return port;
	return NULL;
}

netlist_port_t* Netlist_Comp_AddPort(netlist_comp_t* comp, char* name, unsigned width) {
	netlist_port_t* port = Netlist_Port_New();
	port->component = comp;
	port->name = name;
	port->width = width;
	avl_pp_add_overwrite(&comp->ports, port->name, port);
	return port;
}
netlist_port_t* Netlist_Comp_AddPort_In(netlist_comp_t* comp, char* name, unsigned width) {
	netlist_port_t* port = Netlist_Comp_AddPort(comp, name, width);
	port->direction = NETLIST_PORT_DIR_IN;
	return port;
}
netlist_port_t* Netlist_Comp_AddPort_Out(netlist_comp_t* comp, char* name, unsigned width) {
	netlist_port_t* port = Netlist_Comp_AddPort(comp, name, width);
	port->direction = NETLIST_PORT_DIR_OUT;
	return port;
}

netlist_access_t* Netlist_Comp_GetAccess(netlist_comp_t* comp, const char* name) {
	netlist_access_t* access;
	bool b = avl_pp_find_data(&comp->accesses, name, (void**)&access);
	if(b==true) return access;
	return NULL;
}
netlist_access_t* Netlist_Comp_AddAccess(netlist_comp_t* comp, netlist_access_model_t* model, char* name) {
	assert(model!=NULL);  // Ensure the global pointer is set
	assert(Netlist_AccessModel_Get(model->name)==model);  // Ensure the model is properly declared
	netlist_access_t* access = Netlist_Access_New(model);
	access->component = comp;
	access->name = name;
	avl_pp_add_overwrite(&comp->accesses, name, access);
	return access;
}

bool Netlist_Comp_PortAcc_IsNameAvail(netlist_comp_t* comp, const char* name, int flags, avl_p_tree_t* tree_avoid_names) {
	if( (flags & NETLIST_COMP_CHKNAME_NOPORT) != 0 ) {
		if(Netlist_Comp_GetPort(comp, name)!=NULL) return false;
	}
	if( (flags & NETLIST_COMP_CHKNAME_NOACC) != 0 ) {
		if(Netlist_Comp_GetAccess(comp, name)!=NULL) return false;
	}
	if(tree_avoid_names!=NULL) {
		if(avl_p_isthere(tree_avoid_names, name)==true) return false;
	}
	return true;
}

char* Netlist_Comp_PortAcc_MakeName(netlist_comp_t* comp, char* prefix, int flags, avl_p_tree_t* tree_avoid_names) {
	if(Netlist_Comp_PortAcc_IsNameAvail(comp, prefix, flags, tree_avoid_names)==true) return prefix;

	unsigned len = strlen(prefix);
	unsigned buflen = len + 15;
	char buf[buflen];
	char* name = NULL;

	strcpy(buf, prefix);

	for(unsigned idx = 0; ; idx++) {
		sprintf(buf+len, "%u", idx);
		name = namealloc(buf);
		if(Netlist_Comp_PortAcc_IsNameAvail(comp, name, flags, tree_avoid_names)==true) break;
	}

	return name;
}

// Warning: assume the new name does not exist yet
void Netlist_Port_Rename(netlist_port_t* port, char* name) {
	if(port->name==name) return;
	if(port->component!=NULL) {
		assert(avl_pp_isthere(&port->component->ports, name)==false);
		avl_pp_rem_key(&port->component->ports, port->name);
		avl_pp_add_overwrite(&port->component->ports, name, port);
	}
	port->name = name;
}

bool Netlist_Comp_HasConnections(netlist_comp_t* comp) {
	avl_pp_foreach(&comp->ports, scan) {
		netlist_port_t* port = scan->data;
		if(port->direction==NETLIST_PORT_DIR_IN) {
			if(port->source!=NULL) return true;
		}
		else {
			if(Netlist_PortTargets_CheckEmpty(port)==false) return true;
		}
	}
	return false;
}



//===================================================
// 'ValCat' lists
//===================================================

unsigned Netlist_ValCat_ListWidth(chain_list* list) {
	unsigned width = 0;
	foreach(list, scan) {
		netlist_val_cat_t* valcat = scan->DATA;
		width += valcat->width;
	}
	return width;
}

netlist_val_cat_t* Netlist_ValCat_Dup(netlist_val_cat_t* src) {
	netlist_val_cat_t* dest = Netlist_ValCat_New();
	*dest = *src;
	return dest;
}
chain_list* Netlist_ValCat_DupList(chain_list* src) {
	chain_list* dest = NULL;
	foreach(src, scan) dest = addchain(dest, Netlist_ValCat_Dup(scan->DATA));
	return reverse(dest);
}
bool Netlist_ValCat_Eq(netlist_val_cat_t* valcat0, netlist_val_cat_t* valcat1) {
	if(valcat0->literal!=valcat1->literal) return false;
	if(valcat0->source!=valcat1->source) return false;
	if(valcat0->width!=valcat1->width) return false;
	if(valcat0->left!=valcat1->left) return false;
	if(valcat0->right!=valcat1->right) return false;
	return true;
}
bool Netlist_ValCat_EqList(chain_list* list0, chain_list* list1) {
	while(list0!=NULL && list1!=NULL) {
		bool b = Netlist_ValCat_Eq(list0->DATA, list1->DATA);
		if(b==false) return false;
		list0 = list0->NEXT;
		list1 = list1->NEXT;
	}
	if(list0!=NULL || list1!=NULL) return false;
	return true;
}

netlist_val_cat_t* Netlist_ValCat_Make_Port(netlist_port_t* port, unsigned left, unsigned right) {
	netlist_val_cat_t* valcat = Netlist_ValCat_New();
	valcat->source = port;
	valcat->left = left;
	valcat->right = right;
	valcat->width = left - right + 1;
	return valcat;
}
netlist_val_cat_t* Netlist_ValCat_Make_FullPort(netlist_port_t* port) {
	netlist_val_cat_t* valcat = Netlist_ValCat_New();
	valcat->source = port;
	valcat->width = port->width;
	valcat->left = port->width - 1;
	valcat->right = 0;
	return valcat;
}
chain_list*        Netlist_ValCat_MakeList_FullPort(netlist_port_t* port) {
	netlist_val_cat_t* valcat = Netlist_ValCat_Make_FullPort(port);
	return addchain(NULL, valcat);
}
netlist_val_cat_t* Netlist_ValCat_Make_Literal(char* value) {
	netlist_val_cat_t* valcat = Netlist_ValCat_New();
	valcat->literal = value;
	valcat->width = strlen(value);
	valcat->left = valcat->width - 1;
	valcat->right = 0;
	return valcat;
}
chain_list*        Netlist_ValCat_MakeList_Literal(char* value) {
	netlist_val_cat_t* valcat = Netlist_ValCat_Make_Literal(value);
	return addchain(NULL, valcat);
}
void               Netlist_ValCat_Set_RepeatBit(netlist_val_cat_t* valcat, char bit, unsigned nb) {
	valcat->source = NULL;
	valcat->literal = stralloc_repeat(bit, 1);
	valcat->width = nb;
	valcat->left = 0;
	valcat->right = 0;
}
netlist_val_cat_t* Netlist_ValCat_Make_RepeatBit(char bit, unsigned nb) {
	netlist_val_cat_t* valcat = Netlist_ValCat_New();
	Netlist_ValCat_Set_RepeatBit(valcat, bit, nb);
	return valcat;
}
chain_list*        Netlist_ValCat_MakeList_RepeatBit(char bit, unsigned nb) {
	netlist_val_cat_t* valcat = Netlist_ValCat_Make_RepeatBit(bit, nb);
	return addchain(NULL, valcat);
}

bool Netlist_Literal_IsAllBit(const char* lit, char bit) {
	for(unsigned i=0; ; i++) { char c = lit[i]; if(c==0) break; if(c!=bit) return false; }
	return true;
}
bool Netlist_Literal_IsNoBit(const char* lit, char bit) {
	for(unsigned i=0; ; i++) { char c = lit[i]; if(c==0) break; if(c==bit) return false; }
	return true;
}

bool Netlist_ValCat_ListIsAllBit(const chain_list* list, char bit) {
	foreach(list, scan) {
		bool b = Netlist_ValCat_IsAllBit(scan->DATA, bit);
		if(b==false) return false;
	}
	return true;
}
bool Netlist_ValCat_ListIsLitNoBit(const chain_list* list, char bit) {
	foreach(list, scan) {
		bool b = Netlist_ValCat_IsLitNoBit(scan->DATA, bit);
		if(b==false) return false;
	}
	return true;
}

// If bit=0 it means sign extension
chain_list* Netlist_ValCat_AddBitsLeft(chain_list* valcat, char bit, unsigned bits_nb) {
	if(bits_nb==0) return valcat;

	assert(bits_nb<1000000);  // Only to track bugs

	netlist_val_cat_t* valcat_first = valcat->DATA;

	// Add bits to a more-than-one-bit literal
	if(valcat_first->literal!=NULL && valcat_first->left > valcat_first->right) {
		char buffer[valcat_first->width + bits_nb + 2];
		char pad_bit = bit==0 ? valcat_first->literal[0] : bit;
		for(unsigned i=0; i<bits_nb; i++) buffer[i] = pad_bit;
		for(unsigned i=0; i<valcat_first->width; i++) buffer[bits_nb + i] = valcat_first->literal[i];
		buffer[bits_nb + valcat_first->width] = 0;
		valcat_first->literal = namealloc(buffer);
		valcat_first->width += bits_nb;
		valcat_first->left += bits_nb;
	}
	// Extend an element that has a 1-bit source
	else if(valcat_first->left==valcat_first->right && (
			bit==0 ||  // Sign extension: no additional condition
			(valcat_first->literal!=NULL && valcat_first->literal[0]==bit)  // If literal, must be the right bit
		)
	) {
		valcat_first->width += bits_nb;
	}
	// Append a new element: one repeated bit
	else {
		netlist_val_cat_t* valcat_new = Netlist_ValCat_New();
		if(bit==0 && valcat_first->source!=NULL) {
			valcat_new->source = valcat_first->source;
			valcat_new->left = valcat_first->left;
			valcat_new->right = valcat_first->left;
			valcat_new->width = bits_nb;
		}
		else {
			char buffer[2] = { bit==0 ? valcat_first->literal[0] : bit, 0 };
			valcat_new->literal = namealloc(buffer);
			valcat_new->left = 0;
			valcat_new->right = 0;
			valcat_new->width = bits_nb;
		}
		valcat = addchain(valcat, valcat_new);
	}

	return valcat;
}

chain_list* Netlist_ValCat_Crop(chain_list* list, unsigned left, unsigned right) {
	#if 0  // For debug
	printf("DEBUG %s:%u : left %u right %u : ", __FILE__, __LINE__, left, right);
	Netlist_ValCat_DebugPrintList(list);
	assert(Netlist_ValCat_ListWidth(list) > left + right);
	#endif

	// Crop left
	while(left>0) {
		netlist_val_cat_t* valcat = list->DATA;
		if(valcat->width <= left) {
			left -= valcat->width;
			// Remove the element
			chain_list* tmp_list = list;
			list = list->NEXT;
			tmp_list->NEXT = NULL;
			Netlist_ValCat_FreeList(tmp_list);
		}
		else {
			// Reduce the element width
			if(valcat->left > valcat->right) {
				if(valcat->literal!=NULL) {
					valcat->literal = namealloc(valcat->literal + left);
					valcat->left -= left;
				}
				else valcat->left -= left;
			}
			valcat->width -= left;
			left = 0;
		}
	}

	if(right==0) return list;

	// Crop right
	list = reverse(list);
	while(right>0) {
		netlist_val_cat_t* valcat = list->DATA;
		if(valcat->width <= right) {
			right -= valcat->width;
			// Remove the element
			chain_list* tmp_list = list;
			list = list->NEXT;
			tmp_list->NEXT = NULL;
			Netlist_ValCat_FreeList(tmp_list);
		}
		else {
			// Reduce the element width
			if(valcat->left > valcat->right) {
				if(valcat->literal!=NULL) {
					char buffer[valcat->width+1];
					strcpy(buffer, valcat->literal);
					buffer[ valcat->width - right ] = 0;
					valcat->literal = namealloc(buffer);
					valcat->left -= right;
				}
				else valcat->right += right;
			}
			valcat->width -= right;
			right = 0;
		}
	}

	return reverse(list);
}
// Warning: The ValCat structure must be a reference to a source port
// Warning: The replacement ValCat list must have same width than the source port
chain_list* Netlist_ValCat_CropForSource(netlist_val_cat_t* valcat, chain_list* list) {
	assert(valcat->source!=NULL);
	// Crop the new list to get the part that is read
	list = Netlist_ValCat_Crop(list, valcat->source->width - valcat->left - 1, valcat->right);
	// Handle when the replaced ValCat is a repeated bit
	if(valcat->left==valcat->right) {
		netlist_val_cat_t* new_valcat = list->DATA;
		assert(new_valcat->width==1 && new_valcat->left==new_valcat->right);
		new_valcat->width = valcat->width;
	}
	else {
		assert(valcat->width==valcat->left-valcat->right+1);
	}
	return list;
}

chain_list* Netlist_ValCat_GetLeftBit(chain_list* valcat) {
	if(valcat==NULL) return NULL;

	netlist_val_cat_t* first_elt = valcat->DATA;
	if(first_elt->literal!=NULL) {
		return Netlist_ValCat_MakeList_RepeatBit(first_elt->literal[0], 1);
	}

	chain_list* newlist = Netlist_ValCat_MakeList_FullPort(first_elt->source);
	netlist_val_cat_t* valcat_elt = newlist->DATA;
	valcat_elt->width = 1;
	valcat_elt->right = valcat_elt->left;

	return newlist;
}
char Netlist_ValCat_GetLeftBit_Char(chain_list* valcat) {
	if(valcat==NULL) return 0;
	netlist_val_cat_t* first_elt = valcat->DATA;
	if(first_elt->literal!=NULL) return first_elt->literal[0];
	return 0;
}

chain_list* Netlist_ValCat_Simplify(chain_list* list) {
	// The first element is kept
	chain_list* newlist = list;

	foreach(list, curlist) {
		netlist_val_cat_t* cur_valcat = curlist->DATA;

		// Case: several literals
		// Concatenate in a new, bigger literal. Don't check merging possibilities.
		if(cur_valcat->literal!=NULL) {
			unsigned new_width = cur_valcat->width;
			unsigned valcat_nb = 0;
			foreach(curlist->NEXT, scan) {
				netlist_val_cat_t* valcat = scan->DATA;
				if(valcat->literal==NULL) break;
				new_width += valcat->width;
				valcat_nb++;
			}
			if(valcat_nb > 0) {
				char buffer[new_width+1];
				unsigned buffer_bits_nb = 0;
				chain_list* dellist_last = NULL;
				foreach(curlist, scan) {
					netlist_val_cat_t* valcat = scan->DATA;
					if(valcat->literal==NULL) break;
					if(valcat->width>1 && valcat->left==valcat->right) {
						for(unsigned i=0; i<valcat->width; i++) buffer[buffer_bits_nb++] = valcat->literal[0];
					}
					else {
						for(unsigned i=0; i<valcat->width; i++) buffer[buffer_bits_nb++] = valcat->literal[i];
					}
					dellist_last = scan;
				}
				// Free the merged elements
				chain_list* dellist_first = curlist->NEXT;
				curlist->NEXT = dellist_last->NEXT;
				dellist_last->NEXT = NULL;
				Netlist_ValCat_FreeList(dellist_first);
				// Update the current element
				buffer[buffer_bits_nb] = 0;
				cur_valcat->literal = namealloc(buffer);
				cur_valcat->width = buffer_bits_nb;
				cur_valcat->left = buffer_bits_nb-1;
			}
		}

		// Case: a source port
		else {
			// Case: merge repeated elements
			if(cur_valcat->left==cur_valcat->right) {
				unsigned new_width = cur_valcat->width;
				unsigned valcat_nb = 0;
				chain_list* dellist_last = NULL;
				foreach(curlist->NEXT, scan) {
					netlist_val_cat_t* valcat = scan->DATA;
					if(valcat->source!=cur_valcat->source) break;
					if(valcat->left!=valcat->right) break;
					if(valcat->right!=cur_valcat->right) break;
					// Here the element is right
					new_width += valcat->width;
					dellist_last = scan;
					valcat_nb++;
				}
				if(valcat_nb > 0) {
					// Free the merged elements
					chain_list* dellist_first = curlist->NEXT;
					curlist->NEXT = dellist_last->NEXT;
					dellist_last->NEXT = NULL;
					Netlist_ValCat_FreeList(dellist_first);
					// Update the current element
					cur_valcat->width = new_width;
					continue;
				}
			}
			// Case: contiguous indexes of a same source
			if(cur_valcat->width>1 && cur_valcat->left==cur_valcat->right) {
				unsigned new_right = cur_valcat->right;
				unsigned valcat_nb = 0;
				chain_list* dellist_last = NULL;
				foreach(curlist, scan) {
					netlist_val_cat_t* valcat = scan->DATA;
					if(valcat->source!=cur_valcat->source) break;
					if(valcat->width>1 && valcat->left==valcat->right) break;
					if(valcat->left != new_right-1) break;
					// Here the element is right
					new_right = valcat->right;
					dellist_last = scan;
					valcat_nb++;
				}
				if(valcat_nb > 0) {
					// Free the merged elements
					chain_list* dellist_first = curlist->NEXT;
					curlist->NEXT = dellist_last->NEXT;
					dellist_last->NEXT = NULL;
					Netlist_ValCat_FreeList(dellist_first);
					// Update the current element
					cur_valcat->right = new_right;
					cur_valcat->width = cur_valcat->left - cur_valcat->right + 1;
				}
			}  // Case not repeated
		}  // Case Source port
	}  // Loop on the input elements

	return newlist;
}
void Netlist_Comp_Simplify(netlist_comp_t* comp) {
	// Process all sub-components
	avl_pp_foreach(&comp->children, scanChild) Netlist_Comp_Simplify(scanChild->data);
	// Process the ports
	avl_pp_foreach(&comp->ports, scanPort) {
		netlist_port_t* port = scanPort->data;
		port->source = Netlist_ValCat_Simplify(port->source);
	}
}

void Netlist_ValCat_FreeList(chain_list* list) {
	foreach(list, scan) Netlist_ValCat_Free(scan->DATA);
	freechain(list);
}



//===================================================
// Lists of target ports
//===================================================

// Working with the tree of target ports in the port structure
void Netlist_PortTargets_CompUnlinkIn(netlist_comp_t* comp) {
	avl_pp_foreach(&comp->ports, scanPort) {
		netlist_port_t* port = scanPort->data;
		if(port->direction!=NETLIST_PORT_DIR_IN) continue;
		Netlist_PortTargets_RemSource(port);
	}
}
void Netlist_PortTargets_CompClear(netlist_comp_t* comp) {
	// Process all sub-components
	avl_pp_foreach(&comp->children, scanChild) Netlist_PortTargets_CompClear(scanChild->data);
	// Process the ports
	avl_pp_foreach(&comp->ports, scanPort) {
		netlist_port_t* port = scanPort->data;
		if(port->target_ports==NULL) continue;
		avl_p_reset(port->target_ports);
	}
}
void Netlist_PortTargets_CompFree(netlist_comp_t* comp) {
	// Process all sub-components
	avl_pp_foreach(&comp->children, scanChild) Netlist_PortTargets_CompFree(scanChild->data);
	// Process the ports
	avl_pp_foreach(&comp->ports, scanPort) {
		netlist_port_t* port = scanPort->data;
		if(port->target_ports==NULL) continue;
		avl_p_reset(port->target_ports);
		free(port->target_ports);
		port->target_ports = NULL;
	}
}
void Netlist_PortTargets_SetSource(netlist_port_t* port) {
	foreach(port->source, scan) {
		netlist_val_cat_t* valcat = scan->DATA;
		if(valcat->source==NULL) continue;
		netlist_port_t* port_src = valcat->source;
		if(port_src->target_ports==NULL) {
			port_src->target_ports = malloc(sizeof(*port_src->target_ports));
			avl_p_init(port_src->target_ports);
		}
		avl_p_add_overwrite(port_src->target_ports, port, port);
	}
}
void Netlist_PortTargets_CompBuild(netlist_comp_t* comp) {
	// Process all sub-components
	avl_pp_foreach(&comp->children, scanChild) Netlist_PortTargets_CompBuild(scanChild->data);
	// Process the ports
	avl_pp_foreach(&comp->ports, scanPort) {
		netlist_port_t* port = scanPort->data;
		Netlist_PortTargets_SetSource(port);
	}
}
void Netlist_PortTargets_RemSource(netlist_port_t* port) {
	foreach(port->source, scan) {
		netlist_val_cat_t* valcat = scan->DATA;
		if(valcat->source==NULL) continue;
		netlist_port_t* src_port = valcat->source;
		if(src_port->target_ports==NULL) continue;
		avl_p_rem_key(src_port->target_ports, port);
	}
}

// Warning: port_new is assumed to have same width than port_out
void Netlist_PortTargets_ReplaceOutByPort(netlist_port_t* port_out, netlist_port_t* port_new) {
	if(port_out->target_ports==NULL) return;
	if(port_out==port_new) return;
	assert(port_new->width==port_out->width);
	// Update target ports
	avl_p_foreach(port_out->target_ports, scanTarget) {
		netlist_port_t* target_port = scanTarget->data;
		foreach(target_port->source, scanVC) {
			netlist_val_cat_t* valcat = scanVC->DATA;
			if(valcat->source==port_out) valcat->source = port_new;
		}
		// Update the target ports
		Netlist_PortTargets_SetSource(target_port);
	}
	// Reset the target ports of original port
	avl_p_reset(port_out->target_ports);
}
// Warning: The literal must have same width than port_out. Left bits first. Must be namealloc'd.
void Netlist_PortTargets_ReplaceOutByLit(netlist_port_t* port_out, char* lit_new) {
	if(port_out->target_ports==NULL) return;
	assert(strlen(lit_new)==port_out->width);
	// Update target ports
	avl_p_foreach(port_out->target_ports, scanTarget) {
		netlist_port_t* target_port = scanTarget->data;
		foreach(target_port->source, scanVC) {
			netlist_val_cat_t* valcat = scanVC->DATA;
			if(valcat->source!=port_out) continue;
			if( (valcat->left - valcat->right + 1) == port_out->width ) {
				// This ValCat uses the entire string
				valcat->literal = lit_new;
			}
			else if(valcat->left==valcat->right) {
				char buf[2];
				buf[0] = lit_new[(port_out->width - 1) - valcat->left];
				buf[1] = 0;
				valcat->literal = namealloc(buf);
				valcat->right = 0;
				valcat->left = 0;
			}
			else {
				char buf[valcat->width+1];
				memcpy(buf, lit_new + (port_out->width - 1) - valcat->left, valcat->width);
				buf[valcat->width] = 0;
				valcat->literal = namealloc(buf);
				valcat->right = 0;
				valcat->left = valcat->width - 1;
			}
			valcat->source = NULL;
		}
	}
	// Reset the target ports of original port
	avl_p_reset(port_out->target_ports);
}
// Warning: The ValCat elment MUST have width = 1
void Netlist_PortTargets_ReplaceOutByValcat_Repeat1Bit(netlist_port_t* port_out, netlist_val_cat_t* bit_valcat) {
	if(port_out->target_ports==NULL) return;
	// Work on a copy of the list of target ports
	chain_list* list_targets = NULL;
	avl_p_foreach(port_out->target_ports, scanTarget) list_targets = addchain(list_targets, scanTarget->data);
	// Reset the target ports of original port
	avl_p_reset(port_out->target_ports);
	// Update the target ports
	foreach(list_targets, scanTarget) {
		netlist_port_t* target_port = scanTarget->DATA;
		foreach(target_port->source, scanVC) {
			netlist_val_cat_t* valcat = scanVC->DATA;
			if(valcat->source!=port_out) continue;
			valcat->literal = bit_valcat->literal;
			valcat->source = bit_valcat->source;
			valcat->left = bit_valcat->left;
			valcat->right = bit_valcat->right;
		}
		// Update the target ports
		Netlist_PortTargets_SetSource(target_port);
	}
	// Clean
	freechain(list_targets);
}
void Netlist_PortTargets_ReplaceOutByLit_Repeat1Bit(netlist_port_t* port_out, char c) {
	if(port_out->target_ports==NULL) return;
	netlist_val_cat_t* bit_valcat = Netlist_ValCat_Make_RepeatBit(c, port_out->width);
	Netlist_PortTargets_ReplaceOutByValcat_Repeat1Bit(port_out, bit_valcat);
	Netlist_ValCat_Free(bit_valcat);
}
// Warning: the ValCat list is assumed to have same width than port_out
void Netlist_PortTargets_ReplaceOutByValcat_List(netlist_port_t* port_out, chain_list* list) {
	if(port_out->target_ports==NULL) return;
	assert(Netlist_ValCat_ListWidth(list)==port_out->width);

	// Work on a copy of the list of target ports
	chain_list* list_targets = NULL;
	avl_p_foreach(port_out->target_ports, scanTarget) list_targets = addchain(list_targets, scanTarget->data);
	// Reset the target ports of original port
	avl_p_reset(port_out->target_ports);

	// Update the target ports
	foreach(list_targets, scanTarget) {
		netlist_port_t* target_port = scanTarget->DATA;
		// Variables to build the new source
		chain_list* new_source = NULL;
		chain_list* new_source_last = NULL;

		// A local utility function
		void enqueue_list(chain_list* loc_list) {
			if(new_source==NULL) new_source = loc_list;
			else new_source_last->NEXT = loc_list;
			new_source_last = ChainList_GetLastNode(loc_list);
		}
		void enqueue_valcat(netlist_val_cat_t* valcat) {
			enqueue_list(addchain(NULL, valcat));
		}

		// Scan the ValCat elements and replace what needs to
		bool replaced = false;
		foreach(target_port->source, scanVC) {
			netlist_val_cat_t* valcat = scanVC->DATA;
			if(valcat->source!=port_out) {
				enqueue_valcat(valcat);
				continue;
			}
			// Build the new source list
			chain_list* newlist = Netlist_ValCat_DupList(list);
			newlist = Netlist_ValCat_CropForSource(valcat, newlist);
			// Free the original ValCat element
			Netlist_ValCat_Free(valcat);
			// Link the new list
			enqueue_list(newlist);
			replaced = true;
		}  // Scan all ValCat elements

		// Replace the list
		if(replaced==false) freechain(new_source);
		else {
			assert(Netlist_ValCat_ListWidth(new_source)==target_port->width);
			freechain(target_port->source);
			target_port->source = new_source;
		}

		// Add the present target port to all source ports
		Netlist_PortTargets_SetSource(target_port);
	}

	// Clean
	freechain(list_targets);
}

// Return true if the port is read at least one time
bool Netlist_PortTargets_GetMaxIndexes(netlist_port_t* port, unsigned* max_left, unsigned* min_right) {
	if(port->target_ports==NULL) return false;
	bool found = false;
	avl_p_foreach(port->target_ports, scanTarget) {
		netlist_port_t* target_port = scanTarget->data;
		foreach(target_port->source, scanValCat) {
			netlist_val_cat_t* valcat = scanValCat->DATA;
			if(valcat->source!=port) continue;
			if(found==false) {
				*min_right = valcat->right;
				*max_left = valcat->left;
				found = true;
			}
			else {
				*min_right = GetMin(valcat->right, *min_right);
				*max_left = GetMax(valcat->left, *max_left);
			}
		}  // Scan ValCat elements
	}  // Scan target ports
	return found;
}



//===================================================
// Debug
//===================================================

void Netlist_ValCat_DebugPrintList(chain_list const * list) {
	foreach(list, scan) {
		netlist_val_cat_t* valcat = scan->DATA;
		printf("(%p) Width = %d", valcat, valcat->width);
		if(valcat->literal!=NULL) {
			printf(" Literal = ");
			if(valcat->width==1) printf("'%c'", valcat->literal[0]);
			else printf("\"%s\"", valcat->literal);
		}
		if(valcat->source!=NULL) {
			printf(" Source = L=%u R=%u (%p) port '%s' comp '%s' model '%s'",
				valcat->left, valcat->right,
				valcat->source, valcat->source->name,
				valcat->source->component->name, valcat->source->component->model->name
			);
		}
		printf("\n");
	}
}

void Netlist_PortTargets_DebugPrint(const netlist_port_t* port) {
	if(port->target_ports==NULL) {
		printf("No destination ports (null tree)\n");
		return;
	}
	if(avl_p_isempty(port->target_ports)==true) {
		printf("No destination ports (empty tree)\n");
		return;
	}
	unsigned count = 0;
	avl_p_foreach(port->target_ports, scan) {
		netlist_port_t* port_target = scan->data;
		printf("(%p) port '%s' width %u of (%p) comp '%s' model (%p) '%s'\n",
			port_target, port_target->name, port_target->width,
			port_target->component, port_target->component->name,
			port_target->component->model, port_target->component->model->name
		);
		count++;
	}
	printf("Total: %u destination ports\n", count);
}

void Netlist_Comp_DebugPrintPorts(const netlist_comp_t* comp) {
	unsigned count_acc = 0;
	avl_pp_foreach(&((netlist_comp_t*)comp)->accesses, scan) {
		netlist_access_t* access = scan->data;
		printf("(%p) access '%s' model %s\n", access, access->name, access->model->name);
		count_acc++;
	}
	printf("Total: %u accesses\n", count_acc);
	unsigned count_ports = 0;
	avl_pp_foreach(&((netlist_comp_t*)comp)->ports, scan) {
		netlist_port_t* port = scan->data;
		printf("(%p) %s port '%s' width %u", port, port->direction==NETLIST_PORT_DIR_IN ? "input" : "output", port->name, port->width);
		if(port->access!=NULL) {
			netlist_access_t* access = port->access;
			printf(" -> id '%s' in (%p) access '%s' model '%s'", Netlist_Access_Port_GetName(port), access, access->name, access->model->name);
		}
		printf("\n");
		count_ports++;
	}
	printf("Total: %u ports\n", count_ports);
}

static unsigned Netlist_Debug_CheckIntegrity_recurs(const netlist_comp_t* top) {
	int errors_nb = 0;

	void print_comp(const netlist_comp_t* comp, char* beg, char* end) {
		if(beg!=NULL) printf("%s", beg);
		printf("%p '%s' model '%s'", comp, comp->name, comp->model->name);
		if(end!=NULL) printf("%s", end);
	}
	void print_portcomp(const netlist_port_t* port, const netlist_comp_t* comp, char* beg, char* end) {
		if(beg!=NULL) printf("%s", beg);
		printf("%p '%s' of comp ", port, port->name);
		print_comp(comp, NULL, NULL);
		if(end!=NULL) printf("%s", end);
	}

	void check_targetports(const netlist_port_t* port, const netlist_comp_t* comp, char* strdir) {
		if(port->target_ports==NULL) return;
		avl_p_foreach(port->target_ports, scanTarget) {
			netlist_port_t* targetport = scanTarget->data;
			if(targetport->component==top) continue;
			if(targetport->component->father==NULL) {
				printf("ERROR: %s port ", strdir);
				print_portcomp(port, comp, NULL, NULL);
				print_portcomp(targetport, targetport->component, ": one target is port ", ", which has no father comp.\n");
				errors_nb ++;
				continue;
			}
			if(targetport->component->father!=top) {
				printf("ERROR: %s port ", strdir);
				print_portcomp(port, comp, NULL, NULL);
				print_portcomp(targetport, targetport->component, ": one target is port ", NULL);
				print_comp(targetport->component->father, ", which has father comp ", "\n");
				errors_nb ++;
				continue;
			}
		}  // Scan targets
	}
	void check_sourceports(const netlist_port_t* port, const netlist_comp_t* comp, char* strdir) {
		if(port->source==NULL) return;
		// Check the width of the full ValCat list
		unsigned valcat_width = Netlist_ValCat_ListWidth(port->source);
		if(valcat_width!=port->width) {
			printf("ERROR: %s port ", strdir);
			print_portcomp(port, comp, NULL, NULL);
			printf(": width is %u, source is %u.\n", port->width, valcat_width);
			errors_nb++;
		}
		// Check each ValCat element
		foreach(((netlist_port_t*)port)->source, scan) {
			netlist_val_cat_t* valcat = scan->DATA;
			if(valcat->left < valcat->right) {
				printf("ERROR: %s port ", strdir);
				print_portcomp(port, comp, NULL, NULL);
				printf(": a ValCat element %p has left %u and right %u.\n", valcat, valcat->left, valcat->right);
				errors_nb++;
			}
			if(valcat->left!=valcat->right && valcat->width != valcat->left - valcat->right + 1) {
				printf("ERROR: %s port ", strdir);
				print_portcomp(port, comp, NULL, NULL);
				printf(": a ValCat element %p has width %u, left %u, right %u: mismatch.\n",
					valcat, valcat->width, valcat->left, valcat->right
				);
				errors_nb++;
			}
			// Case where a bit is repeated
			if(valcat->literal!=NULL && valcat->source!=NULL) {
				printf("ERROR: %s port ", strdir);
				print_portcomp(port, comp, NULL, NULL);
				printf(": a ValCat element %p has both literal and source.\n", valcat);
				errors_nb++;
			}
			if(valcat->literal!=NULL) {
				if(strlen(valcat->literal) != valcat->left - valcat->right + 1) {
					printf("ERROR: %s port ", strdir);
					print_portcomp(port, comp, NULL, NULL);
					printf(": a ValCat element %p is literal, left is %u, right is %u but string width is %zu.\n",
						valcat, valcat->left, valcat->right, strlen(valcat->literal)
					);
					errors_nb++;
				}
			}
			if(valcat->source!=NULL) {
				if(valcat->left >= valcat->source->width) {
					printf("ERROR: %s port ", strdir);
					print_portcomp(port, comp, NULL, NULL);
					printf(": a ValCat element %p has source, field left is %u but port width is %u.\n",
						valcat, valcat->left, valcat->source->width
					);
					errors_nb++;
				}
			}
		}  // Scan the ValCat elements
		// Check the hierarchy of the source ports
		foreach(((netlist_port_t*)port)->source, scanSource) {
			netlist_val_cat_t* valcat = scanSource->DATA;
			if(valcat->literal!=NULL) continue;
			if(valcat->source==NULL) {
				printf("ERROR: %s port ", strdir);
				print_portcomp(port, comp, NULL, NULL);
				printf(": one source ValCat %p has no literal nor source port.\n", valcat);
				errors_nb++;
				continue;
			}
			netlist_port_t* sourceport = valcat->source;
			if(sourceport->component==top) continue;
			if(sourceport->component->father==NULL) {
				printf("ERROR: %s port ", strdir);
				print_portcomp(port, comp, NULL, NULL);
				print_portcomp(sourceport, sourceport->component, ": one source is port ", ", which has no father comp.\n");
				errors_nb ++;
				continue;
			}
			if(sourceport->component->father!=top) {
				printf("ERROR: %s port ", strdir);
				print_portcomp(port, comp, NULL, NULL);
				print_portcomp(sourceport, sourceport->component, ": one source is port ", NULL);
				print_comp(sourceport->component->father, ", which has father comp ", ".\n");
				errors_nb ++;
				continue;
			}
		}  // Scan sources
	}

	// Check connections of top-level ports
	avl_pp_foreach(&((netlist_comp_t*)top)->ports, scanPort) {
		netlist_port_t* port = scanPort->data;
		// Check port name
		if(scanPort->key!=port->name) {
			print_portcomp(port, top, "ERROR: Port ", NULL);
			printf(": indexed in father comp as '%s'.\n", (char*)scanPort->key);
			errors_nb ++;
		}
		// Check the field 'component'
		if(port->component!=top) {
			print_portcomp(port, top, "ERROR: Port ", NULL);
			print_comp(port->component, ": field component is ", ".\n");
			errors_nb ++;
		}
		// Check port connections (inside only)
		if(port->direction==NETLIST_PORT_DIR_IN) {
			check_targetports(port, top, "Input");
		}
		else {
			check_sourceports(port, top, "Output");
		}
	}  // Scan top-level ports

	// Scan child components, check father comp
	avl_pp_foreach(&((netlist_comp_t*)top)->children, scanChild) {
		netlist_comp_t* comp = scanChild->data;
		// Check comp name
		if(scanChild->key!=comp->name) {
			printf("ERROR: Comp %p '%s' model '%s', child of comp %p '%s' model '%s': indexed in father comp as '%s'.\n",
				comp, comp->name, comp->model->name, top, top->name, top->model->name, (char*)scanChild->key
			);
			errors_nb ++;
		}
		// Check the father field
		if(comp->father!=top) {
			printf("ERROR: Comp %p '%s' model '%s', child of comp %p '%s' model '%s': Wrong father comp %p '%s' model '%s'.\n",
				comp, comp->name, comp->model->name, top, top->name, top->model->name,
				comp->father, comp->father->name, comp->father->model->name
			);
			errors_nb ++;
		}
		// Check connections of the ports of the component
		avl_pp_foreach(&((netlist_comp_t*)comp)->ports, scanPort) {
			netlist_port_t* port = scanPort->data;
			// Check port connections (inside only)
			if(port->direction==NETLIST_PORT_DIR_IN) {
				check_sourceports(port, comp, "Input");
			}
			else {
				check_targetports(port, comp, "Output");
			}
		}  // Scan ports
	}  // Scan child components

	// Recursively launch the check on the child components
	avl_pp_foreach(&((netlist_comp_t*)top)->children, scanChild) {
		netlist_comp_t* comp = scanChild->data;
		errors_nb += Netlist_Debug_CheckIntegrity_recurs(comp);
	}

	return errors_nb;
}

void Netlist_Debug_CheckIntegrity(const netlist_comp_t* top) {
	int errors_nb = Netlist_Debug_CheckIntegrity_recurs(top);
	if(errors_nb != 0) {
		errprintf("ERROR: %u errors were found when checking Netlist integrity from comp %p '%s' model '%s'.\n",
			errors_nb, top, top->name, top->model->name
		);
		abort();
	}
}



//===================================================
// Duplication of components
//===================================================

void Netlist_Access_Dup_LinkPorts(void* new_data, void* old_data, netlist_access_model_t* model, avl_pp_tree_t* tree_old2new) {
	avl_ip_foreach(&model->ports_offset2name, scan) {
		unsigned off = scan->key;
		void* oldptr = *((void**)(new_data + off));
		void* newptr = NULL;
		avl_pp_find_data(tree_old2new, oldptr, &newptr);
		*((void**)(new_data + off)) = newptr;
		assert( (oldptr==NULL) == (newptr==NULL) );
	}
}

netlist_access_t* Netlist_Access_Dup(netlist_access_t* access, netlist_comp_t* comp_dest, char* newname) {
	if(newname==NULL) newname = access->name;
	assert(Netlist_Comp_GetAccess(comp_dest, newname)==NULL);

	avl_pp_tree_t tree_old2new;
	avl_pp_init(&tree_old2new);

	netlist_comp_t* comp_src = access->component;

	// Utility macro
	#define pointer_add(oldptr, newptr) do { avl_pp_add_overwrite(&tree_old2new, oldptr, newptr); } while(0)

	// Duplicate the access structure
	netlist_access_t* newaccess = Netlist_Comp_AddAccess(comp_dest, access->model, newname);

	// Duplicate the ports
	avl_pp_foreach(&comp_src->ports, scanPort) {
		netlist_port_t* port = scanPort->data;
		if(port->access!=access) continue;
		netlist_port_t* newport = Netlist_Comp_AddPort(comp_dest, port->name, port->width);
		pointer_add(port, newport);
		newport->direction = port->direction;
		newport->access = newaccess;
	}

	#undef pointer_add

	// Duplicate internal data of access structure
	if(access->model->func_dup_internal!=NULL) {
		access->model->func_dup_internal(&tree_old2new, access, newaccess);
	}
	else if(access->model->data_size > 0) {
		// Copy all data
		memcpy(newaccess->data, access->data, access->model->data_size);
		// Update pointers to ports
		Netlist_Access_Dup_LinkPorts(newaccess->data, access->data, access->model, &tree_old2new);
	}

	avl_pp_reset(&tree_old2new);

	return newaccess;
}

// Info: the tree MUST be non-null and ready to use. Data is only added to it.
netlist_comp_t* Netlist_Comp_Dup_Lnk_flag(avl_pp_tree_t* tree_old2new, netlist_comp_t* comp, bool do_internals) {

	// Utility functions
	void pointer_add(void* oldptr, void* newptr) {
		avl_pp_add_overwrite(tree_old2new, oldptr, newptr);
	}
	void* pointer_old2new(void* oldptr) {
		void* newptr = NULL;
		avl_pp_find_data(tree_old2new, oldptr, &newptr);
		return newptr;
	}

	// Create a new component
	netlist_comp_t* newcomp = Netlist_Comp_New(comp->model);
	newcomp->name = comp->name;
	pointer_add(comp, newcomp);

	// Recursivity: duplicate the child components
	if(do_internals==true) {
		avl_pp_foreach(&comp->children, scanChild) {
			netlist_comp_t* child = scanChild->data;
			netlist_comp_t* newchild = Netlist_Comp_Dup_Lnk(tree_old2new, child);
			Netlist_Comp_SetChild(newcomp, newchild);
			pointer_add(child, newchild);
		}
	}

	// Duplicate the ports
	avl_pp_foreach(&comp->ports, scanPort) {
		netlist_port_t* port = scanPort->data;
		netlist_port_t* newport = Netlist_Comp_AddPort(newcomp, port->name, port->width);
		newport->direction = port->direction;
		pointer_add(port, newport);
	}

	// Duplicate the access structures
	avl_pp_foreach(&comp->accesses, scanAccess) {
		netlist_access_t* access = scanAccess->data;
		netlist_access_t* newaccess = Netlist_Comp_AddAccess(newcomp, access->model, access->name);
		pointer_add(access, newaccess);
	}

	// Link the ports to the access structures
	avl_pp_foreach(&comp->ports, scanPort) {
		netlist_port_t* port = scanPort->data;
		if(port->access==NULL) continue;
		netlist_port_t* newport = pointer_old2new(port);
		newport->access = pointer_old2new(port->access);
	}

	// Duplicate internal data of access structures
	avl_pp_foreach(&comp->accesses, scanAccess) {
		netlist_access_t* access = scanAccess->data;
		netlist_access_t* newaccess = pointer_old2new(access);
		if(access->model->func_dup_internal!=NULL) {
			access->model->func_dup_internal(tree_old2new, access, newaccess);
		}
		else if(access->model->data_size > 0) {
			// Copy all data
			memcpy(newaccess->data, access->data, access->model->data_size);
			// Update pointers to ports
			Netlist_Access_Dup_LinkPorts(newaccess->data, access->data, access->model, tree_old2new);
		}
	}

	// Duplicate internal data of the current component
	if(comp->model->func_dup_internal!=NULL) {
		comp->model->func_dup_internal(tree_old2new, comp, newcomp);
	}

	// Duplicate connectivity of ports
	if(do_internals==true) {
		avl_pp_foreach(&comp->children, scanChild) {
			netlist_comp_t* child = scanChild->data;
			avl_pp_foreach(&child->ports, scanPort) {
				netlist_port_t* port = scanPort->data;
				netlist_port_t* newport = pointer_old2new(port);
				if(port->source!=NULL) {
					newport->source = Netlist_ValCat_DupList(port->source);
					foreach(newport->source, scan) {
						netlist_val_cat_t* valcat = scan->DATA;
						if(valcat->source!=NULL) valcat->source = pointer_old2new(valcat->source);
					}
				}
				if(port->target_ports!=NULL) {
					newport->target_ports = malloc(sizeof(*newport->target_ports));
					avl_p_init(newport->target_ports);
					avl_p_dup(newport->target_ports, port->target_ports);
					avl_p_foreach(newport->target_ports, scanTP) scanTP->data = pointer_old2new(scanTP->data);
				}
			}
		}  // The child components
		avl_pp_foreach(&comp->ports, scanPort) {
			netlist_port_t* port = scanPort->data;
			netlist_port_t* newport = pointer_old2new(port);
			if(port->direction==NETLIST_PORT_DIR_IN) {
				if(port->target_ports!=NULL) {
					newport->target_ports = malloc(sizeof(*newport->target_ports));
					avl_p_init(newport->target_ports);
					avl_p_dup(newport->target_ports, port->target_ports);
					avl_p_foreach(newport->target_ports, scanTP) scanTP->data = pointer_old2new(scanTP->data);
				}
			}
			else {
				if(port->source!=NULL) {
					newport->source = Netlist_ValCat_DupList(port->source);
					foreach(newport->source, scan) {
						netlist_val_cat_t* valcat = scan->DATA;
						if(valcat->source!=NULL) valcat->source = pointer_old2new(valcat->source);
					}
				}
			}
		}  // The ports
	}

	return newcomp;
}
netlist_comp_t* Netlist_Comp_Dup_flag(netlist_comp_t* comp, bool do_internals) {
	avl_pp_tree_t tree_old2new;
	avl_pp_init(&tree_old2new);

	netlist_comp_t* newcomp = Netlist_Comp_Dup_Lnk_flag(&tree_old2new, comp, do_internals);

	avl_pp_reset(&tree_old2new);

	return newcomp;
}


