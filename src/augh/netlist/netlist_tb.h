
#ifndef _NETLIST_TB_H_
#define _NETLIST_TB_H_

#include <stdint.h>
#include <stdbool.h>

#include "../hvex/hvex.h"

#include "../chain.h"
#include "netlist.h"


// Declarations of constants and types

// Binary storage
#define TB_DATA_RAWBIN  1

// Text storage. Comments are handled. Separation by spaces.
#define TB_DATA_HEX     2
#define TB_DATA_DEC     3
#define TB_DATA_BIN     4
// Auto detect, in text format only :
// Hex begins by 0x, bin begins by 0b, else decimal
#define TB_DATA_AUTO    5

typedef struct tb_fifo_t {
	bool     is_the_in;
	bool     is_the_out;
	char*    channel_name;  // FIFO access name
	netlist_access_t* access;
	char*    file_name;
	unsigned style;
	// For rawbin style only
	bool     is_littleendian;
	unsigned bytes_nb;
	// Some resuts after vectors are read
	unsigned allocsize;  // The size of the allocated mem for each vector
	unsigned vectors_nb;
	unsigned vector_width;
	chain_list* list_vec;
	// Additional fields so this access can stop simulation (output fifo only)
	unsigned stop_nb;  // Stop simulation when a certain number of outputs are read
	bool     stop_end_vectors;  // Stop when out of input vectors
} tb_fifo_t;


// Utility functions

tb_fifo_t* TB_FifoData_New();
void TB_FifoData_Free(tb_fifo_t* tb_fifo);

tb_fifo_t* TB_FifoData_List_FindName(chain_list* list_fifos, const char* name);
tb_fifo_t* TB_FifoData_List_FindTheIn(chain_list* list_fifos);
tb_fifo_t* TB_FifoData_List_FindTheOut(chain_list* list_fifos);
tb_fifo_t* TB_FifoData_Tree_FindName(avl_pp_tree_t* tree_fifos, const char* name);

int Netlist_TB_ReadFifoVectors(tb_fifo_t* tb_fifo);
void Netlist_TB_DumpFifoVectors(FILE* F, tb_fifo_t* tb_fifo, char* tabs, unsigned linesize);

// Main functions

int Netlist_TB_IdentifyFifos(implem_t* Implem, chain_list* list_fifos);

int Netlist_TB_Gen(implem_t* Implem, avl_pp_tree_t* tree_fifos, const char* destdir, unsigned simu_max_cycles);


#endif  // _NETLIST_TB_H_

