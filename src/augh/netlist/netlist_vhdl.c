
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <ctype.h>

#include "../auto/techno.h"
#include "netlist.h"
#include "netlist_vhdl.h"
#include "netlist_cmd.h"



//===================================================
// VHDL Generation
//===================================================

void Netlist_GenParams_Init(netlist_vhdl_t* params) {
	memset(params, 0, sizeof(*params));
	// Reset
	params->separate = true;
	params->architecture = namealloc("augh");
	avl_p_init(&params->reserved_keywords);
	avl_p_init(&params->libs_keywords);
	avl_p_init(&params->global_ids);
	avl_pp_init(&params->comp_rename);
	// Forbid VHDL keywords
	Netlist_AddKeywords_VHDL(&params->reserved_keywords);
	// Also forbid Verilog keywords if asking to be Verilog-friendly
	if(vhd2vl_friendly==true) {
		Netlist_AddKeywords_Verilog(&params->reserved_keywords);
	}
	// Default VHDL style
	params->inline_comps = do_comps_inline;
	params->objective_simu = objective_simu;
}
void Netlist_GenParams_Clear(netlist_vhdl_t* params) {
	avl_p_reset(&params->reserved_keywords);
	avl_p_reset(&params->libs_keywords);
	avl_p_reset(&params->global_ids);
	avl_pp_foreach(&params->comp_rename, scan) freebitype(scan->data);
	avl_pp_reset(&params->comp_rename);
}

// Uutility functions to list reserved keywords
static void add_keyword(avl_p_tree_t* tree, char* keyword) {
	keyword = namealloc(keyword);
	avl_p_add_overwrite(tree, keyword, keyword);
}
static void add_keyword_fromstring(avl_p_tree_t* tree, char* string, unsigned maxlength) {
	char keyword[maxlength+1];
	do {
		// Find the beginning of the next word
		do {
			if((*string)==0) return;  // End of string reached
			if( isspace(*string)==0 ) break;  // The beginning of a word is found
			string++;
		} while(1);
		// Read the keyword
		char* ptr_kw = keyword;
		do {
			if((*string)==0) break;  // End of string reached
			if( isspace(*string)!=0 ) break;  // The end of the keyword is found
			*(ptr_kw++) = *(string++);
		} while(1);
		*ptr_kw = 0;
		// A feyword is formed, add it
		add_keyword(tree, keyword);
		// And go find the nex keyword
	}while(1);
}

// List the reserved VHDL keywords
// Website: http://www.csee.umbc.edu/portal/help/VHDL/reserved.html
void Netlist_AddKeywords_VHDL(avl_p_tree_t* tree) {

	// VHDL is case-insensitive, only save lowercase.
	// It means that component names in the netlist must be lowercase.
	add_keyword_fromstring(tree, " \
		abs access after alias all and architecture array assert attribute \
		begin block body buffer bus \
		case component configuration constant \
		disconnect downto \
		else elsif end entity exit \
		file for function \
		generate generic group guarded \
		if impure in inertial inout is \
		label library linkage literal loop \
		map mod \
		nand new next nor not null \
		of on open or others out \
		package port postponed procedure process pure \
		range record register reject rem report return rol ror \
		select severity signal shared sla sll sra srl subtype \
		then to transport type \
		unaffected units until use \
		variable \
		wait when while with \
		xnor xor \
	", 15);

}
// List the reserved Verilog keywords
// Website: http://www.csee.umbc.edu/portal/help/VHDL/verilog/reserved.html
void Netlist_AddKeywords_Verilog(avl_p_tree_t* tree) {

	add_keyword_fromstring(tree, " \
		always and assign automatic \
		begin buf bufif0 bufif1 \
		case casex casez cell cmos config \
		deassign default defparam design disable \
		edge else end endcase endconfig endfunction endgenerate endmodule endprimitive endspecify endtable endtask event \
		for force forever fork function \
		generate genvar \
		highz0 highz1 \
		if ifnone incdir include initial inout input instance integer \
		join \
		large liblist library localparam \
		macromodule medium module \
		nand negedge nmos nor noshowcancelled not notif0 notif1 \
		or output \
		parameter pmos posedge primitive pull0 pull1 pulldown pullup pulsestyle_onevent pulsestyle_ondetect \
		remos real realtime reg release repeat rnmos rpmos rtran rtranif0 rtranif1 \
		scalared showcancelled signed small specify specparam strong0 strong1 supply0 supply1 \
		table task time tran tranif0 tranif1 tri tri0 tri1 triand trior trireg \
		unsigned use \
		vectored \
		wait wand weak0 weak1 while wire wor \
		xnor xor \
	", 20);

}

/* A function that checks format of identifiers (for basic identifiers)
=> http://vhdl.renerta.com/mobile/source/vhd00037.htm
no double underscore, no underscore at the beginning nor the end
case collisions
only a letter at the beginning
collisions between component names and instantiation names
collisions between port names and internal identifiers (instantiation names, variables, signals)
FIXME the top-level component name should be checked
Note: Component port names are assumed to be already VHDL-compliant, because the netlist module created them.
*/
bitype_list* Netlist_Comp_Rename(netlist_comp_t* comp, netlist_vhdl_t* params) {
	bitype_list* comp_ren = addbitype(NULL, 0, NULL, NULL);

	// Work on local buffer
	unsigned len = strlen(comp->name);
	if(params->vhdl_prefix!=NULL && (comp->flags & NETLIST_COMP_NOPREFIX)==0) {
		len += strlen(params->vhdl_prefix) + 1;
	}
	char buf[len + 30];
	if(params->vhdl_prefix!=NULL && (comp->flags & NETLIST_COMP_NOPREFIX)==0) {
		sprintf(buf, "%s_%s", params->vhdl_prefix, comp->name);
	}
	else strcpy(buf, comp->name);
	char* newbuf = buf;

	// All lowercase
	for(unsigned i=0; i<len; i++) newbuf[i] = tolower(newbuf[i]);

	// The leading character must be a letter
	while(newbuf[0]!=0 && isalpha(newbuf[0])==0) { newbuf++; len--; }

	// Remove the leading and trailing underscores
	while(len>0 && newbuf[len-1]=='_') { newbuf[len-1] = 0; len--; }

	// Remove occurrences of multiple underscores and of forbidden characters
	unsigned newlen = 0;
	unsigned nb_u = 0;
	for(unsigned i=0; i<len; i++) {
		// Scan all characters in the name
		char c = newbuf[i];
		// Handle underscores
		if(c=='_') {
			if(nb_u > 0) continue;
			nb_u++;
			newbuf[newlen++] = c;
			continue;
		}
		else nb_u = 0;
		// Check other authorized characters
		if( isalnum(c)==0 ) continue;
		newbuf[newlen++] = c;
	}

	// Build the VHDL component name

	// Local utility functions
	bool name_check_taken(char* name) {
		if( avl_p_isthere(&params->reserved_keywords, name)==true ) return true;
		if( avl_p_isthere(&params->global_ids, name)==true ) return true;
		return false;
	}
	char* append_check_name(char* buf, unsigned len, char* suffix, bool tryraw, bool trynoidx) {
		char* newname = NULL;
		if(tryraw==true) {
			newname = namealloc(buf);
			if( name_check_taken(newname)==false ) return newname;
		}
		if(trynoidx==true) {
			sprintf(buf+len, "_%s", suffix);
			newname = namealloc(buf);
			if( name_check_taken(newname)==false ) return newname;
		}
		for(unsigned i=0; ; i++) {
			sprintf(buf+len, "_%s%u", suffix, i);
			newname = namealloc(buf);
			if( name_check_taken(newname)==false ) break;
		}
		return newname;
	}

	char* newcompname = NULL;
	char* newinstname = NULL;

	if(comp->model==NETLIST_COMP_TOP) {
		netlist_top_t* top_data = comp->data;
		if(top_data->extern_model_name!=NULL) newcompname = top_data->extern_model_name;
	}

	if(newlen==0) {
		// Nothing remained that can help identify the component...
		// Build a new name from the compponent model name
		unsigned len1 = strlen(comp->model->name);
		char buf1[len1 + 30];
		strcpy(buf1, comp->model->name);
		if(newcompname==NULL) {
			newcompname = append_check_name(buf1, len1, "c", false, false);
		}
		newinstname = append_check_name(buf1, len1, "i", false, true);
	}
	else {
		if(newcompname==NULL) {
			newcompname = append_check_name(newbuf, newlen, "c", true, false);
		}
		newinstname = append_check_name(newbuf, newlen, "i", false, true);
	}

	// Save these new names
	comp_ren->DATA_FROM = newcompname;
	comp_ren->DATA_TO = newinstname;

	return comp_ren;
}
bitype_list* Netlist_Comp_GetRename(netlist_comp_t* comp, netlist_vhdl_t* params) {
	bitype_list* comp_ren = NULL;
	avl_pp_find_data(&params->comp_rename, comp, (void**)&comp_ren);
	return comp_ren;
}

char* Netlist_Comp_GetRename_Comp(netlist_comp_t* comp, netlist_vhdl_t* params) {
	bitype_list* comp_ren = Netlist_Comp_GetRename(comp, params);
	if(comp_ren==NULL) return NULL;
	return comp_ren->DATA_FROM;
}
char* Netlist_Comp_GetRename_Inst(netlist_comp_t* comp, netlist_vhdl_t* params) {
	bitype_list* comp_ren = Netlist_Comp_GetRename(comp, params);
	if(comp_ren==NULL) return NULL;
	return comp_ren->DATA_TO;
}



void Netlist_GenVHDL_StdLibs(FILE* F) {
	fprintf(F, "library ieee;\n");
	fprintf(F, "use ieee.std_logic_1164.all;\n");
}
void Netlist_GenVHDL_MyToInteger(FILE* F) {
	fprintf(F,
		"\t-- Little utility functions to make VHDL syntactically correct\n"
		"\t--   with the syntax to_integer(unsigned(vector)) when 'vector' is a std_logic.\n"
		"\t--   This happens when accessing arrays with <= 2 cells, for example.\n"
		"\n"

		"\tfunction to_integer(B: std_logic) return integer is\n"
		"\t\tvariable V: std_logic_vector(0 downto 0);\n"
		"\tbegin\n"
		"\t\tV(0) := B;\n"
		"\t\treturn to_integer(unsigned(V));\n"
		"\tend;\n"

		"\n"

		"\tfunction to_integer(V: std_logic_vector) return integer is\n"
		"\tbegin\n"
		"\t\treturn to_integer(unsigned(V));\n"
		"\tend;\n"
	);
}
void Netlist_GenVHDL_MySignedUnsigned(FILE* F) {
	fprintf(F,
		"\t-- Little utility functions to make VHDL syntactically correct\n"
		"\t--   with the syntax signed(vector) when 'vector' is a std_logic.\n"
		"\t--   This happens when data signals are 1-bit large, for example.\n"
		"\n"

		"\tfunction to_signed(B: std_logic) return signed is\n"
		"\t\tvariable V: signed(0 downto 0);\n"
		"\tbegin\n"
		"\t\tV(0) := B;\n"
		"\t\treturn V;\n"
		"\tend;\n"
		"\tfunction to_signed(V: std_logic_vector) return signed is\n"
		"\tbegin\n"
		"\t\treturn signed(V);\n"
		"\tend;\n"

		"\n"

		"\tfunction to_unsigned(B: std_logic) return unsigned is\n"
		"\t\tvariable V: unsigned(0 downto 0);\n"
		"\tbegin\n"
		"\t\tV(0) := B;\n"
		"\t\treturn V;\n"
		"\tend;\n"
		"\tfunction to_unsigned(V: std_logic_vector) return unsigned is\n"
		"\tbegin\n"
		"\t\treturn unsigned(V);\n"
		"\tend;\n"
	);
}
void Netlist_GenVHDL_MyRepeat(FILE* F) {
	fprintf(F,
		"\t-- Little utility function to ease concatenation of an std_logic\n"
		"\t-- and explicitely return an std_logic_vector\n"
		"	function repeat(N: natural; B: std_logic) return std_logic_vector is\n"
		"		variable result: std_logic_vector(N-1 downto 0);\n"
		"	begin\n"
		"		result := (others => B);\n"
		"		return result;\n"
		"	end;\n"
	);
}

char* Netlist_GenVHDL_ClockEdge_TestFunc(int edge) {
	if(edge > 0) return "rising_edge";
	if(edge < 0) return "falling_edge";
	return NULL;
}

char* Netlist_GenVHDL_ClockEdge_TestFunc_port(netlist_port_t* port) {
	assert(port->access!=NULL);
	assert(port->access->model == NETLIST_ACCESS_CLOCK);
	netlist_access_clock_t* clock_data = port->access->data;
	char* str = Netlist_GenVHDL_ClockEdge_TestFunc(clock_data->active_edge);
	assert(str!=NULL);
	return str;
}

// Note: This function is assumed to never receive the flag CHKADDPAR
void Netlist_GenVHDL_ValCat_opt(FILE* F, netlist_comp_t* comp, netlist_val_cat_t* valcat, netlist_vhdl_t* params, int flags) {

	// Separately handle literals
	if(valcat->literal!=NULL) {
		if(valcat->width>1 && valcat->left==valcat->right) {
			Netlist_GenVHDL_StdLogic_Literal_Repeat(F, valcat->width, valcat->literal[0]);
		}
		else {
			Netlist_GenVHDL_StdLogic_Literal(F, valcat->literal);
		}
	}

	// Here it is a repeated port
	else if(valcat->width>1 && valcat->left==valcat->right) {

		// Generate the syntax repeat(width, <expr>)
		// Use a threshold so the repeat is only used for width >= 3
		if((flags & GENVHDL_VALCAT_USEREPEAT)!=0 && vhd2vl_friendly==false && valcat->width>=3) {
			fprintf(F, "repeat(%u, ", valcat->width);
			unsigned save_width = valcat->width;
			valcat->width = 1;
			Netlist_GenVHDL_ValCat_opt(F, comp, valcat, params, flags);
			valcat->width = save_width;
			fputc(')', F);
		}

		// Default: Generate a long concatenation
		else {
			if(valcat->literal!=NULL) {
				fputc('"', F);
				for(unsigned i=0; i<valcat->width; i++) fprintf(F, "%c", valcat->literal[0]);
				fputc('"', F);
			}
			else {
				unsigned save_width = valcat->width;
				valcat->width = 1;
				for(unsigned i=0; i<save_width; i++) {
					if(i>0) fprintf(F, " & ");
					Netlist_GenVHDL_ValCat_opt(F, comp, valcat, params, flags);
				}
				valcat->width = save_width;
			}
		}

	}

	// Here it is a non-repeated port
	else {
		netlist_port_t* other_port = valcat->source;
		netlist_comp_t* other_comp = other_port->component;
		if(other_comp==comp) {
			fprintf(F, "%s", other_port->name);
		}
		else if(other_comp->model==NETLIST_COMP_SIGNAL) {
			assert(other_comp->father==comp);
			fprintf(F, "%s", Netlist_Comp_GetRename_Comp(other_comp, params));
		}
		else if(params->inline_comps==true && other_comp->model->inlvhdl_en==true) {
			assert(other_comp->father==comp);
			fprintf(F, "%s", Netlist_Comp_GetRename_Comp(other_comp, params));
		}
		else {
			// Forbidden situation
			abort();
		}
		if(valcat->width==other_port->width) {}
		else if(valcat->left==valcat->right) fprintf(F, "(%u)", valcat->left);
		else fprintf(F, "(%u downto %u)", valcat->left, valcat->right);
	}

}
void Netlist_GenVHDL_ValCat_List_opt(FILE* F, netlist_comp_t* comp, chain_list* list, netlist_vhdl_t* params, int flags) {
	if(list==NULL) return;

	// Check if adding parentheses around is asked and necessary
	bool addpar = false;
	if( (flags & GENVHDL_VALCAT_CHKADDPAR)!=0 ) {
		if(list->NEXT != NULL) addpar = true;
		else {
			// If vhdl2vl-friendly, the repeat() is generated as a concat, so parenthese are needed
			netlist_val_cat_t* valcat = list->DATA;
			if(valcat->width>1 && valcat->left==valcat->right && vhd2vl_friendly==true) addpar = true;
		}
		// Remove the flag GENVHDL_VALCAT_CHKADDPAR
		flags &= ~GENVHDL_VALCAT_CHKADDPAR;
	}

	if(addpar==true) fputc('(', F);

	foreach(list, scan) {
		if(scan!=list) fprintf(F, " & ");
		Netlist_GenVHDL_ValCat_opt(F, comp, scan->DATA, params, flags);
	}

	if(addpar==true) fputc(')', F);
}

void Netlist_GenVHDL_StdLogic_Init(FILE* F, unsigned width, char init_val) {
	if(init_val!=0) {
		fprintf(F, " := ");
		if(width==1) fprintf(F, "'%c'", init_val);
		else fprintf(F, "(others => '%c')", init_val);
	}
}
void Netlist_GenVHDL_StdLogic_InitArray(FILE* F, unsigned width, char init_val) {
	if(init_val!=0) {
		fprintf(F, " := (others => ");
		if(width==1) fprintf(F, "'%c'", init_val);
		else fprintf(F, "(others => '%c')", init_val);
		fprintf(F, ")");
	}
}
void Netlist_GenVHDL_StdLogic_InitArray2(FILE* F, unsigned width, char init_val) {
	if(init_val!=0) {
		fprintf(F, " := (others => (others =>");
		if(width==1) fprintf(F, "'%c'", init_val);
		else fprintf(F, "(others => '%c')", init_val);
		fprintf(F, "))");
	}
}

void Netlist_GenVHDL_StdLogicType(FILE* F, unsigned width) {
	if(width==1) fprintf(F, "std_logic");
	else fprintf(F, "std_logic_vector(%u downto 0)", width-1);
}
void Netlist_GenVHDL_StdLogicType_be(FILE* F, unsigned width, char* beg, char* end) {
	if(beg!=NULL) fprintf(F, "%s", beg);
	Netlist_GenVHDL_StdLogicType(F, width);
	if(end!=NULL) fprintf(F, "%s", end);
}
void Netlist_GenVHDL_StdLogicType_ibe(FILE* F, unsigned width, char init_val, char* beg, char* end) {
	if(beg!=NULL) fprintf(F, "%s", beg);
	Netlist_GenVHDL_StdLogicType(F, width);
	Netlist_GenVHDL_StdLogic_Init(F, width, init_val);
	if(end!=NULL) fprintf(F, "%s", end);
}

void Netlist_GenVHDL_StdLogic_Literal_Repeat(FILE* F, unsigned width, char val) {
	if(width==1) fprintf(F, "'%c'", val);
	else {
		fputc('"', F);
		for(unsigned i=0; i<width; i++) fputc(val, F);
		fputc('"', F);
	}
}
void Netlist_GenVHDL_StdLogic_Literal_Repeat_be(FILE* F, unsigned width, char val, char* beg, char* end) {
	if(beg!=NULL) fprintf(F, "%s", beg);
	Netlist_GenVHDL_StdLogic_Literal_Repeat(F, width, val);
	if(end!=NULL) fprintf(F, "%s", end);
}
void Netlist_GenVHDL_StdLogic_Literal(FILE* F, char* value) {
	char sep = '\'';
	if(value[1]!=0) sep = '"';
	fprintf(F, "%c%s%c", sep, value, sep);
}
void Netlist_GenVHDL_StdLogic_Literal_be(FILE* F, char* value, char* beg, char* end) {
	if(beg!=NULL) fprintf(F, "%s", beg);
	Netlist_GenVHDL_StdLogic_Literal(F, value);
	if(end!=NULL) fprintf(F, "%s", end);
}

void Netlist_GenVHDL_CompDecl(FILE* F, netlist_comp_t* comp, netlist_vhdl_t* params) {
	fprintf(F, "	component %s is\n", Netlist_Comp_GetRename_Comp(comp, params));
	fprintf(F, "		port (\n");

	char* next_str = "";
	char* end_str = "";

	void genport(netlist_port_t* port) {
		fprintf(F, "%s", next_str);
		fprintf(F, "			%s : ", port->name);
		if(port->direction==NETLIST_PORT_DIR_IN) fprintf(F, "in  ");
		else fprintf(F, "out ");
		Netlist_GenVHDL_StdLogicType(F, port->width);
		next_str = ";\n";
		end_str = "\n";
	}

	avl_pp_foreach(&comp->accesses, scanAcc) {
		netlist_access_t* access = scanAcc->data;
		bool first_acc = true;
		avl_pp_foreach(&comp->ports, scanPort) {
			netlist_port_t* port = scanPort->data;
			if(port->access != access) continue;
			if(first_acc==true) {
				first_acc = false;
				fprintf(F, "%s", next_str);
				fprintf(F, "			-- Access '%s' model '%s'\n", access->name, access->model->name);
				next_str = "";
			}
			genport(port);
		}
	}

	bool first_noacc = true;
	avl_pp_foreach(&comp->ports, scanPort) {
		netlist_port_t* port = scanPort->data;
		if(port->access != NULL) continue;
		if(first_noacc==true) {
			first_noacc = false;
			fprintf(F, "%s", next_str);
			fprintf(F, "			-- Ports not part of access\n");
			next_str = "";
		}
		genport(port);
	}

	fprintf(F, "%s", end_str);

	fprintf(F,
		"		);\n"
		"	end component;\n"
	);
}
void Netlist_GenVHDL_CompInstance(FILE* F, netlist_comp_t* comp, netlist_vhdl_t* params) {
	// Generate the main VHDL section
	fprintf(F, "	%s : %s port map (\n", Netlist_Comp_GetRename_Inst(comp, params), Netlist_Comp_GetRename_Comp(comp, params));

	char* next_str = "";
	char* end_str = "";

	void genport(netlist_port_t* port) {
		fprintf(F, "%s", next_str);
		fprintf(F, "		%s => ", port->name);
		if(port->direction==NETLIST_PORT_DIR_IN) {
			assert(port->source!=NULL);
			Netlist_GenVHDL_ValCat_List(F, comp->father, port->source, params);
		}
		else {
			assert(Netlist_PortTargets_GetNb(port)==1);
			netlist_port_t* target_port = Netlist_PortTargets_GetFirst(port);
			assert(target_port!=NULL);
			assert(Netlist_Port_IsSourceFullPort(target_port, port)==true);  // FIXME actually partial dest is OK as long as there is on repeat
			netlist_comp_t* target_comp = target_port->component;
			if(target_comp->model==NETLIST_COMP_SIGNAL) {
				assert(target_comp->father==comp->father);
				fprintf(F, "%s", Netlist_Comp_GetRename_Comp(target_comp, params));
			}
			else if(params->inline_comps==true && target_comp->model->inlvhdl_en==true) {
				assert(target_comp->father==comp->father);
				fprintf(F, "%s", Netlist_Comp_GetRename_Comp(target_comp, params));
			}
			else {  // Last possiblity: it is a top-level port
				assert(comp->father!=NULL && comp->father==target_comp);
				fprintf(F, "%s", target_port->name);
			}
		}
		next_str = ",\n";
		end_str = "\n";
	}

	avl_pp_foreach(&comp->accesses, scanAcc) {
		netlist_access_t* access = scanAcc->data;
		bool first_acc = true;
		avl_pp_foreach(&comp->ports, scanPort) {
			netlist_port_t* port = scanPort->data;
			if(port->access != access) continue;
			if(first_acc==true) {
				first_acc = false;
				fprintf(F, "%s", next_str);
				fprintf(F, "			-- Access '%s' model '%s'\n", access->name, access->model->name);
				next_str = "";
			}
			genport(port);
		}
	}

	bool first_noacc = true;
	avl_pp_foreach(&comp->ports, scanPort) {
		netlist_port_t* port = scanPort->data;
		if(port->access != NULL) continue;
		if(first_noacc==true) {
			first_noacc = false;
			fprintf(F, "%s", next_str);
			fprintf(F, "			-- Ports not part of access\n");
			next_str = "";
		}
		genport(port);
	}

	fprintf(F, "%s", end_str);

	fprintf(F, "	);\n");
}
void Netlist_GenVHDL_Entity(FILE* F, netlist_comp_t* comp, netlist_vhdl_t* params) {
	fprintf(F, "entity %s is\n", Netlist_Comp_GetRename_Comp(comp, params));

	char* next_str = "";
	char* end_str = "";

	void genport(netlist_port_t* port) {
		fprintf(F, "%s", next_str);
		fprintf(F, "		%s : ", port->name);
		if(port->direction==NETLIST_PORT_DIR_IN) fprintf(F, "in  ");
		else fprintf(F, "out ");
		Netlist_GenVHDL_StdLogicType(F, port->width);
		next_str = ";\n";
		end_str = "\n";
	}

	if(avl_pp_isempty(&comp->ports)==false) {
		fprintf(F, "	port (\n");

		avl_pp_foreach(&comp->accesses, scanAcc) {
			netlist_access_t* access = scanAcc->data;
			bool first_acc = true;
			avl_pp_foreach(&comp->ports, scanPort) {
				netlist_port_t* port = scanPort->data;
				if(port->access != access) continue;
				if(first_acc==true) {
					first_acc = false;
					fprintf(F, "%s", next_str);
					fprintf(F, "		-- Access '%s' model '%s'\n", access->name, access->model->name);
					next_str = "";
				}
				genport(port);
			}
		}

		bool first_noacc = true;
		avl_pp_foreach(&comp->ports, scanPort) {
			netlist_port_t* port = scanPort->data;
			if(port->access != NULL) continue;
			if(first_noacc==true) {
				first_noacc = false;
				fprintf(F, "%s", next_str);
				fprintf(F, "		-- Ports not part of access\n");
				next_str = "";
			}
			genport(port);
		}

		fprintf(F, "%s", end_str);

		fprintf(F, "\t);\n");
	}

	fprintf(F, "end %s;\n", Netlist_Comp_GetRename_Comp(comp, params));
}

void Netlist_GenVHDL_fp(FILE* F, netlist_vhdl_t* params, netlist_comp_t* comp) {
	if(comp->model->func_gen_vhdl!=NULL) {
		comp->model->func_gen_vhdl(F, params, comp);
	}
	else {
		printf("ERROR %s:%d : The component model '%s' has no VHDL generation function.\n", __FILE__, __LINE__, comp->model->name);
		abort();
	}
}
FILE* Netlist_GenVHDL_OpenFile(netlist_comp_t* comp, netlist_vhdl_t* params) {
	char buffer[1024];
	sprintf(buffer, "%s.vhd", Netlist_Comp_GetRename_Comp(comp, params));
	FILE* F = fopen(buffer, "wb");
	if(F==NULL) {
		printf("ERROR: Could not open the file '%s'.\n", buffer);
		abort();
	}
	Netlist_GenVHDL_StdLibs(F);
	fprintf(F, "\n");
	return F;
}

void Netlist_GenVHDL(netlist_comp_t* comp, netlist_vhdl_t* params) {
	// Open a default file
	FILE* F = Netlist_GenVHDL_OpenFile(comp, params);
	// Launch VHDL generation
	Netlist_GenVHDL_fp(F, params, comp);
	// Close the file
	fclose(F);
}
void Netlist_GenVHDL_simple(netlist_comp_t* comp) {
	// Generation parameters, only the minimum necessary
	netlist_vhdl_t params;
	Netlist_GenParams_Init(&params);
	// Rename identifiers
	bitype_list* comp_ren = Netlist_Comp_Rename(comp, &params);
	avl_pp_add_overwrite(&params.comp_rename, comp, comp_ren);
	// Actual VHDL generation
	Netlist_GenVHDL(comp, &params);
	//Clear
	Netlist_GenParams_Clear(&params);
}

// Default VHDL generation function that can be used to initialize component models
void Netlist_Comp_GenVHDL_Default(FILE* F, netlist_vhdl_t* params, netlist_comp_t* comp) {
	fprintf(F, "-- WARNING: The default VHDL generator shouldn't be used (comp '%s' model '%s')\n", comp->name, comp->model->name);
	fprintf(F, "\n");

	Netlist_GenVHDL_Entity(F, comp, params);
	fprintf(F, "\n");

	fprintf(F, "architecture %s of %s is\n", params->architecture, Netlist_Comp_GetRename_Comp(comp, params));
	fprintf(F,
		"begin\n"
		"\n"
	);

	fprintf(F,"	-- FIXME This is the default VHDL generation function.\n");
	fprintf(F,"	-- All output ports are set a phony value.\n");

	avl_pp_foreach(&comp->ports, scan) {
		netlist_port_t* port = scan->data;
		if(port->direction != NETLIST_PORT_DIR_OUT) continue;
		fprintf(F, "	%s <= %s;\n", port->name, port->width==1 ? "'1'" : "(others => '0')");
	}

	fprintf(F,
		"\n"
		"end architecture;\n"
		"\n"
	);
}

