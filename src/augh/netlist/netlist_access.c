
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#include "netlist.h"
#include "netlist_access.h"



//======================================================================
// Common stuff
//======================================================================

// Default callback functions to use with access models
netlist_port_t* Netlist_Access_GetPort_Default(netlist_access_t* access, char* name) {
	return NULL;
}
char* Netlist_Access_GetName_Default(netlist_port_t* port) {
	return NULL;
}



//======================================================================
// Clock
//======================================================================

netlist_access_model_t* NETLIST_ACCESS_CLOCK = NULL;

netlist_port_t* Netlist_Comp_AddPortAccess_Clock(netlist_comp_t* comp, char* name) {
	netlist_access_t* access = Netlist_Comp_AddAccess(comp, NETLIST_ACCESS_CLOCK, name);
	if(access==NULL) return NULL;

	netlist_access_clock_t* clock_data = access->data;

	netlist_port_t* port = Netlist_Comp_AddPort_In(comp, name, 1);
	port->access = access;
	clock_data->port = port;

	// Init private fields
	clock_data->frequency = 0;
	clock_data->period_ns = 0;
	clock_data->active_edge = NETLIST_CLOCK_EDGE_RISE;

	return port;
}

netlist_access_model_t* Netlist_Access_Clock_GetModel() {
	netlist_access_model_t* model = Netlist_AccessModel_New();
	model->name = namealloc("clock");

	model->data_size = sizeof(netlist_access_clock_t);

	char* port_name = namealloc("clock");
	Netlist_AccessModel_Init_PortName(model, netlist_access_clock_t, port, port_name);

	NETLIST_ACCESS_CLOCK = model;
	return model;
}



//======================================================================
// Differential clock
//======================================================================

netlist_access_model_t* NETLIST_ACCESS_DIFFCLOCK = NULL;

netlist_access_t* Netlist_Comp_AddAccess_DiffClock(netlist_comp_t* comp, char* name) {
	netlist_access_t* access = Netlist_Comp_AddAccess(comp, NETLIST_ACCESS_DIFFCLOCK, name);
	if(access==NULL) return NULL;

	netlist_access_diffclock_t* clock_data = access->data;

	char buffer[200];
	char* port_name;

	sprintf(buffer, "%s_p", name);
	port_name = namealloc(buffer);
	clock_data->port_p = Netlist_Comp_AddPort_In(comp, port_name, 1);
	clock_data->port_p->access = access;

	sprintf(buffer, "%s_n", name);
	port_name = namealloc(buffer);
	clock_data->port_n = Netlist_Comp_AddPort_In(comp, port_name, 1);
	clock_data->port_n->access = access;

	// Init private fields
	clock_data->frequency = 0;
	clock_data->period_ns = 0;

	return access;
}

netlist_access_model_t* Netlist_Access_DiffClock_GetModel() {
	netlist_access_model_t* model = Netlist_AccessModel_New();
	model->name = namealloc("diffclock");

	model->data_size = sizeof(netlist_access_diffclock_t);

	char* port_name = NULL;
	port_name = namealloc("clock_p");
	Netlist_AccessModel_Init_PortName(model, netlist_access_diffclock_t, port_p, port_name);
	port_name = namealloc("clock_n");
	Netlist_AccessModel_Init_PortName(model, netlist_access_diffclock_t, port_n, port_name);

	NETLIST_ACCESS_DIFFCLOCK = model;
	return model;
}



//======================================================================
// Reset
//======================================================================

netlist_access_model_t* NETLIST_ACCESS_RESET = NULL;

netlist_port_t* Netlist_Comp_AddPortAccess_Reset(netlist_comp_t* comp, char* name) {
	netlist_access_t* access = Netlist_Comp_AddAccess(comp, NETLIST_ACCESS_RESET, name);
	if(access==NULL) return NULL;

	netlist_access_reset_t* reset_data = access->data;

	netlist_port_t* port = Netlist_Comp_AddPort_In(comp, name, 1);
	port->access = access;
	reset_data->port = port;

	// Init private fields
	reset_data->active_state = '1';

	return port;
}
void Netlist_Access_Reset_Rem(netlist_access_t* access) {
	netlist_access_reset_t* reset_data = access->data;
	// Remove the ports
	Netlist_Port_Free(reset_data->port);
	// Free the structure
	Netlist_Access_Free(access);
}

netlist_access_model_t* Netlist_Access_Reset_GetModel() {
	netlist_access_model_t* model = Netlist_AccessModel_New();
	model->name = namealloc("reset");

	model->data_size = sizeof(netlist_access_reset_t);

	char* port_name = namealloc("reset");
	Netlist_AccessModel_Init_PortName(model, netlist_access_reset_t, port, port_name);

	NETLIST_ACCESS_RESET = model;
	return model;
}



//======================================================================
// Generic access models
//======================================================================

static netlist_port_t* Netlist_Comp_AddPortAccess_SinglePort(
	netlist_comp_t* comp, netlist_access_model_t* access_model,
	char* access_name, char* port_name, unsigned width, int dir
) {
	netlist_access_t* access = Netlist_Comp_AddAccess(comp, access_model, access_name);
	if(access==NULL) return NULL;

	netlist_access_single_port_t* access_port = access->data;

	netlist_port_t* port;
	if(dir==NETLIST_PORT_DIR_IN) port = Netlist_Comp_AddPort_In(comp, port_name, width);
	else                         port = Netlist_Comp_AddPort_Out(comp, port_name, width);
	port->access = access;
	access_port->port = port;

	return port;
}

// Start

netlist_access_model_t* NETLIST_ACCESS_START = NULL;

netlist_port_t* Netlist_Comp_AddPortAccess_Start(netlist_comp_t* comp, char* name) {
	return Netlist_Comp_AddPortAccess_SinglePort(comp, NETLIST_ACCESS_START, name, name, 1, NETLIST_PORT_DIR_IN);
}

netlist_access_model_t* Netlist_Access_Start_GetModel() {
	netlist_access_model_t* model = Netlist_AccessModel_New();
	model->name = namealloc("start");

	model->data_size = sizeof(netlist_access_single_port_t);

	char* port_name = namealloc("start");
	Netlist_AccessModel_Init_PortName(model, netlist_access_single_port_t, port, port_name);

	NETLIST_ACCESS_START = model;
	return model;
}

// Wire

netlist_access_model_t* NETLIST_ACCESS_WIRE_IN = NULL;
netlist_access_model_t* NETLIST_ACCESS_WIRE_OUT = NULL;

netlist_port_t* Netlist_Comp_AddPortAccess_WireIn(netlist_comp_t* comp, char* name, unsigned width) {
	return Netlist_Comp_AddPortAccess_SinglePort(comp, NETLIST_ACCESS_WIRE_IN, name, name, width, NETLIST_PORT_DIR_IN);
}
netlist_port_t* Netlist_Comp_AddPortAccess_WireOut(netlist_comp_t* comp, char* name, unsigned width) {
	return Netlist_Comp_AddPortAccess_SinglePort(comp, NETLIST_ACCESS_WIRE_OUT, name, name, width, NETLIST_PORT_DIR_OUT);
}

netlist_access_model_t* Netlist_Access_WireIn_GetModel() {
	netlist_access_model_t* model = Netlist_AccessModel_New();
	model->name = namealloc("wire_in");

	model->data_size = sizeof(netlist_access_single_port_t);

	char* port_name = namealloc("wire");
	Netlist_AccessModel_Init_PortName(model, netlist_access_single_port_t, port, port_name);

	NETLIST_ACCESS_WIRE_IN = model;
	return model;
}
netlist_access_model_t* Netlist_Access_WireOut_GetModel() {
	netlist_access_model_t* model = Netlist_AccessModel_New();
	model->name = namealloc("wire_out");

	model->data_size = sizeof(netlist_access_single_port_t);

	char* port_name = namealloc("wire");
	Netlist_AccessModel_Init_PortName(model, netlist_access_single_port_t, port, port_name);

	NETLIST_ACCESS_WIRE_OUT = model;
	return model;
}



//======================================================================
// Fifo
//======================================================================

netlist_access_model_t* NETLIST_ACCESS_FIFO_IN = NULL;
netlist_access_model_t* NETLIST_ACCESS_FIFO_OUT = NULL;

static netlist_access_t* Netlist_Comp_AddAccess_Fifo(
	netlist_comp_t* comp, char* access_name, unsigned width, netlist_access_model_t* model,
	bool portnames_noacc, avl_p_tree_t* tree_avoid_names
) {
	netlist_access_t* access = Netlist_Comp_AddAccess(comp, model, access_name);

	netlist_access_fifo_t* access_fifo = access->data;
	access_fifo->width = width;

	char buffer[200];
	char* port_name;

	sprintf(buffer, "%s_data", access_name);
	port_name = Netlist_Comp_MakePortName(comp, namealloc(buffer), portnames_noacc, tree_avoid_names);
	if(model==NETLIST_ACCESS_FIFO_IN) {
		access_fifo->port_data = Netlist_Comp_AddPort_In(comp, port_name, width);
	}
	else {
		access_fifo->port_data = Netlist_Comp_AddPort_Out(comp, port_name, width);
	}
	access_fifo->port_data->access = access;

	sprintf(buffer, "%s_rdy", access_name);
	port_name = Netlist_Comp_MakePortName(comp, namealloc(buffer), portnames_noacc, tree_avoid_names);
	access_fifo->port_rdy = Netlist_Comp_AddPort_Out(comp, port_name, 1);
	access_fifo->port_rdy->access = access;

	sprintf(buffer, "%s_ack", access_name);
	port_name = Netlist_Comp_MakePortName(comp, namealloc(buffer), portnames_noacc, tree_avoid_names);
	access_fifo->port_ack = Netlist_Comp_AddPort_In(comp, port_name, 1);
	access_fifo->port_ack->access = access;

	return access;
}

netlist_access_t* Netlist_Comp_AddAccess_FifoIn(netlist_comp_t* comp, char* access_name, unsigned width, bool portnames_noacc, avl_p_tree_t* tree_avoid_names) {
	return Netlist_Comp_AddAccess_Fifo(comp, access_name, width, NETLIST_ACCESS_FIFO_IN, portnames_noacc, tree_avoid_names);
}
netlist_access_t* Netlist_Comp_AddAccess_FifoOut(netlist_comp_t* comp, char* access_name, unsigned width, bool portnames_noacc, avl_p_tree_t* tree_avoid_names) {
	return Netlist_Comp_AddAccess_Fifo(comp, access_name, width, NETLIST_ACCESS_FIFO_OUT, portnames_noacc, tree_avoid_names);
}

netlist_access_model_t* Netlist_Access_FifoIn_GetModel() {
	netlist_access_model_t* model = Netlist_AccessModel_New();
	model->name = namealloc("fifo_in");

	model->data_size = sizeof(netlist_access_fifo_t);

	char* port_name = NULL;
	port_name = namealloc("data");
	Netlist_AccessModel_Init_PortName(model, netlist_access_fifo_t, port_data, port_name);
	port_name = namealloc("rdy");
	Netlist_AccessModel_Init_PortName(model, netlist_access_fifo_t, port_rdy, port_name);
	port_name = namealloc("ack");
	Netlist_AccessModel_Init_PortName(model, netlist_access_fifo_t, port_ack, port_name);

	NETLIST_ACCESS_FIFO_IN = model;
	return model;
}
netlist_access_model_t* Netlist_Access_FifoOut_GetModel() {
	netlist_access_model_t* model = Netlist_AccessModel_New();
	model->name = namealloc("fifo_out");

	model->data_size = sizeof(netlist_access_fifo_t);

	char* port_name = NULL;
	port_name = namealloc("data");
	Netlist_AccessModel_Init_PortName(model, netlist_access_fifo_t, port_data, port_name);
	port_name = namealloc("rdy");
	Netlist_AccessModel_Init_PortName(model, netlist_access_fifo_t, port_rdy, port_name);
	port_name = namealloc("ack");
	Netlist_AccessModel_Init_PortName(model, netlist_access_fifo_t, port_ack, port_name);

	NETLIST_ACCESS_FIFO_OUT = model;
	return model;
}

void Netlist_Access_Fifo_Connect(netlist_access_t* fifo_in, netlist_access_t* fifo_out) {
	netlist_access_fifo_t* in_data = fifo_in->data;
	netlist_access_fifo_t* out_data = fifo_out->data;

	in_data->port_data->source = Netlist_ValCat_MakeList_FullPort(out_data->port_data);
	Netlist_PortTargets_SetSource(in_data->port_data);

	out_data->port_ack->source = Netlist_ValCat_MakeList_FullPort(in_data->port_rdy);
	Netlist_PortTargets_SetSource(out_data->port_ack);

	in_data->port_ack->source = Netlist_ValCat_MakeList_FullPort(out_data->port_rdy);
	Netlist_PortTargets_SetSource(in_data->port_ack);
}


