
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#include "../chain.h"
#include "../indent.h"
#include "../hvex/hvex_misc.h"  // For FSM expressions

#include "netlist.h"
#include "netlist_comps.h"
#include "netlist_access.h"
#include "netlist_vhdl.h"
#include "netlist_cmd.h"
#include "netlist_simp.h"



// When set to zero, no initial value. Else it is an std_logic bit.
static char default_store_value = '0';



//===================================================
// TOP functions
//===================================================

netlist_comp_model_t* NETLIST_COMP_TOP = NULL;

netlist_comp_t* Netlist_Comp_Top_New(char* name) {
	netlist_comp_t* comp = Netlist_Comp_New(NETLIST_COMP_TOP);
	comp->name = name;

	// Note: by default, nothing is set
	//netlist_top_t* top_data = comp->data;

	return comp;
}

static void Netlist_Comp_Top_DataDup(avl_pp_tree_t* tree_old2new, netlist_comp_t* comp, netlist_comp_t* newcomp) {
	netlist_top_t* top_data = comp->data;
	netlist_top_t* newtop_data = newcomp->data;

	newtop_data->extern_model_name = top_data->extern_model_name;
}

// Note: this generation function can be used for any component
//   that consists ONLY of other properly interconnected components
static void Netlist_Top_GenVHDL(FILE* F, netlist_vhdl_t* params, netlist_comp_t* comp) {

	if(params->separate==true) {
		avl_pp_foreach(&comp->children, scanChild) {
			netlist_comp_t* child = scanChild->data;
			if(child->model==NETLIST_COMP_SIGNAL) continue;
			if( (child->flags & NETLIST_COMP_ISEXTERN) != 0) continue;
			if(params->inline_comps==true && child->model->inlvhdl_en==true) continue;
			Netlist_GenVHDL(child, params);
		}
	}

	Netlist_GenVHDL_Entity(F, comp, params);
	fprintf(F, "\n");

	fprintf(F, "architecture %s of %s is\n", params->architecture, Netlist_Comp_GetRename_Comp(comp, params));
	fprintf(F, "\n");

	fprintf(F, "	-- Declaration of components\n");
	fprintf(F, "\n");

	// This tree is to avoid writing several times the declaration of a given component model
	avl_p_tree_t tree_extcompmods;
	avl_p_init(&tree_extcompmods);

	if(params->separate==true) {
		avl_pp_foreach(&comp->children, scanChild) {
			netlist_comp_t* child = scanChild->data;
			if(child->model==NETLIST_COMP_SIGNAL) continue;
			if(params->inline_comps==true && child->model->inlvhdl_en==true) continue;
			if( (child->flags & NETLIST_COMP_ISEXTERN) != 0) {
				netlist_top_t* child_top_data = child->data;
				bool b = avl_p_add_overwrite(&tree_extcompmods, child_top_data->extern_model_name, child_top_data->extern_model_name);
				if(b==false) continue;  // This component model was already declared
			}
			Netlist_GenVHDL_CompDecl(F, child, params);
			fprintf(F, "\n");
		}
	}

	avl_p_reset(&tree_extcompmods);

	fprintf(F, "	-- Declaration of signals\n");
	fprintf(F, "\n");
	avl_pp_foreach(&comp->children, scanChild) {
		netlist_comp_t* child = scanChild->data;
		if(child->model!=NETLIST_COMP_SIGNAL) continue;
		netlist_signal_t* sig_data = child->data;
		fprintf(F, "	signal %s : ", Netlist_Comp_GetRename_Comp(child, params));
		Netlist_GenVHDL_StdLogicType_be(F, sig_data->width, NULL, ";\n");
	}

	if(params->inline_comps==true) {
		fprintf(F, "\n");
		fprintf(F, "	-- Other inlined components\n");
		fprintf(F, "\n");
		avl_pp_foreach(&comp->children, scanChild) {
			netlist_comp_t* child = scanChild->data;
			if(child->model==NETLIST_COMP_SIGNAL) continue;
			if(child->model->inlvhdl_en==false) continue;
			child->model->func_inlvhdl_sigdecl(F, params, child);
		}
	}

	if(vhd2vl_friendly==false) {
		fprintf(F,
			"\n"
			"	-- This utility function is used for to generate concatenations of std_logic\n"
			"\n"
		);
		Netlist_GenVHDL_MyRepeat(F);
	}

	fprintf(F,
		"\n"
		"begin\n"
		"\n"
		"\t-- Instantiation of components\n"
		"\n"
	);

	// Write the instantiation of the components
	avl_pp_foreach(&comp->children, scanChild) {
		netlist_comp_t* child = scanChild->data;
		if(child->model==NETLIST_COMP_SIGNAL) continue;
		if(params->inline_comps==true && child->model->inlvhdl_en==true) continue;
		Netlist_GenVHDL_CompInstance(F, child, params);
		fprintf(F, "\n");
	}

	// Behaviour of inlined components
	if(params->inline_comps==true) {
		avl_p_tree_t inlcomps_todo;
		avl_p_init(&inlcomps_todo);
		// Get the component models that need InlVHDL_AllComps to be launched
		avl_pp_foreach(&comp->children, scanChild) {
			netlist_comp_t* child = scanChild->data;
			if(child->model->inlvhdl_en==false) continue;
			if(child->model->func_inlvhdl_allcomps!=NULL) {
				avl_p_add_overwrite(&inlcomps_todo, child->model, child->model);
			}
			else {
				assert(child->model->func_inlvhdl_onecomp!=NULL);
				fprintf(F, "	-- Behaviour of component '%s' model '%s'\n", child->name, child->model->name);
				child->model->func_inlvhdl_onecomp(F, params, child);
				fprintf(F, "\n");
			}
		}
		// Generate the code common to all components of same model
		avl_p_foreach(&inlcomps_todo, scanModel) {
			netlist_comp_model_t* model = scanModel->data;
			fprintf(F, "	-- Behaviour of all components of model '%s'\n", model->name);
			model->func_inlvhdl_allcomps(F, params, comp);
			fprintf(F, "\n");
		}
		avl_p_reset(&inlcomps_todo);
	}

	fprintf(F,
		"\t-- Remaining signal assignments\n"
		"\t-- Those who are not assigned by component instantiation\n"
		"\n"
	);

	avl_pp_foreach(&comp->children, scanChild) {
		netlist_comp_t* child = scanChild->data;
		if(child->model!=NETLIST_COMP_SIGNAL) continue;
		netlist_signal_t* sig_data = child->data;
		bool can_skip = true;
		if(sig_data->port_in->source->NEXT!=NULL) can_skip = false;
		else {
			netlist_val_cat_t* valcat = sig_data->port_in->source->DATA;
			if(valcat->literal!=NULL) can_skip = false;
			else {
				assert(valcat->source!=NULL);
				netlist_comp_t* source_comp = valcat->source->component;
				if(source_comp==comp) can_skip = false;
				if(source_comp->model==NETLIST_COMP_SIGNAL) can_skip = false;
				if(params->inline_comps==true && source_comp->model->inlvhdl_en==true) can_skip = false;
			}
		}
		if(can_skip==true) continue;
		fprintf(F, "\t%s <= ", Netlist_Comp_GetRename_Comp(child, params));
		Netlist_GenVHDL_ValCat_List_opt(F, comp, sig_data->port_in->source, params, true);
		fprintf(F, ";\n");
	}

	fprintf(F,
		"\n"
		"\t-- Remaining top-level ports assignments\n"
		"\t-- Those who are not assigned by component instantiation\n"
		"\n"
	);

	avl_pp_foreach(&comp->ports, scanPort) {
		netlist_port_t* port = scanPort->data;
		if(port->direction!=NETLIST_PORT_DIR_OUT) continue;
		// Check if there is a connection
		if(port->source==NULL) {
			printf("WARNING: Top-level output port '%s' has no source. '0' is set by default.\n", port->name);
			fprintf(F, "\t%s <= ", port->name);
			Netlist_GenVHDL_StdLogic_Literal_Repeat(F, port->width, '0');
			fprintf(F, ";\n");
			continue;
		}
		// Process
		bool can_skip = true;
		if(port->source->NEXT!=NULL) can_skip = false;
		else {
			netlist_val_cat_t* valcat = port->source->DATA;
			if(valcat->literal!=NULL) can_skip = false;
			else {
				assert(valcat->source!=NULL);
				netlist_comp_t* source_comp = valcat->source->component;
				if(source_comp==comp) can_skip = false;
				if(source_comp->model==NETLIST_COMP_SIGNAL) can_skip = false;
				if(params->inline_comps==true && source_comp->model->inlvhdl_en==true) can_skip = false;
			}
		}
		if(can_skip==true) continue;
		fprintf(F, "\t%s <= ", port->name);
		Netlist_GenVHDL_ValCat_List_opt(F, comp, port->source, params, true);
		fprintf(F, ";\n");
	}

	fprintf(F,
		"\n"
		"end architecture;\n"
	);
}

netlist_comp_model_t* Netlist_Comp_Top_GetModel() {
	netlist_comp_model_t* model = Netlist_CompModel_New();
	model->name = namealloc("top");

	model->data_size         = sizeof(netlist_top_t);
	model->func_dup_internal = Netlist_Comp_Top_DataDup;
	model->func_gen_vhdl     = Netlist_Top_GenVHDL;

	NETLIST_COMP_TOP = model;
	return model;
}



//===================================================
// FSM functions
//===================================================

netlist_comp_model_t* NETLIST_COMP_FSM = NULL;

// Create a pool of type : netlist_fsm_state_t

#define POOL_prefix      pool_netlist_fsm_state
#define POOL_data_t      netlist_fsm_state_t
#define POOL_ALLOC_SIZE  1000

#define POOL_INSTANCE             POOL_NETLIST_FSM_STATE
#define POOL_INSTANCE_ATTRIBUTES  static

#include "../pool.h"

// Allocation wrappers

netlist_fsm_state_t* Netlist_FsmState_New() {
	netlist_fsm_state_t* state = pool_netlist_fsm_state_pop(&POOL_NETLIST_FSM_STATE);
	memset(state, 0, sizeof(*state));
	state->cycles_nb = 1;
	return state;
}
void Netlist_FsmState_Free(netlist_fsm_state_t* state) {
	pool_netlist_fsm_state_push(&POOL_NETLIST_FSM_STATE, state);
}


// Create a pool of type : netlist_fsm_action_t

#define POOL_prefix      pool_netlist_fsm_action
#define POOL_data_t      netlist_fsm_action_t
#define POOL_ALLOC_SIZE  1000

#define POOL_INSTANCE             POOL_NETLIST_FSM_ACTION
#define POOL_INSTANCE_ATTRIBUTES  static

#include "../pool.h"

// Allocation wrappers

netlist_fsm_action_t* Netlist_FsmAction_New() {
	netlist_fsm_action_t* action = pool_netlist_fsm_action_pop(&POOL_NETLIST_FSM_ACTION);
	memset(action, 0, sizeof(*action));
	action->only_last_cycle = false;
	return action;
}
void Netlist_FsmAction_Free(netlist_fsm_action_t* action) {
	pool_netlist_fsm_action_push(&POOL_NETLIST_FSM_ACTION, action);
}


// Create a pool of type : netlist_fsm_outval_t

#define POOL_prefix      pool_netlist_fsm_outval
#define POOL_data_t      netlist_fsm_outval_t
#define POOL_ALLOC_SIZE  1000

#define POOL_INSTANCE             POOL_NETLIST_FSM_OUTVAL
#define POOL_INSTANCE_ATTRIBUTES  static

#include "../pool.h"

// Allocation wrappers

netlist_fsm_outval_t* Netlist_FsmOutVal_New() {
	netlist_fsm_outval_t* outval = pool_netlist_fsm_outval_pop(&POOL_NETLIST_FSM_OUTVAL);
	memset(outval, 0, sizeof(*outval));
	return outval;
}
void Netlist_FsmOutVal_Free(netlist_fsm_outval_t* outval) {
	pool_netlist_fsm_outval_push(&POOL_NETLIST_FSM_OUTVAL, outval);
}


// Create a pool of type : netlist_fsm_rule_t

#define POOL_prefix      pool_netlist_fsm_rule
#define POOL_data_t      netlist_fsm_rule_t
#define POOL_ALLOC_SIZE  1000

#define POOL_INSTANCE             POOL_NETLIST_FSM_RULE
#define POOL_INSTANCE_ATTRIBUTES  static

#include "../pool.h"

// Allocation wrappers

netlist_fsm_rule_t* Netlist_FsmRule_New() {
	netlist_fsm_rule_t* rule = pool_netlist_fsm_rule_pop(&POOL_NETLIST_FSM_RULE);
	memset(rule, 0, sizeof(*rule));
	return rule;
}
void Netlist_FsmRule_Free(netlist_fsm_rule_t* rule) {
	if(rule==NULL) return;
	if(rule->condition!=NULL) hvex_free(rule->condition);
	if(rule->rule_true!=NULL) Netlist_FsmRule_Free(rule->rule_true);
	if(rule->rule_false!=NULL) Netlist_FsmRule_Free(rule->rule_false);
	if(rule->func_call!=NULL) {
		Netlist_FsmRule_Free(rule->func_call->DATA_TO);
		freebitype(rule->func_call);
	}
	if(rule->func_rets!=NULL) {
		foreach(rule->func_rets, scan) Netlist_FsmRule_Free(scan->DATA_TO);
		freebitype(rule->func_rets);
	}
	pool_netlist_fsm_rule_push(&POOL_NETLIST_FSM_RULE, rule);
}

// Create a pool of type : netlist_fsm_rtmcnt_t

#define POOL_prefix      pool_netlist_fsm_rtmcnt
#define POOL_data_t      netlist_fsm_rtmcnt_t
#define POOL_ALLOC_SIZE  100

#define POOL_INSTANCE             POOL_NETLIST_FSM_RTMCNT
#define POOL_INSTANCE_ATTRIBUTES  static

#include "../pool.h"

// Allocation wrappers

netlist_fsm_rtmcnt_t* Netlist_FsmRtmCnt_New() {
	netlist_fsm_rtmcnt_t* counter = pool_netlist_fsm_rtmcnt_pop(&POOL_NETLIST_FSM_RTMCNT);
	memset(counter, 0, sizeof(*counter));
	return counter;
}
void Netlist_FsmRtmCnt_Free(netlist_fsm_rtmcnt_t* counter) {
	freechain(counter->states);
	pool_netlist_fsm_rtmcnt_push(&POOL_NETLIST_FSM_RTMCNT, counter);
}



netlist_comp_t* Netlist_Comp_Fsm_New(char* name) {
	netlist_comp_t* comp = Netlist_Comp_New(NETLIST_COMP_FSM);
	comp->name = name;

	netlist_fsm_t* fsm_data = comp->data;

	fsm_data->reset_active_level = false;
	avl_pp_init(&fsm_data->output_values);
	avl_pp_init(&fsm_data->func_calls);

	fsm_data->outputs_buf = true;
	fsm_data->retime_en = false;

	char* port_name;
	port_name = namealloc("clock");
	fsm_data->port_clock = Netlist_Comp_AddPortAccess_Clock(comp, port_name);
	port_name = namealloc("reset");
	fsm_data->port_reset = Netlist_Comp_AddPortAccess_Reset(comp, port_name);

	return comp;
}

static void Netlist_Comp_Fsm_Free_Data(netlist_comp_t* comp) {
	netlist_fsm_t* comp_fsm = comp->data;
	// Free the states
	foreach(comp_fsm->states, scanState) {
		netlist_fsm_state_t* state = scanState->DATA;
		// Free the actions
		foreach(state->actions, scanAction) {
			Netlist_FsmAction_Free(scanAction->DATA);
		}
		freechain(state->actions);
		// Free the rules
		Netlist_FsmRule_Free(state->rules);
		freechain(state->prev_rules);
		// Free the state
		Netlist_FsmState_Free(state);
	}
	freechain(comp_fsm->states);
	// Free the output values
	avl_pp_foreach(&comp_fsm->output_values, scanVal) {
		netlist_fsm_outval_t* outval = scanVal->data;
		freechain(outval->actions);
		Netlist_FsmOutVal_Free(outval);
	}
	avl_pp_reset(&comp_fsm->output_values);
	// The retiming counters
	foreach(comp_fsm->rtm_counters, scanCounter) Netlist_FsmRtmCnt_Free(scanCounter->DATA);
	freechain(comp_fsm->rtm_counters);
	// Free the main structure
	free(comp_fsm);
}
// Note: states, actions etc are not duplicated. Too much code, unused for now.
static void Netlist_Comp_Fsm_Dup(avl_pp_tree_t* tree_old2new, netlist_comp_t* comp, netlist_comp_t* newcomp) {
	netlist_fsm_t* fsm_data = comp->data;
	netlist_fsm_t* newfsm_data = newcomp->data;

	avl_pp_init(&newfsm_data->output_values);

	void* pointer_old2new(void* oldptr) {
		void* newptr = NULL;
		avl_pp_find_data(tree_old2new, oldptr, &newptr);
		return newptr;
	}

	newfsm_data->port_clock = pointer_old2new(fsm_data->port_clock);
	newfsm_data->port_reset = pointer_old2new(fsm_data->port_reset);

	newfsm_data->reset_active_level = fsm_data->reset_active_level;

	newfsm_data->inputs_nb = fsm_data->inputs_nb;
	newfsm_data->outputs_nb = fsm_data->outputs_nb;
}


netlist_port_t* Netlist_Comp_Fsm_AddInput(netlist_comp_t* comp, unsigned width) {
	netlist_fsm_t* comp_fsm = comp->data;

	char buffer[200];
	sprintf(buffer, "in%d", comp_fsm->input_name_idx++);
	char* name_alloc = namealloc(buffer);

	netlist_port_t* port = Netlist_Comp_AddPort_In(comp, name_alloc, width);
	comp_fsm->inputs_nb++;

	return port;
}
netlist_port_t* Netlist_Comp_Fsm_AddOutput(netlist_comp_t* comp, unsigned width) {
	netlist_fsm_t* comp_fsm = comp->data;

	char buffer[200];
	sprintf(buffer, "out%d", comp_fsm->output_name_idx++);
	char* name_alloc = namealloc(buffer);

	netlist_port_t* port = Netlist_Comp_AddPort_Out(comp, name_alloc, width);
	comp_fsm->outputs_nb++;

	return port;
}

netlist_fsm_outval_t* Netlist_Comp_Fsm_GetOutVal(netlist_port_t* port) {
	netlist_fsm_t* comp_fsm = port->component->data;

	avl_pp_node_t* avl_node = NULL;
	bool b = avl_pp_find_node(&comp_fsm->output_values, port, &avl_node);
	if(b==false) return NULL;

	return avl_node->data;
}
netlist_fsm_outval_t* Netlist_Comp_Fsm_GetAddOutVal(netlist_port_t* port) {
	netlist_fsm_t* comp_fsm = port->component->data;

	avl_pp_node_t* avl_node = NULL;
	bool b = avl_pp_add_node(&comp_fsm->output_values, port, &avl_node);
	if(b==true) {
		netlist_fsm_outval_t* outval = Netlist_FsmOutVal_New();
		avl_node->data = outval;
		outval->port = port;
		outval->is_buffered = comp_fsm->outputs_buf;
	}

	return avl_node->data;
}

netlist_fsm_state_t* Netlist_Comp_Fsm_AddState(netlist_comp_t* comp) {
	netlist_fsm_t* comp_fsm = comp->data;

	netlist_fsm_state_t* state = Netlist_FsmState_New();
	comp_fsm->states = addchain(comp_fsm->states, state);

	comp_fsm->states_nb ++;
	return state;
}
netlist_fsm_action_t* Netlist_Comp_Fsm_AddAction(netlist_comp_t* comp, netlist_fsm_state_t* state, netlist_port_t* port, char* value) {
	//netlist_fsm_t* comp_fsm = comp->data;

	netlist_fsm_action_t* action = Netlist_FsmAction_New();
	state->actions = addchain(state->actions, action);

	netlist_fsm_outval_t* outval = Netlist_Comp_Fsm_GetAddOutVal(port);
	outval->actions = addchain(outval->actions, action);

	action->state = state;
	action->outval = outval;
	action->value = value;

	return action;
}
void Netlist_Comp_Fsm_SetDefaultValue(netlist_port_t* port, char* value) {
	netlist_fsm_outval_t* outval = Netlist_Comp_Fsm_GetAddOutVal(port);
	outval->default_val = value;
}

void Netlist_Comp_Fsm_RemoveAction(netlist_fsm_action_t* action) {
	netlist_fsm_outval_t* outval = action->outval;
	// Remove the Action from its state
	action->state->actions = ChainList_Remove(action->state->actions, action);
	// Remove the Action from the list of Actions
	outval->actions = ChainList_Remove(outval->actions, action);
	// Free the Action
	Netlist_FsmAction_Free(action);
}
void Netlist_Comp_Fsm_RemoveOutput(netlist_port_t* port) {
	netlist_fsm_t* comp_fsm = port->component->data;

	// Remove the Actions that set something.
	netlist_fsm_outval_t* outval = NULL;
	avl_pp_find_data(&comp_fsm->output_values, port, (void**)&outval);
	if(outval!=NULL) {
		// Cleanly remove and free all Actions
		while(outval->actions!=NULL) {
			netlist_fsm_action_t* action = outval->actions->DATA;
			Netlist_Comp_Fsm_RemoveAction(action);
		}
		// Free the outval structure
		Netlist_FsmOutVal_Free(outval);
		// Remove from the tree
		avl_pp_rem_key(&comp_fsm->output_values, port);
	}

	// Remove the port
	Netlist_Port_Free(port);
	comp_fsm->outputs_nb--;
}

netlist_fsm_func_t* Netlist_Comp_Fsm_Func_New(netlist_comp_t* comp, char* name) {
	netlist_fsm_t* fsm_data = comp->data;
	netlist_fsm_func_t* fsmfunc = calloc(1, sizeof(*fsmfunc));
	fsmfunc->name_func = name;
	avl_pp_add_overwrite(&fsm_data->func_calls, fsmfunc->name_func, fsmfunc);
	return fsmfunc;
}
void Netlist_Comp_Fsm_Func_Free(netlist_comp_t* comp, netlist_fsm_func_t* fsmfunc) {
	netlist_fsm_t* fsm_data = comp->data;
	avl_pp_rem_key(&fsm_data->func_calls, fsmfunc->name_func);
	free(fsmfunc);
}
netlist_fsm_func_t* Netlist_Comp_Fsm_Func_Get(netlist_comp_t* comp, char* name) {
	netlist_fsm_t* fsm_data = comp->data;
	avl_pp_node_t* pp_node = NULL;
	bool b = avl_pp_find(&fsm_data->func_calls, name, &pp_node);
	if(b==true) return pp_node->data;
	return NULL;
}

void Netlist_Comp_Fsm_SetStateIdx(netlist_comp_t* comp) {
	netlist_fsm_t* fsm_data = comp->data;
	unsigned state_idx = 0;
	foreach(fsm_data->states, scanState) {
		netlist_fsm_state_t* state = scanState->DATA;
		state->index = state_idx;
		state_idx++;
	}
}
void Netlist_Comp_Fsm_RtmCounters_Clear(netlist_comp_t* comp) {
	netlist_fsm_t* fsm_data = comp->data;
	if(fsm_data->rtm_counters_nb==0) {
		assert(fsm_data->rtm_counters==NULL);
		return;
	}
	foreach(fsm_data->rtm_counters, scanCounter) {
		netlist_fsm_rtmcnt_t* counter = scanCounter->DATA;
		foreach(counter->states, scanState) {
			netlist_fsm_state_t* state = scanState->DATA;
			state->rtm_counter = NULL;
		}
		Netlist_FsmRtmCnt_Free(counter);
	}
	freechain(fsm_data->rtm_counters);
	fsm_data->rtm_counters = NULL;
	fsm_data->rtm_counters_nb = 0;
}
void Netlist_Comp_Fsm_RtmCounters_Build(netlist_comp_t* comp) {
	netlist_fsm_t* fsm_data = comp->data;

	// Reset previously built counters
	if(fsm_data->rtm_counters_nb!=0) {
		Netlist_Comp_Fsm_RtmCounters_Clear(comp);
	}

	netlist_fsm_rtmcnt_t* counter = NULL;

	// Scan all states and check if a counter is needed
	foreach(fsm_data->states, scanState) {
		netlist_fsm_state_t* state = scanState->DATA;
		if(fsmretime_lut6==true || state->cycles_nb>1) {
			// Ensure there is a counter
			// Ensure the fanout is correct
			if(counter==NULL || (fsm_data->rtm_counter_fanout > 0 && counter->states_nb > fsm_data->rtm_counter_fanout)) {
				counter = Netlist_FsmRtmCnt_New();
				counter->index = fsm_data->rtm_counters_nb++;
				fsm_data->rtm_counters = addchain(fsm_data->rtm_counters, counter);
			}
			// Link the state to the counter
			state->rtm_counter = counter;
			counter->states_nb++;
			counter->states = addchain(counter->states, state);
		}
	}

}
void Netlist_Comp_Fsm_PrevRules_Build(netlist_comp_t* comp) {
	netlist_fsm_t* fsm_data = comp->data;
	// Clear existing lists
	foreach(fsm_data->states, scanState) {
		netlist_fsm_state_t* state = scanState->DATA;
		freechain(state->prev_rules);
		state->prev_rules = NULL;
	}
	// Set the field 'father' of all rules
	void rule_setfather(netlist_fsm_rule_t* rule, void* father, bool father_is_rule) {
		if(rule==NULL) return;
		rule->father = father;
		rule->father_is_rule = father_is_rule;
		rule_setfather(rule->rule_true, rule, true);
		rule_setfather(rule->rule_false, rule, true);
		foreach(rule->func_call, scan) rule_setfather(scan->DATA_TO, rule, true);
		foreach(rule->func_rets, scan) rule_setfather(scan->DATA_TO, rule, true);
	}
	foreach(fsm_data->states, scanState) {
		netlist_fsm_state_t* state = scanState->DATA;
		rule_setfather(state->rules, state, false);
	}
	// Fill the lists of previous rules
	foreach(fsm_data->states, scanState) {
		netlist_fsm_state_t* state = scanState->DATA;
		// A local utility function
		void rule_setprevrule(netlist_fsm_rule_t* rule) {
			if(rule==NULL) return;
			if(rule->state!=NULL) rule->state->prev_rules = addchain(rule->state->prev_rules, rule);
			rule_setprevrule(rule->rule_true);
			rule_setprevrule(rule->rule_false);
			foreach(rule->func_call, scan) rule_setprevrule(scan->DATA_TO);
			foreach(rule->func_rets, scan) rule_setprevrule(scan->DATA_TO);
		}
		rule_setprevrule(state->rules);
	}
}
netlist_fsm_state_t* Netlist_Comp_Fsm_Rule_GetFatherState(netlist_fsm_rule_t* rule) {
	while(rule->father_is_rule==true) rule = rule->father;
	return rule->father;
}

// Implementation good for simulation speed
static void Netlist_Fsm_GenVHDL_Simu(FILE* F, netlist_vhdl_t* params, netlist_comp_t* comp) {
	netlist_fsm_t* fsm_data = comp->data;

	// Set the state indexes
	Netlist_Comp_Fsm_SetStateIdx(comp);

	// Needed for retiming, preliminary version
	fprintf(F,
		"library ieee;\n"
		"use ieee.numeric_std.all;\n"
		"\n"
	);

	Netlist_GenVHDL_Entity(F, comp, params);
	fprintf(F, "\n");

	fprintf(F, "architecture %s of %s is\n", params->architecture, Netlist_Comp_GetRename_Comp(comp, params));
	fputc('\n', F);

	ftabprintf(F, 1, "signal state_cur  : natural := %u;\n", fsm_data->init_state->index);

	fputc('\n', F);
	fprintf(F, "	-- Buffers for outputs\n");
	avl_pp_foreach(&fsm_data->output_values, scanOut) {
		netlist_fsm_outval_t* outval = scanOut->data;
		fprintf(F, "	signal %s_buf : ", outval->port->name);
		Netlist_GenVHDL_StdLogicType(F, outval->port->width);
		fprintf(F, " := ");
		if(outval->default_val!=NULL) {
			Netlist_GenVHDL_StdLogic_Literal(F, outval->default_val);
		}
		else {
			fprintf(F, "%s", outval->port->width==1 ? "'0'" : "(others => '0')");
		}
		fprintf(F, ";\n");
	}

	// For function calls: declare registers
	if(avl_pp_isempty(&fsm_data->func_calls)==false) {
		fputc('\n', F);
		fprintf(F, "	-- Function calls: return IDs\n");
		char buf[20];  // To build register names
		unsigned fsmfunc_idx = 0;
		avl_pp_foreach(&fsm_data->func_calls, scan) {
			netlist_fsm_func_t* fsmfunc = scan->data;
			if(fsmfunc->calls_nb < 2) continue;
			// Build the name of the register
			sprintf(buf, "funccall%u", fsmfunc_idx);
			fsmfunc->name_reg = namealloc(buf);
			fsmfunc_idx++;
			// Declare the signal
			fprintf(F, "	signal %s : natural := 0;\n", fsmfunc->name_reg);
		}
	}

	// Utility function
	void print_action(netlist_fsm_action_t* action) {
		char buf[128];
		sprintf(buf, "%s_buf", action->outval->port->name);
		fprintf(F, "%s <= ", buf);
		Netlist_GenVHDL_StdLogic_Literal(F, action->value);
		fprintf(F, ";\n");
	}

	fprintfm(F,
		"\n"
		"	-- The counter for state duration\n"
		"	signal rtmcounter : natural := 0;\n"
		"\n"
		"begin\n"
		"\n"
		"	-- Sequential process\n"
		"	-- Set the current state\n"
		"\n"
		"	process (%s)\n", fsm_data->port_clock->name,
		"	begin\n"
		"		if %s(%s) then\n", Netlist_GenVHDL_ClockEdge_TestFunc_port(fsm_data->port_clock), fsm_data->port_clock->name,
		"\n"
		"			-- Initialization: Default values for outputs\n",
		NULL
	);

	avl_pp_foreach(&fsm_data->output_values, scanOut) {
		netlist_fsm_outval_t* outval = scanOut->data;
		fprintf(F, "			%s_buf <= ", outval->port->name);
		if(outval->default_val!=NULL) {
			Netlist_GenVHDL_StdLogic_Literal(F, outval->default_val);
		}
		else {
			fprintf(F, "%s", outval->port->width==1 ? "'0'" : "(others => '0')");
		}
		fprintf(F, ";\n");
	}

	fprintf(F,
		"\n"
		"			-- For all states, compute the next state\n"
		"			-- Also set the outputs\n"
		"\n"
		"			case state_cur is\n"
		"\n"
	);

	foreach(fsm_data->states, scanState) {
		netlist_fsm_state_t* state = scanState->DATA;
		if(state==fsm_data->init_state) {
			fprintf(F, "				-- Info: This is the init/reset state\n");
		}
		fprintf(F, "				when %u =>\n", state->index);

		void print_state_cond(netlist_fsm_rule_t* rule, unsigned tabs) {
			if(rule==NULL) return;
			if(rule->state!=NULL) {
				ftabprintf(F, tabs, "state_cur <= %u;\n", rule->state->index);
				ftabprintf(F, tabs, "-- Next values for outputs\n");
				foreach(rule->state->actions, scanAction) {
					netlist_fsm_action_t* action = scanAction->DATA;
					if(action->only_last_cycle==false || rule->state->cycles_nb==1) {
						ftabprint(F, tabs); print_action(action);
					}
				}
				return;
			}
			if(rule->condition!=NULL) {
				// Print the condition
				ftabprintf(F, tabs, "if ");
				hvex_PrintVHDL_cond(F, rule->condition, false);
				// Print what to do
				fprintf(F, " then\n");
				print_state_cond(rule->rule_true, tabs+1);
				ftabprintf(F, tabs, "else\n");
				print_state_cond(rule->rule_false, tabs+1);
				ftabprintf(F, tabs, "end if;\n");
				return;
			}
			if(rule->func_call!=NULL) {
				// This is a function call
				// Note: no need to check the end of the state to store the call ID
				netlist_fsm_func_t* fsmfunc = rule->func_call->DATA_FROM;
				unsigned id = rule->func_call->TYPE;
				ftabprintf(F, tabs, "-- Function call: %s\n", fsmfunc->name_func);
				if(fsmfunc->calls_nb >= 2) {
					ftabprintf(F, tabs, "-- Save the call origin\n");
					ftabprintf(F, tabs, "%s <= %u;\n", fsmfunc->name_reg, id);
					ftabprintf(F, tabs, "-- This is where the function call leads\n");
				}
				print_state_cond(rule->func_call->DATA_TO, tabs);
				return;
			}
			if(rule->func_rets!=NULL) {
				// This is a function return
				netlist_fsm_func_t* fsmfunc = rule->func_rets->DATA_FROM;
				if(fsmfunc->calls_nb < 2) {
					ftabprintf(F, tabs, "-- Return from function: %s\n", fsmfunc->name_func);
					print_state_cond(rule->func_rets->DATA_TO, tabs);
				}
				else {
					foreach(rule->func_rets, scan) {
						unsigned id = scan->TYPE;
						ftabprintf(F, tabs, "-- Return from function: %s\n", fsmfunc->name_func);
						ftabprintf(F, tabs, "if %s = %u then\n", fsmfunc->name_reg, id);
						print_state_cond(scan->DATA_TO, tabs+1);
						ftabprintf(F, tabs, "end if;\n");
					}
				}
				return;
			}
			// Paranoia?
			assert(rule->rule_true!=NULL);
			print_state_cond(rule->rule_true, tabs);
		}

		if(state->cycles_nb==1) {
			print_state_cond(state->rules, 5);
			ftabprintf(F, 5, "rtmcounter <= 0;\n");
		}
		else {
			ftabprintf(F, 5, "if rtmcounter = %u then\n", state->cycles_nb-1);
			ftabprintf(F, 6, "-- This is the last cycle, compute the next state\n");
			print_state_cond(state->rules, 6);
			ftabprintf(F, 6, "-- Reset the counter\n");
			ftabprintf(F, 6, "rtmcounter <= 0;\n");
			ftabprintf(F, 5, "else  -- Stay in the current state\n");
			ftabprintf(F, 6, "rtmcounter <= rtmcounter + 1;\n");
			foreach(state->actions, scanAction) {
				netlist_fsm_action_t* action = scanAction->DATA;
				if(action->only_last_cycle==false) {
					ftabprint(F, 6); print_action(action);
				}
			}
			ftabprintf(F, 6, "if rtmcounter = %u then\n", state->cycles_nb-2);
			ftabprintf(F, 7, "-- Next cycle is the last of current state\n");
			foreach(state->actions, scanAction) {
				netlist_fsm_action_t* action = scanAction->DATA;
				if(action->only_last_cycle==true) {
					ftabprint(F, 7); print_action(action);
				}
			}
			ftabprintf(F, 6, "end if;\n");
			ftabprintf(F, 5, "end if;\n");
		}

		fputc('\n', F);
	}  // Scan all states, generate next state and outputs.

	fprintf(F, "				when others =>\n");
	fprintf(F, "					report \"ERROR This state should not be reachable. Stopping simulation.\" severity failure;\n");
	fputc('\n', F);
	fprintf(F, "			end case;\n");

	if(fsm_data->port_reset!=NULL) {
		fputc('\n', F);
		ftabprintf(F, 3, "-- Reset state\n");
		ftabprintf(F, 3, "if %s = '%c' then\n", fsm_data->port_reset->name, Netlist_Access_Reset_GetActiveState(fsm_data->port_reset->access));
		ftabprintf(F, 4, "-- Next state\n");
		ftabprintf(F, 4, "state_cur <= %u;\n", fsm_data->init_state->index);
		ftabprintf(F, 4, "rtmcounter <= 0;\n");
		ftabprintf(F, 4, "-- Default values for outputs\n");
		avl_pp_foreach(&fsm_data->output_values, scanOut) {
			netlist_fsm_outval_t* outval = scanOut->data;
			ftabprintf(F, 4, "%s_buf <= ", outval->port->name);
			if(outval->default_val!=NULL) {
				Netlist_GenVHDL_StdLogic_Literal(F, outval->default_val);
			}
			else {
				fprintf(F, "%s", outval->port->width==1 ? "'0'" : "(others => '0')");
			}
			fprintf(F, ";\n");
		}
		ftabprintf(F, 3, "end if;\n");
	}  // Reset

	fputc('\n', F);
	ftabprintf(F, 2, "end if;\n");
	ftabprintf(F, 1, "end process;\n");
	fputc('\n', F);
	ftabprintf(F, 2, "-- Assignment of output ports\n");
	fputc('\n', F);
	avl_pp_foreach(&fsm_data->output_values, scanOut) {
		netlist_fsm_outval_t* outval = scanOut->data;
		fprintf(F, "	%s <=  %s_buf;\n", outval->port->name, outval->port->name);
	}

	fprintf(F,
		"\n"
		"end architecture;\n"
		"\n"
	);

}
// Implementation good for synthesis and human readability (handles the two modes)
static void Netlist_Fsm_GenVHDL_OneHot_Small(FILE* F, netlist_vhdl_t* params, netlist_comp_t* comp) {
	netlist_fsm_t* fsm_data = comp->data;

	// Set the state indexes
	Netlist_Comp_Fsm_SetStateIdx(comp);

	// For each state, build the list of previous states
	if(objective_synth==true) {
		Netlist_Comp_Fsm_PrevRules_Build(comp);
	}

	// Build the retiming counters
	Netlist_Comp_Fsm_RtmCounters_Build(comp);

	if(fsm_data->rtm_counters_nb>0) {

		// Ensure that the Actions flagged as 'only last cycle' drive unbuffered outputs
		avl_pp_foreach(&fsm_data->output_values, scan) {
			netlist_fsm_outval_t* outval = scan->data;
			if(outval->is_buffered==false) continue;
			foreach(outval->actions, scanAction) {
				netlist_fsm_action_t* action = scanAction->DATA;
				if(action->only_last_cycle==false) continue;
				printf("WARNING: The FSM output '%s' is buffered but an Action is flagged \"only last cycle\".\n", outval->port->name);
				printf("  Overriding: The output is forced unbuffered.\n");
				outval->is_buffered = false;
				break;
			}
		}

	}

	// Needed for retiming, preliminary version
	fprintf(F,
		"library ieee;\n"
		"use ieee.numeric_std.all;\n"
		"\n"
	);

	if(fsm_data->rtm_counters_nb>0 && fsmretime_lut6==true) {
		fprintf(F,
			"library unisim;\n"
			"use unisim.vcomponents.all;\n"
			"\n"
		);
	}

	Netlist_GenVHDL_Entity(F, comp, params);
	fprintf(F, "\n");

	fprintf(F, "architecture %s of %s is\n", params->architecture, Netlist_Comp_GetRename_Comp(comp, params));
	fputc('\n', F);

	if(vhd2vl_friendly==false) {
		fprintf(F, "	signal state_cur  : std_logic_vector(0 to %u) := (%u => '1', others => '0');\n", fsm_data->states_nb-1, fsm_data->init_state->index);
		fprintf(F, "	signal state_next : std_logic_vector(0 to %u) := (%u => '1', others => '0');\n", fsm_data->states_nb-1, fsm_data->init_state->index);
	}
	else {
		fprintf(F, "	signal state_cur  : std_logic_vector(0 to %u) := \"", fsm_data->states_nb-1);
		for(unsigned i=0; i<fsm_data->states_nb; i++) fprintf(F, "%c", i==fsm_data->init_state->index ? '1' : '0');
		fprintf(F, "\";\n");
		fprintf(F, "	signal state_next : std_logic_vector(0 to %u);\n", fsm_data->states_nb-1);
	}

	if(fsm_data->outputs_buf==true) {
		fputc('\n', F);
		fprintf(F, "	-- Buffers for outputs\n");
		avl_pp_foreach(&fsm_data->output_values, scanOut) {
			netlist_fsm_outval_t* outval = scanOut->data;
			if(outval->is_buffered==false) continue;
			// Write the signal declaration
			fprintf(F, "	signal %s_buf : ", outval->port->name);
			Netlist_GenVHDL_StdLogicType(F, outval->port->width);
			fprintf(F, " := ");
			if(outval->default_val!=NULL) {
				Netlist_GenVHDL_StdLogic_Literal(F, outval->default_val);
			}
			else {
				fprintf(F, "%s", outval->port->width==1 ? "'0'" : "(others => '0')");
			}
			fprintf(F, ";\n");
			fprintf(F, "	signal %s_bufn : ", outval->port->name);
			Netlist_GenVHDL_StdLogicType(F, outval->port->width);
			fprintf(F, ";\n");
		}
	}

	// If retiming: Write signal declarations for counters and comparators
	if(fsm_data->rtm_counters_nb>0) {
		fputc('\n', F);
		fprintf(F, "	-- Retiming: counters\n");
		for(unsigned i=0; i<fsm_data->rtm_counters_nb; i++) {
			fprintf(F, "	signal rtmcounter%u :      unsigned(%u downto 0) := (others => '0');\n",
				i, fsm_data->rtm_counter_bitsnb-1
			);
			fprintf(F, "	signal rtmcounter%u_next : unsigned(%u downto 0);\n",
				i, fsm_data->rtm_counter_bitsnb-1
			);
		}
		fputc('\n', F);
		fprintf(F, "	-- Retiming: Output of comparators\n");
		foreach(fsm_data->states, scanState) {
			netlist_fsm_state_t* state = scanState->DATA;
			if(state->rtm_counter==NULL) continue;
			fprintf(F, "	signal rtmcmp%u : std_logic;\n", state->index);
		}
	}

	// For function calls: declare registers
	if(avl_pp_isempty(&fsm_data->func_calls)==false) {
		fputc('\n', F);
		fprintf(F, "	-- Function calls: return IDs\n");
		char buf[20];  // To build register names
		unsigned fsmfunc_idx = 0;
		avl_pp_foreach(&fsm_data->func_calls, scan) {
			netlist_fsm_func_t* fsmfunc = scan->data;
			if(fsmfunc->calls_nb < 2) continue;
			// Build the name of the register
			sprintf(buf, "funccall%u", fsmfunc_idx);
			fsmfunc->name_reg = namealloc(buf);
			fsmfunc->reg_size = uint_bitsnb(fsmfunc->calls_nb-1);
			fsmfunc_idx++;
			// Declare the register
			fprintf(F, "	signal %s :      unsigned(%u downto 0) := (others => '0');\n", fsmfunc->name_reg, fsmfunc->reg_size-1);
			if(vhd2vl_friendly==false) {
				fprintf(F, "	signal %s_next : unsigned(%u downto 0) := (others => '0');\n", fsmfunc->name_reg, fsmfunc->reg_size-1);
			}
			else {
				fprintf(F, "	signal %s_next : unsigned(%u downto 0);\n", fsmfunc->name_reg, fsmfunc->reg_size-1);
			}
		}
	}

	if(objective_synth==true) {
		fprintf(F,
			"\n"
			"	-- A utility function to convert bool to std_logic\n"
			"	function to_stdl (b: boolean) return std_logic is\n"
			"	begin\n"
			"		if b = true then\n"
			"			return '1';\n"
			"		end if;\n"
			"		return '0';\n"
			"	end function;\n"
		);
	}

	fprintfm(F,
		"\n"
		"begin\n"
		"\n"
		"	-- Sequential process\n"
		"	-- Set the current state\n"
		"\n"
		"	process (%s)\n", fsm_data->port_clock->name,
		"	begin\n"
		"		if %s(%s) then\n", Netlist_GenVHDL_ClockEdge_TestFunc_port(fsm_data->port_clock), fsm_data->port_clock->name,
		"\n"
		"			-- Next state\n"
		"			state_cur <= state_next;\n",
		NULL
	);

	if(fsm_data->outputs_buf==true) {
		ftabprintf(F, 3, "-- Buffers for outputs\n");
		avl_pp_foreach(&fsm_data->output_values, scanOut) {
			netlist_fsm_outval_t* outval = scanOut->data;
			if(outval->is_buffered==false) continue;
			ftabprintf(F, 3, "%s_buf <= %s_bufn;\n", outval->port->name, outval->port->name);
		}
	}

	if(fsm_data->rtm_counters_nb>0) {
		fprintf(F, "			-- Retiming: counters\n");
		for(unsigned i=0; i<fsm_data->rtm_counters_nb; i++) {
			fprintf(F, "			rtmcounter%u <= rtmcounter%u_next;\n", i, i);
		}
	}

	if(avl_pp_isempty(&fsm_data->func_calls)==false) {
		fprintf(F, "			-- Function calls: return IDs\n");
		avl_pp_foreach(&fsm_data->func_calls, scan) {
			netlist_fsm_func_t* fsmfunc = scan->data;
			if(fsmfunc->calls_nb < 2) continue;
			fprintf(F, "			%s <= %s_next;\n", fsmfunc->name_reg, fsmfunc->name_reg);
		}
	}

	fputc('\n', F);
	ftabprintf(F, 2, "end if;\n");
	ftabprintf(F, 1, "end process;\n");

	// Some utility functions
	void print_sym_asg(char* name, char* value) {
		fprintf(F, "%s <= ", name);
		Netlist_GenVHDL_StdLogic_Literal(F, value);
		fprintf(F, ";\n");
	}
	void print_port_asg(netlist_port_t* port, char* value) {
		print_sym_asg(port->name, value);
	}
	void print_action(netlist_fsm_action_t* action) {
		print_sym_asg(action->outval->port->name, action->value);
	}
	void print_action_bufn(netlist_fsm_action_t* action) {
		char buf[128];
		sprintf(buf, "%s_bufn", action->outval->port->name);
		print_sym_asg(buf, action->value);
	}

	// A local utility function: backward scan the rules and print the conditions
	void print_rule_backscan(netlist_fsm_rule_t* rule) {
		if(rule->father_is_rule==true) {
			netlist_fsm_rule_t* prevRule = rule->father;
			print_rule_backscan(prevRule);
			// Print the condition
			if(prevRule->condition!=NULL) {
				fprintf(F, " and ");
				if(rule==prevRule->rule_false) fprintf(F, "not ( ");
				hvex_PrintVHDL_Width1(F, prevRule->condition, false);
				if(rule==prevRule->rule_false) fprintf(F, " )");
				return;
			}
			if(prevRule->func_call!=NULL) {
				assert(prevRule->func_call->DATA_TO==rule);
				// Note: no additional condition is necessary
				return;
			}
			if(prevRule->func_rets!=NULL) {
				// Scan the list to find the right one
				netlist_fsm_func_t* fsmfunc = prevRule->func_rets->DATA_FROM;
				if(fsmfunc->calls_nb < 2) return;
				foreach(prevRule->func_rets, scan) {
					if(scan->DATA_TO!=rule) continue;
					unsigned id = scan->TYPE;
					fprintf(F, " and to_stdl(%s = %u)", fsmfunc->name_reg, id);
					return;
				}
				// This state should not be reachable
				abort();
			}
			// This state should not be reachable
			abort();
		}
		else {
			netlist_fsm_state_t* prevState = rule->father;
			if(prevState->rtm_counter!=NULL) fprintf(F, "rtmcmp%u", prevState->index);
			else fprintf(F, "state_cur(%u)", prevState->index);
		}
	}
	// Print the condition to activate the next state or an output buffer
	void print_rule_leadhere(netlist_fsm_rule_t* prevRule) {
		if(prevRule->father_is_rule==false) {
			// For this simple case, avoid printing unuseful parentheses
			netlist_fsm_state_t* prevState = prevRule->father;
			if(prevState->rtm_counter!=NULL) fprintf(F, "rtmcmp%u", prevState->index);
			else fprintf(F, "state_cur(%u)", prevState->index);
		}
		else {
			// Recursively scan the rules, print the expression from the State signal
			fprintf(F, "( ");
			print_rule_backscan(prevRule);
			fprintf(F, " )");
		}
	}
	// Print the condition to reach a state, or to stay in it
	bool print_cond_or_statebuf(netlist_fsm_state_t* state, bool first) {
		// Take into account the rtm_counter, if any
		if(state->rtm_counter!=NULL) {
			if(first==false) fprintf(F, " or ");
			fprintf(F, "(state_cur(%u) and not rtmcmp%u)", state->index, state->index);
			first = false;
		}
		// List the branch conditions of the previous states
		foreach(state->prev_rules, scanPrevRule) {
			if(first==false) fprintf(F, " or ");
			netlist_fsm_rule_t* prevRule = scanPrevRule->DATA;
			print_rule_leadhere(prevRule);
			first = false;
		}
		return first;
	}
	bool print_cond_or_statebuf_reset(netlist_fsm_state_t* state, bool first, bool act_on_rst, bool act_on_norst) {
		if(fsm_data->port_reset!=NULL) {
			char c = Netlist_Access_Reset_GetActiveState(fsm_data->port_reset->access);
			if(act_on_rst==true && fsm_data->init_state==state) {
				if(first==false) fprintf(F, " or ");
				// Here the expression is: reset signal == reset active state
				if(c=='1') fprintf(F, "%s", fsm_data->port_reset->name);
				else fprintf(F, "(not %s)", fsm_data->port_reset->name);
				first = false;
			}
			if(act_on_norst==true && fsm_data->init_state!=state) {
				if(first==false) fprintf(F, " or ");
				// Here the expression is: reset signal != reset active state
				if(c=='0') fprintf(F, "%s", fsm_data->port_reset->name);
				else fprintf(F, "(not %s)", fsm_data->port_reset->name);
				first = false;
			}
		}
		first = print_cond_or_statebuf(state, first);
		return first;
	}

	// Begin VHDL style for synthesis
	if(objective_synth==true) {

		if(fsm_data->rtm_counters_nb>0) {
			fprintf(F,
				"\n"
				"	-- Retiming: the counters\n"
				"\n"
			);
			foreach(fsm_data->rtm_counters, scanCounter) {
				netlist_fsm_rtmcnt_t* counter = scanCounter->DATA;
				fprintf(F, "	rtmcounter%u_next <= rtmcounter%u + 1 when", counter->index, counter->index);
				if(fsm_data->port_reset!=NULL) {
					char c = Netlist_Access_Reset_GetActiveState(fsm_data->port_reset->access);
					fprintf(F, " (%s /= '%c') and (", fsm_data->port_reset->name, c);
				}
				fprintf(F, "\n");
				unsigned max_per_line = 4;
				unsigned cnt_per_line = 0;
				unsigned count = 0;
				foreach(counter->states, scanState) {
					netlist_fsm_state_t* state = scanState->DATA;
					if(cnt_per_line==max_per_line) { fprintf(F, " or\n"); cnt_per_line = 0; }
					else if(cnt_per_line>0) fprintf(F, " or ");
					if(cnt_per_line==0) fprintf(F, "		");
					fprintf(F, "(state_cur(%u) = '1' and rtmcmp%u = '0')", state->index, state->index);
					cnt_per_line++;
					count++;
				}
				if(count==0) fprintf(F, "false");  // In case the FSM is not simplified enough
				fprintf(F, "\n		");
				if(fsm_data->port_reset!=NULL) fprintf(F, ") ");
				fprintf(F, "else (others => '0');\n");
			}
		}

		if(avl_pp_isempty(&fsm_data->func_calls)==false) {
			fprintf(F,
				"\n"
				"	-- Function calls: The call IDs\n"
				"\n"
			);
			// FIXME This code is not efficient: for each function, all FSM branch rules are rescanned
			avl_pp_foreach(&fsm_data->func_calls, scan) {
				netlist_fsm_func_t* fsmfunc = scan->data;
				if(fsmfunc->calls_nb < 2) continue;
				fprintf(F, "	-- Function '%s'\n", fsmfunc->name_func);
				fprintf(F, "	%s_next <=\n", fsmfunc->name_reg);

				// To recursively scan the rules
				void scanrule(netlist_fsm_rule_t* rule) {
					if(rule==NULL) return;
					if(rule->func_call!=NULL) {
						netlist_fsm_func_t* fsmfunc_loc = rule->func_call->DATA_FROM;
						if(fsmfunc_loc==fsmfunc) {
							unsigned id = rule->func_call->TYPE;
							fprintf(F, "		to_unsigned(%u, %u) when ", id, fsmfunc->reg_size);
							print_rule_leadhere(rule);
							fprintf(F, " = '1' else\n");
						}
					}
					if(rule->condition!=NULL) {
						// Scan the branches
						scanrule(rule->rule_true);
						scanrule(rule->rule_false);
					}
					// Scan the calls/returns
					foreach(rule->func_call, scan) scanrule(scan->DATA_TO);
					foreach(rule->func_rets, scan) scanrule(scan->DATA_TO);
				}

				// Scan all states and all rules to find where calls are made
				foreach(fsm_data->states, scanState) {
					netlist_fsm_state_t* state = scanState->DATA;
					scanrule(state->rules);
				}

				// The value for the last ELSE: the register must not change
				fprintf(F, "		%s;\n", fsmfunc->name_reg);
			}
		}

		fprintf(F,
			"\n"
			"	-- Next state bits\n"
			"\n"
		);
		foreach(fsm_data->states, scanState) {
			netlist_fsm_state_t* state = scanState->DATA;
			fprintf(F, "	state_next(%u) <= ", state->index);
			if(fsm_data->port_reset!=NULL) {
				char c = Netlist_Access_Reset_GetActiveState(fsm_data->port_reset->access);
				if(state==fsm_data->init_state) {
					// Here the expression is: reset signal == reset active state
					if(c=='1') fprintf(F, "%s", fsm_data->port_reset->name);
					else fprintf(F, "(not %s)", fsm_data->port_reset->name);
					fprintf(F, " or ( ");
				}
				else {
					// Here the expression is: reset signal != reset active state
					if(c=='0') fprintf(F, "%s", fsm_data->port_reset->name);
					else fprintf(F, "(not %s)", fsm_data->port_reset->name);
					fprintf(F, " and ( ");
				}
			}
			bool first = true;
			first = print_cond_or_statebuf(state, first);
			// In case there is no branch condition leading there (reset state, for example)
			if(first==true) fprintf(F, "'0'");
			// Write the closing parenthese of the test for reset
			if(fsm_data->port_reset!=NULL) fprintf(F, " )");
			fprintf(F, ";\n");
		}

		fprintf(F,
			"\n"
			"	-- Assignment of buffers for buffered outputs\n"
			"\n"
		);
		avl_pp_foreach(&fsm_data->output_values, scanOut) {
			netlist_fsm_outval_t* outval = scanOut->data;
			if(outval->is_buffered==false) continue;
			if(outval->port->width==1) {
				// Default value is 1, the Actions set 0. The logic is a big NOR.
				if(outval->default_val[0]=='1') {
					fprintf(F, "	%s_bufn <= not ( ", outval->port->name);
					bool first = true;
					foreach(outval->actions, scanAction) {
						netlist_fsm_action_t* action = scanAction->DATA;
						if(action->value[0]!='0') continue;  // In case the FSM has not been simplified
						// Print the condition to reach this state, or to stay here
						first = print_cond_or_statebuf_reset(action->state, first, true, false);
					}
					if(first==true) fprintf(F, "'0'");  // In case the FSM has not been simplified
					fprintf(F, " );\n");
				}
				// Default value is 0 (or -), the Actions set 1. The logic is a big OR.
				else {
					fprintf(F, "	%s_bufn <= ", outval->port->name);
					bool first = true;
					foreach(outval->actions, scanAction) {
						netlist_fsm_action_t* action = scanAction->DATA;
						if(action->value[0]!='1') continue;  // In case the FSM has not been simplified
						// Print the condition to reach this state, or to stay here
						first = print_cond_or_statebuf_reset(action->state, first, true, false);
					}
					if(first==true) fprintf(F, "'0'");  // In case the FSM has not been simplified
					fprintf(F, ";\n");
				}
			}
			else {  // Width > 1
				fprintf(F, "	%s_bufn <= \n", outval->port->name);
				bool first = true;
				foreach(outval->actions, scanAction) {
					netlist_fsm_action_t* action = scanAction->DATA;
					fprintf(F, "		");
					if(first==false) fprintf(F, "else ");
					fprintf(F, "\"%s\" when ", action->value);
					// Print the condition to reach this state, or to stay here
					bool first_or = true;
					first_or = print_cond_or_statebuf_reset(action->state, first_or, true, false);
					if(first_or==true) fprintf(F, "false");  // In case the FSM has not been simplified
					first = false;
				}
				fprintf(F, "		");
				if(first==false) fprintf(F, "else ");
				fprintf(F, "\"%s\";\n", outval->default_val);
			}
		}

		fprintf(F,
			"\n"
			"	-- Assignment of non-buffered outputs\n"
			"\n"
		);
		avl_pp_foreach(&fsm_data->output_values, scanOut) {
			netlist_fsm_outval_t* outval = scanOut->data;
			if(outval->is_buffered==true) continue;
			if(outval->port->width==1) {
				// Case where the logic is a big NOR
				if(outval->default_val[0]=='1') {
					fprintf(F, "	%s <= not ( \n", outval->port->name);
					unsigned max_per_line = 8;
					unsigned cnt_per_line = 0;
					unsigned count = 0;
					foreach(outval->actions, scanAction) {
						netlist_fsm_action_t* action = scanAction->DATA;
						if(action->value[0]!='0') continue;  // Paranoia
						if(cnt_per_line==max_per_line) { fprintf(F, " or\n"); cnt_per_line = 0; }
						else if(cnt_per_line>0) fprintf(F, " or ");
						if(cnt_per_line==0) fprintf(F, "		");
						if(action->only_last_cycle==true && action->state->rtm_counter!=NULL) fprintf(F, "rtmcmp%u", action->state->index);
						else fprintf(F, "state_cur(%u)", action->state->index);
						cnt_per_line++;
						count++;
					}
					if(count==0) fprintf(F, "'0'");  // In case the FSM has not been simplified
					fprintf(F, " );\n");
				}
				// Otherwise, implement a big OR
				else {
					fprintf(F, "	%s <=\n", outval->port->name);
					unsigned max_per_line = 8;
					unsigned cnt_per_line = 0;
					unsigned count = 0;
					foreach(outval->actions, scanAction) {
						netlist_fsm_action_t* action = scanAction->DATA;
						if(action->value[0]!='1') continue;  // Paranoia
						if(cnt_per_line==max_per_line) { fprintf(F, " or\n"); cnt_per_line = 0; }
						else if(cnt_per_line>0) fprintf(F, " or ");
						if(cnt_per_line==0) fprintf(F, "		");
						if(action->only_last_cycle==true && action->state->rtm_counter!=NULL) fprintf(F, "rtmcmp%u", action->state->index);
						else fprintf(F, "state_cur(%u)", action->state->index);
						cnt_per_line++;
						count++;
					}
					if(count==0) fprintf(F, "'0'");  // In case the FSM has not been simplified
					fprintf(F, ";\n");
				}
			}
			else {  // Width > 1
				fprintf(F, "	%s <= \n", outval->port->name);
				unsigned count = 0;
				foreach(outval->actions, scanAction) {
					netlist_fsm_action_t* action = scanAction->DATA;
					fprintf(F, "		");
					if(count>0) fprintf(F, "else ");
					fprintf(F, "\"%s\" when ", action->value);
					if(action->only_last_cycle==true && action->state->rtm_counter!=NULL) fprintf(F, "rtmcmp%u", action->state->index);
					else fprintf(F, "state_cur(%u)", action->state->index);
					fprintf(F, " = '1'\n");
					count++;
				}
				fprintf(F, "		else \"%s\";\n", outval->default_val);
			}
		}

	}  // End structural VHDL style

	// Begin VHDL style for human readability
	else {
		fprintf(F,
			"\n"
			"	-- Combinatorial process\n"
			"	-- Compute the next state\n"
			"	-- Compute the outputs\n"
			"\n"
		);

		fprintf(F, "	process (\n");
		if(vhd2vl_friendly==false) {
			fprintf(F, "		-- Inputs of the FSM\n");
		}
		bool insert_tabs = true;
		bool insert_comma = false;
		// Add the input ports to the sensitivity list
		avl_pp_foreach(&comp->ports, scanPort) {
			netlist_port_t* port = scanPort->data;
			if(port->direction!=NETLIST_PORT_DIR_IN) continue;
			if(port->access!=NULL && port->access->model==NETLIST_ACCESS_CLOCK) continue;
			if(insert_tabs==true) { fprintf(F, "		"); insert_tabs = false; }
			if(insert_comma==true) fprintf(F, ", ");
			insert_comma = true;
			fprintf(F, "%s", port->name);
		}
		if(insert_comma==true) fprintf(F, ",\n");
		insert_tabs = true;
		insert_comma = false;
		// Add the outputs of retiming comparators to the sensitivity list
		if(fsm_data->rtm_counters_nb>0) {
			if(vhd2vl_friendly==false) {
				fprintf(F, "		-- Retiming: outputs of the comparators\n");
			}
			foreach(fsm_data->states, scanState) {
				netlist_fsm_state_t* state = scanState->DATA;
				if(state->rtm_counter==NULL) continue;
				if(insert_tabs==true) { fprintf(F, "		"); insert_tabs = false; }
				if(insert_comma==true) fprintf(F, ", ");
				insert_comma = true;
				fprintf(F, "rtmcmp%u", state->index);
			}
			if(insert_comma==true) fprintf(F, ",\n");
			insert_tabs = true;
			insert_comma = false;
			if(vhd2vl_friendly==false) {
				fprintf(F, "		-- Retiming: the counters\n");
			}
			for(unsigned i=0; i<fsm_data->rtm_counters_nb; i++) {
				if(insert_tabs==true) { fprintf(F, "		"); insert_tabs = false; }
				if(insert_comma==true) fprintf(F, ", ");
				insert_comma = true;
				fprintf(F, "rtmcounter%u", i);
			}
			if(insert_comma==true) fprintf(F, ",\n");
			insert_tabs = true;
			insert_comma = false;
		}
		// Add the IDs for function calls
		if(avl_pp_isempty(&fsm_data->func_calls)==false) {
			if(vhd2vl_friendly==false) {
				fprintf(F, "		-- Function calls: return IDs\n");
			}
			avl_pp_foreach(&fsm_data->func_calls, scan) {
				netlist_fsm_func_t* fsmfunc = scan->data;
				if(fsmfunc->calls_nb < 2) continue;
				if(insert_tabs==true) { fprintf(F, "		"); insert_tabs = false; }
				if(insert_comma==true) fprintf(F, ", ");
				insert_comma = true;
				fprintf(F, "%s", fsmfunc->name_reg);
			}
			if(insert_comma==true) fprintf(F, ",\n");
			insert_tabs = true;
			insert_comma = false;
		}
		// Add the current state to the sensitivity list
		if(vhd2vl_friendly==false) {
			fprintf(F, "		-- Current state\n");
		}
		fprintf(F,
			"		state_cur\n"
			"	)\n"
			"	begin\n"
			"\n"
			"		-- Reset the next state value\n"
			"\n"
			"		state_next <= (others => '0');\n"
		);

		fputc('\n', F);
		ftabprintf(F, 2, "-- Default value to the outputs or output buffers\n");
		fputc('\n', F);
		avl_pp_foreach(&fsm_data->output_values, scanOut) {
			netlist_fsm_outval_t* outval = scanOut->data;
			if(outval->is_buffered==true) {
				fprintf(F, "		%s_bufn <= ", outval->port->name);
			}
			else {
				fprintf(F, "		%s <= ", outval->port->name);
			}
			// Write the expression
			if(outval->default_val!=NULL) {
				Netlist_GenVHDL_StdLogic_Literal(F, outval->default_val);
			}
			else {
				fprintf(F, "%s", outval->port->width==1 ? "'0'" : "(others => '0')");
			}
			fprintf(F, ";\n");
		}

		if(fsm_data->rtm_counters_nb>0) {
			fputc('\n', F);
			fprintf(F, "		-- Retiming: default value for counters\n");
			for(unsigned i=0; i<fsm_data->rtm_counters_nb; i++) {
				fprintf(F, "		rtmcounter%u_next <= (others => '0');\n", i);
			}
		}

		if(avl_pp_isempty(&fsm_data->func_calls)==false) {
			fputc('\n', F);
			fprintf(F, "		-- Function calls: default values (no change)\n");
			avl_pp_foreach(&fsm_data->func_calls, scan) {
				netlist_fsm_func_t* fsmfunc = scan->data;
				if(fsmfunc->calls_nb < 2) continue;
				fprintf(F, "		%s_next <= %s;\n", fsmfunc->name_reg, fsmfunc->name_reg);
			}
		}

		fputc('\n', F);
		ftabprintf(F, 2, "-- For all states, compute the next state bits\n");
		ftabprintf(F, 2, "--   And the outputs, and the next value for buffered outputs\n");
		fputc('\n', F);

		foreach(fsm_data->states, scanState) {
			netlist_fsm_state_t* state = scanState->DATA;
			if(state==fsm_data->init_state) {
				ftabprintf(F, 2, "-- Info: This is the init/reset state\n");
			}
			ftabprintf(F, 2, "if state_cur(%d) = '1' then\n", state->index);

			void print_state_cond(netlist_fsm_rule_t* rule, unsigned tabs) {
				if(rule==NULL) return;
				if(rule->state!=NULL) {
					ftabprintf(F, tabs, "state_next(%d) <= '1';\n", rule->state->index);
					if(fsm_data->outputs_buf==true) {
						ftabprintf(F, tabs, "-- Next values for buffered outputs\n");
						foreach(rule->state->actions, scanAction) {
							netlist_fsm_action_t* action = scanAction->DATA;
							if(action->outval->is_buffered==false) continue;
							ftabprint(F, tabs); print_action_bufn(action);
						}
					}
					return;
				}
				if(rule->condition!=NULL) {
					// Print the condition
					ftabprintf(F, tabs, "if ");
					hvex_PrintVHDL_cond(F, rule->condition, false);
					// Print what to do
					fprintf(F, " then\n");
					print_state_cond(rule->rule_true, tabs+1);
					ftabprintf(F, tabs, "else\n");
					print_state_cond(rule->rule_false, tabs+1);
					ftabprintf(F, tabs, "end if;\n");
					return;
				}
				if(rule->func_call!=NULL) {
					// This is a function call
					// Note: no need to check the end of the state to store the call ID
					netlist_fsm_func_t* fsmfunc = rule->func_call->DATA_FROM;
					unsigned id = rule->func_call->TYPE;
					ftabprintf(F, tabs, "-- Function call: %s\n", fsmfunc->name_func);
					if(fsmfunc->calls_nb >= 2) {
						ftabprintf(F, tabs, "-- Save the origin of the call\n");
						ftabprintf(F, tabs, "%s_next <= to_unsigned(%u, %u);\n", fsmfunc->name_reg, id, fsmfunc->reg_size);
						ftabprintf(F, tabs, "-- This is where the function call leads\n");
					}
					print_state_cond(rule->func_call->DATA_TO, tabs);
					return;
				}
				if(rule->func_rets!=NULL) {
					// This is a function return
					netlist_fsm_func_t* fsmfunc = rule->func_rets->DATA_FROM;
					if(fsmfunc->calls_nb < 2) {
						ftabprintf(F, tabs, "-- Return from function: %s\n", fsmfunc->name_func);
						print_state_cond(rule->func_rets->DATA_TO, tabs);
					}
					else {
						foreach(rule->func_rets, scan) {
							unsigned id = scan->TYPE;
							ftabprintf(F, tabs, "-- Return from function: %s\n", fsmfunc->name_func);
							ftabprintf(F, tabs, "if %s = %u then\n", fsmfunc->name_reg, id);
							print_state_cond(scan->DATA_TO, tabs+1);
							ftabprintf(F, tabs, "end if;\n");
						}
					}
					return;
				}
				// Paranoia?
				assert(rule->rule_true!=NULL);
				print_state_cond(rule->rule_true, tabs);
			}

			// When there is a counter
			if(state->rtm_counter!=NULL) {
				ftabprintf(F, 3, "if rtmcmp%u = '1' then\n", state->index);
				ftabprintf(F, 4, "-- Next state\n");
				print_state_cond(state->rules, 4);
				ftabprintf(F, 4, "-- Last cycle of current state: assignment of non-buffered outputs\n");
				foreach(state->actions, scanAction) {
					netlist_fsm_action_t* action = scanAction->DATA;
					if(action->only_last_cycle==false) continue;
					ftabprint(F, 4); print_action(action);
				}
				ftabprintf(F, 3, "else  -- Stay in the current state\n");
				ftabprintf(F, 4, "state_next(%u) <= '1';\n", state->index);
				ftabprintf(F, 4, "rtmcounter%u_next <= rtmcounter%u + 1;\n", state->rtm_counter->index, state->rtm_counter->index);
				ftabprintf(F, 4, "-- Maintain buffered outputs\n");
				foreach(state->actions, scanAction) {
					netlist_fsm_action_t* action = scanAction->DATA;
					if(action->outval->is_buffered==false) continue;
					ftabprint(F, 4); print_action_bufn(action);
				}
				ftabprintf(F, 3, "end if;\n");
				ftabprintf(F, 3, "-- Assignment of non-buffered outputs;\n");
				foreach(state->actions, scanAction) {
					netlist_fsm_action_t* action = scanAction->DATA;
					if(action->outval->is_buffered==true) continue;
					if(action->only_last_cycle==true) continue;
					ftabprint(F, 4); print_action(action);
				}
			}
			// No counter
			else {
				ftabprintf(F, 3, "-- Next state\n");
				print_state_cond(state->rules, 3);
				if(fsm_data->outputs_buf==true) {
					ftabprintf(F, 3, "-- Assignment of non-buffered outputs\n");
				}
				else {
					ftabprintf(F, 3, "-- Assignment of outputs\n");
				}
				foreach(state->actions, scanAction) {
					netlist_fsm_action_t* action = scanAction->DATA;
					if(action->outval->is_buffered==true) continue;
					ftabprint(F, 3); print_action(action);
				}
			}

			ftabprintf(F, 2, "end if;\n");
			fputc('\n', F);
		}  // Scan all states, generate next state and outputs.

		if(fsm_data->port_reset!=NULL) {
			ftabprintf(F, 2, "-- Reset input\n");
			ftabprintf(F, 2, "if %s = '%c' then\n", fsm_data->port_reset->name, Netlist_Access_Reset_GetActiveState(fsm_data->port_reset->access));
			ftabprintf(F, 3, "-- Set the reset state\n");
			if(vhd2vl_friendly==false) {
				ftabprintf(F, 3, "state_next <= (%u => '1', others => '0');\n", fsm_data->init_state->index);
			}
			else {
				ftabprintf(F, 3, "state_next <= \"");
				for(unsigned i=0; i<fsm_data->states_nb; i++) fprintf(F, "%c", i==fsm_data->init_state->index ? '1' : '0');
				fprintf(F, "\";\n");
			}
			if(fsm_data->outputs_buf==true) {
				ftabprintf(F, 3, "-- Note: Resetting all buffers for outputs here is not necessary.\n");
				ftabprintf(F, 3, "-- It would cost hardware. They will be reset at the next clock front.\n");
				#if 0
				ftabprintf(F, 3, "-- Buffers for outputs\n");
				avl_pp_foreach(&fsm_data->output_values, scanOut) {
					netlist_fsm_outval_t* outval = scanOut->data;
					if(outval->is_buffered==false) continue;
					ftabprintf(F, 3, "%s_bufn <= ", outval->port->name);
					if(outval->default_val!=NULL) {
						Netlist_GenVHDL_StdLogic_Literal(F, outval->default_val);
					}
					else {
						fprintf(F, "%s", outval->port->width==1 ? "'0'" : "(others => '0')");
					}
					fprintf(F, ";\n");
				}
				#endif
			}
			if(fsm_data->rtm_counters_nb>0) {
				fprintf(F, "			-- Retiming: counters\n");
				for(unsigned i=0; i<fsm_data->rtm_counters_nb; i++) {
					fprintf(F, "			rtmcounter%u_next <= (others => '0');\n", i);
				}
			}
			if(fsm_data->outputs_buf==true) {
				fprintf(F, "			-- Reset state: set the buffered outputs\n");
				foreach(fsm_data->init_state->actions, scanAction) {
					netlist_fsm_action_t* action = scanAction->DATA;
					if(action->outval->is_buffered==false) continue;
					ftabprint(F, 3); print_action_bufn(action);
				}
			}
			ftabprintf(F, 2, "end if;\n");
			fputc('\n', F);
		}  // Reset

		ftabprintf(F, 1, "end process;\n");
	}  // Behavioural VHDL style

	if(fsm_data->outputs_buf==true) {
		fputc('\n', F);
		fprintf(F, "	-- Assignment of buffered outputs\n");
		fputc('\n', F);
		avl_pp_foreach(&fsm_data->output_values, scanOut) {
			netlist_fsm_outval_t* outval = scanOut->data;
			if(outval->is_buffered==false) continue;
			fprintf(F, "	%s <= %s_buf;\n", outval->port->name, outval->port->name);
		}
	}

	if(fsm_data->rtm_counters_nb>0) {
		fputc('\n', F);
		fprintf(F, "	-- Retiming: the comparators\n");
		fputc('\n', F);
		foreach(fsm_data->states, scanState) {
			netlist_fsm_state_t* state = scanState->DATA;
			if(state->rtm_counter==NULL) continue;

			if(fsmretime_lut6==true) {
				// Compute the LUT6 configuration
				char buf[17];  // 64 bits => 16 hex characters
				char* ptr = buf;
				unsigned bitsnb_1 = 32 - (state->cycles_nb - 1);
				unsigned bitsnb_0 = 64 - bitsnb_1;
				for( ;  bitsnb_1>=4; bitsnb_1 -= 4) *(ptr++) = 'F';
				if(bitsnb_1>0) {
					char c = '0';
					switch(bitsnb_1) {
						case 1 : c = '8'; break;
						case 2 : c = 'C'; break;
						case 3 : c = 'E'; break;
					}
					bitsnb_0 -= (4-bitsnb_1);
					bitsnb_1 = 0;
					*(ptr++) = c;
				}
				assert(bitsnb_0>=32 && bitsnb_0<64 && bitsnb_0%4==0);
				for( ; bitsnb_0!=0; bitsnb_0 -= 4) *(ptr++) = '0';
				// The final null character
				*ptr = 0;

				unsigned counter_idx = state->rtm_counter->index;

				// Write the LUT6 instantiation
				fprintfm(F,
					"	-- State %u: duration %u cycles\n", state->index, state->cycles_nb,
					"	rtmcmp_i%u : lut6\n", state->index,
					"	generic map (\n"
					"		INIT => X\"%s\")\n", buf,
					"	port map (\n"
					"		o =>  rtmcmp%u,\n", state->index,
					"		i0 => rtmcounter%u(0),\n", counter_idx,
					"		i1 => rtmcounter%u(1),\n", counter_idx,
					"		i2 => rtmcounter%u(2),\n", counter_idx,
					"		i3 => rtmcounter%u(3),\n", counter_idx,
					"		i4 => rtmcounter%u(4),\n", counter_idx,
					"		i5 => state_cur(%u)\n", state->index,
					"	);\n",
					NULL
				);
			}
			else {
				fprintf(F, "	rtmcmp%u <= '1' when state_cur(%u) = '1' and rtmcounter%u = %u else '0';\n",
					state->index, state->index, state->rtm_counter->index, state->cycles_nb - 1
				);
			}

		}
	}

	fprintf(F,
		"\n"
		"end architecture;\n"
		"\n"
	);
}
// Choose between implementations
static void Netlist_Fsm_GenVHDL(FILE* F, netlist_vhdl_t* params, netlist_comp_t* comp) {
	if(params->objective_simu==true) {
		Netlist_Fsm_GenVHDL_Simu(F, params, comp);
	}
	else {
		Netlist_Fsm_GenVHDL_OneHot_Small(F, params, comp);
	}
}

netlist_comp_model_t* Netlist_Comp_Fsm_GetModel() {
	netlist_comp_model_t* model = Netlist_CompModel_New();
	model->name = namealloc("fsm");

	model->data_size         = sizeof(netlist_fsm_t);
	model->func_free_data    = Netlist_Comp_Fsm_Free_Data;
	model->func_dup_internal = Netlist_Comp_Fsm_Dup;
	model->func_gen_vhdl     = Netlist_Fsm_GenVHDL;

	model->func_simp         = (netlist_comp_cb_simp_t)Netlist_Simp_Comp_Fsm;

	NETLIST_COMP_FSM = model;
	return model;
}



//===================================================
// MUX functions
//===================================================

netlist_comp_model_t* NETLIST_COMP_MUX = NULL;

netlist_access_model_t* NETLIST_ACCESS_MUXIN = NULL;

netlist_comp_t* Netlist_Mux_New(char* name, unsigned width) {
	netlist_comp_t* comp = Netlist_Comp_New(NETLIST_COMP_MUX);
	comp->name = name;

	netlist_mux_t* comp_mux = comp->data;

	comp_mux->width = width;
	comp_mux->inputs_nb = 0;

	char* port_name = namealloc("data_out");
	comp_mux->port_out = Netlist_Comp_AddPort_Out(comp, port_name, width);

	return comp;
}

static void Netlist_Mux_DataDup(avl_pp_tree_t* tree_old2new, netlist_comp_t* comp, netlist_comp_t* newcomp) {
	netlist_mux_t* mux_data = comp->data;
	netlist_mux_t* newmux_data = newcomp->data;

	newmux_data->width     = mux_data->width;
	newmux_data->inputs_nb = mux_data->inputs_nb;

	void* pointer_old2new(void* oldptr) {
		void* newptr = NULL;
		avl_pp_find_data(tree_old2new, oldptr, &newptr);
		return newptr;
	}

	newmux_data->port_out = pointer_old2new(mux_data->port_out);
}


netlist_access_t* Netlist_Mux_AddInput(netlist_comp_t* comp) {
	netlist_mux_t* comp_mux = comp->data;

	char buffer[200];
	char* name_alloc;
	netlist_access_t* access;

	sprintf(buffer, "in%d", comp_mux->input_name_idx);
	name_alloc = namealloc(buffer);
	access = Netlist_Comp_AddAccess(comp, NETLIST_ACCESS_MUXIN, name_alloc);

	netlist_mux_access_t* access_mux = access->data;
	access_mux->index = comp_mux->input_name_idx;
	access_mux->is_default = false;

	sprintf(buffer, "%s_data", access->name);
	name_alloc = namealloc(buffer);
	access_mux->port_data = Netlist_Comp_AddPort_In(comp, name_alloc, comp_mux->width);
	access_mux->port_data->access = access;

	sprintf(buffer, "%s_en", access->name);
	name_alloc = namealloc(buffer);
	access_mux->port_en = Netlist_Comp_AddPort_In(comp, name_alloc, 1);
	access_mux->port_en->access = access;

	comp_mux->input_name_idx++;
	comp_mux->inputs_nb++;
	return access;
}
void Netlist_Mux_RemoveInput(netlist_access_t* access) {
	netlist_mux_t* comp_mux = access->component->data;
	netlist_mux_access_t* muxin_data = access->data;

	// Remove the ports
	Netlist_Port_Free(muxin_data->port_data);
	if(muxin_data->port_en!=NULL) Netlist_Port_Free(muxin_data->port_en);

	// Reset the default input field
	if(access==comp_mux->default_input) comp_mux->default_input = NULL;

	// Free the access data
	Netlist_Access_Free(access);

	comp_mux->inputs_nb--;
}
// WARNING for these two functions the width of the valcat MUST be correct
netlist_access_t* Netlist_Mux_GetInput(netlist_comp_t* comp, chain_list* valcat) {
	avl_pp_foreach(&comp->accesses, scanAccess) {
		netlist_access_t* access = scanAccess->data;
		netlist_mux_access_t* access_data = access->data;
		bool b = Netlist_ValCat_EqList(access_data->port_data->source, valcat);
		if(b==true) return access;
	}
	return NULL;
}
netlist_access_t* Netlist_Mux_GetAddInput(netlist_comp_t* comp, chain_list* valcat) {
	netlist_access_t* access = Netlist_Mux_GetInput(comp, valcat);
	if(access==NULL) {
		access = Netlist_Mux_AddInput(comp);
		netlist_mux_access_t* muxIn = access->data;
		muxIn->port_data->source = Netlist_ValCat_DupList(valcat);
	}
	return access;
}

void Netlist_Mux_SetDefaultInput(netlist_comp_t* comp, netlist_access_t* access) {
	netlist_mux_t* comp_mux = comp->data;
	netlist_mux_access_t* muxIn;
	if(comp_mux->default_input!=NULL) {
		if(comp_mux->default_input==access) return;
		muxIn = comp_mux->default_input->data;
		muxIn->is_default = false;
	}
	if(access!=NULL) {
		muxIn = access->data;
		muxIn->is_default = true;
		comp_mux->default_value = NULL;
	}
	comp_mux->default_input = access;
}
void Netlist_Mux_SetDefaultValue(netlist_comp_t* comp, char* value) {
	netlist_mux_t* comp_mux = comp->data;
	if(value!=NULL && comp_mux->default_input!=NULL) {
		netlist_mux_access_t* muxIn = comp_mux->default_input->data;
		muxIn->is_default = false;
		comp_mux->default_input = NULL;
	}
	comp_mux->default_value = value;
}

netlist_access_model_t* Netlist_Access_MuxIn_GetModel() {
	netlist_access_model_t* model = Netlist_AccessModel_New();
	model->name = namealloc("mux_in");

	model->data_size = sizeof(netlist_mux_access_t);

	char* port_name = NULL;
	port_name = namealloc("data");
	Netlist_AccessModel_Init_PortName(model, netlist_mux_access_t, port_data, port_name);
	port_name = namealloc("en");
	Netlist_AccessModel_Init_PortName(model, netlist_mux_access_t, port_en, port_name);

	NETLIST_ACCESS_MUXIN = model;
	return model;
}

// VHDL generation

// Case only zero or one input (default or not), or only a default value
static void Netlist_Mux_GenVHDL_AsWire(FILE* F, netlist_vhdl_t* params, netlist_comp_t* comp) {
	Netlist_GenVHDL_Entity(F, comp, params);
	fprintf(F, "\n");

	netlist_mux_t* mux_data = comp->data;

	fprintf(F, "architecture %s of %s is\n", params->architecture, Netlist_Comp_GetRename_Comp(comp, params));
	fprintf(F,
		"\n"
		"begin\n"
		"\n"
	);

	ftabprint(F, 1);
	if(mux_data->inputs_nb>0) {
		netlist_access_t* access = avl_pp_root_data(&comp->accesses);
		netlist_mux_access_t* muxIn = access->data;
		fprintf(F, "%s <= %s;\n", mux_data->port_out->name, muxIn->port_data->name);
	}
	else if(mux_data->default_value==NULL) {
		fprintf(F, "%s <= ", mux_data->port_out->name);
		Netlist_GenVHDL_StdLogic_Literal(F, mux_data->default_value);
		fprintf(F, ";\n");
	}
	else {
		if(vhd2vl_friendly==false) {
			fprintf(F, "%s <= (others => '-');\n", mux_data->port_out->name);
		}
		else {
			fprintf(F, "%s <= (others => '0');\n", mux_data->port_out->name);
		}
		printf("WARNING : Multiplexer '%s' has zero input and no default value.\n", comp->name);
	}

	fprintf(F,
		"\n"
		"end architecture;\n"
	);
}
// Case at least one non-default input
static void Netlist_Mux_GenVHDL_AsOr(FILE* F, netlist_vhdl_t* params, netlist_comp_t* comp) {
	netlist_mux_t* mux_data = comp->data;

	// If a default input/value is given, check if it is zero of don't care
	bool default_is_zero = false;
	if(mux_data->default_input!=NULL) {
		netlist_mux_access_t* muxIn = mux_data->default_input->data;
		default_is_zero = Netlist_ValCat_ListIsLitNoBit(muxIn->port_data->source, '1');
	}
	if(mux_data->default_value!=NULL) {
		default_is_zero = Netlist_Literal_IsNoBit(mux_data->default_value, '1');
	}

	// Generate the component

	Netlist_GenVHDL_Entity(F, comp, params);

	fprintf(F, "\n");
	fprintf(F, "architecture %s of %s is\n", params->architecture, Netlist_Comp_GetRename_Comp(comp, params));
	fprintf(F, "\n");

	avl_pp_foreach(&comp->accesses, scanAccess) {
		netlist_access_t* access = scanAccess->data;
		netlist_mux_access_t* muxIn = access->data;
		if(muxIn->is_default==true) continue;
		ftabprintf(F, 1, "signal tmp%u : ", muxIn->index);
		Netlist_GenVHDL_StdLogicType_be(F, mux_data->width, NULL, ";\n");
	}
	ftabprint(F, 1);
	Netlist_GenVHDL_StdLogicType_be(F, mux_data->width, "signal tmp_or_data : ", ";\n");
	if( (mux_data->default_input!=NULL || mux_data->default_value!=NULL) && default_is_zero==false ) {
		ftabprint(F, 1);
		Netlist_GenVHDL_StdLogicType_be(F, 1, "signal tmp_or_en : ", ";\n");
	}

	fprintf(F,
		"\n"
		"begin\n"
		"\n"
	);

	avl_pp_foreach(&comp->accesses, scanAccess) {
		netlist_access_t* access = scanAccess->data;
		netlist_mux_access_t* muxIn = access->data;
		if(muxIn->is_default==true) continue;
		ftabprint(F, 1);
		if(mux_data->width==1) {
			fprintf(F, "tmp%d <= %s when %s = '1' else '0';\n",
				muxIn->index, muxIn->port_data->name, muxIn->port_en->name
			);
		}
		else {
			fprintf(F, "tmp%d <= %s when %s = '1' else (others => '0');\n",
				muxIn->index, muxIn->port_data->name, muxIn->port_en->name
			);
		}
	}

	ftabprintf(F, 1, "tmp_or_data <= ");
	bool first = true;
	avl_pp_foreach(&comp->accesses, scanAccess) {
		netlist_access_t* access = scanAccess->data;
		netlist_mux_access_t* muxIn = access->data;
		if(muxIn->is_default==true) continue;
		if(first==false) fprintf(F, " or "); else first = false;
		fprintf(F, "tmp%u", muxIn->index);
	}
	fprintf(F, ";\n");

	if( (mux_data->default_input!=NULL || mux_data->default_value!=NULL) && default_is_zero==false ) {
		ftabprintf(F, 1, "tmp_or_en <= ");
		bool first = true;
		avl_pp_foreach(&comp->accesses, scanAccess) {
			netlist_access_t* access = scanAccess->data;
			netlist_mux_access_t* muxIn = access->data;
			if(muxIn->is_default==true) continue;
			if(first==false) fprintf(F, " or "); else first = false;
			fprintf(F, "%s", muxIn->port_en->name);
		}
		fprintf(F, ";\n");
		fprintf(F, "\n");
		ftabprintf(F, 1, "%s <= tmp_or_data when tmp_or_en = '1' else ", mux_data->port_out->name);
		if(mux_data->default_input!=NULL) {
			netlist_mux_access_t* DefMuxIn = mux_data->default_input->data;
			fprintf(F, "%s", DefMuxIn->port_data->name);
		}
		else if(mux_data->default_value!=NULL) {
			Netlist_GenVHDL_StdLogic_Literal(F, mux_data->default_value);
		}
		fprintf(F, ";\n");
	}
	else {
		fprintf(F, "\n");
		ftabprintf(F, 1, "%s <= tmp_or_data;\n", mux_data->port_out->name);
	}

	fprintf(F,
		"\n"
		"end architecture;\n"
	);
}

static void Netlist_Mux_GenVHDL(FILE* F, netlist_vhdl_t* params, netlist_comp_t* comp) {
	netlist_mux_t* mux_data = comp->data;
	if(mux_data->inputs_nb<=1 && mux_data->default_value==NULL) {
		Netlist_Mux_GenVHDL_AsWire(F, params, comp);
	}
	else if(mux_data->inputs_nb==0 && mux_data->default_value!=NULL) {
		Netlist_Mux_GenVHDL_AsWire(F, params, comp);
	}
	else {
		Netlist_Mux_GenVHDL_AsOr(F, params, comp);
	}
}

// Generate the signal declaration for the inline MUX
static void Netlist_Mux_InlVHDL_SigDecl(FILE* F, netlist_vhdl_t* params, netlist_comp_t* comp) {
	netlist_mux_t* mux_data = comp->data;
	fprintf(F, "\tsignal %s : ", Netlist_Comp_GetRename_Comp(comp, params));
	Netlist_GenVHDL_StdLogicType_be(F, mux_data->width, NULL, ";\n");
}
// Generate a big OR operation (good for synthesis)
static void Netlist_Mux_InlVHDL_OneComp_AsOr(FILE* F, netlist_vhdl_t* params, netlist_comp_t* comp) {
	netlist_mux_t* mux_data = comp->data;
	fprintf(F, "\t%s <=", Netlist_Comp_GetRename_Comp(comp, params));

	bool write_default_val = false;
	if(mux_data->default_input!=NULL || mux_data->default_value!=NULL) {
		write_default_val = true;
		// Check if the default is zero or don't care
		bool default_is_zero = false;
		if(mux_data->default_input!=NULL) {
			netlist_mux_access_t* muxIn = mux_data->default_input->data;
			default_is_zero = Netlist_ValCat_ListIsLitNoBit(muxIn->port_data->source, '1');
		}
		if(mux_data->default_value!=NULL) {
			default_is_zero = Netlist_Literal_IsNoBit(mux_data->default_value, '1');
		}
		// If all '0' or '-', don't write the default case
		if(default_is_zero==true) write_default_val = false;
	}

	if(write_default_val==true) {
		fprintf(F, " ");
		if(mux_data->default_input!=NULL) {
			netlist_mux_access_t* muxIn = mux_data->default_input->data;
			Netlist_GenVHDL_ValCat_List_opt(F, comp->father, muxIn->port_data->source, params, GENVHDL_VALCAT_CHKADDPAR | GENVHDL_VALCAT_USEREPEAT);
		}
		else if(mux_data->default_value!=NULL) {
			Netlist_GenVHDL_StdLogic_Literal(F, mux_data->default_value);
		}
		fprintf(F, " when ");
		unsigned nb = 0;
		avl_pp_foreach(&comp->accesses, scanAccess) {
			netlist_access_t* access = scanAccess->data;
			netlist_mux_access_t* muxIn = access->data;
			if(muxIn->is_default==true) continue;
			if(nb>0) fprintf(F, "and ");
			nb++;
			Netlist_GenVHDL_ValCat_List(F, comp->father, muxIn->port_en->source, params);
			fprintf(F, " = '0' ");
		}
		assert(nb>0);
		fprintf(F, "else");
	}
	fprintf(F, "\n");

	unsigned nb = 0;
	avl_pp_foreach(&comp->accesses, scanAccess) {
		netlist_access_t* access = scanAccess->data;
		netlist_mux_access_t* muxIn = access->data;
		if(muxIn->is_default==true) continue;
		if(nb>0) fprintf(F, " or\n");
		nb++;
		fprintf(F, "		(");

		assert(muxIn->port_en->source!=NULL);
		assert(muxIn->port_en->source->NEXT==NULL);

		// Convert the condition to "repeat" valcat style if needed
		netlist_val_cat_t* valcat = muxIn->port_en->source->DATA;
		assert(valcat->width==1);
		valcat->width = mux_data->width;
		Netlist_GenVHDL_ValCat_List_opt(F, comp->father, muxIn->port_en->source, params, GENVHDL_VALCAT_CHKADDPAR | GENVHDL_VALCAT_USEREPEAT);
		// Set back the original width
		valcat->width = 1;

		// Print the data
		fprintf(F, " and ");
		Netlist_GenVHDL_ValCat_List_opt(F, comp->father, muxIn->port_data->source, params, GENVHDL_VALCAT_CHKADDPAR | GENVHDL_VALCAT_USEREPEAT);
		fputc(')', F);
	}

	fprintf(F, ";\n");
}
// Generate a big conditional statement with "when" statements (good for simulation)
static void Netlist_Mux_InlVHDL_OneComp_AsWhen(FILE* F, netlist_vhdl_t* params, netlist_comp_t* comp) {
	netlist_mux_t* mux_data = comp->data;
	fprintf(F, "\t%s <=\n", Netlist_Comp_GetRename_Comp(comp, params));

	// Print non-default inputs
	avl_pp_foreach(&comp->accesses, scanAccess) {
		netlist_access_t* access = scanAccess->data;
		netlist_mux_access_t* muxIn = access->data;
		if(muxIn->is_default==true) continue;

		assert(muxIn->port_en->source!=NULL);
		assert(muxIn->port_en->source->NEXT==NULL);

		fprintf(F, "		");
		Netlist_GenVHDL_ValCat_List_opt(F, comp->father, muxIn->port_data->source, params, GENVHDL_VALCAT_CHKADDPAR | GENVHDL_VALCAT_USEREPEAT);
		fprintf(F, " when ");
		Netlist_GenVHDL_ValCat_List(F, comp->father, muxIn->port_en->source, params);
		fprintf(F, " = '1' else\n");
	}

	// Print the default input, if any
	fprintf(F, "		");
	if(mux_data->default_value!=NULL) {
		Netlist_GenVHDL_StdLogic_Literal(F, mux_data->default_value);
	}
	else if(mux_data->default_input!=NULL) {
		netlist_mux_access_t* muxIn = mux_data->default_input->data;
		Netlist_GenVHDL_ValCat_List_opt(F, comp->father, muxIn->port_data->source, params, GENVHDL_VALCAT_CHKADDPAR | GENVHDL_VALCAT_USEREPEAT);
	}
	else {
		Netlist_GenVHDL_StdLogic_Literal_Repeat(F, mux_data->width, '0');
	}
	fprintf(F, ";\n");
}
// Select between implementations
static void Netlist_Mux_InlVHDL_OneComp(FILE* F, netlist_vhdl_t* params, netlist_comp_t* comp) {
	if(objective_synth==true) {
		Netlist_Mux_InlVHDL_OneComp_AsOr(F, params, comp);
	}
	else {
		Netlist_Mux_InlVHDL_OneComp_AsWhen(F, params, comp);
	}
}

netlist_comp_model_t* Netlist_Comp_Mux_GetModel() {
	netlist_comp_model_t* model = Netlist_CompModel_New();
	model->name = namealloc("mux");

	model->data_size         = sizeof(netlist_mux_t);
	model->func_dup_internal = Netlist_Mux_DataDup;
	model->func_gen_vhdl     = Netlist_Mux_GenVHDL;

	model->func_simp         = (netlist_comp_cb_simp_t)Netlist_Simp_Comp_Mux;

	model->inlvhdl_en = true;
	model->func_inlvhdl_sigdecl = Netlist_Mux_InlVHDL_SigDecl;
	model->func_inlvhdl_onecomp = Netlist_Mux_InlVHDL_OneComp;

	NETLIST_COMP_MUX = model;
	return model;
}



//===================================================
// SIG functions
//===================================================

netlist_comp_model_t* NETLIST_COMP_SIGNAL = NULL;

netlist_comp_t* Netlist_Comp_Sig_New(char* name, unsigned width) {
	netlist_comp_t* comp = Netlist_Comp_New(NETLIST_COMP_SIGNAL);
	comp->name = name;

	netlist_signal_t* comp_sig = comp->data;
	comp_sig->width = width;

	char* port_name;

	port_name = namealloc("data_out");
	comp_sig->port_out = Netlist_Comp_AddPort_Out(comp, port_name, width);
	port_name = namealloc("data_in");
	comp_sig->port_in = Netlist_Comp_AddPort_In(comp, port_name, width);

	return comp;
}

static void Netlist_Comp_Sig_Dup(avl_pp_tree_t* tree_old2new, netlist_comp_t* comp, netlist_comp_t* newcomp) {
	netlist_signal_t* sig_data = comp->data;
	netlist_signal_t* newsig_data = newcomp->data;

	newsig_data->width = sig_data->width;

	void* pointer_old2new(void* oldptr) {
		void* newptr = NULL;
		avl_pp_find_data(tree_old2new, oldptr, &newptr);
		return newptr;
	}

	newsig_data->port_in = pointer_old2new(sig_data->port_in);
	newsig_data->port_out = pointer_old2new(sig_data->port_out);
}

static void Netlist_Comp_Sig_GenVHDL(FILE* F, netlist_vhdl_t* params, netlist_comp_t* comp) {
	netlist_signal_t* sig_data = comp->data;

	Netlist_GenVHDL_Entity(F, comp, params);

	fprintfm(F,
		"\n"
		"architecture %s of %s is\n", params->architecture, Netlist_Comp_GetRename_Comp(comp, params),
		"begin\n"
		"\n"
		"	%s <= %s;\n", sig_data->port_out->name, sig_data->port_in->name,
		"\n"
		"end architecture;\n",
		NULL
	);
}

netlist_comp_model_t* Netlist_Comp_Sig_GetModel() {
	netlist_comp_model_t* model = Netlist_CompModel_New();
	model->name  = namealloc("sig");

	model->data_size         = sizeof(netlist_signal_t);
	model->func_dup_internal = Netlist_Comp_Sig_Dup;
	model->func_gen_vhdl     = Netlist_Comp_Sig_GenVHDL;

	model->func_simp         = (netlist_comp_cb_simp_t)Netlist_Simp_Comp_Sig;

	NETLIST_COMP_SIGNAL = model;
	return model;
}



//===================================================
// REG functions
//===================================================

netlist_comp_model_t* NETLIST_COMP_REGISTER = NULL;

netlist_comp_t* Netlist_Comp_Reg_New(char* name, unsigned width) {
	netlist_comp_t* comp = Netlist_Comp_New(NETLIST_COMP_REGISTER);
	comp->name = name;

	netlist_register_t* comp_reg = comp->data;
	comp_reg->width = width;

	char* port_name;

	port_name = namealloc("reset");
	comp_reg->port_reset = Netlist_Comp_AddPortAccess_Reset(comp, port_name);
	port_name = namealloc("clk");
	comp_reg->port_clk = Netlist_Comp_AddPortAccess_Clock(comp, port_name);

	port_name = namealloc("data_out");
	comp_reg->port_out = Netlist_Comp_AddPort_Out(comp, port_name, width);
	port_name = namealloc("data_in");
	comp_reg->port_in = Netlist_Comp_AddPort_In(comp, port_name, width);
	port_name = namealloc("enable");
	comp_reg->port_en = Netlist_Comp_AddPort_In(comp, port_name, 1);

	return comp;
}

static void Netlist_Comp_Reg_Dup(avl_pp_tree_t* tree_old2new, netlist_comp_t* comp, netlist_comp_t* newcomp) {
	netlist_register_t* reg_data = comp->data;
	netlist_register_t* newreg_data = newcomp->data;

	newreg_data->width = reg_data->width;
	newreg_data->init_value = reg_data->init_value;

	void* pointer_old2new(void* oldptr) {
		void* newptr = NULL;
		avl_pp_find_data(tree_old2new, oldptr, &newptr);
		return newptr;
	}

	newreg_data->port_in  = pointer_old2new(reg_data->port_in);
	newreg_data->port_out = pointer_old2new(reg_data->port_out);
	newreg_data->port_clk = pointer_old2new(reg_data->port_clk);
	newreg_data->port_en  = pointer_old2new(reg_data->port_en);

	newreg_data->port_reset = pointer_old2new(reg_data->port_reset);
}

static void Netlist_Comp_Reg_GenVHDL(FILE* F, netlist_vhdl_t* params, netlist_comp_t* comp) {
	netlist_register_t* reg_data = comp->data;

	Netlist_GenVHDL_Entity(F, comp, params);

	fprintfm(F,
		"\n"
		"architecture %s of %s is\n", params->architecture, Netlist_Comp_GetRename_Comp(comp, params),
		"\n"
		"	signal buf : ",
		NULL
	);

	if(reg_data->init_value==NULL) {
		Netlist_GenVHDL_StdLogicType_ibe(F, reg_data->width, default_store_value, NULL, ";\n");
	}
	else {
		Netlist_GenVHDL_StdLogicType(F, reg_data->width);
		Netlist_GenVHDL_StdLogic_Literal_be(F, reg_data->init_value, " := ", ";\n");
	}

	fprintf(F,
		"\n"
		"begin\n"
		"\n"
	);

	void print_sync_write(char* indent) {
		fprintf(F, "%sif %s(%s) then\n", indent, Netlist_GenVHDL_ClockEdge_TestFunc_port(reg_data->port_clk), reg_data->port_clk->name);
		if(reg_data->port_en!=NULL) {
			fprintfm(F,
				"%s	if %s = '1' then\n", indent, reg_data->port_en->name,
				"%s		buf <= %s;\n", indent, reg_data->port_in->name,
				"%s	end if;\n", indent,
				NULL
			);
		}
		else {
			fprintfm(F, "%s	buf <= %s;\n", indent, reg_data->port_in->name);
		}
		fprintf(F, "%send if;\n", indent);
	}

	if(reg_data->init_value==NULL || reg_data->port_reset==NULL) {
		fprintfm(F,
			"	process (%s)\n", reg_data->port_clk->name,
			"	begin\n",
			NULL
		);
		print_sync_write("		");
	}
	else {
		fprintfm(F,
			"	process (%s, %s)\n", reg_data->port_clk->name, reg_data->port_reset->name,
			"	begin\n"
			"		if %s = '%c' then\n", reg_data->port_reset->name, Netlist_Access_Reset_GetActiveState(reg_data->port_reset->access),
			NULL
		);
		Netlist_GenVHDL_StdLogic_Literal_be(F, reg_data->init_value, "			buf <= ", ";\n");
		fprintf(F, "		else\n");
		print_sync_write("			");
		fprintf(F, "		end if;\n");
	}

	fprintfm(F,
		"	end process;\n"
		"\n"
		"	%s <= buf;\n", reg_data->port_out->name,
		"\n"
		"end architecture;\n",
		NULL
	);

}
static void Netlist_Comp_Reg_InlVHDL_SigDecl(FILE* F, netlist_vhdl_t* params, netlist_comp_t* comp) {
	netlist_register_t* reg_data = comp->data;
	fprintf(F, "\tsignal %s : ", Netlist_Comp_GetRename_Comp(comp, params));
	if(reg_data->init_value==NULL) {
		Netlist_GenVHDL_StdLogicType_ibe(F, reg_data->width, default_store_value, NULL, ";\n");
	}
	else {
		Netlist_GenVHDL_StdLogicType(F, reg_data->width);
		Netlist_GenVHDL_StdLogic_Literal_be(F, reg_data->init_value, " := ", ";\n");
	}
}
static void Netlist_Comp_Reg_InlVHDL_AllComps(FILE* F, netlist_vhdl_t* params, netlist_comp_t* comp) {
	// Work on a list of all registers to process
	chain_list* list_all_regs = NULL;
	// Get the clock source, check if it is common to all registers
	avl_pp_foreach(&comp->children, scanChild) {
		netlist_comp_t* child = scanChild->data;
		if(child->model!=NETLIST_COMP_REGISTER) continue;
		list_all_regs = addchain(list_all_regs, child);
	}

	// Paranoia
	if(list_all_regs==NULL) return;

	// Pick the first register to process
	// Find the clock are reset sources
	// Get all the similar registers

	while(list_all_regs!=NULL) {
		// These are ValCat lists
		chain_list* clock_expr = NULL;
		chain_list* reset_expr = NULL;
		int  clock_edge = 0;
		char reset_value = 0;

		// This is the list of all register similar to the first one
		chain_list* list_similar_regs = NULL;
		// This is the list of other registers, not similar
		chain_list* new_list = NULL;

		// Scan all registers
		foreach(list_all_regs, scanReg) {
			netlist_comp_t* child = scanReg->DATA;
			netlist_register_t* reg_data = child->data;
			assert(reg_data->port_clk->source!=NULL);

			if(clock_expr==NULL) {
				clock_expr = Netlist_ValCat_DupList(reg_data->port_clk->source);
				clock_edge = Netlist_Access_Clock_GetEdge(reg_data->port_clk->access);
				if(reg_data->port_reset!=NULL && reg_data->init_value!=NULL) {
					assert(reg_data->port_reset->source!=NULL);
					reset_expr = Netlist_ValCat_DupList(reg_data->port_reset->source);
					reset_value = Netlist_Access_Reset_GetActiveState(reg_data->port_reset->access);
				}
				list_similar_regs = addchain(list_similar_regs, child);
				continue;
			}

			// Here we have to compare details of clock and reset
			if(Netlist_ValCat_EqList(reg_data->port_clk->source, clock_expr)==false) goto SKIPREG;
			if(Netlist_Access_Clock_GetEdge(reg_data->port_clk->access) != clock_edge) goto SKIPREG;
			if(reset_expr!=NULL) {
				if(reg_data->port_reset==NULL) goto SKIPREG;
				assert(reg_data->port_reset->source!=NULL);
				if(Netlist_ValCat_EqList(reg_data->port_reset->source, reset_expr)==false) goto SKIPREG;
				if(Netlist_Access_Reset_GetActiveState(reg_data->port_reset->access) != reset_value) goto SKIPREG;
			}
			else {
				if(reg_data->port_reset!=NULL && reg_data->init_value!=NULL) goto SKIPREG;
			}

			// Here we keep this register
			list_similar_regs = addchain(list_similar_regs, child);
			continue;

			SKIPREG:
			new_list = addchain(new_list, child);
		}  // Scan all regs

		// Swap the lists of all registers
		freechain(list_all_regs);
		list_all_regs = new_list;

		// Here we print the VHDL for all these registers
		assert(list_similar_regs!=NULL);
		char* clockedge_testfunc = Netlist_GenVHDL_ClockEdge_TestFunc(clock_edge);

		fprintf(F, "	-- Registers with clock = ");
		Netlist_GenVHDL_ValCat_List(F, comp, clock_expr, params);
		fprintf(F, ", %s, and", clockedge_testfunc);
		if(reset_expr==NULL) {
			fprintf(F, " no reset");
		}
		else {
			fprintf(F, " reset = ");
			Netlist_GenVHDL_ValCat_List(F, comp, reset_expr, params);
			fprintf(F, " active '%c'", reset_value);
		}
		fprintf(F, "\n");

		// The version that has no reset
		if(reset_expr==NULL) {
			fprintf(F, "	process(");
			Netlist_GenVHDL_ValCat_List(F, comp, clock_expr, params);
			fprintf(F, ")\n"
				"	begin\n"
				"		if %s(", clockedge_testfunc
			);
			Netlist_GenVHDL_ValCat_List(F, comp, clock_expr, params);
			fprintf(F, ") then\n");
			// The expressions for all registers
			foreach(list_similar_regs, scanReg) {
				netlist_comp_t* child = scanReg->DATA;
				netlist_register_t* reg_data = child->data;
				if(reg_data->port_en==NULL) {
					fprintf(F, "			%s <= ", Netlist_Comp_GetRename_Comp(child, params));
					Netlist_GenVHDL_ValCat_List(F, comp, reg_data->port_in->source, params);
					fprintf(F, ";\n");
				}
				else {
					fprintf(F, "			if ");
					Netlist_GenVHDL_ValCat_List(F, comp, reg_data->port_en->source, params);
					fprintf(F, " = '1' then\n");
					fprintf(F, "				%s <= ", Netlist_Comp_GetRename_Comp(child, params));
					Netlist_GenVHDL_ValCat_List_opt(F, comp, reg_data->port_in->source, params, GENVHDL_VALCAT_USEREPEAT);
					fprintf(F, ";\n");
					fprintf(F, "			end if;\n");
				}
			}
			fprintf(F, "		end if;  -- Clock edge\n");
			fprintf(F, "	end process;\n");
		}

		// The version that has a reset source.
		// All regs are supposed to have an init value.
		else {
			fprintf(F, "	process(");
			Netlist_GenVHDL_ValCat_List(F, comp, clock_expr, params);
			fprintf(F, ", ");
			// FIXME What if the reset is a literal?
			Netlist_GenVHDL_ValCat_List(F, comp, reset_expr, params);
			fprintf(F, ")\n"
				"	begin\n"
				"		if "
			);
			Netlist_GenVHDL_ValCat_List(F, comp, reset_expr, params);
			fprintf(F, " = '%c' then\n", reset_value);
			foreach(list_similar_regs, scanReg) {
				netlist_comp_t* child = scanReg->DATA;
				netlist_register_t* reg_data = child->data;
				if(reg_data->init_value == NULL) continue;  // In case that comp sneaked through the simplification process
				fprintf(F, "			%s <= ", Netlist_Comp_GetRename_Comp(child, params));
				Netlist_GenVHDL_StdLogic_Literal_be(F, reg_data->init_value, NULL, ";\n");
			}
			fprintf(F,
				"		else\n"
				"			if %s(", clockedge_testfunc
			);
			Netlist_GenVHDL_ValCat_List(F, comp, clock_expr, params);
			fprintf(F, ") then\n");
			// The expressions for all registers
			foreach(list_similar_regs, scanReg) {
				netlist_comp_t* child = scanReg->DATA;
				netlist_register_t* reg_data = child->data;
				if(reg_data->port_en==NULL) {
					fprintf(F, "				%s <= ", Netlist_Comp_GetRename_Comp(child, params));
					Netlist_GenVHDL_ValCat_List(F, comp, reg_data->port_in->source, params);
					fprintf(F, ";\n");
				}
				else {
					fprintf(F, "				if ");
					Netlist_GenVHDL_ValCat_List(F, comp, reg_data->port_en->source, params);
					fprintf(F, " = '1' then\n");
					fprintf(F, "					%s <= ", Netlist_Comp_GetRename_Comp(child, params));
					Netlist_GenVHDL_ValCat_List(F, comp, reg_data->port_in->source, params);
					fprintf(F, ";\n");
					fprintf(F, "				end if;\n");
				}
			}
			fprintf(F, "			end if;  -- Clock edge\n");
			fprintf(F, "		end if;  -- Reset\n");
			fprintf(F, "	end process;\n");
		}

		// Free the list of similar registers
		freechain(list_similar_regs);
		// Free the ValCat for clock and reset
		Netlist_ValCat_FreeList(clock_expr);
		Netlist_ValCat_FreeList(reset_expr);

	}  // End of loop on all registers to process

}

netlist_comp_model_t* Netlist_Comp_Reg_GetModel() {
	netlist_comp_model_t* model = Netlist_CompModel_New();
	model->name = namealloc("reg");

	model->data_size         = sizeof(netlist_register_t);
	model->func_dup_internal = Netlist_Comp_Reg_Dup;
	model->func_gen_vhdl     = Netlist_Comp_Reg_GenVHDL;

	model->func_simp         = (netlist_comp_cb_simp_t)Netlist_Simp_Comp_Reg;

	model->inlvhdl_en = true;
	model->func_inlvhdl_sigdecl  = Netlist_Comp_Reg_InlVHDL_SigDecl;
	model->func_inlvhdl_allcomps = Netlist_Comp_Reg_InlVHDL_AllComps;

	NETLIST_COMP_REGISTER = model;
	return model;
}



//===================================================
// MEM functions
//===================================================

netlist_comp_model_t* NETLIST_COMP_MEMORY = NULL;

netlist_comp_t* Netlist_Comp_Mem_New(char* name, unsigned data_width, unsigned cells_nb, unsigned addr_width) {
	netlist_comp_t* comp = Netlist_Comp_New(NETLIST_COMP_MEMORY);
	comp->name = name;

	netlist_memory_t* comp_mem = comp->data;

	comp_mem->data_width = data_width;
	comp_mem->cells_nb = cells_nb;
	comp_mem->addr_width = addr_width;
	comp_mem->ports_ra_nb = 0;
	comp_mem->ports_wa_nb = 0;
	comp_mem->ports_cam_nb = 0;
	comp_mem->direct_en = false;

	avl_p_init(&comp_mem->access_ports_ra);
	avl_p_init(&comp_mem->access_ports_wa);
	avl_ip_init(&comp_mem->access_ports_d);
	avl_p_init(&comp_mem->access_ports_cam);

	char* port_name;

	port_name = namealloc("clk");
	comp_mem->port_clk = Netlist_Comp_AddPortAccess_Clock(comp, port_name);

	return comp;
}

static void Netlist_Comp_Mem_Free_Data(netlist_comp_t* comp) {
	netlist_memory_t* comp_mem = comp->data;
	avl_p_reset(&comp_mem->access_ports_ra);
	avl_p_reset(&comp_mem->access_ports_wa);
	avl_ip_reset(&comp_mem->access_ports_d);
	avl_p_reset(&comp_mem->access_ports_cam);
	Netlist_Mem_InitValues_Free(comp);
	free(comp_mem);
}
static void Netlist_Comp_Mem_Dup(avl_pp_tree_t* tree_old2new, netlist_comp_t* comp, netlist_comp_t* newcomp) {
	netlist_memory_t* mem_data = comp->data;
	netlist_memory_t* newmem_data = newcomp->data;

	newmem_data->data_width   = mem_data->data_width;
	newmem_data->addr_width   = mem_data->addr_width;
	newmem_data->cells_nb     = mem_data->cells_nb;
	newmem_data->ports_wa_nb  = mem_data->ports_wa_nb;
	newmem_data->ports_ra_nb  = mem_data->ports_ra_nb;
	newmem_data->direct_en    = mem_data->direct_en;
	newmem_data->ports_cam_nb = mem_data->ports_cam_nb;

	void* pointer_old2new(void* oldptr) {
		void* newptr = NULL;
		avl_pp_find_data(tree_old2new, oldptr, &newptr);
		return newptr;
	}

	newmem_data->port_clk = pointer_old2new(mem_data->port_clk);

	avl_p_foreach(&mem_data->access_ports_ra, scanRA) {
		netlist_access_t* newaccess = pointer_old2new(scanRA->data);
		avl_p_add_overwrite(&newmem_data->access_ports_ra, newaccess, newaccess);
	}
	avl_p_foreach(&mem_data->access_ports_wa, scanWA) {
		netlist_access_t* newaccess = pointer_old2new(scanWA->data);
		avl_p_add_overwrite(&newmem_data->access_ports_wa, newaccess, newaccess);
	}
	avl_ip_foreach(&mem_data->access_ports_d, scanDirect) {
		netlist_access_t* newaccess = pointer_old2new(scanDirect->data);
		avl_ip_add_overwrite(&newmem_data->access_ports_d, scanDirect->key, newaccess);
	}
	avl_p_foreach(&mem_data->access_ports_cam, scanCam) {
		netlist_access_t* newaccess = pointer_old2new(scanCam->data);
		avl_p_add_overwrite(&newmem_data->access_ports_cam, newaccess, newaccess);
	}

	// The Init values
	if(mem_data->init_vex!=NULL) {
		newmem_data->init_vex = malloc(mem_data->cells_nb * sizeof(*newmem_data->init_vex));
		for(unsigned i=0; i<mem_data->cells_nb; i++) {
			newmem_data->init_vex[i] = hvex_dup(mem_data->init_vex[i]);
		}
	}
}

// Note : the given array is directly set in the component (no duplication)
// No check is done. The array size must be EQUAL to the number of cells of the memory.
// The init strings can be NULL. It means the result is implementation-dependent.
// if strings_alloc=true, at component free the INIT strings are also freed.
void Netlist_Mem_InitValues_Free(netlist_comp_t* comp) {
	netlist_memory_t* comp_mem = comp->data;
	if(comp_mem->init_vex==NULL) return;
	for(unsigned i=0; i<comp_mem->cells_nb; i++) {
		if(comp_mem->init_vex[i]!=NULL) hvex_free(comp_mem->init_vex[i]);
	}
	free(comp_mem->init_vex);
	comp_mem->init_vex = NULL;
}


netlist_access_model_t* NETLIST_ACCESS_MEM_RA = NULL;
netlist_access_model_t* NETLIST_ACCESS_MEM_WA = NULL;
netlist_access_model_t* NETLIST_ACCESS_MEM_D = NULL;
netlist_access_model_t* NETLIST_ACCESS_MEM_CAM = NULL;

// To access an external memory component
netlist_access_model_t* NETLIST_ACCESS_MEM_RA_EXT = NULL;
netlist_access_model_t* NETLIST_ACCESS_MEM_WA_EXT = NULL;

netlist_access_t* Netlist_Mem_AddAccess_RA(netlist_comp_t* comp) {
	netlist_memory_t* comp_mem = comp->data;

	char buffer[200];
	char* name_alloc;
	netlist_access_t* access;

	sprintf(buffer, "ra%d", comp_mem->ports_ra_nb);
	name_alloc = namealloc(buffer);
	access = Netlist_Comp_AddAccess(comp, NETLIST_ACCESS_MEM_RA, name_alloc);

	netlist_access_mem_addr_t* access_addr = access->data;

	sprintf(buffer, "%s_data", access->name);
	name_alloc = namealloc(buffer);
	access_addr->port_d = Netlist_Comp_AddPort_Out(comp, name_alloc, comp_mem->data_width);
	access_addr->port_d->access = access;

	sprintf(buffer, "%s_addr", access->name);
	name_alloc = namealloc(buffer);
	access_addr->port_a = Netlist_Comp_AddPort_In(comp, name_alloc, comp_mem->addr_width);
	access_addr->port_a->access = access;

	avl_p_add_overwrite(&comp_mem->access_ports_ra, access, access);

	comp_mem->ports_ra_nb++;
	return access;
}
netlist_access_t* Netlist_Mem_AddAccess_WA(netlist_comp_t* comp) {
	netlist_memory_t* comp_mem = comp->data;

	char buffer[200];
	char* name_alloc;
	netlist_access_t* access;

	sprintf(buffer, "wa%d", comp_mem->ports_wa_nb);
	name_alloc = namealloc(buffer);
	access = Netlist_Comp_AddAccess(comp, NETLIST_ACCESS_MEM_WA, name_alloc);

	netlist_access_mem_addr_t* access_addr = access->data;

	sprintf(buffer, "%s_data", access->name);
	name_alloc = namealloc(buffer);
	access_addr->port_d = Netlist_Comp_AddPort_In(comp, name_alloc, comp_mem->data_width);
	access_addr->port_d->access = access;

	sprintf(buffer, "%s_addr", access->name);
	name_alloc = namealloc(buffer);
	access_addr->port_a = Netlist_Comp_AddPort_In(comp, name_alloc, comp_mem->addr_width);
	access_addr->port_a->access = access;

	sprintf(buffer, "%s_en", access->name);
	name_alloc = namealloc(buffer);
	access_addr->port_e = Netlist_Comp_AddPort_In(comp, name_alloc, 1);
	access_addr->port_e->access = access;

	avl_p_add_overwrite(&comp_mem->access_ports_wa, access, access);

	comp_mem->ports_wa_nb++;
	return access;
}
netlist_access_t* Netlist_Mem_AddAccess_Cam(netlist_comp_t* comp, char* hard_mask, char* hard_data) {
	netlist_memory_t* comp_mem = comp->data;

	char buffer[200];
	char* name_alloc;
	netlist_access_t* access;

	sprintf(buffer, "cam%d", comp_mem->ports_cam_nb);
	name_alloc = namealloc(buffer);
	access = Netlist_Comp_AddAccess(comp, NETLIST_ACCESS_MEM_CAM, name_alloc);

	netlist_access_mem_cam_t* access_cam = access->data;

	sprintf(buffer, "%s_out", access->name);
	name_alloc = namealloc(buffer);
	access_cam->port_out = Netlist_Comp_AddPort_Out(comp, name_alloc, comp_mem->cells_nb);
	access_cam->port_out->access = access;

	if(hard_data==NULL) {
		sprintf(buffer, "%s_data", access->name);
		name_alloc = namealloc(buffer);
		access_cam->port_data = Netlist_Comp_AddPort_In(comp, name_alloc, comp_mem->data_width);
		access_cam->port_data->access = access;
	}
	else {
		access_cam->internal_data = hard_data;
	}

	if(hard_mask==NULL) {
		sprintf(buffer, "%s_mask", access->name);
		name_alloc = namealloc(buffer);
		access_cam->port_mask = Netlist_Comp_AddPort_In(comp, name_alloc, comp_mem->data_width);
		access_cam->port_mask->access = access;
	}
	else {
		access_cam->internal_mask = hard_mask;
	}

	avl_p_add_overwrite(&comp_mem->access_ports_cam, access, access);

	comp_mem->ports_cam_nb++;
	return access;
}

void Netlist_Mem_EnableDirect(netlist_comp_t* comp) {
	netlist_memory_t* comp_mem = comp->data;

	if(comp_mem->direct_en==true) return;

	char buffer[200];
	char* name_alloc;
	netlist_access_t* access;

	for(unsigned i=0; i<comp_mem->cells_nb; i++) {
		sprintf(buffer, "cell%d", i);
		name_alloc = namealloc(buffer);

		access = Netlist_Comp_AddAccess(comp, NETLIST_ACCESS_MEM_D, name_alloc);
		netlist_access_mem_direct_t* access_direct = access->data;
		access_direct->cell_index = i;

		sprintf(buffer, "%s_wd", access->name);
		name_alloc = namealloc(buffer);
		access_direct->port_wd = Netlist_Comp_AddPort_In(comp, name_alloc, comp_mem->data_width);
		access_direct->port_wd->access = access;

		sprintf(buffer, "%s_we", access->name);
		name_alloc = namealloc(buffer);
		access_direct->port_we = Netlist_Comp_AddPort_In(comp, name_alloc, 1);
		access_direct->port_we->access = access;

		sprintf(buffer, "%s_rd", access->name);
		name_alloc = namealloc(buffer);
		access_direct->port_rd = Netlist_Comp_AddPort_Out(comp, name_alloc, comp_mem->data_width);
		access_direct->port_rd->access = access;

		avl_ip_add_overwrite(&comp_mem->access_ports_d, i, access);
	}  // Loop on the cells

	comp_mem->direct_en = true;
}
netlist_access_t* Netlist_Mem_GetDirectAccess(netlist_comp_t* comp, unsigned index) {
	netlist_memory_t* comp_mem = comp->data;
	netlist_access_t* access = NULL;
	avl_ip_find_data(&comp_mem->access_ports_d, index, (void**)&access);
	return access;
}

// These accesses are normally not created in a MEMORY component
netlist_access_t* Netlist_AddAccess_RA_Ext(netlist_comp_t* comp, unsigned data_width, unsigned addr_width, char* name) {
	netlist_access_t* access = Netlist_Comp_AddAccess(comp, NETLIST_ACCESS_MEM_RA_EXT, name);
	netlist_access_mem_addr_t* access_addr = access->data;

	char buffer[200];
	char* name_alloc;

	sprintf(buffer, "%s_data", access->name);
	name_alloc = namealloc(buffer);
	access_addr->port_d = Netlist_Comp_AddPort_In(comp, name_alloc, data_width);
	access_addr->port_d->access = access;

	sprintf(buffer, "%s_addr", access->name);
	name_alloc = namealloc(buffer);
	access_addr->port_a = Netlist_Comp_AddPort_Out(comp, name_alloc, addr_width);
	access_addr->port_a->access = access;

	return access;
}
netlist_access_t* Netlist_AddAccess_WA_Ext(netlist_comp_t* comp, unsigned data_width, unsigned addr_width, char* name) {
	netlist_access_t* access = Netlist_Comp_AddAccess(comp, NETLIST_ACCESS_MEM_WA_EXT, name);
	netlist_access_mem_addr_t* access_addr = access->data;

	char buffer[200];
	char* name_alloc;

	sprintf(buffer, "%s_data", access->name);
	name_alloc = namealloc(buffer);
	access_addr->port_d = Netlist_Comp_AddPort_Out(comp, name_alloc, data_width);
	access_addr->port_d->access = access;

	sprintf(buffer, "%s_addr", access->name);
	name_alloc = namealloc(buffer);
	access_addr->port_a = Netlist_Comp_AddPort_Out(comp, name_alloc, addr_width);
	access_addr->port_a->access = access;

	sprintf(buffer, "%s_en", access->name);
	name_alloc = namealloc(buffer);
	access_addr->port_e = Netlist_Comp_AddPort_Out(comp, name_alloc, 1);
	access_addr->port_e->access = access;

	return access;
}

// WARNING : Do not attempt to add ports after having removed some.
// The generated names may conflict.
static void Netlist_AccessRem_MemAddr_common(netlist_access_t* access) {
	netlist_access_mem_addr_t* access_addr = access->data;
	// Remove the ports
	if(access_addr->port_e!=NULL) Netlist_Port_Free(access_addr->port_e);
	if(access_addr->port_d!=NULL) Netlist_Port_Free(access_addr->port_d);
	if(access_addr->port_a!=NULL) Netlist_Port_Free(access_addr->port_a);
	// Free the access
	Netlist_Access_Free(access);
}
static void Netlist_AccessRem_MemDirect_common(netlist_access_t* access) {
	netlist_access_mem_direct_t* memDirect = access->data;
	// Remove the ports
	if(memDirect->port_wd!=NULL) Netlist_Port_Free(memDirect->port_wd);
	if(memDirect->port_we!=NULL) Netlist_Port_Free(memDirect->port_we);
	if(memDirect->port_rd!=NULL) Netlist_Port_Free(memDirect->port_rd);
	// Free the access
	Netlist_Access_Free(access);
}

void Netlist_Access_MemRA_Remove(netlist_access_t* access) {
	// Remove from the parent memory data
	netlist_comp_t* comp = access->component;
	netlist_memory_t* comp_mem = comp->data;
	avl_p_rem_key(&comp_mem->access_ports_ra, access);
	comp_mem->ports_ra_nb--;
	// Finish the free
	Netlist_AccessRem_MemAddr_common(access);
}
void Netlist_Access_MemWA_Remove(netlist_access_t* access) {
	// Remove from the parent memory data
	netlist_memory_t* comp_mem = access->component->data;
	avl_p_rem_key(&comp_mem->access_ports_wa, access);
	comp_mem->ports_wa_nb--;
	// Finish the free
	Netlist_AccessRem_MemAddr_common(access);
}
void Netlist_Access_MemDirect_Remove(netlist_access_t* access) {
	netlist_memory_t* comp_mem = access->component->data;
	netlist_access_mem_direct_t* memDirect = access->data;
	// Unlink from the parent memory component
	avl_ip_rem_key(&comp_mem->access_ports_d, memDirect->cell_index);
	// Finish the free
	Netlist_AccessRem_MemDirect_common(access);
}

// Check if an access RA is hard-connected to a Register, return the register comp
netlist_comp_t* Netlist_Mem_AccessRA_GetReadBuf(netlist_access_t* access) {
	assert(access->model==NETLIST_ACCESS_MEM_RA);

	netlist_access_mem_addr_t* access_addr = access->data;
	// Unused port?
	if(Netlist_PortTargets_CheckEmpty(access_addr->port_d)==true) return NULL;
	// There should be only one target
	if(avl_p_count(access_addr->port_d->target_ports) != 1) return NULL;

	// Get the destination port, ensure it is a register
	netlist_port_t* port = access_addr->port_d->target_ports->root->data;
	netlist_comp_t* compReg = port->component;
	if(compReg->model != NETLIST_COMP_REGISTER) return NULL;

	// The data input of the Reg must be the full data width of the RA access
	netlist_register_t* reg_data = compReg->data;
	if(reg_data->port_in->source==NULL) return NULL;
	if(reg_data->port_in->source->NEXT!=NULL) return NULL;
	netlist_val_cat_t* valcat = reg_data->port_in->source->DATA;
	if(valcat->source != access_addr->port_d) return NULL;
	if(valcat->width != access_addr->port_d->width) return NULL;
	if(valcat->right != 0 || valcat->left != access_addr->port_d->width-1) return NULL;  // in case repeated valcat

	return compReg;
}
bool Netlist_Mem_IsSimpleSyncRead(netlist_comp_t* comp) {
	netlist_memory_t* comp_mem = comp->data;
	if(comp_mem->ports_cam_nb != 0) return false;
	if(comp_mem->ports_wa_nb > 1) return false;
	if(comp_mem->direct_en==true) return false;

	// Scan all RA accesses
	// Check if all are is hard-connected to a Register
	avl_p_foreach(&comp_mem->access_ports_ra, scanAccess) {
		netlist_access_t* access = scanAccess->data;
		netlist_access_mem_addr_t* access_addr = access->data;
		// Unused port?
		if(Netlist_PortTargets_CheckEmpty(access_addr->port_d)==true) continue;
		// Get the target Register buffer
		netlist_comp_t* compReg = Netlist_Mem_AccessRA_GetReadBuf(access);
		if(compReg==NULL) return false;
	}

	// Here all RA accesses perform a synchronous read
	return true;
}

netlist_access_model_t* Netlist_Access_MemRA_GetModel() {
	netlist_access_model_t* model = Netlist_AccessModel_New();
	model->name = namealloc("mem_ra");

	model->data_size = sizeof(netlist_access_mem_addr_t);

	char* port_name = NULL;
	port_name = namealloc("data");
	Netlist_AccessModel_Init_PortName(model, netlist_access_mem_addr_t, port_d, port_name);
	port_name = namealloc("addr");
	Netlist_AccessModel_Init_PortName(model, netlist_access_mem_addr_t, port_a, port_name);

	NETLIST_ACCESS_MEM_RA = model;
	return model;
}
netlist_access_model_t* Netlist_Access_MemWA_GetModel() {
	netlist_access_model_t* model = Netlist_AccessModel_New();
	model->name = namealloc("mem_wa");

	model->data_size = sizeof(netlist_access_mem_addr_t);

	char* port_name = NULL;
	port_name = namealloc("data");
	Netlist_AccessModel_Init_PortName(model, netlist_access_mem_addr_t, port_d, port_name);
	port_name = namealloc("addr");
	Netlist_AccessModel_Init_PortName(model, netlist_access_mem_addr_t, port_a, port_name);
	port_name = namealloc("en");
	Netlist_AccessModel_Init_PortName(model, netlist_access_mem_addr_t, port_e, port_name);

	NETLIST_ACCESS_MEM_WA = model;
	return model;
}
netlist_access_model_t* Netlist_Access_MemD_GetModel() {
	netlist_access_model_t* model = Netlist_AccessModel_New();
	model->name = namealloc("mem_d");

	model->data_size = sizeof(netlist_access_mem_direct_t);

	char* port_name = NULL;
	port_name = namealloc("wd");
	Netlist_AccessModel_Init_PortName(model, netlist_access_mem_direct_t, port_wd, port_name);
	port_name = namealloc("we");
	Netlist_AccessModel_Init_PortName(model, netlist_access_mem_direct_t, port_we, port_name);
	port_name = namealloc("rd");
	Netlist_AccessModel_Init_PortName(model, netlist_access_mem_direct_t, port_rd, port_name);

	NETLIST_ACCESS_MEM_D = model;
	return model;
}
netlist_access_model_t* Netlist_Access_MemCam_GetModel() {
	netlist_access_model_t* model = Netlist_AccessModel_New();
	model->name = namealloc("mem_d");

	model->data_size = sizeof(netlist_access_mem_cam_t);

	char* port_name = NULL;
	port_name = namealloc("data");
	Netlist_AccessModel_Init_PortName(model, netlist_access_mem_cam_t, port_data, port_name);
	port_name = namealloc("mask");
	Netlist_AccessModel_Init_PortName(model, netlist_access_mem_cam_t, port_mask, port_name);
	port_name = namealloc("out");
	Netlist_AccessModel_Init_PortName(model, netlist_access_mem_cam_t, port_out, port_name);

	NETLIST_ACCESS_MEM_CAM = model;
	return model;
}

netlist_access_model_t* Netlist_Access_MemRA_Ext_GetModel() {
	netlist_access_model_t* model = Netlist_AccessModel_New();
	model->name = namealloc("mem_ra_ext");

	model->data_size = sizeof(netlist_access_mem_addr_t);

	char* port_name = NULL;
	port_name = namealloc("data");
	Netlist_AccessModel_Init_PortName(model, netlist_access_mem_addr_t, port_d, port_name);
	port_name = namealloc("addr");
	Netlist_AccessModel_Init_PortName(model, netlist_access_mem_addr_t, port_a, port_name);

	NETLIST_ACCESS_MEM_RA_EXT = model;
	return model;
}
netlist_access_model_t* Netlist_Access_MemWA_Ext_GetModel() {
	netlist_access_model_t* model = Netlist_AccessModel_New();
	model->name = namealloc("mem_wa_ext");

	model->data_size = sizeof(netlist_access_mem_addr_t);

	char* port_name = NULL;
	port_name = namealloc("data");
	Netlist_AccessModel_Init_PortName(model, netlist_access_mem_addr_t, port_d, port_name);
	port_name = namealloc("addr");
	Netlist_AccessModel_Init_PortName(model, netlist_access_mem_addr_t, port_a, port_name);
	port_name = namealloc("en");
	Netlist_AccessModel_Init_PortName(model, netlist_access_mem_addr_t, port_e, port_name);

	NETLIST_ACCESS_MEM_WA_EXT = model;
	return model;
}

/* VHDL generation

If there is only one Write port (Address+Direct), and there are some AR ports,
an embedded LUTRAM is created. The AR ports are only linked to the LUTRAM.
Additionally, if there are direct access ports, the corresponding registers are created.

In all other cases, there is no LUTRAM, all is based on the registers.
*/
static void Netlist_Comp_Mem_GenVHDL(FILE* F, netlist_vhdl_t* params, netlist_comp_t* comp) {

	fprintf(F,
		"\n"
		"library ieee;\n"
		"use ieee.numeric_std.all;\n"
		"\n"
	);

	Netlist_GenVHDL_Entity(F, comp, params);

	netlist_memory_t* mem_data = comp->data;

	// Decide what will be created
	bool create_lutram = true;
	bool create_regs_all = false;
	bool is_rom = true;

	unsigned number_DW_ports = 0;
	avl_ip_foreach(&mem_data->access_ports_d, scan) {
		netlist_access_t* access = scan->data;
		netlist_access_mem_direct_t* memD = access->data;
		if(memD->port_wd!=NULL) { number_DW_ports++; is_rom = false; }
	}

	if(number_DW_ports + mem_data->ports_wa_nb > 1) create_lutram = false;
	if(mem_data->ports_ra_nb==0) create_lutram = false;

	if(mem_data->ports_wa_nb>0) is_rom = false;

	if(create_lutram==false && mem_data->ports_ra_nb>0) create_regs_all = true;
	if(mem_data->ports_cam_nb>0) create_regs_all = true;

	// Write the architecture

	fprintf(F, "architecture %s of %s is\n", params->architecture, Netlist_Comp_GetRename_Comp(comp, params));
	fputc('\n', F);

	if(create_lutram==true) {
		ftabprintf(F, 1, "-- Embedded RAM\n");
		fputc('\n', F);
		ftabprintf(F, 1, "type ram_type is array (0 to %u) of ", mem_data->cells_nb-1);
		Netlist_GenVHDL_StdLogicType_be(F, mem_data->data_width, NULL, ";\n");
		ftabprintf(F, 1, "signal ram : ram_type");

		if(mem_data->init_vex!=NULL) {
			fprintf(F, " := (\n\t\t");

			unsigned line_val_max = 120 / (mem_data->data_width + 4);
			if(line_val_max<1) line_val_max = 1;

			bool do_nl = false;    // To insert a newline
			bool is_first = true;  // First in the line
			unsigned line_val_cur = 0;

			for(unsigned i=0; i<mem_data->cells_nb; i++) {
				if(do_nl==true) { fprintf(F, ",\n\t\t"); do_nl = false; }
				if(is_first==true) is_first = false; else fprintf(F, ", ");
				hvex_t* Expr = mem_data->init_vex[i];
				if(Expr!=NULL) {
					Netlist_GenVHDL_StdLogic_Literal(F, hvex_lit_getval(Expr));
				}
				else {
					Netlist_GenVHDL_StdLogic_Literal_Repeat(F, mem_data->data_width, '0');
				}
				line_val_cur ++;
				if(line_val_max>0 && line_val_cur>=line_val_max) { line_val_cur = 0; do_nl = true; is_first = true; }
			}

			fprintf(F, "\n\t)");
		}
		else {
			Netlist_GenVHDL_StdLogic_InitArray(F, mem_data->data_width, default_store_value);
		}

		fprintf(F, ";\n");
		fputc('\n', F);
	}
	if(create_lutram==false && mem_data->ports_ra_nb>0) {
		ftabprintf(F, 1, "-- Signal to aggregate internal registers for the address read ports\n");
		fputc('\n', F);
		ftabprintf(F, 1, "type regsall_type is array (0 to %u) of ", mem_data->cells_nb-1);
		Netlist_GenVHDL_StdLogicType_be(F, mem_data->data_width, NULL, ";\n");
		ftabprintf(F, 1, "signal regs_all : regsall_type");
		Netlist_GenVHDL_StdLogic_InitArray(F, mem_data->data_width, default_store_value);
		fprintf(F, ";\n");
		fputc('\n', F);
	}

	void write_create_reg(unsigned i) {
		ftabprintf(F, 1, "signal reg%u : ", i);
		Netlist_GenVHDL_StdLogicType(F, mem_data->data_width);

		if(mem_data->init_vex!=NULL) {
			fprintf(F, " := ");
			hvex_t* Expr = mem_data->init_vex[i];
			if(Expr!=NULL) {
				Netlist_GenVHDL_StdLogic_Literal(F, hvex_lit_getval(Expr));
			}
			else {
				Netlist_GenVHDL_StdLogic_Literal_Repeat(F, mem_data->data_width, '0');
			}
		}

		else {
			Netlist_GenVHDL_StdLogic_Init(F, mem_data->data_width, default_store_value);
		}
		fprintf(F, ";\n");
	}

	if(create_regs_all==true) {
		ftabprintf(F, 1, "-- The independent registers\n");
		fputc('\n', F);
		for(unsigned i=0; i<mem_data->cells_nb; i++) {
			write_create_reg(i);
		}
		fputc('\n', F);
	}
	else if(avl_ip_isempty(&mem_data->access_ports_d)==false) {
		ftabprintf(F, 1, "-- The independent registers\n");
		fputc('\n', F);
		avl_ip_foreach(&mem_data->access_ports_d, scan) {
			netlist_access_t* access = scan->data;
			netlist_access_mem_direct_t* memD = access->data;
			write_create_reg(memD->cell_index);
		}
		fputc('\n', F);
	}

	fputc('\n', F);

	Netlist_GenVHDL_MyToInteger(F);

	fprintf(F,
		"\n"
		"begin\n"
		"\n"
	);

	if(is_rom==false) {

		// The sequential process

		fprintf(F,
			"\t-- Sequential process\n"
			"\t-- It handles the Writes\n"
			"\n"
		);

		ftabprintf(F, 1, "process (%s)\n", mem_data->port_clk->name);

		if(create_regs_all==true || avl_ip_isempty(&mem_data->access_ports_d)==false) {
			fputc('\n', F);
			ftabprintf(F, 2, "variable var_data : ");
			Netlist_GenVHDL_StdLogicType_be(F, mem_data->data_width, NULL, ";\n");
			ftabprintf(F, 2, "variable var_en   : std_logic;\n");
			fputc('\n', F);
		}

		ftabprintf(F, 1, "begin\n");
		ftabprintf(F, 2, "if %s(%s) then\n", Netlist_GenVHDL_ClockEdge_TestFunc_port(mem_data->port_clk), mem_data->port_clk->name);
		fputc('\n', F);

		if(create_lutram==true) {
			ftabprintf(F, 3, "-- Write to the RAM\n");
			ftabprintf(F, 3, "-- Note: there should be only one port.\n");
			fputc('\n', F);
			avl_p_foreach(&mem_data->access_ports_wa, scan) {
				netlist_access_t* access = scan->data;
				netlist_access_mem_addr_t* memWA = access->data;
				ftabprintf(F, 3, "if %s = '1' then\n", memWA->port_e->name);
				ftabprintf(F, 4, "ram( to_integer(%s) ) <= %s;\n", memWA->port_a->name, memWA->port_d->name);
				ftabprintf(F, 3, "end if;\n");
			}
			avl_ip_foreach(&mem_data->access_ports_d, scan) {
				netlist_access_t* access = scan->data;
				netlist_access_mem_direct_t* memD = access->data;
				ftabprintf(F, 3, "if %s = '1' then\n", memD->port_we->name);
				ftabprintf(F, 4, "ram(%u) ) <= %s;\n", memD->cell_index, memD->port_wd->name);
				ftabprintf(F, 3, "end if;\n");
			}
			fputc('\n', F);
		}

		if(create_regs_all==true || avl_ip_isempty(&mem_data->access_ports_d)==false) {
			ftabprintf(F, 3, "-- Write to the registers\n");
			fputc('\n', F);

			if(create_regs_all==true) {
				for(unsigned i=0; i<mem_data->cells_nb; i++) {
					ftabprintf(F, 3, "var_data := (others => '0');\n");
					ftabprintf(F, 3, "var_en   := '0';\n");
					// All Address Write ports
					avl_p_foreach(&mem_data->access_ports_wa, scan_wa) {
						netlist_access_t* access = scan_wa->data;
						netlist_access_mem_addr_t* memWA = access->data;
						ftabprintf(F, 3, "if %s = '1' and to_integer(%s) = %u then\n", memWA->port_e->name, memWA->port_a->name, i);
						ftabprintf(F, 4, "var_data := var_data or %s;\n", memWA->port_d->name);
						ftabprintf(F, 4, "var_en   := '1';\n");
						ftabprintf(F, 3, "end if;\n");
					}
					// The local direct write port
					netlist_access_t* accessD = NULL;
					avl_ip_find_data(&mem_data->access_ports_d, i, (void**)&accessD);
					if(accessD!=NULL) {
						netlist_access_mem_direct_t* memD = accessD->data;
						if(memD->port_wd!=NULL) {
							ftabprintf(F, 3, "if %s = '1' then\n", memD->port_we->name);
							ftabprintf(F, 4, "var_data := var_data or %s;\n", memD->port_wd->name);
							ftabprintf(F, 4, "var_en   := '1';\n");
							ftabprintf(F, 3, "end if;\n");
						}
					}
					// The actual register assignment
					ftabprintf(F, 3, "if var_en = '1' then reg%u <= var_data; end if;\n", i);
					fputc('\n', F);
				}
			}
			else {
				avl_ip_foreach(&mem_data->access_ports_d, scan_d) {
					netlist_access_t* access = scan_d->data;
					netlist_access_mem_direct_t* memD = access->data;
					ftabprint(F, 3);
					if(mem_data->data_width==1) fprintf(F, "var_data := '0';\n");
					else fprintf(F, "var_data := (others => '0');\n");
					ftabprintf(F, 3, "var_en   := '0';\n");
					// All Address Write ports
					avl_p_foreach(&mem_data->access_ports_wa, scan_wa) {
						netlist_access_t* accessWA = scan_wa->data;
						netlist_access_mem_addr_t* memWA = accessWA->data;
						ftabprintf(F, 3, "if %s = '1' and to_integer(%s) = %u then\n", memWA->port_e->name, memWA->port_a->name, memD->cell_index);
						ftabprintf(F, 4, "var_data := var_data or %s;\n", memWA->port_d->name);
						ftabprintf(F, 4, "var_en   := '1';\n");
						ftabprintf(F, 3, "end if;\n");
					}
					// The local direct write port
					if(memD->port_wd!=NULL) {
						ftabprintf(F, 3, "if %s = '1' then\n", memD->port_we->name);
						ftabprintf(F, 4, "var_data := var_data or %s;\n", memD->port_wd->name);
						ftabprintf(F, 4, "var_en   := '1';\n");
						ftabprintf(F, 3, "end if;\n");
					}
					// The actual register assignment
					ftabprintf(F, 3, "if var_en = '1' then reg%u <= var_data; end if;\n", memD->cell_index);
					fputc('\n', F);
				}
			}

		}

		// End of the sequential process
		ftabprintf(F, 2, "end if;\n");
		ftabprintf(F, 1, "end process;\n");
		fputc('\n', F);

	}  // if(is_rom==false)
	else {
		fprintf(F,
			"\t-- The component is a ROM.\n"
			"\t-- There is no Write side.\n"
			"\n"
		);
	}
	// The READ side

	fprintf(F,
		"\t-- The Read side (the outputs)\n"
		"\n"
	);

	// The address read ports

	if(create_lutram==true) {
		avl_p_foreach(&mem_data->access_ports_ra, scan) {
			netlist_access_t* access = scan->data;
			netlist_access_mem_addr_t* memRA = access->data;
			// Handle when the range of the address port is higher than the memory size
			if(memRA->port_a->width > uint_log_floor(2, mem_data->cells_nb)) {
				ftabprintf(F, 1, "%s <= ram( to_integer(%s) ) when to_integer(%s) < %u else ",
					memRA->port_d->name, memRA->port_a->name, memRA->port_a->name, mem_data->cells_nb
				);
				if(vhd2vl_friendly==false) {
					if(mem_data->data_width > 1) fprintf(F, "(others => '-')");
					else fprintf(F, "'-'");
				}
				else {
					if(mem_data->data_width > 1) fprintf(F, "(others => '0')");
					else fprintf(F, "'0'");
				}
				fprintf(F, ";\n");
			}
			else {
				ftabprintf(F, 1, "%s <= ram( to_integer(%s) );\n", memRA->port_d->name, memRA->port_a->name);
			}
		}
	}
	else if(mem_data->ports_ra_nb>0) {
		// All registers exist
		for(unsigned i=0; i<mem_data->cells_nb; i++) {
			ftabprintf(F, 1, "regs_all(%u) <= reg%u;\n", i, i);
		}
		fputc('\n', F);
		avl_p_foreach(&mem_data->access_ports_ra, scan) {
			netlist_access_t* access = scan->data;
			netlist_access_mem_addr_t* memRA = access->data;
			// Handle when the range of the address port is higher than the memory size
			if(memRA->port_a->width > uint_log_floor(2, mem_data->cells_nb)) {
				ftabprintf(F, 1, "%s <= regs_all( to_integer(%s) ) when to_integer(%s) < %u else ",
					memRA->port_d->name, memRA->port_a->name, memRA->port_a->name, mem_data->cells_nb
				);
				if(vhd2vl_friendly==false) {
					if(mem_data->data_width > 1) fprintf(F, "(others => '-')");
					else fprintf(F, "'-'");
				}
				else {
					if(mem_data->data_width > 1) fprintf(F, "(others => '0')");
					else fprintf(F, "'0'");
				}
				fprintf(F, ";\n");
			}
			else {
				ftabprintf(F, 1, "%s <= regs_all( to_integer(%s) );\n", memRA->port_d->name, memRA->port_a->name);
			}
		}
	}
	if(mem_data->ports_ra_nb>0) {
		fputc('\n', F);
	}

	// The direct read ports

	bool things_written = false;
	avl_ip_foreach(&mem_data->access_ports_d, scan) {
		netlist_access_t* access = scan->data;
		netlist_access_mem_direct_t* memD = access->data;
		if(memD->port_rd==NULL) continue;
		ftabprintf(F, 1, "%s <= reg%u;\n", memD->port_rd->name, memD->cell_index);
		things_written = true;
	}
	if(things_written==true) {
		fputc('\n', F);
	}

	// The content-access ports (CAM)

	things_written = false;
	avl_p_foreach(&mem_data->access_ports_cam, scan) {
		netlist_access_t* access = scan->data;
		netlist_access_mem_cam_t* memCam = access->data;
		// We know that all registers exist
		// The written VHDL is :   out(<idx>) <= '1' when (reg<idx> and mask) = (data and mask) else '0';
		for(unsigned i=0; i<mem_data->cells_nb; i++) {
			ftabprintf(F, 3, "%s(%u) <= '1' when (reg%u and ", memCam->port_out->name, i, i);
			if(memCam->port_mask!=NULL) fprintf(F, "%s", memCam->port_mask->name);
			else Netlist_GenVHDL_StdLogic_Literal(F, memCam->internal_mask);
			fprintf(F, ") = (");
			if(memCam->port_data!=NULL) fprintf(F, "%s", memCam->port_data->name);
			else Netlist_GenVHDL_StdLogic_Literal(F, memCam->internal_data);
			fprintf(F, " and ");
			if(memCam->port_mask!=NULL) fprintf(F, "%s", memCam->port_mask->name);
			else Netlist_GenVHDL_StdLogic_Literal(F, memCam->internal_mask);
			fprintf(F, ") else '0';\n");
		}
		fputc('\n', F);
		things_written = true;
	}
	if(things_written==true) {
		fputc('\n', F);
	}

	fprintf(F,
		"end architecture;\n"
	);
}

netlist_comp_model_t* Netlist_Comp_Mem_GetModel() {
	netlist_comp_model_t* model = Netlist_CompModel_New();
	model->name = namealloc("mem");

	model->data_size         = sizeof(netlist_memory_t);
	model->func_free_data    = Netlist_Comp_Mem_Free_Data;
	model->func_dup_internal = Netlist_Comp_Mem_Dup;
	model->func_gen_vhdl     = Netlist_Comp_Mem_GenVHDL;

	model->func_simp         = (netlist_comp_cb_simp_t)Netlist_Simp_Comp_Mem;

	NETLIST_COMP_MEMORY = model;
	return model;
}



//===================================================
// ADD, SUB, ALU
//===================================================

netlist_comp_model_t* NETLIST_COMP_ADD = NULL;
netlist_comp_model_t* NETLIST_COMP_SUB = NULL;

static netlist_comp_t* Netlist_Asb_New_internal(netlist_comp_model_t* model, char* name, unsigned width, bool with_ci, bool with_co) {
	netlist_comp_t* comp = Netlist_Comp_New(model);
	comp->name = name;

	netlist_asb_t* comp_asb = comp->data;

	comp_asb->width = width;
	comp_asb->default_ci = '0';

	char* port_name;

	port_name = namealloc("result");
	comp_asb->port_out = Netlist_Comp_AddPort_Out(comp, port_name, width);
	port_name = namealloc("in_a");
	comp_asb->port_inA = Netlist_Comp_AddPort_In(comp, port_name, width);
	port_name = namealloc("in_b");
	comp_asb->port_inB = Netlist_Comp_AddPort_In(comp, port_name, width);

	if(with_ci==true) {
		port_name = namealloc("ci");
		comp_asb->port_ci = Netlist_Comp_AddPort_In(comp, port_name, 1);
	}
	if(with_co==true) {
		port_name = namealloc("co");
		comp_asb->port_co = Netlist_Comp_AddPort_Out(comp, port_name, 1);
	}

	return comp;
}

netlist_comp_t* Netlist_Add_New(char* name, unsigned width, bool with_ci, bool with_co) {
	return Netlist_Asb_New_internal(NETLIST_COMP_ADD, name, width, with_ci, with_co);
}
netlist_comp_t* Netlist_Sub_New(char* name, unsigned width, bool with_ci, bool with_co) {
	return Netlist_Asb_New_internal(NETLIST_COMP_SUB, name, width, with_ci, with_co);
}
#if 0
netlist_comp_t* Netlist_Alu_New(char* name, unsigned width, bool with_ci, bool with_co) {
	netlist_comp_t* comp = Netlist_Asb_New_internal(NETLIST_COMP_ALU, name, width, with_ci, with_co);
	Netlist_Asb_EnableFSM(comp);
	return comp;
}
#endif

void Netlist_Asb_SetDefaultCI(netlist_comp_t* comp, char bit) {
	netlist_asb_t* comp_asb = comp->data;
	comp_asb->default_ci = bit;
}

void Netlist_Asb_EnableSign(netlist_comp_t* comp, bool ext_eq) {
	netlist_asb_t* comp_asb = comp->data;

	comp_asb->sign_extend = (netlist_asb_sign_t*)calloc(1, sizeof(netlist_asb_sign_t));
	netlist_asb_sign_t* sign_extend = comp_asb->sign_extend;

	char* port_name;

	port_name = namealloc("sign");
	sign_extend->port_sign = Netlist_Comp_AddPort_In(comp, port_name, 1);
	if(ext_eq==true) {
		port_name = namealloc("ext_eq");
		sign_extend->port_extern_eq = Netlist_Comp_AddPort_In(comp, port_name, 1);
	}

	port_name = namealloc("gt");
	sign_extend->port_gt = Netlist_Comp_AddPort_Out(comp, port_name, 1);
	port_name = namealloc("ge");
	sign_extend->port_ge = Netlist_Comp_AddPort_Out(comp, port_name, 1);
	port_name = namealloc("lt");
	sign_extend->port_lt = Netlist_Comp_AddPort_Out(comp, port_name, 1);
	port_name = namealloc("le");
	sign_extend->port_le = Netlist_Comp_AddPort_Out(comp, port_name, 1);

	port_name = namealloc("ov");
	sign_extend->port_ov = Netlist_Comp_AddPort_Out(comp, port_name, 1);
}
void Netlist_Asb_EnableFSM(netlist_comp_t* comp) {
	netlist_asb_t* comp_asb = comp->data;

	comp_asb->fsm_extend = (netlist_asb_fsm_t*)calloc(1, sizeof(netlist_asb_fsm_t));
	netlist_asb_fsm_t* fsm_extend = comp_asb->fsm_extend;

	char* port_name;

	port_name = namealloc("cmd");
	fsm_extend->port_fsm = Netlist_Comp_AddPort_In(comp, port_name, 3);

	// At most 8 operations are possible.
	// Because for the carry chain, the 2 outputs of the LUT6 must be used,
	// so the LUTS are actually LUT5.

	// No particular operation is enabled by default.
}

void Netlist_Asb_RemoveSign(netlist_comp_t* comp) {
	netlist_asb_t* asb_data = comp->data;
	netlist_asb_sign_t* asb_sign = asb_data->sign_extend;
	// Free input ports
	if(asb_sign->port_sign!=NULL) Netlist_Port_Free(asb_sign->port_sign);
	if(asb_sign->port_extern_eq!=NULL) Netlist_Port_Free(asb_sign->port_extern_eq);
	// Free output ports
	if(asb_sign->port_ov!=NULL) Netlist_Port_Free(asb_sign->port_ov);
	if(asb_sign->port_gt!=NULL) Netlist_Port_Free(asb_sign->port_gt);
	if(asb_sign->port_ge!=NULL) Netlist_Port_Free(asb_sign->port_ge);
	if(asb_sign->port_lt!=NULL) Netlist_Port_Free(asb_sign->port_lt);
	if(asb_sign->port_le!=NULL) Netlist_Port_Free(asb_sign->port_le);
	// Free the structure
	free(asb_data->sign_extend);
	asb_data->sign_extend = NULL;
}

static void Netlist_Asb_Free_Data(netlist_comp_t* comp) {
	netlist_asb_t* comp_asb = comp->data;
	if(comp_asb->sign_extend!=NULL) free(comp_asb->sign_extend);
	if(comp_asb->fsm_extend!=NULL) free(comp_asb->fsm_extend);
	free(comp_asb);
}
static void Netlist_Asb_Dup(avl_pp_tree_t* tree_old2new, netlist_comp_t* comp, netlist_comp_t* newcomp) {
	netlist_asb_t* asb_data = comp->data;
	netlist_asb_t* newasb_data = newcomp->data;

	newasb_data->width      = asb_data->width;
	newasb_data->default_ci = asb_data->default_ci;

	void* pointer_old2new(void* oldptr) {
		void* newptr = NULL;
		avl_pp_find_data(tree_old2new, oldptr, &newptr);
		return newptr;
	}

	newasb_data->port_out = pointer_old2new(asb_data->port_out);
	newasb_data->port_inA = pointer_old2new(asb_data->port_inA);
	newasb_data->port_inB = pointer_old2new(asb_data->port_inB);

	newasb_data->port_ci = pointer_old2new(asb_data->port_ci);
	newasb_data->port_co = pointer_old2new(asb_data->port_co);

	// The sign extension
	if(asb_data->sign_extend!=NULL) {
		newasb_data->sign_extend = (netlist_asb_sign_t*)calloc(1, sizeof(netlist_asb_sign_t));
		netlist_asb_sign_t* newsign_extend = newasb_data->sign_extend;
		netlist_asb_sign_t* sign_extend = asb_data->sign_extend;

		newsign_extend->port_sign = pointer_old2new(sign_extend->port_sign);

		newsign_extend->port_extern_eq = pointer_old2new(sign_extend->port_extern_eq);

		newsign_extend->port_ov = pointer_old2new(sign_extend->port_ov);
		newsign_extend->port_gt = pointer_old2new(sign_extend->port_gt);
		newsign_extend->port_ge = pointer_old2new(sign_extend->port_ge);
		newsign_extend->port_lt = pointer_old2new(sign_extend->port_lt);
		newsign_extend->port_le = pointer_old2new(sign_extend->port_le);
	}
}

static void Netlist_Asb_GenVHDL(FILE* F, netlist_vhdl_t* params, netlist_comp_t* comp) {
	// ALU is not yet handled. Generating the LUTs is complicated.
	assert(comp->model==NETLIST_COMP_ADD || comp->model==NETLIST_COMP_SUB);

	fprintf(F,
		"library ieee;\n"
		"use ieee.numeric_std.all;\n"
		"\n"
	);

	Netlist_GenVHDL_Entity(F, comp, params);
	fputc('\n', F);

	netlist_asb_t* asb_data = comp->data;
	fprintf(F, "architecture %s of %s is\n", params->architecture, Netlist_Comp_GetRename_Comp(comp, params));
	fputc('\n', F);

	// Add 2 bits : one for CI, one for CO.
	// Even if they are not used (the netlist will be optimized by back-end tools)
	fprintf(F, "\tsignal carry_inA : std_logic_vector(%d downto 0);\n", asb_data->width+1);
	fprintf(F, "\tsignal carry_inB : std_logic_vector(%d downto 0);\n", asb_data->width+1);
	fprintf(F, "\tsignal carry_res : std_logic_vector(%d downto 0);\n", asb_data->width+1);

	// Add signals specific to the sign extension

	if(asb_data->sign_extend!=NULL) {
		fputc('\n', F);
		fprintf(F, "\t-- Signals to generate the comparison outputs\n");
		fprintf(F, "\tsignal msb_abr  : std_logic_vector(2 downto 0);\n");
		fprintf(F, "\tsignal tmp_sign : std_logic;\n");
		fprintf(F, "\tsignal tmp_eq   : std_logic;\n");
		fprintf(F, "\tsignal tmp_le   : std_logic;\n");
		fprintf(F, "\tsignal tmp_ge   : std_logic;\n");
	}

	fprintf(F,
		"\n"
		"begin\n"
		"\n"
	);

	if(comp->model==NETLIST_COMP_ADD) {
		fprintf(F, "\t-- To handle the CI input, the operation is '1' + CI\n");
		fprintf(F, "\t-- If CI is not present, the operation is '1' + '0'\n");
		fprintf(F, "\tcarry_inA <= '0' & %s & '1';\n", asb_data->port_inA->name);
		fprintf(F, "\tcarry_inB <= '0' & %s & ", asb_data->port_inB->name);
		if(asb_data->port_ci!=NULL) fprintf(F, "%s", asb_data->port_ci->name);
		else fprintf(F, "'%c'", asb_data->default_ci);
		fprintf(F, ";\n");
		fprintf(F, "\t-- Compute the result\n");
		fprintf(F, "\tcarry_res <= std_logic_vector(unsigned(carry_inA) + unsigned(carry_inB));\n");
	}

	if(comp->model==NETLIST_COMP_SUB) {
		fprintf(F, "\t-- To handle the CI input, the operation is '0' - CI\n");
		fprintf(F, "\t-- If CI is not present, the operation is '0' - '0'\n");
		fprintf(F, "\tcarry_inA <= '0' & %s & '0';\n", asb_data->port_inA->name);
		fprintf(F, "\tcarry_inB <= '0' & %s & ", asb_data->port_inB->name);
		if(asb_data->port_ci!=NULL) fprintf(F, "%s", asb_data->port_ci->name);
		else fprintf(F, "'%c'", asb_data->default_ci);
		fprintf(F, ";\n");
		fprintf(F, "\t-- Compute the result\n");
		fprintf(F, "\tcarry_res <= std_logic_vector(unsigned(carry_inA) - unsigned(carry_inB));\n");
	}

	fputc('\n', F);
	fprintf(F, "\t-- Set the outputs\n");
	if(asb_data->width > 1) {
		fprintf(F, "\t%s <= carry_res(%d downto 1);\n", asb_data->port_out->name, asb_data->width);
	}
	else {
		fprintf(F, "\t%s <= carry_res(%d);\n", asb_data->port_out->name, asb_data->width);
	}
	if(asb_data->port_co!=NULL) {
		fprintf(F, "\t%s <= carry_res(%d);\n", asb_data->port_co->name, asb_data->width+1);
	}

	if(asb_data->sign_extend!=NULL) {
		netlist_asb_sign_t* sign_extend = asb_data->sign_extend;

		fputc('\n', F);
		fprintf(F, "\t-- Other comparison outputs\n");
		fputc('\n', F);

		fprintf(F, "\t-- Temporary signals\n");
		fprintf(F, "\tmsb_abr <= carry_inA(%u) & carry_inB(%u) & carry_res(%u);\n", asb_data->width, asb_data->width, asb_data->width);
		fprintf(F, "\ttmp_sign <= %s;\n", sign_extend->port_sign->name);
		if(sign_extend->port_extern_eq==NULL) {
			fprintf(F, "\ttmp_eq  <= '1' when %s = %s else '0';\n", asb_data->port_inA->name, asb_data->port_inB->name);
		}
		else fprintf(F, "\ttmp_eq  <= %s;\n", sign_extend->port_extern_eq->name);
		fputc('\n', F);

		if(comp->model==NETLIST_COMP_ADD) {

			if(sign_extend->port_ov!=NULL) {
				ftabprintf(F, 1, "%s <=\n", sign_extend->port_ov->name);
				ftabprintf(F, 2, "'1' when tmp_sign = '0' and msb_abr = '100' else\n");
				ftabprintf(F, 2, "'1' when tmp_sign = '0' and msb_abr = '010' else\n");
				ftabprintf(F, 2, "'1' when tmp_sign = '0' and msb_abr = '110' else\n");
				ftabprintf(F, 2, "'1' when tmp_sign = '1' and msb_abr = '110' else\n");
				ftabprintf(F, 2, "'1' when tmp_sign = '1' and msb_abr = '001' else\n");
				ftabprintf(F, 2, "'0';\n");
			}
			if(sign_extend->port_gt!=NULL) {
				fprintf(F, "\t%s <= '0';\n", sign_extend->port_gt->name);
			}
			if(sign_extend->port_ge!=NULL) {
				fprintf(F, "\t%s <= '0';\n", sign_extend->port_ge->name);
			}
			if(sign_extend->port_lt!=NULL) {
				fprintf(F, "\t%s <= '0';\n", sign_extend->port_lt->name);
			}
			if(sign_extend->port_le!=NULL) {
				fprintf(F, "\t%s <= '0';\n", sign_extend->port_le->name);
			}

		}  // Case ADD

		if(comp->model==NETLIST_COMP_SUB) {

			/* Explanations on how to compute the outputs GT, GE, LT, LE, OV.

			Includes results for signed and unsigned.
			The symbols EQ and NEQ respectively mean A=B and A!=B.
				Note: LT = not(GE) and LE = not(GT).
			The column CO (carry out, or borrow) is only shown for information.
				Note: CO = not(GE) as unsigned.

			Truth table:
			+-------++----------------------------------------------------------------------+
			|  msb  ||               outputs                                                |
			+-------++-----------++-----------++-----------++-----------++-----------++-----+
			|       ||    GT     ||    GE     ||    LT     ||    LE     ||    OV     || CO  |
			| A B R ++-----------++-----------++-----------++-----------++-----------++-----+
			|       ||  U  |  S  ||  U  |  S  ||  U  |  S  ||  U  |  S  ||  U  |  S  ||     |
			+=======++=====+=====++=====+=====++=====+=====++=====+=====++=====+=====++=====+
			| 0 0 0 || NEQ | NEQ ||  1  |  1  ||  0  |  0  || EQ  | EQ  ||  0  |  0  ||  0  |
			+-------++-----+-----++-----+-----++-----+-----++-----+-----++-----+-----++-----+
			| 0 0 1 ||  0  |  0  ||  0  |  0  ||  1  |  1  ||  1  |  1  ||  1  |  0  ||  1  |
			+-------++-----+-----++-----+-----++-----+-----++-----+-----++-----+-----++-----+
			| 0 1 0 ||  0  |  1  ||  0  |  1  ||  1  |  0  ||  1  |  0  ||  0  |  0  ||  1  |
			+-------++-----+-----++-----+-----++-----+-----++-----+-----++-----+-----++-----+
			| 0 1 1 ||  0  |  1  ||  0  |  1  ||  1  |  0  ||  1  |  0  ||  1  |  1  ||  1  |
			+-------++-----+-----++-----+-----++-----+-----++-----+-----++-----+-----++-----+
			| 1 0 0 ||  1  |  0  ||  1  |  0  ||  0  |  1  ||  0  |  1  ||  0  |  1  ||  0  |
			+-------++-----+-----++-----+-----++-----+-----++-----+-----++-----+-----++-----+
			| 1 0 1 ||  1  |  0  ||  1  |  0  ||  0  |  1  ||  0  |  1  ||  0  |  0  ||  0  |
			+-------++-----+-----++-----+-----++-----+-----++-----+-----++-----+-----++-----+
			| 1 1 0 || NEQ | NEQ ||  1  |  1  ||  0  |  0  || EQ  | EQ  ||  0  |  0  ||  0  |
			+-------++-----+-----++-----+-----++-----+-----++-----+-----++-----+-----++-----+
			| 1 1 1 ||  0  |  0  ||  0  |  0  ||  1  |  1  ||  1  |  1  ||  1  |  0  ||  1  |
			+-------++-----+-----++-----+-----++-----+-----++-----+-----++-----+-----++-----+

			*/

			if(sign_extend->port_ov!=NULL) {
				ftabprintf(F, 1, "%s <=\n", sign_extend->port_ov->name);
				ftabprintf(F, 2, "'1' when tmp_sign = '0' and msb_abr = '001' else\n");
				ftabprintf(F, 2, "'1' when tmp_sign = '0' and msb_abr = '011' else\n");
				ftabprintf(F, 2, "'1' when tmp_sign = '0' and msb_abr = '111' else\n");
				ftabprintf(F, 2, "'1' when tmp_sign = '1' and msb_abr = '011' else\n");
				ftabprintf(F, 2, "'1' when tmp_sign = '1' and msb_abr = '100' else\n");
				ftabprintf(F, 2, "'0';\n");
			}

			ftabprintf(F, 1, "tmp_le <=\n");
			ftabprintf(F, 2, "tmp_eq when msb_abr = \"000\" or msb_abr = \"110\" else\n");
			ftabprintf(F, 2, "'1' when msb_abr = \"001\" or msb_abr = \"111\" else\n");
			ftabprintf(F, 2, "'1' when tmp_sign = '0' and (msb_abr = \"010\" or msb_abr = \"011\") else\n");
			ftabprintf(F, 2, "'1' when tmp_sign = '1' and (msb_abr = \"100\" or msb_abr = \"101\") else\n");
			ftabprintf(F, 2, "'0';\n");
			fputc('\n', F);

			ftabprintf(F, 1, "tmp_ge <=\n");
			ftabprintf(F, 2, "'1' when msb_abr = \"000\" or msb_abr = \"110\" else\n");
			ftabprintf(F, 2, "'1' when tmp_sign = '0' and (msb_abr = \"100\" or msb_abr = \"101\") else\n");
			ftabprintf(F, 2, "'1' when tmp_sign = '1' and (msb_abr = \"010\" or msb_abr = \"011\") else\n");
			ftabprintf(F, 2, "'0';\n");
			fputc('\n', F);

			if(sign_extend->port_gt!=NULL) {
				fprintf(F, "\t%s <= not(tmp_le);\n", sign_extend->port_gt->name);
			}
			if(sign_extend->port_ge!=NULL) {
				fprintf(F, "\t%s <= tmp_ge;\n", sign_extend->port_ge->name);
			}
			if(sign_extend->port_lt!=NULL) {
				fprintf(F, "\t%s <= not(tmp_ge);\n", sign_extend->port_lt->name);
			}
			if(sign_extend->port_le!=NULL) {
				fprintf(F, "\t%s <= tmp_le;\n", sign_extend->port_le->name);
			}

		}  // Case SUB

	}  // Sign extension

	fprintf(F,
		"\n"
		"end architecture;\n"
	);
}

netlist_comp_model_t* Netlist_Add_GetModel() {
	netlist_comp_model_t* model = Netlist_CompModel_New();
	model->name = namealloc("add");

	model->data_size         = sizeof(netlist_asb_t);
	model->func_free_data    = Netlist_Asb_Free_Data;
	model->func_dup_internal = Netlist_Asb_Dup;
	model->func_gen_vhdl     = Netlist_Asb_GenVHDL;

	model->func_simp         = (netlist_comp_cb_simp_t)Netlist_Simp_Comp_Asb;

	NETLIST_COMP_ADD = model;
	return model;
}
netlist_comp_model_t* Netlist_Sub_GetModel() {
	netlist_comp_model_t* model = Netlist_CompModel_New();
	model->name              = namealloc("sub");

	model->data_size         = sizeof(netlist_asb_t);
	model->func_free_data    = Netlist_Asb_Free_Data;
	model->func_dup_internal = Netlist_Asb_Dup;
	model->func_gen_vhdl     = Netlist_Asb_GenVHDL;

	model->func_simp         = (netlist_comp_cb_simp_t)Netlist_Simp_Comp_Asb;

	NETLIST_COMP_SUB = model;
	return model;
}



//===================================================
// MUL
//===================================================

netlist_comp_model_t* NETLIST_COMP_MUL = NULL;

netlist_comp_t* Netlist_Mul_New(char* name, unsigned width_inA, unsigned width_inB, unsigned width_out, bool is_signed) {
	netlist_comp_t* comp = Netlist_Comp_New(NETLIST_COMP_MUL);
	comp->name = name;

	netlist_mul_t* comp_mul = comp->data;

	comp_mul->width_out = width_out;
	comp_mul->width_inA = width_inA;
	comp_mul->width_inB = width_inB;
	comp_mul->is_signed = is_signed;

	char* port_name;

	port_name = namealloc("result");
	comp_mul->port_out = Netlist_Comp_AddPort_Out(comp, port_name, width_out);
	port_name = namealloc("in_a");
	comp_mul->port_inA = Netlist_Comp_AddPort_In(comp, port_name, width_inA);
	port_name = namealloc("in_b");
	comp_mul->port_inB = Netlist_Comp_AddPort_In(comp, port_name, width_inB);

	return comp;
}

static void Netlist_Mul_Dup(avl_pp_tree_t* tree_old2new, netlist_comp_t* comp, netlist_comp_t* newcomp) {
	netlist_mul_t* mul_data = comp->data;
	netlist_mul_t* newmul_data = newcomp->data;

	newmul_data->width_out = mul_data->width_out;
	newmul_data->width_inA = mul_data->width_inA;
	newmul_data->width_inB = mul_data->width_inB;
	newmul_data->is_signed = mul_data->is_signed;

	void* pointer_old2new(void* oldptr) {
		void* newptr = NULL;
		avl_pp_find_data(tree_old2new, oldptr, &newptr);
		return newptr;
	}

	newmul_data->port_out = pointer_old2new(mul_data->port_out);
	newmul_data->port_inA = pointer_old2new(mul_data->port_inA);
	newmul_data->port_inB = pointer_old2new(mul_data->port_inB);
}

static void Netlist_Mul_GenVHDL(FILE* F, netlist_vhdl_t* params, netlist_comp_t* comp) {
	netlist_mul_t* mul_data = comp->data;

	fprintf(F,
		"library ieee;\n"
		"use ieee.numeric_std.all;\n"
		"\n"
	);

	Netlist_GenVHDL_Entity(F, comp, params);

	char* datatype = mul_data->is_signed==true ? "signed" : "unsigned";
	unsigned tmpsize = mul_data->port_inA->width + mul_data->port_inB->width;

	fprintfm(F,
		"\n"
		"architecture %s of %s is\n", params->architecture, Netlist_Comp_GetRename_Comp(comp, params),
		"\n"
		"	signal tmp_res : %s(%d downto 0);\n", datatype, tmpsize - 1,
		"\n",
		NULL
	);

	Netlist_GenVHDL_MySignedUnsigned(F);

	fprintfm(F,
		"\n"
		"begin\n"
		"\n"
		"	-- The actual multiplication\n"
		"	tmp_res <= to_%s(%s) * to_%s(%s);\n", datatype, mul_data->port_inA->name, datatype, mul_data->port_inB->name,
		"\n"
		"	-- Set the output\n",
		NULL
	);

	if(mul_data->port_out->width <= tmpsize) {
		fprintf(F, "	%s <= std_logic_vector(tmp_res(%d downto 0));\n", mul_data->port_out->name, mul_data->width_out-1);
	}
	else {
		// Print the sign extension
		if(mul_data->is_signed==true) {
			fprintf(F, "	%s(%u downto %u) <= (others => tmp_res(%d));\n", mul_data->port_out->name, mul_data->width_out-1, tmpsize, tmpsize-1);
		}
		else {
			fprintf(F, "	%s(%u downto %u) <= (others => '0');\n", mul_data->port_out->name, mul_data->width_out-1, tmpsize);
		}
		// Print the part that reads the tmp signal
		fprintf(F, "	%s(%u downto 0) <= std_logic_vector(tmp_res);\n", mul_data->port_out->name, tmpsize-1);
	}

	fprintf(F,
		"\n"
		"end architecture;\n"
	);
}

netlist_comp_model_t* Netlist_Mul_GetModel() {
	netlist_comp_model_t* model = Netlist_CompModel_New();
	model->name = namealloc("mul");

	model->data_size         = sizeof(netlist_mul_t);
	model->func_dup_internal = Netlist_Mul_Dup;
	model->func_gen_vhdl     = Netlist_Mul_GenVHDL;

	NETLIST_COMP_MUL = model;
	return model;
}



//======================================================================
// Integer divisions
//======================================================================

netlist_comp_model_t* NETLIST_COMP_DIVQR = NULL;

netlist_comp_t* Netlist_DivQR_New(char* name, unsigned width_num, unsigned width_den) {
	netlist_comp_t* comp = Netlist_Comp_New(NETLIST_COMP_DIVQR);
	comp->name = name;

	netlist_divqr_t* comp_div = comp->data;

	comp_div->width_num = width_num;
	comp_div->width_den = width_den;

	char* port_name;

	port_name = namealloc("in_num");
	comp_div->port_in_num = Netlist_Comp_AddPort_In(comp, port_name, width_num);
	port_name = namealloc("in_den");
	comp_div->port_in_den = Netlist_Comp_AddPort_In(comp, port_name, width_den);

	port_name = namealloc("out_quo");
	comp_div->port_out_quo = Netlist_Comp_AddPort_Out(comp, port_name, width_num);
	port_name = namealloc("out_rem");
	comp_div->port_out_rem = Netlist_Comp_AddPort_Out(comp, port_name, width_den);

	return comp;
}

static void Netlist_DivQR_Dup(avl_pp_tree_t* tree_old2new, netlist_comp_t* comp, netlist_comp_t* newcomp) {
	netlist_divqr_t* div_data = comp->data;
	netlist_divqr_t* newdiv_data = newcomp->data;

	newdiv_data->width_num = div_data->width_num;
	newdiv_data->width_den = div_data->width_den;

	void* pointer_old2new(void* oldptr) {
		void* newptr = NULL;
		avl_pp_find_data(tree_old2new, oldptr, &newptr);
		return newptr;
	}

	newdiv_data->port_in_num  = pointer_old2new(div_data->port_in_num);
	newdiv_data->port_in_den  = pointer_old2new(div_data->port_in_den);
	newdiv_data->port_out_quo = pointer_old2new(div_data->port_out_quo);
	newdiv_data->port_out_rem = pointer_old2new(div_data->port_out_rem);
}

static void Netlist_DivQR_GenVHDL(FILE* F, netlist_vhdl_t* params, netlist_comp_t* comp) {
	netlist_divqr_t* div_data = comp->data;

	fprintf(F,
		"library ieee;\n"
		"use ieee.numeric_std.all;\n"
		"\n"
	);

	Netlist_GenVHDL_Entity(F, comp, params);
	fputc('\n', F);

	fprintfm(F,
		"architecture %s of %s is\n", params->architecture, Netlist_Comp_GetRename_Comp(comp, params),
		"begin\n"
		"\n"
		"	process(%s, %s)\n", div_data->port_in_num->name, div_data->port_in_den->name,
		"\n"
		"		-- Note: Use one extra bit to have the carry out\n"
		"		variable var_level_num : unsigned(%d downto 0);\n", div_data->width_den,
		"		variable var_level_den : unsigned(%d downto 0);\n", div_data->width_den,
		"		variable var_level_out : unsigned(%d downto 0);\n", div_data->width_den,
		"\n"
		"	begin\n"
		"\n",
		NULL
	);

	fprintf(F, "		-- Initialize output ports\n");
	if(div_data->port_out_quo!=NULL) {
		fprintf(F, "		%s <= (others => '0');\n", div_data->port_out_quo->name);
	}
	if(div_data->port_out_rem!=NULL) {
		fprintf(F, "		%s <= (others => '0');\n", div_data->port_out_rem->name);
	}

	fprintfm(F,
		"\n"
		"		-- Initialize the variable for the denominator\n"
		"		var_level_den(%u) := '0';\n", div_data->width_den,
		"		var_level_den(%u downto 0) := unsigned(%s);\n", div_data->width_den-1, div_data->port_in_den->name,
		"\n",
		NULL
	);

	// Print the first level
	fprintfm(F,
		"		-- Level 0\n"
		"		var_level_num := (others => '0');\n"
		"		var_level_num(0) := %s(%u);\n", div_data->port_in_num->name, div_data->width_num-1,
		"		var_level_out := var_level_num - var_level_den;\n",
		NULL
	);
	if(div_data->port_out_quo!=NULL) {
		fprintf(F, "		%s(%u) <= not var_level_out(%u);\n", div_data->port_out_quo->name, div_data->width_num-1, div_data->width_den);
	}
	fputc('\n', F);

	// Print the other levels
	for(unsigned i=1; i<div_data->width_num; i++) {
		fprintfm(F,
			"		-- Level %u\n", i,
			"		var_level_num(%u downto 1) := var_level_num(%u downto 0);\n", div_data->width_den, div_data->width_den-1,
			"		if var_level_out(%u) = '0' then\n", div_data->width_den,
			"			var_level_num(%u downto 1) := var_level_out(%u downto 0);\n", div_data->width_den, div_data->width_den-1,
			"		end if;\n"
			"		var_level_num(%u) := '0';\n", div_data->width_den,
			"		var_level_num(0) := %s(%u);\n", div_data->port_in_num->name, div_data->width_num-1-i,
			"		var_level_out := var_level_num - var_level_den;\n",
			NULL
		);
		if(div_data->port_out_quo!=NULL) {
			fprintf(F, "		%s(%u) <= not var_level_out(%u);\n", div_data->port_out_quo->name, div_data->width_num-1-i, div_data->width_den);
		}
		fputc('\n', F);
	}

	// Print the final assignment of the remainder
	if(div_data->port_out_rem!=NULL) {
		fprintfm(F,
			"		-- Final assignment of the remainder\n"
			"		%s <= std_logic_vector(var_level_num(%u downto 0));\n", div_data->port_out_rem->name, div_data->width_den-1,
			"		if var_level_out(%u) = '0' then\n", div_data->width_den,
			"			%s <= std_logic_vector(var_level_out(%u downto 0));\n", div_data->port_out_rem->name, div_data->width_den-1,
			"		end if;\n",
			NULL
		);
	}

	fprintf(F,
		"\n"
		"	end process;\n"
		"\n"
		"end architecture;\n"
		"\n"
	);
}

netlist_comp_model_t* Netlist_DivQR_GetModel() {
	netlist_comp_model_t* model = Netlist_CompModel_New();
	model->name = namealloc("divqr");

	model->data_size         = sizeof(netlist_divqr_t);
	model->func_dup_internal = Netlist_DivQR_Dup;
	model->func_gen_vhdl     = Netlist_DivQR_GenVHDL;

	model->func_simp         = (netlist_comp_cb_simp_t)Netlist_Simp_Comp_DivQR;

	NETLIST_COMP_DIVQR = model;
	return model;
}



//===================================================
// SHIFT
//===================================================

netlist_comp_model_t* NETLIST_COMP_SHL = NULL;
netlist_comp_model_t* NETLIST_COMP_SHR = NULL;
netlist_comp_model_t* NETLIST_COMP_ROTL = NULL;
netlist_comp_model_t* NETLIST_COMP_ROTR = NULL;

// Kind : <0 = left, 0 = signed, >0 = right
static netlist_comp_t* Netlist_Shift_New_raw(netlist_comp_model_t* model, char* name, unsigned width_data, unsigned width_shift) {
	netlist_comp_t* comp = Netlist_Comp_New(model);
	comp->name = name;

	netlist_shift_t* comp_shift = comp->data;

	comp_shift->width_data  = width_data;
	comp_shift->width_shift = width_shift;

	char* port_name;

	port_name = namealloc("data_out");
	comp_shift->port_out = Netlist_Comp_AddPort_Out(comp, port_name, width_data);
	port_name = namealloc("data_in");
	comp_shift->port_in = Netlist_Comp_AddPort_In(comp, port_name, width_data);
	port_name = namealloc("shift");
	comp_shift->port_shift = Netlist_Comp_AddPort_In(comp, port_name, width_shift);
	if(model==NETLIST_COMP_SHL || model==NETLIST_COMP_SHR) {
		port_name = namealloc("padding");
		comp_shift->port_padding = Netlist_Comp_AddPort_In(comp, port_name, 1);
	}

	return comp;
}

netlist_comp_t* Netlist_ShL_New(char* name, unsigned width_data, unsigned width_shift) {
	return Netlist_Shift_New_raw(NETLIST_COMP_SHL, name, width_data, width_shift);
}
netlist_comp_t* Netlist_ShR_New(char* name, unsigned width_data, unsigned width_shift) {
	return Netlist_Shift_New_raw(NETLIST_COMP_SHR, name, width_data, width_shift);
}
netlist_comp_t* Netlist_RotL_New(char* name, unsigned width_data, unsigned width_shift) {
	return Netlist_Shift_New_raw(NETLIST_COMP_ROTL, name, width_data, width_shift);
}
netlist_comp_t* Netlist_RotR_New(char* name, unsigned width_data, unsigned width_shift) {
	return Netlist_Shift_New_raw(NETLIST_COMP_ROTR, name, width_data, width_shift);
}

static void Netlist_Shift_Dup(avl_pp_tree_t* tree_old2new, netlist_comp_t* comp, netlist_comp_t* newcomp) {
	netlist_shift_t* shift_data = comp->data;
	netlist_shift_t* newshift_data = newcomp->data;

	newshift_data->width_data  = shift_data->width_data;
	newshift_data->width_shift = shift_data->width_shift;

	void* pointer_old2new(void* oldptr) {
		void* newptr = NULL;
		avl_pp_find_data(tree_old2new, oldptr, &newptr);
		return newptr;
	}

	newshift_data->port_out      = pointer_old2new(shift_data->port_out);
	newshift_data->port_in       = pointer_old2new(shift_data->port_in);
	newshift_data->port_shift    = pointer_old2new(shift_data->port_shift);
	newshift_data->port_padding  = pointer_old2new(shift_data->port_padding);
}

static void Netlist_Shift_GenVHDL(FILE* F, netlist_vhdl_t* params, netlist_comp_t* comp) {
	netlist_shift_t* shift_data = comp->data;

	fprintf(F,
		"library ieee;\n"
		"use ieee.numeric_std.all;\n"
		"\n"
	);

	Netlist_GenVHDL_Entity(F, comp, params);
	fputc('\n', F);

	fprintf(F, "architecture %s of %s is\n", params->architecture, Netlist_Comp_GetRename_Comp(comp, params));

	if(comp->model==NETLIST_COMP_SHL || comp->model==NETLIST_COMP_SHR) {
		fputc('\n', F);
		fprintf(F, "\tsignal tmp_padding : std_logic;\n");
		fprintf(F, "\tsignal tmp_result  : std_logic_vector(%u downto 0);\n", shift_data->width_data);
	}

	fputc('\n', F);

	Netlist_GenVHDL_MyToInteger(F);

	fprintf(F,
		"\n"
		"begin\n"
		"\n"
	);

	if(comp->model==NETLIST_COMP_SHL || comp->model==NETLIST_COMP_SHR) {
		fprintf(F, "\t-- Temporary signals\n");
		if(shift_data->port_padding!=NULL) {
			fprintf(F, "\ttmp_padding <= %s;\n", shift_data->port_padding->name);
		}
		else fprintf(F, "\ttmp_padding <= '0';\n");

		if(comp->model==NETLIST_COMP_SHL) {
			fprintf(F, "\ttmp_result <= std_logic_vector(shift_left( unsigned(%s & padding), to_integer(%s) ));\n", shift_data->port_in->name, shift_data->port_shift->name);
		}
		if(comp->model==NETLIST_COMP_SHR) {
			fprintf(F, "\ttmp_result <= std_logic_vector(shift_right( unsigned(padding & %s), to_integer(%s) ));\n", shift_data->port_in->name, shift_data->port_shift->name);
		}
		fputc('\n', F);

		fprintf(F, "\t-- The output\n");

		if(comp->model==NETLIST_COMP_SHL) {
			fprintf(F, "\t%s <= tmp_result(%d downto 1);\n", shift_data->port_out->name, shift_data->width_data);
		}
		if(comp->model==NETLIST_COMP_SHR) {
			fprintf(F, "\t%s <= tmp_result(%d downto 0);\n", shift_data->port_out->name, shift_data->width_data-1);
		}
	}

	if(comp->model==NETLIST_COMP_ROTL || comp->model==NETLIST_COMP_ROTL) {
		fprintf(F, "\t%s <= std_logic_vector( unsigned(%s) ", shift_data->port_out->name, shift_data->port_in->name);
		if(comp->model==NETLIST_COMP_ROTL) fprintf(F, "rol");
		if(comp->model==NETLIST_COMP_ROTR) fprintf(F, "ror");
		fprintf(F, " to_integer(%s) );\n", shift_data->port_shift->name);
	}

	fprintf(F,
		"\n"
		"end architecture;\n"
	);
}

netlist_comp_model_t* Netlist_ShL_GetModel() {
	netlist_comp_model_t* model = Netlist_CompModel_New();
	model->name = namealloc("shl");

	model->data_size         = sizeof(netlist_shift_t);
	model->func_dup_internal = Netlist_Shift_Dup;
	model->func_gen_vhdl     = Netlist_Shift_GenVHDL;

	NETLIST_COMP_SHL = model;
	return model;
}
netlist_comp_model_t* Netlist_ShR_GetModel() {
	netlist_comp_model_t* model = Netlist_CompModel_New();
	model->name = namealloc("shr");

	model->data_size         = sizeof(netlist_shift_t);
	model->func_dup_internal = Netlist_Shift_Dup;
	model->func_gen_vhdl     = Netlist_Shift_GenVHDL;

	NETLIST_COMP_SHR = model;
	return model;
}
netlist_comp_model_t* Netlist_RotL_GetModel() {
	netlist_comp_model_t* model = Netlist_CompModel_New();
	model->name = namealloc("rotl");

	model->data_size         = sizeof(netlist_shift_t);
	model->func_dup_internal = Netlist_Shift_Dup;
	model->func_gen_vhdl     = Netlist_Shift_GenVHDL;

	NETLIST_COMP_ROTL = model;
	return model;
}
netlist_comp_model_t* Netlist_RotR_GetModel() {
	netlist_comp_model_t* model = Netlist_CompModel_New();
	model->name = namealloc("rotr");

	model->data_size         = sizeof(netlist_shift_t);
	model->func_dup_internal = Netlist_Shift_Dup;
	model->func_gen_vhdl     = Netlist_Shift_GenVHDL;

	NETLIST_COMP_ROTR = model;
	return model;
}



//===================================================
// CMP
//===================================================

netlist_comp_model_t* NETLIST_COMP_CMP = NULL;

netlist_comp_t* Netlist_Cmp_New_Raw(char* name, unsigned width) {
	netlist_comp_t* comp = Netlist_Comp_New(NETLIST_COMP_CMP);
	comp->name = name;

	netlist_cmp_t* comp_cmp = comp->data;

	comp_cmp->width = width;
	comp_cmp->inputs_nb = 0;

	char* name_alloc;

	name_alloc = namealloc("eq");
	comp_cmp->port_eq = Netlist_Comp_AddPort_Out(comp, name_alloc, 1);

	name_alloc = namealloc("ne");
	comp_cmp->port_ne = Netlist_Comp_AddPort_Out(comp, name_alloc, 1);

	return comp;
}

netlist_port_t* Netlist_Cmp_AddInput(netlist_comp_t* comp) {
	netlist_cmp_t* comp_cmp = comp->data;

	char buffer[200];
	char* name_alloc;
	netlist_port_t* port;

	sprintf(buffer, "in%d", comp_cmp->inputs_nb);
	name_alloc = namealloc(buffer);
	port = Netlist_Comp_AddPort_In(comp, name_alloc, comp_cmp->width);

	comp_cmp->inputs_nb++;
	return port;
}

static void Netlist_Cmp_Dup(avl_pp_tree_t* tree_old2new, netlist_comp_t* comp, netlist_comp_t* newcomp) {
	netlist_cmp_t* cmp_data = comp->data;
	netlist_cmp_t* newcmp_data = newcomp->data;

	newcmp_data->width = cmp_data->width;
	newcmp_data->value = cmp_data->value;

	newcmp_data->inputs_nb = cmp_data->inputs_nb;

	void* pointer_old2new(void* oldptr) {
		void* newptr = NULL;
		avl_pp_find_data(tree_old2new, oldptr, &newptr);
		return newptr;
	}

	newcmp_data->port_eq = pointer_old2new(cmp_data->port_eq);
	newcmp_data->port_ne = pointer_old2new(cmp_data->port_ne);
}

static void Netlist_Cmp_GenVHDL(FILE* F, netlist_vhdl_t* params, netlist_comp_t* comp) {
	netlist_cmp_t* cmp_data = comp->data;

	Netlist_GenVHDL_Entity(F, comp, params);
	fputc('\n', F);

	fprintf(F, "architecture %s of %s is\n", params->architecture, Netlist_Comp_GetRename_Comp(comp, params));
	fputc('\n', F);

	// Insert one intermediate signal. It eases generation
	fprintf(F, "\tsignal tmp : ");
	Netlist_GenVHDL_StdLogicType_be(F, 1, NULL, ";\n");

	fprintf(F,
		"\n"
		"begin\n"
		"\n"
	);

	fprintf(F, "\t-- Compute the result\n");
	fprintf(F, "\ttmp <=\n");
	netlist_port_t* first_input = NULL;
	avl_pp_foreach(&comp->ports, scanPort) {
		netlist_port_t* port = scanPort->data;
		if(port->direction!=NETLIST_PORT_DIR_IN) continue;
		if(first_input==NULL) { first_input = port; continue; }
		ftabprintf(F, 2, "'0' when %s /= %s else\n", first_input->name, port->name);
	}
	// The optional internal value
	if(cmp_data->value!=NULL) {
		ftabprintf(F, 2, "'0' when %s /= \"%s\" else\n", first_input->name, cmp_data->value);
	}
	ftabprintf(F, 2, "'1';\n");
	fputc('\n', F);

	fprintf(F, "\t-- Set the outputs\n");
	if(cmp_data->port_eq!=NULL) fprintf(F, "\t%s <= tmp;\n", cmp_data->port_eq->name);
	if(cmp_data->port_ne!=NULL) fprintf(F, "\t%s <= not(tmp);\n", cmp_data->port_ne->name);

	fprintf(F,
		"\n"
		"end architecture;\n"
	);
}

netlist_comp_model_t* Netlist_Cmp_GetModel() {
	netlist_comp_model_t* model = Netlist_CompModel_New();
	model->name = namealloc("cmp");

	model->data_size         = sizeof(netlist_cmp_t);
	model->func_dup_internal = Netlist_Cmp_Dup;
	model->func_gen_vhdl     = Netlist_Cmp_GenVHDL;

	model->func_simp         = (netlist_comp_cb_simp_t)Netlist_Simp_Comp_Cmp;

	NETLIST_COMP_CMP = model;
	return model;
}



//===================================================
// Logic Unit, 2 inputs
//===================================================

#if 0  // FIXME The LU component model is not used

netlist_comp_model_t* NETLIST_COMP_LU = NULL;

netlist_comp_t* Netlist_Lu_New(char* name, unsigned width) {
	netlist_comp_t* comp = Netlist_Comp_New(NETLIST_COMP_LU);
	comp->name = name;

	netlist_lu_t* comp_lu = comp->data;

	comp_lu->width = width;

	char* name_alloc;

	name_alloc = namealloc("data_out");
	comp_lu->port_out = Netlist_Comp_AddPort_Out(comp, name_alloc, width);
	name_alloc = namealloc("inA");
	comp_lu->port_inA = Netlist_Comp_AddPort_In(comp, name_alloc, width);
	name_alloc = namealloc("inB");
	comp_lu->port_inB = Netlist_Comp_AddPort_In(comp, name_alloc, width);

	name_alloc = namealloc("cmd");
	comp_lu->port_fsm = Netlist_Comp_AddPort_In(comp, name_alloc, 4);

	comp_lu->cmd_and   = namealloc("0000");
	comp_lu->cmd_nand  = namealloc("0001");
	comp_lu->cmd_or    = namealloc("0010");
	comp_lu->cmd_nor   = namealloc("0011");
	comp_lu->cmd_xor   = namealloc("0100");
	comp_lu->cmd_nxor  = namealloc("0101");
	comp_lu->cmd_notA  = namealloc("0110");
	comp_lu->cmd_notB  = namealloc("0111");

	comp_lu->cmd_andN   = namealloc("1000");
	comp_lu->cmd_nandN  = namealloc("1001");
	comp_lu->cmd_orN    = namealloc("1010");
	comp_lu->cmd_norN   = namealloc("1011");
	comp_lu->cmd_xorN   = namealloc("1100");
	comp_lu->cmd_nxorN  = namealloc("1101");

	comp_lu->cmd_A      = namealloc("1110");
	comp_lu->cmd_B      = namealloc("1111");

	return comp;
}

#endif



//======================================================================
// Logic operators, multi-port
//======================================================================

netlist_comp_model_t* NETLIST_COMP_AND = NULL;
netlist_comp_model_t* NETLIST_COMP_NAND = NULL;
netlist_comp_model_t* NETLIST_COMP_OR = NULL;
netlist_comp_model_t* NETLIST_COMP_NOR = NULL;
netlist_comp_model_t* NETLIST_COMP_XOR = NULL;
netlist_comp_model_t* NETLIST_COMP_NXOR = NULL;
netlist_comp_model_t* NETLIST_COMP_NOT = NULL;

netlist_comp_t* Netlist_Logic_New(netlist_comp_model_t* model, char* name, unsigned width) {
	netlist_comp_t* comp = Netlist_Comp_New(model);
	comp->name = name;

	netlist_logic_t* comp_logic = comp->data;

	comp_logic->width = width;
	comp_logic->inputs_nb = 0;

	char* name_alloc;

	name_alloc = namealloc("data_out");
	comp_logic->port_out = Netlist_Comp_AddPort_Out(comp, name_alloc, width);

	return comp;
}

netlist_comp_t* Netlist_Logic_New_And(char* name, unsigned width) {
	return Netlist_Logic_New(NETLIST_COMP_AND, name, width);
}
netlist_comp_t* Netlist_Logic_New_Nand(char* name, unsigned width) {
	return Netlist_Logic_New(NETLIST_COMP_NAND, name, width);
}
netlist_comp_t* Netlist_Logic_New_Or(char* name, unsigned width) {
	return Netlist_Logic_New(NETLIST_COMP_OR, name, width);
}
netlist_comp_t* Netlist_Logic_New_Nor(char* name, unsigned width) {
	return Netlist_Logic_New(NETLIST_COMP_NOR, name, width);
}
netlist_comp_t* Netlist_Logic_New_Xor(char* name, unsigned width) {
	return Netlist_Logic_New(NETLIST_COMP_XOR, name, width);
}
netlist_comp_t* Netlist_Logic_New_Nxor(char* name, unsigned width) {
	return Netlist_Logic_New(NETLIST_COMP_NXOR, name, width);
}
netlist_comp_t* Netlist_Logic_New_Not(char* name, unsigned width) {
	return Netlist_Logic_New(NETLIST_COMP_NOT, name, width);
}

netlist_port_t* Netlist_Logic_AddInput(netlist_comp_t* comp) {
	netlist_logic_t* comp_logic = comp->data;

	char buffer[200];
	char* name_alloc;
	netlist_port_t* port;

	sprintf(buffer, "in%d", comp_logic->inputs_nb);
	name_alloc = namealloc(buffer);
	port = Netlist_Comp_AddPort_In(comp, name_alloc, comp_logic->width);

	comp_logic->inputs_nb++;
	return port;
}

static void Netlist_Logic_Dup(avl_pp_tree_t* tree_old2new, netlist_comp_t* comp, netlist_comp_t* newcomp) {
	netlist_logic_t* logic_data = comp->data;
	netlist_logic_t* newlogic_data = newcomp->data;

	newlogic_data->width = logic_data->width;
	newlogic_data->value = logic_data->value;

	newlogic_data->inputs_nb = logic_data->inputs_nb;

	void* pointer_old2new(void* oldptr) {
		void* newptr = NULL;
		avl_pp_find_data(tree_old2new, oldptr, &newptr);
		return newptr;
	}

	newlogic_data->port_out = pointer_old2new(logic_data->port_out);
}

static void Netlist_Logic_GenVHDL(FILE* F, netlist_vhdl_t* params, netlist_comp_t* comp) {
	netlist_logic_t* logic_data = comp->data;

	Netlist_GenVHDL_Entity(F, comp, params);
	fputc('\n', F);

	fprintf(F, "architecture %s of %s is\n", params->architecture, Netlist_Comp_GetRename_Comp(comp, params));
	fputc('\n', F);

	// Insert one intermediate signal. In case the final result needs a NOT.
	fprintf(F, "\tsignal tmp : ");
	Netlist_GenVHDL_StdLogicType_be(F, logic_data->width, NULL, ";\n");

	fprintf(F,
		"\n"
		"begin\n"
		"\n"
	);

	// Determine the first operation, and if an inversion is needed.
	bool invert = false;
	char* first_oper = NULL;
	if     (comp->model==NETLIST_COMP_AND)  { first_oper = "and"; }
	else if(comp->model==NETLIST_COMP_NAND) { first_oper = "and"; invert = true; }
	else if(comp->model==NETLIST_COMP_OR)   { first_oper = "or"; }
	else if(comp->model==NETLIST_COMP_NOR)  { first_oper = "or"; invert = true; }
	else if(comp->model==NETLIST_COMP_XOR)  { first_oper = "xor"; }
	else if(comp->model==NETLIST_COMP_NXOR) { first_oper = "xor"; invert = true; }
	else if(comp->model==NETLIST_COMP_NOT)  { first_oper = NULL; invert = true; }
	else {
		printf("INTERNAL ERROR %s:%d : Operator '%s' is not handled.\n", __FILE__, __LINE__, comp->model->name);
		abort();
	}

	fprintf(F, "\t-- First operation\n");
	fprintf(F, "\ttmp <= ");
	unsigned nb = 0;
	avl_pp_foreach(&comp->ports, scanPort) {
		netlist_port_t* port = scanPort->data;
		if(port->direction!=NETLIST_PORT_DIR_IN) continue;
		if(nb>0) {
			assert(first_oper!=NULL);
			fprintf(F, " %s ", first_oper);
		}
		fprintf(F, "%s", port->name);
		nb ++;
	}

	if(logic_data->value!=NULL) {
		assert(nb>0);
		assert(first_oper!=NULL);
		fprintf(F, " %s ", first_oper);
		Netlist_GenVHDL_StdLogic_Literal(F, logic_data->value);
	}

	fprintf(F, ";\n");

	fputc('\n', F);
	fprintf(F, "\t-- Set the output\n");
	if(invert==false) fprintf(F, "\t%s <= tmp;\n", logic_data->port_out->name);
	else              fprintf(F, "\t%s <= not tmp;\n", logic_data->port_out->name);

	fprintf(F,
		"\n"
		"end architecture;\n"
	);
}

static void Netlist_Logic_InlVHDL_SigDecl(FILE* F, netlist_vhdl_t* params, netlist_comp_t* comp) {
	netlist_logic_t* logic_data = comp->data;
	fprintf(F, "	signal %s : ", Netlist_Comp_GetRename_Comp(comp, params));
	Netlist_GenVHDL_StdLogicType_be(F, logic_data->width, NULL, ";\n");
}
static void Netlist_Logic_InlVHDL_OneComp(FILE* F, netlist_vhdl_t* params, netlist_comp_t* comp) {
	netlist_logic_t* logic_data = comp->data;
	fprintf(F, "	%s <=", Netlist_Comp_GetRename_Comp(comp, params));

	// Determine the first operation, and if an inversion is needed.
	bool invert = false;
	char* first_oper = NULL;
	if     (comp->model==NETLIST_COMP_AND)  { first_oper = "and"; }
	else if(comp->model==NETLIST_COMP_NAND) { first_oper = "and"; invert = true; }
	else if(comp->model==NETLIST_COMP_OR)   { first_oper = "or"; }
	else if(comp->model==NETLIST_COMP_NOR)  { first_oper = "or"; invert = true; }
	else if(comp->model==NETLIST_COMP_XOR)  { first_oper = "xor"; }
	else if(comp->model==NETLIST_COMP_NXOR) { first_oper = "xor"; invert = true; }
	else if(comp->model==NETLIST_COMP_NOT)  { first_oper = NULL; invert = true; }
	else {
		printf("INTERNAL ERROR %s:%d : Operator '%s' is not handled.\n", __FILE__, __LINE__, comp->model->name);
		abort();
	}

	if(invert==true) fprintf(F, " not (");
	fprintf(F, "\n");

	unsigned nb = 0;
	avl_pp_foreach(&comp->ports, scanPort) {
		netlist_port_t* port = scanPort->data;
		if(port->direction!=NETLIST_PORT_DIR_IN) continue;
		if(nb>0) {
			assert(first_oper!=NULL);
			fprintf(F, " %s\n", first_oper);
		}
		fprintf(F, "		");
		Netlist_GenVHDL_ValCat_List_opt(F, comp->father, port->source, params, GENVHDL_VALCAT_CHKADDPAR | GENVHDL_VALCAT_USEREPEAT);
		nb ++;
	}

	if(logic_data->value!=NULL) {
		assert(nb>0);
		assert(first_oper!=NULL);
		fprintf(F, " %s\n		", first_oper);
		Netlist_GenVHDL_StdLogic_Literal(F, logic_data->value);
	}

	if(invert==true) fprintf(F, "\n	)");
	fprintf(F, ";\n");
}


netlist_comp_model_t* Netlist_And_GetModel() {
	netlist_comp_model_t* model = Netlist_CompModel_New();
	model->name = namealloc("and");

	model->data_size         = sizeof(netlist_logic_t);
	model->func_dup_internal = Netlist_Logic_Dup;
	model->func_gen_vhdl     = Netlist_Logic_GenVHDL;

	model->func_simp         = (netlist_comp_cb_simp_t)Netlist_Simp_Comp_Logic;

	model->inlvhdl_en = true;
	model->func_inlvhdl_sigdecl = Netlist_Logic_InlVHDL_SigDecl;
	model->func_inlvhdl_onecomp = Netlist_Logic_InlVHDL_OneComp;

	NETLIST_COMP_AND = model;
	return model;
}
netlist_comp_model_t* Netlist_Nand_GetModel() {
	netlist_comp_model_t* model = Netlist_CompModel_New();
	model->name = namealloc("nand");

	model->data_size         = sizeof(netlist_logic_t);
	model->func_dup_internal = Netlist_Logic_Dup;
	model->func_gen_vhdl     = Netlist_Logic_GenVHDL;

	model->func_simp         = (netlist_comp_cb_simp_t)Netlist_Simp_Comp_Logic;

	model->inlvhdl_en = true;
	model->func_inlvhdl_sigdecl = Netlist_Logic_InlVHDL_SigDecl;
	model->func_inlvhdl_onecomp = Netlist_Logic_InlVHDL_OneComp;

	NETLIST_COMP_NAND = model;
	return model;
}
netlist_comp_model_t* Netlist_Or_GetModel() {
	netlist_comp_model_t* model = Netlist_CompModel_New();
	model->name = namealloc("or");

	model->data_size         = sizeof(netlist_logic_t);
	model->func_dup_internal = Netlist_Logic_Dup;
	model->func_gen_vhdl     = Netlist_Logic_GenVHDL;

	model->func_simp         = (netlist_comp_cb_simp_t)Netlist_Simp_Comp_Logic;

	model->inlvhdl_en = true;
	model->func_inlvhdl_sigdecl = Netlist_Logic_InlVHDL_SigDecl;
	model->func_inlvhdl_onecomp = Netlist_Logic_InlVHDL_OneComp;

	NETLIST_COMP_OR = model;
	return model;
}
netlist_comp_model_t* Netlist_Nor_GetModel() {
	netlist_comp_model_t* model = Netlist_CompModel_New();
	model->name = namealloc("nor");

	model->data_size         = sizeof(netlist_logic_t);
	model->func_dup_internal = Netlist_Logic_Dup;
	model->func_gen_vhdl     = Netlist_Logic_GenVHDL;

	model->func_simp         = (netlist_comp_cb_simp_t)Netlist_Simp_Comp_Logic;

	model->inlvhdl_en = true;
	model->func_inlvhdl_sigdecl = Netlist_Logic_InlVHDL_SigDecl;
	model->func_inlvhdl_onecomp = Netlist_Logic_InlVHDL_OneComp;

	NETLIST_COMP_NOR = model;
	return model;
}
netlist_comp_model_t* Netlist_Xor_GetModel() {
	netlist_comp_model_t* model = Netlist_CompModel_New();
	model->name = namealloc("xor");

	model->data_size         = sizeof(netlist_logic_t);
	model->func_dup_internal = Netlist_Logic_Dup;
	model->func_gen_vhdl     = Netlist_Logic_GenVHDL;

	model->func_simp         = (netlist_comp_cb_simp_t)Netlist_Simp_Comp_Logic;

	model->inlvhdl_en = true;
	model->func_inlvhdl_sigdecl = Netlist_Logic_InlVHDL_SigDecl;
	model->func_inlvhdl_onecomp = Netlist_Logic_InlVHDL_OneComp;

	NETLIST_COMP_XOR = model;
	return model;
}
netlist_comp_model_t* Netlist_Nxor_GetModel() {
	netlist_comp_model_t* model = Netlist_CompModel_New();
	model->name = namealloc("nxor");

	model->data_size         = sizeof(netlist_logic_t);
	model->func_dup_internal = Netlist_Logic_Dup;
	model->func_gen_vhdl     = Netlist_Logic_GenVHDL;

	model->func_simp         = (netlist_comp_cb_simp_t)Netlist_Simp_Comp_Logic;

	model->inlvhdl_en = true;
	model->func_inlvhdl_sigdecl = Netlist_Logic_InlVHDL_SigDecl;
	model->func_inlvhdl_onecomp = Netlist_Logic_InlVHDL_OneComp;

	NETLIST_COMP_NXOR = model;
	return model;
}
netlist_comp_model_t* Netlist_Not_GetModel() {
	netlist_comp_model_t* model = Netlist_CompModel_New();
	model->name = namealloc("not");

	model->data_size         = sizeof(netlist_logic_t);
	model->func_dup_internal = Netlist_Logic_Dup;
	model->func_gen_vhdl     = Netlist_Logic_GenVHDL;

	model->func_simp         = (netlist_comp_cb_simp_t)Netlist_Simp_Comp_Logic;

	model->inlvhdl_en = true;
	model->func_inlvhdl_sigdecl = Netlist_Logic_InlVHDL_SigDecl;
	model->func_inlvhdl_onecomp = Netlist_Logic_InlVHDL_OneComp;

	NETLIST_COMP_NOT = model;
	return model;
}



//======================================================================
// Binary MUX
//======================================================================

netlist_comp_model_t* NETLIST_COMP_MUXBIN = NULL;

netlist_comp_t* Netlist_MuxBin_New(char* name, unsigned width_sel, unsigned width_data, unsigned inputs_nb) {
	netlist_comp_t* comp = Netlist_Comp_New(NETLIST_COMP_MUXBIN);
	comp->name = name;

	netlist_muxbin_t* comp_muxbin = comp->data;

	comp_muxbin->width_sel  = width_sel;
	comp_muxbin->width_data = width_data;
	comp_muxbin->inputs_nb  = inputs_nb;

	comp_muxbin->ports_datain = calloc(inputs_nb, sizeof(*comp_muxbin->ports_datain));

	char* port_name;

	port_name = namealloc("in_sel");
	comp_muxbin->port_sel = Netlist_Comp_AddPort_In(comp, port_name, width_sel);
	port_name = namealloc("out_data");
	comp_muxbin->port_out = Netlist_Comp_AddPort_Out(comp, port_name, width_data);

	char buf[128];
	for(unsigned i=0; i<inputs_nb; i++) {
		sprintf(buf, "in_data%u", i);
		port_name = namealloc(buf);
		comp_muxbin->ports_datain[i] = Netlist_Comp_AddPort_In(comp, port_name, width_data);
	}

	return comp;
}

static void Netlist_MuxBin_Free_Data(netlist_comp_t* comp) {
	netlist_muxbin_t* comp_muxbin = comp->data;
	free(comp_muxbin->ports_datain);
	free(comp_muxbin);
}
static void Netlist_MuxBin_Dup(avl_pp_tree_t* tree_old2new, netlist_comp_t* comp, netlist_comp_t* newcomp) {
	netlist_muxbin_t* muxbin_data = comp->data;
	netlist_muxbin_t* newmuxbin_data = newcomp->data;

	newmuxbin_data->width_sel  = muxbin_data->width_sel;
	newmuxbin_data->width_data = muxbin_data->width_data;
	newmuxbin_data->inputs_nb  = muxbin_data->inputs_nb;

	newmuxbin_data->ports_datain = calloc(newmuxbin_data->inputs_nb, sizeof(*newmuxbin_data->ports_datain));

	void* pointer_old2new(void* oldptr) {
		void* newptr = NULL;
		avl_pp_find_data(tree_old2new, oldptr, &newptr);
		return newptr;
	}

	newmuxbin_data->port_sel = pointer_old2new(muxbin_data->port_sel);
	newmuxbin_data->port_out = pointer_old2new(muxbin_data->port_out);

	for(unsigned i=0; i<newmuxbin_data->inputs_nb; i++) {
		newmuxbin_data->ports_datain[i] = pointer_old2new(muxbin_data->ports_datain[i]);
	}
}

static void Netlist_MuxBin_GenVHDL(FILE* F, netlist_vhdl_t* params, netlist_comp_t* comp) {
	netlist_muxbin_t* muxbin_data = comp->data;

	fprintf(F,
		"library ieee;\n"
		"use ieee.numeric_std.all;\n"
		"\n"
	);

	Netlist_GenVHDL_Entity(F, comp, params);
	fputc('\n', F);

	fprintfm(F,
		"architecture %s of %s is\n", params->architecture, Netlist_Comp_GetRename_Comp(comp, params),
		"begin\n"
		"\n",
		NULL
	);

	if(muxbin_data->width_sel==1) {
		fprintf(F, "	%s <= %s when %s = '0' else %s;\n",
			muxbin_data->port_out->name,
			muxbin_data->ports_datain[0]->name,
			muxbin_data->port_sel->name,
			muxbin_data->ports_datain[1]->name
		);
	}
	else {
		fprintf(F, "	%s <=\n", muxbin_data->port_out->name);
		for(unsigned i=0; i<muxbin_data->inputs_nb; i++) {
			fprintf(F, "		%s when to_integer(unsigned(%s)) = %u else\n",
				muxbin_data->ports_datain[i]->name,
				muxbin_data->port_sel->name, i
			);
		}
		if(vhd2vl_friendly==false) {
			fprintf(F, "		(others => '-');\n");
		}
		else {
			fprintf(F, "		(others => '0');\n");
		}
	}

	fprintf(F,
		"\n"
		"end architecture;\n"
		"\n"
	);
}

netlist_comp_model_t* Netlist_MuxBin_GetModel() {
	netlist_comp_model_t* model = Netlist_CompModel_New();
	model->name              = namealloc("muxb");

	model->data_size         = sizeof(netlist_muxbin_t);
	model->func_free_data    = Netlist_MuxBin_Free_Data;
	model->func_dup_internal = Netlist_MuxBin_Dup;
	model->func_gen_vhdl     = Netlist_MuxBin_GenVHDL;

	model->func_simp         = NULL;

	NETLIST_COMP_MUXBIN = model;
	return model;
}



//======================================================================
// Clock divider
//======================================================================

// Shared variable
netlist_comp_model_t* NETLIST_COMP_CLKDIV = NULL;

static void Netlist_ClkDiv_DataDup(avl_pp_tree_t* tree_old2new, netlist_comp_t* comp, netlist_comp_t* newcomp) {
	netlist_clkdiv_t* clk_data = comp->data;
	netlist_clkdiv_t* newclk_data = newcomp->data;

	newclk_data->divider = clk_data->divider;

	void* pointer_old2new(void* oldptr) {
		void* newptr = NULL;
		avl_pp_find_data(tree_old2new, oldptr, &newptr);
		return newptr;
	}

	newclk_data->port_in  = pointer_old2new(clk_data->port_in);
	newclk_data->port_out = pointer_old2new(clk_data->port_out);
}

static void Netlist_ClkDiv_GenVHDL(FILE* F, netlist_vhdl_t* params, netlist_comp_t* comp) {
	netlist_clkdiv_t* clk_data = comp->data;

	fprintf(F,
		"\n"
		"library ieee;\n"
		"use ieee.numeric_std.all;\n"
		"\n"
	);

	Netlist_GenVHDL_Entity(F, comp, params);

	fprintfm(F,
		"\n"
		"architecture %s of %s is\n", params->architecture, Netlist_Comp_GetRename_Comp(comp, params),
		"\n"
		"	-- Note: Clock divider is %u\n", clk_data->divider,
		"	constant cnt_limit : natural := %u;\n", (clk_data->divider/2)-1,
		"	constant cnt_width : natural := %u;\n", uint_bitsnb((clk_data->divider/2)-1),
		"\n"
		"	signal counter : unsigned(cnt_width-1 downto 0) := (others => '0');\n"
		"	signal buf_out : std_logic := '0';\n"
		"\n"
		"begin\n"
		"\n"
		"	process(%s)\n", clk_data->port_in->name,
		"	begin\n"
		"		if %s(%s) then\n", Netlist_GenVHDL_ClockEdge_TestFunc_port(clk_data->port_in), clk_data->port_in->name,
		"			if counter = cnt_limit then\n"
		"				counter <= (others => '0');\n"
		"				buf_out <= not buf_out;\n"
		"			else\n"
		"				counter <= counter + 1;\n"
		"			end if;\n"
		"		end if;\n"
		"	end process;\n"
		"\n"
		"	%s <= buf_out;\n", clk_data->port_out->name,
		"\n"
		"end architecture;\n"
		"\n",
		NULL
	);

}

netlist_comp_model_t* Netlist_ClkDiv_GetModel() {
	netlist_comp_model_t* model = Netlist_CompModel_New();
	model->name = namealloc("clkdiv");

	model->data_size         = sizeof(netlist_clkdiv_t);
	model->func_dup_internal = Netlist_ClkDiv_DataDup;
	model->func_gen_vhdl     = Netlist_ClkDiv_GenVHDL;

	NETLIST_COMP_CLKDIV = model;
	return model;
}

netlist_comp_t* Netlist_ClkDiv_New(char* name, unsigned divider) {
	netlist_comp_t* comp = Netlist_Comp_New(NETLIST_COMP_CLKDIV);
	comp->name = name;

	netlist_clkdiv_t* clk_data = comp->data;

	// The configuration
	clk_data->divider = divider;

	// The ports
	clk_data->port_in  = Netlist_Comp_AddPort_In(comp, namealloc("clk_in"), 1);
	clk_data->port_out = Netlist_Comp_AddPort_Out(comp, namealloc("clk_out"), 1);

	return comp;
}



//======================================================================
// FIFO Functions
//======================================================================

// Shared variable
netlist_comp_model_t* NETLIST_COMP_FIFO = NULL;

static void Netlist_Fifo_DataDup(avl_pp_tree_t* tree_old2new, netlist_comp_t* comp, netlist_comp_t* newcomp) {
	netlist_fifo_t* fifo_data = comp->data;
	netlist_fifo_t* newfifo_data = newcomp->data;

	newfifo_data->data_width = fifo_data->data_width;
	newfifo_data->cells_nb   = fifo_data->cells_nb;

	void* pointer_old2new(void* oldptr) {
		void* newptr = NULL;
		avl_pp_find_data(tree_old2new, oldptr, &newptr);
		return newptr;
	}

	newfifo_data->port_clk   = pointer_old2new(fifo_data->port_clk);
	newfifo_data->port_reset = pointer_old2new(fifo_data->port_reset);

	newfifo_data->fifo_in  = pointer_old2new(fifo_data->fifo_in);
	newfifo_data->fifo_out = pointer_old2new(fifo_data->fifo_out);
}

static void Netlist_Fifo_GenVHDL_AsMem(FILE* F, netlist_vhdl_t* params, netlist_comp_t* comp) {
	netlist_fifo_t* fifo_data = comp->data;

	netlist_access_fifo_t* fifoin_data = fifo_data->fifo_in->data;
	netlist_access_fifo_t* fifoout_data = fifo_data->fifo_out->data;

	fprintf(F,
		"\n"
		"library ieee;\n"
		"use ieee.numeric_std.all;\n"
		"\n"
	);

	Netlist_GenVHDL_Entity(F, comp, params);

	fprintfm(F,
		"\n"
		"architecture %s of %s is\n", params->architecture, Netlist_Comp_GetRename_Comp(comp, params),
		"\n"
		"	-- Some constants to ease VHDL generation\n"
		"\n"
		"	constant data_bits : natural := %u;\n", fifo_data->data_width,
		"	constant cells_nb  : natural := %u;\n", fifo_data->cells_nb,
		"	constant idx_width : natural := %u;\n", uint_bitsnb(fifo_data->cells_nb-1),
		"\n"
		"	-- The embedded memory\n",
		"	type mem_type is array (0 to %u) of ", fifo_data->cells_nb-1,
		NULL
	);

	Netlist_GenVHDL_StdLogicType_be(F, fifo_data->data_width, NULL, ";\n");

	fprintf(F,
		"	signal mem : mem_type;\n"
		"\n"
		"	-- Internal registers\n"
		"	signal idx_in        : unsigned(idx_width-1 downto 0) := (others => '0');\n"
		"	signal idx_in_next   : unsigned(idx_width-1 downto 0) := (others => '0');\n"
		"	signal idx_out       : unsigned(idx_width-1 downto 0) := (others => '0');\n"
		"	signal idx_out_next  : unsigned(idx_width-1 downto 0) := (others => '0');\n"
		"	signal empty         : std_logic := '1';\n"
		"	signal empty_next    : std_logic := '1';\n"
		"	signal wen           : std_logic := '0';  -- Write enable\n"
		"\n"
	);

	Netlist_GenVHDL_MyToInteger(F);

	fprintfm(F,
		"\n"
		"begin\n"
		"\n"
		"	-- Sequential process\n"
		"	process (%s, %s)\n", fifo_data->port_clk->name, fifo_data->port_reset->name,
		"	begin\n"
		"		if %s = '%c' then\n", fifo_data->port_reset->name, Netlist_Access_Reset_GetActiveState(fifo_data->port_reset->access),
		"			idx_in  <= (others => '0');\n"
		"			idx_out <= (others => '0');\n"
		"			empty   <= '1';\n"
		"		elsif %s(%s) then\n", Netlist_GenVHDL_ClockEdge_TestFunc_port(fifo_data->port_clk), fifo_data->port_clk->name,
		"			idx_in  <= idx_in_next;\n"
		"			idx_out <= idx_out_next;\n"
		"			empty   <= empty_next;\n"
		"		end if;\n"
		"		-- Note: The memory content is not affected by reset\n"
		"		if %s(%s) then\n", Netlist_GenVHDL_ClockEdge_TestFunc_port(fifo_data->port_clk), fifo_data->port_clk->name,
		"			if wen = '1' then\n"
		"				mem(to_integer(idx_in)) <= %s;\n", fifoin_data->port_data->name,
		"			end if;\n"
		"		end if;\n"
		"	end process;\n"
		"\n"
		"	-- Combinatorial process\n"
		"	process (idx_in, idx_out, empty, %s, %s)\n", fifoin_data->port_ack->name, fifoout_data->port_ack->name,
		"		variable var_idxin_next  : unsigned(idx_width-1 downto 0) := (others => '0');\n"
		"		variable var_idxout_next : unsigned(idx_width-1 downto 0) := (others => '0');\n"
		"		variable var_doin        : std_logic := '0';\n"
		"		variable var_doout       : std_logic := '0';\n"
		"	begin\n"
		"\n"
		"		-- Default values for ports\n"
		"		%s <= '0';\n", fifoin_data->port_rdy->name,
		"		%s <= '0';\n", fifoout_data->port_rdy->name,
		"		%s <= mem(to_integer(idx_out));\n", fifoout_data->port_data->name,
		"\n"
		"		-- Default values for internal signals and registers\n"
		"		idx_in_next  <= idx_in;\n"
		"		idx_out_next <= idx_out;\n"
		"		empty_next   <= empty;\n"
		"		wen          <= '0';\n"
		"\n"
		"		-- Default values for the variables\n"
		"		var_idxin_next  := idx_in;\n"
		"		var_idxout_next := idx_out;\n"
		"		var_doin        := '0';\n"
		"		var_doout       := '0';\n"
		"\n"
		"		-- Note: When both indexes are equal,\n"
		"		--   empty = '0' means the mem is empty (data input is always OK)\n"
		"		--   empty = '1' means the mem is full (data output is always OK)\n"
		"\n"
		"		-- Handle input\n"
		"		if idx_in /= idx_out or empty = '1' then\n"
		"			%s <= '1';\n", fifoin_data->port_rdy->name,
		"			if %s = '1' then\n", fifoin_data->port_ack->name,
		"				wen <= '1';\n"
		"				var_doin := '1';\n"
		"				-- Increment index\n"
		"				if idx_in = cells_nb-1 then\n"
		"					idx_in_next    <= (others => '0');\n"
		"					var_idxin_next := (others => '0');\n"
		"				else\n"
		"					idx_in_next    <= idx_in + 1;\n"
		"					var_idxin_next := idx_in + 1;\n"
		"				end if;\n"
		"			end if;\n"
		"		end if;\n"
		"\n"
		"		-- Handle output\n"
		"		if idx_in /= idx_out or empty = '0' then\n"
		"			%s <= '1';\n", fifoout_data->port_rdy->name,
		"			if %s = '1' then\n", fifoout_data->port_ack->name,
		"				var_doout := '1';\n"
		"				-- Increment index\n"
		"				if idx_out = cells_nb-1 then\n"
		"					idx_out_next    <= (others => '0');\n"
		"					var_idxout_next := (others => '0');\n"
		"				else\n"
		"					idx_out_next    <= idx_out + 1;\n"
		"					var_idxout_next := idx_out + 1;\n"
		"				end if;\n"
		"			end if;\n"
		"		end if;\n"
		"\n"
		"		-- Set the flag for empty mem\n"
		"		if var_idxin_next = var_idxout_next and var_doin = '0' and var_doout = '1' then\n"
		"			empty_next <= '1';\n"
		"		end if;\n"
		"		if var_doin = '1' and var_doout = '0' then\n"
		"			empty_next <= '0';\n"
		"		end if;\n"
		"\n"
		"	end process;\n"
		"\n"
		"end architecture;\n"
		"\n",
		NULL
	);

}
static void Netlist_Fifo_GenVHDL_AsWire(FILE* F, netlist_vhdl_t* params, netlist_comp_t* comp) {
	netlist_fifo_t* fifo_data = comp->data;

	netlist_access_fifo_t* fifoin_data = fifo_data->fifo_in->data;
	netlist_access_fifo_t* fifoout_data = fifo_data->fifo_out->data;

	Netlist_GenVHDL_Entity(F, comp, params);

	fprintfm(F,
		"\n"
		"architecture %s of %s is\n", params->architecture, Netlist_Comp_GetRename_Comp(comp, params),
		"begin\n"
		"\n"
		"	%s <= %s;\n", fifoout_data->port_data->name, fifoin_data->port_data->name,
		"	%s <= %s;\n", fifoout_data->port_rdy->name, fifoin_data->port_ack->name,
		"	%s <= %s;\n", fifoin_data->port_rdy->name, fifoout_data->port_ack->name,
		"\n"
		"end architecture;\n"
		"\n",
		NULL
	);

}
static void Netlist_Fifo_GenVHDL(FILE* F, netlist_vhdl_t* params, netlist_comp_t* comp) {
	netlist_fifo_t* fifo_data = comp->data;
	if(fifo_data->cells_nb==0) Netlist_Fifo_GenVHDL_AsWire(F, params, comp);
	else Netlist_Fifo_GenVHDL_AsMem(F, params, comp);
}

netlist_comp_model_t* Netlist_Fifo_GetModel() {
	netlist_comp_model_t* model = Netlist_CompModel_New();
	model->name = namealloc("fifo");

	model->data_size         = sizeof(netlist_fifo_t);
	model->func_dup_internal = Netlist_Fifo_DataDup;
	model->func_gen_vhdl     = Netlist_Fifo_GenVHDL;

	model->func_simp         = (netlist_comp_cb_simp_t)Netlist_Simp_Comp_Fifo;

	NETLIST_COMP_FIFO = model;
	return model;
}

netlist_comp_t* Netlist_Fifo_New(char* name, unsigned data_width, unsigned cells_nb) {
	netlist_comp_t* comp = Netlist_Comp_New(NETLIST_COMP_FIFO);
	comp->name = name;

	netlist_fifo_t* fifo_data = comp->data;

	fifo_data->data_width = data_width;
	fifo_data->cells_nb = cells_nb;

	char* port_name;

	port_name = namealloc("clk");
	fifo_data->port_clk = Netlist_Comp_AddPortAccess_Clock(comp, port_name);

	port_name = namealloc("reset");
	fifo_data->port_reset = Netlist_Comp_AddPortAccess_Reset(comp, port_name);

	port_name = namealloc("fifo_in");
	fifo_data->fifo_in = Netlist_Comp_AddAccess_FifoIn(comp, port_name, data_width, false, NULL);

	port_name = namealloc("fifo_out");
	fifo_data->fifo_out = Netlist_Comp_AddAccess_FifoOut(comp, port_name, data_width, false, NULL);

	return comp;
}



//======================================================================
// PingPong Buffer Functions
//======================================================================

// shared variables
netlist_comp_model_t* NETLIST_COMP_PINGPONG_IN = NULL;
netlist_comp_model_t* NETLIST_COMP_PINGPONG_OUT = NULL;

// Creation of PingPong buffer
netlist_comp_t* Netlist_PingPong_New(
	char* name, unsigned data_width, unsigned cells_nb, unsigned addr_width, unsigned banks_nb, bool is_input
) {
	// FIXME When fully implemented, this will be removed
	assert(banks_nb==2);

	netlist_comp_model_t* model = NULL;
	if(is_input==true) model = NETLIST_COMP_PINGPONG_IN;
	else               model = NETLIST_COMP_PINGPONG_OUT;

	netlist_comp_t* comp = Netlist_Comp_New(model);
	comp->name = name;

	netlist_pingpong_t* comp_pp = comp->data;

	comp_pp->data_width = data_width;
	comp_pp->cells_nb = cells_nb;
	comp_pp->addr_width = addr_width;
	comp_pp->banks_nb = banks_nb;
	comp_pp->ports_ra_nb = 0;
	comp_pp->ports_wa_nb = 0;

	// All PP are created by default in a by-address mode
	comp_pp->is_direct = false;

	char* port_name;
	char buffer[200];

	port_name = namealloc("clk");
	comp_pp->port_clk = Netlist_Comp_AddPortAccess_Clock(comp, port_name);

	port_name = namealloc("reset");
	comp_pp->port_reset = Netlist_Comp_AddPortAccess_Reset(comp, port_name);

	sprintf(buffer, "%s_switch", name);
	port_name = namealloc(buffer);
	comp_pp->port_switch = Netlist_Comp_AddPort_In(comp, port_name, 1);

	sprintf(buffer, "%s_ratio_c", name);
	port_name = namealloc(buffer);
	comp_pp->port_ratio_c = Netlist_Comp_AddPort_Out(comp, port_name, addr_width);

	sprintf(buffer, "%s_full_c", name);
	port_name = namealloc(buffer);
	comp_pp->port_full_c = Netlist_Comp_AddPort_Out(comp, port_name, 1);

	sprintf(buffer, "%s_ratio_n", name);
	port_name = namealloc(buffer);
	comp_pp->port_ratio_n = Netlist_Comp_AddPort_Out(comp, port_name, addr_width);

	sprintf(buffer, "%s_full_n", name);
	port_name = namealloc(buffer);
	comp_pp->port_full_n = Netlist_Comp_AddPort_Out(comp, port_name, 1);

	if(comp->model == NETLIST_COMP_PINGPONG_OUT){
		sprintf(buffer, "%s_setmax", name);
		port_name = namealloc(buffer);
		comp_pp->port_setmax = Netlist_Comp_AddPort_In(comp, port_name, addr_width);
	}

	avl_p_init(&comp_pp->access_ports_ra);
	avl_p_init(&comp_pp->access_ports_wa);
	avl_ip_init(&comp_pp->access_ports_d);

	if(is_input==true) comp_pp->access_fifo = Netlist_Comp_AddAccess_FifoIn(comp, name, data_width, false, NULL);
	else               comp_pp->access_fifo = Netlist_Comp_AddAccess_FifoOut(comp, name, data_width, false, NULL);

	return comp;
}

// Free of PingPong buffer
static void Netlist_PingPong_Free_Data(netlist_comp_t* comp) {
	netlist_pingpong_t* comp_pp = comp->data;
	avl_p_reset(&comp_pp->access_ports_ra);
	avl_p_reset(&comp_pp->access_ports_wa);
	avl_ip_reset(&comp_pp->access_ports_d);
	free(comp_pp);
}
static void Netlist_PingPong_Dup(avl_pp_tree_t* tree_old2new, netlist_comp_t* comp, netlist_comp_t* newcomp) {
	netlist_pingpong_t* pp_data = comp->data;
	netlist_pingpong_t* newpp_data = newcomp->data;

	newpp_data->data_width  = pp_data->data_width;
	newpp_data->addr_width  = pp_data->addr_width;
	newpp_data->cells_nb    = pp_data->cells_nb;
	newpp_data->banks_nb	  = pp_data->banks_nb;
	newpp_data->ports_ra_nb = pp_data->ports_ra_nb;
	newpp_data->ports_wa_nb = pp_data->ports_wa_nb;
	newpp_data->is_direct   = pp_data->is_direct;

	void* pointer_old2new(void* oldptr) {
		void* newptr = NULL;
		avl_pp_find_data(tree_old2new, oldptr, &newptr);
		return newptr;
	}

	newpp_data->port_clk   = pointer_old2new(pp_data->port_clk);
	newpp_data->port_reset = pointer_old2new(pp_data->port_reset);

	newpp_data->port_switch  = pointer_old2new(pp_data->port_switch);
	newpp_data->port_ratio_c = pointer_old2new(pp_data->port_ratio_c);
	newpp_data->port_full_c  = pointer_old2new(pp_data->port_full_c);
	newpp_data->port_ratio_n = pointer_old2new(pp_data->port_ratio_n);
	newpp_data->port_full_n  = pointer_old2new(pp_data->port_full_n);

	if(comp->model == NETLIST_COMP_PINGPONG_OUT){
		newpp_data->port_setmax  = pointer_old2new(pp_data->port_setmax);
	}

	avl_p_foreach(&pp_data->access_ports_ra, scanAccess) {
		netlist_access_t* newAccess = pointer_old2new(scanAccess->data);
		avl_p_add_overwrite(&newpp_data->access_ports_ra, newAccess, newAccess);
	}

	avl_p_foreach(&pp_data->access_ports_wa, scanAccess) {
		netlist_access_t* newAccess = pointer_old2new(scanAccess->data);
		avl_p_add_overwrite(&newpp_data->access_ports_wa, newAccess, newAccess);
	}

	avl_ip_foreach(&pp_data->access_ports_d, scanAccess) {
		netlist_access_t* newAccess = pointer_old2new(scanAccess->data);
		avl_ip_add_overwrite(&newpp_data->access_ports_d, scanAccess->key, newAccess);
	}

	newpp_data->access_fifo = pointer_old2new(pp_data->access_fifo);
}

netlist_access_t* Netlist_PingPong_AddAccess_RA(netlist_comp_t* comp) {
	netlist_pingpong_t* comp_pp = comp->data;

	char buffer[200];
	char* name_alloc;
	netlist_access_t* access;

	sprintf(buffer, "ra%d", comp_pp->ports_ra_nb);
	name_alloc = namealloc(buffer);

	access = Netlist_Comp_AddAccess(comp, NETLIST_ACCESS_MEM_RA, name_alloc);
	netlist_access_mem_addr_t* access_addr = access->data;

	sprintf(buffer, "%s_data", access->name);
	name_alloc = namealloc(buffer);
	access_addr->port_d = Netlist_Comp_AddPort_Out(comp, name_alloc, comp_pp->data_width);
	access_addr->port_d->access = access;

	sprintf(buffer, "%s_addr", access->name);
	name_alloc = namealloc(buffer);
	access_addr->port_a = Netlist_Comp_AddPort_In(comp, name_alloc, comp_pp->addr_width);
	access_addr->port_a->access = access;

	avl_p_add_overwrite(&comp_pp->access_ports_ra, access, access);

	comp_pp->ports_ra_nb++;
	return access;
}
netlist_access_t* Netlist_PingPong_AddAccess_WA(netlist_comp_t* comp) {
	netlist_pingpong_t* comp_pp = comp->data;

	char buffer[200];
	char* name_alloc;
	netlist_access_t* access;

	sprintf(buffer, "wa%d", comp_pp->ports_wa_nb);
	name_alloc = namealloc(buffer);

	access = Netlist_Comp_AddAccess(comp, NETLIST_ACCESS_MEM_WA, name_alloc);
	netlist_access_mem_addr_t* access_addr = access->data;

	sprintf(buffer, "%s_data", access->name);
	name_alloc = namealloc(buffer);
	access_addr->port_d = Netlist_Comp_AddPort_In(comp, name_alloc, comp_pp->data_width);
	access_addr->port_d->access = access;

	sprintf(buffer, "%s_addr", access->name);
	name_alloc = namealloc(buffer);
	access_addr->port_a = Netlist_Comp_AddPort_In(comp, name_alloc, comp_pp->addr_width);
	access_addr->port_a->access = access;

	sprintf(buffer, "%s_en", access->name);
	name_alloc = namealloc(buffer);
	access_addr->port_e = Netlist_Comp_AddPort_In(comp, name_alloc, 1);
	access_addr->port_e->access = access;

	avl_p_add_overwrite(&comp_pp->access_ports_wa, access, access);

	comp_pp->ports_wa_nb++;
	return access;
}

void Netlist_PingPong_EnableDirect(netlist_comp_t* comp) {
	netlist_pingpong_t* pp_data = comp->data;

	if(pp_data->is_direct==true) return;

	char buffer[200];
	char* name_alloc;
	netlist_access_t* access;

	for(unsigned i=0; i<pp_data->cells_nb; i++) {
		sprintf(buffer, "cell%d", i);
		name_alloc = namealloc(buffer);

		access = Netlist_Comp_AddAccess(comp, NETLIST_ACCESS_MEM_D, name_alloc);
		netlist_access_mem_direct_t* access_direct = access->data;
		access_direct->cell_index = i;

		sprintf(buffer, "%s_wd", access->name);
		name_alloc = namealloc(buffer);
		access_direct->port_wd = Netlist_Comp_AddPort_In(comp, name_alloc, pp_data->data_width);
		access_direct->port_wd->access = access;

		sprintf(buffer, "%s_we", access->name);
		name_alloc = namealloc(buffer);
		access_direct->port_we = Netlist_Comp_AddPort_In(comp, name_alloc, 1);
		access_direct->port_we->access = access;

		sprintf(buffer, "%s_rd", access->name);
		name_alloc = namealloc(buffer);
		access_direct->port_rd = Netlist_Comp_AddPort_Out(comp, name_alloc, pp_data->data_width);
		access_direct->port_rd->access = access;

		avl_ip_add_overwrite(&pp_data->access_ports_d, i, access);
	}  // Loop on the cells

	// Remove the by-address accesses
	while(avl_p_isempty(&pp_data->access_ports_ra)==false) Netlist_PingPong_Remove_RA(avl_p_deepest_data(&pp_data->access_ports_ra));
	while(avl_p_isempty(&pp_data->access_ports_wa)==false) Netlist_PingPong_Remove_WA(avl_p_deepest_data(&pp_data->access_ports_wa));

	// Final flag
	pp_data->is_direct = true;
}
netlist_access_t* Netlist_PingPong_GetDirectAccess(netlist_comp_t* comp, unsigned index) {
	netlist_pingpong_t* pp_data = comp->data;
	netlist_access_t* access = NULL;
	avl_ip_find_data(&pp_data->access_ports_d, index, (void**)&access);
	return access;
}

void Netlist_PingPong_Remove_RA(netlist_access_t* access) {
	netlist_pingpong_t* comp_pp = access->component->data;
	// Remove from the parent memory data
	avl_p_rem_key(&comp_pp->access_ports_ra, access);
	comp_pp->ports_ra_nb--;
	// Finish the free
	Netlist_AccessRem_MemAddr_common(access);
}
void Netlist_PingPong_Remove_WA(netlist_access_t* access) {
	netlist_pingpong_t* comp_pp = access->component->data;
	// Remove from the parent memory data
	avl_p_rem_key(&comp_pp->access_ports_wa, access);
	comp_pp->ports_wa_nb--;
	// Finish the free
	Netlist_AccessRem_MemAddr_common(access);
}
void Netlist_PingPong_Remove_Direct(netlist_access_t* access) {
	netlist_pingpong_t* pp_data = access->component->data;
	netlist_access_mem_direct_t* memDirect = access->data;
	// Unlink from the parent memory component
	avl_ip_rem_key(&pp_data->access_ports_d, memDirect->cell_index);
	// Finish the free
	Netlist_AccessRem_MemDirect_common(access);
}

// Writing of VHDL of PingPong buffer
// FIXME This implementation will be removed when its functionality is properly split
//   into the other VHDL generator functions
static void Netlist_PingPong_GenVHDL_ram(FILE* F, netlist_vhdl_t* params, netlist_comp_t* comp) {
	netlist_pingpong_t* pp_data = comp->data;
	netlist_access_fifo_t* FifoAccess = pp_data->access_fifo->data;

	bool is_input = false;
	if(comp->model == NETLIST_COMP_PINGPONG_IN) is_input = true;

	netlist_access_t* access;
	netlist_access_mem_addr_t* memRA = NULL;
	netlist_access_mem_addr_t* memWA = NULL;

	int i;
	int nb_ram = pp_data->banks_nb;
	char* clkedge_testfunc = Netlist_GenVHDL_ClockEdge_TestFunc_port(pp_data->port_clk);

	if(is_input == true){
		access = avl_p_first(&pp_data->access_ports_ra)->data; // for only 1 RA access
		memRA = access->data;
	}
	else{
		access = avl_p_first(&pp_data->access_ports_wa)->data; // for only 1 WA access
		memWA = access->data;
	}

	// FIXME this code segfaults when one of these ports is NULL... which is very often
	bool simplify =
		(Netlist_PortTargets_CheckEmpty(pp_data->port_full_c)==true && Netlist_PortTargets_CheckEmpty(pp_data->port_ratio_c)==true) ||
		(Netlist_PortTargets_CheckEmpty(pp_data->port_full_n)==true && Netlist_PortTargets_CheckEmpty(pp_data->port_ratio_n)==true);

	char buffer[2000];
	char* name_ram = NULL;
	char* name_fsm = NULL;
	char* name_count = NULL;
	char* name_cmp = NULL;
	sprintf(buffer, "%s_ram_%x",comp->name,rand());
	name_ram = namealloc(buffer);
	sprintf(buffer, "%s_fsm_%x",comp->name,rand());
	name_fsm = namealloc(buffer);
	sprintf(buffer, "%s_count_%x",comp->name,rand());
	name_count = namealloc(buffer);
	sprintf(buffer, "%s_cmp_%x",comp->name,rand());
	name_cmp = namealloc(buffer);

	#if 1 /* PINGPONG */
	fprintf(F, "--------------------------------- TOP_PINGPONG --------------------------------- \n");

	Netlist_GenVHDL_Entity(F, comp, params);
	fputc('\n', F);

	fprintf(F, "architecture %s of %s is\n", params->architecture, Netlist_Comp_GetRename_Comp(comp, params));
	fputc('\n', F);
		// declaration of components
	fprintf(F, "	--declaration of components-- \n");

	fprintf(F, " 	component %s is \n",name_ram);
	fprintf(F, "	port( \n");
	fprintf(F, "		data_in    : in  std_logic_vector(%u downto 0); \n", pp_data->data_width-1);
	fprintf(F, "		clk	       : in  std_logic; \n");
	fprintf(F, "		enable     : in  std_logic; \n");
	fprintf(F, "		address_r	 : in  std_logic_vector(%u downto 0); \n", pp_data->addr_width-1);
	fprintf(F, "		address_w  : in  std_logic_vector(%u downto 0); \n", pp_data->addr_width-1);
	fprintf(F, "		data_out   : out std_logic_vector(%u downto 0) \n", pp_data->data_width-1);
	fprintf(F, "	); \n");
	fprintf(F, "	end component; \n");

	fprintf(F, "	 \n");

	fprintf(F, "	component %s is \n", name_count);
	fprintf(F, "	port( \n");
	fprintf(F, "		clk       : in  std_logic; \n");
	fprintf(F, "		c_clear   : in  std_logic; \n");
	fprintf(F, "		c_enable  : in  std_logic; \n");
	fprintf(F, "		c_out     : out std_logic_vector(%u downto 0) \n", pp_data->addr_width-1);
	fprintf(F, "	); \n");
	fprintf(F, "	end component; \n");

	fprintf(F, "	 \n");

	fprintf(F, "	component %s is \n", name_cmp);
	fprintf(F, "	port( \n");
	fprintf(F, "		cmp_in  : in  std_logic_vector(%u downto 0); \n", pp_data->addr_width-1);
	fprintf(F, "		N       : in  integer; \n");
	fprintf(F, "		cmp_out : out std_logic \n");
	fprintf(F, "	); \n");
	fprintf(F, "	end component; \n");

	fprintf(F, "	 \n");

	fprintf(F, "	component %s is \n", name_fsm);
	fprintf(F, "	port( \n");
	fprintf(F, "	clk				:in std_logic; \n");
	fprintf(F, "	reset				:in std_logic; \n");
	fprintf(F, "	switch_pp  			:in std_logic; \n");
	if(is_input == true)
		fprintf(F, "	rok				:in std_logic; \n");
	else
		fprintf(F, "	wok				:in std_logic; \n");
	fprintf(F, "	full	  			:in std_logic; \n");
	fprintf(F, "	enable_count			:out std_logic; \n");
	fprintf(F, "	clear_count	   		:out std_logic; \n");
	fprintf(F, "	enable_w			:out std_logic; \n");
	if(is_input == true)
		fprintf(F, "	read				:out std_logic; \n");
	else
		fprintf(F, "	write				:out std_logic; \n");
	fprintf(F, "	sel_out				:out std_logic_vector(%d downto 0) \n", nb_ram-1);
	fprintf(F, "	); \n");
	fprintf(F, "	end component; \n");

	fprintf(F, "	 \n");
			// declaration of internal signals
	fprintf(F, "	-- declaration of internal signals -- \n");
	for(i=0; i<nb_ram ; i++){
		fprintf(F, "	signal  address_%d						: std_logic_vector(%u downto 0); \n", i, pp_data->addr_width-1);
		fprintf(F, "	signal  data_out_%d						: std_logic_vector(%u downto 0); \n", i, pp_data->data_width-1);
		fprintf(F, "	signal	enable_w_%d						: std_logic; \n",i);
	}
	fprintf(F, "	signal	clear_count						: std_logic; \n");
	fprintf(F, "	signal	enable_count						: std_logic; \n");
	fprintf(F, "	signal	enable_w_fsm						: std_logic; \n");
	fprintf(F, "	signal	sel_out							: std_logic_vector(%d downto 0); \n", nb_ram-1);

	if(simplify == true){
		fprintf(F, "	signal  sig_full, sig_full_next					: std_logic := '0'; \n");
		fprintf(F, "	signal  sig_c_clear, sig_c_en, sig_cmp_out			: std_logic; \n");
		fprintf(F, "	signal  sig_c_out						: std_logic_vector(%u downto 0); \n", pp_data->addr_width-1);
	}
	else{
		fprintf(F, "	signal  sig_full1, sig_full1_next, sig_full2, sig_full2_next	: std_logic := '0'; \n");
		fprintf(F, "	signal  sig_c_clear_A, sig_c_clear_B				: std_logic; \n");
		fprintf(F, "	signal  sig_c_en_A, sig_c_en_B 					: std_logic; \n");
		fprintf(F, "	signal  sig_cmp_out_A, sig_cmp_out_B				: std_logic; \n");
		fprintf(F, "	signal  sig_c_out_A, sig_c_out_B				: std_logic_vector(%u downto 0); \n", pp_data->addr_width-1);
	}

	fprintf(F, "   \n");

	fprintf(F, "begin \n");

	fprintf(F, "	 \n");

	if(is_input == true) {
		for(i=0; i<nb_ram ; i++){
			fprintf(F, "	RAM_%d : %s port map( \n", i, name_ram);
			fprintf(F, "		data_in	=> %s, \n", FifoAccess->port_data->name);
			fprintf(F, "		clk => %s, \n", pp_data->port_clk->name);
			fprintf(F, "		enable => enable_w_%d, \n", i);
			fprintf(F, "		address_r => %s, \n", memRA->port_a->name);
			fprintf(F, "		address_w => address_%d, \n", i);
			fprintf(F, "		data_out =>data_out_%d	 \n", i);
			fprintf(F, "	); \n");

			fprintf(F, "	\n");
		}
	}
	else{
		for(i=0; i<nb_ram ; i++){
			fprintf(F, "	RAM_%d : %s port map( \n", i, name_ram);
			fprintf(F, "		data_in	=> %s, \n", memWA->port_d->name);
			fprintf(F, "		clk => %s, \n", pp_data->port_clk->name);
			fprintf(F, "		enable => enable_w_%d, \n", i);
			fprintf(F, "		address_r => address_%d, \n", i);
			fprintf(F, "		address_w => %s, \n", memWA->port_a->name);
			fprintf(F, "		data_out =>data_out_%d	 \n", i);
			fprintf(F, "	); \n");

			fprintf(F, "	\n");
		}
	}

	fprintf(F, "	 \n");

	if(simplify == true){

		fprintf(F, "	COUNT_A : %s port map( \n", name_count);
		fprintf(F, "		clk => %s, \n", pp_data->port_clk->name);
		fprintf(F, "		c_clear => sig_c_clear, \n");
		fprintf(F, "		c_enable => sig_c_en, \n");
		fprintf(F, "		c_out => sig_c_out \n");
		fprintf(F, "	); \n");

		fprintf(F, "	 \n");

		if(is_input == false && pp_data->port_setmax->source!= NULL){
			fprintf(F, "	CMP_A : %s port map( \n", name_cmp);
			fprintf(F, "		cmp_in => sig_c_out, \n");
			fprintf(F, "		N => %s, \n",  pp_data->port_setmax->name);
			fprintf(F, "		cmp_out => sig_cmp_out \n");
			fprintf(F, "	); \n");

		}
		else{
			fprintf(F, "	CMP_A : %s port map( \n", name_cmp);
			fprintf(F, "		cmp_in => sig_c_out, \n");
			fprintf(F, "		N => %u, \n",  pp_data-> cells_nb-1);
			fprintf(F, "		cmp_out => sig_cmp_out \n");
			fprintf(F, "	); \n");
		}
	}

	else{
		fprintf(F, "	COUNT_A : %s port map( \n", name_count);
		fprintf(F, "		clk => %s, \n", pp_data->port_clk->name);
		fprintf(F, "		c_clear => sig_c_clear_A, \n");
		fprintf(F, "		c_enable => sig_c_en_A, \n");
		fprintf(F, "		c_out => sig_c_out_A \n");
		fprintf(F, "	); \n");

		fprintf(F, "	 \n");

		fprintf(F, "	COUNT_B : %s port map( \n", name_count);
		fprintf(F, "		clk => %s, \n", pp_data->port_clk->name);
		fprintf(F, "		c_clear => sig_c_clear_B, \n");
		fprintf(F, "		c_enable => sig_c_en_B, \n");
		fprintf(F, "		c_out => sig_c_out_B \n");
		fprintf(F, "	); \n");

		fprintf(F, "	 \n");

		if(is_input == false && pp_data->port_setmax->source!= NULL){
			fprintf(F, "	CMP_A : %s port map( \n", name_cmp);
			fprintf(F, "		cmp_in => sig_c_out_A, \n");
			fprintf(F, "		N => %s, \n",  pp_data->port_setmax->name);
			fprintf(F, "		cmp_out => sig_cmp_out_A \n");
			fprintf(F, "	); \n");

			fprintf(F, "	 \n");

			fprintf(F, "	CMP_B : %s port map( \n", name_cmp);
			fprintf(F, "		cmp_in => sig_c_out_B, \n");
			fprintf(F, "		N => %s, \n",  pp_data->port_setmax->name);
			fprintf(F, "		cmp_out => sig_cmp_out_B \n");
			fprintf(F, "	); \n");
		}
		else{
			fprintf(F, "	CMP_A : %s port map( \n", name_cmp);
			fprintf(F, "		cmp_in => sig_c_out_A, \n");
			fprintf(F, "		N => %u, \n",  pp_data-> cells_nb-1);
			fprintf(F, "		cmp_out => sig_cmp_out_A \n");
			fprintf(F, "	); \n");

			fprintf(F, "	 \n");

			fprintf(F, "	CMP_B : %s port map( \n", name_cmp);
			fprintf(F, "		cmp_in => sig_c_out_B, \n");
			fprintf(F, "		N => %u, \n",  pp_data-> cells_nb-1);
			fprintf(F, "		cmp_out => sig_cmp_out_B \n");
			fprintf(F, "	); \n");
		}
	}
	fprintf(F, " \n");

	fprintf(F, "  	FSM1: 	%s port map( \n", name_fsm);
	fprintf(F, "		clk => %s, \n", pp_data->port_clk->name);
	fprintf(F, "		reset => %s, \n", pp_data->port_reset->name);
	fprintf(F, "		switch_pp => %s, \n", pp_data->port_switch->name);
	if(is_input == true)
		fprintf(F, "		rok => %s, \n", FifoAccess->port_ack->name);
	else
		fprintf(F, "		wok => %s, \n", FifoAccess->port_ack->name);
	if(simplify == true)
		fprintf(F, "		full => sig_full, \n");
	else
		fprintf(F, "		full => sig_full1, \n");
	fprintf(F, "		enable_count => enable_count, \n");
	fprintf(F, "		clear_count => clear_count, \n");
	fprintf(F, "		enable_w => enable_w_fsm, \n");
	if(is_input == true)
		fprintf(F, "		read => %s, \n", FifoAccess->port_rdy->name);
	else
		fprintf(F, "		write => %s, \n", FifoAccess->port_rdy->name);
	fprintf(F, "		sel_out => sel_out \n");
	fprintf(F, "	); \n");

	fprintf(F, "	 \n");


	if(simplify){
		fprintf(F, "	process(clk, reset) \n");
		fprintf(F, "	begin \n");

		fprintf(F, "		if %s(clk) then \n", clkedge_testfunc);
		fprintf(F, "			if reset='1' then \n");
		fprintf(F, "				sig_full <= '0'; \n");
		fprintf(F, "			else \n");
		fprintf(F, "				sig_full <= sig_full_next; \n");
		fprintf(F, "			end if; \n");
		fprintf(F, "		end if; \n");

		fprintf(F, "	end process; \n");

		fprintf(F, "	 \n");

		for(i=0 ; i < nb_ram ; i++){
			fprintf(F, "	address_%d <= sig_c_out; \n", i);
		}

		fprintf(F, "	sig_full_next <= sig_cmp_out; \n");
		fprintf(F, "	sig_c_clear <= clear_count; \n");
		fprintf(F, "	sig_c_en <= enable_count; \n");

		fprintf(F, "	 \n");

	}

	else{
		fprintf(F, "	process(clk, reset) \n");
		fprintf(F, "	begin \n");

		fprintf(F, "		if %s(clk) then \n", clkedge_testfunc);
		fprintf(F, "			if reset='1' then \n");
		fprintf(F, "				sig_full1 <= '0'; \n");
		fprintf(F, "				sig_full2 <= '0'; \n");
		fprintf(F, "			else \n");
		fprintf(F, "				sig_full1 <= sig_full1_next; \n");
		fprintf(F, "				sig_full2 <= sig_full2_next; \n");
		fprintf(F, "			end if; \n");
		fprintf(F, "		end if; \n");

		fprintf(F, "	end process; \n");

		fprintf(F, "	 \n");

		// to link the 2 counters and comparators
		fprintf(F, "	process(sel_out, enable_count, clear_count, sig_c_out_A, sig_c_out_B, sig_cmp_out_A, sig_cmp_out_B ) \n");
		fprintf(F, "	begin \n");

		for(i=0 ; i<nb_ram ; i++)
			fprintf(F, "		address_%d <= (others => '0'); \n", i);
		fprintf(F, "		sig_full1_next <= '0'; \n");
		fprintf(F, "		sig_full2_next <= '0'; \n");
		fprintf(F, "		sig_c_clear_A <= '0'; \n");
		fprintf(F, "		sig_c_en_A <= '0'; \n");
		fprintf(F, "		sig_c_clear_B <= '0'; \n");
		fprintf(F, "		sig_c_en_B <= '0'; \n");

		fprintf(F, "	 \n");

		for(i=0 ; i<nb_ram ; i++){
			fprintf(F, "		if sel_out(%d)='1' then \n", i);
			if( i % 2 == 0){
				fprintf(F, "			address_%d <= sig_c_out_A; \n", i);
				fprintf(F, "			sig_c_clear_A <= clear_count; \n");
				fprintf(F, "			sig_c_en_A <= enable_count; \n");
				fprintf(F, "			sig_full1_next <= sig_cmp_out_A; \n");
				fprintf(F, "			sig_full2_next <= sig_cmp_out_B; \n");
			}
			else{
				fprintf(F, "			address_%d <= sig_c_out_B; \n", i);
				fprintf(F, "			sig_c_clear_B <= clear_count; \n");
				fprintf(F, "			sig_c_en_B <= enable_count; \n");
				fprintf(F, "			sig_full1_next <= sig_cmp_out_B; \n");
				fprintf(F, "			sig_full2_next <= sig_cmp_out_A; \n");
			}
			fprintf(F, "		end if; \n");
		}
		fprintf(F, "	end process; \n");
	}

	if(is_input == true){

		fprintf(F, "	process(sel_out, enable_w_fsm) \n");
		fprintf(F, "	begin \n");
		for(i=0 ; i<nb_ram ; i++)
			fprintf(F, "		enable_w_%d <= '0'; \n", i);
		for(i=0 ; i<nb_ram ; i++){
			fprintf(F, "		if sel_out(%d)='1' then \n", i);
			fprintf(F, "			enable_w_%d <= enable_w_fsm; \n", i);
			fprintf(F, "		end if; \n");
		}
		fprintf(F, "	end process; \n");
	}
	else{
		fprintf(F, "	process(sel_out,%s) \n", memWA->port_e->name);
		fprintf(F, "	begin \n");

		for(i=0 ; i<nb_ram ; i++)
			fprintf(F, "		enable_w_%d <= '0'; \n", i);
		for(i=0 ; i<nb_ram ; i++){
			fprintf(F, "		if sel_out(%d)='1' then \n", i);
			fprintf(F, "			enable_w_%d <= %s; \n", i, memWA->port_e->name);
			fprintf(F, "		end if; \n");
		}
		fprintf(F, "	end process; \n");
	}
	fprintf(F, "	 \n");

		// assignation of outputs of the pingpong buffer
	fprintf(F, "	-- assignation of outputs of the pingpong buffer -- \n");

	fprintf(F, "	 \n");

	if(simplify == true) {
		if(Netlist_PortTargets_CheckEmpty(pp_data->port_full_c)==false) {
			fprintf(F, "	%s <= sig_full; \n", pp_data->port_full_c->name);
		}
	}
	else {
		if(Netlist_PortTargets_CheckEmpty(pp_data->port_full_c)==false)
			fprintf(F, "	%s <= sig_full1; \n", pp_data->port_full_c->name);
		if(Netlist_PortTargets_CheckEmpty(pp_data->port_full_n)==false)
			fprintf(F, "	%s <= sig_full2; \n", pp_data->port_full_n->name);
	}

	fprintf(F, "	 \n");

	fprintf(F, "	process(sel_out,");
		for(i=0 ; i<nb_ram ; i++){
			if(Netlist_PortTargets_CheckEmpty(pp_data->port_ratio_c)==false || Netlist_PortTargets_CheckEmpty(pp_data->port_ratio_n)==false)
				fprintf(F, " data_out_%d, address_%d", i, i);
			else
				fprintf(F, " data_out_%d", i);
			if(i!=nb_ram-1) fprintf(F, ",");
		}
		fprintf(F, ")\n");
		fprintf(F, "	begin \n");


		for(i=0 ; i<nb_ram ; i++){
			fprintf(F, "		if sel_out(%d)='1' then \n", i);
			if(is_input == true){
				if(i == 0) fprintf(F, "			%s <= data_out_%d; \n", memRA->port_d->name, nb_ram-1);
				else       fprintf(F, "			%s <= data_out_%d; \n", memRA->port_d->name, i-1);
				fprintf(F, "		end if; \n");
			}
			else{
				if(i == 0) 	fprintf(F, "			%s <= data_out_%d; \n", FifoAccess->port_data->name, nb_ram-1);
				else 		fprintf(F, "			%s <= data_out_%d; \n", FifoAccess->port_data->name, i-1);
				fprintf(F, "		end if; \n");
			}

			if(Netlist_PortTargets_CheckEmpty(pp_data->port_ratio_c)==false)
				fprintf(F, "			%s <= address_%d; \n", pp_data->port_ratio_c->name, i);

			if(Netlist_PortTargets_CheckEmpty(pp_data->port_ratio_n)==false) {
				if(i == 0) fprintf(F, "			%s <= address_%d; \n", pp_data->port_ratio_n->name, nb_ram-1);
				else 	     fprintf(F, "			%s <= address_%d; \n", pp_data->port_ratio_n->name, i-1);
			}
		}

	fprintf(F, "	end process; \n");

	fprintf(F, " \n");

	fprintf(F, "end architecture; \n");

	fprintf(F, " \n");

	#endif

	#if 1 /* FSM_PP */
	fprintf(F, "----------------------------------- FSM_PP ------------------------------------ \n");

	Netlist_GenVHDL_StdLibs(F);

	fprintf(F, " \n");

	fprintf(F, "entity %s is \n",name_fsm);

	fprintf(F, "port( \n");
	fprintf(F, "	clk				:in std_logic; \n");
	fprintf(F, "	reset				:in std_logic; \n");
	fprintf(F, "	switch_pp  			:in std_logic; \n");
	if(is_input == true)
		fprintf(F, "	rok				:in std_logic; \n");
	else
		fprintf(F, "	wok				:in std_logic; \n");
	fprintf(F, "	full	  			:in std_logic; \n");
	fprintf(F, "	enable_count			:out std_logic; \n");
	fprintf(F, "	clear_count	   		:out std_logic; \n");
	fprintf(F, "	enable_w			:out std_logic; \n");
	if(is_input == true)
		fprintf(F, "	read				:out std_logic; \n");
	else
		fprintf(F, "	write				:out std_logic; \n");
	fprintf(F, "	sel_out				:out std_logic_vector(%d downto 0) \n", nb_ram-1);
	fprintf(F, "); \n");
	fprintf(F, "end %s; \n", name_fsm);

	fprintf(F, " \n");

	fprintf(F, "architecture A of %s is \n", name_fsm);

	fprintf(F, " \n");

	fprintf(F, "	signal ram_cur, ram_next : std_logic_vector(%d downto 0) := (others => '0'); \n", nb_ram-1);
	fprintf(F, "	type state is (S0_Clear_Count,S1_Read_Load,S2_Count_Wait, S3_Full); \n");
	fprintf(F, "	signal current_state, next_state : state; \n");
	fprintf(F, "	signal prev_switch_pp, prev_switch_pp_next : std_logic;\n");

	fprintf(F, " \n");

	fprintf(F, "begin \n");

	fprintf(F, "	-- FSM managing the switching of RAM -- \n");

	fprintf(F, "	process(clk) \n");
	fprintf(F, "	begin \n");

	fprintf(F, "		if %s(clk) then \n", clkedge_testfunc);
	fprintf(F, " \n");
	fprintf(F, "			ram_cur <= ram_next; \n");
	fprintf(F, "			prev_switch_pp <= prev_switch_pp_next; \n");
	fprintf(F, " \n");
	fprintf(F, "			if reset='1' then \n");
	fprintf(F, "				prev_switch_pp <= '0'; \n");
	fprintf(F, "				ram_cur <= (others => '0'); \n");
	fprintf(F, "				ram_cur(0) <= '1'; \n");
	fprintf(F, "			end if; \n");
	fprintf(F, "		end if; \n");

	fprintf(F, "	end process; \n");

	fprintf(F, " \n");

	fprintf(F, "	process(ram_cur,switch_pp) \n");
	fprintf(F, "	begin \n");
	fprintf(F, " \n");
	fprintf(F, "		ram_next <= (others => '0'); \n  \n");
	fprintf(F, " \n");
	for(i=0 ; i < nb_ram ; i++){
		fprintf(F, "		if ram_cur(%d)='1' then  \n", i);
		fprintf(F, "			if (switch_pp xor prev_switch_pp) = '1' then  \n");
		if(i!=nb_ram-1)
			fprintf(F, "				ram_next(%d) <= '1'; \n", i+1);
		else
			fprintf(F, "				ram_next(0) <= '1'; \n");
		fprintf(F, "				prev_switch_pp_next <= '1'; \n");
		fprintf(F, "			else \n");
		fprintf(F, "				ram_next(%d) <= '1'; \n", i);
		fprintf(F, "				prev_switch_pp_next <= '0'; \n");
		fprintf(F, "			end if; \n");
		fprintf(F, "		end if; \n");
	}
	fprintf(F, "	end process; \n");

	fprintf(F, "	 \n");

	fprintf(F, "	-- FSM managing outputs controlling the different part of the buffer -- \n");

	fprintf(F, "	 \n");

	fprintf(F, "	process(clk, reset) \n");
	fprintf(F, "	begin \n");

	fprintf(F, "		if %s(clk) then \n", clkedge_testfunc);
	fprintf(F, "			if reset='1' then \n");
	fprintf(F, "				current_state <= S0_Clear_Count; \n");
	fprintf(F, "			else \n");
	fprintf(F, "				current_state <= next_state; \n");
	fprintf(F, "			end if; \n");
	fprintf(F, "		end if; \n");

	fprintf(F, "	end process; \n");

	fprintf(F, "	 \n");
	if(is_input == true)
		fprintf(F, "	process (current_state,full,rok,switch_pp) \n");
	else
		fprintf(F, "	process (current_state,full,wok,switch_pp) \n");
	fprintf(F, "	begin \n");

	fprintf(F, "		enable_count<= '0'; \n");
	fprintf(F, "		clear_count<= '0'; \n");
	fprintf(F, "		enable_w<= '0'; \n");
	if(is_input == true)
		fprintf(F, "		read<= '0'; \n");
	else
		fprintf(F, "		write<= '0'; \n");
	fprintf(F, "		next_state<=current_state; \n");

	fprintf(F, "		 \n");

	fprintf(F, "		case current_state is \n");

	fprintf(F, "			when S0_Clear_Count => \n");
	fprintf(F, "				clear_count <= '1'; \n");
	fprintf(F, "				next_state <= S1_Read_Load; \n");

	fprintf(F, " \n");

	fprintf(F, "			when S1_Read_Load => \n");
	if(is_input == true)
		fprintf(F, "				read <= '1'; \n");
	else
		fprintf(F, "				write <= '1'; \n");
	fprintf(F, "				if switch_pp='1' then \n");
	fprintf(F, "					next_state <= S0_Clear_Count; \n");
	fprintf(F, "				end if; \n");
	if(is_input == true)
		fprintf(F, "				if (not(rok) and not(full) and not(switch_pp))='1' then \n");
	else
		fprintf(F, "				if (not(wok) and not(full) and not(switch_pp))='1' then \n");
	fprintf(F, "					next_state <= S1_Read_Load; \n");
	fprintf(F, "				end if; \n");
	if(is_input == true)
		fprintf(F, "				if (rok and not(full) and not(switch_pp))='1' then \n");
	else
		fprintf(F, "				if (wok and not(full) and not(switch_pp))='1' then \n");
	fprintf(F, "					next_state <= S2_Count_Wait; \n");
	fprintf(F, "				end if; \n");
	fprintf(F, "				if (full and not(switch_pp))='1' then \n");
	fprintf(F, "					next_state <= S3_Full; \n");
	fprintf(F, "				end if; \n");

	fprintf(F, " \n");

	fprintf(F, "			when S2_Count_Wait => \n");
	if(is_input == true)
		fprintf(F, "				read <= '1'; \n");
	else
		fprintf(F, "				write <= '1'; \n");
	fprintf(F, "				enable_w <= '1'; \n");
	fprintf(F, "				enable_count <= '1'; \n");
	fprintf(F, "				if switch_pp='1' then \n");
	fprintf(F, "					next_state <= S0_Clear_Count; \n");
	fprintf(F, "				else \n");
	fprintf(F, "					next_state <= S1_Read_Load; \n");
	fprintf(F, "				end if; \n");

	fprintf(F, " \n");

	fprintf(F, "			when S3_Full => \n");
	fprintf(F, "				if switch_pp='1' then \n");
	fprintf(F, "					next_state<=S0_Clear_Count; \n");
	fprintf(F, "				else \n");
	fprintf(F, "					next_state<=S3_Full; \n");
	fprintf(F, "				end if; \n");

	fprintf(F, " \n");

	fprintf(F, "			when others => \n");
	fprintf(F, "				next_state <= S0_Clear_Count; \n");

	fprintf(F, "		end case; \n");
	fprintf(F, "	end process; \n");

	fprintf(F, "	 \n");

	fprintf(F, "	sel_out <= ram_cur; \n");

	fprintf(F, "	 \n");


	fprintf(F, " \n");

	fprintf(F, "end architecture; \n");

	#endif

	#if 1 /* RAM */

	fprintf(F, "----------------------------------- RAM ------------------------------------ \n");

	Netlist_GenVHDL_StdLibs(F);

	fprintf(F, "use ieee.numeric_std.ALL; \n");

	fprintf(F, " \n");

	fprintf(F, "entity %s is \n", name_ram);

	fprintf(F, "port( \n");
	fprintf(F, "	data_in		:in std_logic_vector(%u downto 0); \n", pp_data->data_width-1);
	fprintf(F, "	clk        	:in std_logic; \n");
	fprintf(F, "	enable      	:in std_logic; \n");
	fprintf(F, "	address_r	:in std_logic_vector(%u downto 0); \n", pp_data->addr_width-1);
	fprintf(F, "	address_w	:in std_logic_vector(%u downto 0); \n", pp_data->addr_width-1);
	fprintf(F, "	data_out	:out std_logic_vector(%u downto 0) \n", pp_data->data_width-1);
	fprintf(F, "); \n");

	fprintf(F, "end %s; \n", name_ram);

	fprintf(F, " \n");

	fprintf(F, "architecture A of %s is \n", name_ram);

	fprintf(F, "	 \n");

	fprintf(F, "	type ram_type is array (%u downto 0) of std_logic_vector(%u downto 0); \n", pp_data->cells_nb-1, pp_data->data_width-1);
	fprintf(F, "	signal ram : ram_type := (others => (others => '0')); \n");

	fprintf(F, " \n");

	fprintf(F, "begin \n");
	fprintf(F, "		-- Synchronous writing \n");
	fprintf(F, "	 \n");
	fprintf(F, "	process(clk) \n");
	fprintf(F, "	begin \n");

	fprintf(F, "		if %s(clk) then \n", clkedge_testfunc);
	fprintf(F, "			if enable = '1' then  \n");
	fprintf(F, "				ram(to_integer(unsigned(address_w))) <= data_in; \n");
	fprintf(F, "			end if; \n");

	fprintf(F, "		end if; \n");

	fprintf(F, "	end process; \n");

	fprintf(F, " \n");

	fprintf(F, "		-- Asynchronous reading \n");
	fprintf(F, "	 \n");
	fprintf(F, "	data_out <= ram( to_integer(unsigned(address_r)) ); \n");
	fprintf(F, " \n");
	fprintf(F, "end architecture; \n");

	#endif

	#if 1 /* COUNT */

	fprintf(F, "----------------------------------- COUNT ---------------------------------- \n");

	Netlist_GenVHDL_StdLibs(F);

	fprintf(F, "use ieee.numeric_std.ALL; \n");

	fprintf(F, " \n");

	fprintf(F, "entity %s is \n", name_count);
	fprintf(F, "port( \n");
	fprintf(F, "	clk		:in std_logic; \n");
	fprintf(F, "	c_clear		:in std_logic; \n");
	fprintf(F, "	c_enable       	:in std_logic; \n");
	fprintf(F, "	c_out      	:out std_logic_vector(%u downto 0) \n", pp_data->addr_width-1);
	fprintf(F, "); \n");
	fprintf(F, "end %s; \n", name_count);

	fprintf(F, " \n");

	fprintf(F, "architecture A of %s is \n", name_count);
	fprintf(F, "	signal count, count_suiv : integer := 0; \n");
	fprintf(F, "	 \n");
	fprintf(F, "begin \n");
	fprintf(F, "	 \n");
	fprintf(F, "	process(clk, c_clear) \n");
	fprintf(F, "	begin \n");

	fprintf(F, "		if %s(clk) then \n", clkedge_testfunc);
	fprintf(F, "			if c_clear ='1' then \n");
	fprintf(F, "				count <= 0; \n");
	fprintf(F, "			else  \n");
	fprintf(F, "				count <= count_suiv; \n");
	fprintf(F, "			end if; \n");
	fprintf(F, "		end if; \n");
	fprintf(F, "	end process; \n");

	fprintf(F, "	 \n");

	fprintf(F, "	process(c_clear,c_enable,count) \n");
	fprintf(F, "	begin			 \n");

	fprintf(F, "		if c_enable = '1' then  \n");
	fprintf(F, "			count_suiv <= count + 1; \n");
	fprintf(F, "		else \n");
	fprintf(F, "			count_suiv <= count; \n");
	fprintf(F, "		end if; \n");

	fprintf(F, "	end process; \n");

	fprintf(F, "	 \n");

	fprintf(F, "	c_out<=std_logic_vector(to_unsigned(count, c_out'length)); \n");

	fprintf(F, "	 \n");

	fprintf(F, "end architecture; \n");

	#endif

	#if 1 /* CMP */

	fprintf(F, "----------------------------------- CMP ------------------------------------ \n");

	Netlist_GenVHDL_StdLibs(F);

	fprintf(F, "use ieee.numeric_std.ALL; \n");

	fprintf(F, " \n");

	fprintf(F, "entity %s is \n", name_cmp);
	fprintf(F, "port( \n");
	fprintf(F, "	cmp_in		:in std_logic_vector(%u downto 0); \n", pp_data->addr_width-1);
	fprintf(F, "	N        	:in integer; \n");
	fprintf(F, "	cmp_out		:out std_logic \n");
	fprintf(F, "); \n");
	fprintf(F, "end %s; \n", name_cmp);

	fprintf(F, " \n");

	fprintf(F, "architecture A of %s is \n", name_cmp);

	fprintf(F, "	signal cmp_in_int : integer; \n");

	fprintf(F, "begin \n");
	fprintf(F, "	cmp_in_int <= to_integer(unsigned(cmp_in)); \n");

	fprintf(F, "	process(cmp_in_int, N) \n");
	fprintf(F, "	begin \n");

	fprintf(F, "		if cmp_in_int < N then \n");
	fprintf(F, "			cmp_out <= '0'; \n");
	fprintf(F, "		else \n");
	fprintf(F, "			cmp_out <= '1'; \n");
	fprintf(F, "		end if; \n");

	fprintf(F, "	end process; \n");

	fprintf(F, " \n");

	fprintf(F, "end architecture; \n");

	#endif
}

// Internal storage in registers.
// Generic case, multi-bank, where the banks are used as a circular buffer.
// Data does not switch between banks.
// Note: This function should also work for storage as RAM
static void Netlist_PingPong_GenVHDL_reg_in(FILE* F, netlist_vhdl_t* params, netlist_comp_t* comp) {
	netlist_pingpong_t* pp_data = comp->data;

	netlist_access_fifo_t* FifoAccess = pp_data->access_fifo->data;
	unsigned banks_nb = pp_data->banks_nb;
	unsigned cells_nb = pp_data->cells_nb;
	unsigned width_data = pp_data->data_width;
	unsigned width_fill = uint_bitsnb(cells_nb);
	unsigned width_banksnb = uint_bitsnb(banks_nb-1);

	bool fill_is_array = false;
	if(pp_data->port_ratio_c!=NULL || pp_data->port_full_c!=NULL) fill_is_array = true;
	if(pp_data->port_ratio_n!=NULL || pp_data->port_full_n!=NULL) fill_is_array = true;
	bool fill_next_is_fifo = false;
	if(banks_nb==2 && (pp_data->port_ratio_c!=NULL || pp_data->port_full_c!=NULL)) fill_next_is_fifo = true;

	fprintf(F,
		"\n"
		"library ieee;\n"
		"use ieee.numeric_std.all;\n"
		"\n"
	);

	Netlist_GenVHDL_Entity(F, comp, params);

	fprintfm(F,
		"\n"
		"architecture %s of %s is\n", params->architecture, Netlist_Comp_GetRename_Comp(comp, params),
		"\n"
		"	-- General parameters\n"
		"	constant cells_nb is integer := %u;\n", cells_nb,
		"	constant banks_nb is integer := %u;\n", banks_nb,
		"\n"
		"	-- All registers\n"
		"	type onebank_t is array (0 to cells_nb-1) of ",
		NULL
	);
	Netlist_GenVHDL_StdLogicType_be(F, width_data, NULL, ";\n");
	fprintfm(F,
		"	type allbanks_t is array (0 to banks_nb-1) of onebank_t;\n"
		"	signal allbanks : allbanks_t;"
		"\n"
		"	-- Signals containing fill level of banks\n"
		"\n",
		"	signal bankfill :%s unsigned (%u downto 0);\n", fill_is_array==true ? " array (0 to banks_nb-1) of" : "", width_fill-1,
		"\n",
		"	signal bank_fifo_idx :     unsigned (%u downto 0);\n", width_banksnb-1,
		"	signal bank_fifo_idx1 :    unsigned (%u downto 0);\n", width_banksnb-1,
		"	signal bank_circuit_idx :  unsigned (%u downto 0);\n", width_banksnb-1,
		" signal bank_circuit_idx1 : unsigned (%u downto 0);\n", width_banksnb-1,
		NULL
	);
	if(fill_next_is_fifo==false) {
		fprintf(F, "	signal bank_next_idx :     unsigned (%u downto 0);\n", width_banksnb-1);
		fprintf(F, "	signal bank_next_idx1 :    unsigned (%u downto 0);\n", width_banksnb-1);
	}
	fputc('\n', F);

	Netlist_GenVHDL_MyToInteger(F);

	fprintfm(F,
		"\n"
		"begin\n"
		"\n"
		"\t-- Sequential process\n"
		"\n"
		"	process (%s)\n", pp_data->port_clk->name,
		"	begin\n"
		"\n"
		"		if %s(%s) then\n", Netlist_GenVHDL_ClockEdge_TestFunc_port(pp_data->port_clk), pp_data->port_clk->name,
		"			if %s = '%c' then\n", pp_data->port_reset->name, Netlist_Access_Reset_GetActiveState(pp_data->port_reset->access),
		"\n"
		"				--Reset\n"
		"				bankfill(0) <= 0;\n", fill_is_array==true ? "(0)" : "",
		"\n"
		"				bank_fifo_idx <= 1;\n"
		"				bank_circuit_idx <= 0;\n",
		NULL
	);
	if(fill_next_is_fifo==false) {
		fprintf(F, "				bank_next_idx <= 1;\n");
	}

	fprintfm(F,
		"\n"
		"			else\n"
		"\n"
		"				-- Normal behaviour\n"
		"\n"
		"				-- Switch if asked circuit-side\n"
		"				if %s = '1' then\n", pp_data->port_switch->name,
		"\n"
		"					bank_circuit_idx <= bank_circuit_idx1;\n",
		NULL
	);
	if(fill_next_is_fifo==false) {
		fprintf(F, "					bank_next_idx <= bank_next_idx1;\n");
	}
	fprintfm(F,
		"					-- Increment bank_fifo_idx only if bank_fifo_idx was blocking\n"
		"					if bankfill%s = cells_nb and bank_fifo_idx1 = bank_circuit_idx then\n",
			fill_is_array==true ? "(to_integer(bank_fifo_idx))" : "",
		"						bank_fifo_idx <= bank_fifo_idx1;\n"
		"						bankfill%s <= 0;\n", fill_is_array==true ? "(to_integer(bank_fifo_idx1))" : "",
		"					end if;\n"
		"\n"
		"				-- No switch, read from FIFO\n"
		"				elsif %s = '1' and bankfill%s /= cells_nb then\n",
			FifoAccess->port_ack->name, fill_is_array==true ? "(to_integer(bank_fifo_idx))" : "",
		"\n"
		"					allbanks(to_integer(bank_fifo_idx))(bankfill%s) <= %s;\n",
			fill_is_array==true ? "(to_integer(bank_fifo_idx))" : "", FifoAccess->port_data->name,
		"					bankfill%s <= bankfill%s + 1;\n",
			fill_is_array==true ? "(to_integer(bank_fifo_idx))" : "",
			fill_is_array==true ? "(to_integer(bank_fifo_idx))" : "",
		"					-- Increment bank_fifo_idx if current is full and other is available\n"
		"					if bankfill%s = cells_nb and bank_fifo_idx1 /= bank_circuit_idx then\n",
			fill_is_array==true ? "(to_integer(bank_fifo_idx))" : "",
		"						bank_fifo_idx <= bank_fifo_idx1;\n"
		"						bankfill%s <= 0;\n", fill_is_array==true ? "(to_integer(bank_fifo_idx1))" : "",
		"					end if;\n"
		"\n"
		"				end if;\n"
		"\n"

		"				-- Circuit-side direct writes\n",
		NULL
	);

	avl_ip_foreach(&pp_data->access_ports_d, scan) {
		netlist_access_t* access = scan->data;
		netlist_access_mem_direct_t* memd_data = access->data;
		if(memd_data->port_wd==NULL) continue;
		fprintfm(F,
			"				if %s = '1' then\n", memd_data->port_we->name,
			"					allbanks(to_integer(bank_circuit_idx))(%u) <= %s;\n", memd_data->cell_index, memd_data->port_wd->name,
			"				end if;\n",
			NULL
		);
	}

	fprintf(F,
		"\n"
		"				-- Circuit-side by-address writes\n"
	);

	avl_p_foreach(&pp_data->access_ports_wa, scan) {
		netlist_access_t* access = scan->data;
		netlist_access_mem_addr_t* memwa_data = access->data;
		fprintfm(F,
			"				if %s = '1' then\n", memwa_data->port_e->name,
			"					allbanks(to_integer(bank_circuit_idx))(to_integer(%s)) <= %s;\n", memwa_data->port_a->name, memwa_data->port_d->name,
			"				end if;\n",
			NULL
		);
	}

	fprintfm(F,
		"\n"
		"			end if;  -- Reset\n"
		"		end if;  -- Clock\n"
		"\n"
		"	end process;\n"
		"\n"
		"\n"
		"	-- Combinatorial process\n"
		"\n"
		"	process (bankfill%s)\n", fill_is_array==true ? ", bank_fifo_idx" : "",
		"	begin\n"
		"\n"
		"		-- Default values\n"
		"		%s <= '0';\n", FifoAccess->port_rdy->name,
		"\n"
		"		if bankfill%s /= cells_nb then\n", fill_is_array==true ? "(to_integer(bank_fifo_idx))" : "",
		"			%s <= '1';\n", FifoAccess->port_rdy->name,
		"		end if;\n"
		"\n"
		"	end process;\n"
		"\n"
		"	-- Combinatorial assignations\n"
		"\n"
		"	bank_fifo_idx1 <= 0 when bank_fifo_idx = banks_nb-1 else bank_fifo_idx + 1;\n"
		"	bank_circuit_idx1 <= 0 when bank_circuit_idx = banks_nb-1 else bank_circuit_idx + 1;\n",
		NULL
	);
	if(fill_next_is_fifo==false) {
		fprintf(F, "	bank_next_idx1 <= 0 when bank_next_idx = banks_nb-1 else bank_next_idx + 1;\n");
	}

	fprintf(F,
		"\n"
		"\t-- Other outputs\n"
		"\n"
		"	-- Circuit-side direct writes\n"
	);

	avl_ip_foreach(&pp_data->access_ports_d, scan) {
		netlist_access_t* access = scan->data;
		netlist_access_mem_direct_t* memd_data = access->data;
		if(memd_data->port_rd==NULL) continue;
		fprintf(F, "	%s <= allbanks(to_integer(bank_circuit_idx))(%u);\n",
			memd_data->port_rd->name, memd_data->cell_index
		);
	}

	fprintf(F,
		"\n"
		"	-- Circuit-side by-address writes\n"
	);

	avl_p_foreach(&pp_data->access_ports_ra, scan) {
		netlist_access_t* access = scan->data;
		netlist_access_mem_addr_t* memra_data = access->data;
		fprintf(F, "	%s <= allbanks(to_integer(bank_circuit_idx))(to_integer(%s));\n",
			memra_data->port_d->name, memra_data->port_a->name
		);
	}

	fprintf(F,
		"\n"
		"	-- Outputs: fullness of banks\n"
		"\n"
	);

	if(pp_data->port_ratio_c!=NULL) {
		fprintf(F, "	%s <= bankfill(to_integer(bank_circuit_idx));\n", pp_data->port_ratio_c->name);
	}
	if(pp_data->port_full_c!=NULL) {
		fprintf(F, "	%s <= '1' when bankfill(to_integer(bank_circuit_idx)) = cells_nb else '0';\n", pp_data->port_full_c->name);
	}
	if(pp_data->port_ratio_n!=NULL) {
		fprintf(F, "	%s <= bankfill(to_integer(%s));\n",
			pp_data->port_ratio_n->name, fill_next_is_fifo==true ? "bank_fifo_idx" : "bank_next_idx"
		);
	}
	if(pp_data->port_full_n!=NULL) {
		fprintf(F, "	%s <= '1' when bankfill(to_integer(%s)) = cells_nb else '0';\n",
			pp_data->port_full_n->name, fill_next_is_fifo==true ? "bank_fifo_idx" : "bank_next_idx"
		);
	}

	fprintf(F,
		"\n"
		"end architecture;\n"
		"\n"
	);

}
static void Netlist_PingPong_GenVHDL_reg_out(FILE* F, netlist_vhdl_t* params, netlist_comp_t* comp) {
	fprintf(F, "-- FIXME Not finished\n");
	// FIXME TODO
}

// For the case with only 2 banks.
// The data is transferred between banks.
static void Netlist_PingPong_GenVHDL_reg_in_simple(FILE* F, netlist_vhdl_t* params, netlist_comp_t* comp) {
	netlist_pingpong_t* pp_data = comp->data;
	assert(pp_data->banks_nb==2);
	unsigned cells_nb = pp_data->cells_nb;
	unsigned banks_nb = pp_data->banks_nb;
	unsigned width_data = pp_data->data_width;
	unsigned width_fill = uint_bitsnb(cells_nb);

	bool fill_circuit_en = false;
	if(pp_data->port_ratio_c!=NULL || pp_data->port_full_c!=NULL) fill_circuit_en = true;

	netlist_access_fifo_t* FifoAccess = pp_data->access_fifo->data;

	// WARNING
	// port_ratio1 is assumed to be the nb of values available to the circuit
	// port_ratio2 is assumed to be the nb of values read from FIFO

	fprintf(F,
		"\n"
		"library ieee;\n"
		"use ieee.numeric_std.all;\n"
		"\n"
	);

	Netlist_GenVHDL_Entity(F, comp, params);
	fputc('\n', F);

	fprintf(F, "architecture %s of %s is\n", params->architecture, Netlist_Comp_GetRename_Comp(comp, params));
	fputc('\n', F);

	ftabprintf(F, 1, "-- General parameters\n");
	ftabprintf(F, 1, "constant cells_nb : integer := %u;\n", cells_nb);
	ftabprintf(F, 1, "constant banks_nb : integer := %u;\n", banks_nb);
	fputc('\n', F);

	// bankf is filled by the FIFO.
	// bankc is available to the circuit.
	// At switch, the data is transitted from bankf to bankc.

	ftabprintf(F, 1, "-- All registers\n");
	ftabprintf(F, 1, "type onebank_t is array (0 to cells_nb-1) of ");
	Netlist_GenVHDL_StdLogicType_be(F, width_data, NULL, ";\n");
	ftabprintf(F, 1, "signal bankf, bankc : onebank_t;");
	fputc('\n', F);

	// Signals containing fill level of banks

	fputc('\n', F);
	ftabprintf(F, 1, "signal bankf_fill : unsigned(%u downto 0) := (others => '0');\n", width_fill-1);
	if(fill_circuit_en==true) {
		ftabprintf(F, 1, "signal bankc_fill : unsigned(%u downto 0) := (others => '0');\n", width_fill-1);
	}

	fprintf(F,
		"\n"
		"begin\n"
		"\n"
		"\t-- Sequential process\n"
		"\n"
	);

	ftabprintf(F, 1, "process (%s)\n", pp_data->port_clk->name);
	ftabprintf(F, 1, "begin\n");
	fputc('\n', F);
	ftabprintf(F, 2, "if %s(%s) then\n", Netlist_GenVHDL_ClockEdge_TestFunc_port(pp_data->port_clk), pp_data->port_clk->name);
	fputc('\n', F);

	ftabprintf(F, 3, "-- Circuit-side Write operations, if any\n");
	avl_ip_foreach(&pp_data->access_ports_d, scan) {
		netlist_access_t* access = scan->data;
		netlist_access_mem_direct_t* memd_data = access->data;
		if(memd_data->port_wd==NULL) continue;
		ftabprintf(F, 3, "if %s = '1' then\n", memd_data->port_we->name);
		ftabprintf(F, 4, "bankc(%u) <= %s;\n", memd_data->cell_index, memd_data->port_wd->name);
		ftabprintf(F, 3, "end if;\n");
	}
	fputc('\n', F);

	// If toggle: do general shift and check the read from FIFO
	//   Note: the general shift has precedence over circuit-side writes
	// Else: check the read from FIFO and the circuit-side writes

	ftabprintf(F, 3, "-- Change banks\n");
	ftabprintf(F, 3, "if %s = '1' then\n", pp_data->port_switch->name);
	ftabprintf(F, 4, "bankf_fill <= to_unsigned(0, %u);\n", width_fill);
	if(fill_circuit_en==true) {
		ftabprintf(F, 4, "bankc_fill <= bankf_fill;\n");
	}
	ftabprintf(F, 4, "bankc <= bankf;\n");

	ftabprintf(F, 4, "-- Read from FIFO\n");
	ftabprintf(F, 4, "if %s = '1' then\n", FifoAccess->port_ack->name);
	ftabprintf(F, 5, "bankf(0) <= %s;\n", FifoAccess->port_data->name);
	ftabprintf(F, 5, "bankf_fill <= to_unsigned(1, %u);\n", width_fill);
	ftabprintf(F, 4, "end if;\n");

	// There toggle is inactive
	ftabprintf(F, 3, "else\n");

	ftabprintf(F, 4, "-- Read from FIFO\n");
	ftabprintf(F, 4, "if %s = '1' and bankf_fill /= cells_nb then\n", FifoAccess->port_ack->name);
	ftabprintf(F, 5, "bankf(to_integer(bankf_fill)) <= %s;\n", FifoAccess->port_data->name);
	ftabprintf(F, 5, "bankf_fill <= bankf_fill + to_unsigned(1, %u);\n", width_fill);
	ftabprintf(F, 4, "end if;\n");

	ftabprintf(F, 3, "end if;\n");  // switch banks
	fputc('\n', F);

	ftabprintf(F, 3, "-- Reset\n");
	ftabprintf(F, 3, "if %s = '%c' then\n", pp_data->port_reset->name, Netlist_Access_Reset_GetActiveState(pp_data->port_reset->access));
	ftabprintf(F, 4, "bankf_fill <= to_unsigned(0, %u);\n", width_fill);
	if(fill_circuit_en==true) {
		ftabprintf(F, 4, "bankc_fill <= to_unsigned(0, %u);\n", width_fill);
	}
	ftabprintf(F, 3, "end if;\n");  // reset
	fputc('\n', F);

	ftabprintf(F, 2, "end if;\n");  // Clock event

	fprintf(F,
		"\n"
		"\tend process;\n"
		"\n"
		"\n"
		"\t-- Outputs: The FIFO\n"
		"\n"
	);

	ftabprintf(F, 1, "%s <= '1' when %s = '0' and (bankf_fill /= cells_nb or %s = '1') else '0';\n",
		FifoAccess->port_rdy->name, pp_data->port_reset->name, pp_data->port_switch->name
	);

	fprintf(F,
		"\n"
		"\t-- Outputs: Circuit-side read\n"
		"\n"
	);

	avl_ip_foreach(&pp_data->access_ports_d, scan) {
		netlist_access_t* access = scan->data;
		netlist_access_mem_direct_t* memd_data = access->data;
		if(memd_data->port_rd==NULL) continue;
		ftabprintf(F, 1, "%s <= bankc(%u);\n", memd_data->port_rd->name, memd_data->cell_index);
	}

	fputc('\n', F);
	ftabprintf(F, 1, "-- Outputs: Fullness of banks\n");
	fputc('\n', F);

	if(pp_data->port_ratio_c!=NULL) {
		ftabprintf(F, 1, "%s <= bankc_fill;\n", pp_data->port_ratio_c->name);
	}
	if(pp_data->port_full_c!=NULL) {
		ftabprintf(F, 1, "%s <= '1' when bankc_fill = cells_nb else '0';\n", pp_data->port_full_c->name);
	}
	if(pp_data->port_ratio_n!=NULL) {
		ftabprintf(F, 1, "%s <= bankf_fill;\n", pp_data->port_ratio_n->name);
	}
	if(pp_data->port_full_n!=NULL) {
		ftabprintf(F, 1, "%s <= '1' when bankf_fill = cells_nb else '0';\n", pp_data->port_full_n->name);
	}

	fprintf(F,
		"\n"
		"end architecture;\n"
		"\n"
	);

}
static void Netlist_PingPong_GenVHDL_reg_out_simple(FILE* F, netlist_vhdl_t* params, netlist_comp_t* comp) {
	netlist_pingpong_t* pp_data = comp->data;
	assert(pp_data->banks_nb==2);

	unsigned cells_nb = pp_data->cells_nb;
	unsigned banks_nb = pp_data->banks_nb;
	unsigned width_data = pp_data->data_width;
	unsigned width_fill = uint_bitsnb(cells_nb);

	netlist_access_fifo_t* FifoAccess = pp_data->access_fifo->data;

	fprintf(F,
		"\n"
		"library ieee;\n"
		"use ieee.numeric_std.all;\n"
		"\n"
	);

	Netlist_GenVHDL_Entity(F, comp, params);
	fputc('\n', F);

	fprintf(F, "architecture %s of %s is\n", params->architecture, Netlist_Comp_GetRename_Comp(comp, params));
	fputc('\n', F);

	ftabprintf(F, 1, "-- General parameters\n");
	ftabprintf(F, 1, "constant cells_nb : integer := %u;\n", cells_nb);
	ftabprintf(F, 1, "constant banks_nb : integer := %u;\n", banks_nb);
	fputc('\n', F);

	// bankf is filled by the FIFO.
	// bankc is available to the circuit.
	// At switch, the data is transitted from bankc to bankf.

	ftabprintf(F, 1, "-- All registers\n");
	ftabprintf(F, 1, "type onebank_t is array (0 to cells_nb-1) of ");
	Netlist_GenVHDL_StdLogicType_be(F, width_data, NULL, ";\n");
	ftabprintf(F, 1, "signal bankf, bankc : onebank_t;");
	fputc('\n', F);

	// Signals containing fill level of banks

	fputc('\n', F);
	ftabprintf(F, 1, "signal bankf_fill : unsigned(%u downto 0) := (others => '0');\n", width_fill-1);
	ftabprintf(F, 1, "signal bankf_idx  : unsigned(%u downto 0) := (others => '0');\n", width_fill-1);

	fprintf(F,
		"\n"
		"begin\n"
		"\n"
		"\t-- Sequential process\n"
		"\n"
	);

	ftabprintf(F, 1, "process (%s)\n", pp_data->port_clk->name);
	ftabprintf(F, 1, "begin\n");
	fputc('\n', F);
	ftabprintf(F, 2, "if %s(%s) then\n", Netlist_GenVHDL_ClockEdge_TestFunc_port(pp_data->port_clk), pp_data->port_clk->name);
	fputc('\n', F);

	ftabprintf(F, 3, "-- Write to FIFO\n");
	ftabprintf(F, 3, "if %s = '1' and bankf_fill /= 0 then\n", FifoAccess->port_ack->name);
	ftabprintf(F, 4, "bankf_fill <= bankf_fill - to_unsigned(1, %u);\n", width_fill);
	ftabprintf(F, 4, "if bankf_fill /= 1 then\n");
	ftabprintf(F, 5, "bankf_idx <= bankf_idx + to_unsigned(1, %u);\n", width_fill);
	ftabprintf(F, 4, "end if;\n");
	ftabprintf(F, 3, "end if;\n");
	fputc('\n', F);

	ftabprintf(F, 3, "-- Circuit-side Write operations, if any\n");
	avl_ip_foreach(&pp_data->access_ports_d, scan) {
		netlist_access_t* access = scan->data;
		netlist_access_mem_direct_t* memd_data = access->data;
		if(memd_data->port_wd==NULL) continue;
		// Circuit-side Write
		ftabprintf(F, 3, "if %s = '1' then\n", memd_data->port_we->name);
		ftabprintf(F, 4, "bankc(%u) <= %s;\n", memd_data->cell_index, memd_data->port_wd->name);
		ftabprintf(F, 3, "end if;\n");
	}
	fputc('\n', F);

	ftabprintf(F, 3, "-- Change banks\n");
	ftabprintf(F, 3, "if %s = '1' then\n", pp_data->port_switch->name);

	ftabprintf(F, 4, "bankf <= bankc;\n");
	if(pp_data->port_setmax!=NULL) {
		ftabprintf(F, 4, "bankf_fill <= %s;\n", pp_data->port_setmax->name);
	}
	else {
		ftabprintf(F, 4, "bankf_fill <= to_unsigned(cells_nb, %u);\n", width_fill);
	}
	ftabprintf(F, 4, "bankf_idx <= to_unsigned(0, %u);\n", width_fill);
	ftabprintf(F, 3, "end if;\n");  // switch command
	fputc('\n', F);

	ftabprintf(F, 3, "-- Reset\n");
	ftabprintf(F, 3, "if %s = '%c' then\n", pp_data->port_reset->name, Netlist_Access_Reset_GetActiveState(pp_data->port_reset->access));
	ftabprintf(F, 4, "bankf_fill <= to_unsigned(0, %u);\n", width_fill);
	ftabprintf(F, 4, "bankf_idx <= to_unsigned(0, %u);\n", width_fill);
	ftabprintf(F, 3, "end if;\n");  // reset
	fputc('\n', F);

	ftabprintf(F, 2, "end if;\n");  // Clock event

	fprintf(F,
		"\n"
		"\tend process;\n"
		"\n"
		"\n"
		"\t-- Outputs: the FIFO\n"
		"\n"
	);

	ftabprintf(F, 1, "%s <= bankf(to_integer(bankf_idx));\n", FifoAccess->port_data->name);
	ftabprintf(F, 1, "%s <= '1' when %s = '0' and bankf_fill /= 0 else '0';\n", FifoAccess->port_rdy->name, pp_data->port_reset->name);

	fprintf(F,
		"\n"
		"\t-- Outputs: Circuit-side read\n"
		"\n"
	);

	avl_ip_foreach(&pp_data->access_ports_d, scan) {
		netlist_access_t* access = scan->data;
		netlist_access_mem_direct_t* memd_data = access->data;
		if(memd_data->port_rd==NULL) continue;
		ftabprintf(F, 1, "%s <= bankc(%u);\n", memd_data->port_rd->name, memd_data->cell_index);
	}

	fputc('\n', F);
	ftabprintf(F, 1, "-- Outputs: Fullness of banks\n");
	fputc('\n', F);

	if(pp_data->port_ratio_n!=NULL) {
		ftabprintf(F, 1, "%s <= bankf_fill;\n", pp_data->port_ratio_n->name);
	}
	if(pp_data->port_full_n!=NULL) {
		ftabprintf(F, 1, "%s <= '1' when bankf_fill = 0 or (bankf_fill = 1 and %s = '1') else '0';\n",
			pp_data->port_full_n->name, FifoAccess->port_ack->name
		);
	}

	fprintf(F,
		"\n"
		"end architecture;\n"
		"\n"
	);

}

static void Netlist_PingPong_GenVHDL(FILE* F, netlist_vhdl_t* params, netlist_comp_t* comp) {
	netlist_pingpong_t* pp_data = comp->data;

	if(pp_data->is_direct==false) {
		Netlist_PingPong_GenVHDL_ram(F, params, comp);
	}
	else {

		bool is_input = false;
		if(comp->model == NETLIST_COMP_PINGPONG_IN) is_input = true;

		netlist_pingpong_t* pp_data = comp->data;
		unsigned banks_nb = pp_data->banks_nb;

		if(is_input==true) {
			if(banks_nb==2) Netlist_PingPong_GenVHDL_reg_in_simple(F, params, comp);
			else            Netlist_PingPong_GenVHDL_reg_in(F, params, comp);
		}
		else {
			if(banks_nb==2) Netlist_PingPong_GenVHDL_reg_out_simple(F, params, comp);
			else            Netlist_PingPong_GenVHDL_reg_out(F, params, comp);
		}
	}

}

netlist_comp_model_t* Netlist_Comp_PingPong_In_GetModel() {
	netlist_comp_model_t* model = Netlist_CompModel_New();
	model->name = namealloc("pingpong_in");

	model->data_size         = sizeof(netlist_pingpong_t);
	model->func_free_data    = Netlist_PingPong_Free_Data;
	model->func_dup_internal = Netlist_PingPong_Dup;
	model->func_gen_vhdl     = Netlist_PingPong_GenVHDL;

	model->func_simp         = (netlist_comp_cb_simp_t)Netlist_Simp_Comp_PingPong;

	NETLIST_COMP_PINGPONG_IN = model;
	return model;
}
netlist_comp_model_t* Netlist_Comp_PingPong_Out_GetModel() {
	netlist_comp_model_t* model = Netlist_CompModel_New();
	model->name = namealloc("pingpong_out");

	model->data_size         = sizeof(netlist_pingpong_t);
	model->func_free_data    = Netlist_PingPong_Free_Data;
	model->func_dup_internal = Netlist_PingPong_Dup;
	model->func_gen_vhdl     = Netlist_PingPong_GenVHDL;

	model->func_simp         = (netlist_comp_cb_simp_t)Netlist_Simp_Comp_PingPong;

	NETLIST_COMP_PINGPONG_OUT = model;
	return model;
}



//======================================================================
// UART interface
//======================================================================

// The access

netlist_access_model_t* NETLIST_ACCESS_UART = NULL;

netlist_access_t* Netlist_Comp_AddAccess_Uart(netlist_comp_t* comp, char* access_name) {
	netlist_access_t* access = Netlist_Comp_AddAccess(comp, NETLIST_ACCESS_UART, access_name);
	netlist_uart_access_t* uart_data = access->data;

	char buffer[200];
	char* port_name;

	sprintf(buffer, "%s_tx", access_name);
	port_name = namealloc(buffer);
	uart_data->port_tx = Netlist_Comp_AddPort_Out(comp, port_name, 1);
	uart_data->port_tx->access = access;

	sprintf(buffer, "%s_rx", access_name);
	port_name = namealloc(buffer);
	uart_data->port_rx = Netlist_Comp_AddPort_In(comp, port_name, 1);
	uart_data->port_rx->access = access;

	uart_data->baudrate = 0;

	return access;
}

netlist_access_model_t* Netlist_Access_Uart_GetModel() {
	netlist_access_model_t* model = Netlist_AccessModel_New();
	model->name = namealloc("uart");

	model->data_size = sizeof(netlist_uart_access_t);

	char* port_name = NULL;
	port_name = namealloc("rx");
	Netlist_AccessModel_Init_PortName(model, netlist_uart_access_t, port_rx, port_name);
	port_name = namealloc("tx");
	Netlist_AccessModel_Init_PortName(model, netlist_uart_access_t, port_tx, port_name);

	NETLIST_ACCESS_UART = model;
	return model;
}

// The components

netlist_comp_model_t* NETLIST_COMP_UART = NULL;

netlist_comp_t* Netlist_Uart_New(char* name, unsigned data_width, bool parity, unsigned divider) {
	netlist_comp_t* comp = Netlist_Comp_New(NETLIST_COMP_UART);
	comp->name = name;

	netlist_uart_t* uart_data = comp->data;

	// The configuration
	uart_data->data_width = data_width;
	uart_data->parity = parity;
	uart_data->clk_divider = divider;

	char* port_name;
	char buffer[200];

	// The system ports
	port_name = namealloc("clk");
	uart_data->port_clk = Netlist_Comp_AddPortAccess_Clock(comp, port_name);

	port_name = namealloc("reset");
	uart_data->port_reset = Netlist_Comp_AddPortAccess_Reset(comp, port_name);

	// The UART access
	sprintf(buffer, "%s_uart", name);
	uart_data->access_uart = Netlist_Comp_AddAccess_Uart(comp, namealloc(buffer));

	// The Data accesses
	sprintf(buffer, "%s_fifo_in", name);
	uart_data->access_data_in = Netlist_Comp_AddAccess_FifoIn(comp, namealloc(buffer), data_width, false, NULL);

	sprintf(buffer, "%s_fifo_out", name);
	uart_data->access_data_out = Netlist_Comp_AddAccess_FifoOut(comp, namealloc(buffer), data_width, false, NULL);

	return comp;
}

static void Netlist_Uart_DataDup(avl_pp_tree_t* tree_old2new, netlist_comp_t* comp, netlist_comp_t* newcomp) {
	netlist_uart_t* uart_data = comp->data;
	netlist_uart_t* newuart_data = newcomp->data;

	newuart_data->data_width  = uart_data->data_width;
	newuart_data->parity      = uart_data->parity;
	newuart_data->clk_divider = uart_data->clk_divider;

	void* pointer_old2new(void* oldptr) {
		void* newptr = NULL;
		avl_pp_find_data(tree_old2new, oldptr, &newptr);
		return newptr;
	}

	newuart_data->port_clk   = pointer_old2new(uart_data->port_clk);
	newuart_data->port_reset = pointer_old2new(uart_data->port_reset);

	newuart_data->access_uart    = pointer_old2new(uart_data->access_uart);
	newuart_data->access_data_in = pointer_old2new(uart_data->access_data_in);
	newuart_data->access_data_out = pointer_old2new(uart_data->access_data_out);
}

static void Netlist_Uart_GenVHDL(FILE* F, netlist_vhdl_t* params, netlist_comp_t* comp) {
	netlist_uart_t* uart_data = comp->data;

	netlist_uart_access_t* uartacc_data = uart_data->access_uart->data;
	netlist_access_fifo_t* fifoin_data = uart_data->access_data_in->data;
	netlist_access_fifo_t* fifoout_data = uart_data->access_data_out->data;

	// By default, data is transmitted/received as little-endian
	bool endianness_le = true;

	fprintf(F,
		"\n"
		"library ieee;\n"
		"use ieee.numeric_std.all;\n"
		"\n"
	);

	Netlist_GenVHDL_Entity(F, comp, params);

	fprintfm(F,
		"\n"
		"architecture %s of %s is\n", params->architecture, Netlist_Comp_GetRename_Comp(comp, params),
		"\n"
		"	-- Note: Data is transmitted/received as %s-endian\n", endianness_le==true ? "little" : "big",
		"\n"
		"	-- Some constants to ease VHDL generation\n"
		"\n"
		"	constant data_bits : natural := %u;\n", uart_data->data_width,
		"	constant clock_divider : natural := %u;\n", uart_data->clk_divider,
		"	-- Bit width of a counter that counts from 0 to clock_divider-1\n",
		"	constant clockdiv_width : natural := %u;\n", uint_bitsnb(uart_data->clk_divider-1),
		"	-- Bit width of a counter that counts from 0 to data_bits-1\n",
		"	constant bitcount_width : natural := %u;\n", uint_bitsnb(uart_data->data_width-1),
		"\n"
		"	-- Types for little FSM\n"
		"	type status_type is (fsm_start, fsm_bits",
		NULL
	);

	if(uart_data->parity==true) fprintf(F, ", fsm_parity");

	fprintf(F, ", fsm_stop);\n"
		"\n"
		"	-- Note: Init state is STOP to ensure at least one STOP symbol is generated\n"
		"	signal tx_status          : status_type := fsm_stop;\n"
		"	signal tx_status_next     : status_type := fsm_stop;\n"
		"	signal tx_bits            : std_logic_vector(data_bits-1 downto 0);\n"
		"	signal tx_bits_next       : std_logic_vector(data_bits-1 downto 0);\n"
		"	signal tx_bits_idx        : unsigned(bitcount_width-1 downto 0) := (others => '0');\n"
		"	signal tx_bits_idx_next   : unsigned(bitcount_width-1 downto 0) := (others => '0');\n"
		"	signal tx_bits_avail      : std_logic := '0';\n"
		"	signal tx_bits_avail_next : std_logic := '0';\n"
		"	signal tx_clkdiv_cnt      : unsigned(clockdiv_width-1 downto 0) := (others => '0');\n"
		"	signal tx_clkdiv_cnt_next : unsigned(clockdiv_width-1 downto 0) := (others => '0');\n"
	);

	if(uart_data->parity==true) {
		fprintf(F,
			"	signal tx_parity          : std_logic := '0';\n"
			"	signal tx_parity_next     : std_logic := '0';\n"
		);
	}

	fprintfm(F,
		"\n"
		"	signal rx_status          : status_type := fsm_start;\n"
		"	signal rx_status_next     : status_type := fsm_start;\n"
		"	signal rx_bits            : std_logic_vector(data_bits-1 downto 0);\n"
		"	signal rx_bits_next       : std_logic_vector(data_bits-1 downto 0);\n"
		"	signal rx_bits_idx        : unsigned(bitcount_width-1 downto 0) := (others => '0');\n"
		"	signal rx_bits_idx_next   : unsigned(bitcount_width-1 downto 0) := (others => '0');\n"
		"	signal rx_bits_avail      : std_logic := '0';\n"
		"	signal rx_bits_avail_next : std_logic := '0';\n"
		"	signal rx_clkdiv_cnt      : unsigned(clockdiv_width-1 downto 0) := (others => '0');\n"
		"	signal rx_clkdiv_cnt_next : unsigned(clockdiv_width-1 downto 0) := (others => '0');\n"
		"\n"
		"begin\n"
		"\n"
		"	-- TX side\n"
		"\n"
		"	-- Sequential process\n"
		"	process (%s, %s)\n", uart_data->port_clk->name, uart_data->port_reset->name,
		"	begin\n"
		"		if %s = '%c' then\n", uart_data->port_reset->name, Netlist_Access_Reset_GetActiveState(uart_data->port_reset->access),
		"			tx_status        <= fsm_start;\n"
		"			tx_bits_idx      <= (others => '0');\n"
		"			tx_bits_avail    <= '0';\n"
		"			tx_clkdiv_cnt    <= (others => '0');\n",
		NULL
	);

	if(uart_data->parity==true) fprintf(F, "			tx_parity        <= '0';\n");

	fprintfm(F,
		"		elsif %s(%s) then\n", Netlist_GenVHDL_ClockEdge_TestFunc_port(uart_data->port_clk), uart_data->port_clk->name,
		"			tx_status        <= tx_status_next;\n"
		"			tx_bits_idx      <= tx_bits_idx_next;\n"
		"			tx_bits_avail    <= tx_bits_avail_next;\n"
		"			tx_clkdiv_cnt    <= tx_clkdiv_cnt_next;\n",
		NULL
	);

	if(uart_data->parity==true) fprintf(F, "			tx_parity        <= tx_parity;\n");

	fprintfm(F,
		"		end if;\n"
		"		-- Note: tx_bits is not affected by reset\n"
		"		if %s(%s) then\n", Netlist_GenVHDL_ClockEdge_TestFunc_port(uart_data->port_clk), uart_data->port_clk->name,
		"			tx_bits <= tx_bits_next;\n"
		"		end if;\n"
		"	end process;\n"
		"\n"
		"	-- Combinatorial process\n"
		"	process (tx_status, tx_bits, tx_bits_idx, tx_bits_avail, tx_clkdiv_cnt",
		NULL
	);

	if(uart_data->parity==true) fprintf(F, ", tx_parity");

	fprintfm(F, ", %s, %s)\n", fifoin_data->port_data->name, fifoin_data->port_ack->name,
		"	begin\n"
		"\n"
		"		-- Default values for ports\n"
		"		%s <= '1';\n", uartacc_data->port_tx->name,
		"		%s <= '0';\n", fifoin_data->port_rdy->name,
		"		-- Default values for internal signals\n"
		"		tx_status_next     <= tx_status;\n"
		"		tx_bits_next       <= tx_bits;\n"
		"		tx_bits_idx_next   <= tx_bits_idx;\n"
		"		tx_bits_avail_next <= tx_bits_avail;\n",
		NULL
	);

	if(uart_data->parity==true) fprintf(F, "		tx_parity_next     <= tx_parity;\n");

	fprintfm(F,
		"\n"
		"		-- Clock divider\n"
		"		if tx_clkdiv_cnt = clock_divider-1 then\n"
		"			tx_clkdiv_cnt_next <= (others => '0');\n"
		"		else\n"
		"			tx_clkdiv_cnt_next <= tx_clkdiv_cnt + 1;\n"
		"		end if;\n"
		"\n"
		"		-- Compute next state and generate TX\n"
		"		if tx_status = fsm_start then\n"
		"			-- Send the start bit\n"
		"			%s <= '0';\n", uartacc_data->port_tx->name,
		"			tx_bits_idx_next <= (others => '0');\n",
		NULL
	);

	if(uart_data->parity==true) fprintf(F, "			tx_parity_next <= '0';\n");

	fprintfm(F,
		"			if tx_clkdiv_cnt = clock_divider-1 then\n"
		"				tx_status_next <= fsm_bits;\n"
		"			end if;\n"
		"		elsif tx_status = fsm_bits then\n"
		"			-- Send the data bits\n"
		"			%s <= tx_bits(%s);\n", uartacc_data->port_tx->name, endianness_le==true ? "0" : "data_bits-1",
		"			if tx_clkdiv_cnt = clock_divider-1 then\n",
		NULL
	);

	if(endianness_le==true) {
		fprintf(F, "				tx_bits_next <= '0' & tx_bits(data_bits-1 downto 1);\n");
	}
	else {
		fprintf(F, "				tx_bits_next <= tx_bits(data_bits-2 downto 0) & '0';\n");
	}

	if(uart_data->parity==true) fprintf(F, "				tx_parity_next <= tx_parity xor tx_bits(%s);\n", endianness_le==true ? "0" : "data_bits-1");

	fprintfm(F,
		"				if tx_bits_idx = data_bits-1 then\n"
		"					tx_bits_avail_next <= '0';\n"
		"					tx_status_next <= %s;\n", uart_data->parity==true ? "fsm_parity" : "fsm_stop",
		"				else\n"
		"					tx_bits_idx_next <= tx_bits_idx + 1;\n"
		"				end if;\n"
		"			end if;\n",
		NULL
	);

	if(uart_data->parity==true) {
		fprintfm(F,
			"		elsif tx_status = fsm_parity then\n"
			"			-- Send the parity bit\n"
			"			%s <= tx_parity;\n", uartacc_data->port_tx->name,
			"			if tx_clkdiv_cnt = clock_divider-1 then\n"
			"				tx_status_next <= fsm_stop;\n"
			"			end if;\n",
			NULL
		);
	}

	fprintfm(F,
		"		elsif tx_status = fsm_stop then\n"
		"			-- Send the stop bit\n"
		"			if tx_clkdiv_cnt = clock_divider-1 then\n"
		"				if tx_bits_avail = '0' then\n"
		"					-- Stay here until some data is available to send\n"
		"					tx_clkdiv_cnt_next <= tx_clkdiv_cnt;\n"
		"				else\n"
		"					tx_status_next <= fsm_start;\n"
		"				end if;\n"
		"			end if;\n"
		"		end if;\n"
		"\n"
		"		-- Handle data input\n"
		"		if tx_status /= fsm_bits and tx_bits_avail = '0' then\n"
		"			%s <= '1';\n", fifoin_data->port_rdy->name,
		"			if %s = '1' then\n", fifoin_data->port_ack->name,
		"				tx_bits_avail_next <= '1';\n",
		"				tx_bits_next <= %s;\n", fifoin_data->port_data->name,
		"			end if;\n"
		"		end if;\n"
		"\n"
		"	end process;\n"
		"\n"
		"	-- RX side\n"
		"\n"
		"	-- Sequential process\n"
		"	process (%s, %s)\n", uart_data->port_clk->name, uart_data->port_reset->name,
		"	begin\n"
		"		if %s = '%c' then\n", uart_data->port_reset->name, Netlist_Access_Reset_GetActiveState(uart_data->port_reset->access),
		"			rx_status        <= fsm_start;\n"
		"			rx_bits_idx      <= (others => '0');\n"
		"			rx_bits_avail    <= '0';\n"
		"			rx_clkdiv_cnt    <= (others => '0');\n"
		"		elsif %s(%s) then\n", Netlist_GenVHDL_ClockEdge_TestFunc_port(uart_data->port_clk), uart_data->port_clk->name,
		"			rx_status        <= rx_status_next;\n"
		"			rx_bits_idx      <= rx_bits_idx_next;\n"
		"			rx_bits_avail    <= rx_bits_avail_next;\n"
		"			rx_clkdiv_cnt    <= rx_clkdiv_cnt_next;\n"
		"		end if;\n"
		"		-- Note: rx_bits is not affected by reset\n"
		"		if %s(%s) then\n", Netlist_GenVHDL_ClockEdge_TestFunc_port(uart_data->port_clk), uart_data->port_clk->name,
		"			rx_bits <= rx_bits_next;\n"
		"		end if;\n"
		"	end process;\n"
		"\n"
		"	-- Combinatorial process\n"
		"	process (rx_status, rx_bits, rx_bits_idx, rx_bits_avail, rx_clkdiv_cnt, %s, %s)\n",
			fifoout_data->port_ack->name, uartacc_data->port_rx->name,
		"	begin\n"
		"		-- Default values for ports\n"
		"		%s <= '0';\n", fifoout_data->port_rdy->name,
		"		-- Default values for internal signals\n"
		"		rx_status_next     <= rx_status;\n"
		"		rx_bits_next       <= rx_bits;\n"
		"		rx_bits_idx_next   <= rx_bits_idx;\n"
		"		rx_bits_avail_next <= rx_bits_avail;\n"
		"\n"
		"		-- Clock divider\n"
		"		if rx_clkdiv_cnt = clock_divider-1 then\n"
		"			rx_clkdiv_cnt_next <= (others => '0');\n"
		"		else\n"
		"			rx_clkdiv_cnt_next <= rx_clkdiv_cnt + 1;\n"
		"		end if;\n"
		"\n"
		"		-- Handle data output\n"
		"		%s <= rx_bits;\n", fifoout_data->port_data->name,
		"		if rx_bits_avail = '1' then\n"
		"			%s <= '1';\n", fifoout_data->port_rdy->name,
		"			if %s = '1' then\n", fifoout_data->port_ack->name,
		"				rx_bits_avail_next <= '0';\n"
		"			end if;\n"
		"		end if;\n"
		"\n"
		"		-- Compute next state and read RX\n"
		"		if rx_status = fsm_start then\n"
		"			rx_bits_idx_next <= (others => '0');\n"
		"			if %s = '1' and rx_clkdiv_cnt < clock_divider/2 then\n", uartacc_data->port_rx->name,
		"				-- We are in a stop bit\n"
		"				rx_clkdiv_cnt_next <= (others => '0');\n"
		"			else\n"
		"				-- We are in the start bit\n"
		"				if rx_clkdiv_cnt = clock_divider-1 then\n"
		"					rx_status_next <= fsm_bits;\n"
		"				end if;\n"
		"			end if;\n"
		"		elsif rx_status = fsm_bits then\n"
		"			-- Receive the data bits\n"
		"			-- Drop the previously received data\n"
		"			rx_bits_avail_next <= '0';\n"
		"			-- Read the data bit at the middle of the bit duration\n"
		"			if rx_clkdiv_cnt = clock_divider/2 then\n",
		NULL
	);

	if(endianness_le==true) {
		fprintf(F, "				rx_bits_next <= %s & rx_bits(data_bits-1 downto 1);\n", uartacc_data->port_rx->name);
	}
	else {
		fprintf(F, "				rx_bits_next <= rx_bits(data_bits-2 downto 0) & %s;\n", uartacc_data->port_rx->name);
	}

	fprintfm(F,
		"			end if;\n"
		"			-- Increment the bit counter and change state\n"
		"			if rx_clkdiv_cnt = clock_divider-1 then\n"
		"				if rx_bits_idx = data_bits-1 then\n"
		"					rx_bits_avail_next <= '1';\n"
		"					rx_status_next <= %s;\n", uart_data->parity==true ? "fsm_parity" : "fsm_stop",
		"				else\n"
		"					rx_bits_idx_next <= rx_bits_idx + 1;\n"
		"				end if;\n"
		"			end if;\n",
		NULL
	);

	if(uart_data->parity==true) {
		fprintf(F,
			"		elsif rx_status = fsm_parity then\n"
			"			-- Read the parity bit... FIXME it is discarded\n"
			"			if rx_clkdiv_cnt = clock_divider-1 then\n"
			"				rx_status_next <= fsm_stop;\n"
			"			end if;\n"
		);
	}

	fprintf(F,
		"		elsif rx_status = fsm_stop then\n"
		"			-- Read the stop bit (discarded) for just half a symbol to let some slack\n"
		"			if rx_clkdiv_cnt = clock_divider/2 then\n"
		"				-- Go waiting for next falling edge\n"
		"				rx_clkdiv_cnt_next <= (others => '0');\n"
		"				rx_status_next <= fsm_start;\n"
		"			end if;\n"
		"		end if;\n"
		"\n"
		"	end process;\n"
		"\n"
		"end architecture;\n"
		"\n"
	);

}

netlist_comp_model_t* Netlist_Comp_Uart_GetModel() {
	netlist_comp_model_t* model = Netlist_CompModel_New();
	model->name = namealloc("uart");

	model->data_size         = sizeof(netlist_uart_t);
	model->func_dup_internal = Netlist_Uart_DataDup;
	model->func_gen_vhdl     = Netlist_Uart_GenVHDL;

	NETLIST_COMP_UART = model;
	return model;
}



//======================================================================
// UART interface: Standalone one-side, Rx or Tx
//======================================================================

// Config

void Netlist_Uart_Side_Cfg_Init(netlist_uart_side_cfg_t* cfg) {
	// Main config
	cfg->databits_nb = 8;
	cfg->baudrate    = 115200;
	cfg->clk_divider = 0;
	cfg->parity      = false;
	// By default, data is transmitted/received as little-endian
	cfg->uart_le     = true;

	// FIXME Not used yet: Extension for control flow
	cfg->control_en  = false;
	cfg->control_active_level = '0';

	// Internal buffer size. Zero means none specified (and default to 1).
	// For RX, width is the UART number of bits. For TX, width is fifo width.
	cfg->buffer_size = 0;

	// When the channel is used to transmit values larger than the uart bits_nb
	// These fields can also be used to send data shorter than the fifo width
	cfg->elts_nb   = 0;     // Default: the min needed to cover the FIFO width
	cfg->elts_le   = true;
	cfg->elts_pad  = '0';
}

// Access models

netlist_access_model_t* NETLIST_ACCESS_UART_RX = NULL;
netlist_access_model_t* NETLIST_ACCESS_UART_TX = NULL;

netlist_access_model_t* Netlist_Access_Uart_Rx_GetModel() {
	netlist_access_model_t* model = Netlist_AccessModel_New();
	model->name = namealloc("uart_rx");

	model->data_size = sizeof(netlist_access_uart_side_t);

	char* port_name = NULL;
	port_name = namealloc("uart");
	Netlist_AccessModel_Init_PortName(model, netlist_access_uart_side_t, port_uart, port_name);
	port_name = namealloc("cts");
	Netlist_AccessModel_Init_PortName(model, netlist_access_uart_side_t, port_cts, port_name);
	port_name = namealloc("rts");
	Netlist_AccessModel_Init_PortName(model, netlist_access_uart_side_t, port_rts, port_name);

	NETLIST_ACCESS_UART_RX = model;
	return model;
}
netlist_access_model_t* Netlist_Access_Uart_Tx_GetModel() {
	netlist_access_model_t* model = Netlist_AccessModel_New();
	model->name = namealloc("uart_tx");

	model->data_size = sizeof(netlist_access_uart_side_t);

	char* port_name = NULL;
	port_name = namealloc("uart");
	Netlist_AccessModel_Init_PortName(model, netlist_access_uart_side_t, port_uart, port_name);
	port_name = namealloc("cts");
	Netlist_AccessModel_Init_PortName(model, netlist_access_uart_side_t, port_cts, port_name);
	port_name = namealloc("rts");
	Netlist_AccessModel_Init_PortName(model, netlist_access_uart_side_t, port_rts, port_name);

	NETLIST_ACCESS_UART_TX = model;
	return model;
}

// Adding access to components

netlist_access_t* Netlist_Comp_AddAccess_Uart_Rx(netlist_comp_t* comp, char* access_name) {
	netlist_access_t* access = Netlist_Comp_AddAccess(comp, NETLIST_ACCESS_UART_RX, access_name);
	netlist_access_uart_side_t* uart_data = access->data;

	// Init config

	Netlist_Uart_Side_Cfg_Init(&uart_data->cfg);

	// Create the port

	char buffer[200];
	char* port_name;

	sprintf(buffer, "%s_rx", access_name);
	port_name = namealloc(buffer);
	uart_data->port_uart = Netlist_Comp_AddPort_In(comp, port_name, 1);
	uart_data->port_uart->access = access;

	return access;
}
netlist_access_t* Netlist_Comp_AddAccess_Uart_Tx(netlist_comp_t* comp, char* access_name) {
	netlist_access_t* access = Netlist_Comp_AddAccess(comp, NETLIST_ACCESS_UART_TX, access_name);
	netlist_access_uart_side_t* uart_data = access->data;

	// Init config

	Netlist_Uart_Side_Cfg_Init(&uart_data->cfg);

	// Create the port

	char buffer[200];
	char* port_name;

	sprintf(buffer, "%s_tx", access_name);
	port_name = namealloc(buffer);
	uart_data->port_uart = Netlist_Comp_AddPort_Out(comp, port_name, 1);
	uart_data->port_uart->access = access;

	return access;
}

// Component models

static void Netlist_UartSide_DataDup(avl_pp_tree_t* tree_old2new, netlist_comp_t* comp, netlist_comp_t* newcomp) {
	netlist_uart_side_t* uart_data = comp->data;
	netlist_uart_side_t* newuart_data = newcomp->data;

	// The ports and accesses

	void* pointer_old2new(void* oldptr) {
		void* newptr = NULL;
		avl_pp_find_data(tree_old2new, oldptr, &newptr);
		return newptr;
	}

	newuart_data->port_clk   = pointer_old2new(uart_data->port_clk);
	newuart_data->port_reset = pointer_old2new(uart_data->port_reset);

	newuart_data->access_uart = pointer_old2new(uart_data->access_uart);
	newuart_data->access_fifo = pointer_old2new(uart_data->access_fifo);

	// Link to the config structure

	netlist_access_uart_side_t* newuartacc_data = newuart_data->access_uart->data;
	newuart_data->cfg = &newuartacc_data->cfg;
}

// VHDL generation: FIXME TODO

static void Netlist_UartSide_Rx_GenVHDL(FILE* F, netlist_vhdl_t* params, netlist_comp_t* comp) {
	netlist_uart_side_t* uart_data = comp->data;

	netlist_uart_side_cfg_t* cfg = uart_data->cfg;
	netlist_access_uart_side_t* uartacc_data = uart_data->access_uart->data;
	netlist_access_fifo_t* fifo_data = uart_data->access_fifo->data;

	unsigned elts_nb = cfg->elts_nb;
	if(cfg->elts_nb == 0) {
		elts_nb = fifo_data->width / cfg->databits_nb;
		if(elts_nb * cfg->databits_nb < fifo_data->width) elts_nb++;
	}

	unsigned buffer_size = GetMax(1, cfg->buffer_size);

	unsigned baudrate = cfg->baudrate;
	unsigned divider  = cfg->clk_divider;
	double freq = Netlist_Access_Clock_GetFrequency(uart_data->port_clk->access);
	if(freq>0) {
		if(cfg->baudrate != 0) divider = freq / cfg->baudrate;
		else if(cfg->clk_divider != 0) baudrate = freq / cfg->clk_divider;
	}

	// Checks
	unsigned errors_nb = 0;
	unsigned frame_length = cfg->databits_nb + (cfg->parity==true ? 1 : 0) + 1;
	if(divider < 2*frame_length) {
		printf("Error: comp '%s' model '%s': Clock divider too low: %u\n", comp->name, comp->model->name, divider);
		errors_nb ++;
	}
	if(divider==0) {
		printf("Error: comp '%s' model '%s': No way to compute the clock divider value.\n", comp->name, comp->model->name);
		errors_nb ++;
	}
	if(buffer_size!=1) {
		printf("Error: comp '%s' model '%s': Buffer size of more than 1 cell is not handled.\n", comp->name, comp->model->name);
		errors_nb ++;
	}
	if(cfg->parity==true) {
		printf("Warning: comp '%s' model '%s': Parity check is not handled.\n", comp->name, comp->model->name);
		errors_nb ++;
	}
	if(errors_nb>0) {
		exit(EXIT_FAILURE);
	}

	fprintf(F,
		"\n"
		"library ieee;\n"
		"use ieee.numeric_std.all;\n"
		"\n"
	);

	Netlist_GenVHDL_Entity(F, comp, params);

	fprintf(F,
		"\n"
		"architecture %s of %s is\n", params->architecture, Netlist_Comp_GetRename_Comp(comp, params)
	);
	fprintf(F, "\n");

	if(freq>0) {
		fprintf(F, "	-- Note: Target clock frequency is %g Hz\n", freq);
	}
	else {
		fprintf(F, "	-- Note: Target clock frequency was not specified\n");
	}
	fprintf(F, "	-- Note: Baudrate is %u%s\n", baudrate, cfg->baudrate != 0 ? "" : " (computed from clock divider)");

	fprintf(F, "	-- Note: Data bits are received as %s endian\n", cfg->uart_le==true ? "little" : "big");
	if(elts_nb>1) {
		fprintf(F, "	-- Note: Data elements are received as %s endian\n", cfg->elts_le==true ? "little" : "big");
	}

	fprintfm(F,
		"\n"
		"	-- Some constants to ease VHDL generation\n"
		"\n"
		"	constant data_bits      : natural := %u;\n", cfg->databits_nb,
		"	constant frames_nb      : natural := %u;\n", elts_nb,
		"	constant clock_divider  : natural := %u;\n", divider,
		"	-- Bit width of a counter that counts from 0 to clock_divider-1\n",
		"	constant clockdiv_width : natural := %u;\n", uint_bitsnb(divider-1),
		"	-- Bit width of a counter that counts from 0 to data_bits-1\n",
		"	constant bitcount_width : natural := %u;\n", uint_bitsnb(cfg->databits_nb-1),
		"	-- Bit width of a counter that counts from 0 to frames_nb\n",
		"	constant framecnt_width : natural := %u;\n", uint_bitsnb(elts_nb),
		"\n"
		"	-- Types for little FSM\n"
		"	type status_type is (fsm_start, fsm_bits",
		NULL
	);

	if(cfg->parity==true) fprintf(F, ", fsm_parity");

	fprintf(F, ", fsm_stop);\n");

	fprintf(F,
		"\n"
		"	signal rx_status          : status_type := fsm_start;\n"
		"	signal rx_status_next     : status_type := fsm_start;\n"
		"	signal rx_bits_idx        : unsigned(bitcount_width-1 downto 0) := (others => '0');\n"
		"	signal rx_bits_idx_next   : unsigned(bitcount_width-1 downto 0) := (others => '0');\n"
		"	signal rx_clkdiv_cnt      : unsigned(clockdiv_width-1 downto 0) := (others => '0');\n"
		"	signal rx_clkdiv_cnt_next : unsigned(clockdiv_width-1 downto 0) := (others => '0');\n"
		"	-- An intermediate signal to cover cases where the fifo width is not a multiple of data_bits\n"
		"	signal rx_fifo_tmp_data   : std_logic_vector(frames_nb*data_bits-1 downto 0);\n"
		"	-- Frames of data bits. When there are several frames to receive, the number 0 is the one being filled from Rx.\n"
	);
	for(unsigned i=0; i<elts_nb; i++) {
		fprintfm(F,
			"	signal rx_bits%u           : std_logic_vector(data_bits-1 downto 0);\n", i,
			"	signal rx_bits%u_next      : std_logic_vector(data_bits-1 downto 0);\n", i,
			NULL
		);
	}
	fprintf(F,
		"	-- Number of frames received. Reset to 0 on FIFO transfer.\n"
		"	-- It is incremented each time a frame is received.\n"
		"	signal rx_frames          : unsigned(framecnt_width-1 downto 0) := (others => '0');\n"
		"	signal rx_frames_next     : unsigned(framecnt_width-1 downto 0) := (others => '0');\n"
	);

	fprintfm(F,
		"\n"
		"begin\n"
		"\n"
		"	-- Sequential process\n"
		"	process (%s, %s)\n", uart_data->port_clk->name, uart_data->port_reset->name,
		"	begin\n"
		"		if %s = '%c' then\n", uart_data->port_reset->name, Netlist_Access_Reset_GetActiveState(uart_data->port_reset->access),
		"			rx_status        <= fsm_start;\n"
		"			rx_bits_idx      <= (others => '0');\n"
		"			rx_frames        <= (others => '0');\n"
		"			rx_clkdiv_cnt    <= (others => '0');\n"
		"		elsif %s(%s) then\n", Netlist_GenVHDL_ClockEdge_TestFunc_port(uart_data->port_clk), uart_data->port_clk->name,
		"			rx_status        <= rx_status_next;\n"
		"			rx_bits_idx      <= rx_bits_idx_next;\n"
		"			rx_frames        <= rx_frames_next;\n"
		"			rx_clkdiv_cnt    <= rx_clkdiv_cnt_next;\n"
		"		end if;\n"
		"		-- Note: the data frames are not affected by reset\n"
		"		if %s(%s) then\n", Netlist_GenVHDL_ClockEdge_TestFunc_port(uart_data->port_clk), uart_data->port_clk->name,
		NULL
	);
	for(unsigned i=0; i<elts_nb; i++) {
		fprintf(F, "			rx_bits%u <= rx_bits%u_next;\n", i, i);
	}
	fprintf(F,
		"		end if;\n"
		"	end process;\n"
		"\n"
		"	-- Combinatorial process\n"
		"	process (rx_status, rx_bits_idx, rx_frames, rx_clkdiv_cnt"
	);
	for(unsigned i=0; i<elts_nb; i++) fprintf(F, ", rx_bits%u", i);
	fprintfm(F, ", %s, %s)\n", fifo_data->port_ack->name, uartacc_data->port_uart->name,
		"	begin\n"
		"		-- Default values for ports\n"
		"		%s <= '0';\n", fifo_data->port_rdy->name,
		"		-- Default values for internal signals\n"
		"		rx_status_next     <= rx_status;\n"
		"		rx_bits_idx_next   <= rx_bits_idx;\n"
		"		rx_frames_next     <= rx_frames;\n",
		NULL
	);
	for(unsigned i=0; i<elts_nb; i++) {
		fprintf(F, "		rx_bits%u_next      <= rx_bits%u;\n", i, i);
	}

	fprintfm(F,
		"\n"
		"		-- Clock divider\n"
		"		if rx_clkdiv_cnt = clock_divider-1 then\n"
		"			rx_clkdiv_cnt_next <= (others => '0');\n"
		"		else\n"
		"			rx_clkdiv_cnt_next <= rx_clkdiv_cnt + 1;\n"
		"		end if;\n"
		"\n"
		"		-- Handle data output\n"
		"		if rx_frames = frames_nb then\n"
		"			%s <= '1';\n", fifo_data->port_rdy->name,
		"			if %s = '1' then\n", fifo_data->port_ack->name,
		"				rx_frames_next <= (others => '0');\n"
		"			end if;\n"
		"		end if;\n"
		"\n"
		"		-- Compute next state and read Rx\n"
		"		if rx_status = fsm_start then\n"
		"			rx_bits_idx_next <= (others => '0');\n"
		"			if %s = '1' and rx_clkdiv_cnt < clock_divider/2 then\n", uartacc_data->port_uart->name,
		"				-- We are in a stop bit\n"
		"				rx_clkdiv_cnt_next <= (others => '0');\n"
		"			else\n"
		"				-- We are in the start bit\n"
		"				if rx_clkdiv_cnt = clock_divider-1 then\n"
		"					rx_status_next <= fsm_bits;\n"
		"					-- Drop the previously received data... FIXME Overrun is not handled\n"
		"					if rx_frames = frames_nb then\n"
		"						rx_frames_next <= (others => '0');\n"
		"					end if;\n",
		NULL
	);
	if(elts_nb>1) {
		fprintf(F, "					-- Shift the received data frames\n");
		for(unsigned i=0; i<elts_nb-1; i++) {
			fprintf(F, "					rx_bits%u_next <= rx_bits%u;\n", i+1, i);
		}
	}
	fprintf(F,
		"				end if;\n"
		"			end if;\n"
		"		elsif rx_status = fsm_bits then\n"
		"			-- Receive the data bits\n"
		"			-- Read the data bit at the middle of the bit duration\n"
		"			if rx_clkdiv_cnt = clock_divider/2 then\n"
	);

	if(cfg->uart_le==true) {
		fprintf(F, "				rx_bits0_next <= %s & rx_bits0(data_bits-1 downto 1);\n", uartacc_data->port_uart->name);
	}
	else {
		fprintf(F, "				rx_bits0_next <= rx_bits0(data_bits-2 downto 0) & %s;\n", uartacc_data->port_uart->name);
	}

	fprintfm(F,
		"			end if;\n"
		"			-- Increment the bit counter and change state\n"
		"			if rx_clkdiv_cnt = clock_divider-1 then\n"
		"				if rx_bits_idx = data_bits-1 then\n"
		"					rx_frames_next <= rx_frames + 1;\n"
		"					rx_status_next <= %s;\n", cfg->parity==true ? "fsm_parity" : "fsm_stop",
		"				else\n"
		"					rx_bits_idx_next <= rx_bits_idx + 1;\n"
		"				end if;\n"
		"			end if;\n",
		NULL
	);

	if(cfg->parity==true) {
		fprintf(F,
			"		elsif rx_status = fsm_parity then\n"
			"			-- Read the parity bit... FIXME it is discarded\n"
			"			if rx_clkdiv_cnt = clock_divider-1 then\n"
			"				rx_status_next <= fsm_stop;\n"
			"			end if;\n"
		);
	}

	fprintf(F,
		"		elsif rx_status = fsm_stop then\n"
		"			-- Read the stop bit (discarded) for just half a symbol to let some slack\n"
		"			if rx_clkdiv_cnt = clock_divider/2 then\n"
		"				-- Go waiting for next falling edge\n"
		"				rx_clkdiv_cnt_next <= (others => '0');\n"
		"				rx_status_next <= fsm_start;\n"
		"			end if;\n"
		"		end if;\n"
		"\n"
		"	end process;\n"
		"\n"
	);

	fprintf(F, "	-- The intermediate signal for FIFO data output\n");
	if(elts_nb>1) {
		for(unsigned i=0; i<elts_nb; i++) {
			fprintf(F, "	rx_fifo_tmp_data(%u downto %u) <= rx_bits%u;\n",
				(i+1) * cfg->databits_nb - 1,
				i * cfg->databits_nb,
				cfg->elts_le==true ? i : (elts_nb -1 - i)
			);
		}
	}
	else {
		fprintf(F, "	rx_fifo_tmp_data <= rx_bits0;\n");
	}

	fprintf(F,
		"\n"
		"	-- The FIFO data output\n"
	);
	if(elts_nb * cfg->databits_nb > fifo_data->width) {
		// Send more bits than the FIFO width
		fprintf(F, "	%s(%u downto %u) <= (others => ", fifo_data->port_data->name, elts_nb * cfg->databits_nb - 1, fifo_data->width);
		if(cfg->elts_pad!=0) fprintf(F, "'%c'", cfg->elts_pad);
		else fprintf(F, "rx_fifo_tmp_data(%u)", fifo_data->width-1);
		fprintf(F, ");\n");
		fprintf(F, "	%s(%u downto 0) <= rx_fifo_tmp_data;\n", fifo_data->port_data->name, fifo_data->width-1);
	}
	else if(elts_nb * cfg->databits_nb < fifo_data->width) {
		// Send less bits than the FIFO width
		fprintf(F, "	%s <= rx_fifo_tmp_data(%u downto 0);\n", fifo_data->port_data->name, elts_nb * cfg->databits_nb - 1);
	}
	else {
		// Send exactly the FIFO width
		fprintf(F, "	%s <= rx_fifo_tmp_data;\n", fifo_data->port_data->name);
	}

	fprintf(F,
		"\n"
		"end architecture;\n"
		"\n"
	);

}
static void Netlist_UartSide_Tx_GenVHDL(FILE* F, netlist_vhdl_t* params, netlist_comp_t* comp) {
	netlist_uart_side_t* uart_data = comp->data;

	netlist_uart_side_cfg_t* cfg = uart_data->cfg;
	netlist_access_uart_side_t* uartacc_data = uart_data->access_uart->data;
	netlist_access_fifo_t* fifo_data = uart_data->access_fifo->data;

	unsigned elts_nb = cfg->elts_nb;
	if(cfg->elts_nb == 0) {
		elts_nb = fifo_data->width / cfg->databits_nb;
		if(elts_nb * cfg->databits_nb < fifo_data->width) elts_nb++;
	}

	unsigned buffer_size = GetMax(1, cfg->buffer_size);

	unsigned baudrate = cfg->baudrate;
	unsigned divider  = cfg->clk_divider;
	double freq = Netlist_Access_Clock_GetFrequency(uart_data->port_clk->access);
	if(freq>0) {
		if(cfg->baudrate != 0) divider = freq / cfg->baudrate;
		else if(cfg->clk_divider != 0) baudrate = freq / cfg->clk_divider;
	}

	// Checks
	unsigned errors_nb = 0;
	unsigned frame_length = cfg->databits_nb + (cfg->parity==true ? 1 : 0) + 1;
	if(divider < 2*frame_length) {
		printf("Error: comp '%s' model '%s': Clock divider too low: %u\n", comp->name, comp->model->name, divider);
		errors_nb ++;
	}
	if(divider==0) {
		printf("Error: comp '%s' model '%s': No way to compute the clock divider value.\n", comp->name, comp->model->name);
		errors_nb ++;
	}
	if(buffer_size!=1) {
		printf("Error: comp '%s' model '%s': Buffer size of more than 1 cell is not handled.\n", comp->name, comp->model->name);
		errors_nb ++;
	}
	if(cfg->parity==true) {
		printf("Warning: comp '%s' model '%s': Parity check is not handled.\n", comp->name, comp->model->name);
		errors_nb ++;
	}
	if(errors_nb>0) {
		exit(EXIT_FAILURE);
	}

	fprintf(F,
		"\n"
		"library ieee;\n"
		"use ieee.numeric_std.all;\n"
		"\n"
	);

	Netlist_GenVHDL_Entity(F, comp, params);

	fprintf(F,
		"\n"
		"architecture %s of %s is\n", params->architecture, Netlist_Comp_GetRename_Comp(comp, params)
	);
	fprintf(F, "\n");

	if(freq>0) {
		fprintf(F, "	-- Note: Target clock frequency is %g Hz\n", freq);
	}
	else {
		fprintf(F, "	-- Note: Target clock frequency was not specified\n");
	}
	fprintf(F, "	-- Note: Baudrate is %u%s\n", baudrate, cfg->baudrate != 0 ? "" : " (computed from clock divider)");

	fprintf(F, "	-- Note: Data bits are sent as %s endian\n", cfg->uart_le==true ? "little" : "big");
	if(elts_nb>1) {
		fprintf(F, "	-- Note: Data elements are sent as %s endian\n", cfg->elts_le==true ? "little" : "big");
	}

	fprintfm(F,
		"\n"
		"	-- Some constants to ease VHDL generation\n"
		"\n"
		"	constant data_bits      : natural := %u;\n", cfg->databits_nb,
		"	constant frames_nb      : natural := %u;\n", elts_nb,
		"	constant clock_divider  : natural := %u;\n", divider,
		"	-- Bit width of a counter that counts from 0 to clock_divider-1\n",
		"	constant clockdiv_width : natural := %u;\n", uint_bitsnb(divider-1),
		"	-- Bit width of a counter that counts from 0 to data_bits-1\n",
		"	constant bitcount_width : natural := %u;\n", uint_bitsnb(cfg->databits_nb-1),
		"	-- Bit width of a counter that counts from 0 to frames_nb\n",
		"	constant framecnt_width : natural := %u;\n", uint_bitsnb(elts_nb),
		"\n"
		"	-- Types for little FSM\n"
		"	type status_type is (fsm_start, fsm_bits",
		NULL
	);

	if(cfg->parity==true) fprintf(F, ", fsm_parity");

	fprintf(F, ", fsm_stop);\n"
		"\n"
		"	-- Note: Init state is STOP to ensure at least one STOP symbol is generated\n"
		"	signal tx_status          : status_type := fsm_stop;\n"
		"	signal tx_status_next     : status_type := fsm_stop;\n"
		"	signal tx_bits_idx        : unsigned(bitcount_width-1 downto 0) := (others => '0');\n"
		"	signal tx_bits_idx_next   : unsigned(bitcount_width-1 downto 0) := (others => '0');\n"
		"	signal tx_clkdiv_cnt      : unsigned(clockdiv_width-1 downto 0) := (others => '0');\n"
		"	signal tx_clkdiv_cnt_next : unsigned(clockdiv_width-1 downto 0) := (others => '0');\n"
		"	-- An intermediate signal to cover cases where the fifo width is not a multiple of data_bits\n"
		"	signal tx_fifo_tmp_data   : std_logic_vector(frames_nb*data_bits-1 downto 0);\n"
		"	-- Frames of data bits. When there are several frames to send, the number 0 is the one being sent.\n"
	);
	for(unsigned i=0; i<elts_nb; i++) {
		fprintfm(F,
			"	signal tx_bits%u           : std_logic_vector(data_bits-1 downto 0);\n", i,
			"	signal tx_bits%u_next      : std_logic_vector(data_bits-1 downto 0);\n", i,
			NULL
		);
	}
	fprintf(F,
		"	-- Number of frames to send. Set to frames_nb at load from FIFO\n"
		"	-- It decremented each time a frame is sent.\n"
		"	signal tx_frames          : unsigned(framecnt_width-1 downto 0) := (others => '0');\n"
		"	signal tx_frames_next     : unsigned(framecnt_width-1 downto 0) := (others => '0');\n"
	);

	if(cfg->parity==true) {
		fprintf(F,
			"	signal tx_parity          : std_logic := '0';\n"
			"	signal tx_parity_next     : std_logic := '0';\n"
		);
	}

	fprintfm(F,
		"\n"
		"begin\n"
		"\n"
		"	-- Sequential process\n"
		"	process (%s, %s)\n", uart_data->port_clk->name, uart_data->port_reset->name,
		"	begin\n"
		"		if %s = '%c' then\n", uart_data->port_reset->name, Netlist_Access_Reset_GetActiveState(uart_data->port_reset->access),
		"			tx_status        <= fsm_stop;\n"
		"			tx_bits_idx      <= (others => '0');\n"
		"			tx_frames        <= (others => '0');\n"
		"			tx_clkdiv_cnt    <= (others => '0');\n",
		NULL
	);

	if(cfg->parity==true) fprintf(F, "			tx_parity        <= '0';\n");

	fprintfm(F,
		"		elsif %s(%s) then\n", Netlist_GenVHDL_ClockEdge_TestFunc_port(uart_data->port_clk), uart_data->port_clk->name,
		"			tx_status        <= tx_status_next;\n"
		"			tx_bits_idx      <= tx_bits_idx_next;\n"
		"			tx_frames        <= tx_frames_next;\n"
		"			tx_clkdiv_cnt    <= tx_clkdiv_cnt_next;\n",
		NULL
	);

	if(cfg->parity==true) fprintf(F, "			tx_parity        <= tx_parity_next;\n");

	fprintf(F,
		"		end if;\n"
		"		-- Note: The data frames are not affected by reset\n"
		"		if %s(%s) then\n", Netlist_GenVHDL_ClockEdge_TestFunc_port(uart_data->port_clk), uart_data->port_clk->name
	);
	for(unsigned i=0; i<elts_nb; i++) {
		fprintf(F, "			tx_bits%u <= tx_bits%u_next;\n", i, i);
	}
	fprintf(F,
		"		end if;\n"
		"	end process;\n"
		"\n"
		"	-- Combinatorial process\n"
		"	process (tx_status, tx_bits_idx, tx_frames, tx_clkdiv_cnt"
	);
	if(cfg->parity==true) fprintf(F, ", tx_parity");
	for(unsigned i=0; i<elts_nb; i++) fprintf(F, ", tx_bits%u", i);
	fprintfm(F, ", tx_fifo_tmp_data, %s)\n", fifo_data->port_ack->name,
		"	begin\n"
		"\n"
		"		-- Default values for ports\n"
		"		%s <= '1';\n", uartacc_data->port_uart->name,
		"		%s <= '0';\n", fifo_data->port_rdy->name,
		"		-- Default values for internal signals\n"
		"		tx_status_next     <= tx_status;\n"
		"		tx_bits_idx_next   <= tx_bits_idx;\n"
		"		tx_frames_next     <= tx_frames;\n",
		NULL
	);
	for(unsigned i=0; i<elts_nb; i++) {
		fprintf(F, "		tx_bits%u_next      <= tx_bits%u;\n", i, i);
	}

	if(cfg->parity==true) fprintf(F, "		tx_parity_next     <= tx_parity;\n");

	fprintfm(F,
		"\n"
		"		-- Clock divider\n"
		"		if tx_clkdiv_cnt = clock_divider-1 then\n"
		"			tx_clkdiv_cnt_next <= (others => '0');\n"
		"		else\n"
		"			tx_clkdiv_cnt_next <= tx_clkdiv_cnt + 1;\n"
		"		end if;\n"
		"\n"
		"		-- Compute next state and generate TX\n"
		"		if tx_status = fsm_start then\n"
		"			-- Send the start bit\n"
		"			%s <= '0';\n", uartacc_data->port_uart->name,
		"			tx_bits_idx_next <= (others => '0');\n",
		NULL
	);

	if(cfg->parity==true) fprintf(F, "			tx_parity_next <= '0';\n");

	fprintfm(F,
		"			if tx_clkdiv_cnt = clock_divider-1 then\n"
		"				tx_status_next <= fsm_bits;\n"
		"			end if;\n"
		"		elsif tx_status = fsm_bits then\n"
		"			-- Send the data bits\n"
		"			%s <= tx_bits0(%s);\n", uartacc_data->port_uart->name, cfg->uart_le==true ? "0" : "data_bits-1",
		"			if tx_clkdiv_cnt = clock_divider-1 then\n",
		NULL
	);

	if(cfg->uart_le==true) {
		fprintf(F, "				tx_bits0_next <= '0' & tx_bits0(data_bits-1 downto 1);\n");
	}
	else {
		fprintf(F, "				tx_bits_next <= tx_bits(data_bits-2 downto 0) & '0';\n");
	}

	if(cfg->parity==true) {
		fprintf(F, "				tx_parity_next <= tx_parity xor tx_bits0(%s);\n", cfg->uart_le==true ? "0" : "data_bits-1");
	}

	fprintfm(F,
		"				if tx_bits_idx = data_bits-1 then\n"
		"					tx_frames_next <= tx_frames - 1;\n"
		"					tx_status_next <= %s;\n", cfg->parity==true ? "fsm_parity" : "fsm_stop",
		NULL
	);
	if(elts_nb>1) {
		fprintf(F, "					-- Shift the data frames to send\n");
		for(unsigned i=0; i<elts_nb-1; i++) {
			fprintf(F, "					tx_bits%u_next  <= tx_bits%u;\n", i, i+1);
		}
	}
	fprintf(F,
		"				else\n"
		"					tx_bits_idx_next <= tx_bits_idx + 1;\n"
		"				end if;\n"
		"			end if;\n"
	);

	if(cfg->parity==true) {
		fprintfm(F,
			"		elsif tx_status = fsm_parity then\n"
			"			-- Send the parity bit\n"
			"			%s <= tx_parity;\n", uartacc_data->port_uart->name,
			"			if tx_clkdiv_cnt = clock_divider-1 then\n"
			"				tx_status_next <= fsm_stop;\n"
			"			end if;\n",
			NULL
		);
	}

	fprintfm(F,
		"		elsif tx_status = fsm_stop then\n"
		"			-- Send the stop bit\n"
		"			if tx_clkdiv_cnt = clock_divider-1 then\n"
		"				if tx_frames = 0 then\n"
		"					-- Stay here until some data is available to send\n"
		"					tx_clkdiv_cnt_next <= tx_clkdiv_cnt;\n"
		"				else\n"
		"					tx_status_next <= fsm_start;\n"
		"				end if;\n"
		"			end if;\n"
		"		end if;\n"
		"\n"
		"		-- Handle data input\n"
		"		if tx_frames = 0 then\n"
		"			%s <= '1';\n", fifo_data->port_rdy->name,
		"			if %s = '1' then\n", fifo_data->port_ack->name,
		"				tx_frames_next <= to_unsigned(frames_nb, framecnt_width);\n",
		NULL
	);

	if(elts_nb>1) {
		for(unsigned i=0; i<elts_nb; i++) {
			fprintf(F, "				tx_bits%u_next <= tx_fifo_tmp_data(%u downto %u);\n",
				cfg->elts_le==true ? i : (elts_nb -1 - i),
				(i+1) * cfg->databits_nb - 1,
				i * cfg->databits_nb
			);
		}
	}
	else {
		fprintf(F, "				tx_bits0_next <= tx_fifo_tmp_data;\n");
	}

	fprintf(F,
		"			end if;\n"
		"		end if;\n"
		"\n"
		"	end process;\n"
		"\n"
	);

	fprintf(F, "	-- The intermediate signal for FIFO data input\n");
	if(elts_nb * cfg->databits_nb > fifo_data->width) {
		// Send more bits than the FIFO width
		fprintf(F, "	tx_fifo_tmp_data(%u downto %u) <= (others => ", elts_nb * cfg->databits_nb - 1, fifo_data->width);
		if(cfg->elts_pad!=0) fprintf(F, "'%c'", cfg->elts_pad);
		else fprintf(F, "%s(%u)", fifo_data->port_data->name, fifo_data->width-1);
		fprintf(F, ");\n");
		fprintf(F, "	tx_fifo_tmp_data(%u downto 0) <= %s;\n", fifo_data->width-1, fifo_data->port_data->name);
	}
	else if(elts_nb * cfg->databits_nb < fifo_data->width) {
		// Send less bits than the FIFO width
		fprintf(F, "	tx_fifo_tmp_data <= %s(%u downto 0);\n", fifo_data->port_data->name, elts_nb * cfg->databits_nb - 1);
	}
	else {
		// Send exactly the FIFO width
		fprintf(F, "	tx_fifo_tmp_data <= %s;\n", fifo_data->port_data->name);
	}

	fprintf(F,
		"\n"
		"end architecture;\n"
		"\n"
	);

}

// Declaration of component models

netlist_comp_model_t* NETLIST_COMP_UART_RX = NULL;
netlist_comp_model_t* NETLIST_COMP_UART_TX = NULL;

netlist_comp_model_t* Netlist_Comp_Uart_Rx_GetModel() {
	netlist_comp_model_t* model = Netlist_CompModel_New();
	model->name  = namealloc("uart_rx");

	model->data_size         = sizeof(netlist_uart_side_t);
	model->func_dup_internal = Netlist_UartSide_DataDup;
	model->func_gen_vhdl     = Netlist_UartSide_Rx_GenVHDL;

	NETLIST_COMP_UART_RX = model;
	return model;
}
netlist_comp_model_t* Netlist_Comp_Uart_Tx_GetModel() {
	netlist_comp_model_t* model = Netlist_CompModel_New();
	model->name  = namealloc("uart_tx");

	model->data_size         = sizeof(netlist_uart_side_t);
	model->func_dup_internal = Netlist_UartSide_DataDup;
	model->func_gen_vhdl     = Netlist_UartSide_Tx_GenVHDL;

	NETLIST_COMP_UART_TX = model;
	return model;
}

// Functions specific to these components

netlist_comp_t* Netlist_Uart_Rx_New(char* name, unsigned fifo_width) {
	netlist_comp_t* comp = Netlist_Comp_New(NETLIST_COMP_UART_RX);
	comp->name = name;

	netlist_uart_side_t* uart_data = comp->data;

	char* port_name;
	char buffer[200];

	// The system ports
	port_name = namealloc("clk");
	uart_data->port_clk = Netlist_Comp_AddPortAccess_Clock(comp, port_name);

	port_name = namealloc("reset");
	uart_data->port_reset = Netlist_Comp_AddPortAccess_Reset(comp, port_name);

	// The UART access
	sprintf(buffer, "%s_uart", name);
	uart_data->access_uart = Netlist_Comp_AddAccess_Uart_Rx(comp, namealloc(buffer));

	// The Data accesses
	sprintf(buffer, "%s_fifo", name);
	uart_data->access_fifo = Netlist_Comp_AddAccess_FifoOut(comp, namealloc(buffer), fifo_width, false, NULL);

	// Shortcut to get the component config
	uart_data->cfg = Netlist_Access_UartSide_GetCfg(uart_data->access_uart);

	return comp;
}
netlist_comp_t* Netlist_Uart_Tx_New(char* name, unsigned fifo_width) {
	netlist_comp_t* comp = Netlist_Comp_New(NETLIST_COMP_UART_TX);
	comp->name = name;

	netlist_uart_side_t* uart_data = comp->data;

	char* port_name;
	char buffer[200];

	// The system ports
	port_name = namealloc("clk");
	uart_data->port_clk = Netlist_Comp_AddPortAccess_Clock(comp, port_name);

	port_name = namealloc("reset");
	uart_data->port_reset = Netlist_Comp_AddPortAccess_Reset(comp, port_name);

	// The UART access
	sprintf(buffer, "%s_uart", name);
	uart_data->access_uart = Netlist_Comp_AddAccess_Uart_Tx(comp, namealloc(buffer));

	// The Data accesses
	sprintf(buffer, "%s_fifo", name);
	uart_data->access_fifo = Netlist_Comp_AddAccess_FifoIn(comp, namealloc(buffer), fifo_width, false, NULL);

	// Shortcut to get the component config
	uart_data->cfg = Netlist_Access_UartSide_GetCfg(uart_data->access_uart);

	return comp;
}



//======================================================================
// Input buffer for differential clock
//======================================================================

netlist_comp_model_t* NETLIST_COMP_DIFFCLOCK = NULL;

static void Netlist_DiffClock_DataDup(avl_pp_tree_t* tree_old2new, netlist_comp_t* comp, netlist_comp_t* newcomp) {
	netlist_diffclock_t* dclk_data = comp->data;
	netlist_diffclock_t* newdclk_data = newcomp->data;

	// The ports and accesses

	void* pointer_old2new(void* oldptr) {
		void* newptr = NULL;
		avl_pp_find_data(tree_old2new, oldptr, &newptr);
		return newptr;
	}

	newdclk_data->port_clk_out     = pointer_old2new(dclk_data->port_clk_out);
	newdclk_data->access_diffclock = pointer_old2new(dclk_data->access_diffclock);
}

static void Netlist_DiffClock_GenVHDL_simu_latch(FILE* F, netlist_vhdl_t* params, netlist_comp_t* comp) {
	netlist_diffclock_t* dclk_data = comp->data;
	netlist_access_diffclock_t* accdclk_data = dclk_data->access_diffclock->data;

	Netlist_GenVHDL_Entity(F, comp, params);

	fprintfm(F,
		"\n"
		"architecture %s of %s is\n", params->architecture, Netlist_Comp_GetRename_Comp(comp, params),
		"\n"
		"	signal buf_out : std_logic := '0';\n"
		"\n"
		"begin\n"
		"\n"
		"	-- Warning: this is a latch, use it only for simulation\n"
		"	process(%s, %s)\n", accdclk_data->port_p->name, accdclk_data->port_n->name,
		"	begin\n"
		"		if %s = not %s then\n", accdclk_data->port_p->name, accdclk_data->port_n->name,
		"			buf_out <= %s;\n", accdclk_data->port_p->name,
		"		end if;\n"
		"	end process;\n"
		"\n"
		"	%s <= buf_out;\n", dclk_data->port_clk_out->name,
		"\n"
		"end architecture;\n"
		"\n",
		NULL
	);

}

netlist_comp_model_t* Netlist_Comp_DiffClock_GetModel() {
	netlist_comp_model_t* model = Netlist_CompModel_New();
	model->name = namealloc("diffclock");

	model->data_size         = sizeof(netlist_diffclock_t);
	model->func_dup_internal = Netlist_DiffClock_DataDup;
	model->func_gen_vhdl     = Netlist_DiffClock_GenVHDL_simu_latch;

	NETLIST_COMP_DIFFCLOCK = model;
	return model;
}

netlist_comp_t* Netlist_Comp_DiffClock_New(char* name) {
	netlist_comp_t* comp = Netlist_Comp_New(NETLIST_COMP_DIFFCLOCK);
	comp->name = name;

	netlist_diffclock_t* dclk_data = comp->data;

	dclk_data->port_clk_out     = Netlist_Comp_AddPort_Out(comp, namealloc("clk_out"), 1);
	dclk_data->access_diffclock = Netlist_Comp_AddAccess_DiffClock(comp, namealloc("diffclock"));

	return comp;
}


