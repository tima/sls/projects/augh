
// The netlist model

#ifndef _NETLIST_H_
#define _NETLIST_H_

#include <stdint.h>
#include <stdbool.h>

#include <mut.h>
#include "../hvex/hvex.h"

#include "../chain.h"



//======================================================================
// The main netlist structures and identifiers
//======================================================================

typedef struct netlist_comp_model_t    netlist_comp_model_t;
typedef struct netlist_access_model_t  netlist_access_model_t;

typedef struct netlist_val_cat_t  netlist_val_cat_t;

typedef struct netlist_comp_t    netlist_comp_t;
typedef struct netlist_port_t    netlist_port_t;
typedef struct netlist_access_t  netlist_access_t;

typedef struct netlist_vhdl_t  netlist_vhdl_t;

// Type definitions for callback functions
typedef void (*netlist_comp_cb_simp_t)(void* data, netlist_comp_t* comp);

// Flags for component models
#define NETLIST_COMPMOD_FLAG_CANSHARE  0x01

// A model that describes a particular component type
struct netlist_comp_model_t {
	char* name;
	// The size of the per-component data structure (0 if no allocation needed)
	unsigned data_size;
	// Reference to the plugin that owns this component model (NULL if AUGH)
	void* plugin;
	// Standard functions to handle this component model
	void (*func_free_data)(netlist_comp_t* comp);
	void (*func_dup_internal)(avl_pp_tree_t* tree_old2new, netlist_comp_t* comp, netlist_comp_t* newcomp);
	void (*func_gen_vhdl)(FILE* F, netlist_vhdl_t* params, netlist_comp_t* comp);
	// For netlist simplification
	void (*func_simp)(void* data, netlist_comp_t* comp);
	// For generation of functionality "inlined" in the top-level component
	bool inlvhdl_en;
	void (*func_inlvhdl_sigdecl)(FILE* F, netlist_vhdl_t* params, netlist_comp_t* comp);
	void (*func_inlvhdl_allcomps)(FILE* F, netlist_vhdl_t* params, netlist_comp_t* comp);
	void (*func_inlvhdl_onecomp)(FILE* F, netlist_vhdl_t* params, netlist_comp_t* comp);
	// Functions for technology-specific evaluations
	ptype_list* (*func_eval_size)(void* synth_target, netlist_comp_t* comp);
	void (*func_propag_delays)(void* data, netlist_port_t* port, double* delays);
	void (*func_write_comp)(void* data, netlist_comp_t* comp, double* delay, double* delay_we);
	void (*func_write_access)(void* data, netlist_access_t* access, double* delay, double* delay_we);
	void (*func_trans_delay)(void* data, netlist_comp_t* comp, double* delay);
	// For generic component models, may need some descriptive data
	void* data;
	netlist_comp_t* (*func_comp_new)(char* name, netlist_comp_model_t* model);
	void            (*func_comp_hwres_avail)(netlist_comp_t* comp, void* avail_heap);
	// Flags
	int flags;
};

// A model that describes a particular access type
struct netlist_access_model_t {
	char* name;
	// The size of the per-access data structure (0 if no allocation needed)
	unsigned data_size;
	// Reference to the plugin that owns this component model (NULL if AUGH)
	void* plugin;
	// Standard functions to handle this access model
	void (*func_free_data)(netlist_access_t* access);
	void (*func_dup_internal)(avl_pp_tree_t* tree_old2new, netlist_access_t* access, netlist_access_t* newaccess);
	// Trees to get a port from its standard, access-specific name
	// And to get the standard name from a port
	// The ports are identified from the offset in the access structure where the pointer of the port is stored
	avl_pi_tree_t ports_name2offset;
	avl_ip_tree_t ports_offset2name;
};

// An access structure
struct netlist_access_t {
	netlist_access_model_t* model;  // The access type (fifo, wire, clock, reset, boundary scan...)
	char*            name;      // The name identifies the access instance for this component only.
	netlist_comp_t*  component; // The component it belongs to.
	// The internal data. Specific to the access type.
	void* data;
};

// Per-component flags
#define NETLIST_COMP_ISEXTERN  0x01
#define NETLIST_COMP_NOPREFIX  0x02

// A component
struct netlist_comp_t {
	netlist_comp_model_t* model; // The component model
	char*            name;       // The component instance name
	avl_pp_tree_t    ports;      // Indexed by name (pointer char*)
	avl_pp_tree_t    accesses;   // Indexed by name (pointer char*)
	netlist_comp_t*  father;     // The father component, NULL if top level.
	avl_pp_tree_t    children;   // Child components, indexed by name (pointer char*)
	void*            data;       // Internal data.
	unsigned         flags;      // Per-component flags (is extern, ...)
};

// A port of a component
struct netlist_port_t {
	netlist_comp_t*   component;
	char*     name;
	char      direction;
	unsigned  width;

	netlist_access_t* access;

	// Input side: the source is the concatenation of "ValCat" elements
	// To generate VHDL, all non-Signal ports must be linked to a Signal input/output port.
	chain_list*     source;

	// Target ports (note: this field may be NULL)
	// Don't assume data in here is coherent unless your program is designed in a way it is.
	avl_p_tree_t* target_ports;
};

// Shared identifiers

// Direction of ports  FIXME Missing clean integration of INOUT in the netlist lib

#define NETLIST_PORT_DIR_NONE  0
#define NETLIST_PORT_DIR_IN    1
#define NETLIST_PORT_DIR_OUT   2

// Endianness  FIXME Unused for now

#define NETLIST_ENDIAN_NONE  0
#define NETLIST_ENDIAN_BE    1
#define NETLIST_ENDIAN_LE    2



//======================================================================
// Utility functions
//======================================================================

void Netlist_Init();

avl_pp_tree_t* Netlist_CompModel_GetTree();
avl_pp_tree_t* Netlist_AccessModel_GetTree();

netlist_comp_model_t* Netlist_CompModel_New();
void Netlist_CompModel_Free(netlist_comp_model_t* model);
bool Netlist_CompModel_Declare(netlist_comp_model_t* model);
bool Netlist_CompModel_IsRegistered(const char* name);
void Netlist_CompModel_Remove(const char* name);
netlist_comp_model_t* Netlist_CompModel_Get(const char* name);

netlist_access_model_t* Netlist_AccessModel_New();
void Netlist_AccessModel_Free(netlist_access_model_t* model);
bool Netlist_AccessModel_Declare(netlist_access_model_t* model);
bool Netlist_AccessModel_IsRegistered(const char* name);
void Netlist_AccessModel_Remove(const char* name);
netlist_access_model_t* Netlist_AccessModel_Get(const char* name);

netlist_access_t* Netlist_Access_New(netlist_access_model_t* model);
netlist_comp_t* Netlist_Comp_New(netlist_comp_model_t* model);

void Netlist_Access_Free(netlist_access_t* access);
void Netlist_Comp_Free(netlist_comp_t* comp);
void Netlist_Port_Free(netlist_port_t* port);

netlist_port_t* Netlist_Comp_GetPort(netlist_comp_t* comp, const char* name);
netlist_port_t* Netlist_Comp_AddPort(netlist_comp_t* comp, char* name, unsigned width);
netlist_port_t* Netlist_Comp_AddPort_In(netlist_comp_t* comp, char* name, unsigned width);
netlist_port_t* Netlist_Comp_AddPort_Out(netlist_comp_t* comp, char* name, unsigned width);

netlist_access_t* Netlist_Comp_GetAccess(netlist_comp_t* comp, const char* name);
netlist_access_t* Netlist_Comp_AddAccess(netlist_comp_t* comp, netlist_access_model_t* model, char* name);
netlist_comp_t*   Netlist_Comp_GetChild(netlist_comp_t* comp, const char* name);
void Netlist_Comp_SetChild(netlist_comp_t* comp, netlist_comp_t* child);
void Netlist_Comp_UnlinkChild(netlist_comp_t* comp);

#define NETLIST_COMP_CHKNAME_NOPORT  0x01
#define NETLIST_COMP_CHKNAME_NOACC   0x02

bool Netlist_Comp_PortAcc_IsNameAvail(netlist_comp_t* comp, const char* name, int flags, avl_p_tree_t* tree_avoid_names);
char* Netlist_Comp_PortAcc_MakeName(netlist_comp_t* comp, char* prefix, int flags, avl_p_tree_t* tree_avoid_names);

static inline char* Netlist_Comp_MakePortName(netlist_comp_t* comp, char* prefix, bool noaccess, avl_p_tree_t* tree_avoid_names) {
	int flags = NETLIST_COMP_CHKNAME_NOPORT | (noaccess == true ? NETLIST_COMP_CHKNAME_NOACC : 0);
	return Netlist_Comp_PortAcc_MakeName(comp, prefix, flags, tree_avoid_names);
}
static inline char* Netlist_Comp_MakeAccessName(netlist_comp_t* comp, char* prefix, bool noport, avl_p_tree_t* tree_avoid_names) {
	int flags = NETLIST_COMP_CHKNAME_NOACC | (noport == true ? NETLIST_COMP_CHKNAME_NOPORT : 0);
	return Netlist_Comp_PortAcc_MakeName(comp, prefix, flags, tree_avoid_names);
}

void Netlist_Port_Rename(netlist_port_t* port, char* name);

static inline void Netlist_Port_Unlink(netlist_port_t* port) {
	avl_pp_rem_key(&port->component->ports, port->name);
	port->component = NULL;
}
static inline void Netlist_Port_Link(netlist_comp_t* comp, netlist_port_t* port) {
	avl_pp_add_overwrite(&comp->ports, port->name, port);
	port->component = comp;
}

static inline
netlist_access_t* Netlist_Comp_AddAccess_prefix(netlist_comp_t* comp, netlist_access_model_t* model, char* prefix) {
	char* name = Netlist_Comp_MakeAccessName(comp, prefix, false, NULL);
	return Netlist_Comp_AddAccess(comp, model, name);
}

bool Netlist_IsBit10u(char c);
bool Netlist_IsBit10(char c);
char Netlist_EvalBit_Not(char c);

// Default function that can be used in component models
ptype_list* Netlist_EvalSize_Default(void* synth_target, netlist_comp_t* comp);



//======================================================================
// 'ValCat' lists
//======================================================================

// This represents a concatenation of values (i.e. output ports of other components)
// It assigns an input port.
// Usage of indexes : big endian
// If left=right and width>1, it means the selected bit is repeated.

struct netlist_val_cat_t {
	char* literal;
	netlist_port_t* source;
	unsigned width;
	unsigned left;
	unsigned right;
};

netlist_val_cat_t* Netlist_ValCat_New();
void               Netlist_ValCat_Free(netlist_val_cat_t* elt);
void               Netlist_ValCat_FreeList(chain_list* list);

unsigned           Netlist_ValCat_ListWidth(chain_list* list);
netlist_val_cat_t* Netlist_ValCat_Dup(netlist_val_cat_t* src);
chain_list*        Netlist_ValCat_DupList(chain_list* src);
bool               Netlist_ValCat_Eq(netlist_val_cat_t* valcat0, netlist_val_cat_t* valcat1);
bool               Netlist_ValCat_EqList(chain_list* list0, chain_list* list1);

netlist_val_cat_t* Netlist_ValCat_Make_Port(netlist_port_t* port, unsigned left, unsigned right);
netlist_val_cat_t* Netlist_ValCat_Make_FullPort(netlist_port_t* port);
chain_list*        Netlist_ValCat_MakeList_FullPort(netlist_port_t* port);
netlist_val_cat_t* Netlist_ValCat_Make_Literal(char* value);
chain_list*        Netlist_ValCat_MakeList_Literal(char* value);
void               Netlist_ValCat_Set_RepeatBit(netlist_val_cat_t* valcat, char bit, unsigned nb);
netlist_val_cat_t* Netlist_ValCat_Make_RepeatBit(char bit, unsigned nb);
chain_list*        Netlist_ValCat_MakeList_RepeatBit(char bit, unsigned nb);

chain_list*        Netlist_ValCat_GetLeftBit(chain_list* valcat);
char               Netlist_ValCat_GetLeftBit_Char(chain_list* valcat);

chain_list*        Netlist_ValCat_AddBitsLeft(chain_list* valcat, char bit, unsigned bits_nb);
chain_list*        Netlist_ValCat_Crop(chain_list* list, unsigned left, unsigned right);

chain_list*        Netlist_ValCat_Simplify(chain_list* list);
void               Netlist_Comp_Simplify(netlist_comp_t* comp);

bool               Netlist_Literal_IsAllBit(const char* lit, char bit);
bool               Netlist_Literal_IsNoBit(const char* lit, char bit);

static inline bool Netlist_ValCat_IsAllBit(const netlist_val_cat_t* valcat, char bit) {
	if(valcat->literal==NULL) return false;
	return Netlist_Literal_IsAllBit(valcat->literal, bit);
}
bool               Netlist_ValCat_ListIsAllBit(const chain_list* list, char bit);

static inline bool Netlist_ValCat_IsLitNoBit(const netlist_val_cat_t* valcat, char bit) {
	if(valcat->literal==NULL) return false;
	return Netlist_Literal_IsNoBit(valcat->literal, bit);
}
bool               Netlist_ValCat_ListIsLitNoBit(const chain_list* list, char bit);

static inline bool Netlist_ValCat_IsSourceFullPort(const netlist_val_cat_t* valcat, const netlist_port_t* srcport) {
	if(valcat->source!=srcport || valcat->right!=0 || valcat->left!=srcport->width-1 || valcat->width!=srcport->width) return false;
	return true;
}

static inline bool Netlist_Port_IsSourceFullPort(const netlist_port_t* port, const netlist_port_t* srcport) {
	if(port->source==NULL) return false;
	if(port->source->NEXT!=NULL) return false;
	netlist_val_cat_t* valcat = port->source->DATA;
	return Netlist_ValCat_IsSourceFullPort(valcat, srcport);
}



//===================================================
// Lists of target ports
//===================================================

// Working with the tree of target ports in the port structure
void Netlist_PortTargets_CompUnlinkIn(netlist_comp_t* comp);
void Netlist_PortTargets_CompClear(netlist_comp_t* comp);
void Netlist_PortTargets_CompFree(netlist_comp_t* comp);
void Netlist_PortTargets_CompBuild(netlist_comp_t* comp);
void Netlist_PortTargets_SetSource(netlist_port_t* port);
void Netlist_PortTargets_RemSource(netlist_port_t* port);

void Netlist_PortTargets_ReplaceOutByPort(netlist_port_t* port_out, netlist_port_t* port_new);
void Netlist_PortTargets_ReplaceOutByLit(netlist_port_t* port_out, char* lit_new);
void Netlist_PortTargets_ReplaceOutByValcat_Repeat1Bit(netlist_port_t* port_out, netlist_val_cat_t* bit_valcat);
void Netlist_PortTargets_ReplaceOutByLit_Repeat1Bit(netlist_port_t* port_out, char c);
void Netlist_PortTargets_ReplaceOutByValcat_List(netlist_port_t* port_out, chain_list* list);

bool Netlist_PortTargets_GetMaxIndexes(netlist_port_t* port, unsigned* max_left, unsigned* min_right);

static inline bool Netlist_PortTargets_CheckEmpty(const netlist_port_t* port) {
	if(port->target_ports==NULL || avl_p_isempty(port->target_ports)==true) return true;
	return false;
}
static inline unsigned Netlist_PortTargets_GetNb(const netlist_port_t* port) {
	if(port->target_ports==NULL) return 0;
	return avl_p_count(port->target_ports);
}

static inline netlist_port_t* Netlist_PortTargets_GetFirst(const netlist_port_t* port) {
	if(Netlist_PortTargets_CheckEmpty(port)==true) return NULL;
	return port->target_ports->root->data;
}

static inline chain_list* Netlist_ValCat_SignExtend_Add(chain_list* valcat, unsigned bits_nb) {
	return Netlist_ValCat_AddBitsLeft(valcat, 0, bits_nb);
}
static inline chain_list* Netlist_ValCat_CropOrExtend(chain_list* valcat, unsigned width, unsigned new_width, char bit) {
	if(width<new_width) return Netlist_ValCat_AddBitsLeft(valcat, new_width-width, bit);
	if(width>new_width) return Netlist_ValCat_Crop(valcat, width-new_width, 0);
	return valcat;
}

bool Netlist_Comp_HasConnections(netlist_comp_t* comp);



//===================================================
// Debug
//===================================================

void Netlist_ValCat_DebugPrintList(chain_list const * list);

void Netlist_PortTargets_DebugPrint(const netlist_port_t* port);

void Netlist_Comp_DebugPrintPorts(const netlist_comp_t* comp);

void Netlist_Debug_CheckIntegrity(const netlist_comp_t* top);



//======================================================================
// Duplication
//======================================================================

void Netlist_Access_Dup_LinkPorts(void* new_data, void* old_data, netlist_access_model_t* model, avl_pp_tree_t* tree_old2new);
netlist_access_t* Netlist_Access_Dup(netlist_access_t* access, netlist_comp_t* comp_dest, char* newname);

netlist_comp_t* Netlist_Comp_Dup_Lnk_flag(avl_pp_tree_t* tree_old2new, netlist_comp_t* comp, bool do_internals);
netlist_comp_t* Netlist_Comp_Dup_flag(netlist_comp_t* comp, bool do_internals);

static inline netlist_comp_t* Netlist_Comp_Dup(netlist_comp_t* comp) {
	return Netlist_Comp_Dup_flag(comp, true);
}
static inline netlist_comp_t* Netlist_Comp_Dup_Lnk(avl_pp_tree_t* tree_old2new, netlist_comp_t* comp) {
	return Netlist_Comp_Dup_Lnk_flag(tree_old2new, comp, true);
}



#endif  // _NETLIST_H_


