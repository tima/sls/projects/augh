
#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#include "stdint.h"
#include "stdbool.h"
#include "ctype.h"
#include "assert.h"

// For mkdir flags
#include <sys/stat.h>

#include "../auto/auto.h"

#include "netlist.h"
#include "netlist_tb.h"
#include "netlist_comps.h"
#include "netlist_access.h"
#include "netlist_vhdl.h"
#include "netlist_cmd.h"



tb_fifo_t* TB_FifoData_New() {
	tb_fifo_t* tb_fifo = calloc(1, sizeof(*tb_fifo));
	tb_fifo->is_the_in = false;
	tb_fifo->is_the_out = false;
	tb_fifo->is_littleendian = true;
	tb_fifo->stop_end_vectors = false;
	return tb_fifo;
}
void TB_FifoData_Free(tb_fifo_t* tb_fifo) {
	foreach(tb_fifo->list_vec, scanVec) free(scanVec->DATA);
	freechain(tb_fifo->list_vec);
	free(tb_fifo);
}

tb_fifo_t* TB_FifoData_List_FindName(chain_list* list_fifos, const char* name) {
	foreach(list_fifos, scan) {
		tb_fifo_t* tb_fifo = scan->DATA;
		if(tb_fifo->channel_name==name) return tb_fifo;
	}
	return NULL;
}
tb_fifo_t* TB_FifoData_List_FindTheIn(chain_list* list_fifos) {
	foreach(list_fifos, scan) {
		tb_fifo_t* tb_fifo = scan->DATA;
		if(tb_fifo->is_the_in==true) return tb_fifo;
	}
	return NULL;
}
tb_fifo_t* TB_FifoData_List_FindTheOut(chain_list* list_fifos) {
	foreach(list_fifos, scan) {
		tb_fifo_t* tb_fifo = scan->DATA;
		if(tb_fifo->is_the_out==true) return tb_fifo;
	}
	return NULL;
}
tb_fifo_t* TB_FifoData_Tree_FindName(avl_pp_tree_t* tree_fifos, const char* name) {
	avl_pp_node_t* pp_node = NULL;
	bool b = avl_pp_find(tree_fifos, name, &pp_node);
	if(b==true) return pp_node->data;
	return NULL;
}

// Note: in the generated TB, the vector format is: X"00" (+ the comma-space separator)
// New lines are used to make line length "normal"
// The list returned contains only the hex string, without the X""

static int Netlist_TB_ReadFifoVectors_rawbin(FILE* vecF, tb_fifo_t* tb_fifo) {
	chain_list* last_vec = NULL;

	tb_fifo->vectors_nb = 0;
	tb_fifo->vector_width = 8 * tb_fifo->bytes_nb;
	tb_fifo->allocsize = tb_fifo->bytes_nb * 2 + 1;

	// A buffer to store one vector from the input file
	uint8_t vecbuf[tb_fifo->bytes_nb];

	// Read the vectors
	do {
		// Read one vector
		size_t s = fread(vecbuf, 1, tb_fifo->bytes_nb, vecF);
		if(s < tb_fifo->bytes_nb) break;

		// Swab bytes if little endian
		if(tb_fifo->is_littleendian==true) {
			for(unsigned i=0; i<tb_fifo->bytes_nb/2; i++) Swap(vecbuf[i], vecbuf[tb_fifo->bytes_nb-i-1]);
		}

		// Convert to hex representation
		char* allocbuf = calloc(tb_fifo->allocsize, sizeof(*allocbuf));
		for(unsigned i=0; i<tb_fifo->bytes_nb; i++) sprintf(allocbuf + 2*i, "%02x", vecbuf[i]);

		// Save in the chained list
		chain_list* newelt = addchain(NULL, allocbuf);
		if(tb_fifo->list_vec==NULL) tb_fifo->list_vec = newelt;
		else last_vec->NEXT = newelt;
		last_vec = newelt;

		tb_fifo->vectors_nb ++;
	}while(1);

	return 0;
}
static int Netlist_TB_ReadFifoVectors_text(FILE* vecF, tb_fifo_t* tb_fifo) {
	netlist_access_fifo_t* fifo_data = tb_fifo->access->data;
	chain_list* last_vec = NULL;

	unsigned bytes_nb = (fifo_data->width + 7) / 8;
	unsigned hex_bytes_nb = (fifo_data->width + 3) / 4;

	tb_fifo->vectors_nb = 0;
	tb_fifo->vector_width = hex_bytes_nb * 4;
	tb_fifo->allocsize = hex_bytes_nb + 1;

	// Utility functions to convert digit->value in context hex, dec or bin
	bool invalid_char = false;
	unsigned conv_hex(char c) {
		unsigned val = 0;
		if('0' <= c && c <= '9') val = c - '0';
		else if( ('a' <= c && c <= 'f') || ('A' <= c && c <= 'F') ) val = 10 + c - 'a';
		else invalid_char = true;
		return val;
	}
	unsigned conv_dec(char c) {
		unsigned val = 0;
		if('0' <= c && c <= '9') val = c - '0';
		else invalid_char = true;
		return val;
	}
	unsigned conv_bin(char c) {
		unsigned val = 0;
		if('0' <= c && c <= '1') val = c - '0';
		else invalid_char = true;
		return val;
	}

	// To store the temporary machine-binary representation
	// FIXME add one byte in case the lowest negative value is used in a decimal vector
	//   because first the positive value is read, then it is negated
	//   it the FIFO is 8 bits, and the value -128 is given...
	uint8_t binbuf[bytes_nb];
	for(unsigned i=0; i<bytes_nb; i++) binbuf[i] = 0;
	char c;

	// Read the vectors
	c = fgetc(vecF);
	do {
		// Read one vector

		// Find the next char
		do {
			if(c==EOF) break;
			if(isspace(c)!=0) continue;
			if(c==',') continue;
			if(c=='#') {
				// Skip all until EOL
				do {
					c = fgetc(vecF);
					if(c==EOF) break;
					if(c=='\n') break;
				} while(1);
				if(c==EOF) break;
				continue;
			}
			c = fgetc(vecF);
		} while(1);
		if(c==EOF) break;

		// Read the number
		unsigned readchars = 0;
		char charsign = 0;
		uint8_t char1 = 0;

		unsigned datatype = 0;  // bin, dec or hex
		unsigned digit_factor = 0;

		void set_style_hex() {
			digit_factor = 16;
			datatype = TB_DATA_HEX;
		}
		void set_style_dec() {
			digit_factor = 10;
			datatype = TB_DATA_DEC;
		}
		void set_style_bin() {
			digit_factor = 2;
			datatype = TB_DATA_BIN;
		}

		if     (tb_fifo->style==TB_DATA_HEX) set_style_hex();
		else if(tb_fifo->style==TB_DATA_DEC) set_style_dec();
		else if(tb_fifo->style==TB_DATA_BIN) set_style_bin();

		// Use only when datatype!=0
		void add_digit(char d) {
			// Get the value to add
			unsigned val = 0;
			if     (datatype==TB_DATA_HEX) val = conv_hex(d);
			else if(datatype==TB_DATA_DEC) val = conv_dec(d);
			else if(datatype==TB_DATA_BIN) val = conv_bin(d);
			else abort();  // paranoia
			// Multiply the vector and add
			for(int i=bytes_nb-1; i>=0; i--) {
				unsigned tmp = binbuf[i] * digit_factor + val;
				binbuf[i] = tmp & 0xFF;
				val = (tmp >> 8) & 0xFF;
			}
		}

		// Process the already-read character
		if(c=='+' || c=='-') { charsign = c; c = fgetc(vecF); }

		do {
			if(c==EOF) break;
			if(isspace(c)!=0) break;
			if(c=='#' || c==',') break;

			if(datatype==0) {
				readchars++;
				if(readchars==1) char1 = c;
				if(readchars==2 && char1=='0') {
					if (c=='x' || c=='X') {
						set_style_hex();
						readchars = 0;
						binbuf[bytes_nb-1] = 16 * conv_hex(char1) + conv_hex(c);
					}
					else if(c=='b' || c=='B') {
						set_style_bin();
						readchars = 0;
						binbuf[bytes_nb-1] = 2 * conv_bin(char1) + conv_bin(c);
					}
				}
				if(readchars==2 && datatype==0) {
					set_style_dec();
					binbuf[bytes_nb-1] = 10 * conv_dec(char1) + conv_dec(c);
				}
			}
			else {
				add_digit(c);
			}
			if(invalid_char==true) break;

			// Read the next char
			c = fgetc(vecF);
		} while(1);  // Loop that reads the digits

		if(readchars==0) break;
		if(invalid_char==true) break;

		if(datatype==0) {
			assert(readchars==1);
			set_style_dec();
			binbuf[bytes_nb-1] = conv_dec(char1);
		}

		// Handle signedness
		if(datatype==TB_DATA_DEC && charsign=='-') {
			// Inverse the bits, then add 1
			unsigned carry = 1;
			for(int i=bytes_nb-1; i>=0; i--) {
				unsigned tmp = (~binbuf[i]) + carry;
				binbuf[i] = tmp & 0xFF;
				carry = (tmp >> 8) & 0xFF;
			}
		}

		// Convert to hex representation
		char* allocbuf = calloc(tb_fifo->allocsize, sizeof(*allocbuf));
		for(unsigned i=0; i<bytes_nb; i++) sprintf(allocbuf + 2*i, "%02x", binbuf[i]);
		// Save in the chained list
		chain_list* newelt = addchain(NULL, allocbuf);
		if(tb_fifo->list_vec==NULL) tb_fifo->list_vec = newelt;
		else last_vec->NEXT = newelt;
		last_vec = newelt;

		tb_fifo->vectors_nb ++;

	}while(1);

	if(invalid_char==true) {
		printf("WARNING: Invalid characters were found when reading test vectors for fifo '%s'. Vectors may be incomplete.\n", tb_fifo->channel_name);
	}

	return 0;
}

int Netlist_TB_ReadFifoVectors(tb_fifo_t* tb_fifo) {
	if(tb_fifo->file_name==NULL) {
		// This should not happen here
		abort();
	}

	FILE* vecF = fopen(tb_fifo->file_name, "rb");
	if(vecF==NULL) {
		printf("ERROR: Could not open the file %s\n", tb_fifo->file_name);
		return -1;
	}

	int ret = 0;

	if(tb_fifo->style==TB_DATA_RAWBIN) {
		ret = Netlist_TB_ReadFifoVectors_rawbin(vecF, tb_fifo);
	}
	else if(tb_fifo->style==TB_DATA_AUTO || tb_fifo->style==TB_DATA_HEX || tb_fifo->style==TB_DATA_DEC || tb_fifo->style==TB_DATA_BIN) {
		ret = Netlist_TB_ReadFifoVectors_text(vecF, tb_fifo);
	}
	else {
		abort();
	}

	fclose(vecF);

	if(ret!=0) return ret;

	if(tb_fifo->vectors_nb==0) {
		printf("ERROR: No vector was read for fifo '%s' file '%s'\n", tb_fifo->channel_name, tb_fifo->file_name);
		return -1;
	}

	return 0;
}

void Netlist_TB_DumpFifoVectors(FILE* F, tb_fifo_t* tb_fifo, char* tabs, unsigned linesize) {
	if(tabs==NULL) tabs = "";

	unsigned line_val_max = 0;
	if(linesize>0) {
		line_val_max = linesize / (2 * tb_fifo->bytes_nb + 6);
		if(line_val_max<1) line_val_max = 1;
	}

	bool do_nl = false;    // To insert a newline
	bool is_first = true;  // First in the line
	unsigned line_val_cur = 0;

	fprintf(F, tabs);
	foreach(tb_fifo->list_vec, scanVec) {
		char* vec = scanVec->DATA;
		if(do_nl==true) { fprintf(F, ",\n%s", tabs); do_nl = false; }
		if(is_first==true) is_first = false; else fprintf(F, ", ");
		fprintf(F, "X\"%s\"", vec);
		line_val_cur ++;
		if(line_val_max>0 && line_val_cur>=line_val_max) { line_val_cur = 0; do_nl = true; is_first = true; }
	}

	fprintf(F, "\n");
}

// FIXME Check if there are more than one clock, reset, start signals
// FIXME The clock may be generated inside
int Netlist_TB_Gen(implem_t* Implem, avl_pp_tree_t* tree_fifos, const char* destdir, unsigned simu_max_cycles) {
	int error_code = 0;
	unsigned nb_ports_noacc = 0;

	int   clkedge              = Netlist_Access_Clock_GetEdge(Implem->netlist.port_clock->access);
	char* clkedge_testfunc     = Netlist_GenVHDL_ClockEdge_TestFunc(clkedge);
	int   clkedge_neg          = -clkedge;
	char* clkedge_neg_testfunc = Netlist_GenVHDL_ClockEdge_TestFunc(clkedge_neg);

	// Generation parameters, only the minimum necessary
	netlist_vhdl_t params;
	Netlist_GenParams_Init(&params);
	// Rename identifiers
	bitype_list* comp_ren = Netlist_Comp_Rename(Implem->netlist.top, &params);
	avl_pp_add_overwrite(&params.comp_rename, Implem->netlist.top, comp_ren);

	mkdir(destdir, S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);

	char path[1024];
	sprintf(path, "%s/tb.vhd", destdir);
	FILE* F = fopen(path, "wb");
	if(F==NULL) {
		printf("ERROR: Could not open the file %s\n", path);
		error_code = __LINE__;
		goto END_POINT;
	}

	fprintf(F,
		"\n"
		"library ieee;\n"
		"use ieee.std_logic_1164.all;\n"
		"use ieee.numeric_std.all;\n"
		"\n"
		"library std;\n"
		"use std.textio.all;\n"
		"\n"
		"entity tb is\n"
		"end tb;\n"
		"\n"
		"architecture augh of tb is\n"
		"\n"
	);

	fprintf(F, "	constant simu_max_cycles : natural := %u;\n", simu_max_cycles);
	fprintf(F, "	constant simu_disp_cycles : std_logic := '%c';\n", tb_disp_cycles==true ? '1' : '0');
	fprintf(F, "	constant simu_err_end_in : std_logic := '%c';\n", tb_err_end_in==true ? '1' : '0');
	fprintf(F, "	constant reset_cycles : natural := 4;\n");
	fputc('\n', F);

	// Component declaration
	Netlist_GenVHDL_CompDecl(F, Implem->netlist.top, &params);

	// Top-level control signals
	fprintf(F,
		"\n"
		"	signal clock : std_logic := '0';\n"
		"	signal reset : std_logic := '0';\n"
		"	signal start : std_logic := '0';\n"
		"\n"
		"	signal clock_next : std_logic := '0';\n"
		"\n"
	);

	// Create signals for top-level accesses
	avl_pp_foreach(&Implem->netlist.top->accesses, scanAccess) {
		netlist_access_t* access = scanAccess->data;
		fprintf(F, "	-- Access '%s' model '%s'\n", access->name, access->model->name);

		// Main control ports
		if(access->model==NETLIST_ACCESS_CLOCK || access->model==NETLIST_ACCESS_RESET || access->model==NETLIST_ACCESS_START) {
			// Note: Special signals are already created for these special ports

			//netlist_access_single_port_t* singleport_data = access->data;
			//fprintf(F, "	signal wrap_%s : std_logic := '0';\n", singleport_data->port->name);
		}

		// FIFOs
		else if(access->model==NETLIST_ACCESS_FIFO_IN || access->model==NETLIST_ACCESS_FIFO_OUT) {
			// Generate the signals
			netlist_access_fifo_t* fifo_data = access->data;
			fprintf(F, "	signal %s_data : ", access->name); Netlist_GenVHDL_StdLogicType_ibe(F, fifo_data->width, '0', NULL, ";\n");
			fprintf(F, "	signal %s_rdy  : std_logic := '0';\n", access->name);
			fprintf(F, "	signal %s_ack  : std_logic := '0';\n", access->name);
			fprintf(F, "	signal %s_data_idx : natural := 0;\n", access->name);

			// Get the fifo config
			tb_fifo_t* tb_fifo = TB_FifoData_Tree_FindName(tree_fifos, access->name);
			if(tb_fifo==NULL || tb_fifo->file_name==NULL) {
				fputc('\n', F);
				continue;
			}

			// Read the test vectors, convert them to VHDL syntax, save in a temp file
			int z = Netlist_TB_ReadFifoVectors(tb_fifo);
			if(z!=0) {
				printf("ERROR: Could not get the vectors for fifo '%s'.\n", tb_fifo->channel_name);
				error_code = __LINE__;
				goto END_POINT;
			}

			if(tb_fifo->stop_nb > 0 || tb_fifo->stop_end_vectors==true) {
				fprintf(F, "	signal %s_wantstop : boolean := false;\n", access->name);
			}
			fprintf(F, "	signal %s_errors : natural := 0;\n", access->name);
			fprintf(F, "	signal %s_vector : ", access->name); Netlist_GenVHDL_StdLogicType_ibe(F, tb_fifo->vector_width, '0', NULL, ";\n");
			fprintf(F, "	-- Test vectors\n");
			fprintf(F, "	constant %s_vectors_nb : natural := %u;\n", access->name, tb_fifo->vectors_nb);
			fprintf(F, "	type %s_vec_type is array (0 to %s_vectors_nb-1) of ", access->name, access->name);
			Netlist_GenVHDL_StdLogicType_be(F, tb_fifo->vector_width, NULL, ";\n");
			fprintf(F, "	constant %s_vectors : %s_vec_type := (", access->name, access->name);
			if(tb_fifo->vectors_nb==1) fprintf(F, " 0 => ");
			fputc('\n', F);

			// Dump the vectors
			Netlist_TB_DumpFifoVectors(F, tb_fifo, "\t\t", 120);
			fprintf(F, "	);\n");
		}  // End case fifo

		// Unknown access
		else {
			fprintf(F, "		-- Warning: This access is not handled\n");
			avl_pp_foreach(&Implem->netlist.top->ports, scanPort) {
				netlist_port_t* port = scanPort->data;
				if(port->access!=access) continue;
				fprintf(F, "	signal wrap_%s : ", port->name); Netlist_GenVHDL_StdLogicType_ibe(F, port->width, '0', NULL, ";\n");
			}
			continue;
		}

		fputc('\n', F);
	}

	// Create signals for top-level ports that are not part of an access
	nb_ports_noacc = 0;
	avl_pp_foreach(&Implem->netlist.top->ports, scanPort) {
		netlist_port_t* port = scanPort->data;
		if(port->access!=NULL) continue;
		if(nb_ports_noacc==0) fprintf(F, "		-- Ports with unknown functionality\n");
		nb_ports_noacc ++;
		fprintf(F, "		signal wrap_%s : ", port->name); Netlist_GenVHDL_StdLogicType_ibe(F, port->width, '0', NULL, ";\n");
	}

	// Top-level control signals
	fprintf(F,
		"	signal clock_counter      : natural := 0;\n"
		"	signal clock_counter_stop : natural := 0;\n"
		"\n"
		"	signal errors_nb : natural := 0;\n"
		"\n"
		"	-- Defined in VHDL 2008, not handled by GHDL\n"
		"	function to_string(sv: std_logic_vector) return string is\n"
		"		variable bv: bit_vector(sv\'range) := to_bitvector(sv);\n"
		"		variable lp: line;\n"
		"	begin\n"
		"		write(lp, bv);\n"
		"		return lp.all;\n"
		"	end;\n"
		"\n"
		"begin\n"
		"\n"
	);

	// Instantiation of the main component

	fprintf(F,
		"	-- Instantiation of the main component\n"
		"	%s_i : %s port map (\n", Implem->netlist.top->name, Implem->netlist.top->name
	);

	bool ports_written = false;

	// Connect ports of top-level accesses
	avl_pp_foreach(&Implem->netlist.top->accesses, scanAccess) {
		netlist_access_t* access = scanAccess->data;
		ports_written = true;
		fprintf(F, "		-- Access '%s' model '%s'\n", access->name, access->model->name);

		// Main control ports
		if(access->model==NETLIST_ACCESS_CLOCK) {
			netlist_access_single_port_t* singleport_data = access->data;
			fprintf(F, "		%s => clock,\n", singleport_data->port->name);
		}
		else if(access->model==NETLIST_ACCESS_RESET) {
			netlist_access_single_port_t* singleport_data = access->data;
			fprintf(F, "		%s => reset,\n", singleport_data->port->name);
		}
		else if(access->model==NETLIST_ACCESS_START) {
			netlist_access_single_port_t* singleport_data = access->data;
			fprintf(F, "		%s => start,\n", singleport_data->port->name);
		}

		// Fifo channels
		else if(access->model==NETLIST_ACCESS_FIFO_IN || access->model==NETLIST_ACCESS_FIFO_OUT) {
			netlist_access_fifo_t* fifo_data = access->data;
			fprintfm(F,
				"		%s => %s_data,\n", fifo_data->port_data->name, access->name,
				"		%s => %s_rdy,\n", fifo_data->port_rdy->name, access->name,
				"		%s => %s_ack,\n", fifo_data->port_ack->name, access->name,
				NULL
			);
		}

		// Unknown access
		else {
			fprintf(F, "		-- Warning: this access is not handled\n");
			avl_pp_foreach(&Implem->netlist.top->ports, scanPort) {
				netlist_port_t* port = scanPort->data;
				if(port->access!=access) continue;
				fprintf(F, "		%s => wrap_%s,\n", port->name, port->name);
			}
		}

	}  // Process all accesses

	// Connect the top-level ports that are not part of an access
	nb_ports_noacc = 0;
	avl_pp_foreach(&Implem->netlist.top->ports, scanPort) {
		netlist_port_t* port = scanPort->data;
		if(port->access!=NULL) continue;
		ports_written = true;
		if(nb_ports_noacc==0) fprintf(F, "		-- Ports with unknown functionality\n");
		nb_ports_noacc ++;
		fprintf(F, "		%s => wrap_%s,\n", port->name, port->name);
	}

	// Overwrite the last ',\n'
	if(ports_written==true) fseek(F, -2, SEEK_END);
	// End of instantiation
	fprintf(F,
		"\n"
		"	);\n"
		"\n"
	);

	// Create the functionality associated to each top-level access / port

	avl_pp_foreach(&Implem->netlist.top->accesses, scanAccess) {
		netlist_access_t* access = scanAccess->data;
		fprintf(F, "	-- Functionality for top-level access '%s' model '%s'\n", access->name, access->model->name);

		char* n = access->name;  // Shortcut to ease writing the VHDL generation code

		// Get the fifo config
		tb_fifo_t* tb_fifo = TB_FifoData_Tree_FindName(tree_fifos, access->name);

		if(access->model==NETLIST_ACCESS_CLOCK) {
			fprintfm(F,
				"	-- Generation of clock: 100MHz (note: arbitrary value)\n"
				"	clock <= clock_next after 5 ns;\n"
				"	clock_next <= not clock when clock_counter_stop = 0 or clock_counter <= clock_counter_stop else \'0\';\n"
				"\n"
				"	-- Clock counter and global messages\n"
				"	process (clock)\n"
				"		-- To print simulation messages\n"
				"		variable l : line;\n"
				"	begin\n"
				"\n"
				"		-- Increment clock counter\n"
				"		if %s(clock) then\n", clkedge_testfunc,
				"\n"
				"			clock_counter <= clock_counter + 1;\n"
				"\n"
				"			if simu_disp_cycles = '1' then\n"
				"				-- Write simulation message\n"
				"				write(l, string\'(\"INFO clock cycle \"));\n"
				"				write(l, clock_counter);\n"
				"				writeline(output, l);\n"
				"			end if;\n"
				"\n"
				"		end if;\n"
				"\n"
				"		-- Messages\n"
				"		if %s(clock) then\n", clkedge_neg_testfunc,
				"\n"
				"			if clock_counter_stop = 0 and clock_counter >= simu_max_cycles then\n"
				"				write(l, string\'(\"Max number of clock cycles reached. Stopping simulation.\"));\n"
				"				writeline(output, l);\n"
				"				clock_counter_stop <= clock_counter + 3;\n"
				"			end if;\n"
				"\n",
				NULL
			);

			avl_pp_foreach(&Implem->netlist.top->accesses, scanOtherAccess) {
				netlist_access_t* other_access = scanOtherAccess->data;
				if(other_access->model!=NETLIST_ACCESS_FIFO_OUT) continue;
				tb_fifo_t* other_tb_fifo = TB_FifoData_Tree_FindName(tree_fifos, other_access->name);
				if(other_tb_fifo==NULL) continue;
				if(other_tb_fifo->stop_nb==0 && other_tb_fifo->stop_end_vectors==false) continue;
				fprintfm(F,
					"			if clock_counter_stop = 0 and %s_wantstop = true then\n", other_access->name,
					"				clock_counter_stop <= clock_counter + 3;\n"
					"			end if;\n"
					"\n",
					NULL
				);
			}

			fprintf(F,
				"			if clock_counter_stop > 0 and clock_counter + 1 = clock_counter_stop then\n"
			);
			avl_pp_foreach(&Implem->netlist.top->accesses, scanOtherAccess) {
				netlist_access_t* other_access = scanOtherAccess->data;
				if(other_access->model!=NETLIST_ACCESS_FIFO_OUT) continue;
				tb_fifo_t* other_tb_fifo = TB_FifoData_Tree_FindName(tree_fifos, other_access->name);
				if(other_tb_fifo==NULL) continue;
				if(other_tb_fifo->file_name==NULL) continue;
				fprintfm(F,
					"				if %s_errors > 0 then\n", other_access->name,
					"					write(l, string\'(\"ERROR Wrong outputs were found for fifo %s.\"));\n", other_access->name,
					"					writeline(output, l);\n"
					"				end if;\n",
					NULL
				);
			}
			fprintf(F,
				"			end if;\n"
				"\n"
				"			if clock_counter < reset_cycles then\n"
				"				report \"INFO Reset\" severity note;\n"
				"			end if;\n"
				"\n"
				"			if clock_counter = reset_cycles then\n"
				"				report \"INFO Start\" severity note;\n"
				"			end if;\n"
				"\n"
				"		end if;\n"
				"\n"
				"	end process;\n"
			);
		}
		else if(access->model==NETLIST_ACCESS_RESET) {
			fprintf(F,
				"	-- Generation of reset\n"
				"	reset <= \'1\' when clock_counter < reset_cycles else \'0\';\n"
			);
		}
		else if(access->model==NETLIST_ACCESS_START) {
			fprintf(F,
				"	-- Generation of start\n"
				"	start <= \'1\';\n"
			);
		}

		else if(access->model==NETLIST_ACCESS_FIFO_IN) {
			netlist_access_fifo_t* fifo_data = access->data;

			fprintfm(F,
				"	-- FIFO %s\n", n,
				"	-- Sending inputs\n"
				"\n",
				NULL
			);

			if(tb_fifo!=NULL && tb_fifo->file_name!=NULL) {
				fprintfm(F,
					"	%s_vector <= %s_vectors(%s_data_idx) when %s_data_idx < %s_vectors_nb else (others => \'0\');\n", n, n, n, n, n,
					"	%s_data <= %s_vector(%u downto 0);\n", n, n, fifo_data->width-1,
					"\n"
					"	%s_ack <= \'1\' when reset = \'0\' and %s_data_idx < %s_vectors_nb else \'0\';\n", n, n, n,
					"\n",
					NULL
				);
			}
			else {
				fprintfm(F,
					"	-- Note: No test vectors, send nothing\n"
					"	%s_ack <= \'0\';\n", n,
					"	%s_data <= ", n,
					"\n",
					NULL
				);
				Netlist_GenVHDL_StdLogic_Literal_Repeat_be(F, fifo_data->width, '0', NULL, ";\n");
			}

			fprintfm(F,
				"	process (clock)\n"
				"		-- To print simulation messages\n"
				"		variable l : line;\n"
				"	begin\n"
				"\n"
				"		if %s(clock) then\n", clkedge_testfunc,
				"			if %s_rdy = \'1\' and %s_ack = \'1\' and reset = \'0\' then\n", n, n,
				"\n"
				"				-- Write simulation message\n"
				"				write(l, string\'(\"INFO Fifo %s: Input \"));\n", n,
				"				write(l, %s_data_idx);\n", n,
				"				write(l, string\'(\" at cycle \"));\n"
				"				write(l, clock_counter);\n"
				"				writeline(output, l);\n"
				"\n",
				NULL
			);

			if(tb_fifo!=NULL && tb_fifo->file_name!=NULL) {
				fprintfm(F,
					"				if %s_data_idx < %s_vectors_nb then\n", n, n,
					"\n"
					"					if %s_data_idx = 0 then\n", n,
					"						write(l, string\'(\"INFO Fifo %s: First input vector sent at clock cycle \"));\n", n,
					"						write(l, clock_counter);\n"
					"						writeline(output, l);\n"
					"					end if;\n"
					"\n"
					"					if %s_data_idx = %s_vectors_nb - 1 then\n", n, n,
					"						write(l, string\'(\"INFO Fifo %s: Last input vector sent at clock cycle \"));\n", n,
					"						write(l, clock_counter);\n"
					"						writeline(output, l);\n",
					"					end if;\n"
					"\n"
					"				end if;  -- Input vector index\n"
					"\n",
					NULL
				);
			}

			fprintfm(F,
				"				-- Increment data index\n"
				"				%s_data_idx <= %s_data_idx + 1;\n", n, n,
				"\n"
				"			end if;  -- FIFO handshake\n"
				"		end if;  -- Clock edge\n"
				"\n"
				"	end process;\n",
				NULL
			);

		}  // Input FIFO

		else if(access->model==NETLIST_ACCESS_FIFO_OUT) {
			netlist_access_fifo_t* fifo_data = access->data;

			fprintfm(F,
				"	-- FIFO %s\n", n,
				"	-- Checking outputs\n"
				"\n"
				"	-- Always enable output FIFO\n"
				"	%s_ack <= \'1\' when reset = \'0\' else \'0\';\n", n,
				"\n",
				NULL
			);

			if(tb_fifo!=NULL && tb_fifo->file_name!=NULL) {
				fprintfm(F,
					"	%s_vector <= %s_vectors(%s_data_idx) when %s_data_idx < %s_vectors_nb else (others => \'0\');\n", n, n, n, n, n,
					"\n",
					NULL
				);
			}

			fprintfm(F,
				"	-- Get outputs\n"
				"	process (clock)\n"
				"		variable l : line;\n"
				"	begin\n"
				"\n"
				"		if %s(clock) then\n", clkedge_testfunc,
				"			if %s_rdy = \'1\' and %s_ack = \'1\' and reset = \'0\' then\n", n, n,
				"\n"
					"				write(l, string\'(\"INFO Fifo %s: Output \"));\n", n,
					"				write(l, %s_data_idx);\n", n,
					"				write(l, string\'(\" value \"));\n"
					"				write(l, to_string(%s_data));\n", n,
					"				write(l, string\'(\" at cycle \"));\n"
					"				write(l, clock_counter);\n"
					"				writeline(output, l);\n"
				"\n",
				NULL
			);

			if(tb_fifo!=NULL && tb_fifo->file_name!=NULL) {
				fprintfm(F,
					"				if %s_data_idx < %s_vectors_nb then\n", n, n,
					"					-- Check output value with test vectors\n"
					"\n"
					"					if %s_data /= %s_vector(%u downto 0) then\n", n, n, fifo_data->width-1,
					"						-- An error is detected\n"
					"\n"
					"						write(l, string\'(\"ERROR Expected \"));\n"
					"						write(l, to_string(%s_vector(%u downto 0)));\n", n, fifo_data->width-1,
					"						writeline(output, l);\n"
					"\n"
					"						%s_errors <= %s_errors + 1;\n", n, n,
					"\n"
					"						--report \"ERROR A simulation error was found.\" severity failure;\n"
					"\n"
					"					end if;\n"
					"\n"
					"					if %s_data_idx = %s_vectors_nb - 1 then\n", n, n,
					"\n"
					"						write(l, string\'(\"INFO Fifo %s: Last output vector read at cycle \"));\n", n,
					"						write(l, clock_counter);\n"
					"						writeline(output, l);\n"
					"\n",
					NULL
				);
				if(tb_fifo->stop_end_vectors==true) {
					fprintfm(F,
						"						write(l, string\'(\"INFO Fifo %s: Stopping simulation because end of test vectors reached at cycle \"));\n", n,
						"						write(l, clock_counter);\n"
						"						writeline(output, l);\n"
						"						%s_wantstop <= true;\n", n,
						"\n",
						NULL
					);
				}
				fprintf(F,
					"					end if;\n"
					"\n"
				);
				if(tb_fifo->stop_nb > 0) {
					fprintfm(F,
						"					if %s_data_idx = %u then\n", n, tb_fifo->stop_nb - 1,
						"\n"
						"						write(l, string\'(\"INFO Fifo %s: Stopping simulation because max number of outputs reached at cycle \"));\n", n,
						"						write(l, clock_counter);\n"
						"						writeline(output, l);\n"
						"						%s_wantstop <= true;\n", n,
						"\n"
						"					end if;\n"
						"\n",
						NULL
					);
				}
				fprintfm(F,
					"				else\n"
					"					-- All vectors have been read\n"
					"\n"
					"					write(l, string\'(\"WARNING This is out of test vectors\"));\n"
					"					writeline(output, l);\n"
					"\n"
					"				end if;  -- Check with test vectors\n"
					"\n",
					NULL
				);
			}

			fprintfm(F,
				"				-- Increment data index\n"
				"				%s_data_idx <= %s_data_idx + 1;\n", n, n,
				"\n"
				"			end if;  -- FIFO handshake\n"
				"		end if;  -- Clock edge\n"
				"\n"
				"	end process;\n",
				NULL
			);

		}  // Output FIFO

		// Unknown access functionality
		else {
			fprintf(F, "	-- Warning: This access is not handled\n");
			avl_pp_foreach(&Implem->netlist.top->ports, scanPort) {
				netlist_port_t* port = scanPort->data;
				if(port->access!=access) continue;
				if(port->direction==NETLIST_PORT_DIR_IN) {
					fprintf(F, "	wrap_%s <= ", port->name);
					Netlist_GenVHDL_StdLogic_Literal_Repeat_be(F, port->width, '0', NULL, ";\n");
				}
				else {
					fprintf(F, "	-- Note: No VHDL generated for output port '%s'\n", port->name);
				}
			}
		}

		fputc('\n', F);
	}  // Loop on each top-level access

	// Dummy functionality for top-level ports that are not part of an access
	nb_ports_noacc = 0;
	avl_pp_foreach(&Implem->netlist.top->ports, scanPort) {
		netlist_port_t* port = scanPort->data;
		if(port->access!=NULL) continue;
		if(nb_ports_noacc==0) fprintf(F, "	-- Top-level ports with unknown functionality\n");
		nb_ports_noacc ++;
		if(port->direction==NETLIST_PORT_DIR_IN) {
			fprintf(F, "	wrap_%s <= ", port->name);
			Netlist_GenVHDL_StdLogic_Literal_Repeat_be(F, port->width, '0', NULL, ";\n");
		}
		else {
			fprintf(F, "	-- Note: No VHDL generated for output port '%s'\n", port->name);
		}
	}  // Process top-level ports with no access

	// End of the testbench

	fprintf(F,
		"end augh;\n"
		"\n"
	);

	END_POINT:

	if(F!=NULL) fclose(F);
	Netlist_GenParams_Clear(&params);

	return error_code;
}

// Identify all FIFOs
int Netlist_TB_IdentifyFifos(implem_t* Implem, chain_list* list_fifos) {
	tb_fifo_t* tb_fifo = NULL;

	// Identify the input FIFO
	tb_fifo = TB_FifoData_List_FindTheIn(list_fifos);
	if(tb_fifo!=NULL) {
		netlist_access_t* access_found = NULL;
		avl_pp_foreach(&Implem->netlist.top->accesses, scanAccess) {
			netlist_access_t* access = scanAccess->data;
			if(access->model!=NETLIST_ACCESS_FIFO_IN) continue;
			if(access_found!=NULL) {
				printf("ERROR: Multiple input FIFOs are found. Expected only one.\n");
				return -1;
			}
			access_found = access;
		}
		if(access_found==NULL) {
			printf("ERROR: The main input FIFO was not found.\n");
			return -1;
		}
		if(TB_FifoData_List_FindName(list_fifos, access_found->name)!=NULL) {
			printf("ERROR: The main input FIFO is '%s' and it is already specified.\n", access_found->name);
			return -1;
		}
		tb_fifo->channel_name = access_found->name;
	}
	// Identify the output FIFO
	tb_fifo = TB_FifoData_List_FindTheOut(list_fifos);
	if(tb_fifo!=NULL) {
		netlist_access_t* access_found = NULL;
		avl_pp_foreach(&Implem->netlist.top->accesses, scanAccess) {
			netlist_access_t* access = scanAccess->data;
			if(access->model!=NETLIST_ACCESS_FIFO_OUT) continue;
			if(access_found!=NULL) {
				printf("ERROR: Multiple output FIFOs are found. Expected only one.\n");
				return -1;
			}
			access_found = access;
		}
		if(access_found==NULL) {
			printf("ERROR: The main output FIFO was not found.\n");
			return -1;
		}
		if(TB_FifoData_List_FindName(list_fifos, access_found->name)!=NULL) {
			printf("ERROR: The main output FIFO is '%s' and it is already specified.\n", access_found->name);
			return -1;
		}
		tb_fifo->channel_name = access_found->name;
	}

	// Identify all FIFO accesses
	foreach(list_fifos, scan) {
		tb_fifo = scan->DATA;
		tb_fifo->access = Netlist_Comp_GetAccess(Implem->netlist.top, tb_fifo->channel_name);
		if(tb_fifo->access==NULL) {
			printf("ERROR: The fifo named '%s' was not found.\n", tb_fifo->channel_name);
			return -1;
		}
		if(tb_fifo->access->model!=NETLIST_ACCESS_FIFO_IN && tb_fifo->access->model!=NETLIST_ACCESS_FIFO_OUT) {
			printf("ERROR: The name '%s' is model '%s', expected fifo.\n", tb_fifo->channel_name, tb_fifo->access->model->name);
			return -1;
		}
		if(tb_fifo->file_name==NULL) {
			printf("ERROR: No file was specified for fifo '%s'.\n", tb_fifo->channel_name);
			return -1;
		}
		// Set all remaining fields
		netlist_access_fifo_t* fifo_data = tb_fifo->access->data;
		if(tb_fifo->style==TB_DATA_RAWBIN && tb_fifo->bytes_nb==0) {
			tb_fifo->bytes_nb = (fifo_data->width + 7) / 8;
		}
	}

	return 0;
};


