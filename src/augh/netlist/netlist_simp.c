
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#include "../auto/auto.h"
#include "netlist.h"
#include "netlist_comps.h"
#include "netlist_access.h"
#include "netlist_cmd.h"

#include "netlist_simp.h"



void Netlist_Simp_Init(netlist_simp_data_t* data) {
	avl_pi_init(&data->comps_protect);
	avl_p_init(&data->comps_todo);
	avl_p_init(&data->comps_todo_next);
	avl_pi_init(&data->stats_comps_del);
	data->comps_protect_unlinked = NULL;
}
void Netlist_Simp_Clear(netlist_simp_data_t* data) {
	avl_pi_reset(&data->comps_protect);
	avl_p_reset(&data->comps_todo);
	avl_p_reset(&data->comps_todo_next);
	avl_pi_reset(&data->stats_comps_del);
	freechain(data->comps_protect_unlinked);
}
void Netlist_Simp_ClearResults(netlist_simp_data_t* data) {
	avl_p_reset(&data->comps_todo_next);
	avl_pi_reset(&data->stats_comps_del);
	freechain(data->comps_protect_unlinked);
	data->comps_protect_unlinked = NULL;
}

void Netlist_Simp_Protect_AddFlag(netlist_simp_data_t* data, netlist_comp_t* comp, int flag) {
	avl_pi_node_t* pi_node = NULL;
	bool b = avl_pi_add(&data->comps_protect, comp, &pi_node);
	if(b==true) pi_node->data = 0;
	pi_node->data |= flag;
}
bool Netlist_Simp_Protect_ChkFlag(netlist_simp_data_t* data, netlist_comp_t* comp, int flag) {
	avl_pi_node_t* pi_node = NULL;
	bool b = avl_pi_find(&data->comps_protect, comp, &pi_node);
	if(b==false) return false;
	if( (pi_node->data & flag) != 0) return true;
	return false;
}

// Note: The todo list in data has to be filled before launching this function
void Netlist_Simp(netlist_simp_data_t* data) {

	// Paranoia
	Netlist_Simp_ClearResults(data);

	// Loop as long as there are things to do
	do {
		if(avl_p_isempty(&data->comps_todo)==true) break;

		if(simp_verbose==true) {
			printf("Info: Beginning simplification iteration...\n");
		}

		// Launch simplification function on all components to do
		while(avl_p_isempty(&data->comps_todo)==false) {
			netlist_comp_t* comp = avl_p_deepest_data_rem(&data->comps_todo);
			if(Netlist_Simp_Protect_ChkFlag(data, comp, NETLIST_SIMP_PROTECT_NOSIMP)==true) continue;
			// Launch the simplification
			if(comp->model->func_simp!=NULL) {
				comp->model->func_simp(data, comp);
			}
			else {
				// Check if the entire component can be removed
				Netlist_Simp_DelComp_IfNoOutUsed(data, comp);
			}
		}

		// Swap trees
		avl_p_reset(&data->comps_todo);  // Paranoia
		Swap(data->comps_todo, data->comps_todo_next);

	} while(1);

	// Display some stats
	if(simp_verbose==true) {
		printf("Info: Removed components:");
		avl_pi_foreach(&data->stats_comps_del, scan) {
			char* model_name = scan->key;
			printf(" %s:%u", model_name, scan->data);
		}
		printf("\n");
	}
}



//======================================================
// Utility functions during simplification
//======================================================

void Netlist_Simp_PortOut_FlagTargetComps(netlist_simp_data_t* data, netlist_port_t* port) {
	if(port->target_ports==NULL) return;
	avl_p_foreach(port->target_ports, scanTarget) {
		netlist_port_t* target_port = scanTarget->data;
		Netlist_Simp_AddCompNext(data, target_port->component);
	}
}
void Netlist_Simp_PortIn_FlagSourceComps(netlist_simp_data_t* data, netlist_port_t* port) {
	foreach(port->source, scanVC) {
		netlist_val_cat_t* valcat = scanVC->DATA;
		if(valcat->source==NULL) continue;
		Netlist_Simp_AddCompNext(data, valcat->source->component);
	}
}
void Netlist_Simp_UnlinkPortIn(netlist_simp_data_t* data, netlist_port_t* port) {
	// Flag components for re-scan
	Netlist_Simp_PortIn_FlagSourceComps(data, port);
	Netlist_Simp_AddCompNext(data, port->component);
	// Clean unlink
	Netlist_PortTargets_RemSource(port);
	Netlist_ValCat_FreeList(port->source);
	port->source = NULL;
}
void Netlist_Simp_UnlinkPortOut(netlist_simp_data_t* data, netlist_port_t* port) {
	if(Netlist_PortTargets_CheckEmpty(port)==true) return;
	// Flag components for re-scan
	Netlist_Simp_AddCompNext(data, port->component);
	Netlist_Simp_PortOut_FlagTargetComps(data, port);
	// Clean unlink
	while(avl_p_isempty(port->target_ports)==false) {
		netlist_port_t* target_port = avl_p_deepest_data_rem(port->target_ports);
		Netlist_Simp_UnlinkPortIn(data, target_port);
	}
}
void Netlist_Simp_DelComp(netlist_simp_data_t* data, netlist_comp_t* comp) {
	if(simp_verbose==true) {
		printf("Info: Removing component '%s' model '%s'.\n", comp->name, comp->model->name);
	}

	// Unlink all ports, input and output
	avl_pp_foreach(&comp->ports, scanPort) {
		netlist_port_t* port = scanPort->data;
		if(port->direction==NETLIST_PORT_DIR_IN) {
			Netlist_Simp_UnlinkPortIn(data, port);
		}
		else if(port->direction==NETLIST_PORT_DIR_OUT) {
			Netlist_Simp_UnlinkPortOut(data, port);
		}
		else {
			abort();
		}
	}

	// Ensure the comp is no longer in the todo lists
	avl_p_rem_key(&data->comps_todo, comp);
	avl_p_rem_key(&data->comps_todo_next, comp);

	// Check if the comp is protected
	if(Netlist_Simp_Protect_ChkFlag(data, comp, NETLIST_SIMP_PROTECT_NODEL)==true) {
		// Protected component: add to a special list
		data->comps_protect_unlinked = addchain(data->comps_protect_unlinked, comp);
	}
	else {
		// Not protected
		// Increment the number of comps deleted for this model
		avl_pi_node_t* pi_node = NULL;
		bool b = avl_pi_add(&data->stats_comps_del, comp->model->name, &pi_node);
		if(b==true) pi_node->data = 0;
		pi_node->data ++;
		// Delete the component
		Netlist_Comp_Free(comp);
	}

}
bool Netlist_Simp_DelComp_IfNoOutUsed(netlist_simp_data_t* data, netlist_comp_t* comp) {
	// Check if output ports used
	avl_pp_foreach(&comp->ports, scanPort) {
		netlist_port_t* port = scanPort->data;
		if(port->direction==NETLIST_PORT_DIR_OUT) {
			if(Netlist_PortTargets_CheckEmpty(port)==false) return false;
		}
	}
	Netlist_Simp_DelComp(data, comp);
	return true;
}

__attribute__((__unused__))
static void Netlist_Simp_RemUnusedPort_PrintInfo(netlist_comp_t* comp, netlist_port_t* port) {
	printf("Info: Component '%s' type '%s': Removing unused port '%s' dir %s\n",
		comp->name, comp->model->name, port->name, port->direction==NETLIST_PORT_DIR_IN ? "IN" : "OUT"
	);
}

static void Netlist_Simp_pPortIn_CheckFree(netlist_port_t** pport) {
	netlist_port_t* port = *pport;
	if(port==NULL) return;
	if(port->source!=NULL) return;
	Netlist_Port_Free(port);
	*pport = NULL;
}
static void Netlist_Simp_pPortOut_CheckFree(netlist_port_t** pport) {
	netlist_port_t* port = *pport;
	if(port==NULL) return;
	if(Netlist_PortTargets_CheckEmpty(port)==false) return;
	Netlist_Port_Free(port);
	*pport = NULL;
}
static void Netlist_Simp_pPortIn_UnlinkFree(netlist_simp_data_t* data, netlist_port_t** pport) {
	netlist_port_t* port = *pport;
	if(port==NULL) return;
	Netlist_Simp_UnlinkPortIn(data, port);
	Netlist_Port_Free(port);
	*pport = NULL;
}
static void Netlist_Simp_pPortOut_UnlinkFree(netlist_simp_data_t* data, netlist_port_t** pport) {
	netlist_port_t* port = *pport;
	if(port==NULL) return;
	Netlist_Simp_UnlinkPortOut(data, port);
	Netlist_Port_Free(port);
	*pport = NULL;
}

typedef struct netlist_shrink_data_t {
	netlist_port_t* ref_port;
	unsigned max_left;
	unsigned min_right;
	unsigned crop_left;
	unsigned crop_right;
	bool     todo;
	unsigned prev_width;
	unsigned new_width;
} netlist_shrink_data_t;

static void Netlist_Simp_Shrink_DataInit(netlist_shrink_data_t* shrink_data, netlist_port_t* port) {
	#if 0  // For debug/development: disable component shrinking
	shrink_data->todo = false;
	return;
	#endif
	shrink_data->ref_port = port;
	// Find the leftmost and rightmost indexes used
	// If shorter than the register width, shrink the register
	shrink_data->min_right = 0;
	shrink_data->max_left = 0;
	bool found = Netlist_PortTargets_GetMaxIndexes(port, &shrink_data->max_left, &shrink_data->min_right);
	assert(found==true);
	assert(shrink_data->max_left <= port->width - 1);

	shrink_data->crop_right = shrink_data->min_right;
	shrink_data->crop_left = port->width - shrink_data->max_left - 1;
	if(shrink_data->crop_right==0 && shrink_data->crop_left==0) {
		// Nothing to shrink
		shrink_data->todo = false;
		return;
	}
	// There is some shrinking to do!
	shrink_data->prev_width = port->width;
	shrink_data->new_width = shrink_data->max_left - shrink_data->min_right + 1;
	shrink_data->todo = true;
}
static void Netlist_Simp_Shrink_Disp(netlist_shrink_data_t* shrink_data, netlist_comp_t* comp) {
	printf("Info: Shrinking comp '%s' model '%s': L:%u R:%u W:%u->%u\n",
		comp->name, comp->model->name,
		shrink_data->crop_left, shrink_data->crop_right,
		shrink_data->prev_width, shrink_data->new_width
	);
}
static void Netlist_Simp_PortIn_Shrink(netlist_simp_data_t* data, netlist_port_t* port, unsigned crop_left, unsigned crop_right) {
	// Flag the source of the port, for re-scan
	Netlist_Simp_PortIn_FlagSourceComps(data, port);
	// Crop the input ValCat list
	Netlist_PortTargets_RemSource(port);
	port->source = Netlist_ValCat_Crop(port->source, crop_left, crop_right);
	Netlist_PortTargets_SetSource(port);
	// Set the new width
	port->width -= crop_left + crop_right;
}
// IMPORTANT: This function assumes that the shrinked bits are not read
static void Netlist_Simp_PortOut_Shrink(netlist_simp_data_t* data, netlist_port_t* port, unsigned crop_left, unsigned crop_right) {
	// Note: No need to flag target components re-scan
	if(crop_right>0 && port->target_ports!=NULL) {
		avl_p_foreach(port->target_ports, scanTarget) {
			netlist_port_t* target_port = scanTarget->data;
			foreach(target_port->source, scanValCat) {
				netlist_val_cat_t* valcat = scanValCat->DATA;
				if(valcat->source!=port) continue;
				assert(valcat->right+1 >= crop_right);
				valcat->right -= crop_right;
				valcat->left -= crop_right;
			}
		}
	}
	// Set the new width
	port->width -= crop_left + crop_right;
}



//======================================================
// All simplification functions, per component model
//======================================================

void Netlist_Simp_Comp_Sig(netlist_simp_data_t* data, netlist_comp_t* comp) {
	netlist_signal_t* sig_data = comp->data;

	// If the output is not used, the component can be entirely deleted
	if(Netlist_PortTargets_CheckEmpty(sig_data->port_out)==true) {
		Netlist_Simp_DelComp(data, comp);
		return;
	}

	// Shrink the component
	netlist_shrink_data_t shrink_data_i;
	netlist_shrink_data_t* shrink_data = &shrink_data_i;

	Netlist_Simp_Shrink_DataInit(shrink_data, sig_data->port_out);
	if(shrink_data->todo==true) {

		if(simp_verbose==true) {
			Netlist_Simp_Shrink_Disp(shrink_data, comp);
		}

		// Shrink the input port
		Netlist_Simp_PortIn_Shrink(data, sig_data->port_in, shrink_data->crop_left, shrink_data->crop_right);

		// Shift all references to the target port if necessary
		Netlist_Simp_PortOut_Shrink(data, sig_data->port_out, shrink_data->crop_left, shrink_data->crop_right);

		// Shrink the register
		sig_data->width = shrink_data->new_width;
	}
}

void Netlist_Simp_Comp_Reg(netlist_simp_data_t* data, netlist_comp_t* comp) {
	netlist_register_t* reg_data = comp->data;

	// If the output is not used, the component can be entirely deleted
	if(Netlist_PortTargets_CheckEmpty(reg_data->port_out)==true) {
		Netlist_Simp_DelComp(data, comp);
		return;
	}

	// If there is no init value, the reset port can be removed
	if(reg_data->init_value==NULL) {
		if(reg_data->port_reset!=NULL) {
			Netlist_Simp_UnlinkPortIn(data, reg_data->port_reset);
			Netlist_Access_Reset_Rem(reg_data->port_reset->access);
			reg_data->port_reset = NULL;
		}
	}

	// If the source WE is literal '1', remove the WE port
	if(reg_data->port_en!=NULL) {
		if(reg_data->port_en->source==NULL) {
			Netlist_Simp_pPortIn_UnlinkFree(data, &reg_data->port_en);
		}
		else {
			netlist_val_cat_t* valcat = reg_data->port_en->source->DATA;
			if(valcat->literal!=NULL) {
				if(valcat->literal[0]=='0') {
					// The Reg never updates. Replace by the reset value, if any.
					Netlist_Simp_PortOut_FlagTargetComps(data, reg_data->port_out);
					if(reg_data->init_value!=NULL) {
						// Replace by the reset value
						Netlist_PortTargets_ReplaceOutByLit(reg_data->port_out, reg_data->init_value);
					}
					else {
						// Replace by all '0' by default
						Netlist_PortTargets_ReplaceOutByLit_Repeat1Bit(reg_data->port_out, '0');
					}
					// Remove the component
					Netlist_Simp_DelComp(data, comp);
					return;
				}
				else if(valcat->literal[0]=='1') {
					Netlist_Simp_pPortIn_UnlinkFree(data, &reg_data->port_en);
				}
			}
		}
	}

	// Shrink the component to usage of output
	netlist_shrink_data_t shrink_data_i;
	netlist_shrink_data_t* shrink_data = &shrink_data_i;

	Netlist_Simp_Shrink_DataInit(shrink_data, reg_data->port_out);
	if(shrink_data->todo==true) {

		if(simp_verbose==true) {
			Netlist_Simp_Shrink_Disp(shrink_data, comp);
		}

		// Shrink the input port
		Netlist_Simp_PortIn_Shrink(data, reg_data->port_in, shrink_data->crop_left, shrink_data->crop_right);

		// Shift all references to the target port if necessary
		Netlist_Simp_PortOut_Shrink(data, reg_data->port_out, shrink_data->crop_left, shrink_data->crop_right);

		// Shrink the init value
		if(reg_data->init_value!=NULL) {
			char buf[shrink_data->new_width + 1];
			memcpy(buf, reg_data->init_value + shrink_data->crop_left, shrink_data->new_width * sizeof(*buf));
			buf[shrink_data->new_width] = 0;
			reg_data->init_value = namealloc(buf);
		}

		// Shrink the component
		reg_data->width = shrink_data->new_width;
	}

	// Shrink the component where the input is literal (handle only '0' for now)
	unsigned bits_nb = 0;
	char buf[reg_data->width+1];
	foreach(reg_data->port_in->source, scanValCat) {
		netlist_val_cat_t* valcat = scanValCat->DATA;
		if(valcat->literal==NULL) break;
		if(valcat->left==valcat->right) {
			for(unsigned i=0; i<valcat->width; i++) buf[bits_nb++] = valcat->literal[0];
		}
		else {
			for(unsigned i=0; i<valcat->width; i++) buf[bits_nb++] = valcat->literal[i];
		}
	}  // Scan all source ValCat structures
	buf[bits_nb] = 0;
	// Verify that this is all compatible with the init value
	if(reg_data->init_value!=NULL) {
		for(unsigned i=0; i<bits_nb; i++) if(reg_data->init_value[i] != buf[i]) { bits_nb = i; break; }
		buf[bits_nb] = 0;
	}

	// If the entire register has constant literal value, replace it
	if(bits_nb==reg_data->width) {

		if(simp_verbose==true) {
			//Netlist_ValCat_DebugPrintList(reg_data->port_in->source);
			printf("Info: Comp '%s' model '%s' is replaced by the constant '%s'\n",
				comp->name, comp->model->name, buf
			);
		}

		// Flag components for re-scan
		Netlist_Simp_PortOut_FlagTargetComps(data, reg_data->port_out);

		// Replace all references to the reg output by the literal
		Netlist_PortTargets_ReplaceOutByLit(reg_data->port_out, namealloc(buf));

		// Remove the component
		Netlist_Simp_DelComp(data, comp);
		return;
	}

	// Shrink if some literal bits are found
	if(bits_nb>0) {

		if(simp_verbose==true) {
			printf("Info: Replacing %u left bits of comp '%s' model '%s' by the constant '%s'\n",
				bits_nb, comp->name, comp->model->name, buf
			);
		}

		// Flag components for re-scan
		Netlist_Simp_PortOut_FlagTargetComps(data, reg_data->port_out);

		// Replace all references to the reg output, these bits only, by the literal
		chain_list* new_list = Netlist_ValCat_MakeList_Literal(namealloc(buf));
		new_list = append(new_list,
			Netlist_ValCat_Crop(Netlist_ValCat_MakeList_FullPort(reg_data->port_out), bits_nb, 0)
		);
		Netlist_PortTargets_ReplaceOutByValcat_List(reg_data->port_out, new_list);
		Netlist_ValCat_FreeList(new_list);

		// Shrink the input port
		Netlist_Simp_PortIn_Shrink(data, reg_data->port_in, bits_nb, 0);

		// Shrink the component
		reg_data->port_out->width -= bits_nb;
		reg_data->width -= bits_nb;

		// Shrink the init value
		if(reg_data->init_value!=NULL) {
			reg_data->init_value = namealloc(reg_data->init_value + bits_nb);
		}

		// Flag the current component for re-scan
		Netlist_Simp_AddCompNext(data, comp);
	}

}

void Netlist_Simp_Comp_Mux(netlist_simp_data_t* data, netlist_comp_t* comp) {
	netlist_mux_t* mux_data = comp->data;

	// If the output is not used, the component can be entirely deleted
	if(Netlist_PortTargets_CheckEmpty(mux_data->port_out)==true) {
		Netlist_Simp_DelComp(data, comp);
		return;
	}

	// If there is a default input access, which is literal: set it as internal default value
	if(mux_data->default_input!=NULL) {
		netlist_mux_access_t* muxIn_data = mux_data->default_input->data;
		bool is_lit = true;
		foreach(muxIn_data->port_data->source, scanValCat) {
			netlist_val_cat_t* valcat = scanValCat->DATA;
			if(valcat->literal==NULL) { is_lit = false; break; }
		}
		if(is_lit==true) {
			// Build the flattened literal value
			char buf[mux_data->width + 1];
			unsigned index = 0;
			foreach(muxIn_data->port_data->source, scanValCat) {
				netlist_val_cat_t* valcat = scanValCat->DATA;
				if(valcat->left!=valcat->right) for(unsigned i=0; i<valcat->width; i++) buf[index++] = valcat->literal[i];
				else for(unsigned i=0; i<valcat->width; i++) buf[index++] = valcat->literal[0];
			}
			buf[mux_data->width] = 0;
			// Set the default value
			mux_data->default_value = namealloc(buf);
			// Remove this MUX input
			Netlist_Mux_RemoveInput(mux_data->default_input);
		}
	}

	// If there is no default value: remove the input whose data is all '0'
	if(mux_data->default_value==NULL && mux_data->default_input==NULL) {
		avl_pp_foreach(&comp->accesses, scanAccess) {
			netlist_access_t* access = scanAccess->data;
			netlist_mux_access_t* muxIn_data = access->data;
			if(Netlist_ValCat_ListIsAllBit(muxIn_data->port_data->source, '0')==false) continue;
			// Unlink the EN port from the other components
			if(muxIn_data->port_en!=NULL) {
				Netlist_Simp_UnlinkPortIn(data, muxIn_data->port_en);
			}
			// Remove this MUX input
			Netlist_Mux_RemoveInput(access);
			// Set the default value to all '0'
			char buf[mux_data->width + 1];
			for(unsigned i=0; i<mux_data->width; i++) buf[i] = '0';
			buf[mux_data->width] = 0;
			mux_data->default_value = namealloc(buf);
			break;
		}  // Scan input accesses
	}

	// Empty MUX can be created before beginning mapping.
	// So empty mux are a problem only if they are not the only source of a target port
	if(mux_data->inputs_nb==0 && mux_data->default_value==NULL) {

		#if 0
		printf("INTERNAL ERROR %s:%d : Empty Mux '%s'.\n", __FILE__, __LINE__, comp->name);
		if(Netlist_PortTargets_CheckEmpty(mux_data->port_out)==true) {
			printf("  Its output seems floating.\n");
		}
		else {
			avl_p_foreach(mux_data->port_out->target_ports, scanTarget) {
				netlist_port_t* port = scanTarget->data;
				printf("  Its output is linked to component %s '%s' port '%s'.\n",
					port->component->model->name, port->component->name, port->name
				);
			}
		}
		abort();
		#endif

		Netlist_Simp_DelComp(data, comp);
		return;
	}

	// Only a default value => remove the MUX, replace the output by the value
	if(mux_data->inputs_nb==0 && mux_data->default_value!=NULL) {
		// Update the connections to target components
		Netlist_Simp_PortOut_FlagTargetComps(data, mux_data->port_out);
		Netlist_PortTargets_ReplaceOutByLit(mux_data->port_out, mux_data->default_value);
		// Remove the MUX
		Netlist_Simp_DelComp(data, comp);
		return;
	}

	// One input, no default value => Bypass the MUX and remove it
	// The input can also be the default input
	if(mux_data->inputs_nb==1 && (mux_data->default_input!=NULL || mux_data->default_value==NULL)) {
		// Update the connections to target components
		netlist_access_t* access = avl_pp_root_data(&comp->accesses);
		netlist_mux_access_t* muxIn_data = access->data;
		Netlist_Simp_PortIn_FlagSourceComps(data, muxIn_data->port_data);
		if(muxIn_data->port_data!=NULL) Netlist_Simp_PortOut_FlagTargetComps(data, muxIn_data->port_data);
		Netlist_Simp_PortOut_FlagTargetComps(data, mux_data->port_out);
		Netlist_PortTargets_ReplaceOutByValcat_List(mux_data->port_out, muxIn_data->port_data->source);
		// Remove the MUX
		Netlist_Simp_DelComp(data, comp);
		return;
	}

	// One input at '1' or '-', and a default value='0' or '-'
	// Bypass the MUX, replace by only the Enable input
	if(mux_data->inputs_nb==1 && mux_data->default_value!=NULL) {
		netlist_access_t* access = avl_pp_root_data(&comp->accesses);
		netlist_mux_access_t* muxIn_data = access->data;
		if(Netlist_Literal_IsNoBit(mux_data->default_value, '1')==true &&
			Netlist_ValCat_ListIsLitNoBit(muxIn_data->port_data->source, '0')
		) {
			// Get the Enable source
			netlist_access_t* access = avl_pp_root_data(&comp->accesses);
			netlist_mux_access_t* muxIn_data = access->data;
			assert(muxIn_data->port_en->source!=NULL);
			netlist_val_cat_t* valcat_en = muxIn_data->port_en->source->DATA;
			// Update the connections to target components
			assert(valcat_en->source!=NULL);
			Netlist_Simp_PortIn_FlagSourceComps(data, muxIn_data->port_en);
			Netlist_Simp_PortOut_FlagTargetComps(data, mux_data->port_out);
			Netlist_PortTargets_ReplaceOutByValcat_Repeat1Bit(mux_data->port_out, valcat_en);
			// Remove the MUX
			Netlist_Simp_DelComp(data, comp);
			return;
		}
	}

	// Remove EN port of default input
	if(mux_data->default_input!=NULL) {
		netlist_mux_access_t* muxIn_data = mux_data->default_input->data;
		if(muxIn_data->port_en!=NULL) {
			Netlist_Simp_PortIn_FlagSourceComps(data, muxIn_data->port_en);
			Netlist_Simp_pPortIn_UnlinkFree(data, &muxIn_data->port_en);
		}
	}

	// Shrink the component to only what's needed
	netlist_shrink_data_t shrink_data_i;
	netlist_shrink_data_t* shrink_data = &shrink_data_i;

	Netlist_Simp_Shrink_DataInit(shrink_data, mux_data->port_out);
	if(shrink_data->todo==true) {

		if(simp_verbose==true) {
			Netlist_Simp_Shrink_Disp(shrink_data, comp);
		}

		// Shrink the input ports
		avl_pp_foreach(&comp->accesses, scanAccess) {
			netlist_access_t* access = scanAccess->data;
			netlist_mux_access_t* muxIn_data = access->data;
			Netlist_Simp_PortIn_Shrink(data, muxIn_data->port_data, shrink_data->crop_left, shrink_data->crop_right);
		}

		// Crop the default value
		if(mux_data->default_value!=NULL) {
			char buf[shrink_data->new_width + 1];
			memcpy(buf, mux_data->default_value + shrink_data->crop_left, shrink_data->new_width * sizeof(*buf));
			buf[shrink_data->new_width] = 0;
			mux_data->default_value = namealloc(buf);
		}

		// Shift all references to the target port if necessary
		Netlist_Simp_PortOut_Shrink(data, mux_data->port_out, shrink_data->crop_left, shrink_data->crop_right);

		// Shrink the Mux
		mux_data->port_out->width = shrink_data->new_width;
		mux_data->width = shrink_data->new_width;
	}

	// Shrink the component where all inputs are the same literal
	unsigned bits_nb = 0;
	char buf[mux_data->width + 1];
	// Read the default value
	if(mux_data->default_value!=NULL) {
		strcpy(buf, mux_data->default_value);
		bits_nb = mux_data->width;
	}
	// Read the inputs
	avl_pp_foreach(&comp->accesses, scanAccess) {
		netlist_access_t* access = scanAccess->data;
		netlist_mux_access_t* muxIn_data = access->data;
		// Scan the ValCat source
		unsigned idx = 0;
		foreach(muxIn_data->port_data->source, scanValCat) {
			netlist_val_cat_t* valcat = scanValCat->DATA;
			if(valcat->literal==NULL) break;
			bool earlystop = false;
			for(unsigned i=0; i<valcat->width; i++) {
				char c = valcat->literal[ valcat->left==valcat->right ? 0 : i ];
				if(bits_nb==0) buf[idx++] = c;
				else {
					if(c != buf[idx]) { earlystop = true; break; }
					idx ++;
					if(idx>=bits_nb) { earlystop = true; break; }
				}
			}
			if(earlystop==true) break;
		}  // Scan all source ValCat structures
		if(bits_nb==0 || idx<bits_nb) bits_nb = idx;
		if(bits_nb==0) break;
	}  // Scan Mux inputs
	buf[bits_nb] = 0;

	// If the entire output has constant literal value, replace the component
	if(bits_nb==mux_data->width) {

		if(simp_verbose==true) {
			printf("Info: Comp '%s' model '%s' is replaced by the constant '%s'\n",
				comp->name, comp->model->name, buf
			);
		}

		// Flag components for re-scan
		Netlist_Simp_PortOut_FlagTargetComps(data, mux_data->port_out);

		// Replace all references to the output, these bits only, by the literal
		Netlist_PortTargets_ReplaceOutByLit(mux_data->port_out, namealloc(buf));

		// Remove the component
		Netlist_Simp_DelComp(data, comp);
		return;
	}

	// Shrink if some literal bits are found
	if(bits_nb>0) {

		if(simp_verbose==true) {
			printf("Info: Replacing %u left bits of comp '%s' model '%s' by the constant '%s'\n",
				bits_nb, comp->name, comp->model->name, buf
			);
		}

		// Flag components for re-scan
		Netlist_Simp_PortOut_FlagTargetComps(data, mux_data->port_out);

		// Replace all references to the mux output, these bits only, by the literal
		chain_list* new_list = Netlist_ValCat_MakeList_Literal(namealloc(buf));
		new_list = append(new_list,
			Netlist_ValCat_Crop(Netlist_ValCat_MakeList_FullPort(mux_data->port_out), bits_nb, 0)
		);
		Netlist_PortTargets_ReplaceOutByValcat_List(mux_data->port_out, new_list);
		Netlist_ValCat_FreeList(new_list);

		// Shrink the default value
		if(mux_data->default_value!=NULL) {
			mux_data->default_value = namealloc(mux_data->default_value + bits_nb);
		}

		// Shrink the input ports
		avl_pp_foreach(&comp->accesses, scanAccess) {
			netlist_access_t* access = scanAccess->data;
			netlist_mux_access_t* muxIn_data = access->data;
			Netlist_Simp_PortIn_Shrink(data, muxIn_data->port_data, bits_nb, 0);
		}

		// Shrink the component
		mux_data->port_out->width -= bits_nb;
		mux_data->width -= bits_nb;

		// Flag the current component for re-scan
		Netlist_Simp_AddCompNext(data, comp);
	}

	// Find common left part on all inputs and extract that from the MUX
	// FIXME There is code to merge with the previous cropping functionality
	// FIXME Also detect common part from the right
	if(mux_data->default_value==NULL) {
		netlist_val_cat_t common_left;

		bool first = true;
		bool found = true;
		avl_pp_foreach(&comp->accesses, scanAccess) {
			netlist_access_t* access = scanAccess->data;
			netlist_mux_access_t* muxIn_data = access->data;
			netlist_val_cat_t* valcat = muxIn_data->port_data->source->DATA;
			if(valcat->source==NULL) { found = false; break; }
			// Use the first occurrence as reference
			if(first==true) { common_left = *valcat; first = false; continue; }
			// Here we compare to the common valcat
			if(valcat->source != common_left.source) { found = false; break; }
			if(valcat->left != common_left.left) { found = false; break; }
			if(valcat->right != common_left.right) { found = false; break; }
		}

		// Replace the left part of the output by the common Valcat
		if(found==true) {

			chain_list* common_list = addchain(NULL, Netlist_ValCat_Dup(&common_left));

			// If the entire output has constant literal value, replace the component
			if(common_left.width == mux_data->width) {

				if(simp_verbose==true) {
					printf("Info: Comp '%s' model '%s' is replaced by the common operand\n",
						comp->name, comp->model->name
					);
				}

				// Flag components for re-scan
				Netlist_Simp_PortOut_FlagTargetComps(data, mux_data->port_out);

				// Replace all references to the output, these bits only, by the literal
				Netlist_PortTargets_ReplaceOutByValcat_List(mux_data->port_out, common_list);

				// Clean
				Netlist_ValCat_FreeList(common_list);

				// Remove the component
				Netlist_Simp_DelComp(data, comp);
				return;
			}

			// Here the Mux output is only partially replaced

			if(simp_verbose==true) {
				printf("Info: Replacing %u left bits of comp '%s' model '%s' by a common left input source\n",
					common_left.width, comp->name, comp->model->name
				);
			}

			// Flag components for re-scan
			Netlist_Simp_PortOut_FlagTargetComps(data, mux_data->port_out);

			// Replace all references to the mux output, these bits only, by the literal
			chain_list* new_list = append(common_list,
				Netlist_ValCat_Crop(Netlist_ValCat_MakeList_FullPort(mux_data->port_out), common_left.width, 0)
			);
			Netlist_PortTargets_ReplaceOutByValcat_List(mux_data->port_out, new_list);
			Netlist_ValCat_FreeList(new_list);

			// Shrink the input ports
			avl_pp_foreach(&comp->accesses, scanAccess) {
				netlist_access_t* access = scanAccess->data;
				netlist_mux_access_t* muxIn_data = access->data;
				Netlist_Simp_PortIn_Shrink(data, muxIn_data->port_data, common_left.width, 0);
			}

			// Shrink the component
			mux_data->port_out->width -= common_left.width;
			mux_data->width -= common_left.width;

			// Flag the current component for re-scan
			Netlist_Simp_AddCompNext(data, comp);

		}  // End of section where a common left part of the operands is extracted
	}

}

void Netlist_Simp_Comp_Fsm(netlist_simp_data_t* data, netlist_comp_t* comp) {
	netlist_fsm_t* fsm_data = comp->data;

	unsigned outputs_unused = 0;
	unsigned outputs_unbuf = 0;
	unsigned outputs_setdef = 0;
	unsigned outputs_always = 0;
	unsigned outputs_invert = 0;
	unsigned outputs_literal = 0;
	unsigned outputs_merge = 0;

	// Remove the output ports that drive nothing
	avl_pp_foreach(&fsm_data->output_values, scan) {
		netlist_fsm_outval_t* outval = scan->data;
		if(Netlist_PortTargets_CheckEmpty(outval->port)==false) continue;
		Netlist_Comp_Fsm_RemoveOutput(outval->port);
		outputs_unused ++;
	}

	// Un-buffer the outputs that are used in only one State
	avl_pp_foreach(&fsm_data->output_values, scan) {
		netlist_fsm_outval_t* outval = scan->data;
		// Only Actions on buffered outputs
		if(outval->is_buffered==false) continue;
		if(ChainList_Count(outval->actions)!=1) continue;
		// Only Actions that set '1' (to ensure there is no logic for generation of outputs)
		netlist_fsm_action_t* action = outval->actions->DATA;
		bool only1 = true;
		for(unsigned i=0; i<outval->port->width; i++) if(action->value[i]!='1') { only1 = false; break; }
		if(only1==false) continue;
		//dbgprintf("FSM simplify: Un-buffering output '%s'.\n", outval->port->name);
		outval->is_buffered = false;
		outputs_unbuf ++;
	}

	// Remove Actions that set the default value
	avl_pp_foreach(&fsm_data->output_values, scan) {
		netlist_fsm_outval_t* outval = scan->data;
		chain_list* list_del = NULL;
		foreach(outval->actions, scanAction) {
			netlist_fsm_action_t* action = scanAction->DATA;
			if(action->value==outval->default_val) list_del = addchain(list_del, action);
		}
		if(list_del==NULL) continue;
		foreach(list_del, scanAction) {
			Netlist_Comp_Fsm_RemoveAction(scanAction->DATA);
			outputs_setdef ++;
		}
		freechain(list_del);
	}

	// Invert the default value for the Actions that are set more than half the nb of States
	if(objective_human==true) {
		avl_pp_foreach(&fsm_data->output_values, scan) {
			netlist_fsm_outval_t* outval = scan->data;
			// Only 1-bit outputs, where there is a default value
			if(outval->port->width!=1) continue;
			if(outval->default_val==NULL) continue;
			// No flag only-last-cycle
			bool found_flag = false;
			foreach(outval->actions, scanAction) {
				netlist_fsm_action_t* action = scanAction->DATA;
				if(action->only_last_cycle==true) { found_flag = true; break; }
			}
			if(found_flag==true) continue;
			// Count the number of Actions
			unsigned actions_nb = ChainList_Count(outval->actions);
			if(actions_nb<=fsm_data->states_nb/2) continue;
			// This output will be inverted.
			// Flag the states where there already is an Action
			char* value_default = outval->default_val;
			char* value_action = ((netlist_fsm_action_t*)outval->actions->DATA)->value;
			avl_p_tree_t states_with_act;
			avl_p_init(&states_with_act);
			foreach(outval->actions, scanAction) {
				netlist_fsm_action_t* action = scanAction->DATA;
				avl_p_add_overwrite(&states_with_act, action->state, action->state);
			}
			// Remove existing Actions
			while(outval->actions!=NULL) {
				netlist_fsm_action_t* action = outval->actions->DATA;
				Netlist_Comp_Fsm_RemoveAction(action);
			}
			// Create new Actions
			foreach(fsm_data->states, scanState) {
				netlist_fsm_state_t* state = scanState->DATA;
				if(avl_p_isthere(&states_with_act, state)==true) continue;
				Netlist_Comp_Fsm_AddAction(comp, state, outval->port, value_default);
			}
			// Invert the default value
			outval->default_val = value_action;
			// Clean and count
			avl_p_reset(&states_with_act);
			outputs_invert ++;
		}
	}

	// For outputs that are set to a same value at all states, set that value as default and remove all Actions
	avl_pp_foreach(&fsm_data->output_values, scan) {
		netlist_fsm_outval_t* outval = scan->data;
		// No flag only-last-cycle
		bool found_flag = false;
		foreach(outval->actions, scanAction) {
			netlist_fsm_action_t* action = scanAction->DATA;
			if(action->only_last_cycle==true) { found_flag = true; break; }
		}
		if(found_flag==true) continue;
		// Count the number of Actions
		unsigned actions_nb = ChainList_Count(outval->actions);
		if(actions_nb!=fsm_data->states_nb) continue;
		// Check if the values in Actions are always the same
		netlist_fsm_action_t* action = outval->actions->DATA;
		char* value_action = action->value;
		bool all_same = true;
		foreach(outval->actions, scanAction) {
			action = scanAction->DATA;
			if(action->value!=value_action) { all_same = false; break; }
		}
		if(all_same==false) continue;
		// Remove all Actions
		while(outval->actions!=NULL) {
			netlist_fsm_action_t* action = outval->actions->DATA;
			Netlist_Comp_Fsm_RemoveAction(action);
		}
		// Set the default value to the Action value
		outval->default_val = value_action;
		outputs_always ++;
	}

	// Outputs that have no Action: remove the output port and replace by the default literal value
	chain_list* outval_to_del = NULL;
	avl_pp_foreach(&fsm_data->output_values, scan) {
		netlist_fsm_outval_t* outval = scan->data;
		if(outval->actions!=NULL) continue;
		assert(outval->default_val!=NULL);
		outval_to_del = addchain(outval_to_del, outval);
	}
	foreach(outval_to_del, scanOutval) {
		netlist_fsm_outval_t* outval = scanOutval->DATA;
		Netlist_Simp_PortOut_FlagTargetComps(data, outval->port);
		Netlist_PortTargets_ReplaceOutByLit(outval->port, outval->default_val);
		Netlist_Comp_Fsm_RemoveOutput(outval->port);
		outputs_literal ++;
	}
	freechain(outval_to_del);

	// Merge outputs that have same default value and same Actions
	// In this tree: key = Outval to merge and remove, data = Outval in which it is merged
	avl_pp_tree_t tree_merge_outval;
	avl_pp_init(&tree_merge_outval);
	// Scan all Outval
	avl_pp_foreach(&fsm_data->output_values, scan) {
		netlist_fsm_outval_t* outval = scan->data;
		// Skip already merged Outval
		if(avl_pp_isthere(&tree_merge_outval, outval)==true) continue;
		// Count the number of Actions
		unsigned actions_nb = ChainList_Count(outval->actions);
		if(actions_nb==0) continue;
		// To list the states where this outval has an Action: key=state, data=action
		avl_pp_tree_t tree_state2act;
		avl_pp_init(&tree_state2act);
		// Scan all states where there is an Action, keep where there is the min nb of Actions
		netlist_fsm_state_t* refstate = NULL;
		unsigned refstate_act_nb = 0;
		foreach(outval->actions, scanAction) {
			netlist_fsm_action_t* action = scanAction->DATA;
			avl_pp_add_overwrite(&tree_state2act, action->state, action);
			unsigned act_nb = ChainList_Count(action->state->actions);
			if(refstate==NULL || act_nb<refstate_act_nb) { refstate = action->state; refstate_act_nb = act_nb; }
		}
		assert(refstate!=NULL);
		// Scan all Actions of the ref state, list the compatible outval
		chain_list* outval_to_test = NULL;
		foreach(refstate->actions, scanAction) {
			netlist_fsm_action_t* action = scanAction->DATA;
			netlist_fsm_outval_t* loc_outval = action->outval;
			if(loc_outval==outval) continue;
			if(avl_pp_isthere(&tree_merge_outval, loc_outval)==true) continue;
			if(loc_outval->port->width!=outval->port->width) continue;
			if(loc_outval->default_val!=outval->default_val) continue;
			if(loc_outval->is_buffered!=outval->is_buffered) continue;
			unsigned act_nb = ChainList_Count(loc_outval->actions);
			if(act_nb!=actions_nb) continue;
			outval_to_test = addchain(outval_to_test, loc_outval);
		}
		// The candidate Outval has the right number of Actions. Compare them to the original Actions.
		foreach(outval_to_test, scanOutval) {
			netlist_fsm_outval_t* loc_outval = scanOutval->DATA;
			bool all_identical = true;
			foreach(loc_outval->actions, scanAction) {
				netlist_fsm_action_t* action = scanAction->DATA;
				netlist_fsm_action_t* orig_action = NULL;
				bool b = avl_pp_find_data(&tree_state2act, action->state, (void**)&orig_action);
				if(b==false) { all_identical = false; break; }
				assert(orig_action!=NULL);
				if( (action->only_last_cycle!=orig_action->only_last_cycle) ||
				    (action->value!=orig_action->value)
				) { all_identical = false; break; }
			}
			if(all_identical==false) continue;
			// Flag this Outval as a duplicate of the first Outval
			avl_pp_add_overwrite(&tree_merge_outval, loc_outval, outval);
		}
		// Clean
		avl_pp_reset(&tree_state2act);
		freechain(outval_to_test);
	}
	// Now do the merge of output ports
	avl_pp_foreach(&tree_merge_outval, scanOutval) {
		netlist_fsm_outval_t* outval = scanOutval->key;
		netlist_fsm_outval_t* dest_outval = scanOutval->data;
		Netlist_Simp_PortOut_FlagTargetComps(data, outval->port);
		Netlist_PortTargets_ReplaceOutByPort(outval->port, dest_outval->port);
		Netlist_Comp_Fsm_RemoveOutput(outval->port);
		outputs_merge ++;
	}
	avl_pp_reset(&tree_merge_outval);

	if(simp_verbose==true) {
		printfm("Info: FSM simplify outputs: "
			"unused=%u, ", outputs_unused,
			"unbuf=%u, ", outputs_unbuf,
			"setdef=%u, ", outputs_setdef,
			"invert=%u, ", outputs_invert,
			"always=%u, ", outputs_always,
			"literal=%u, ", outputs_literal,
			"merge=%u.\n", outputs_merge,
			NULL
		);
	}

	// Check if the entire component can be removed
	Netlist_Simp_DelComp_IfNoOutUsed(data, comp);
}

void Netlist_Simp_Comp_Asb(netlist_simp_data_t* data, netlist_comp_t* comp) {
	netlist_asb_t* asb_data = comp->data;

	// Simplify sign extension
	if(asb_data->sign_extend!=NULL) {
		netlist_asb_sign_t* asb_sign = asb_data->sign_extend;
		bool is_used = false;

		void check_free_port_out(netlist_port_t** pport) {
			netlist_port_t* port = *pport;
			if(port==NULL) return;
			if(Netlist_PortTargets_CheckEmpty(port)==false) { is_used = true; return; }
			Netlist_Port_Free(port);
			*pport = NULL;
		}

		check_free_port_out(&asb_sign->port_ov);
		check_free_port_out(&asb_sign->port_gt);
		check_free_port_out(&asb_sign->port_ge);
		check_free_port_out(&asb_sign->port_lt);
		check_free_port_out(&asb_sign->port_le);

		if(is_used==false) {
			if(simp_verbose==true) {
				printf("Info: Removing sign extension of component %s '%s'.\n", comp->model->name, comp->name);
			}

			void process_port_in(netlist_port_t* port) {
				if(port==NULL) return;
				Netlist_Simp_UnlinkPortIn(data, port);
				// Note: The port itself will be removed by the function that removes the sign extension
			}

			process_port_in(asb_sign->port_sign);
			process_port_in(asb_sign->port_extern_eq);

			Netlist_Asb_RemoveSign(comp);
		}
	}  // Process Sign extension

	// The Carry Out port
	Netlist_Simp_pPortOut_CheckFree(&asb_data->port_co);

	// Check if the entire component can be removed
	bool b = Netlist_Simp_DelComp_IfNoOutUsed(data, comp);
	if(b==true) return;

	// Remove the ci input if source is literal
	if(asb_data->port_ci!=NULL) {
		bool can_remove = false;
		char default_ci = '0';

		if(asb_data->port_ci->source==NULL) can_remove = true;
		else {
			netlist_val_cat_t* valcat = asb_data->port_ci->source->DATA;
			if(valcat->literal!=NULL) {
				can_remove = true;
				default_ci = valcat->literal[0];
			}
		}

		if(can_remove==true) {
			Netlist_Simp_pPortIn_UnlinkFree(data, &asb_data->port_ci);
			asb_data->default_ci = default_ci;
		}
	}

	// Shrink the component if possible
	if(asb_data->sign_extend==NULL && asb_data->port_co==NULL) {
		netlist_shrink_data_t shrink_data_i;
		netlist_shrink_data_t* shrink_data = &shrink_data_i;

		Netlist_Simp_Shrink_DataInit(shrink_data, asb_data->port_out);
		if(shrink_data->todo==true && shrink_data->crop_left > 0) {

			if(simp_verbose==true) {
				Netlist_Simp_Shrink_Disp(shrink_data, comp);
			}

			// Shrink the inputs, but don't touch right indexes
			Netlist_Simp_PortIn_Shrink(data, asb_data->port_inA, shrink_data->crop_left, 0);
			Netlist_Simp_PortIn_Shrink(data, asb_data->port_inB, shrink_data->crop_left, 0);

			// Shift all references to the target port if necessary
			Netlist_Simp_PortOut_Shrink(data, asb_data->port_out, shrink_data->crop_left, 0);

			// Shrink the component
			asb_data->width -= shrink_data->crop_left;
		}
	}
}

// TODO Add simp for MUL:
//   crop inputs if there are leading zeros (and unsigned)
//   replace output by zero if one input is zero
//   Crop output port according to width of input ports

void Netlist_Simp_Comp_DivQR(netlist_simp_data_t* data, netlist_comp_t* comp) {
	netlist_divqr_t* div_data = comp->data;

	// The output ports
	Netlist_Simp_pPortOut_CheckFree(&div_data->port_out_quo);
	Netlist_Simp_pPortOut_CheckFree(&div_data->port_out_rem);

	// Check if the entire component can be removed
	bool b = Netlist_Simp_DelComp_IfNoOutUsed(data, comp);
	if(b==true) return;

	// TODO Replace outputs by zero if the numerator is zero
	// TODO Replace outputs by all '-' if the denominator is zero, and output a warning
	// TODO Crop num and den inputs if there are leading zeros
}

void Netlist_Simp_Comp_Cmp(netlist_simp_data_t* data, netlist_comp_t* comp) {
	netlist_cmp_t* cmp_data = comp->data;

	Netlist_Simp_pPortOut_CheckFree(&cmp_data->port_eq);
	Netlist_Simp_pPortOut_CheckFree(&cmp_data->port_ne);

	// Check if the entire component can be removed
	Netlist_Simp_DelComp_IfNoOutUsed(data, comp);
}

void Netlist_Simp_Comp_Logic(netlist_simp_data_t* data, netlist_comp_t* comp) {
	netlist_logic_t* logic_data = comp->data;

	// Check if the entire component can be removed
	bool b = Netlist_Simp_DelComp_IfNoOutUsed(data, comp);
	if(b==true) return;

	// Shrink the component
	netlist_shrink_data_t shrink_data_i;
	netlist_shrink_data_t* shrink_data = &shrink_data_i;

	Netlist_Simp_Shrink_DataInit(shrink_data, logic_data->port_out);
	if(shrink_data->todo==true) {

		if(simp_verbose==true) {
			Netlist_Simp_Shrink_Disp(shrink_data, comp);
		}

		// Shrink the input ports
		avl_pp_foreach(&comp->ports, scanPort) {
			netlist_port_t* port = scanPort->data;
			if(port->direction!=NETLIST_PORT_DIR_IN) continue;
			Netlist_Simp_PortIn_Shrink(data, port, shrink_data->crop_left, shrink_data->crop_right);
		}

		// Shrink the literal value
		if(logic_data->value!=NULL) {
			char buf[shrink_data->new_width + 1];
			memcpy(buf, logic_data->value + shrink_data->crop_left, shrink_data->new_width * sizeof(*buf));
			buf[shrink_data->new_width] = 0;
			logic_data->value = namealloc(buf);
		}

		// Shift all references to the target port if necessary
		Netlist_Simp_PortOut_Shrink(data, logic_data->port_out, shrink_data->crop_left, shrink_data->crop_right);

		// Shrink the component
		logic_data->width = shrink_data->new_width;
	}
}



// The Address Read accesses
static void Netlist_Simp_Access_MemRA_tree(
	netlist_simp_data_t* data, netlist_comp_t* comp, avl_p_tree_t* accesses, void (*cb)(netlist_access_t* access)
) {
	// Work on a local list of accesses
	chain_list* list_ra_access = NULL;
	avl_p_foreach(accesses, scan) list_ra_access = addchain(list_ra_access, scan->data);

	foreach(list_ra_access, scanAccess) {
		netlist_access_t* access = scanAccess->DATA;
		netlist_access_mem_addr_t* memIn = access->data;

		// The output port
		if(Netlist_PortTargets_CheckEmpty(memIn->port_d)==false) continue;

		// Remove the memory access
		if(simp_verbose==true) {
			printf("Info: Removing RA access '%s' of comp '%s' type '%s'.\n", access->name, comp->name, comp->model->name);
		}

		Netlist_Simp_PortIn_FlagSourceComps(data, memIn->port_a);
		Netlist_Simp_UnlinkPortIn(data, memIn->port_a);
		if(cb!=NULL) cb(access);
	}

	freechain(list_ra_access);
}
// The Address Write accesses
static void Netlist_Simp_Access_MemWA_tree(
	netlist_simp_data_t* data, netlist_comp_t* comp, avl_p_tree_t* accesses, void (*cb)(netlist_access_t* access)
) {
	// Work on a local list of accesses
	chain_list* list_wa_access = NULL;
	avl_p_foreach(accesses, scan) list_wa_access = addchain(list_wa_access, scan->data);

	foreach(list_wa_access, scanAccess) {
		netlist_access_t* access = scanAccess->DATA;
		netlist_access_mem_addr_t* memOut = access->data;
		// Check the ports Data, Addr (not enable because the MUX has a default value)
		if(memOut->port_a!=NULL || memOut->port_d!=NULL) continue;
		assert(memOut->port_a==NULL && memOut->port_d==NULL);

		// Remove the memory access
		if(simp_verbose==true) {
			printf("Removing WA access '%s' of comp '%s' type '%s'.\n", access->name, comp->name, comp->model->name);
		}

		// Flag the source of Enable port in case it still has a MUX
		Netlist_Simp_PortIn_FlagSourceComps(data, memOut->port_e);
		Netlist_Simp_UnlinkPortIn(data, memOut->port_e);

		if(cb!=NULL) cb(access);
	}

	freechain(list_wa_access);
}
// The Direct Memory accesses
static void Netlist_Simp_Access_MemDirect_tree(
	netlist_simp_data_t* data, netlist_comp_t* comp, avl_ip_tree_t* accesses, void (*cb)(netlist_access_t* access)
) {
	// Work on a local list of accesses
	chain_list* list_direct_access = NULL;
	avl_ip_foreach(accesses, scan) list_direct_access = addchain(list_direct_access, scan->data);

	foreach(list_direct_access, scanAccess) {
		netlist_access_t* access = scanAccess->DATA;
		netlist_access_mem_direct_t* memDirect = access->data;
		// Check the ports
		bool write_unused = true;
		bool read_unused = true;
		// The Write port
		if(memDirect->port_wd!=NULL && memDirect->port_we!=NULL) {
			// Check the port Data (not the Enable port because the MUX has a default value)
			if(memDirect->port_wd->source!=NULL) write_unused = false;
		}
		// The Read port
		if(memDirect->port_rd!=NULL) {
			if(Netlist_PortTargets_CheckEmpty(memDirect->port_rd)==false) read_unused = false;
		}
		// Remove the memory access
		if(read_unused==true && write_unused==true) {
			if(simp_verbose==true) {
				printf("Info: Removing Direct access '%s' of comp '%s' type '%s'.\n", access->name, comp->name, comp->model->name);
			}
			Netlist_Simp_PortIn_FlagSourceComps(data, memDirect->port_wd);
			Netlist_Simp_PortIn_FlagSourceComps(data, memDirect->port_we);
			Netlist_Simp_UnlinkPortIn(data, memDirect->port_wd);
			Netlist_Simp_UnlinkPortIn(data, memDirect->port_we);
			if(cb!=NULL) cb(access);
		}
		else {
			if(memDirect->port_rd!=NULL && read_unused==true) {
				if(simp_verbose==true) {
					printf("Info: Removing Direct Read access '%s' of comp '%s' type '%s'.\n", access->name, comp->name, comp->model->name);
				}
				Netlist_Simp_pPortOut_UnlinkFree(data, &memDirect->port_rd);
			}
			if(memDirect->port_wd!=NULL && memDirect->port_we!=NULL && write_unused==true) {
				if(simp_verbose==true) {
					printf("Info: Removing Direct Write access '%s' of comp '%s' type '%s'.\n", access->name, comp->name, comp->model->name);
				}
				Netlist_Simp_pPortIn_UnlinkFree(data, &memDirect->port_wd);
				Netlist_Simp_pPortIn_UnlinkFree(data, &memDirect->port_we);
			}
		}
	}

	freechain(list_direct_access);
}

void Netlist_Simp_Comp_Mem(netlist_simp_data_t* data, netlist_comp_t* comp) {
	netlist_memory_t* mem_data = comp->data;

	// The Address Read accesses
	Netlist_Simp_Access_MemRA_tree(data, comp, &mem_data->access_ports_ra, Netlist_Access_MemRA_Remove);

	// The Address Write accesses
	Netlist_Simp_Access_MemWA_tree(data, comp, &mem_data->access_ports_wa, Netlist_Access_MemWA_Remove);

	// The Direct accesses
	Netlist_Simp_Access_MemDirect_tree(data, comp, &mem_data->access_ports_d, Netlist_Access_MemDirect_Remove);

	// Check if the entire component can be removed
	Netlist_Simp_DelComp_IfNoOutUsed(data, comp);
}

void Netlist_Simp_Comp_Fifo(netlist_simp_data_t* data, netlist_comp_t* comp) {
	netlist_fifo_t* fifo_data = comp->data;
	netlist_access_fifo_t* fifoin_data = fifo_data->fifo_in->data;
	netlist_access_fifo_t* fifoout_data = fifo_data->fifo_out->data;

	// If the FIFO has no internal storage, simplify it out
	if(fifo_data->cells_nb == 0) {

		void bypass_port2port(netlist_port_t* port_dest, netlist_port_t* port_src) {
			if(Netlist_PortTargets_CheckEmpty(port_dest)==true) return;
			if(port_src->source != NULL) {
				Netlist_PortTargets_ReplaceOutByValcat_List(port_dest, port_src->source);
			}
			else {
				printf("Warning: Bypassing empty comp '%s' model '%s': port '%s' has no source, driving port '%s' by zero.\n",
					comp->name, comp->model->name, port_src->name, port_dest->name
				);
				Netlist_PortTargets_ReplaceOutByLit_Repeat1Bit(port_dest, '0');
			}
		}

		if(simp_verbose==true) {
			printf("Info: Comp '%s' model '%s' is bypassed because there is no storage inside.\n", comp->name, comp->model->name);
		}

		// Bypass ack->rdy and data->data
		bypass_port2port(fifoin_data->port_rdy, fifoout_data->port_ack);
		bypass_port2port(fifoout_data->port_rdy, fifoin_data->port_ack);
		bypass_port2port(fifoout_data->port_data, fifoin_data->port_data);

		// Remove the component
		Netlist_Simp_DelComp(data, comp);
		return;
	}

	// FIXME Shrink the data width if the output is not read entirely

}

void Netlist_Simp_Comp_PingPong(netlist_simp_data_t* data, netlist_comp_t* comp) {
	netlist_pingpong_t* pp_data = comp->data;

	Netlist_Simp_pPortOut_CheckFree(&pp_data->port_ratio_c);
	Netlist_Simp_pPortOut_CheckFree(&pp_data->port_full_c);

	Netlist_Simp_pPortOut_CheckFree(&pp_data->port_ratio_n);
	Netlist_Simp_pPortOut_CheckFree(&pp_data->port_full_n);

	Netlist_Simp_pPortOut_CheckFree(&pp_data->port_full_n);

	Netlist_Simp_pPortIn_CheckFree(&pp_data->port_setmax);

	// The Address Read accesses
	Netlist_Simp_Access_MemRA_tree(data, comp, &pp_data->access_ports_ra, Netlist_PingPong_Remove_RA);

	// The Address Write accesses
	Netlist_Simp_Access_MemWA_tree(data, comp, &pp_data->access_ports_wa, Netlist_PingPong_Remove_WA);

	// The Address Write accesses
	Netlist_Simp_Access_MemDirect_tree(data, comp, &pp_data->access_ports_d, Netlist_PingPong_Remove_Direct);

	// Check if the entire component can be removed
	Netlist_Simp_DelComp_IfNoOutUsed(data, comp);
}



//======================================================
// Standalone modification functions
//======================================================

// FIXME These functions should be shared, reuse them in AUGH
static void Netlist_PortIn_Crop(netlist_port_t* port, unsigned left, unsigned right) {
	// Crop the input ValCat list
	if(port->source!=NULL) {
		Netlist_PortTargets_RemSource(port);
		port->source = Netlist_ValCat_Crop(port->source, left, right);
		Netlist_PortTargets_SetSource(port);
	}
	// Set the new width
	port->width -= left + right;
}
static void Netlist_PortOut_Crop(netlist_port_t* port, unsigned left, unsigned right) {
	// Replace all references to the reg output, these bits only, by the literal '0'
	if(Netlist_PortTargets_CheckEmpty(port)==false) {
		// Prepare the replacement expression for the port
		chain_list* new_list = NULL;
		if(right > 0) new_list = Netlist_ValCat_MakeList_RepeatBit('0', right);
		new_list = append(new_list,
			Netlist_ValCat_Crop(Netlist_ValCat_MakeList_FullPort(port), left, right)
		);
		if(left > 0) new_list = append(new_list, Netlist_ValCat_MakeList_RepeatBit('0', left));
		// Perform replacement
		Netlist_PortTargets_ReplaceOutByValcat_List(port, new_list);
		Netlist_ValCat_FreeList(new_list);
	}
	// Set the new width
	port->width -= left + right;
}

void Netlist_Comp_Sig_Crop(netlist_comp_t* comp, unsigned left, unsigned right) {
	if(left==0 && right==0) return;

	netlist_signal_t* sig_data = comp->data;
	assert(left + right < sig_data->width);
	unsigned new_width = sig_data->width - left - right;

	// Crop the input port
	Netlist_PortIn_Crop(sig_data->port_in, left, right);
	// Crop the output port
	Netlist_PortOut_Crop(sig_data->port_out, left, right);

	// Set the new width to the component
	sig_data->width = new_width;
}

void Netlist_Comp_Reg_Crop(netlist_comp_t* comp, unsigned left, unsigned right) {
	if(left==0 && right==0) return;

	netlist_register_t* reg_data = comp->data;
	assert(left + right < reg_data->width);
	unsigned new_width = reg_data->width - left - right;

	// Crop the input port
	Netlist_PortIn_Crop(reg_data->port_in, left, right);
	// Crop the output port
	Netlist_PortOut_Crop(reg_data->port_out, left, right);

	// Crop the init value
	if(reg_data->init_value!=NULL) {
		char buf[new_width+1];
		for(unsigned i=0; i<new_width; i++) buf[i] = reg_data->init_value[left+i];
		buf[new_width] = 0;
		reg_data->init_value = stralloc(buf);
	}

	// Set the new width to the component
	reg_data->width = new_width;
}

void Netlist_Comp_Mem_CropData(netlist_comp_t* comp, unsigned left, unsigned right) {
	if(left==0 && right==0) return;

	netlist_memory_t* mem_data = comp->data;
	assert(left + right < mem_data->data_width);
	unsigned new_width = mem_data->data_width - left - right;

	// Crop all data ports, input and output
	avl_pp_foreach(&comp->accesses, scan) {
		netlist_access_t* access = scan->data;
		if(access->model==NETLIST_ACCESS_MEM_D) {
			netlist_access_mem_direct_t* direct_data = access->data;
			if(direct_data->port_rd!=NULL) Netlist_PortIn_Crop(direct_data->port_rd, left, right);
			if(direct_data->port_wd!=NULL) Netlist_PortOut_Crop(direct_data->port_wd, left, right);
		}
		else if(
			access->model==NETLIST_ACCESS_MEM_RA || access->model==NETLIST_ACCESS_MEM_RA_EXT ||
			access->model==NETLIST_ACCESS_MEM_WA || access->model==NETLIST_ACCESS_MEM_WA_EXT
		) {
			netlist_access_mem_addr_t* addr_data = access->data;
			if(addr_data->port_d!=NULL) {
				if(addr_data->port_d->direction==NETLIST_PORT_DIR_IN) Netlist_PortIn_Crop(addr_data->port_d, left, right);
				else Netlist_PortIn_Crop(addr_data->port_d, left, right);
			}
		}
	}  // Scan accesses

	// Crop the init values
	if(mem_data->init_vex!=NULL) {
		for(unsigned i = 0; i<mem_data->cells_nb; i++) {
			hvex_t* vex = mem_data->init_vex[i];
			if(vex==NULL) continue;
			assert(vex->width==mem_data->data_width);
			hvex_croprel(vex, left, right);
		}
	}

	// Set the new width to the component
	mem_data->data_width = new_width;
}


