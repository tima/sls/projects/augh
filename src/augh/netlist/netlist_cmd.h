
#ifndef _NETLIST_CMD_H_
#define _NETLIST_CMD_H_

#include "../auto/auto.h"


// Shared values
extern bool do_comps_inline;
extern bool tb_disp_cycles;
extern bool tb_err_end_in;
extern bool objective_simu;
extern bool objective_human;
extern bool objective_synth;
extern bool fsmretime_lut6;
extern bool simp_verbose;
extern bool vhd2vl_friendly;


int Netlist_Command(implem_t* Implem, command_t* cmd_data);


#endif  // _NETLIST_CMD_H_

