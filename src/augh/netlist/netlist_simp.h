
#ifndef _NETLIST_SIMP_H_
#define _NETLIST_SIMP_H_


//======================================================
// Structures
//======================================================

typedef struct netlist_simp_data_t  netlist_simp_data_t;

#define NETLIST_SIMP_PROTECT_NODEL   0x01
#define NETLIST_SIMP_PROTECT_NOSIMP  0x02

struct netlist_simp_data_t {
	// A list of 'protected' components: don't remove them from the netlist
	// These must be removed from the main application, in case they are indexed somewhere else
	// Data = pointer to component
	avl_pi_tree_t comps_protect;

	// A tree to store what's modified
	// Data = pointer to component
	avl_p_tree_t comps_todo;
	avl_p_tree_t comps_todo_next;

	// Results: the number of components removed, for each model
	// Key = model name
	avl_pi_tree_t stats_comps_del;
	// Results: the components that were not removed because protected
	chain_list*   comps_protect_unlinked;
};


//======================================================
// Main functions
//======================================================

static inline void Netlist_Simp_AddCompNext(netlist_simp_data_t* data, netlist_comp_t* comp) {
	avl_p_add_overwrite(&data->comps_todo_next, comp, comp);
}

void Netlist_Simp_Init(netlist_simp_data_t* data);
void Netlist_Simp_Clear(netlist_simp_data_t* data);
void Netlist_Simp_ClearResults(netlist_simp_data_t* data);

void Netlist_Simp_Protect_AddFlag(netlist_simp_data_t* data, netlist_comp_t* comp, int flag);
bool Netlist_Simp_Protect_ChkFlag(netlist_simp_data_t* data, netlist_comp_t* comp, int flag);

void Netlist_Simp_PortOut_FlagTargetComps(netlist_simp_data_t* data, netlist_port_t* port);
void Netlist_Simp_PortIn_FlagSourceComps(netlist_simp_data_t* data, netlist_port_t* port);
void Netlist_Simp_UnlinkPortIn(netlist_simp_data_t* data, netlist_port_t* port);
void Netlist_Simp_UnlinkPortOut(netlist_simp_data_t* data, netlist_port_t* port);
void Netlist_Simp_DelComp(netlist_simp_data_t* data, netlist_comp_t* comp);
bool Netlist_Simp_DelComp_IfNoOutUsed(netlist_simp_data_t* data, netlist_comp_t* comp);

void Netlist_Simp(netlist_simp_data_t* data);


//======================================================
// Functions, per component model
//======================================================

void Netlist_Simp_Comp_Sig(netlist_simp_data_t* data, netlist_comp_t* comp);
void Netlist_Simp_Comp_Reg(netlist_simp_data_t* data, netlist_comp_t* comp);
void Netlist_Simp_Comp_Mux(netlist_simp_data_t* data, netlist_comp_t* comp);
void Netlist_Simp_Comp_Fsm(netlist_simp_data_t* data, netlist_comp_t* comp);
void Netlist_Simp_Comp_Asb(netlist_simp_data_t* data, netlist_comp_t* comp);
void Netlist_Simp_Comp_DivQR(netlist_simp_data_t* data, netlist_comp_t* comp);
void Netlist_Simp_Comp_Cmp(netlist_simp_data_t* data, netlist_comp_t* comp);
void Netlist_Simp_Comp_Logic(netlist_simp_data_t* data, netlist_comp_t* comp);

void Netlist_Simp_Comp_Mem(netlist_simp_data_t* data, netlist_comp_t* comp);
void Netlist_Simp_Comp_Fifo(netlist_simp_data_t* data, netlist_comp_t* comp);
void Netlist_Simp_Comp_PingPong(netlist_simp_data_t* data, netlist_comp_t* comp);

void Netlist_Comp_Sig_Crop(netlist_comp_t* comp, unsigned left, unsigned right);
void Netlist_Comp_Reg_Crop(netlist_comp_t* comp, unsigned left, unsigned right);
void Netlist_Comp_Mem_CropData(netlist_comp_t* comp, unsigned left, unsigned right);


#endif  // _NETLIST_SIMP_H_

