
#ifndef _NETLIST_ACCESS_H_
#define _NETLIST_ACCESS_H_

#include "netlist.h"


// Default callback functions (return NULL)
netlist_port_t* Netlist_Access_GetPort_Default(netlist_access_t* access, char* name);
char* Netlist_Access_GetName_Default(netlist_port_t* port);

// Utility functions to ease initialization of new access models
#define Netlist_AccessModel_Init_PortName(model, acctype_t, field, name) do { \
	int zzz_offset = ((void*)(&((acctype_t*)NULL)->field)) - NULL; \
	avl_pi_add_overwrite(&model->ports_name2offset, name, zzz_offset); \
	avl_ip_add_overwrite(&model->ports_offset2name, zzz_offset, name); \
} while(0)

// Utility functions to use models
static inline netlist_port_t* Netlist_Access_Name2Port(netlist_access_t* access, char* name) {
	avl_pi_node_t* pi_node = NULL;
	avl_pi_find(&access->model->ports_name2offset, name, &pi_node);
	netlist_port_t** pport = ((void*)access->data) + pi_node->data;
	return *pport;
}
static inline char* Netlist_Access_pPort2Name(netlist_access_t* access, netlist_port_t** pport) {
	int offset = ((void*)pport) - ((void*)access->data);
	avl_ip_node_t* ip_node = NULL;
	bool b = avl_ip_find(&access->model->ports_offset2name, offset, &ip_node);
	if(b==true) return ip_node->data;
	return NULL;
}
static inline char* Netlist_Access_Port_GetName(netlist_port_t* port) {
	netlist_access_t* access = port->access;
	avl_ip_foreach(&access->model->ports_offset2name, scan) {
		netlist_port_t** p_acc_port = ((void*)access->data) + scan->key;
		if(*p_acc_port==port) return scan->data;
	}
	return NULL;
}



//======================================================================
// Clock
//======================================================================

#define NETLIST_CLOCK_EDGE_UNSET 0  // Should not happen, the default is RISE
#define NETLIST_CLOCK_EDGE_RISE 1
#define NETLIST_CLOCK_EDGE_FALL -1

typedef struct netlist_access_clock_t  netlist_access_clock_t;
struct netlist_access_clock_t {
	netlist_port_t* port;
	double frequency;  // In Hertz
	double period_ns;  // In nanoseconds
	int active_edge;
};

extern netlist_access_model_t* NETLIST_ACCESS_CLOCK;

netlist_port_t* Netlist_Comp_AddPortAccess_Clock(netlist_comp_t* comp, char* name);

netlist_access_model_t* Netlist_Access_Clock_GetModel();

static inline void Netlist_Access_Clock_SetFrequency(netlist_access_t* access, double val) {
	netlist_access_clock_t* clock_data = access->data;
	if(val<=0) {
		clock_data->frequency = 0;
		clock_data->period_ns = 0;
	}
	else {
		clock_data->frequency = val;
		clock_data->period_ns = 1e9/val;
	}
}
static inline double Netlist_Access_Clock_GetFrequency(netlist_access_t* access) {
	netlist_access_clock_t* clock_data = access->data;
	return clock_data->frequency;
}

static inline int Netlist_Access_Clock_GetEdge(netlist_access_t* access) {
	netlist_access_clock_t* clock_data = access->data;
	return clock_data->active_edge;
}
static inline void Netlist_Access_Clock_SetEdge(netlist_access_t* access, int edge) {
	netlist_access_clock_t* clock_data = access->data;
	clock_data->active_edge = edge;
}

static inline void Netlist_Access_Clock_CopyParams(netlist_access_t* access, netlist_access_t* ref_access) {
	netlist_access_clock_t* clock_data = access->data;
	netlist_access_clock_t* refclock_data = ref_access->data;
	clock_data->frequency   = refclock_data->frequency;
	clock_data->period_ns   = refclock_data->period_ns;
	clock_data->active_edge = refclock_data->active_edge;
}



//======================================================================
// Differential clock
//======================================================================

typedef struct netlist_access_diffclock_t  netlist_access_diffclock_t;
struct netlist_access_diffclock_t {
	netlist_port_t* port_p;
	netlist_port_t* port_n;
	double frequency;  // In Hertz
	double period_ns;  // In nanoseconds
};

extern netlist_access_model_t* NETLIST_ACCESS_DIFFCLOCK;

netlist_access_t* Netlist_Comp_AddAccess_DiffClock(netlist_comp_t* comp, char* name);

netlist_access_model_t* Netlist_Access_DiffClock_GetModel();

static inline void Netlist_Access_DiffClock_SetFrequency(netlist_access_t* access, double val) {
	netlist_access_diffclock_t* clock_data = access->data;
	if(val<=0) {
		clock_data->frequency = 0;
		clock_data->period_ns = 0;
	}
	else {
		clock_data->frequency = val;
		clock_data->period_ns = 1e9/val;
	}
}
static inline double Netlist_Access_DiffClock_GetFrequency(netlist_access_t* access) {
	netlist_access_diffclock_t* clock_data = access->data;
	return clock_data->frequency;
}

static inline void Netlist_Access_DiffClock_CopyParams(netlist_access_t* access, netlist_access_t* ref_access) {
	netlist_access_diffclock_t* clock_data = access->data;
	netlist_access_diffclock_t* refclock_data = ref_access->data;
	clock_data->frequency = refclock_data->frequency;
	clock_data->period_ns = refclock_data->period_ns;
}



//======================================================================
// Reset
//======================================================================

typedef struct netlist_access_reset_t  netlist_access_reset_t;
struct netlist_access_reset_t {
	netlist_port_t* port;
	char active_state;  // The character '0' or '1' (0 means unspecified, default is '1')
};

extern netlist_access_model_t* NETLIST_ACCESS_RESET;

netlist_port_t* Netlist_Comp_AddPortAccess_Reset(netlist_comp_t* comp, char* name);
void Netlist_Access_Reset_Rem(netlist_access_t* access);

netlist_access_model_t* Netlist_Access_Reset_GetModel();

static inline void Netlist_Access_Reset_SetActiveState(netlist_access_t* access, char bit) {
	netlist_access_reset_t* reset_data = access->data;
	reset_data->active_state = bit;
}
static inline char Netlist_Access_Reset_GetActiveState(netlist_access_t* access) {
	netlist_access_reset_t* reset_data = access->data;
	return reset_data->active_state;
}



//======================================================================
// Generic access models
//======================================================================

// Data structure for accesses that have only one private field: one port
typedef struct netlist_access_single_port_t  netlist_access_single_port_t;
struct netlist_access_single_port_t {
	netlist_port_t* port;
};

// Start

extern netlist_access_model_t* NETLIST_ACCESS_START;

netlist_port_t* Netlist_Comp_AddPortAccess_Start(netlist_comp_t* comp, char* name);

netlist_access_model_t* Netlist_Access_Start_GetModel();

// Bare wires

extern netlist_access_model_t* NETLIST_ACCESS_WIRE_IN;
extern netlist_access_model_t* NETLIST_ACCESS_WIRE_OUT;

netlist_port_t* Netlist_Comp_AddPortAccess_WireIn(netlist_comp_t* comp, char* name, unsigned width);
netlist_port_t* Netlist_Comp_AddPortAccess_WireOut(netlist_comp_t* comp, char* name, unsigned width);

netlist_access_model_t* Netlist_Access_WireIn_GetModel();
netlist_access_model_t* Netlist_Access_WireOut_GetModel();



//======================================================================
// FIFO
//======================================================================

extern netlist_access_model_t* NETLIST_ACCESS_FIFO_IN;
extern netlist_access_model_t* NETLIST_ACCESS_FIFO_OUT;

typedef struct netlist_access_fifo_t  netlist_access_fifo_t;
struct netlist_access_fifo_t {
	unsigned        width;
	netlist_port_t* port_data;
	netlist_port_t* port_rdy;  // direction is IN
	netlist_port_t* port_ack;  // direction is OUT
};

netlist_access_t* Netlist_Comp_AddAccess_FifoIn(netlist_comp_t* comp, char* access_name, unsigned width, bool portnames_noacc, avl_p_tree_t* tree_avoid_names);
netlist_access_t* Netlist_Comp_AddAccess_FifoOut(netlist_comp_t* comp, char* access_name, unsigned width, bool portnames_noacc, avl_p_tree_t* tree_avoid_names);

netlist_access_model_t* Netlist_Access_FifoIn_GetModel();
netlist_access_model_t* Netlist_Access_FifoOut_GetModel();

void Netlist_Access_Fifo_Connect(netlist_access_t* fifo_in, netlist_access_t* fifo_out);



#endif  // _NETLIST_ACCESS_H_

