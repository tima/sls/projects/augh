
//===================================================
// Command interpreter for Netlist
//===================================================

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#include "../auto/auto.h"
#include "../auto/command.h"

#include "netlist.h"
#include "netlist_tb.h"
#include "netlist_comps.h"
#include "netlist_access.h"
#include "netlist_vhdl.h"
#include "netlist_cmd.h"



// Shared values
bool do_comps_inline = true;
bool tb_disp_cycles = false;
bool tb_err_end_in = false;
bool objective_simu = false;
bool objective_human = true;
bool objective_synth = false;
bool fsmretime_lut6 = false;
bool simp_verbose = false;
bool vhd2vl_friendly = false;

typedef struct netlist_tb_allopts_t {
	char* odir;   // Output directory
	char* oname;  // Output file name

	chain_list* list_fifos;

	unsigned simu_cycles;

	bool     all_littleendian;
	unsigned all_style;
	unsigned all_bytes;

	tb_fifo_t* cur_fifo;

	unsigned stop_nb;
	bool     stop_end_vectors;

	int ret;
} netlist_tb_allopts_t;

static void allopts_init(netlist_tb_allopts_t* allopts) {
	memset(allopts, 0, sizeof(*allopts));
	allopts->odir = "vhdl";
	allopts->list_fifos = NULL;
	allopts->simu_cycles = 10000;
	allopts->all_littleendian = true;
	allopts->all_style = TB_DATA_AUTO;
	allopts->all_bytes = 0;
	allopts->cur_fifo = NULL;
	allopts->stop_nb = 0;
	allopts->stop_end_vectors = false;
	allopts->ret = 0;
}
static void allopts_clear(netlist_tb_allopts_t* allopts) {
	foreach(allopts->list_fifos, scan) TB_FifoData_Free(scan->DATA);
	freechain(allopts->list_fifos);
}

static void allopts_curfifo_init(netlist_tb_allopts_t* allopts) {
	allopts->cur_fifo->style = allopts->all_style;
	allopts->cur_fifo->bytes_nb = allopts->all_bytes;
	allopts->cur_fifo->is_littleendian = allopts->all_littleendian;
	allopts->cur_fifo->stop_nb = allopts->stop_nb;
	allopts->cur_fifo->stop_end_vectors = allopts->stop_end_vectors;
}

static void allopts_set_style(netlist_tb_allopts_t* allopts, bool s) {
	if(allopts->cur_fifo==NULL) allopts->all_style = s;
	else allopts->cur_fifo->style = s;
}
static void allopts_set_litend(netlist_tb_allopts_t* allopts, bool b) {
	if(allopts->cur_fifo==NULL) allopts->all_littleendian = b;
	else allopts->cur_fifo->is_littleendian = b;
}
static void allopts_set_stop_nb(netlist_tb_allopts_t* allopts, unsigned nb) {
	if(allopts->cur_fifo==NULL) allopts->stop_nb = nb;
	else allopts->cur_fifo->stop_nb = nb;
}
static void allopts_set_stop_end_vectors(netlist_tb_allopts_t* allopts) {
	if(allopts->cur_fifo==NULL) allopts->stop_end_vectors = true;
	else allopts->cur_fifo->stop_end_vectors = true;
}

static int allopts_parse(implem_t* Implem, command_t* cmd_data, const char* cmd, netlist_tb_allopts_t* allopts) {

	do {
		const char* opt = Command_PopParam(cmd_data);
		if(opt==NULL) break;
		if(opt[0]!='-') break;

		// Output directory
		if(strcmp(opt, "-odir")==0) {
			allopts->odir = Command_PopParam_stralloc(cmd_data);
			if(allopts->odir==NULL) {
				printf("Error: Missing value for option '%s' to command '%s'.\n", opt, cmd);
				allopts->ret = __LINE__;
				goto GETOUT;
			}
		}
		// Output file
		else if(strcmp(opt, "-of")==0) {
			allopts->oname = Command_PopParam_stralloc(cmd_data);
			if(allopts->oname==NULL) {
				printf("Error: Missing value for option '%s' to command '%s'.\n", opt, cmd);
				allopts->ret = __LINE__;
				goto GETOUT;
			}
		}
		// Max number of cycles to simulate
		else if(strcmp(opt, "-cy")==0) {
			const char* param = Command_PopParam(cmd_data);
			if(param==NULL) {
				printf("Error: Missing value for option '%s' to command '%s'.\n", opt, cmd);
				allopts->ret = __LINE__;
				goto GETOUT;
			}
			allopts->simu_cycles = atoi(param);
			if(allopts->simu_cycles<=0) {
				printf("Error: Invalid value for option '%s' to command '%s'.\n", opt, cmd);
				allopts->ret = __LINE__;
				goto GETOUT;
			}
		}

		// Salect a new channel
		else if(strcmp(opt, "-the-in")==0) {
			if(TB_FifoData_List_FindTheIn(allopts->list_fifos)!=NULL) {
				printf("Error: Parameter '%s' can only be used once.\n", opt);
				allopts->ret = __LINE__;
				goto GETOUT;
			}
			allopts->cur_fifo = TB_FifoData_New();
			allopts->list_fifos = addchain(allopts->list_fifos, allopts->cur_fifo);
			allopts->cur_fifo->is_the_in = true;
			allopts_curfifo_init(allopts);
		}
		else if(strcmp(opt, "-the-out")==0) {
			if(TB_FifoData_List_FindTheOut(allopts->list_fifos)!=NULL) {
				printf("Error: Parameter '%s' can only be used once.\n", opt);
				allopts->ret = __LINE__;
				goto GETOUT;
			}
			allopts->cur_fifo = TB_FifoData_New();
			allopts->list_fifos = addchain(allopts->list_fifos, allopts->cur_fifo);
			allopts->cur_fifo->is_the_out = true;
			allopts_curfifo_init(allopts);
		}
		else if(strcmp(opt, "-name")==0) {
			char* name = Command_PopParam_namealloc(cmd_data);
			if(name==NULL) {
				printf("Error: Missing value for option '%s' to command '%s'.\n", opt, cmd);
				allopts->ret = __LINE__;
				goto GETOUT;
			}
			if(TB_FifoData_List_FindName(allopts->list_fifos, name)!=NULL) {
				printf("Error: Fifo '%s' already specified.\n", name);
				allopts->ret = __LINE__;
				goto GETOUT;
			}
			allopts->cur_fifo = TB_FifoData_New();
			allopts->list_fifos = addchain(allopts->list_fifos, allopts->cur_fifo);
			allopts->cur_fifo->channel_name = name;
			allopts_curfifo_init(allopts);
		}

		// The data style
		else if(strcmp(opt, "-auto")==0)   allopts_set_style(allopts, TB_DATA_AUTO);
		else if(strcmp(opt, "-hex")==0)    allopts_set_style(allopts, TB_DATA_HEX);
		else if(strcmp(opt, "-dec")==0)    allopts_set_style(allopts, TB_DATA_DEC);
		else if(strcmp(opt, "-bin")==0)    allopts_set_style(allopts, TB_DATA_BIN);
		else if(strcmp(opt, "-rawbin")==0) allopts_set_style(allopts, TB_DATA_RAWBIN);
		// Other parameters
		else if(strcmp(opt, "-le")==0) allopts_set_litend(allopts, true);
		else if(strcmp(opt, "-be")==0) allopts_set_litend(allopts, false);
		else if(strcmp(opt, "-nb")==0) {
			const char* param = Command_PopParam(cmd_data);
			if(param==NULL) {
				printf("Error: Missing value for option '%s' to command '%s'.\n", opt, cmd);
				allopts->ret = __LINE__;
				goto GETOUT;
			}
			unsigned nb = atoi(param);
			if(allopts->cur_fifo==NULL) allopts->all_bytes = nb;
			else allopts->cur_fifo->bytes_nb = nb;
		}
		// Specify the file that contains vectors
		else if(strcmp(opt, "-f")==0) {
			if(allopts->cur_fifo==NULL) {
				printf("Error: A channel must be selected to use option '%s' to command '%s'.\n", opt, cmd);
				allopts->ret = __LINE__;
				goto GETOUT;
			}
			char* filename = Command_PopParam_stralloc(cmd_data);
			if(filename==NULL) {
				printf("Error: Missing value for option '%s' to command '%s'.\n", opt, cmd);
				allopts->ret = __LINE__;
				goto GETOUT;
			}
			allopts->cur_fifo->file_name = filename;
		}

		// Other options to stop simulation when a certain number of outputs are obtained
		else if(strcmp(opt, "-stopnb")==0) {
			const char* param = Command_PopParam(cmd_data);
			if(param==NULL) {
				printf("Error: Missing value for option '%s' to command '%s'.\n", opt, cmd);
				allopts->ret = __LINE__;
				goto GETOUT;
			}
			int nb = atoi(param);
			if(nb<=0) {
				printf("Error: Invalid value for option '%s' to command '%s'.\n", opt, cmd);
				allopts->ret = __LINE__;
				goto GETOUT;
			}
			allopts_set_stop_nb(allopts, nb);
		}
		else if(strcmp(opt, "-stopvectors")==0) allopts_set_stop_end_vectors(allopts);

		else {
			printf("Error: Unknown option '%s' for command '%s'.\n", opt, cmd);
			allopts->ret = __LINE__;
			goto GETOUT;
		}

	}while(1);  // Infinite loop: parse parameters

	// Finish FIFO identification
	allopts->ret = Netlist_TB_IdentifyFifos(Implem, allopts->list_fifos);
	if(allopts->ret!=0) goto GETOUT;

	GETOUT:

	return allopts->ret;
}

int Netlist_Command(implem_t* Implem, command_t* cmd_data) {
	const char* cmd = Command_PopParam(cmd_data);
	if(cmd==NULL) {
		printf("Error [netlist] : No command received. Try 'help'.\n");
		return -1;
	}

	if(strcmp(cmd, "help")==0) {
		printf(
			"Setting parameters:\n"
			"  comps-inline [<bool>]   When possible, do not generate standalone VHDL components.\n"
			"                          Their functionality is merged in the top-level component (init=true, default=true)\n"
			"  tb-disp-cycles [<bool>] The generated testbench will display the current simulated cycle.\n"
			"                          (init=false, default=true)\n"
			"  tb-err-end-in [<bool>]  The testbench will stop with failure when the design asks inputs but none remains.\n"
			"                          (init=false, default=true)\n"
			"  objective-simu          The generated VHDL will be designed for simulation speed.\n"
			"  objective-human         The generated VHDL will be designed for readability by humans. (default)\n"
			"  objective-synth         The generated VHDL will be designed for logic synthesis speed.\n"
			"  fsmretime-lut6 [<bool>] The generated VHDL will implement LUT6 directly for retiming.\n"
			"                          Must not be used for technologies not based on LUT6, or when the library UNUSIM is not available.\n"
			"                          (init=false, default=true)\n"
			"  simp-verbose [<bool>]   Select verbosity for simplification (init=false, default=true).\n"
			"  vhd2vl-friendly [<bool>] The generated VHDL will be easily convertible to Verilog (init=false, default=true).\n"
			"\n"
			"Possible commands:\n"
			"  tb-gen-simple [tb-options]\n"
			"                          Generate a full testbench for the current circuit.\n"
			"  vectors-rawbin2vhdl [tb-options]\n"
			"                          To help manually build testbenches: convert raw binary to vhdl syntax.\n"
			"                          Select one unique channel.\n"
			"\n"
			"Possible [tb-options]:\n"
			"    Options to specify the top-level channel:\n"
			"      -the-in             Select the input fifo, mut be only one of them.\n"
			"      -the-out            Select the output fifo, mut be only one of them.\n"
			"      -name <name>        Select the channel named <name>.\n"
			"    Misc options:\n"
			"      -odir <path>        Set output directory.\n"
			"      -cy <cycles>        Set the simulation timeout, in clock cycles (default: 10000).\n"
			"                          If zero, there is no timeout (dangerous!).\n"
			"      -f <file>           Set the files that contains vectors.\n"
			"      -of <file>          Set the name of the output file (only to dump test vectors).\n"
			"      -stopvectors        Output FIFO: Stop simulation when all vectors have been read.\n"
			"      -stopnb <nb>        Output FIFO: Stop simulation when <nb> vectors have been read.\n"
			"    Options to specify the encoding of the vectors:\n"
			"      -auto               Autodetect for each vector in each file (hex, dec, bin) (default).\n"
			"      -hex                The vectors are stored as hexadecimal (leading 0x optional).\n"
			"      -dec                The vectors are stored as signed decimal.\n"
			"      -bin                The vectors are stored as binary digits (leading 0b optional).\n"
			"      -rawbin             The vectors are stored as raw machine bytes.\n"
			"    Options specific to RAWBIN encoding:\n"
			"      -nb <bytes>         Set the number of bytes per input vector.\n"
			"      -le                 The bytes are stored as little endian (default).\n"
			"      -be                 The bytes are stored as big endian.\n"
		);
		return 0;
	}

	if(strcmp(cmd, "comps-inline")==0) {
		int z = Command_PopParam_YesNoDef_Verbose(cmd_data, true, cmd);
		if(z<0) return -1;
		do_comps_inline = z;
		return 0;
	}
	else if(strcmp(cmd, "tb-disp-cycles")==0) {
		int z = Command_PopParam_YesNoDef_Verbose(cmd_data, true, cmd);
		if(z<0) return -1;
		tb_disp_cycles = z;
		return 0;
	}
	else if(strcmp(cmd, "tb-err-end-in")==0) {
		int z = Command_PopParam_YesNoDef_Verbose(cmd_data, true, cmd);
		if(z<0) return -1;
		tb_err_end_in = z;
		return 0;
	}
	else if(strcmp(cmd, "objective-simu")==0) {
		objective_simu = true;
		objective_human = false;
		objective_synth = false;
		return 0;
	}
	else if(strcmp(cmd, "objective-human")==0) {
		objective_simu = false;
		objective_human = true;
		objective_synth = false;
		return 0;
	}
	else if(strcmp(cmd, "objective-synth")==0) {
		objective_simu = false;
		objective_human = false;
		objective_synth = true;
		return 0;
	}
	else if(strcmp(cmd, "fsmretime-lut6")==0) {
		int z = Command_PopParam_YesNoDef_Verbose(cmd_data, true, cmd);
		if(z<0) return -1;
		fsmretime_lut6 = z;
		return 0;
	}
	else if(strcmp(cmd, "simp-verbose")==0) {
		int z = Command_PopParam_YesNoDef_Verbose(cmd_data, true, cmd);
		if(z<0) return -1;
		simp_verbose = z;
		return 0;
	}
	else if(strcmp(cmd, "vhd2vl-friendly")==0) {
		int z = Command_PopParam_YesNoDef_Verbose(cmd_data, true, cmd);
		if(z<0) return -1;
		vhd2vl_friendly = z;
		return 0;
	}

	else if(strcmp(cmd, "vectors-rawbin2vhdl")==0) {
		netlist_tb_allopts_t allopts_i;
		netlist_tb_allopts_t* allopts = &allopts_i;
		allopts_init(allopts);
		allopts->all_style = TB_DATA_RAWBIN;

		int ret = 0;

		allopts_parse(Implem, cmd_data, cmd, allopts);
		if(allopts->ret!=0) { ret = __LINE__; goto CLEANOPTS; }

		if(allopts->oname==NULL) {
			printf("Error [netlist] : Must specify output file for command '%s'.\n", cmd);
			ret = __LINE__;
			goto CLEANOPTS;
		}
		if(ChainList_Count(allopts->list_fifos) != 1) {
			printf("Error [netlist] : Must specify exactly one channel for command '%s' (got %u).\n", cmd, ChainList_Count(allopts->list_fifos));
			ret = __LINE__;
			goto CLEANOPTS;
		}

		tb_fifo_t* cur_fifo = allopts->list_fifos->DATA;
		if(cur_fifo->file_name == NULL) {
			printf("Error [netlist] : Missing input file for command '%s'.\n", cmd);
			ret = __LINE__;
			goto CLEANOPTS;
		}
		if(cur_fifo->style != TB_DATA_RAWBIN) {
			printf("Error [netlist] : The channel data style must be RAWBIN for command '%s'.\n", cmd);
			ret = __LINE__;
			goto CLEANOPTS;
		}

		// Load vectors
		int z = Netlist_TB_ReadFifoVectors(cur_fifo);
		if(z!=0) { ret = __LINE__; goto CLEANOPTS; }

		// Display info
		printf("Loaded %u vectors\n", cur_fifo->vectors_nb);

		// Open the output file
		FILE* F = fopen(allopts->oname, "wb");
		if(F==NULL) {
			printf("Error [netlist] : Can't open output file '%s'.\n", allopts->oname);
			ret = __LINE__;
			goto CLEANOPTS;
		}

		// Dump the vectors
		Netlist_TB_DumpFifoVectors(F, cur_fifo, "\t\t", 120);
		fclose(F);

		CLEANOPTS:

		allopts_clear(allopts);

		return ret;
	}

	else if(strcmp(cmd, "tb-gen")==0) {
		netlist_tb_allopts_t allopts_i;
		netlist_tb_allopts_t* allopts = &allopts_i;
		allopts_init(allopts);

		int ret = 0;

		allopts_parse(Implem, cmd_data, cmd, allopts);
		if(allopts->ret!=0) {ret = __LINE__; goto TB_CLEANOPTS; }

		// Convert list of fifos to a tree, indexed by name
		avl_pp_tree_t tree_fifos;
		avl_pp_init(&tree_fifos);
		foreach(allopts->list_fifos, scan) {
			tb_fifo_t* tb_fifo = scan->DATA;
			avl_pp_add_overwrite(&tree_fifos, tb_fifo->channel_name, tb_fifo);
		}

		ret = Netlist_TB_Gen(Implem, &tree_fifos, allopts->odir, allopts->simu_cycles);

		avl_pp_reset(&tree_fifos);

		TB_CLEANOPTS:

		allopts_clear(allopts);

		return ret;
	}

	#ifndef NDEBUG  // These functions are for debug purposes only

	else if(strcmp(cmd, "test-pp-vhdl")==0) {

		netlist_comp_t* comp;

		// Name, datawidth, cellsnb, addrwidth, banksnb, isinput
		comp = Netlist_PingPong_New("testpp_in_reg_2x8x32", 32, 8, 3, 2, true);
		Netlist_PingPong_EnableDirect(comp);
		Netlist_GenVHDL_simple(comp);
		Netlist_Comp_Free(comp);

		comp = Netlist_PingPong_New("testpp_in_reg_3x8x32", 32, 8, 3, 3, true);
		Netlist_PingPong_EnableDirect(comp);
		Netlist_GenVHDL_simple(comp);
		Netlist_Comp_Free(comp);

		return 0;
	}

	// Parameters : data width, parity (0/1), divider
	else if(strcmp(cmd, "test-uart-vhdl")==0) {
		char const * param;

		param = Command_PopParam(cmd_data);
		unsigned data_width = atoi(param);
		param = Command_PopParam(cmd_data);
		unsigned parity = atoi(param);
		param = Command_PopParam(cmd_data);
		unsigned divider = atoi(param);

		netlist_comp_t* comp = Netlist_Uart_New(namealloc("uart"), data_width, parity!=0 ? true : false, divider);
		Netlist_GenVHDL_simple(comp);
		Netlist_Comp_Free(comp);

		return 0;
	}

	// Parameters : fifo width
	else if(strcmp(cmd, "test-uart-tx-vhdl")==0) {
		const char* param = Command_PopParam(cmd_data);
		unsigned fifo_width = atoi(param);

		netlist_comp_t* comp = Netlist_Uart_Tx_New(namealloc("uart_tx"), fifo_width);
		netlist_uart_side_t* uart_data = comp->data;

		//uart_data->cfg->clk_divider = 500;
		Netlist_Access_Clock_SetFrequency(uart_data->port_clk->access, 100000000);

		uart_data->cfg->baudrate = 0;
		uart_data->cfg->clk_divider = 500;

		uart_data->cfg->elts_pad = 0;
		uart_data->cfg->elts_le = false;

		Netlist_GenVHDL_simple(comp);
		Netlist_Comp_Free(comp);

		return 0;
	}
	else if(strcmp(cmd, "test-uart-rx-vhdl")==0) {
		const char* param = Command_PopParam(cmd_data);
		unsigned fifo_width = atoi(param);

		netlist_comp_t* comp = Netlist_Uart_Rx_New(namealloc("uart_rx"), fifo_width);
		netlist_uart_side_t* uart_data = comp->data;

		//uart_data->cfg->clk_divider = 500;
		Netlist_Access_Clock_SetFrequency(uart_data->port_clk->access, 100000000);

		uart_data->cfg->baudrate = 0;
		uart_data->cfg->clk_divider = 500;

		uart_data->cfg->elts_pad = 0;
		uart_data->cfg->elts_le = false;

		Netlist_GenVHDL_simple(comp);
		Netlist_Comp_Free(comp);

		return 0;
	}

	// Parameters : data width, cells nb
	else if(strcmp(cmd, "test-fifo-vhdl")==0) {
		char const * param;

		param = Command_PopParam(cmd_data);
		unsigned data_width = atoi(param);
		param = Command_PopParam(cmd_data);
		unsigned cells_nb = atoi(param);

		netlist_comp_t* comp = Netlist_Fifo_New(namealloc("fifo"), data_width, cells_nb);
		Netlist_GenVHDL_simple(comp);
		Netlist_Comp_Free(comp);

		return 0;
	}

	// Parameters : divider
	else if(strcmp(cmd, "test-clkdiv-vhdl")==0) {
		const char* param = Command_PopParam(cmd_data);
		unsigned divider = atoi(param);

		netlist_comp_t* comp = Netlist_ClkDiv_New(namealloc("clkdiv"), divider);
		Netlist_GenVHDL_simple(comp);
		Netlist_Comp_Free(comp);

		return 0;
	}

	else if(strcmp(cmd, "test-retiming")==0) {
		if(Implem->netlist.fsm==NULL) {
			printf("Error: The FSM component is not built yet. Can't enable retiming\n");
			return -1;
		}

		netlist_fsm_t* fsm_data = Implem->netlist.fsm->data;
		fsm_data->retime_en = true;
		fsm_data->rtm_counter_bitsnb = 5;
		fsm_data->rtm_counter_fanout = 0;

		return 0;
	}

	#endif

	else {
		printf("Error [netlist] : Unknown command '%s'.\n", cmd);
		return -1;
	}

	return 0;
}

