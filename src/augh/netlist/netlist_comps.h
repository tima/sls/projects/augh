
#ifndef _NETLIST_COMPS_H_
#define _NETLIST_COMPS_H_

#include <stdint.h>
#include <stdbool.h>

#include "../hvex/hvex.h"

#include "../chain.h"
#include "netlist.h"



//======================================================================
// Top-level component
//======================================================================

typedef struct netlist_top_t {
	// If this is non-NULL, the component model is assumed to be externally defined
	// In particular, VHDL generation of the content of this comp instance is not attempted
	char* extern_model_name;
} netlist_top_t;

extern netlist_comp_model_t* NETLIST_COMP_TOP;

netlist_comp_t* Netlist_Comp_Top_New(char* name);

netlist_comp_model_t* Netlist_Comp_Top_GetModel();



//======================================================================
// Finite State Machine
//======================================================================

/* Implementation details

The FSM is one-hot
One register bit is associated to each FSM state,
and only one of these registers is at '1' at any given time.

About buffering of outputs:

The outputs can be buffered or not.
Important: at VHDL generation, if the field 'outputs_buf' of the FSM component is false,
then all outputs are considered unbuffered.

Outputs that control datapath (mainly mux) must be buffered
if they are used in FSM states that can last more than one clock cycle.
Because otherwise the logic that generates the outputs can produce glitches.

Note: To save hardware, if an output that drives datapath is used only in one state,
then it can be unbuffered because it is then equal the corresponding state register.

About retiming:

The FSM can optionally be generated with special instrumentation
for post- place-and-route retiming, with the state freezing technique.
In this case, the FSM contains a counter.
When entering a new FSM state, the counter is reset to zero.
The counter is incremented at each clock cycle spent in the same state.
Going into another state only happens when the counter value reaches
the number of clock cycles the state should last.
For this purpose, each state is also associated to a little comparator,
which outputs '1' only when in the last clock cycle of the state.

Important: we want the be able to set the duration after place-and-route.
For this reason, the comparator is implemented in a separate entity,
directly described with bare FPGA prmitives (e.g. one LUT6).
Special flags should also be passed to the back-end synthesis tools
to keep these primitives untouched after place-and-route.

Depending on the implementation, it may be possible to save some hardware
and use non-buffered outputs for stuff that is not on the critical path:
- outputs write_enable for registers/memories
- handshaking signals for communications
More generally, outputs that need to be set only at the last cycle of the states.

*/

extern netlist_comp_model_t* NETLIST_COMP_FSM;

typedef struct netlist_fsm_t         netlist_fsm_t;

typedef struct netlist_fsm_func_t    netlist_fsm_func_t;
typedef struct netlist_fsm_rule_t    netlist_fsm_rule_t;
typedef struct netlist_fsm_state_t   netlist_fsm_state_t;
typedef struct netlist_fsm_action_t  netlist_fsm_action_t;
typedef struct netlist_fsm_outval_t  netlist_fsm_outval_t;
typedef struct netlist_fsm_rtmcnt_t  netlist_fsm_rtmcnt_t;

struct netlist_fsm_func_t {
	char* name_func;
	char* name_reg;
	unsigned reg_size;
	unsigned calls_nb;
};
struct netlist_fsm_rule_t {
	void* father;  // rule, or state
	bool father_is_rule;

	// If this field is non-NULL, it is the next state unconditionally (except father rules)
	netlist_fsm_state_t* state;

	// A branch condition
	hvex_t*              condition;
	netlist_fsm_rule_t*  rule_true;
	netlist_fsm_rule_t*  rule_false;

	// If we came here from a function call, or if it's a return
	// TYPE = call ID, DATA = netlist_fsm_func_t*, DATA_TO = netlist_fsm_rule_t*
	bitype_list* func_call;
	bitype_list* func_rets;

	// FIXME Take into account when it's already known that only one rule will be true among many
	//   To better generate SWITCH branch condition and avoid priority tree if-then-elsif-else
};
struct netlist_fsm_state_t {
	// States are indexed from 0
	unsigned index;
	// The list of Actions
	chain_list* actions;
	// And the conditions to change state
	netlist_fsm_rule_t* rules;
	chain_list* prev_rules;  // The rules that lead to this state
	// In case the State lasts several clock cycles
	unsigned  cycles_nb;
	// The clock cycle counter
	netlist_fsm_rtmcnt_t* rtm_counter;
};
struct netlist_fsm_action_t {
	netlist_fsm_state_t* state;
	netlist_fsm_outval_t* outval;
	char* value;  // string with characters '0', '1', '-'
	// In case the State lasts several clock cycles
	// Used before state duplication, or for retiming
	bool only_last_cycle;
};
struct netlist_fsm_outval_t {
	netlist_port_t* port;
	char*           default_val;  // string with characters '0', '1', '-'
	chain_list*     actions;
	bool            is_buffered;
};
struct netlist_fsm_rtmcnt_t {
	unsigned    index;
	chain_list* states;
	unsigned    states_nb;
};

struct netlist_fsm_t {
	// Directly accessible ports : clock, reset
	netlist_port_t* port_clock;
	netlist_port_t* port_reset;

	char reset_active_level;

	// The data input and output
	unsigned inputs_nb;
	unsigned outputs_nb;
	// Indexes to generate name of I/O ports
	unsigned input_name_idx;
	unsigned output_name_idx;

	// The states
	netlist_fsm_state_t* init_state;
	chain_list* states;
	unsigned    states_nb;

	// The function calls
	avl_pp_tree_t func_calls;

	// The clock cycle counters, for retiming
	chain_list* rtm_counters;
	unsigned    rtm_counters_nb;

	// For retiming
	// FIXME the techno library should declare the available "profiles" for FSM implem
	//   e.g. about counter width (related to nb of inputs of LUT)
	bool retime_en;
	unsigned rtm_counter_bitsnb;
	unsigned rtm_counter_fanout;  // 0 if only one counter

	// The data for output ports. Key is netlist_port_t*, data is netlist_fsm_outval_t*
	avl_pp_tree_t output_values;
	// If true, outputs are buffered by default
	bool          outputs_buf;

	// Optional extention, for example two-hot structure...
	void* extend;
};


netlist_comp_t* Netlist_Comp_Fsm_New(char* name);

netlist_port_t* Netlist_Comp_Fsm_AddInput(netlist_comp_t* comp, unsigned width);
netlist_port_t* Netlist_Comp_Fsm_AddOutput(netlist_comp_t* comp, unsigned width);

netlist_fsm_outval_t* Netlist_Comp_Fsm_GetOutVal(netlist_port_t* port);
netlist_fsm_outval_t* Netlist_Comp_Fsm_GetAddOutVal(netlist_port_t* port);
void                  Netlist_Comp_Fsm_SetDefaultValue(netlist_port_t* port, char* value);

netlist_fsm_state_t*  Netlist_Comp_Fsm_AddState(netlist_comp_t* comp);
netlist_fsm_action_t* Netlist_Comp_Fsm_AddAction(netlist_comp_t* comp, netlist_fsm_state_t* state, netlist_port_t* port, char* value);

netlist_fsm_rule_t* Netlist_FsmRule_New();
void Netlist_FsmRule_Free(netlist_fsm_rule_t* rule);

netlist_fsm_func_t* Netlist_Comp_Fsm_Func_New(netlist_comp_t* comp, char* name);
void Netlist_Comp_Fsm_Func_Free(netlist_comp_t* comp, netlist_fsm_func_t* fsmfunc);
netlist_fsm_func_t* Netlist_Comp_Fsm_Func_Get(netlist_comp_t* comp, char* name);

void Netlist_Comp_Fsm_RemoveAction(netlist_fsm_action_t* action);
void Netlist_Comp_Fsm_RemoveOutput(netlist_port_t* port);

void Netlist_Comp_Fsm_SetStateIdx(netlist_comp_t* comp);
void Netlist_Comp_Fsm_BuildRtmCounters(netlist_comp_t* comp);
void Netlist_Comp_Fsm_PrevRules_Build(netlist_comp_t* comp);
netlist_fsm_state_t* Netlist_Comp_Fsm_Rule_GetFatherState(netlist_fsm_rule_t* rule);

netlist_comp_model_t* Netlist_Comp_Fsm_GetModel();



//======================================================================
// Signal (empty component, only wire)
//======================================================================

extern netlist_comp_model_t* NETLIST_COMP_SIGNAL;

typedef struct netlist_signal_t  netlist_signal_t;
struct netlist_signal_t {
	unsigned width;
	// Directly accessible ports : Data input and output
	netlist_port_t* port_out;
	netlist_port_t* port_in;
};

netlist_comp_t* Netlist_Comp_Sig_New(char* name, unsigned width);

netlist_comp_model_t* Netlist_Comp_Sig_GetModel();

static inline netlist_val_cat_t* Netlist_ValCat_Make_Sig(netlist_comp_t* comp) {
	netlist_signal_t* sig_data = (netlist_signal_t*)comp->data;
	return Netlist_ValCat_Make_FullPort(sig_data->port_out);
}
static inline chain_list*        Netlist_ValCat_MakeList_Sig(netlist_comp_t* comp) {
	netlist_val_cat_t* valcat = Netlist_ValCat_Make_Sig(comp);
	return addchain(NULL, valcat);
}



//======================================================================
// Multiplexer, decoded
//======================================================================

extern netlist_comp_model_t* NETLIST_COMP_MUX;

// FIXME the structure of the MUX allows to perform a logic operation for each input

// Access structure representing a MUX input.
typedef struct netlist_mux_access_t {
	unsigned        index;
	bool            is_default;
	netlist_port_t* port_data;
	netlist_port_t* port_en;  // Optional if default input
} netlist_mux_access_t;

typedef struct netlist_mux_t  netlist_mux_t;
struct netlist_mux_t {
	unsigned inputs_nb;
	unsigned width;
	// Index to generate names of input accesses
	unsigned input_name_idx;

	// Value to output when no input is enabled
	// Indicate a default port, or a default value (internal literal)
	netlist_access_t* default_input;
	char* default_value;

	// Directly accessible ports : Output
	netlist_port_t* port_out;
};

netlist_comp_t* Netlist_Mux_New(char* name, unsigned width);

netlist_access_t* Netlist_Mux_AddInput(netlist_comp_t* comp);
void Netlist_Mux_RemoveInput(netlist_access_t* access);

netlist_access_t* Netlist_Mux_GetInput(netlist_comp_t* comp, chain_list* valcat);
netlist_access_t* Netlist_Mux_GetAddInput(netlist_comp_t* comp, chain_list* valcat);

void Netlist_Mux_SetDefaultInput(netlist_comp_t* comp, netlist_access_t* access);
void Netlist_Mux_SetDefaultValue(netlist_comp_t* comp, char* value);

netlist_comp_model_t* Netlist_Comp_Mux_GetModel();
netlist_access_model_t* Netlist_Access_MuxIn_GetModel();



//======================================================================
// Register
//======================================================================

extern netlist_comp_model_t* NETLIST_COMP_REGISTER;

typedef struct netlist_register_t  netlist_register_t;
struct netlist_register_t {
	unsigned width;

	// Mandatory ports
	netlist_port_t* port_out;
	netlist_port_t* port_in;
	netlist_port_t* port_clk;
	// Note: the EN port is optional. If NULL, it means data is stored at each clock edge.
	netlist_port_t* port_en;

	// The (optional) reset is asynchronous, because it uses SET/CLEAR inputs of fpga primitives
	netlist_port_t* port_reset;

	// An optional value to set at init or reset
	char* init_value;

	// Note: Nothing here about boundary scan (yet)
};

netlist_comp_t* Netlist_Comp_Reg_New(char* name, unsigned width);

netlist_comp_model_t* Netlist_Comp_Reg_GetModel();

static inline netlist_val_cat_t* Netlist_ValCat_Make_Reg(netlist_comp_t* comp) {
	netlist_register_t* reg_data = (netlist_register_t*)comp->data;
	return Netlist_ValCat_Make_FullPort(reg_data->port_out);
}
static inline chain_list*        Netlist_ValCat_MakeList_Reg(netlist_comp_t* comp) {
	netlist_val_cat_t* valcat = Netlist_ValCat_Make_Reg(comp);
	return addchain(NULL, valcat);
}



//======================================================================
// Multi-port Memory
//======================================================================

extern netlist_comp_model_t* NETLIST_COMP_MEMORY;

extern netlist_access_model_t* NETLIST_ACCESS_MEM_RA;
extern netlist_access_model_t* NETLIST_ACCESS_MEM_WA;
extern netlist_access_model_t* NETLIST_ACCESS_MEM_D;
extern netlist_access_model_t* NETLIST_ACCESS_MEM_CAM;  // Content Access Memory

// To access an external memory component
extern netlist_access_model_t* NETLIST_ACCESS_MEM_RA_EXT;
extern netlist_access_model_t* NETLIST_ACCESS_MEM_WA_EXT;

// One multi-port memory bank.
// Accessed by address, direct access or content access.

typedef struct netlist_access_mem_addr_t {
	netlist_port_t* port_e;
	netlist_port_t* port_d;
	netlist_port_t* port_a;
} netlist_access_mem_addr_t;

typedef struct netlist_access_mem_direct_t {
	unsigned        cell_index;
	netlist_port_t* port_wd;
	netlist_port_t* port_we;
	netlist_port_t* port_rd;
} netlist_access_mem_direct_t;

typedef struct netlist_access_mem_cam_t {
	char*           internal_mask;
	char*           internal_data;
	netlist_port_t* port_data;
	netlist_port_t* port_mask;
	netlist_port_t* port_out;  // One bit per cell
} netlist_access_mem_cam_t;

typedef struct netlist_memory_t    netlist_memory_t;
struct netlist_memory_t {
	unsigned  data_width;
	unsigned  addr_width;
	unsigned  cells_nb;
	unsigned  ports_wa_nb;
	unsigned  ports_ra_nb;
	bool      direct_en;
	unsigned  ports_cam_nb;

	// Directly accessible ports : Clock
	netlist_port_t* port_clk;

	// The memory has a higher-level notion of 'access port'.
	// An access port is a set of ports : Data/address/enable, or data/address
	// These access ports are numbered with :
	//   - The cell index for direct access
	//   - the index of the port for access by address

	// The access ports
	avl_p_tree_t access_ports_wa;
	avl_p_tree_t access_ports_ra;
	avl_ip_tree_t access_ports_d;
	avl_p_tree_t access_ports_cam;  // Content-access memory

	// Also, boundary scan could be enabled (also enabling direct access).

	// The INIT values, stored as VEX
	// Note: it is not stored in the direct access structure because of the size overhead for simple ROM.
	hvex_t** init_vex;
};

netlist_comp_t* Netlist_Comp_Mem_New(char* name, unsigned data_width, unsigned cells_nb, unsigned addr_width);

netlist_comp_model_t* Netlist_Comp_Mem_GetModel();

void Netlist_Mem_InitValues_Free(netlist_comp_t* comp);

netlist_access_t* Netlist_Mem_AddAccess_RA(netlist_comp_t* comp);
netlist_access_t* Netlist_Mem_AddAccess_WA(netlist_comp_t* comp);
netlist_access_t* Netlist_Mem_AddAccess_Cam(netlist_comp_t* comp, char* hard_mask, char* hard_data);

void Netlist_Mem_EnableDirect(netlist_comp_t* comp);
netlist_access_t* Netlist_Mem_GetDirectAccess(netlist_comp_t* comp, unsigned index);

netlist_access_t* Netlist_AddAccess_RA_Ext(netlist_comp_t* comp, unsigned data_width, unsigned addr_width, char* name);
netlist_access_t* Netlist_AddAccess_WA_Ext(netlist_comp_t* comp, unsigned data_width, unsigned addr_width, char* name);

void Netlist_Access_MemRA_Remove(netlist_access_t* access);
void Netlist_Access_MemWA_Remove(netlist_access_t* access);
void Netlist_Access_MemDirect_Remove(netlist_access_t* access);

netlist_comp_t* Netlist_Mem_AccessRA_GetReadBuf(netlist_access_t* access);
bool Netlist_Mem_IsSimpleSyncRead(netlist_comp_t* comp);

netlist_access_model_t* Netlist_Access_MemRA_GetModel();
netlist_access_model_t* Netlist_Access_MemWA_GetModel();
netlist_access_model_t* Netlist_Access_MemD_GetModel();
netlist_access_model_t* Netlist_Access_MemCam_GetModel();

netlist_access_model_t* Netlist_Access_MemRA_Ext_GetModel();
netlist_access_model_t* Netlist_Access_MemWA_Ext_GetModel();



//======================================================================
// ADD, SUB, ALU
//======================================================================

extern netlist_comp_model_t* NETLIST_COMP_ADD;
extern netlist_comp_model_t* NETLIST_COMP_SUB;

// Anything that fits in one carry chain and its associated LUTs

// FIXME : Missing the operations
//  (A logic B) +/- cst
//  cst +/- (A logic B)
//  cst +/- logic(A, B, cst)
//  cst +/- logic((A logic cst), (B logic cst), cst)
//  comparisons: the carry chain can propagate a flag meaning eq, ne, ge, gt, le, lt

// FIXME the FSM extension could be implemented in a more simple and generic way:
//   only store the LUT config bits to define a functionality
// To map an expression, generate the corresponding config bits,
//   and search if it is already present in the FSM extension


typedef struct netlist_asb_fsm_t {

	// Only 8 operations are possible with the carry chain.
	// The width of the FSM port is fixed: 3.

	netlist_port_t* port_fsm;

	// When the value is full of '-', it is the default operation.

	// Arithmetic operations
	char* value_AaddB;
	char* value_AminB;
	char* value_BminA;

	// The carry chain can be used to compare A and B, result is carry out.
	char* value_eq;

	// Other arithmetic instructions with fixed parameters.
	char* value_AaddLit;
	char* lit_addA;
	char* value_BaddLit;
	char* lit_addB;
	char* value_LitminA;
	char* lit_minA;
	char* value_LitminB;
	char* lit_minB;

	// Logic operations
	// The carry chain must be loaded with a '0'.

	char* value_and;
	char* value_nand;
	char* value_or;
	char* value_nor;
	char* value_xor;
	char* value_nxor;
	char* value_notA;
	char* value_notB;

	// Same operations as above, but internally invert the input A.
	char* value_andN;
	char* value_nandN;
	char* value_orN;
	char* value_norN;
	char* value_xorN;
	char* value_nxorN;

	// Embed a tiny MUX
	char* value_A;
	char* value_B;

} netlist_asb_fsm_t;

typedef struct netlist_asb_sign_t {

	// Notes :
	// - Only applies when a subtraction is done. Inputs and outputs are signed.
	// - GE = Greater of Equal (A >= B), OV = Overflow
	// These outputs are computed from :
	// - MSB of both inputs
	// - MSB of result

	// When the FSM command is used, another bit could identify AminB and BminA.
	// This structure could have additional fields for that.

	// Input ports
	netlist_port_t* port_sign;  // '1' means the operation is signed.
	// '1' means A = B, given by an external comparator.
	// If NULL (or left unlinked) but still required, a dedicated comparator is embedded.
	netlist_port_t* port_extern_eq;

	// Output ports
	netlist_port_t* port_ov;  // Overflow
	netlist_port_t* port_gt;  // A > B
	netlist_port_t* port_ge;  // A >= B
	netlist_port_t* port_lt;  // A < B
	netlist_port_t* port_le;  // A <= B

} netlist_asb_sign_t;

typedef struct netlist_asb_t {

	// The width of the carry chain, of the inputs, and output.
	unsigned width;

	// Directly accessible ports
	netlist_port_t* port_out;

	netlist_port_t* port_inA;
	netlist_port_t* port_inB;

	char default_ci;
	netlist_port_t* port_ci;  // Input Carry
	netlist_port_t* port_co;  // Output Carry

	// To enable "greater than", and "overflow" functionalities, with "eq"
	netlist_asb_sign_t* sign_extend;

	// FSM input, for ASB and ALU mode
	netlist_asb_fsm_t* fsm_extend;

} netlist_asb_t;

netlist_comp_t* Netlist_Add_New(char* name, unsigned width, bool with_ci, bool with_co);
netlist_comp_t* Netlist_Sub_New(char* name, unsigned width, bool with_ci, bool with_co);
//netlist_comp_t* Netlist_Alu_New(char* name, unsigned width, bool with_ci, bool with_co);

void Netlist_Asb_SetDefaultCI(netlist_comp_t* comp, char bit);

void Netlist_Asb_EnableSign(netlist_comp_t* comp, bool ext_eq);
//void Netlist_Asb_EnableFSM(netlist_comp_t* comp);

void Netlist_Asb_RemoveSign(netlist_comp_t* comp);

netlist_comp_model_t* Netlist_Add_GetModel();
netlist_comp_model_t* Netlist_Sub_GetModel();



//======================================================================
// MUL, signed or unsigned
//======================================================================

extern netlist_comp_model_t* NETLIST_COMP_MUL;

typedef struct netlist_mul_t {

	// Structural parameters
	unsigned width_out;
	unsigned width_inA;
	unsigned width_inB;
	bool     is_signed;

	// Directly accessible ports
	netlist_port_t* port_out;
	netlist_port_t* port_inA;
	netlist_port_t* port_inB;

} netlist_mul_t;

netlist_comp_t* Netlist_Mul_New(char* name, unsigned width_inA, unsigned width_inB, unsigned width_out, bool is_signed);

netlist_comp_model_t* Netlist_Mul_GetModel();



//======================================================================
// Integer divisions
//======================================================================

extern netlist_comp_model_t* NETLIST_COMP_DIVQR;

typedef struct netlist_divqr_t {

	// Structural parameters
	unsigned width_num;
	unsigned width_den;

	// Directly accessible ports
	netlist_port_t* port_in_num;
	netlist_port_t* port_in_den;
	netlist_port_t* port_out_quo;
	netlist_port_t* port_out_rem;

} netlist_divqr_t;

netlist_comp_t* Netlist_DivQR_New(char* name, unsigned width_num, unsigned width_den);

netlist_comp_model_t* Netlist_DivQR_GetModel();



//======================================================================
// SHIFT
//======================================================================

extern netlist_comp_model_t* NETLIST_COMP_SHR;
extern netlist_comp_model_t* NETLIST_COMP_SHL;
extern netlist_comp_model_t* NETLIST_COMP_ROTR;
extern netlist_comp_model_t* NETLIST_COMP_ROTL;

// This structure works for left and right shift, arithmetic and logic shift.
// The shift value is always logical. For a non-zero padding, use the input padding port.

typedef struct netlist_shift_t {

	// Structural parameters
	unsigned width_data;
	unsigned width_shift;

	// Directly accessible ports
	netlist_port_t* port_out;
	netlist_port_t* port_in;
	netlist_port_t* port_shift;
	netlist_port_t* port_padding;

} netlist_shift_t;

netlist_comp_t* Netlist_ShL_New(char* name, unsigned width_data, unsigned width_shift);
netlist_comp_t* Netlist_ShR_New(char* name, unsigned width_data, unsigned width_shift);
netlist_comp_t* Netlist_RotL_New(char* name, unsigned width_data, unsigned width_shift);
netlist_comp_t* Netlist_RotR_New(char* name, unsigned width_data, unsigned width_shift);

netlist_comp_model_t* Netlist_ShL_GetModel();
netlist_comp_model_t* Netlist_ShR_GetModel();
netlist_comp_model_t* Netlist_RotL_GetModel();
netlist_comp_model_t* Netlist_RotR_GetModel();



//======================================================================
// CMP
//======================================================================

extern netlist_comp_model_t* NETLIST_COMP_CMP;

// Comparison of several inputs with themselves and with an optional literal.

typedef struct netlist_cmp_t {

	// Structural parameters
	unsigned width;

	// Optional : a literal value as extra input
	char* value;

	// The output (one bit)
	netlist_port_t* port_eq;  // '1' if equal
	netlist_port_t* port_ne;  // '1' if non-equal

	// Only the number of inputs is kept here.
	unsigned inputs_nb;

} netlist_cmp_t;

netlist_comp_t* Netlist_Cmp_New_Raw(char* name, unsigned width);
netlist_port_t* Netlist_Cmp_AddInput(netlist_comp_t* comp);

netlist_comp_model_t* Netlist_Cmp_GetModel();



//======================================================================
// Logic Unit, 2 inputs
//======================================================================

// This logic unit is designed to fit nicely in LUT6,
// while providing every possible logic operation.

// FIXME missing some operations : logic((A logic cst), (B logic cst), cst)
#if 0

extern netlist_comp_model_t* NETLIST_COMP_LU;

typedef struct {

	// Structural parameters
	unsigned width;

	// Optional : a literal value as extra input
	char* value;

	// The ports
	netlist_port_t* port_out;
	netlist_port_t* port_inA;
	netlist_port_t* port_inB;

	// 16 operations are possible.
	// The width if the FSM port is fixed : 4.
	// When there is no FSM port, only one operation

	netlist_port_t* port_fsm;

	// When the value is full of '-', it is the default operation.
	char* cmd_and;     // "0000"
	char* cmd_nand;    // "0001"
	char* cmd_or;      // "0010"
	char* cmd_nor;     // "0011"
	char* cmd_xor;     // "0100"
	char* cmd_nxor;    // "0101"
	char* cmd_notA;    // "0110"
	char* cmd_notB;    // "0111"

	// Same operation as above, but nternally invert the input A
	char* cmd_andN;    // "1000"
	char* cmd_nandN;   // "1001"
	char* cmd_orN;     // "1010"
	char* cmd_norN;    // "1011"
	char* cmd_xorN;    // "1100"
	char* cmd_nxorN;   // "1101"

	// Embed a tiny MUX functionality
	char* cmd_A;       // "1110"
	char* cmd_B;       // "1111"

} netlist_lu_t;

netlist_comp_t* Netlist_Lu_New(char* name, unsigned width);
void Netlist_Lu_Free_Data(netlist_comp_t* comp);
#endif



//======================================================================
// Logic operators, multi-port
//======================================================================

extern netlist_comp_model_t* NETLIST_COMP_AND;
extern netlist_comp_model_t* NETLIST_COMP_OR;
extern netlist_comp_model_t* NETLIST_COMP_NAND;
extern netlist_comp_model_t* NETLIST_COMP_NOR;
extern netlist_comp_model_t* NETLIST_COMP_XOR;
extern netlist_comp_model_t* NETLIST_COMP_NXOR;
extern netlist_comp_model_t* NETLIST_COMP_NOT;

typedef struct netlist_logic_t {

	// Structural parameters
	unsigned width;

	// Optional : a literal value as extra input
	char* value;

	// The output port
	netlist_port_t* port_out;

	// Only the number of inputs is useful here.
	unsigned inputs_nb;

} netlist_logic_t;

netlist_comp_t* Netlist_Logic_New(netlist_comp_model_t* model, char* name, unsigned width);

netlist_comp_t* Netlist_Logic_New_And(char* name, unsigned width);
netlist_comp_t* Netlist_Logic_New_Nand(char* name, unsigned width);
netlist_comp_t* Netlist_Logic_New_Or(char* name, unsigned width);
netlist_comp_t* Netlist_Logic_New_Nor(char* name, unsigned width);
netlist_comp_t* Netlist_Logic_New_Xor(char* name, unsigned width);
netlist_comp_t* Netlist_Logic_New_Nxor(char* name, unsigned width);
netlist_comp_t* Netlist_Logic_New_Not(char* name, unsigned width);

netlist_port_t* Netlist_Logic_AddInput(netlist_comp_t* comp);

netlist_comp_model_t* Netlist_And_GetModel();
netlist_comp_model_t* Netlist_Nand_GetModel();
netlist_comp_model_t* Netlist_Or_GetModel();
netlist_comp_model_t* Netlist_Nor_GetModel();
netlist_comp_model_t* Netlist_Xor_GetModel();
netlist_comp_model_t* Netlist_Nxor_GetModel();
netlist_comp_model_t* Netlist_Not_GetModel();



//======================================================================
// Binary MUX
//======================================================================

extern netlist_comp_model_t* NETLIST_COMP_MUXBIN;

typedef struct netlist_muxbin_t  netlist_muxbin_t;
struct netlist_muxbin_t {
	unsigned width_sel;
	unsigned width_data;
	unsigned inputs_nb;

	// Directly accessible ports
	netlist_port_t* port_sel;  // Selection input
	netlist_port_t* port_out;  // Output

	// All data ports
	netlist_port_t** ports_datain;
};

netlist_comp_t* Netlist_MuxBin_New(char* name, unsigned width_sel, unsigned width_data, unsigned inputs_nb);

netlist_comp_model_t* Netlist_MuxBin_GetModel();



//======================================================================
// Clock divider
//======================================================================

extern netlist_comp_model_t* NETLIST_COMP_CLKDIV;

typedef struct netlist_clkdiv_t  netlist_clkdiv_t;
struct netlist_clkdiv_t {
	unsigned divider;
	netlist_port_t* port_in;
	netlist_port_t* port_out;
};

netlist_comp_model_t* Netlist_ClkDiv_GetModel();

netlist_comp_t* Netlist_ClkDiv_New(char* name, unsigned divider);



//======================================================================
// FIFO Functions
//======================================================================

extern netlist_comp_model_t* NETLIST_COMP_FIFO;

typedef struct netlist_fifo_t  netlist_fifo_t;
struct netlist_fifo_t {
	unsigned data_width;
	unsigned cells_nb;

	// System ports
	netlist_port_t* port_clk;
	netlist_port_t* port_reset;

	// Data ports
	netlist_access_t* fifo_in;
	netlist_access_t* fifo_out;
};

netlist_comp_t* Netlist_Fifo_New(char* name, unsigned data_width, unsigned cells_nb);

netlist_comp_model_t* Netlist_Fifo_GetModel();



//======================================================================
// PingPong Buffer
//======================================================================

extern netlist_comp_model_t* NETLIST_COMP_PINGPONG_IN;
extern netlist_comp_model_t* NETLIST_COMP_PINGPONG_OUT;

typedef struct netlist_pingpong_t  netlist_pingpong_t;
struct netlist_pingpong_t {
	unsigned data_width;
	unsigned addr_width;
	unsigned banks_nb;     // Number of mem banks as circular buffer
	unsigned cells_nb;     // Number of cells per bank
	unsigned ports_ra_nb;  // Number of ports for read with address
	unsigned ports_wa_nb;  // Number of ports for write with address

	// Storage in registers intead of RAM
	// Only 2 situations are handled:
	//  - storage as RAM, only access by address (default)
	//  - storage as registers, only direct access
	bool is_direct;

	// Directly accessible ports : Clock,Reset, PingPong ports
	netlist_port_t* port_clk;
	netlist_port_t* port_reset;

	// The input to toggle banks
	netlist_port_t* port_switch;
	// FIXME: add another input to switch right when full
	// This avoids long critical path in circuit when checking fullness
	//   (or one clock cycle lost in the process)

	// These two (optional) outputs indicate fullness of the bank available to the circuit
	// They are unused for output PP.
	// Tnis is the nb of values in the bank available to the circuit
	netlist_port_t* port_ratio_c;
	// '1' if port_ratio_c = cells_nb
	netlist_port_t* port_full_c;

	// These two (optional) outputs indicate fullness of the next bank
	// Tnis is the nb of values in the next bank
	netlist_port_t* port_ratio_n;
	// The value is '1' if port_ratio_n = cells_nb (PP-IN) or port_ratio_n = 0 (PP-OUT)
	netlist_port_t* port_full_n;   //

	// An input port for the output PP.
	// Send the nb of valid values. Taken into account at switch.
	netlist_port_t* port_setmax;
	// Other functionality
	// FIXME Not implemented yet
	// port_ratio2 indicates nb of values remaining in the next bank to fill
	// port_full2  indicates if port_ratio2 is ZERO (sorry for naming)

	// The access ports
	avl_p_tree_t access_ports_ra;   // type NETLIST_ACCESS_MEM_RA
	avl_p_tree_t access_ports_wa;   // type NETLIST_ACCESS_MEM_WA
	avl_ip_tree_t access_ports_d;   // type NETLIST_ACCESS_MEM_DIRECT
	netlist_access_t* access_fifo;  // type NETLIST_ACCESS_FIFO_IN/OUT
};

netlist_comp_t* Netlist_PingPong_New(
	char* name, unsigned data_width, unsigned cells_nb, unsigned addr_width, unsigned ram_nb, bool is_input
);

netlist_comp_model_t* Netlist_Comp_PingPong_In_GetModel();
netlist_comp_model_t* Netlist_Comp_PingPong_Out_GetModel();

netlist_access_t* Netlist_PingPong_AddAccess_RA(netlist_comp_t* comp);
netlist_access_t* Netlist_PingPong_AddAccess_WA(netlist_comp_t* comp);

void Netlist_PingPong_EnableDirect(netlist_comp_t* comp);
netlist_access_t* Netlist_PingPong_GetDirectAccess(netlist_comp_t* comp, unsigned index);

void Netlist_PingPong_Remove_RA(netlist_access_t* access);
void Netlist_PingPong_Remove_WA(netlist_access_t* access);
void Netlist_PingPong_Remove_Direct(netlist_access_t* access);



//======================================================================
// UART interface
//======================================================================

// The access

extern netlist_access_model_t* NETLIST_ACCESS_UART;

typedef struct netlist_uart_access_t  netlist_uart_access_t;
struct netlist_uart_access_t {
	netlist_port_t* port_tx;
	netlist_port_t* port_rx;
	// Some config
	unsigned bits_nb;
	unsigned baudrate;
	bool     parity;
};

netlist_access_t* Netlist_Comp_AddAccess_Uart(netlist_comp_t* comp, char* access_name);

netlist_access_model_t* Netlist_Access_Uart_GetModel();

static inline void Netlist_Access_Uart_SetBaudrate(netlist_access_t* access, unsigned baud) {
	netlist_uart_access_t* uart_data = (netlist_uart_access_t*)access->data;
	uart_data->baudrate = baud;
}
static inline unsigned Netlist_Access_Uart_GetBaudrate(netlist_access_t* access) {
	netlist_uart_access_t* uart_data = (netlist_uart_access_t*)access->data;
	return uart_data->baudrate;
}

// The component

extern netlist_comp_model_t* NETLIST_COMP_UART;

typedef struct netlist_uart_t  netlist_uart_t;
struct netlist_uart_t {
	// Data width = generally 5 to 9 bits ?
	unsigned data_width;

	// Indicate whether a parity bit is used
	bool parity;

	// Divide the main clock to get a desired baudrate
	unsigned clk_divider;

	// Main system ports
	netlist_port_t* port_clk;
	netlist_port_t* port_reset;

	// UART ports
	netlist_access_t* access_uart;

	// Data ports: FIFO type
	netlist_access_t* access_data_in;
	netlist_access_t* access_data_out;
};

netlist_comp_t* Netlist_Uart_New(char* name, unsigned data_width, bool parity, unsigned divider);

netlist_comp_model_t* Netlist_Comp_Uart_GetModel();



//======================================================================
// UART interface: Standalone one-side, Rx or Tx
//======================================================================

// The configuration

typedef struct netlist_uart_side_cfg_t {
	// Main config
	unsigned        databits_nb;
	unsigned        baudrate;
	unsigned        clk_divider;
	bool            parity;
	// Some config that should be let to default
	bool            uart_le;

	// FIXME Not used yet: Extension for control flow
	bool            control_en;
	char            control_active_level;

	// FIXME Not used yet: Internal buffer size. Zero means none specified (and default to 1).
	// For RX, width is the UART number of bits. For TX, width is fifo width.
	// Note: Maybe an internal buffer is needed for proper CTS/RTS handling
	unsigned buffer_size;

	// When the channel is used to transmit values larger than the uart bits_nb
	// These fields can also be used to send data shorter than the fifo width
	unsigned elts_nb;      // If zero, it defaults to the min number to cover the FIFO width
	bool     elts_le;      // Endianness
	char     elts_pad;

} netlist_uart_side_cfg_t;

// The access structure

typedef struct netlist_access_uart_side_t  netlist_access_uart_side_t;
struct netlist_access_uart_side_t {
	// All config
	netlist_uart_side_cfg_t cfg;
	// The main UART port
	netlist_port_t* port_uart;
	// FIXME Not used yet: Extension for control flow
	netlist_port_t* port_cts;
	netlist_port_t* port_rts;
};

extern netlist_access_model_t* NETLIST_ACCESS_UART_RX;
extern netlist_access_model_t* NETLIST_ACCESS_UART_TX;

netlist_access_model_t* Netlist_Access_Uart_Rx_GetModel();
netlist_access_model_t* Netlist_Access_Uart_Tx_GetModel();

// Functions specific to this access model

netlist_access_t* Netlist_Comp_AddAccess_Uart_Rx(netlist_comp_t* comp, char* access_name);
netlist_access_t* Netlist_Comp_AddAccess_Uart_Tx(netlist_comp_t* comp, char* access_name);

static inline netlist_uart_side_cfg_t* Netlist_Access_UartSide_GetCfg(netlist_access_t* access) {
	netlist_access_uart_side_t* uartacc_data = (netlist_access_uart_side_t*)access->data;
	return &uartacc_data->cfg;
}

// The components

typedef struct netlist_uart_side_t  netlist_uart_side_t;
struct netlist_uart_side_t {
	// This points to the config in the UART SIDE access structure. For faster access.
	netlist_uart_side_cfg_t* cfg;
	// Main system ports
	netlist_port_t* port_clk;
	netlist_port_t* port_reset;
	// UART access, Rx or Tx
	netlist_access_t* access_uart;
	// The FIFO for circuit-side communication
	netlist_access_t* access_fifo;
};

extern netlist_comp_model_t* NETLIST_COMP_UART_RX;
extern netlist_comp_model_t* NETLIST_COMP_UART_TX;

netlist_comp_model_t* Netlist_Comp_Uart_Rx_GetModel();
netlist_comp_model_t* Netlist_Comp_Uart_Tx_GetModel();

// Functions specific to this component model

netlist_comp_t* Netlist_Uart_Rx_New(char* name, unsigned fifo_width);
netlist_comp_t* Netlist_Uart_Tx_New(char* name, unsigned fifo_width);

static inline netlist_uart_side_cfg_t* Netlist_UartSide_GetCfg(netlist_comp_t* comp) {
	netlist_uart_side_t* uart_data = (netlist_uart_side_t*)comp->data;
	return uart_data->cfg;
}

static inline unsigned Netlist_UartSide_GetBaudrate(netlist_comp_t* comp) {
	netlist_uart_side_cfg_t* cfg = Netlist_UartSide_GetCfg(comp);
	return cfg->baudrate;
}
static inline void Netlist_UartSide_SetBaudrate(netlist_comp_t* comp, unsigned baudrate) {
	netlist_uart_side_cfg_t* cfg = Netlist_UartSide_GetCfg(comp);
	cfg->baudrate = baudrate;
}



//======================================================================
// Input buffer for differential clock
//======================================================================

extern netlist_comp_model_t* NETLIST_COMP_DIFFCLOCK;

typedef struct netlist_diffclock_t  netlist_diffclock_t;
struct netlist_diffclock_t {
	netlist_port_t* port_clk_out;
	netlist_access_t* access_diffclock;
};

netlist_comp_model_t* Netlist_Comp_DiffClock_GetModel();

netlist_comp_t* Netlist_Comp_DiffClock_New(char* name);



#endif  // _NETLIST_COMPS_H_

