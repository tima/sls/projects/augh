
#ifndef _NETLIST_VHDL_H_
#define _NETLIST_VHDL_H_

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>

#include "../chain.h"
#include "netlist.h"
#include "netlist_access.h"



// Flags for Netlist_GenVHDL_ValCat_opt()
#define GENVHDL_VALCAT_USEREPEAT  0x01
#define GENVHDL_VALCAT_CHKADDPAR  0x02

struct netlist_vhdl_t {
	bool   separate;      // One VHDL file per entity
	char*  architecture;  // Name of the VHDL architecture
	// Keywords (all lowercase)
	avl_p_tree_t  reserved_keywords;
	avl_p_tree_t  libs_keywords;  // FIXME not used yet
	avl_p_tree_t  global_ids;  // all component model names and component names
	// Component names more appropriate for VHDL, and guarantee no collisions
	// Key = netlist_comp_t*, Data = a bitype_list*
	//   DATA_FROM = name of the VHDL component
	//   DATA_TO = name of the instance of that component
	avl_pp_tree_t  comp_rename;
	char*          vhdl_prefix;  // A prefix to all VHDL component and instance names
	// Some flags to specify the generated VHDL style
	bool inline_comps;
	bool objective_simu;
};

void Netlist_GenParams_Init(netlist_vhdl_t* params);
void Netlist_GenParams_Clear(netlist_vhdl_t* params);

void Netlist_AddKeywords_VHDL(avl_p_tree_t* tree);
void Netlist_AddKeywords_Verilog(avl_p_tree_t* tree);

bitype_list* Netlist_Comp_Rename(netlist_comp_t* comp, netlist_vhdl_t* params);
bitype_list* Netlist_Comp_GetRename(netlist_comp_t* comp, netlist_vhdl_t* params);

char* Netlist_Comp_GetRename_Comp(netlist_comp_t* comp, netlist_vhdl_t* params);
char* Netlist_Comp_GetRename_Inst(netlist_comp_t* comp, netlist_vhdl_t* params);

void Netlist_GenVHDL_StdLibs(FILE* F);
void Netlist_GenVHDL_MyToInteger(FILE* F);
void Netlist_GenVHDL_MySignedUnsigned(FILE* F);
void Netlist_GenVHDL_MyRepeat(FILE* F);

char* Netlist_GenVHDL_ClockEdge_TestFunc(int edge);
char* Netlist_GenVHDL_ClockEdge_TestFunc_port(netlist_port_t* port);

void Netlist_GenVHDL_ValCat_opt(FILE* F, netlist_comp_t* comp, netlist_val_cat_t* valcat, netlist_vhdl_t* params, int flags);
void Netlist_GenVHDL_ValCat_List_opt(FILE* F, netlist_comp_t* comp, chain_list* list, netlist_vhdl_t* params, int flags);

static inline void Netlist_GenVHDL_ValCat(FILE* F, netlist_comp_t* comp, netlist_val_cat_t* valcat, netlist_vhdl_t* params) {
	Netlist_GenVHDL_ValCat_opt(F, comp, valcat, params, 0);
}
static inline void Netlist_GenVHDL_ValCat_List(FILE* F, netlist_comp_t* comp, chain_list* list, netlist_vhdl_t* params) {
	Netlist_GenVHDL_ValCat_List_opt(F, comp, list, params, 0);
}

void Netlist_GenVHDL_StdLogic_Init(FILE* F, unsigned width, char init_val);
void Netlist_GenVHDL_StdLogic_InitArray(FILE* F, unsigned width, char init_val);
void Netlist_GenVHDL_StdLogic_InitArray2(FILE* F, unsigned width, char init_val);

void Netlist_GenVHDL_StdLogicType(FILE* F, unsigned width);
void Netlist_GenVHDL_StdLogicType_be(FILE* F, unsigned width, char* beg, char* end);
void Netlist_GenVHDL_StdLogicType_ibe(FILE* F, unsigned width, char init_val, char* beg, char* end);

void Netlist_GenVHDL_StdLogic_Literal_Repeat(FILE* F, unsigned width, char val);
void Netlist_GenVHDL_StdLogic_Literal_Repeat_be(FILE* F, unsigned width, char val, char* beg, char* end);
void Netlist_GenVHDL_StdLogic_Literal(FILE* F, char* value);
void Netlist_GenVHDL_StdLogic_Literal_be(FILE* F, char* value, char* beg, char* end);

void Netlist_GenVHDL_CompDecl(FILE* F, netlist_comp_t* comp, netlist_vhdl_t* params);
void Netlist_GenVHDL_CompInstance(FILE* F, netlist_comp_t* comp, netlist_vhdl_t* params);
void Netlist_GenVHDL_Entity(FILE* F, netlist_comp_t* comp, netlist_vhdl_t* params);

void Netlist_GenVHDL_fp(FILE* F, netlist_vhdl_t* params, netlist_comp_t* comp);
FILE* Netlist_GenVHDL_OpenFile(netlist_comp_t* comp, netlist_vhdl_t* params);

void Netlist_GenVHDL(netlist_comp_t* comp, netlist_vhdl_t* params);
void Netlist_GenVHDL_simple(netlist_comp_t* comp);

// Default VHDL generation function that can be used to initialize component models
void Netlist_Comp_GenVHDL_Default(FILE* F, netlist_vhdl_t* params, netlist_comp_t* comp);



#endif  // _NETLIST_VHDL_H_

