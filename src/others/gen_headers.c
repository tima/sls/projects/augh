
#include <stdio.h>
#include <stdlib.h>
#include <string.h>


int main() {

	FILE* F = fopen("augh_types.h", "wb");
	if(F==NULL) {
		printf("ERROR: Couldn't create the file augh_types.h\n");
		abort();
	}

	fprintf(F,
		"\n"
		"#ifndef _AUGH_TYPES_H_\n"
		"#define _AUGH_TYPES_H_\n"
		"\n"
		"\n"
		"// Data types\n"
		"// This is an extension of the C99 standard types\n"
		"\n"
	);

	for(unsigned i=1; i<=8; i++) fprintf(F, "typedef char int%u_t;\n", i);
	fputc('\n', F);
	for(unsigned i=1; i<=8; i++) fprintf(F, "typedef unsigned char uint%u_t;\n", i);
	fputc('\n', F);

	for(unsigned i=9; i<=16; i++) fprintf(F, "typedef short int int%u_t;\n", i);
	fputc('\n', F);
	for(unsigned i=9; i<=16; i++) fprintf(F, "typedef unsigned short int uint%u_t;\n", i);
	fputc('\n', F);

	for(unsigned i=17; i<=32; i++) fprintf(F, "typedef int int%u_t;\n", i);
	fputc('\n', F);
	for(unsigned i=17; i<=32; i++) fprintf(F, "typedef unsigned int uint%u_t;\n", i);
	fputc('\n', F);

	for(unsigned i=33; i<=64; i++) fprintf(F, "typedef long long int int%u_t;\n", i);
	fputc('\n', F);
	for(unsigned i=33; i<=64; i++) fprintf(F, "typedef unsigned long long int uint%u_t;\n", i);
	fputc('\n', F);

	fprintf(F,
		"typedef uint1_t bool;\n"
    "\n"
		"#define false 0\n"
		"#define true  1\n"
		"\n"
		"\n"
		"#endif  // _AUGH_TYPES_H_\n"
		"\n"
	);

	fclose(F);

	return 0;
}

