
#ifndef _AUGH_SIMU_H_
#define _AUGH_SIMU_H_


// Data types

#include <stdint.h>
#include <stdbool.h>


// Needed standard includes

#include <stdio.h>


// Some function declarations
// All accesses are little endian

// IO on FIFO (actually GALS channel)

#define augh_read(file, pdata) do { \
	typeof(*(file)) augh_read_buf; \
	for(unsigned augh_read_i=0; augh_read_<sizeof(*(file)); augh_read_++) \
		((char*)&augh_read_buf)[augh_read_i] = fgetc((FILE*)file); \
	*(pdata) = augh_read_buf;
} while(0)

#define augh_write(file, pdata) do { \
	typeof(*(file)) augh_write_buf = *(pdata); \
	for(unsigned augh_write_i=0; augh_write_i<sizeof(*(file)); augh_write_i++) \
		fputc((char*)&augh_write_buf)[augh_write_i], (FILE*)file); \
} while(0)

#define fifo_read(file, pdata) augh_read(file, pdata)
#define fifo_write(file, pdata) augh_write(file, pdata)


// IO on arrays

#define augh_read_vector(file, array, nb) do { \
	for(unsigned augh_read_iv=0; augh_read_iv<(nb); augh_read_iv++) \
		augh_read(file, &((array)[augh_read_iv])); \
} while(0)

#define augh_write_vector(file, array, nb) do { \
	for(unsigned augh_write_iv=0; augh_write_iv<(nb); augh_write_iv++) \
		augh_write(file, &((array)[augh_write_iv])); \
} while(0)

#define fifo_read_vector(file, array, nb) augh_read_vector(file, array, nb)
#define fifo_write_vector(file, array, nb) augh_write_vector(file, array, nb)


#endif  // _AUGH_SIMU_H_

