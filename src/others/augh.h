
#ifndef _AUGH_H_
#define _AUGH_H_


#ifdef AUGH_SYNTHESIS
	#include "augh_synth.h"
#else
	#include "augh_simu.h"
#endif


#endif  // _AUGH_H_

