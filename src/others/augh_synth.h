
#ifndef _AUGH_SYNTH_H_
#define _AUGH_SYNTH_H_


// Data types

#include "augh_types.h"


// Some function declarations

// IO on FIFO (actually GALS channel)

void augh_read(int fifo, void* data);
void augh_write(int fifo, void* data);

#define fifo_read(f, d) augh_read(f, d)
#define fifo_write(f, d) augh_write(f, d)


// IO on arrays

void augh_read_vector(int fifo, void* input, int nb);
void augh_write_vector(int fifo, void* input, int nb);

#define fifo_read_vector(f, i, n) augh_read_vector(f, i, n)
#define fifo_write_vector(f, i, n) augh_write_vector(f, i, n)


// Non-blocking IO on FIFO (actually GALS channel)

bool augh_tryread(int fifo, void* data);
bool augh_trywrite(int fifo, void* data);
void aughin_setready(int fifo);
void aughout_setready(int fifo);
void aughin_setreadyval(int fifo, bool val);
void aughout_setreadyval(int fifo, bool val);
void aughin_spydata(int fifo, void* data);
void aughout_setdata(int fifo, void* data);
bool aughin_spyready(int fifo);
bool aughout_spyready(int fifo);

#define fifo_tryread(f, d)   augh_tryread(f, d)
#define fifo_trywrite(f, d)  augh_trywrite(f, d)
#define fifoin_setready(f)   aughin_setready(f)
#define fifoout_setready(f)  aughout_setready(f)
#define fifoin_setreadyval(f, d)   aughin_setreadyval(f, d)
#define fifoout_setreadyval(f, d)  aughout_setreadyval(f, d)
#define fifoin_spydata(f, d)   aughin_spydata(f, d)
#define fifoout_setdata(f, d)  aughout_setdata(f, d)
#define fifoin_spyready(f)   aughin_spyready(f)
#define fifoout_spyready(f)  aughout_spyready(f)


// Utility functions

void sleep(unsigned sec_nb);
void usleep(unsigned usec_nb);
void nsleep(unsigned nsec_nb);
void cysleep(unsigned cycles_nb);


// Top-level interfaces

void augh_port_in(int var);
void augh_port_out(int var);

void augh_access_fifo_in(int var);
void augh_access_fifo_out(int var);

void augh_access_uart_rx(int var);
void augh_access_uart_tx(int var);


#endif  // _AUGH_SYNTH_H_

