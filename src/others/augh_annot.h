
/* declare the variables for user annotations

The variables are declared as static, so compilation for simulation works.
Note: Maybe function calls could also be used as annotation.

*/

#ifndef _AUGH_ANNOT_H_
#define _AUGH_ANNOT_H_


static unsigned int augh_wired;
static unsigned int augh_branch_wired;
static unsigned int augh_parallel;
static unsigned int augh_branch_prob;
static unsigned int augh_branch_prob_m;
static unsigned int augh_branch_prob_u;
static unsigned int augh_iter_nb;
static unsigned int augh_iter_nb_m;
static unsigned int augh_iter_nb_power2;

static unsigned int augh_toplevel;


#endif  // _AUGH_ANNOT_H_

