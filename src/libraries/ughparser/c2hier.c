
// Note: The C parser is code taken from GCC
// The GCC version can be found in cpp_main.c: version_string = "2.8.1"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <assert.h>

#include "mut.h"
#include "aut.h"   // FIXME unuseful now?

// This declares all CTree types
#include "gcp.h"

#include "c2hier.h"
#include "../../augh/auto/linelist.h"
#include "../../augh/hvex/hvex_misc.h"



//===========================================================
// Declaration of constants and macros
//===========================================================

#define C2HIER_CTREE_DONE_MASK  0x00100000

#define C2Hier_IsNodeDone(N)        ((N)->COMMON.FLAGS &   C2HIER_CTREE_DONE_MASK)
#define C2Hier_SetNodeDone(N)   do { (N)->COMMON.FLAGS |=  C2HIER_CTREE_DONE_MASK; } while(0)
#define C2Hier_ClearNodeDone(N) do { (N)->COMMON.FLAGS &= ~C2HIER_CTREE_DONE_MASK; } while(0)

#define ctree_foreach(list, iter) for(typeof(list) iter=list; iter!=NULL; iter=CTreeChain(iter))



//===========================================================
// Declaration of shared variables
//===========================================================

// Key = c_tree_node*, Data = hvex_t*
// Note: for structures, the VEX is a CONCAT operation, each element is a field
// FIXME How is it stored for unions?
static avl_pp_tree_t shC2Hier_Type2Vex;
static avl_pp_tree_t shC2Hier_Field2Vex;
// Similar to shC2Hier_Type2Vex, but stores the min array size for netlist component
// Example: array[3] -> 3
// Example: array[3][5] -> 3*8 = 18
static authtable *shC2Hier_Type2CellsNb = NULL;

// Flag to avoid cyclic inlining of functions
static authtable *shC2Hier_CyclicFunc = NULL;

static int shC2Hier_ModeNoAsg = 0;  // FIXME This is not taken into account
static int C2Hier_DeclPointer = 0;
static int C2Hier_DeclIllegal = 0;

typedef struct c2hier_funcinfo_t {
	char* name;
	c_tree_node* cnode;
	hier_node_t* nodeProc;
	int is_inline;
	int is_static;
	int is_used;
	int inlined_num;
	int only_declared;
	chain_list* list_args;  // Stored data: c2hier_declinfo_t*
	hvex_t* vex_ret;
	char* vex_ret_name;
} c2hier_funcinfo_t;

typedef struct c2hier_declinfo_t {
	char* name;          // The name of the declared stuff, not the component name
	char* netlist_name;  // The name of the component that will be created if checks pass
	c_tree_node* cnode;
	netlist_comp_t* comp;
	hvex_t* vex_atom;
	hvex_t* vex_init;
	// Some flags
	int is_const;
	int is_pointer;
	int is_static;
	int is_extern;
	// If member of function
	int is_func_arg;
	c2hier_funcinfo_t* func;
	// For unhandled declarations
	char* error_msg;
} c2hier_declinfo_t;

// Key = c_tree_node*, data = declinfo*
static avl_pp_tree_t shDeclInfo;
static avl_pp_tree_t shName2DI;

static c2hier_declinfo_t* AddDeclInfo_FromDecl(c_tree_node* DeclNode) {
	c2hier_declinfo_t* declinfo = calloc(1, sizeof(*declinfo));
	declinfo->cnode = DeclNode;
	avl_pp_add_overwrite(&shDeclInfo, DeclNode, declinfo);
	return declinfo;
}
static c2hier_declinfo_t* GetDeclInfo_FromDecl(c_tree_node *DeclNode) {
	avl_pp_node_t* pp_node = NULL;
	bool b = avl_pp_find(&shDeclInfo, DeclNode, &pp_node);
	if(b==true) return pp_node->data;;
	return NULL;
}
static c2hier_declinfo_t* GetDeclInfo_FromName(char* name) {
	avl_pp_node_t* pp_node = NULL;
	bool b = avl_pp_find(&shName2DI, name, &pp_node);
	if(b==true) return pp_node->data;;
	return NULL;
}

// Key = c_tree_node*, data = funcinfo*
static avl_pp_tree_t shFuncInfo;

static c2hier_funcinfo_t* AddFuncInfo_FromDecl(c_tree_node* DeclNode) {
	c2hier_funcinfo_t* funcinfo = calloc(1, sizeof(*funcinfo));
	funcinfo->cnode = DeclNode;
	avl_pp_add_overwrite(&shFuncInfo, DeclNode, funcinfo);
	return funcinfo;
}
static c2hier_funcinfo_t* GetFuncInfo_FromDecl(c_tree_node* DeclNode) {
	avl_pp_node_t* pp_node = NULL;
	bool b = avl_pp_find(&shFuncInfo, DeclNode, &pp_node);
	if(b==true) return pp_node->data;;
	return NULL;
}

// Key = c_tree_node*, data = hier_node_t*
static avl_pp_tree_t shC2Hier;

static hier_node_t* GetNode_HFromC(c_tree_node *TreeNode) {
	avl_pp_node_t* pp_node = NULL;
	bool b = avl_pp_find(&shC2Hier, TreeNode, &pp_node);
	if(b==true) return pp_node->data;;
	return NULL;
}
static void AddNodes_C2H(c_tree_node *TreeNode, hier_node_t* HierNode) {
	avl_pp_add_overwrite(&shC2Hier, TreeNode, HierNode);
}

static implem_t* shImplem = NULL;
static hier_t* shHier = NULL;

static c2hier_funcinfo_t* shCurFunc = NULL;

static hier_node_t* shCurSwitch = NULL;
static hier_node_t* shCurCase = NULL;
// These two nodes are label nodes
static hier_node_t* shLabelCont = NULL;
static hier_node_t* shLabelBreak = NULL;

// For instructions ++, --, function calls, etc
static hier_node_t* shPrevNodeFirst = NULL;
static hier_node_t* shPrevNodeLast = NULL;
static hier_node_t* shNextNodeFirst = NULL;
static hier_node_t* shNextNodeLast = NULL;

static void EnqueuePrevNode(hier_node_t* node) {
	if(shPrevNodeFirst==NULL) shPrevNodeFirst = node;
	else Hier_Nodes_Link(shPrevNodeLast, node);
	shPrevNodeLast = node;
}
static void EnqueuePrevLevel(hier_node_t* node) {
	if(shPrevNodeFirst==NULL) shPrevNodeFirst = node;
	else Hier_Nodes_Link(shPrevNodeLast, node);
	shPrevNodeLast = Hier_Level_GetLastNode(node);
}
static void EnqueueNextNode(hier_node_t* node) {
	if(shNextNodeFirst==NULL) shNextNodeFirst = node;
	else Hier_Nodes_Link(shNextNodeLast, node);
	shNextNodeLast = node;
}
static void EnqueueNextInPrev() {
	if(shNextNodeFirst==NULL) return;
	if(shPrevNodeFirst==NULL) shPrevNodeFirst = shNextNodeFirst;
	else Hier_Nodes_Link(shPrevNodeLast, shNextNodeFirst);
	shPrevNodeLast = shNextNodeLast;
	shNextNodeFirst = NULL;
	shNextNodeLast = NULL;
}

static hier_action_t* Enqueue_NewStateAction(bool inprev) {
	hier_node_t* nodeState = Hier_Node_NewType(HIERARCHY_STATE);
	Hier_Lists_AddNode(shHier, nodeState);
	if(inprev==true) EnqueuePrevNode(nodeState);
	else EnqueueNextNode(nodeState);
	hier_action_t* action = Hier_Action_New();
	Hier_Action_Link(nodeState, action);
	return action;
}

static void C2Hier_SetNodeLines(hier_node_t* hnode, c_tree_node* cnode) {
	hnode->source_lines = LineList_Add(hnode->source_lines, CTreeFileName(cnode), CTreeLineNum(cnode));
}
static void C2Hier_SetActLines(hier_action_t* action, c_tree_node* cnode) {
	action->source_lines = LineList_Add(action->source_lines, CTreeFileName(cnode), CTreeLineNum(cnode));
}

static char* C2Hier_GetPrevLabelName() {
	if(shPrevNodeLast==NULL) return NULL;
	if(shPrevNodeLast->TYPE!=HIERARCHY_LABEL) return NULL;
	hier_label_t* label_data = &shPrevNodeLast->NODE.LABEL;
	return label_data->name;
}



//===========================================================
// Declaration of functions
//===========================================================

static hvex_t *C2Hier_TreeExpr(c_tree_node *TreeNode);

static void C2Hier_NodeChain(c_tree_node *TreeNode);
static void C2Hier_CDecl(c_tree_node *TreeDecl);

static hvex_t *C2Hier_TreeType2Vex(c_tree_node *TreeType);



//===========================================================
// Misc functions that work on vexexpr*
//===========================================================

static void C2Hier_RenameAllVexAtom(hvex_t* VexExpr, char* Name) {
	if(VexExpr->model==HVEX_VECTOR) hvex_vec_setname(VexExpr, Name);
	else hvex_foreach(VexExpr->operands, ScanOper) C2Hier_RenameAllVexAtom(ScanOper, Name);
}

// Warning: The returned Hvex is newly allocated
static hvex_t *C2Hier_Assign_vex(hvex_t* VexTarget, hvex_t* VexData, bool inprev, char* filename, int fileline) {
	hvex_simp(VexTarget);
	hvex_resize(VexData, VexTarget->width);

	hier_action_t* action = Enqueue_NewStateAction(inprev);

	if(VexTarget->model==HVEX_VECTOR) {
		action->expr = hvex_asg_make(hvex_dup(VexTarget), NULL, VexData, NULL);
	}
	else if(VexTarget->model==HVEX_INDEX) {
		hvex_t* vex_dest = hvex_dup(VexTarget);
		hvex_t* vex_addr = hvex_unlinkop(vex_dest->operands);
		vex_dest->model = HVEX_VECTOR;
		action->expr = hvex_asg_make(vex_dest, vex_addr, VexData, NULL);
	}
	else {
		abort();
	}

	if(filename!=NULL) {
		action->source_lines = LineList_Add(action->source_lines, filename, fileline);
	}

	return VexTarget;
}

static hvex_t *C2Hier_VexBinOper(hvex_model_t* vex_model, hvex_t *VexOper1, hvex_t *VexOper2, unsigned extrawidth) {
	// Resize operands
	int maxWidth = GetMax(VexOper1->width, VexOper2->width) + extrawidth;
	hvex_resize(VexOper1, maxWidth);
	hvex_resize(VexOper2, maxWidth);

	// Build
	hvex_t* VexExpr = hvex_newmodel_op2(vex_model, maxWidth, VexOper1, VexOper2);
	hvex_simp(VexExpr);

	// Handle signedness: if one operand is signe,d the result is signed
	if(hvex_is_signed(VexOper1)==true || hvex_is_signed(VexOper2)==true) hvex_set_signed(VexExpr);

	return VexExpr;
}



//===========================================================
// Functions for variable/argument declarations
//===========================================================

// FIXME What is this function doing?
static int C2Hier_GetMaxWidth(c_tree_node *TreeNode) {
	if(TreeNode==NULL) return 0;

	int Width = 0;
	int FirstCode = CTreeNodeFirstCode(TreeNode);

	switch(FirstCode) {

		case C_LIST_NODE:  // For array init values
		case C_DECL_NODE:
		case C_INT_CST_NODE: {
			if(CTreeType(TreeNode)) {
				hvex_t* VexExpr = C2Hier_TreeType2Vex(CTreeType(TreeNode));
				Width = VexExpr->width;
			}
			break;
		}

		case C_TYPE_NODE: {
			if(CTreeType(TreeNode)) {
				hvex_t* VexExpr = C2Hier_TreeType2Vex(TreeNode);
				Width = VexExpr->width;
			}
			break;
		}

		case C_EXPR_NODE: {
			int Code = CTreeNodeCode(TreeNode);

			switch(Code) {
				case C_CAST_EXPR: {
					if(CTreeType(TreeNode)) {
						hvex_t* VexExpr = C2Hier_TreeType2Vex(CTreeType(TreeNode));
						Width = VexExpr->width;
					}
					break;
				}
				default: {
					for(int Index=0; Index<CTreeExprNumOper(TreeNode); Index++) {
						c_tree_node* TreeScan = CTreeExprOperand(TreeNode, Index);
						int WidthAux = C2Hier_GetMaxWidth(TreeScan);
						if(Width<WidthAux) Width = WidthAux;
					}
				}
			}  // Switch(Code)
		}
	}  // Switch(FirstCode)

	if(CTreeType(TreeNode)) {
		if(Width < CTreeTypePrecision(CTreeType(TreeNode))) Width = CTreeTypePrecision(CTreeType(TreeNode));
	}

	return Width;
}

// Purpose: Rewrite the input VEX to match the given type of declaration (the VEX is not duplicated)
// FIXME Clearly describe what happens with the input when it's a multi-dimension array, etc
static hvex_t *C2Hier_TreeTypeFormat2Vex(c_tree_node* TreeDecl, hvex_t *VexExpr, c2hier_declinfo_t* declinfo) {
	c_tree_node* TreeType = CTreeType(TreeDecl);

	hvex_t* VexAtom = NULL;
	bool found = avl_pp_find_data(&shC2Hier_Type2Vex, TreeType, (void**)&VexAtom);
	if(found==false) {
		VexAtom = C2Hier_TreeType2Vex(TreeType);
	}

	long NumberBit = 0;
	NumberBit = VexAtom->width;

	int TypeCode = CTreeTypeCode(TreeType);
	switch(TypeCode) {

		case C_INTEGER_TYPE:
			break;

		case C_UNION_TYPE: {
			// Take the biggest field
			ctree_foreach(CTreeTypeValues(TreeType), TreeNode) {
				VexAtom = NULL;
				avl_pp_find_data(&shC2Hier_Type2Vex, CTreeType(TreeNode), (void**)&VexAtom);
				assert(VexAtom!=NULL);
				if(NumberBit==VexAtom->width) return C2Hier_TreeTypeFormat2Vex(TreeNode, VexExpr, declinfo);
			}
			break;
		}

		case C_RECORD_TYPE: {  // structure
			assert(VexExpr->model==HVEX_CONCAT);

			hvex_t* VexAtom = hvex_newmodel(HVEX_CONCAT, NumberBit);

			ctree_foreach(CTreeTypeValues(TreeType), TreeNode) {
				hvex_t* FirstOperand = VexExpr->operands;
				if(FirstOperand==NULL) break;  // Uninitialized
				hvex_unlinkop(FirstOperand);
				hvex_t* VexField = C2Hier_TreeTypeFormat2Vex(TreeNode, FirstOperand, declinfo);
				hvex_addop_tail(VexAtom, VexField);
			}

			// If the last fields are missing
			hvex_resize(VexAtom, NumberBit);
			hvex_simp(VexAtom);

			return VexAtom;
		}

		case C_ARRAY_TYPE: {

			// N-dimensions array, ignore the first N-1 because the array is flatten.
			if(CTreeTypeCode(CTreeType(TreeType))==C_ARRAY_TYPE) {
				return C2Hier_TreeTypeFormat2Vex(TreeType, VexExpr, declinfo);
			}
			assert(VexExpr->model==HVEX_CONCAT);

			// Get the array size
			NumberBit = declinfo->vex_atom->width;
			int count = 1 << declinfo->vex_atom->operands->next->width;

			VexAtom = hvex_newmodel(HVEX_CONCAT, 0);
			chain_list* list_operands = NULL;

			while(VexExpr->operands!=NULL) {
				// Skipping extra values from initialization
				if(count<=0) break;

				hvex_t* VexField = C2Hier_TreeTypeFormat2Vex(TreeType, VexExpr, declinfo);

				// Check if size is coherent with data availability
				// FIXME What is this check?
				assert(VexField->width==NumberBit || VexExpr->operands==NULL);

				hvex_resize(VexField, NumberBit);
				list_operands = addchain(list_operands, VexField);

				VexAtom->width += VexField->width;
				VexAtom->left   = VexAtom->width - 1;
				count--;
			}

			// Read the extra values
			unsigned count_extra = 0;
			while(VexExpr->operands!=NULL) {
				hvex_t* VexField = C2Hier_TreeTypeFormat2Vex(TreeType, VexExpr, declinfo);
				hvex_free(VexField);
				count_extra++;
			}
			if(count_extra>0) {
				printf("Warning: %d extra values were skipped in initialization of '%s'.\n", count_extra, declinfo->name);
			}

			// Add zeroes to uninitialized cells
			for( ; count>0; count-- ) {
				hvex_t* VexField = hvex_newlit_repeat('0', NumberBit);
				list_operands = addchain(list_operands, VexField);
				VexAtom->width += VexField->width;
				VexAtom->width = VexAtom->width - 1;
			}

			// Set the new operands to the output VEX
			hvex_free(VexExpr);
			foreach(list_operands, scanOp) hvex_addop_head(VexAtom, scanOp->DATA);
			freechain(list_operands);

			return VexAtom;
		}

		case C_POINTER_TYPE: {
			return C2Hier_TreeTypeFormat2Vex(TreeType, VexExpr, declinfo);
		}

		case C_REAL_TYPE        :
		case C_CHAR_TYPE        :
		case C_UNKNOWN_TYPE     :
		case C_VOID_TYPE        :
		case C_COMPLEX_TYPE     :
		case C_ENUMERAL_TYPE    :
		case C_BOOLEAN_TYPE     :
		case C_OFFSET_TYPE      :
		case C_REFERENCE_TYPE   :
		case C_METHOD_TYPE      :
		case C_FILE_TYPE        :
		case C_SET_TYPE         :
		case C_QUAL_UNION_TYPE  :
		case C_FUNCTION_TYPE    :
		case C_LANG_TYPE        :

		default :
			// FIXME Use a default pointer size
			NumberBit = CTreeTypePrecision(TreeType);
			C2Hier_DeclIllegal++;
	}

	// Array of int: Only take the leaf
	// FIXME mem leak + resize should be applied on all operands?
	if(VexExpr->model==HVEX_CONCAT && declinfo->vex_atom->model==HVEX_ARRAY) {
		VexExpr = VexExpr->operands;
		hvex_unlinkop(VexExpr);
	}

	// Truncate
	if(NumberBit < VexExpr->width) {
		printf("Warning: At %s:%d: Initial value truncated from %d bits to %ld: ",
			CTreeFileName(TreeDecl), CTreeLineNum(TreeDecl), VexExpr->width, NumberBit
		);
		hvex_print(VexExpr);
		printf("\n");
	}

	hvex_resize(VexExpr, NumberBit);

	return VexExpr;
}

// Note: The returned VEX is stored somewhere else, don't modify
static hvex_t *C2Hier_TreeType2Vex(c_tree_node *TreeType) {
	hvex_t* VexAtom = NULL;
	avl_pp_find_data(&shC2Hier_Type2Vex, TreeType, (void**)&VexAtom);
	if(VexAtom!=NULL) return VexAtom;

	int TypeCode = CTreeTypeCode(TreeType);
	c_tree_node* TypeName = CTreeTypeName(TreeType);

	char* Name = NULL;
	if(TypeName!=NULL && CTreeDeclName(TypeName)!=NULL) {
		Name = CTreeIdentPointer(CTreeDeclName(TypeName));
	}
	else {
		Name = "unnamed";
	}
	Name = namealloc(Name);

	long NumberBit = 0;
	int  TypeSigned = -1;

	switch(TypeCode) {

		case C_INTEGER_TYPE: {
			// Explicitely sized type
			if     (sscanf(Name, "uint%ld", &NumberBit)==1) TypeSigned = false;
			else if(sscanf(Name, "int%ld", &NumberBit)==1) TypeSigned = true;
			else if(strcmp(Name, "bool" ) == 0 ) { NumberBit = 1; TypeSigned = false; }
			// Follow typedefs
			else if(CTreeDeclResult(TypeName)!=NULL) {
				return C2Hier_TreeType2Vex(CTreeDeclResult(TypeName));
			}
			// Size of standard C types
			else {
				NumberBit = CTreeTypePrecision(TreeType);
				TypeSigned = CIsTreeNodeUnsigned(TreeType) == 0 ? true : false;
			}
			//dbgprintf("Type '%s':%u:%c\n", Name, NumberBit, TypeSigned<0 ? '?' : TypeSigned==true ? 's' : 'u');
			break;
		}

		case C_RECORD_TYPE: {

			ctree_foreach(CTreeTypeValues(TreeType), TreeNode) {
				hvex_t* VexAtom = C2Hier_TreeType2Vex(CTreeType(TreeNode));
				NumberBit += VexAtom->width;
			}

			int FieldLeft = NumberBit - 1;

			ctree_foreach(CTreeTypeValues(TreeType), TreeNode) {
				char* FieldName = CTreeIdentPointer(CTreeDeclName(TreeNode));
				FieldName = (char*)namealloc(FieldName);

				hvex_t* VexAtom = C2Hier_TreeType2Vex(CTreeType(TreeNode));
				int FieldRigth = FieldLeft - VexAtom->width + 1;

				hvex_t* VexField = hvex_newvec(FieldName, FieldLeft, FieldRigth);

				if(hvex_is_signed(VexAtom)==true) hvex_set_signed(VexField);

				avl_pp_add_overwrite(&shC2Hier_Field2Vex, TreeNode, VexField);
				FieldLeft = FieldRigth - 1;
			}

			break;
		}

		case C_UNION_TYPE: {

			ctree_foreach(CTreeTypeValues(TreeType), TreeNode) {
				hvex_t* VexAtom = C2Hier_TreeType2Vex(CTreeType(TreeNode));
				if(NumberBit < VexAtom->width) NumberBit = VexAtom->width;
			}

			ctree_foreach(CTreeTypeValues(TreeType), TreeNode) {
				int FieldLeft = NumberBit - 1;

				char* FieldName = CTreeIdentPointer(CTreeDeclName(TreeNode));
				FieldName = namealloc(FieldName);

				hvex_t* VexAtom = C2Hier_TreeType2Vex(CTreeType(TreeNode));
				int FieldRigth = FieldLeft - VexAtom->width + 1;

				hvex_t* VexField = hvex_newvec(FieldName, FieldLeft, FieldRigth);
				if(hvex_is_signed(VexAtom)==true) hvex_set_signed(VexField);

				avl_pp_add_overwrite(&shC2Hier_Field2Vex, TreeNode, VexField);
			}

			break;
		}

		case C_ARRAY_TYPE: {
			hvex_t* VexAtom = C2Hier_TreeType2Vex(CTreeType(TreeType));
			VexAtom = hvex_dup(VexAtom);
			C2Hier_RenameAllVexAtom(VexAtom, Name);

			bool is_signed = hvex_is_signed(VexAtom);

			NumberBit = VexAtom->width;

			c_tree_node* TreeNode = CTreeTypeValues(TreeType);
			long ArrayMin = CTreeIntCstLow(CTreeTypeMinValue(TreeNode));
			long ArrayMax = CTreeIntCstLow(CTreeTypeMaxValue(TreeNode));
			long ArrayCells = ArrayMax - ArrayMin + 1;
			long ArrayAddrW = uint_bitsnb(ArrayMax - ArrayMin);

			hvex_t* VexArray = hvex_newvec(Name, ArrayAddrW - 1, 0);
			avl_pp_add_overwrite(&shC2Hier_Type2Vex, TreeType, VexArray);

			// Concat the two dimensions
			if(VexAtom->model==HVEX_ARRAY) {
				// Compute the total number of cells
				long TotalArrayCells = ((long)1 << VexAtom->operands->next->width) * ArrayCells;
				addauthelem(shC2Hier_Type2CellsNb, (char*)TreeType, (long)TotalArrayCells);

				// Create the final VEX
				hvex_t* prevOper = VexAtom->operands->next;
				hvex_unlinkop(prevOper);
				VexArray = hvex_newmodel_op2(HVEX_CONCAT, prevOper->width + ArrayAddrW, prevOper, VexArray);
				hvex_addop_tail(VexAtom, VexArray);

				return VexAtom;  // FIXME Mem leak: this is not stored in shC2Hier_Type2Vex
			}

			addauthelem(shC2Hier_Type2CellsNb, (char*)TreeType, (long)ArrayCells);
			VexAtom = hvex_newmodel_op2(HVEX_ARRAY, VexAtom->width, VexAtom, VexArray);

			if(is_signed!=0) hvex_set_signed(VexAtom);
			else hvex_set_unsigned(VexAtom);

			return VexAtom;  // FIXME Mem leak: this is not stored in shC2Hier_Type2Vex
		}

		case C_POINTER_TYPE: {
			C2Hier_DeclPointer++;
			// Return the type pointed to
			return C2Hier_TreeType2Vex(CTreeType(TreeType));
		}

		case C_CHAR_TYPE        :
		case C_UNKNOWN_TYPE     :
		case C_VOID_TYPE        :
		case C_COMPLEX_TYPE     :
		case C_ENUMERAL_TYPE    :
		case C_BOOLEAN_TYPE     :
		case C_OFFSET_TYPE      :
		case C_REFERENCE_TYPE   :
		case C_METHOD_TYPE      :
		case C_FILE_TYPE        :
		case C_SET_TYPE         :
		case C_QUAL_UNION_TYPE  :
		case C_FUNCTION_TYPE    :
		case C_LANG_TYPE        :

		default : {
			// Give a default pointer size
			NumberBit = CTreeTypePrecision(TreeType);
			C2Hier_DeclIllegal++;
			break;
		}
	}

	if(NumberBit <= 0) { C2Hier_DeclIllegal++; NumberBit = 1; }

	VexAtom = hvex_newvec(Name, NumberBit - 1, 0);

	if(TypeSigned < 0) {
		TypeSigned = CIsTreeNodeUnsigned(TreeType) == 0 ? true : false;
	}

	if(TypeSigned == true) hvex_set_signed(VexAtom);
	else hvex_set_unsigned(VexAtom);

	avl_pp_add_overwrite(&shC2Hier_Type2Vex, TreeType, VexAtom);

	return VexAtom;
}

// Note: This function allocates a new hvex expression at each call.
static hvex_t *C2Hier_TreeDecl2Vex(c_tree_node *TreeDecl) {
	c2hier_declinfo_t* declinfo = GetDeclInfo_FromDecl(TreeDecl);

	// If not declared yet, launch declaration
	if(declinfo==NULL) {
		C2Hier_CDecl(TreeDecl);
		declinfo = GetDeclInfo_FromDecl(TreeDecl);
	}

	if(declinfo==NULL) {
		char* name = CTreeIdentPointer(CTreeDeclName(TreeDecl));
		errprintfa("At %s:%d: Identifier '%s' undeclared\n", CTreeFileName(TreeDecl), CTreeLineNum(TreeDecl), name);
	}

	// Unhandled declaration? Print error message.
	if(declinfo->error_msg!=NULL) {
		errprintfa("%s", declinfo->error_msg);
	}

	c_tree_node *DeclType = CTreeType(TreeDecl);
	hvex_t* VexAtom = hvex_dup(C2Hier_TreeType2Vex(DeclType));
	C2Hier_RenameAllVexAtom(VexAtom, declinfo->netlist_name);

	return VexAtom;
}

static void C2Hier_DeclVar(c_tree_node *TreeDecl) {
	c2hier_declinfo_t* declinfo = AddDeclInfo_FromDecl(TreeDecl);
	c_tree_node* DeclIdent = CTreeDeclName(TreeDecl);
	declinfo->name = CTreeIdentPointer(DeclIdent);
	declinfo->name = stralloc(declinfo->name);

	if(shCurFunc!=NULL) {
		declinfo->func = shCurFunc;
		if(CTreeNodeCode(TreeDecl)==C_PARAM_DECL) {
			shCurFunc->list_args = ChainList_Add_Tail(shCurFunc->list_args, declinfo);
			declinfo->is_func_arg = 1;
		}
	}
	if(CIsTreeNodeReadOnly(TreeDecl)) declinfo->is_const = 1;
	if(CIsTreeNodeStatic(TreeDecl))   declinfo->is_static = 1;
	if(CIsTreeNodeExternal(TreeDecl)) declinfo->is_extern = 1;

	// Create the name of the Netlist component
	if(shCurFunc!=NULL) {
		char buf[strlen(shCurFunc->name) + strlen(declinfo->name) + 3];
		sprintf(buf, "%s_%s", shCurFunc->name, declinfo->name);
		declinfo->netlist_name = Map_Netlist_MakeInstanceName_TryThis(shImplem, namealloc(buf));
	}
	else {
		// Here it is mandatory that the name is not modified
		// Because some variables can be used for annotations
		declinfo->netlist_name = namealloc(declinfo->name);  // Take lowercase version
		if(Netlist_Comp_GetChild(shImplem->netlist.top, declinfo->netlist_name)!=NULL) {
			printf("Error: Can't handle variable '%s': a component with same name already exists\n", declinfo->netlist_name);
			exit(EXIT_FAILURE);
		}
	}

	// FIXME These variables should be removed and replaced by the flags in declinfo
	C2Hier_DeclPointer = 0;  // pointer flag
	C2Hier_DeclIllegal = 0;  // type decl flag

	hvex_t* VexAtom = C2Hier_TreeDecl2Vex(TreeDecl);
	declinfo->vex_atom = VexAtom;

	#if 0  // Display, for debug
	dbgprintf("Declaration at %s:%u : ", CTreeFileName(TreeDecl), CTreeLineNum(TreeDecl));
	hvex_print_bn(declinfo->vex_atom);
	#endif

	if(C2Hier_DeclPointer > 0) declinfo->is_pointer = 1;

	// Check bad types
	if(C2Hier_DeclIllegal > 0) {
		char buf[2048];
		sprintf(buf, "Error at %s:%d: Illegal type for variable '%s'\n",
			CTreeFileName(TreeDecl), CTreeLineNum(TreeDecl), declinfo->name
		);
		declinfo->error_msg = stralloc(buf);
		return;
	}

	// Initial values
	c_tree_node *DeclInitial = NULL;
	if(CTreeNodeCode(TreeDecl)!=C_PARAM_DECL) DeclInitial = CTreeDeclInitial(TreeDecl);
	if(DeclInitial!=NULL) {
		declinfo->vex_init = C2Hier_TreeExpr(DeclInitial);
		// Check for unhandled constructs
		if(shNextNodeFirst!=NULL) {
			errprintfa("At %s:%u: Usage of postInc/postDec operations in initialization of variables is not handled.\n",
				CTreeFileName(TreeDecl), CTreeLineNum(TreeDecl)
			);
		}
		// Resize with appropriate type
		if(declinfo->vex_init->model!=HVEX_STRING) {
			declinfo->vex_init = C2Hier_TreeTypeFormat2Vex(TreeDecl, declinfo->vex_init, declinfo);
		}
	}

	// Handle declarations like: char* ptr = "Hello World!";
	// There is no specified array length, it must be inferred from the string
	if(declinfo->vex_init!=NULL && declinfo->vex_init->model==HVEX_STRING) {
		bool is_signed = hvex_is_signed(declinfo->vex_atom);
		unsigned dataw = declinfo->vex_atom->width;

		// Change the declaration to VEX_ARRAY
		if(declinfo->vex_atom->model!=HVEX_ARRAY) {
			assert(declinfo->vex_atom->model==HVEX_VECTOR);
			hvex_t* vexaddr = hvex_dup(declinfo->vex_atom);
			hvex_resize(vexaddr, uint_bitsnb(declinfo->vex_init->width-1));
			hvex_t* newatom = hvex_newmodel_op2(HVEX_ARRAY, dataw, declinfo->vex_atom, vexaddr);
			if(is_signed==true) hvex_set_signed(newatom);
			declinfo->vex_atom = newatom;
			// Clear the pointer flag, now it's a normal array
			declinfo->is_pointer = 0;
		}

		// Save the number of cells
		c_tree_node* TreeType = CTreeType(TreeDecl);
		addauthelem(shC2Hier_Type2CellsNb, (char*)TreeType, (long)declinfo->vex_init->width);

		// Now, convert the init string to the common CONCAT operation

		unsigned NumberBit = 0;
		hvex_t* vexcat = hvex_newmodel(HVEX_CONCAT, NumberBit);
		char* str = declinfo->vex_init->data;

		// Note: First, store the operands in a list for efficiency
		chain_list* list_ops = NULL;
		for(unsigned i=0; i<declinfo->vex_init->width; i++) {
			hvex_t* VexValue = hvex_newlit_int(str[i], 8, is_signed);
			hvex_resize(VexValue, dataw);
			NumberBit += VexValue->width;
			list_ops = addchain(list_ops, VexValue);
		}

		// Now add operands to the vex node, all at the head for efficiency
		foreach(list_ops, scan) hvex_addop_head(vexcat, scan->DATA);
		freechain(list_ops);

		vexcat->left  = NumberBit-1;
		vexcat->right = 0;
		vexcat->width = NumberBit;

		// Replace the previous init value
		hvex_free(declinfo->vex_init);
		declinfo->vex_init = vexcat;
	}

	#if 0  // Display, for debug
	dbgprintf("Declaration at %s:%u : ", CTreeFileName(TreeDecl), CTreeLineNum(TreeDecl));
	hvex_print_bn(declinfo->vex_atom);
	#endif

	// Forbid pointer variable, only pointer parameter authorized
	if(declinfo->is_pointer!=0 && CTreeNodeCode(TreeDecl)!=C_PARAM_DECL) {
		// Tolerate pointer declaration but not the use
		char buf[2048];
		sprintf(buf, "Error at %s:%d: Pointer '%s' only tolerated for function parameters.\n",
			CTreeFileName(TreeDecl), CTreeLineNum(TreeDecl), declinfo->name
		);
		declinfo->error_msg = stralloc(buf);
		return;
	}

	// Create the Netlist component
	if(declinfo->is_func_arg!=0 && declinfo->is_pointer!=0) {
		// Note: A dummy component is created to reserve the name and keep it unique
		declinfo->comp = Netlist_Comp_Reg_New(declinfo->netlist_name, declinfo->vex_atom->width);
		Netlist_Comp_SetChild(shImplem->netlist.top, declinfo->comp);
	}
	else {
		if(declinfo->vex_atom->model==HVEX_ARRAY) {
			// Get the declared array size
			authelem* ElementCells = searchauthelem(shC2Hier_Type2CellsNb, (char*)CTreeType(TreeDecl));
			assert(ElementCells!=NULL);
			// Create the netlist component
			hvex_t* vex_base = declinfo->vex_atom->operands;
			hvex_t* vex_addr = declinfo->vex_atom->operands->next;
			//declinfo->comp = Netlist_Comp_Mem_New(declinfo->netlist_name, vex_base->WIDTH, (1 << vex_addr->WIDTH), vex_addr->WIDTH);
			declinfo->comp = Netlist_Comp_Mem_New(declinfo->netlist_name, vex_base->width, ElementCells->VALUE, vex_addr->width);
			Netlist_Comp_SetChild(shImplem->netlist.top, declinfo->comp);
		}
		else {
			declinfo->comp = Netlist_Comp_Reg_New(declinfo->netlist_name, declinfo->vex_atom->width);
			Netlist_Comp_SetChild(shImplem->netlist.top, declinfo->comp);
		}
	}
	// Set a flag to indicate the component can be duplicated when (later) creating multiple top-levels
	if(declinfo->func!=NULL && declinfo->is_static==0) {
		Implem_SymFlag_Add(shImplem, declinfo->netlist_name, SYM_FLAG_CANDUP);
	}
	// Set a flag to indicate the component must not be automatically converted into top-level interface
	if(declinfo->func==NULL && CIsTreeNodePublic(TreeDecl)==0) {
		Implem_SymFlag_Add(shImplem, declinfo->netlist_name, SYM_FLAG_NOEXTERN);
	}

	// Store in a tree to search by name
	avl_pp_add_overwrite(&shName2DI, declinfo->netlist_name, declinfo);

	// Checks
	if(DeclInitial==NULL && declinfo->is_const!=0) {
		errprintfa("At %s:%d: Symbol '%s' is declared const but has no initial value.\n",
			CTreeFileName(TreeDecl), CTreeLineNum(TreeDecl), declinfo->name
		);
	}
	if(DeclInitial==NULL && declinfo->func!=NULL && declinfo->is_func_arg==0 && declinfo->is_static!=0) {
		errprintfa("At %s:%d: Static variable '%s' of function '%s' has no initial value.\n",
			CTreeFileName(TreeDecl), CTreeLineNum(TreeDecl), declinfo->name, declinfo->func->name
		);
	}

	if(declinfo->vex_init!=NULL) {

		// Global symbol: Set initialization into the component
		if(declinfo->func==NULL || declinfo->is_const!=0 || (declinfo->func!=NULL && declinfo->is_static!=0)) {
			if(declinfo->comp->model==NETLIST_COMP_REGISTER) {
				// Check VEX Atom and literal
				if(declinfo->vex_init->model!=HVEX_LITERAL) {
					errprintfa("At %s:%d: Initial value for '%s' is not literal.\n",
						CTreeFileName(TreeDecl), CTreeLineNum(TreeDecl), declinfo->name
					);
				}
				// Get the full width literal
				netlist_register_t* reg_data = declinfo->comp->data;
				hvex_resize(declinfo->vex_init, reg_data->width);
				reg_data->init_value = hvex_lit_getval(declinfo->vex_init);
			}
			else if(declinfo->comp->model==NETLIST_COMP_MEMORY) {
				// Check VEX Oper and oper==VEX_CONCAT
				if(declinfo->vex_init->model!=HVEX_CONCAT) {
					errprintfa("At %s:%d: Initial value for '%s' is not a list of values.\n",
						CTreeFileName(TreeDecl), CTreeLineNum(TreeDecl), declinfo->name
					);
				}
				// Get initial values
				netlist_memory_t* mem_data = declinfo->comp->data;
				mem_data->init_vex = calloc(mem_data->cells_nb, sizeof(*mem_data->init_vex));
				unsigned idx = 0;
				unsigned excess_nb = 0;
				hvex_foreach(declinfo->vex_init->operands, VexOp) {
					if(idx >= mem_data->cells_nb) {
						excess_nb++;
						continue;
					}
					// Get the full width literal
					if(VexOp->model!=HVEX_LITERAL) {
						errprintfa("At %s:%d: Initial value of '%s' index %u is not literal.\n",
							CTreeFileName(TreeDecl), CTreeLineNum(TreeDecl), declinfo->name, idx
						);
					}
					hvex_resize(VexOp, mem_data->data_width);
					mem_data->init_vex[idx] = hvex_dup(VexOp);
					// Next index
					idx++;
				}
				if(excess_nb>0) {
					printf("Warning: At %s:%d, initialization of array '%s': there are %u values out of the array boundary.\n",
						CTreeFileName(TreeDecl), CTreeLineNum(TreeDecl), declinfo->name, excess_nb
					);
				}
			}
			else {
				abort();
			}
		}

		// Initialization of non-static symbol in a function: create Actions
		else if(declinfo->func!=NULL && declinfo->is_static==0) {
			if(declinfo->comp->model==NETLIST_COMP_REGISTER) {
				// Note: in this situation there is no need for the init value to be literal
				netlist_register_t* reg_data = declinfo->comp->data;
				hvex_resize(declinfo->vex_init, reg_data->width);
				// Create init Action
				hier_action_t* action = Enqueue_NewStateAction(true);
				action->expr = hvex_asg_make(hvex_dup(declinfo->vex_atom), NULL, hvex_dup(declinfo->vex_init), NULL);
				// Add the line information
				C2Hier_SetActLines(action, TreeDecl);
			}
			else if(declinfo->comp->model==NETLIST_COMP_MEMORY) {
				printf("Warning: At %s:%d in function '%s': Initialization of memory '%s' done with Actions because it is not declared static.\n",
					CTreeFileName(TreeDecl), CTreeLineNum(TreeDecl), declinfo->func->name, declinfo->name
				);
				printf("  This is very inefficient.\n");

				if(declinfo->vex_init->model!=HVEX_CONCAT) {
					errprintfa("At %s:%d: Initial value for '%s' is not a list of values.\n",
						CTreeFileName(TreeDecl), CTreeLineNum(TreeDecl), declinfo->name
					);
				}
				// Get initial values
				netlist_memory_t* mem_data = declinfo->comp->data;

				hvex_t* VexOper = declinfo->vex_atom->operands;
				hvex_t* VexIndex = declinfo->vex_atom->operands->next;

				unsigned idx = 0;
				unsigned excess_nb = 0;
				hvex_foreach(declinfo->vex_init->operands, scanVex) {
					if(idx >= mem_data->cells_nb) {
						excess_nb++;
						continue;
					}
					// Note: in this situation there is no need for the init value to be literal
					hvex_resize(scanVex, mem_data->data_width);
					// Create init Action
					hier_action_t* action = Enqueue_NewStateAction(true);
					action->expr = hvex_asg_make(
						hvex_dup(VexOper),
						hvex_newlit_int(idx, VexIndex->width, false),
						hvex_dup(scanVex), NULL
					);
					// Add the line information
					C2Hier_SetActLines(action, TreeDecl);
					// Next index
					idx++;
				}
				if(excess_nb>0) {
					printf("Warning: At %s:%d, initialization of array '%s': there are %u values out of the array boundary.\n",
						CTreeFileName(TreeDecl), CTreeLineNum(TreeDecl), declinfo->name, excess_nb
					);
				}
			}
			else {
				abort();
			}
		}

		else {
			abort();
		}

	}  // End handling of init values

}

static void C2Hier_DeclLabel(c_tree_node *TreeDecl) {
	// Create the Hier node that corresponds to the CTree node
	// It will be linked later in the Hier
	hier_node_t* node = Hier_Node_NewType(HIERARCHY_LABEL);
	hier_label_t* label_data = &node->NODE.LABEL;

	Hier_Lists_AddNode(shHier, node);
	AddNodes_C2H(TreeDecl, node);

	c_tree_node* DeclIdent = CTreeDeclName(TreeDecl);
	char* name = CTreeIdentPointer(DeclIdent);
	label_data->name = stralloc(name);
}



//===========================================================
// Handling of function calls
//===========================================================

static hvex_t* C2Hier_InlineCall(c_tree_node* CallNode, c2hier_funcinfo_t* funcinfo, chain_list* ChainParam) {
	//dbgprintf("At %s:%u: Inlining function '%s'\n", CTreeFileName(CallNode), CTreeLineNum(CallNode), funcinfo->name);

	// Do nothing if the function is empty
	if(funcinfo->nodeProc->NEXT==NULL) {
		if(funcinfo->vex_ret!=NULL) {
			errprintfa("At %s:%d: Trying to inline function '%s' which is empty but returns something\n",
				CTreeFileName(CallNode), CTreeLineNum(CallNode), funcinfo->name
			);
		}
		return NULL;
	}

	// Check for cyclic Inlining
	authelem* ElemFunc = searchauthelem(shC2Hier_CyclicFunc, funcinfo->name);
	if(ElemFunc!=NULL) {
		errprintfa("At %s:%d: Can't inline recursive function '%s'\n",
			CTreeFileName(CallNode), CTreeLineNum(CallNode), funcinfo->name
		);
	}

	// Count use number
	int InlinedNum = funcinfo->inlined_num++;

	// Build a new return register at each inline to avoid conflicts
	char* newretname = NULL;
	if(funcinfo->vex_ret!=NULL) {
		// Get a new name
		char buf[strlen(funcinfo->name) + 20];
		sprintf(buf, "%s_ret%u", funcinfo->name, InlinedNum);
		newretname = Map_Netlist_MakeInstanceName_Prefix(shImplem, stralloc(buf));
		// Create the new Register
		netlist_comp_t* comp = Netlist_Comp_Reg_New(newretname, funcinfo->vex_ret->width);
		Netlist_Comp_SetChild(shImplem->netlist.top, comp);
		// Set a flag to indicate the component can be duplicaated when (later) creating multiple top-levels
		Implem_SymFlag_Add(shImplem, newretname, SYM_FLAG_CANDUP);
	}

	// Link pointer arguments to their value
	avl_pp_tree_t tree_ptrarg2vex;
	avl_pp_init(&tree_ptrarg2vex);

	chain_list* scanArg = funcinfo->list_args;
	chain_list* scanParam = ChainParam;

	while(scanArg!=NULL && scanParam!=NULL) {
		c2hier_declinfo_t* declinfo = scanArg->DATA;
		hvex_t* vexParam = scanParam->DATA;

		if(declinfo->is_pointer != 0) {
			if(declinfo->vex_atom->width != vexParam->width) {
				errprintfa("At %s:%u: Incompatible data width for parameter '%s'. Expected %u, got %u.\n",
					CTreeFileName(CallNode), CTreeLineNum(CallNode), declinfo->name,
					declinfo->vex_atom->width, vexParam->width
				);
			}
			avl_pp_add_overwrite(&tree_ptrarg2vex, declinfo, vexParam);
			// Next parameter
			goto NEXT;
		}

		assert(declinfo->vex_atom->model==HVEX_LITERAL || declinfo->vex_atom->model==HVEX_VECTOR);

		vexParam = hvex_dup(vexParam);
		hvex_resize(vexParam, declinfo->vex_atom->width);

		// Create an Action to assign this argument
		hier_action_t* action = Enqueue_NewStateAction(true);
		action->expr = hvex_asg_make(hvex_dup(declinfo->vex_atom), NULL, vexParam, NULL);

		// Add the line information
		C2Hier_SetActLines(action, CallNode);
		C2Hier_SetNodeLines(action->node, CallNode);

		NEXT:
		scanArg = scanArg->NEXT;
		scanParam = scanParam->NEXT;
	}

	if(scanArg!=NULL || scanParam!=NULL) {
		errprintfa("At %s:%d: Number of arguments mismatch for function '%s'\n",
			CTreeFileName(CallNode), CTreeLineNum(CallNode), funcinfo->name
		);
	}

	// Handle the post-inc etc actions that could be in the expression of arguments
	EnqueueNextInPrev();

	// Here, duplicate the function body and insert it in the PREV nodes

	avl_pp_tree_t dup_old2new;
	avl_pp_init(&dup_old2new);

	// Note: Only call this dup function if the body is not empty (the check is already done)
	hier_node_t* newbody = Hier_Dup_ProcBody_link(shHier, funcinfo->nodeProc, &dup_old2new);
	EnqueuePrevLevel(newbody);

	// Add the line information
	avl_pp_foreach(&dup_old2new, scan) {
		C2Hier_SetNodeLines(scan->data, CallNode);
	}

	// Replace the occurrences of the return symbol
	if(funcinfo->vex_ret_name!=NULL) {
		avl_pp_foreach(&dup_old2new, scan) {
			hier_node_t* node = scan->data;
			if(node->TYPE!=HIERARCHY_STATE) continue;
			hier_state_t* state_data = &node->NODE.STATE;
			foreach(state_data->actions, scanAct) {
				hier_action_t* action = scanAct->DATA;
				hvex_t* vexDest = hvex_asg_get_dest(action->expr);
				if(vexDest==NULL) continue;
				if(hvex_vec_getname(vexDest)==funcinfo->vex_ret_name) hvex_vec_setname(vexDest, newretname);
			}  // Scan Actions
		}  // Scan nodes
	}

	// Replace the occurrences of the pointers
	if(avl_pp_isempty(&tree_ptrarg2vex)==false) {
		avl_pp_foreach(&dup_old2new, scan) {
			hier_node_t* node = scan->data;
			if(node->TYPE!=HIERARCHY_STATE) continue;
			hier_state_t* state_data = &node->NODE.STATE;
			foreach(state_data->actions, scanAct) {
				hier_action_t* action = scanAct->DATA;

				// A local utility function to replace the pointer occurrences
				void replace_read_occurrences(hvex_t* vex) {
					if(vex==NULL) return;
					if(vex->model==HVEX_VECTOR) {
						char* name = hvex_vec_getname(vex);
						// Get the declaration of this symbol
						c2hier_declinfo_t* declinfo = GetDeclInfo_FromName(name);
						// Check whether this declinfo is among the pointers currently handled
						avl_pp_node_t* node_pp = NULL;
						if(avl_pp_find(&tree_ptrarg2vex, declinfo, &node_pp)==false) return;
						// Here we have a pointer
						hvex_t* newVex = hvex_dup(node_pp->data);
						hvex_replace_resize_free(vex, newVex);
					}
					else {
						// Replace in operands
						hvex_foreach(vex->operands, VexOp) replace_read_occurrences(VexOp);
					}
				}

				if(hvex_model_is_asg(action->expr->model)==true) {
					// Replace in the read symbols
					replace_read_occurrences(hvex_asg_get_expr(action->expr));
					replace_read_occurrences(hvex_asg_get_addr(action->expr));
					replace_read_occurrences(hvex_asg_get_cond(action->expr));
					// Replace the written symbol
					hvex_t* vexDest = hvex_asg_get_dest(action->expr);
					assert(vexDest!=NULL && vexDest->model==HVEX_VECTOR);
					// Get the declaration of this symbol
					c2hier_declinfo_t* declinfo = GetDeclInfo_FromName(hvex_vec_getname(vexDest));
					// Check whether this declinfo is among the pointers currently handled
					avl_pp_node_t* node_pp = NULL;
					if(avl_pp_find(&tree_ptrarg2vex, declinfo, &node_pp)==true) {
						// FIXME For now, no array pointer is handled
						replace_read_occurrences(vexDest);
					}
				}
				else {
					if(action->expr->model!=HVEX_FUNCTION) abort();
					replace_read_occurrences(action->expr);
				}

			}  // Scan Actions
		}  // Scan nodes
	}  // Scan parameters that are pointers

	// Clean
	avl_pp_reset(&dup_old2new);
	avl_pp_reset(&tree_ptrarg2vex);

	// Build the return VEX with the new name
	hvex_t* vex_newret = NULL;
	if(funcinfo->vex_ret!=NULL) {
		vex_newret = hvex_dup(funcinfo->vex_ret);
		assert(vex_newret->model==HVEX_VECTOR);
		hvex_vec_setname(vex_newret, newretname);
	}

	return vex_newret;
}

static hvex_t *C2Hier_FuncCall(c_tree_node *TreeNode) {
	c_tree_node* CallFunc = CTreeExprOperand(TreeNode, 0);
	int          OperCode = CTreeNodeCode(CallFunc);

	// FIXME is this for function pointers?
	if(OperCode!=C_ADDR_EXPR) {
		c_tree_view_node(TreeNode);
		abort();
	}

	CallFunc = CTreeExprOperand(CallFunc, 0);
	OperCode = CTreeNodeCode(CallFunc);

	if(OperCode!=C_FUNCTION_DECL) {
		c_tree_view_node(TreeNode);
		abort();
	}

	// Find info about this function
	c2hier_funcinfo_t* funcinfo = GetFuncInfo_FromDecl(CallFunc);
	if(funcinfo==NULL) {
		char* name = CTreeIdentPointer(CTreeDeclName(CallFunc));
		errprintfa("At %s:%d: No function '%s' known\n", CTreeFileName(TreeNode), CTreeLineNum(TreeNode), name);
	}

	// Process arguments
	int NbArgs = 0;
	chain_list *ChainParam = NULL;
	ctree_foreach(CTreeExprOperand(TreeNode, 1), CallScan) {
		c_tree_node* CallOper = CTreeListValue(CallScan);
		hvex_t* VexOper = C2Hier_TreeExpr(CallOper);
		ChainParam = addchain(ChainParam, VexOper);
		NbArgs++;
	}
	ChainParam = reverse(ChainParam);

	// Detect when it's necessary to inline the function here
	bool need_inline = funcinfo->is_inline!=0 ? true : false;
	// Scan arguments: inline when there are pointer arguments
	foreach(funcinfo->list_args, scanArg) {
		c2hier_declinfo_t* declinfo = scanArg->DATA;
		if(declinfo->is_pointer != 0) { need_inline = true; break; }
	}
	// Handle when asked to inline all functions
	if(need_inline==false && shImplem->env.inline_all==true) {
		// Detect when the function was only declared: it should be a built-in function
		if(funcinfo->only_declared==0) need_inline = true;
	}

	if(need_inline==true) {
		// Inline function
		hvex_t* VexCall = C2Hier_InlineCall(TreeNode, funcinfo, ChainParam);
		foreach(ChainParam, scan) hvex_free(scan->DATA);
		freechain(ChainParam);
		return VexCall;  // Note: Can be NULL
	}

	funcinfo->is_used++;

	// Handle when the function returns something
	if(funcinfo->vex_ret!=NULL) {
		// Create the Function VEX
		hvex_t* VexCall = hvex_newfunc(funcinfo->name, funcinfo->vex_ret->width);
		foreach(ChainParam, scan) hvex_addop_tail(VexCall, scan->DATA);
		freechain(ChainParam);
		hvex_sign_copy(VexCall, funcinfo->vex_ret);

		// Note: To avoid Function VEX duplication during various VEX simplifications,
		// need to create a separate Action for the call and use a temporary variable for the return

		// Count use number
		unsigned InlinedNum = funcinfo->inlined_num++;

		// Create a temporary register to hold the return value
		char buf[strlen(funcinfo->name) + 20];
		sprintf(buf, "%s_ret%u", funcinfo->name, InlinedNum);
		char* newretname = Map_Netlist_MakeInstanceName_Prefix(shImplem, stralloc(buf));
		// Create the new Register
		netlist_comp_t* comp = Netlist_Comp_Reg_New(newretname, funcinfo->vex_ret->width);
		Netlist_Comp_SetChild(shImplem->netlist.top, comp);
		// Set a flag to indicate the component can be duplicated when (later) creating multiple top-levels
		Implem_SymFlag_Add(shImplem, newretname, SYM_FLAG_CANDUP);

		// Create the VEX for the temp reg
		hvex_t* vex_newret = hvex_dup(funcinfo->vex_ret);
		assert(vex_newret->model==HVEX_VECTOR);
		hvex_vec_setname(vex_newret, newretname);

			// Create an Action to do the call
		hier_action_t* action = Enqueue_NewStateAction(true);
		action->expr = hvex_asg_make(hvex_dup(vex_newret), NULL, VexCall, NULL);
		// Add the line information
		C2Hier_SetActLines(action, TreeNode);

		return vex_newret;
	}

	// Here the function returns nothing (it is a procedure)
	funcinfo->is_used++;

	// Add a Hier Action
	hier_action_t* action = Enqueue_NewStateAction(true);
	action->expr = hvex_newfunc(funcinfo->name, 1);
	foreach(ChainParam, scan) hvex_addop_tail(action->expr, scan->DATA);
	freechain(ChainParam);

	// Add the line information
	C2Hier_SetActLines(action, TreeNode);

	// Handle the post-inc etc actions that could be in the expression of arguments
	// FIXME if there was ++/-- on variables that are also used inside the function call
	//   Then their behaviour here is incorrect
	EnqueueNextInPrev();

	return NULL;
}



//===========================================================
// Building expressions -> vexexpr*
//===========================================================

// FIXME Clearly describe what this function does
static hvex_t *C2Hier_Pointer2Array(c_tree_node *TreeNode, hvex_t *VexExpr) {
	// If already an array, nothing to do
	if(VexExpr->model==HVEX_INDEX) return VexExpr;

	// Change to real size of array (size of pointer)
	if(VexExpr->model==HVEX_VECTOR) 	{
		c2hier_declinfo_t* declinfo = GetDeclInfo_FromName(hvex_vec_getname(VexExpr));
		assert(declinfo!=NULL);
		VexExpr->width = declinfo->vex_atom->width;
		VexExpr->left  = declinfo->vex_atom->left;
		VexExpr->right = declinfo->vex_atom->right;
		hvex_sign_copy(VexExpr, declinfo->vex_atom);
		return VexExpr;
	}

	// FIXME Handle additions and subtractions with pointer?
	if(VexExpr->model==HVEX_ADD || VexExpr->model==HVEX_SUB) {
		hvex_t* VexValue = NULL;

		// Transform "i - cst" into "i + (-cst)"
		if(VexExpr->model==HVEX_SUB) {
			VexValue = VexExpr->operands->next;
			hvex_unlinkop(VexValue);
			VexValue = hvex_newmodel_op1(HVEX_NEG, VexValue->width, VexValue);
			hvex_sign_copy(VexValue, VexExpr->operands->next);
			hvex_addop_tail(VexExpr, VexValue);
			VexExpr->model = HVEX_ADD;
		}

		VexValue = VexExpr->operands;
		if(VexValue->model==HVEX_VECTOR) {
			c2hier_declinfo_t* declinfo = GetDeclInfo_FromName(hvex_vec_getname(VexValue));
			assert(declinfo!=NULL);

			// Remove the first operand, which just had the name
			hvex_unlinkop(VexValue);
			hvex_free(VexValue);

			// Fix width and sign
			VexExpr->width = declinfo->vex_atom->width;
			VexExpr->left  = declinfo->vex_atom->left;
			VexExpr->right = declinfo->vex_atom->right;
			hvex_sign_copy(VexExpr, declinfo->vex_atom);

			// Resize if array
			if(declinfo->vex_atom->model==HVEX_ARRAY) {
				unsigned NumberBit = declinfo->vex_atom->operands->next->width;
				hvex_resize(VexExpr->operands, NumberBit);
			}

			// Convert into model HVEX_INDEX
			VexExpr->model = HVEX_INDEX;
			hvex_index_setname(VexExpr, hvex_vec_getname(VexExpr->operands));
			hvex_remop(VexExpr->operands);

			return VexExpr;
		}
	}

	errprintf("At %s:%d: Forbidden use of pointer: ", CTreeFileName(TreeNode), CTreeLineNum(TreeNode));
	hvex_print_bn(VexExpr);
	abort();
}

// FIXME Clearly describe what this function does
static hvex_t *C2Hier_ArrayRef(c_tree_node * TreeNode) {
	c_tree_node* TreeDecl  = CTreeExprOperand(TreeNode, 0);
	c_tree_node* TreeIndex = CTreeExprOperand(TreeNode, 1);

	hvex_t* VexAtom  = C2Hier_TreeExpr(TreeDecl);
	if(VexAtom->model==HVEX_LITERAL) {
		// Probably a ROM with static indexes
		// FIXME Make sure we understand what may happen in there
		return VexAtom;
	}
	hvex_t* VexIndex = C2Hier_TreeExpr(TreeIndex);
	// WARNING: Don't handle signed index
	hvex_set_unsigned(VexIndex);

	c_tree_node* DeclType = CTreeType(TreeDecl);
	hvex_t* VexDecl = C2Hier_TreeType2Vex(DeclType);

	// Make index size coherent with declaration
	hvex_resize(VexIndex, VexDecl->width);

	// If already an array then concat the two dimensions
	if(VexAtom->model==HVEX_INDEX) {
		hvex_t* VexIndexNew = VexAtom->operands;
		hvex_unlinkop(VexIndexNew);
		VexIndexNew = hvex_newmodel_op2(HVEX_CONCAT, VexIndexNew->width + VexIndex->width, VexIndexNew, VexIndex);
		hvex_addop_head(VexAtom, VexIndexNew);
		return VexAtom;
	}

	if(VexAtom->model!=HVEX_VECTOR) {
		errprintf("At %s:%d: Forbidden array name: ", CTreeFileName(TreeDecl), CTreeLineNum(TreeDecl));
		hvex_print_bn(VexAtom);
		abort();
	}

	c2hier_declinfo_t* declinfo = GetDeclInfo_FromDecl(TreeDecl);
	if(declinfo==NULL) {
		char* VexName = hvex_vec_getname(VexAtom);
		errprintfa("At %s:%d: Can't find declaration of variable '%s'.\n", CTreeFileName(TreeDecl), CTreeLineNum(TreeDecl), VexName);
	}

	VexDecl = declinfo->vex_atom;

	if(VexDecl==NULL || VexDecl->model!=HVEX_ARRAY) {
		char* VexName = hvex_vec_getname(VexAtom);
		errprintfa("At %s:%d: Variable '%s' should be declared as an array.\n", CTreeFileName(TreeDecl), CTreeLineNum(TreeDecl), VexName);
	}

	hvex_free(VexAtom);

	hvex_t* VexExpr = hvex_dup(VexDecl->operands);
	VexExpr->model = HVEX_INDEX;
	hvex_addop_head(VexExpr, VexIndex);
	hvex_sign_copy(VexExpr, VexDecl);

	return VexExpr;
}

// Force build the expression as combinational, forbid intermediate ASG
static hvex_t *C2Hier_BoolCond2Vex(c_tree_node *TreeNode) {

	shC2Hier_ModeNoAsg ++;  // Force mode "no assignment"
	hvex_t* VexExpr = C2Hier_TreeExpr(TreeNode);
	shC2Hier_ModeNoAsg --;  // Use previous mode

	if(VexExpr->width > 1) {
		VexExpr = hvex_newmodel_op2(HVEX_NE, 1, VexExpr, hvex_newlit_repeat('0', VexExpr->width));
	}

	return VexExpr;
}
// Build an intermediate Action and return a reference to a Signal
static hvex_t *C2Hier_BoolCondSigAsg(c_tree_node *TreeNode) {
	// Get the expression
	hvex_t* VexExpr = C2Hier_TreeExpr(TreeNode);
	if(VexExpr->width > 1) {
		VexExpr = hvex_newmodel_op2(HVEX_NE, 1, VexExpr, hvex_newlit_repeat('0', VexExpr->width));
	}

	if(shNextNodeFirst!=NULL) {
		errprintfa("At %s:%u: Usage of postInc/postDec operations in tests is not handled.\n", CTreeFileName(TreeNode), CTreeLineNum(TreeNode));
	}

	// Create a new test Signal
	char* name = Map_Netlist_MakeInstanceName_Prefix(shImplem, namealloc("augh_test"));
	netlist_comp_t* comp = Netlist_Comp_Sig_New(name, 1);
	Netlist_Comp_SetChild(shImplem->netlist.top, comp);
	// Set a flag to indicate the Sig can be duplicated when (later) creating several top-levels
	Implem_SymFlag_Add(shImplem, name, SYM_FLAG_CANDUP);

	// Create a new Action
	hier_action_t* action = Enqueue_NewStateAction(true);
	action->expr = hvex_asg_make(hvex_newvec_bit(name), NULL, VexExpr, NULL);

	// Add the line information
	C2Hier_SetActLines(action, TreeNode);

	return hvex_newvec_bit(name);
}

static hvex_t *C2Hier_BitBool2(c_tree_node *TreeNode) {
	c_tree_node* TreeOper1 = CTreeExprOperand(TreeNode, 0);
	c_tree_node* TreeOper2 = CTreeExprOperand(TreeNode, 1);

	hvex_t* VexOper1 = C2Hier_TreeExpr(TreeOper1);
	hvex_t* VexOper2 = C2Hier_TreeExpr(TreeOper2);

	hvex_model_t* oper = HVEX_AND;
	int Code = CTreeNodeCode(TreeNode);
	switch(Code) {
		case C_BIT_IOR_EXPR : oper = HVEX_OR; break;
		case C_BIT_XOR_EXPR : oper = HVEX_XOR; break;
		case C_BIT_AND_EXPR : oper = HVEX_AND; break;
		default : abort();
	}

	// GCC does this: result is signed if both operands are signed
	bool is_signed = false;
	if(hvex_is_signed(VexOper1)==true && hvex_is_signed(VexOper2)==true) is_signed = true;

	// Modify operand width
	unsigned maxWidth = GetMax(VexOper1->width, VexOper2->width);
	hvex_resize(VexOper1, maxWidth);
	hvex_resize(VexOper2, maxWidth);

	// Build final expression
	hvex_t* VexExpr = hvex_newmodel_op2(oper, maxWidth, VexOper1, VexOper2);
	if(is_signed==true) hvex_set_signed(VexExpr);

	return VexExpr;
}

static hvex_t *C2Hier_MultDivMod(c_tree_node *TreeNode) {
	c_tree_node* TreeOper1 = CTreeExprOperand(TreeNode, 0);
	c_tree_node* TreeOper2 = CTreeExprOperand(TreeNode, 1);

	hvex_t* VexOper1 = C2Hier_TreeExpr(TreeOper1);
	hvex_t* VexOper2 = C2Hier_TreeExpr(TreeOper2);

	// Check coherency about sign bit
	if(hvex_is_signed(VexOper1) != hvex_is_signed(VexOper2)) {
		// VexOper1 unsigned and larger
		if(hvex_is_unsigned(VexOper1)==true && VexOper1->width >= VexOper2->width) {
			if(VexOper1->width < C2Hier_GetMaxWidth(TreeOper1))
				hvex_resize(VexOper1, VexOper1->width + 1);
		}
		// VexOper2 unsigned and larger
		if(hvex_is_unsigned(VexOper2)==true && VexOper2->width >= VexOper1->width) {
			if(VexOper2->width < C2Hier_GetMaxWidth(TreeOper2))
				hvex_resize(VexOper2, VexOper2->width + 1);
		}
	}

	unsigned Width1 = VexOper1->width;
	unsigned Width2 = VexOper2->width;

	unsigned Width = 0;
	hvex_model_t* Oper = NULL;

	int Code = CTreeNodeCode(TreeNode);
	switch(Code) {

		case C_TRUNC_DIV_EXPR:
			Oper = HVEX_DIVQ;
			Width = Width1;
			break;

		case C_TRUNC_MOD_EXPR:
			Oper = HVEX_DIVR;
			Width = Width2;
			break;

		case C_MULT_EXPR:
			Oper = HVEX_MUL;
			Width = Width1 + Width2;
			break;

		default : abort();
	}

	// Build expression
	hvex_t* VexExpr = hvex_newmodel_op2(Oper, Width, VexOper1, VexOper2);

	// The result is signed if one operand is signed
	if(hvex_is_signed(VexOper1) || hvex_is_signed(VexOper2)) hvex_set_signed(VexExpr);

	return VexExpr;
}

static hvex_t *C2Hier_Compare(c_tree_node *TreeNode) {
	c_tree_node* TreeOper1 = CTreeExprOperand(TreeNode, 0);
	c_tree_node* TreeOper2 = CTreeExprOperand(TreeNode, 1);
	hvex_t* VexOper1 = C2Hier_TreeExpr(TreeOper1);
	hvex_t* VexOper2 = C2Hier_TreeExpr(TreeOper2);

	// Ensure signs are coherent on the operands
	if(hvex_is_signed(VexOper1) != hvex_is_signed(VexOper2)) {
		// VexOper1 unsigned and larger
		if(hvex_is_unsigned(VexOper1) && VexOper1->width >= VexOper2->width) {
			if(VexOper1->width < C2Hier_GetMaxWidth(TreeOper1)) hvex_resize(VexOper1, VexOper1->width + 1);
		}
		// VexOper2 unsigned and larger
		if(hvex_is_unsigned(VexOper2) && VexOper2->width >= VexOper1->width) {
			if(VexOper2->width < C2Hier_GetMaxWidth(TreeOper2)) hvex_resize(VexOper2, VexOper2->width + 1);
		}
	}

	int Code = CTreeNodeCode(TreeNode);
	hvex_t* VexExpr = NULL;
	hvex_model_t* Oper = NULL;

	switch(Code) {

		case C_EQ_EXPR: Oper = HVEX_EQ; break;
		case C_NE_EXPR: Oper = HVEX_NE; break;

		case C_LT_EXPR:
		case C_LE_EXPR:
		case C_GT_EXPR:
		case C_GE_EXPR: {
			int Sign = hvex_is_signed(VexOper1) || hvex_is_signed(VexOper2);

			switch(Code) {
				case C_LT_EXPR : Oper = Sign ? HVEX_LTS : HVEX_LT; break;
				case C_LE_EXPR : Oper = Sign ? HVEX_LES : HVEX_LE; break;
				case C_GT_EXPR : Oper = Sign ? HVEX_GTS : HVEX_GT; break;
				case C_GE_EXPR : Oper = Sign ? HVEX_GES : HVEX_GE; break;
			}

			break;
		}

		default : abort();
	}

	unsigned maxWidth = GetMax(VexOper1->width, VexOper2->width);
	hvex_resize(VexOper1, maxWidth);
	hvex_resize(VexOper2, maxWidth);

	VexExpr = hvex_newmodel_op2(Oper, 1, VexOper1, VexOper2);

	return VexExpr;
}

// The main conversion function: scan expr codes and launch appropriate conversion
static hvex_t *C2Hier_TreeExpr(c_tree_node *TreeNode) {
	hvex_t* VexExpr = NULL;

	int FirstCode = CTreeNodeFirstCode(TreeNode);
	switch(FirstCode) {

		case C_INT_CST_NODE: {
			int Array[2];
			Array[0] = CTreeIntCstHigh(TreeNode);
			Array[1] = CTreeIntCstLow(TreeNode);
			bool is_signed = CIsTreeNodeUnsigned(TreeNode) == 0 ? true : false;
			VexExpr = hvex_newlit_arrint32_be(Array, 2, 2*32, is_signed);
			if(is_signed==true && hvex_lit_getval(VexExpr)[0]=='0') hvex_set_unsigned(VexExpr);
			break;
		}

		case C_REAL_CST_NODE: {
			errprintfa("At %s:%d: Invalid float value.\n", CTreeFileName(TreeNode), CTreeLineNum(TreeNode));
			break;
		}

		case C_TYPE_NODE:  // FIXME What's that? A typedef?
			VexExpr = hvex_dup(C2Hier_TreeType2Vex(TreeNode));
			break;

		case C_DECL_NODE:
			VexExpr = C2Hier_TreeDecl2Vex(TreeNode);
			break;

		case C_LIST_NODE: {  // For init values of array
			int NumberBit = 0;
			VexExpr = hvex_newmodel(HVEX_CONCAT, NumberBit);

			// Note: First, store the operands in a list for efficiency
			chain_list* list_ops = NULL;
			ctree_foreach(TreeNode, TreeScan) {
				hvex_t* VexValue = C2Hier_TreeExpr(CTreeListValue(TreeScan));
				assert(VexValue!=NULL);
				NumberBit += VexValue->width;
				list_ops = addchain(list_ops, VexValue);
			}

			// Now add operands to the vex node, all at the head for efficiency
			foreach(list_ops, scan) hvex_addop_head(VexExpr, scan->DATA);
			freechain(list_ops);

			VexExpr->left  = NumberBit-1;
			VexExpr->right = 0;
			VexExpr->width = NumberBit;
			break;
		}

		// Most expression codes are handled in there
		case C_EXPR_NODE: {
			int Code = CTreeNodeCode(TreeNode);

			switch(Code) {

				// Assignment
				case C_MODIFY_EXPR: {
					c_tree_node* TreeTarget = CTreeExprOperand(TreeNode, 0);
					c_tree_node* TreeData   = CTreeExprOperand(TreeNode, 1);
					hvex_t* VexTarget = C2Hier_TreeExpr(TreeTarget);
					hvex_t* VexData = C2Hier_TreeExpr(TreeData);
					VexExpr = C2Hier_Assign_vex(VexTarget, VexData, true, stralloc(CTreeFileName(TreeNode)), CTreeLineNum(TreeNode));
					break;
				}

				case C_RSHIFT_EXPR :
				case C_LSHIFT_EXPR :
				case C_RROTATE_EXPR :
				case C_LROTATE_EXPR : {
					c_tree_node* TreeOper = CTreeExprOperand(TreeNode, 0);
					c_tree_node* TreeShift = CTreeExprOperand(TreeNode, 1);

					hvex_t* VexOper = C2Hier_TreeExpr(TreeOper);
					hvex_t* VexShift = C2Hier_TreeExpr(TreeShift);

					// Kernighan & Richie: implicit conversion
					// FIXME If we do want an operation on a 29-bits input this returns width = 32
					int MaxWidth = C2Hier_GetMaxWidth(TreeOper);
					hvex_resize(VexOper, MaxWidth);

					hvex_model_t* Oper = NULL;

					switch(Code) {
						case C_RSHIFT_EXPR : Oper = HVEX_SHR; break;
						case C_LSHIFT_EXPR : Oper = HVEX_SHL; break;
						case C_RROTATE_EXPR : Oper = HVEX_ROTR; break;
						case C_LROTATE_EXPR : Oper = HVEX_ROTL; break;
						default: abort();
					}

					// Kernighan & Richie: Assume shift inside the size of the operand
					int MaxWidthS = uint_bitsnb(VexOper->width);
					if(MaxWidthS < VexShift->width) hvex_resize(VexShift, MaxWidthS);

					// Force unsigned shift value
					hvex_set_unsigned(VexShift);

					// Build the result expression
					VexExpr = hvex_newmodel_op2(Oper, VexOper->width, VexOper, VexShift);
					if(hvex_is_signed(VexOper)==true) hvex_set_signed(VexExpr);

					break;
				}

				case C_PLUS_EXPR:
				case C_MINUS_EXPR: {
					hvex_model_t* oper = HVEX_ADD;
					if(Code==C_MINUS_EXPR) oper = HVEX_SUB;

					c_tree_node* TreeOper1 = CTreeExprOperand(TreeNode, 0);
					c_tree_node* TreeOper2 = CTreeExprOperand(TreeNode, 1);

					hvex_t* VexOper1 = C2Hier_TreeExpr(TreeOper1);
					hvex_t* VexOper2 = C2Hier_TreeExpr(TreeOper2);

					VexExpr = C2Hier_VexBinOper(oper, VexOper1, VexOper2, 1);
					break;
				}

				case C_NEGATE_EXPR: {
					c_tree_node* TreeOper = CTreeExprOperand(TreeNode, 0);
					hvex_t* VexOper = C2Hier_TreeExpr(TreeOper);
					hvex_resize(VexOper, VexOper->width+1);
					VexExpr = hvex_newmodel_op1(HVEX_NEG, VexOper->width, VexOper);
					hvex_set_signed(VexExpr);
					break;
				}

				case C_MULT_EXPR:
				case C_TRUNC_DIV_EXPR:
				case C_TRUNC_MOD_EXPR:
					VexExpr = C2Hier_MultDivMod(TreeNode);
					break;

				// Bitwise operations
				case C_BIT_IOR_EXPR:
				case C_BIT_XOR_EXPR:
				case C_BIT_AND_EXPR: {
					VexExpr = C2Hier_BitBool2(TreeNode);
					break;
				}

				// Bitwise NOT
				case C_BIT_NOT_EXPR: {
					c_tree_node* TreeOper = CTreeExprOperand(TreeNode, 0);
					hvex_t* VexOper = C2Hier_TreeExpr(TreeOper);
					VexExpr = hvex_newmodel_op1(HVEX_NOT, VexOper->width, VexOper);
					if(hvex_is_signed(VexOper)==true) hvex_set_signed(VexExpr);
					break;
				}

				// Boolean operators
				// FIXME What does it mean, ANDIF, ORIF??? And no XORIF?
				case C_TRUTH_ANDIF_EXPR:
				case C_TRUTH_ORIF_EXPR:
				case C_TRUTH_AND_EXPR:
				case C_TRUTH_OR_EXPR:
				case C_TRUTH_XOR_EXPR: {
					c_tree_node* TreeOper1 = CTreeExprOperand(TreeNode, 0);
					c_tree_node* TreeOper2 = CTreeExprOperand(TreeNode, 1);
					hvex_t* VexOper1 = C2Hier_TreeExpr(TreeOper1);
					hvex_t* VexOper2 = C2Hier_TreeExpr(TreeOper2);

					// Make operands 1-bit
					if(VexOper1->width > 1) VexOper1 = hvex_newmodel_op2(HVEX_NE, 1, VexOper1, hvex_newlit_repeat('0', VexOper1->width));
					if(VexOper2->width > 1) VexOper2 = hvex_newmodel_op2(HVEX_NE, 1, VexOper2, hvex_newlit_repeat('0', VexOper2->width));

					hvex_model_t* oper = HVEX_AND;
					switch(Code) {
						case C_TRUTH_ANDIF_EXPR :
						case C_TRUTH_AND_EXPR : oper = HVEX_AND; break;
						case C_TRUTH_ORIF_EXPR :
						case C_TRUTH_OR_EXPR :  oper = HVEX_OR; break;
						case C_TRUTH_XOR_EXPR : oper = HVEX_XOR; break;
						default : abort();
					}

					VexExpr = hvex_newmodel_op2(oper, 1, VexOper1, VexOper2);
					break;
				}

				// Boolean Not
				case C_TRUTH_NOT_EXPR: {
					c_tree_node* TreeOper = CTreeExprOperand(TreeNode, 0);
					hvex_t* VexOper = C2Hier_TreeExpr(TreeOper);
					VexExpr = hvex_newmodel_op2(HVEX_EQ, 1, VexOper, hvex_newlit_repeat('0', VexOper->width));
					break;
				}

				// Comparisons
				case C_LT_EXPR:
				case C_LE_EXPR:
				case C_GT_EXPR:
				case C_GE_EXPR:
				case C_EQ_EXPR:
				case C_NE_EXPR:
					VexExpr = C2Hier_Compare(TreeNode);
					break;

				// Ternary operator ?:
				case C_COND_EXPR: {
					c_tree_node* TreeCond  = CTreeExprOperand(TreeNode, 0);
					c_tree_node* TreeOper1 = CTreeExprOperand(TreeNode, 1);
					c_tree_node* TreeOper2 = CTreeExprOperand(TreeNode, 2);

					shC2Hier_ModeNoAsg ++;  // Force mode "no assignment"

					hvex_t* VexCond = C2Hier_BoolCond2Vex(TreeCond);
					assert(VexCond->width==1);

					hvex_t* VexOper1 = C2Hier_TreeExpr(TreeOper1);
					hvex_t* VexOper2 = C2Hier_TreeExpr(TreeOper2);

					shC2Hier_ModeNoAsg --;  // Use previous mode

					int is_signed = false;
					if(hvex_is_signed(VexOper1)==true || hvex_is_signed(VexOper2)==true) is_signed = true;

					// Adjust width of operands
					int MaxWidth = GetMax(VexOper1->width, VexOper2->width);
					if(hvex_is_signed(VexOper1) != hvex_is_signed(VexOper2)) {
						// Handle operations between signed and unsigned
						if(hvex_is_unsigned(VexOper1)) MaxWidth = GetMax(MaxWidth, VexOper1->width+1);
						if(hvex_is_unsigned(VexOper2)) MaxWidth = GetMax(MaxWidth, VexOper2->width+1);
					}
					hvex_resize(VexOper1, MaxWidth);
					hvex_resize(VexOper2, MaxWidth);

					// Build final Expr
					VexExpr = hvex_newmodel_opn(HVEX_MUXB, MaxWidth, VexCond, VexOper2, VexOper1, NULL);
					if(is_signed==true) hvex_set_signed(VexExpr);

					break;
				}

				case C_PREDECREMENT_EXPR:
				case C_PREINCREMENT_EXPR:
				case C_POSTDECREMENT_EXPR:
				case C_POSTINCREMENT_EXPR: {
					bool inprev = true;
					hvex_model_t* oper = HVEX_ADD;

					switch(Code) {
						case C_PREDECREMENT_EXPR:  inprev = true; oper = HVEX_SUB; break;
						case C_PREINCREMENT_EXPR:  inprev = true; oper = HVEX_ADD; break;
						case C_POSTDECREMENT_EXPR: inprev = false; oper = HVEX_SUB; break;
						case C_POSTINCREMENT_EXPR: inprev = false; oper = HVEX_ADD; break;
					}

					c_tree_node* TreeTarget = CTreeExprOperand(TreeNode, 0);
					hvex_t* VexTarget = C2Hier_TreeExpr(TreeTarget);
					hvex_t* VexData = C2Hier_VexBinOper(
						oper, hvex_dup(VexTarget), hvex_newlit_int(1, VexTarget->width, false), 0
					);

					VexExpr = C2Hier_Assign_vex(VexTarget, VexData, inprev, stralloc(CTreeFileName(TreeNode)), CTreeLineNum(TreeNode));
					break;
				}

				case C_COMPOUND_EXPR:
					for(int Index=0; Index<CTreeExprNumOper(TreeNode); Index++) {
						c_tree_node* TreeScan = CTreeExprOperand(TreeNode, Index);
						if(VexExpr!=NULL) hvex_free(VexExpr);
						VexExpr = C2Hier_TreeExpr(TreeScan);
					}
					break;

				case C_CAST_EXPR: {
					c_tree_node* TreeScan = CTreeExprOperand(TreeNode, 0);
					VexExpr = C2Hier_TreeExpr(TreeScan);

					// Resize
					hvex_t* VexValue = C2Hier_TreeType2Vex(CTreeType(TreeNode));
					hvex_resize(VexExpr, VexValue->width);
					// Handle sign
					hvex_sign_copy(VexExpr, VexValue);

					break;
				}

				case C_NON_LVALUE_EXPR:
				case C_NOP_EXPR: {
					c_tree_node* TreeScan = CTreeExprOperand(TreeNode, 0);
					VexExpr = C2Hier_TreeExpr(TreeScan);
					break;
				}

				case C_CALL_EXPR:  // Function call
					VexExpr = C2Hier_FuncCall(TreeNode);
					break;

				case C_ARRAY_REF:  // array[i]
					VexExpr = C2Hier_ArrayRef(TreeNode);
					break;

				case C_COMPONENT_REF: {  // Field of struct
					c_tree_node* TreeDecl = CTreeExprOperand(TreeNode, 0);
					c_tree_node* TreeField = CTreeExprOperand(TreeNode, 1);
					assert(CTreeNodeCode(TreeField)==C_FIELD_DECL);

					hvex_t* VexAtom = C2Hier_TreeExpr(TreeDecl);
					c_tree_node* DeclType = CTreeType(TreeDecl);
					hvex_t* VexDecl = C2Hier_TreeType2Vex(DeclType);

					// Set the full width of the structure (cropping is done right after)
					VexAtom->width = VexDecl->width;
					VexAtom->left  = VexDecl->left;
					VexAtom->right = VexDecl->right;

					// Select the indexes that correspond to the field
					hvex_t* VexField = NULL;
					avl_pp_find_data(&shC2Hier_Field2Vex, TreeField, (void**)&VexField);
					assert(VexField!=NULL);
					VexExpr = VexAtom;
					hvex_croprel(VexExpr, VexExpr->left - VexField->left, VexField->right - VexExpr->right);

					// Set the sign of this field
					hvex_sign_copy(VexExpr, VexField);

					break;
				}

				case C_INDIRECT_REF: {  // deferencing: *pointer or pointer[i]
					c_tree_node* TreeScan = CTreeExprOperand(TreeNode, 0);  // deferencing
					// pointer + ( i * 4 ) or pointer or pointer + cst
					VexExpr = C2Hier_TreeExpr(TreeScan);
					break;
				}

				case C_CONVERT_EXPR: {  // dereferencing: *pointer or pointer[i]
					c_tree_node* TreeScan = CTreeExprOperand(TreeNode, 0);  /*ignore indirection */
					// if it is a pointer
					if(CTreeExprNumOper(TreeScan)==1) {
						// ignore i * 4
						TreeScan = CTreeExprOperand(TreeScan, 0);
					}
					else {
						errprintfa("At %s:%d: Illegal conversion.\n",
							CTreeFileName(TreeNode), CTreeLineNum(TreeNode)
						);
					}
					VexExpr = C2Hier_TreeExpr(TreeScan);   /* i */
					break;
				}

				case C_FLOAT_EXPR: {  // FIXME What's that? A cast?
					c_tree_node* TreeScan = CTreeExprOperand(TreeNode, 0);  // Ignore cast
					VexExpr = C2Hier_TreeExpr(TreeScan);   // Expression
					break;
				}

				case C_ADDR_EXPR: {  // Referencing: &var
					c_tree_node* TreeScan = CTreeExprOperand(TreeNode, 0);  // ignore referencing node
					VexExpr = C2Hier_TreeExpr(TreeScan);   // variable
					break;
				}

				case C_MAX_EXPR :    // a > b ? a : b
				case C_MIN_EXPR : {  // a < b ? a : b
					c_tree_node* TreeOper1 = CTreeExprOperand(TreeNode, 0);
					c_tree_node* TreeOper2 = CTreeExprOperand(TreeNode, 1);

					shC2Hier_ModeNoAsg ++;  // Force mode "no assignment"

					hvex_t* VexOper1 = C2Hier_TreeExpr(TreeOper1);
					hvex_t* VexOper2 = C2Hier_TreeExpr(TreeOper2);

					shC2Hier_ModeNoAsg --;  // Use previous mode

					int Signed = 0;
					if(hvex_is_signed(VexOper1)==true || hvex_is_signed(VexOper2)==true) Signed = 1;

					// Adjust width of operands
					int MaxWidth = GetMax(VexOper1->width, VexOper2->width);
					if(hvex_is_signed(VexOper1) != hvex_is_signed(VexOper2)) {
						// Handle operations between signed and unsigned
						if(hvex_is_unsigned(VexOper1)) MaxWidth = GetMax(MaxWidth, VexOper1->width+1);
						if(hvex_is_unsigned(VexOper2)) MaxWidth = GetMax(MaxWidth, VexOper2->width+1);
					}
					hvex_resize(VexOper1, MaxWidth);
					hvex_resize(VexOper2, MaxWidth);

					// Create the test VEX
					hvex_t* VexTest = NULL;
					switch(Code) {
						case C_MAX_EXPR : {
							hvex_model_t* oper = Signed!=0 ? HVEX_GES : HVEX_GE;
							VexTest = hvex_newmodel_op2(oper, 1, hvex_dup(VexOper1), hvex_dup(VexOper2));
							break;
						}
						case C_MIN_EXPR : {
							hvex_model_t* oper = Signed!=0 ? HVEX_LES : HVEX_LE;
							VexTest = hvex_newmodel_op2(oper, 1, hvex_dup(VexOper1), hvex_dup(VexOper2));
							break;
						}
						default : abort();
					}

					// Create the full VEX
					VexExpr = hvex_newmodel_opn(HVEX_MUXB, MaxWidth, VexTest, VexOper2, VexOper1, NULL);
					if(Signed!=0) hvex_set_signed(VexExpr);

					break;
				}

				default : {
					errprintfa("At %s:%d: Unhandled CTree node Code: %u(dec) %x(hex)\n",
						CTreeFileName(TreeNode), CTreeLineNum(TreeNode), Code, Code
					);
				}

			} // Switch on Code

			if(VexExpr==NULL) return NULL;

			// Check if pointer operation
			// FIXME Handle strings more elegantly
			if(CTreeTypeCode(CTreeType(TreeNode))==C_POINTER_TYPE) {
				if(VexExpr->model!=HVEX_STRING) VexExpr = C2Hier_Pointer2Array(TreeNode, VexExpr);
			}

			break;
		}

		case C_STRING_NODE : {
			char* str = CTreeStringPointer(TreeNode);  // Warning: the string contains the quotes
			unsigned len = strlen(str) - 2;
			char buf[len+2];
			strcpy(buf, str+1);
			buf[len] = 0;
			VexExpr = hvex_newmodel(HVEX_STRING, len+1);  // The trailing nul character is stored
			VexExpr->data = stralloc(buf);
			break;
		}

		default: {
			errprintfa("At %s:%d: Unhandled CTree node FirstCode: %u(dec) %x(hex)\n",
				CTreeFileName(TreeNode), CTreeLineNum(TreeNode), FirstCode, FirstCode
			);
		}

	} // Switch on FirstCode

	// Remove bits without information, except for lists where each bit is information
	if(VexExpr!=NULL && FirstCode!=C_LIST_NODE && VexExpr->model!=HVEX_STRING) {

		// For debug: Use this code to display intermediate vex and check results
		#if 0
		dbgprintf("VexExpr %p: ", VexExpr); hvex_print_bn(VexExpr);
		hvex_checkabort(VexExpr);
		#endif

		int MaxWidth = C2Hier_GetMaxWidth(TreeNode);
		if(VexExpr->width > MaxWidth) hvex_resize(VexExpr, MaxWidth);

		hvex_simp(VexExpr);

		// Remove most significant bits that are unuseful sign extension
		hvex_shrink(VexExpr);

		#ifndef NDEBUG
		hvex_checkabort(VexExpr);
		#endif
	}

	return VexExpr;
}



//===========================================================
// Conversion of control flow nodes
//===========================================================

static void C2Hier_For(c_tree_node *TreeFor) {
	c_tree_node* ForStart = CTreeInstForStart(TreeFor);
	c_tree_node* ForCond  = CTreeInstForCond(TreeFor);
	c_tree_node* ForInc   = CTreeInstForInc(TreeFor);
	c_tree_node* ForBody  = CTreeInstForBody(TreeFor);

	// Get the label in the previous node if applicable
	char* label_name = C2Hier_GetPrevLabelName();

	// Convert the START

	C2Hier_NodeChain(ForStart);

	// Create the LOOP node

	hier_node_t* nodeLoop = Hier_Node_NewType(HIERARCHY_LOOP);
	hier_loop_t* loop_data = &nodeLoop->NODE.LOOP;
	loop_data->label_name = label_name;

	Hier_Lists_AddNode(shHier, nodeLoop);
	AddNodes_C2H(TreeFor, nodeLoop);

	assert(shNextNodeFirst==NULL);
	EnqueuePrevNode(nodeLoop);

	// Create labels for CONTINUE and BREAK

	hier_node_t* labelCont = Hier_Node_NewType(HIERARCHY_LABEL);
	Hier_Lists_AddNode(shHier, labelCont);
	hier_node_t* labelBreak = Hier_Node_NewType(HIERARCHY_LABEL);
	Hier_Lists_AddNode(shHier, labelBreak);

	EnqueuePrevNode(labelBreak);

	// Add the line information
	C2Hier_SetNodeLines(nodeLoop, TreeFor);
	C2Hier_SetNodeLines(labelCont, TreeFor);
	C2Hier_SetNodeLines(labelBreak, TreeFor);

	// Save the shared variables

	hier_node_t* oldPrevFirst = shPrevNodeFirst;
	hier_node_t* oldPrevLast = shPrevNodeLast;
	hier_node_t* oldLoopCont = shLabelCont;
	hier_node_t* oldLoopBreak = shLabelBreak;

	shPrevNodeFirst = NULL;
	shPrevNodeLast = NULL;
	shLabelCont = labelCont;
	shLabelBreak = labelBreak;

	// Process the body AFTER: the test

	hvex_t* VexCond = NULL;

	if(ForCond!=NULL) {
		shPrevNodeFirst = NULL;
		shPrevNodeLast = NULL;

		VexCond = C2Hier_BoolCondSigAsg(ForCond);

		assert(shNextNodeFirst==NULL);
		loop_data->body_after = shPrevNodeFirst;
		if(loop_data->body_after!=NULL) {
			loop_data->body_after->PARENT = nodeLoop;
			nodeLoop->CHILDREN = addchain(nodeLoop->CHILDREN, loop_data->body_after);
		}

		shPrevNodeFirst = NULL;
		shPrevNodeLast = NULL;
	}
	else 	{
		VexCond = hvex_newlit_bit('1');
	}

	loop_data->testinfo->cond = VexCond;

	// Process the body BEFORE: the loop body + INC

	if(ForBody!=NULL || ForInc!=NULL) {
		shPrevNodeFirst = NULL;
		shPrevNodeLast = NULL;

		if(ForBody!=NULL) C2Hier_NodeChain(ForBody);

		assert(shNextNodeFirst==NULL);
		EnqueuePrevNode(labelCont);

		if(ForInc!=NULL) C2Hier_NodeChain(ForInc);

		assert(shNextNodeFirst==NULL);
		loop_data->body_before = shPrevNodeFirst;
		if(loop_data->body_before!=NULL) {
			loop_data->body_before->PARENT = nodeLoop;
			nodeLoop->CHILDREN = addchain(nodeLoop->CHILDREN, loop_data->body_before);
		}

		shPrevNodeFirst = NULL;
		shPrevNodeLast = NULL;
	}

	// Set the old values again

	shPrevNodeFirst = oldPrevFirst;
	shPrevNodeLast = oldPrevLast;
	shLabelCont = oldLoopCont;
	shLabelBreak = oldLoopBreak;
}

static void C2Hier_While(c_tree_node *TreeWhile) {
	c_tree_node* WhileCond = CTreeInstWhileCond(TreeWhile);
	c_tree_node* WhileBody = CTreeInstWhileBody(TreeWhile);

	// Get the label in the previous node if applicable
	char* label_name = C2Hier_GetPrevLabelName();

	// Create the LOOP node

	hier_node_t* nodeLoop = Hier_Node_NewType(HIERARCHY_LOOP);
	hier_loop_t* loop_data = &nodeLoop->NODE.LOOP;
	loop_data->label_name = label_name;

	Hier_Lists_AddNode(shHier, nodeLoop);
	AddNodes_C2H(TreeWhile, nodeLoop);

	assert(shNextNodeFirst==NULL);
	EnqueuePrevNode(nodeLoop);

	// Create labels for CONTINUE and BREAK

	hier_node_t* labelCont = Hier_Node_NewType(HIERARCHY_LABEL);
	Hier_Lists_AddNode(shHier, labelCont);
	hier_node_t* labelBreak = Hier_Node_NewType(HIERARCHY_LABEL);
	Hier_Lists_AddNode(shHier, labelBreak);

	EnqueuePrevNode(labelBreak);

	// Add the line information
	C2Hier_SetNodeLines(nodeLoop, TreeWhile);
	C2Hier_SetNodeLines(labelCont, TreeWhile);
	C2Hier_SetNodeLines(labelBreak, TreeWhile);

	// Save the shared variables

	hier_node_t* oldPrevFirst = shPrevNodeFirst;
	hier_node_t* oldPrevLast = shPrevNodeLast;
	hier_node_t* oldLoopCont = shLabelCont;
	hier_node_t* oldLoopBreak = shLabelBreak;

	shPrevNodeFirst = NULL;
	shPrevNodeLast = NULL;
	shLabelCont = labelCont;
	shLabelBreak = labelBreak;

	// Process the body AFTER: the test

	hvex_t* VexCond = NULL;

	if(WhileCond!=NULL) {
		shPrevNodeFirst = NULL;
		shPrevNodeLast = NULL;

		EnqueuePrevNode(labelCont);

		VexCond = C2Hier_BoolCondSigAsg(WhileCond);

		assert(shNextNodeFirst==NULL);
		loop_data->body_after = shPrevNodeFirst;
		if(loop_data->body_after!=NULL) {
			loop_data->body_after->PARENT = nodeLoop;
			nodeLoop->CHILDREN = addchain(nodeLoop->CHILDREN, loop_data->body_after);
		}

		shPrevNodeFirst = NULL;
		shPrevNodeLast = NULL;
	}
	else 	{
		VexCond = hvex_newlit_bit('1');
	}

	loop_data->testinfo->cond = VexCond;

	// Process the body BEFORE: the loop body

	if(WhileBody!=NULL) {
		shPrevNodeFirst = NULL;
		shPrevNodeLast = NULL;

		C2Hier_NodeChain(WhileBody);

		assert(shNextNodeFirst==NULL);
		loop_data->body_before = shPrevNodeFirst;
		if(loop_data->body_before!=NULL) {
			loop_data->body_before->PARENT = nodeLoop;
			nodeLoop->CHILDREN = addchain(nodeLoop->CHILDREN, loop_data->body_before);
		}

		shPrevNodeFirst = NULL;
		shPrevNodeLast = NULL;
	}

	// Set the old values again

	shPrevNodeFirst = oldPrevFirst;
	shPrevNodeLast = oldPrevLast;
	shLabelCont = oldLoopCont;
	shLabelBreak = oldLoopBreak;
}

static void C2Hier_Do(c_tree_node *TreeDo) {
	c_tree_node* DoCond = CTreeInstDoCond(TreeDo);
	c_tree_node* DoBody = CTreeInstDoBody(TreeDo);

	// Get the label in the previous node if applicable
	char* label_name = C2Hier_GetPrevLabelName();

	// Create the LOOP node

	hier_node_t* nodeLoop = Hier_Node_NewType(HIERARCHY_LOOP);
	hier_loop_t* loop_data = &nodeLoop->NODE.LOOP;
	loop_data->label_name = label_name;

	Hier_Lists_AddNode(shHier, nodeLoop);
	AddNodes_C2H(TreeDo, nodeLoop);

	assert(shNextNodeFirst==NULL);
	EnqueuePrevNode(nodeLoop);

	// Create labels for CONTINUE and BREAK

	hier_node_t* labelCont = Hier_Node_NewType(HIERARCHY_LABEL);
	Hier_Lists_AddNode(shHier, labelCont);
	hier_node_t* labelBreak = Hier_Node_NewType(HIERARCHY_LABEL);
	Hier_Lists_AddNode(shHier, labelBreak);

	EnqueuePrevNode(labelBreak);

	// Add the line information
	C2Hier_SetNodeLines(nodeLoop, TreeDo);
	C2Hier_SetNodeLines(labelCont, TreeDo);
	C2Hier_SetNodeLines(labelBreak, TreeDo);

	// Save the shared variables

	hier_node_t* oldPrevFirst = shPrevNodeFirst;
	hier_node_t* oldPrevLast = shPrevNodeLast;
	hier_node_t* oldLoopCont = shLabelCont;
	hier_node_t* oldLoopBreak = shLabelBreak;

	shPrevNodeFirst = NULL;
	shPrevNodeLast = NULL;
	shLabelCont = labelCont;
	shLabelBreak = labelBreak;

	// Process the body AFTER: the test

	hvex_t* VexCond = NULL;

	if(DoBody!=NULL || DoCond!=NULL) {
		shPrevNodeFirst = NULL;
		shPrevNodeLast = NULL;

		EnqueuePrevNode(labelCont);

		if(DoBody!=NULL) C2Hier_NodeChain(DoBody);

		assert(shNextNodeFirst==NULL);

		VexCond = C2Hier_BoolCondSigAsg(DoCond);

		assert(shNextNodeFirst==NULL);
		loop_data->body_after = shPrevNodeFirst;
		if(loop_data->body_after!=NULL) {
			loop_data->body_after->PARENT = nodeLoop;
			nodeLoop->CHILDREN = addchain(nodeLoop->CHILDREN, loop_data->body_after);
		}

		shPrevNodeFirst = NULL;
		shPrevNodeLast = NULL;
	}
	else 	{
		VexCond = hvex_newlit_bit('1');
	}

	loop_data->testinfo->cond = VexCond;

	// Set the old values again

	shPrevNodeFirst = oldPrevFirst;
	shPrevNodeLast = oldPrevLast;
	shLabelCont = oldLoopCont;
	shLabelBreak = oldLoopBreak;
}

static void C2Hier_If(c_tree_node *TreeIf) {
	c_tree_node* IfCond = CTreeInstIfCond( TreeIf );
	c_tree_node* IfThen = CTreeInstIfThen( TreeIf );
	c_tree_node* IfElse = CTreeInstIfElse( TreeIf );

	// Get the label in the previous node if applicable
	char* label_name = C2Hier_GetPrevLabelName();

	// Set the branch condition to the TRUE branch
	hvex_t* VexCond = C2Hier_BoolCondSigAsg(IfCond);

	// Create the SWITCH node

	hier_node_t* nodeSwitch = Hier_Node_NewType(HIERARCHY_SWITCH);
	hier_switch_t* switch_data = &nodeSwitch->NODE.SWITCH;
	switch_data->info.was_if = 1;
	switch_data->label_name = label_name;

	Hier_Lists_AddNode(shHier, nodeSwitch);
	AddNodes_C2H(TreeIf, nodeSwitch);

	assert(shNextNodeFirst==NULL);
	EnqueuePrevNode(nodeSwitch);

	// Create the branches

	hier_node_t* nodeThen = Hier_Node_NewType(HIERARCHY_CASE);
	hier_node_t* nodeElse = Hier_Node_NewType(HIERARCHY_CASE);

	nodeThen->PARENT = nodeSwitch;
	nodeSwitch->CHILDREN = addchain(nodeSwitch->CHILDREN, nodeThen);
	nodeElse->PARENT = nodeSwitch;
	nodeSwitch->CHILDREN = addchain(nodeSwitch->CHILDREN, nodeElse);

	Hier_Lists_AddNode(shHier, nodeThen);
	Hier_Lists_AddNode(shHier, nodeElse);

	// Add the line information
	C2Hier_SetNodeLines(nodeSwitch, TreeIf);
	C2Hier_SetNodeLines(nodeThen, TreeIf);
	C2Hier_SetNodeLines(nodeElse, TreeIf);

	// Assign the test to the TRUE branch

	hier_testinfo_t* testinfo = Hier_TestInfo_New();
	testinfo->cond = VexCond;

	hier_case_t* case_data = &nodeThen->NODE.CASE;
	avl_p_add_overwrite(&case_data->values, testinfo, testinfo);

	// Set the ELSE branch as default

	case_data = &nodeElse->NODE.CASE;
	case_data->is_default = true;
	switch_data->info.default_case = nodeElse;

	// Save the shared variables

	hier_node_t* oldPrevFirst = shPrevNodeFirst;
	hier_node_t* oldPrevLast = shPrevNodeLast;

	shPrevNodeFirst = NULL;
	shPrevNodeLast = NULL;

	// Scan the TRUE branch

	if(IfThen!=NULL) {
		shPrevNodeFirst = NULL;
		shPrevNodeLast = NULL;

		C2Hier_NodeChain(IfThen);

		assert(shNextNodeFirst==NULL);
		case_data = &nodeThen->NODE.CASE;
		case_data->body = shPrevNodeFirst;
		if(case_data->body!=NULL) {
			case_data->body->PARENT = nodeThen;
			nodeThen->CHILDREN = addchain(nodeThen->CHILDREN, case_data->body);
		}

		shPrevNodeFirst = NULL;
		shPrevNodeLast = NULL;
	}

	// Scan the FALSE branch

	if(IfElse!=NULL) {
		shPrevNodeFirst = NULL;
		shPrevNodeLast = NULL;

		C2Hier_NodeChain(IfElse);

		assert(shNextNodeFirst==NULL);
		case_data = &nodeElse->NODE.CASE;
		case_data->body = shPrevNodeFirst;
		if(case_data->body!=NULL) {
			case_data->body->PARENT = nodeElse;
			nodeElse->CHILDREN = addchain(nodeElse->CHILDREN, case_data->body);
		}

		shPrevNodeFirst = NULL;
		shPrevNodeLast = NULL;
	}

	// Set the old values again

	shPrevNodeFirst = oldPrevFirst;
	shPrevNodeLast = oldPrevLast;
}

static void C2Hier_Switch(c_tree_node *TreeSwitch) {
	c_tree_node* SwitchOn   = CTreeInstSwitchOn(TreeSwitch);
	c_tree_node* SwitchBody = CTreeInstSwitchBody(TreeSwitch);

	// Get the label in the previous node if applicable
	char* label_name = C2Hier_GetPrevLabelName();

	// Process the tested value

	hvex_t* VexExpr = C2Hier_TreeExpr(SwitchOn);
	if(shNextNodeFirst!=NULL) {
		errprintfa("At %s:%u: Usage of postInc/postDec operations in Switch tests is not handled.\n",
			CTreeFileName(TreeSwitch), CTreeLineNum(TreeSwitch)
		);
	}

	// Create a State for the test Actions

	hier_node_t* nodeState = Hier_Node_NewType(HIERARCHY_STATE);
	Hier_Lists_AddNode(shHier, nodeState);
	EnqueuePrevNode(nodeState);

	// Create the SWITCH node

	hier_node_t* nodeSwitch = Hier_Node_NewType(HIERARCHY_SWITCH);
	hier_switch_t* switch_data = &nodeSwitch->NODE.SWITCH;
	switch_data->info.switch_test = VexExpr;
	switch_data->label_name = label_name;

	Hier_Lists_AddNode(shHier, nodeSwitch);
	AddNodes_C2H(TreeSwitch, nodeSwitch);

	assert(shNextNodeFirst==NULL);
	EnqueuePrevNode(nodeSwitch);

	// Create label for BREAK

	hier_node_t* labelBreak = Hier_Node_NewType(HIERARCHY_LABEL);
	Hier_Lists_AddNode(shHier, labelBreak);

	// Add the line information
	C2Hier_SetNodeLines(nodeState, TreeSwitch);
	C2Hier_SetNodeLines(nodeSwitch, TreeSwitch);
	C2Hier_SetNodeLines(labelBreak, TreeSwitch);

	EnqueuePrevNode(labelBreak);

	// Save the shared variables

	hier_node_t* oldPrevFirst = shPrevNodeFirst;
	hier_node_t* oldPrevLast = shPrevNodeLast;
	hier_node_t* oldLabelBreak = shLabelBreak;
	hier_node_t* oldSwitch = shCurSwitch;
	hier_node_t* oldCase = shCurCase;

	shPrevNodeFirst = NULL;
	shPrevNodeLast = NULL;
	shCurSwitch = nodeSwitch;
	shCurCase = NULL;
	shLabelBreak = labelBreak;

	// Convert the body

	C2Hier_NodeChain(SwitchBody);

	if(shCurCase!=NULL) {
		hier_case_t* prevcase_data = &shCurCase->NODE.CASE;
		// Assign the body
		if(shPrevNodeFirst!=NULL) {
			prevcase_data->body = shPrevNodeFirst;
			shPrevNodeFirst->PARENT = shCurCase;
			shCurCase->CHILDREN = addchain(shCurCase->CHILDREN, shPrevNodeFirst);
			shPrevNodeFirst = NULL;
			shPrevNodeLast = NULL;
		}
	}
	shCurCase = NULL;

	// If no default case, create an empty one

	if(switch_data->info.default_case==NULL) {
		hier_node_t* nodeCase = Hier_Node_NewType(HIERARCHY_CASE);
		Hier_Lists_AddNode(shHier, nodeCase);
		nodeCase->PARENT = nodeSwitch;
		nodeSwitch->CHILDREN = addchain(nodeSwitch->CHILDREN, nodeCase);
		nodeCase->NODE.CASE.is_default = 1;
		switch_data->info.default_case = nodeCase;
	}

	// Scan all Cases and link the test Actions to the previously created State
	foreach(nodeSwitch->CHILDREN, scanCase) {
		hier_node_t* nodeCase = scanCase->DATA;
		hier_case_t* case_data = &nodeCase->NODE.CASE;
		avl_p_foreach(&case_data->values, scanVal) {
			hier_testinfo_t* testinfo = scanVal->data;
			assert(testinfo->sig_asg!=NULL);
			assert(testinfo->sig_asg->node==NULL);
			Hier_Action_Link(nodeState, testinfo->sig_asg);
		}
	}

	// Set the old values again

	shPrevNodeFirst = oldPrevFirst;
	shPrevNodeLast = oldPrevLast;
	shLabelBreak = oldLabelBreak;
	shCurSwitch = oldSwitch;
	shCurCase = oldCase;
}

static void C2Hier_Case(c_tree_node *TreeCase) {
	c_tree_node* CaseFrom = CTreeInstCaseFrom(TreeCase);
	int          CaseCode = CTreeInstCaseCode(TreeCase);

	assert(shCurSwitch!=NULL);
	hier_switch_t* switch_data = &shCurSwitch->NODE.SWITCH;

	// Read the case value

	hier_testinfo_t* testinfo = NULL;

	if(CaseCode==C_STANDARD_CASE) {
		shC2Hier_ModeNoAsg ++;  // Force mode "no assignment"
		hvex_t* VexCond = C2Hier_TreeExpr(CaseFrom);
		assert(VexCond->model==HVEX_LITERAL);
		shC2Hier_ModeNoAsg --;  // Back to previous mode

		if(VexCond->width > switch_data->info.switch_test->width) {
			printf("%s:%u: Unreachable case value. Ignoring current case.\n", CTreeFileName(CaseFrom), CTreeLineNum(CaseFrom));
			hvex_free(VexCond);
			return;
		}

		hvex_resize(VexCond, switch_data->info.switch_test->width);
		VexCond = hvex_newmodel_op2(HVEX_EQ, 1, hvex_dup(switch_data->info.switch_test), VexCond);

		// Create a Signal component to hold the test result
		char* name = Map_Netlist_MakeInstanceName_Prefix(shImplem, namealloc("augh_test"));
		netlist_comp_t* comp = Netlist_Comp_Sig_New(name, 1);
		Netlist_Comp_SetChild(shImplem->netlist.top, comp);
		// Set a flag to indicate the Sig can be duplicated when (later) creating several top-levels
		Implem_SymFlag_Add(shImplem, name, SYM_FLAG_CANDUP);

		// Create a new Action
		// Don't link this Action to a State. This will be done when scanning the Cases is finished.
		hier_action_t* action = Hier_Action_New();
		action->expr = hvex_asg_make(hvex_newvec_bit(name), NULL, VexCond, NULL);

		// Add the line information
		C2Hier_SetActLines(action, TreeCase);

		// Create the testinfo structure
		testinfo = Hier_TestInfo_New();
		testinfo->cond = hvex_newvec_bit(name);
		testinfo->sig_asg = action;
	}
	else {
		assert(CaseCode==C_DEFAULT_CASE);
	}

	// Create the CASE node

	hier_node_t* nodeCase = Hier_Node_NewType(HIERARCHY_CASE);
	hier_case_t* case_data = &nodeCase->NODE.CASE;
	nodeCase->PARENT = shCurSwitch;
	shCurSwitch->CHILDREN = addchain(shCurSwitch->CHILDREN, nodeCase);

	// Add the line information
	C2Hier_SetNodeLines(nodeCase, TreeCase);

	Hier_Lists_AddNode(shHier, nodeCase);

	if(CaseCode==C_DEFAULT_CASE) {
		assert(testinfo==NULL);
		case_data->is_default = true;
		switch_data->info.default_case = nodeCase;
	}
	else {
		assert(testinfo!=NULL);
		// Link the testinfo structure to the Case
		avl_p_add_overwrite(&case_data->values, testinfo, testinfo);
	}

	// If there was a previous case, assign the code to it
	// And link the cases previous/next

	if(shCurCase!=NULL) {
		hier_case_t* prevcase_data = &shCurCase->NODE.CASE;
		// Assign the body
		if(shPrevNodeFirst!=NULL) {
			prevcase_data->body = shPrevNodeFirst;
			shPrevNodeFirst->PARENT = shCurCase;
			shCurCase->CHILDREN = addchain(shCurCase->CHILDREN, shPrevNodeFirst);
			shPrevNodeFirst = NULL;
			shPrevNodeLast = NULL;
		}
		// Link the cases
		prevcase_data->build.case_after = nodeCase;
		case_data->build.case_before = shCurCase;
	}
	else {
		assert(shPrevNodeFirst==NULL);
	}

	// Set this Case as being the new current one
	shCurCase = nodeCase;
}

static void C2Hier_Label(c_tree_node *TreeLabel) {
	// Find the Hier node that was previously created but not linked
	hier_node_t* nodeLabel = GetNode_HFromC(TreeLabel);
	if(nodeLabel==NULL) {
		errprintfa("Label node not found at %s:%d\n", CTreeFileName(TreeLabel), CTreeLineNum(TreeLabel));
	}
	EnqueuePrevNode(nodeLabel);
}

static void C2Hier_Goto(c_tree_node *TreeGoto) {
	hier_node_t* nodeGoto = Hier_Node_NewType(HIERARCHY_JUMP);
	hier_jump_t* jump_data = &nodeGoto->NODE.JUMP;
	jump_data->type = HIER_JUMP_GOTO;

	Hier_Lists_AddNode(shHier, nodeGoto);
	AddNodes_C2H(TreeGoto, nodeGoto);

	EnqueuePrevNode(nodeGoto);

	c_tree_node* TreeLabel = CTreeInstGotoLabel(TreeGoto);
	hier_node_t* nodeLabel = GetNode_HFromC(TreeLabel);
	assert(nodeLabel!=NULL);
	Hier_JumpLabel_Link(nodeGoto, nodeLabel);

	// Add the line information
	C2Hier_SetNodeLines(nodeGoto, TreeGoto);
}

static void C2Hier_Continue(c_tree_node *TreeCont) {
	hier_node_t* nodeCont = Hier_Node_NewType(HIERARCHY_JUMP);
	hier_jump_t* jump_data = &nodeCont->NODE.JUMP;
	jump_data->type = HIER_JUMP_CONT;

	Hier_Lists_AddNode(shHier, nodeCont);
	AddNodes_C2H(TreeCont, nodeCont);

	EnqueuePrevNode(nodeCont);

	assert(shLabelCont!=NULL);
	Hier_JumpLabel_Link(nodeCont, shLabelCont);

	// Add the line information
	C2Hier_SetNodeLines(nodeCont, TreeCont);
}

static void C2Hier_Break(c_tree_node *TreeBreak) {
	hier_node_t* nodeBreak = Hier_Node_NewType(HIERARCHY_JUMP);
	hier_jump_t* jump_data = &nodeBreak->NODE.JUMP;
	jump_data->type = HIER_JUMP_BREAK;

	Hier_Lists_AddNode(shHier, nodeBreak);
	AddNodes_C2H(TreeBreak, nodeBreak);

	EnqueuePrevNode(nodeBreak);

	assert(shLabelBreak!=NULL);
	Hier_JumpLabel_Link(nodeBreak, shLabelBreak);

	// Add the line information
	C2Hier_SetNodeLines(nodeBreak, TreeBreak);
}

static void C2Hier_Return(c_tree_node *TreeReturn) {
	assert(shCurFunc!=NULL);
	//hier_proc_t* proc_data = &shCurFunc->nodeProc->NODE.PROCESS;

	// Add an Action for the return value

	c_tree_node* TreeValue = CTreeInstReturnValue(TreeReturn);
	if(TreeValue!=NULL) {
		assert(shCurFunc->vex_ret!=NULL);

		// Get the return expression
		hvex_t* VexExpr = C2Hier_TreeExpr(TreeValue);
		hvex_resize(VexExpr, shCurFunc->vex_ret->width);

		// Create an Action to assign the return value
		hier_action_t* action = Enqueue_NewStateAction(true);
		action->expr = hvex_asg_make(hvex_dup(shCurFunc->vex_ret), NULL, VexExpr, NULL);

		// In case there were post-inc/dec in the return expression
		EnqueueNextInPrev();

		// Add the line information
		C2Hier_SetActLines(action, TreeReturn);
		C2Hier_SetNodeLines(action->node, TreeReturn);
	}

	// Add the RETURN node

	hier_node_t* nodeRet = Hier_Node_NewType(HIERARCHY_RETURN);
	Hier_RetProc_Link(nodeRet, shCurFunc->nodeProc);

	Hier_Lists_AddNode(shHier, nodeRet);
	AddNodes_C2H(TreeReturn, nodeRet);
	EnqueuePrevNode(nodeRet);

	// Add the line information
	C2Hier_SetNodeLines(nodeRet, TreeReturn);
}



//===========================================================
// Scan of chains of CTree nodes
//===========================================================

static void C2Hier_NodeChain(c_tree_node *TreeNode) {
	if(TreeNode==NULL) return;
	if(TreeNode==c_error_mark_node) {
		errprintfa("Parse error at %s:%d\n", CTreeFileName(TreeNode), CTreeLineNum(TreeNode));
	}

	int ExitChain = 0;

	while(TreeNode!=NULL) {
		assert(shNextNodeFirst==NULL);
		assert(shNextNodeLast==NULL);

		int FirstCode = CTreeNodeFirstCode(TreeNode);

		switch(FirstCode) {

			case C_LIST_NODE:
				C2Hier_NodeChain(CTreeListValue(TreeNode));
				break;

			case C_TYPE_NODE:
				// Nothing more to do
				break;

			case C_BLOCK_NODE: {
				ctree_foreach(CTreeBlockVars(TreeNode), TreeDecl) {
					int Code = CTreeNodeCode( TreeDecl );

					switch(Code) {
						case C_VAR_DECL: {
							C2Hier_DeclVar(TreeDecl);
							break;
						}
						case C_LABEL_DECL: {
							// Create the Label node in advance so it's always found by Goto
							C2Hier_DeclLabel(TreeDecl);
							break;
						}
						// FIXME What happens in other situations? Which ones are they?
					}

				}
				C2Hier_NodeChain(CTreeBlockBody(TreeNode));
				break;
			}

			case C_EXPR_NODE: {
				hvex_t* VexExpr = C2Hier_TreeExpr(TreeNode);
				if(VexExpr!=NULL) hvex_free(VexExpr);
				EnqueueNextInPrev();
				break;
			}

			case C_INST_NODE: {
				int InstCode = CTreeInstCode( TreeNode );
				switch(InstCode) {
					case C_IF_INST:
						C2Hier_If(TreeNode);
						break;
					case C_FOR_INST:
						C2Hier_For( TreeNode );
						break;
					case C_WHILE_INST:
						C2Hier_While(TreeNode);
						break;
					case C_DO_INST:
						C2Hier_Do(TreeNode);
						break;
					case C_GOTO_INST:
						C2Hier_Goto(TreeNode);
						break;
					case C_RETURN_INST:
						C2Hier_Return(TreeNode);
						break;
					case C_CONTINUE_INST:
						C2Hier_Continue(TreeNode);
						break;
					case C_BREAK_INST:
						C2Hier_Break(TreeNode);
						break;
					case C_SWITCH_INST:
						C2Hier_Switch(TreeNode);
						break;
					case C_CASE_INST:
						C2Hier_Case(TreeNode);
						break;
					default : {
						errprintfa("Invalid InstCode at %s:%d : Code %u(dec) %x(hex).\n", CTreeFileName(TreeNode), CTreeLineNum(TreeNode), InstCode, InstCode);
					}
				}
				break;
			}

			case C_DECL_NODE: {
				int Code = CTreeNodeCode( TreeNode );

				switch ( Code ) {

					case C_LABEL_DECL: {
						// This is the real place where the Label was written in the source file
						C2Hier_Label(TreeNode);
						break;
					}

					case C_VAR_DECL:
						// FIXME Why ignore var declarations here?
						break;
					case C_TYPE_DECL:
						// Ignoring typedef implicit or explicit
						break;
					default:
						errprintfa("Illegal instruction at %s:%d : Code %u(dec) %x(hex).\n", CTreeFileName(TreeNode), CTreeLineNum(TreeNode), Code, Code);
				}

				// These declarations are just pointers inside the list of declarations, which is stored elsewhere.
				// So we must exit the loop right there.
				// FIXME Not sure if this is the correct way to handle this.
				ExitChain = 1;

				break;
			}

			default: {
				errprintfa("Illegal instruction at %s:%d : FirstCode %u(dec) %x(hex).\n", CTreeFileName(TreeNode), CTreeLineNum(TreeNode), FirstCode, FirstCode);
			}

		}  // Switch on FirstCode

		// Next node
		if(ExitChain!=0) break;
		TreeNode = CTreeChain(TreeNode);

	}  // Loop on the nodes of the chain

}

static void C2Hier_CDeclFunc(c_tree_node* TreeDecl) {
	assert(shCurFunc==NULL);

	assert(shPrevNodeFirst==NULL);
	assert(shPrevNodeLast==NULL);
	assert(shNextNodeFirst==NULL);
	assert(shNextNodeLast==NULL);

	// Check the name of the function
	c_tree_node *DeclIdent = CTreeDeclName(TreeDecl);
	char* name = CTreeIdentPointer(DeclIdent);
	name = stralloc(name);

	//dbgprintf("Declaration of function '%s' at %s:%u\n", name, CTreeFileName(TreeDecl), CTreeLineNum(TreeDecl));

	if(Hier_GetProc(shHier, name)!=NULL) {
		errprintfa("Function '%s' declared multiple times.\n", name);
	}

	// Begin conversion of a new function
	hier_node_t* nodeProc = Hier_Node_NewType(HIERARCHY_PROCESS);
	hier_proc_t* proc_data = &nodeProc->NODE.PROCESS;
	proc_data->name_orig = name;

	Hier_Lists_AddNode(shHier, nodeProc);

	shCurFunc = AddFuncInfo_FromDecl(TreeDecl);
	shCurFunc->name = name;
	shCurFunc->nodeProc = nodeProc;

	// Add the line information
	C2Hier_SetNodeLines(nodeProc, TreeDecl);

	// Flag if inline
	if(CIsTreeDeclInline(TreeDecl)) shCurFunc->is_inline = 1;
	if(CIsTreeNodeStatic(TreeDecl)) shCurFunc->is_static = 1;
	if(CIsTreeNodeExternal(TreeDecl)) shCurFunc->is_static = 0;

	//dbgprintf("  DECL FLAGS    %lu(dec) %lx(hex)\n", TreeDecl->DECL.FLAGS, TreeDecl->DECL.FLAGS);
	//dbgprintf("  COMMON FLAGS  %lu(dec) %lx(hex)\n", TreeDecl->COMMON.FLAGS, TreeDecl->COMMON.FLAGS);

	c_tree_node* FuncBody = CTreeDeclInitial(TreeDecl);
	if(FuncBody==NULL) {
		shCurFunc->only_declared = 1;
		proc_data->user.only_declared = true;
	}
	else {
		shCurFunc->only_declared = 0;
		proc_data->user.only_declared = false;
	}

	c_tree_node* FuncArgs = CTreeDeclArguments(TreeDecl);

	c_tree_node* FuncResult = CTreeDeclResult(TreeDecl);
	if(FuncResult==NULL) FuncResult = CTreeType(TreeDecl);

	// Return value
	if(CTreeType(FuncResult)!=c_void_type_node) {
		c_tree_node* DeclType = CTreeType(FuncResult);
		char* retname = namealloc(name);
		hvex_t* VexAtom = hvex_dup(C2Hier_TreeType2Vex(DeclType));
		//dbgprintf("Func %s: return: ", name); hvex_print_bn(VexAtom);
		C2Hier_RenameAllVexAtom(VexAtom, retname);
		shCurFunc->vex_ret = VexAtom;
		shCurFunc->vex_ret_name = retname;
		// Create a Register in the netlist to hold the return value
		assert(Netlist_Comp_GetChild(shImplem->netlist.top, retname)==NULL);
		netlist_comp_t* comp = Netlist_Comp_Reg_New(retname, VexAtom->width);
		Netlist_Comp_SetChild(shImplem->netlist.top, comp);
		// Set a flag to indicate the Sig can be duplicated when (later) creating several top-levels
		Implem_SymFlag_Add(shImplem, retname, SYM_FLAG_CANDUP);
	}

	// Arguments
	for( ; FuncArgs!=NULL; FuncArgs=CTreeChain(FuncArgs)) {
		C2Hier_CDecl(FuncArgs);
	}

	// Convert the function body

	// Flag to detect recursive use of inline
	addauthelem(shC2Hier_CyclicFunc, name, 0);

	// Begin conversion of the function body
	C2Hier_NodeChain(FuncBody);

	// Clear flag
	delauthelem(shC2Hier_CyclicFunc, name);

	assert(shNextNodeFirst==NULL);
	assert(shNextNodeLast==NULL);

	// Get the body nodes
	if(shPrevNodeFirst!=NULL) {
		Hier_Nodes_Link(nodeProc, shPrevNodeFirst);
		shPrevNodeFirst = NULL;
		shPrevNodeLast = NULL;
	}

	// Ensure there is a RETURN node at end of main level of function
	hier_node_t* lastnode = nodeProc;
	while(lastnode->NEXT!=NULL) lastnode = lastnode->NEXT;
	if(lastnode->TYPE!=HIERARCHY_RETURN) {
		hier_node_t* nodeRet = Hier_Node_NewType(HIERARCHY_RETURN);
		Hier_RetProc_Link(nodeRet, nodeProc);
		Hier_Lists_AddNode(shHier, nodeRet);
		Hier_Nodes_Link(lastnode, nodeRet);
	}

	#ifndef NDEBUG  // For debug: check Hier integrity
	Hier_Check_Integrity(shHier);
	#endif

	shCurFunc = NULL;
}

// FIXME This duplicates stuff in C2Hier_NodeChain?
static void C2Hier_CDecl(c_tree_node *TreeDecl) {
	if(C2Hier_IsNodeDone(TreeDecl)) return;

	int DeclCode = CTreeDeclCode(TreeDecl);

	switch(DeclCode) {
		case C_TYPE_DECL :
		case C_CONST_DECL :
			break;

		case C_PARAM_DECL :
			C2Hier_DeclVar(TreeDecl);
			break;

		case C_VAR_DECL :
			C2Hier_DeclVar(TreeDecl);
			break;

		case C_FUNCTION_DECL :
			C2Hier_CDeclFunc(TreeDecl);
			break;

		default: {
			errprintf("Unhandled CTree decl code at %s:%d : %u(dec) %x(hex).\n",
				CTreeFileName(TreeDecl), CTreeLineNum(TreeDecl), DeclCode, DeclCode
			);
			c_tree_view_node(TreeDecl);
			abort();
		}
	}

	C2Hier_SetNodeDone(TreeDecl);
}

// FIXME This duplicates stuff in C2Hier_NodeChain?
static void C2Hier_CNode(c_tree_node *TreeNode) {
	if(TreeNode==NULL) return;
	if(TreeNode==c_error_mark_node) {
		errprintfa("Parse error at %s:%d\n", CTreeFileName(TreeNode), CTreeLineNum(TreeNode));
	}

	for( ; TreeNode!=NULL; TreeNode=CTreeChain(TreeNode)) {
		int FirstCode = CTreeNodeFirstCode( TreeNode );

		switch(FirstCode) {
			case C_LIST_NODE: {
				C2Hier_CNode(CTreeListValue(TreeNode));
				break;
			}
			case C_DECL_NODE: {
				C2Hier_CDecl(TreeNode);
				return;
			}
			case C_TYPE_NODE: {
				// FIXME Really nothing to do?
				return;
			}
			default: {
				errprintf("Unhandled CTree node at %s:%d : %u(dec) %x(hex).\n",
					CTreeFileName(TreeNode), CTreeLineNum(TreeNode), FirstCode, FirstCode
				);
				c_tree_view_node(TreeNode);
				abort();
			}
		}

	}
}



//===========================================================
// The main function
//===========================================================

static void C2Hier_RemoveInlinedFunc() {
	chain_list* list_proc_del = NULL;

	// List the Proc to remove
	avl_pp_foreach(&shFuncInfo, scanFI) {
		c2hier_funcinfo_t* funcinfo = scanFI->data;
		if(funcinfo->is_inline!=0) {
			list_proc_del = addchain(list_proc_del, funcinfo);
		}
		else {
			// Copy info about function arguments and return value
			hier_proc_t* proc_data = &funcinfo->nodeProc->NODE.PROCESS;
			proc_data->build.vex_ret = hvex_dup(funcinfo->vex_ret);
			proc_data->build.list_args = dupchain(funcinfo->list_args);
			foreach(proc_data->build.list_args, scanArg) {
				c2hier_declinfo_t* declinfo = scanArg->DATA;
				assert(declinfo->vex_atom->model==HVEX_VECTOR);
				scanArg->DATA = hvex_dup(declinfo->vex_atom);
			}
		}
	}  // Scan function declarations

	// Remove the processes
	foreach(list_proc_del, scanFI) {
		c2hier_funcinfo_t* funcinfo = scanFI->DATA;
		hier_proc_t* proc_data = &funcinfo->nodeProc->NODE.PROCESS;
		assert(proc_data->calls==NULL);
		Hier_RemoveProc(shHier, funcinfo->nodeProc);
		funcinfo->nodeProc = NULL;  // Paranoia
	}
	freechain(list_proc_del);

}

static int C2Hier_AllCNodes(implem_t* Implem, c_tree_node *TreeNode) {

	// Allocation for shared storage elements
	avl_pp_init(&shC2Hier_Type2Vex);
	avl_pp_init(&shC2Hier_Field2Vex);
	shC2Hier_Type2CellsNb = createauthtable(100);

	shC2Hier_CyclicFunc = createauthtable(10);

	avl_pp_init(&shC2Hier);
	avl_pp_init(&shDeclInfo);
	avl_pp_init(&shName2DI);
	avl_pp_init(&shFuncInfo);

	// Set the main shared variables
	shImplem = Implem;
	shHier = Implem->H;

	// Launch conversion of the entire CTree graph
	C2Hier_CNode(TreeNode);

	// Remove the inlined functions
	// Note: Don't remove the unused functions because there are the top-level functions
	C2Hier_RemoveInlinedFunc();

	// Clean

	avl_pp_foreach(&shC2Hier_Type2Vex, scan) hvex_free(scan->data);
	avl_pp_reset(&shC2Hier_Type2Vex);
	avl_pp_foreach(&shC2Hier_Field2Vex, scan) hvex_free(scan->data);
	avl_pp_reset(&shC2Hier_Field2Vex);
	destroyauthtable(shC2Hier_Type2CellsNb);

	destroyauthtable(shC2Hier_CyclicFunc);

	avl_pp_reset(&shC2Hier);

	avl_pp_foreach(&shDeclInfo, scan) {
		c2hier_declinfo_t* declinfo = scan->data;
		if(declinfo->vex_atom!=NULL) hvex_free(declinfo->vex_atom);
		if(declinfo->vex_init!=NULL) hvex_free(declinfo->vex_init);
		free(declinfo);
	}
	avl_pp_reset(&shDeclInfo);
	avl_pp_reset(&shName2DI);

	avl_pp_foreach(&shFuncInfo, scan) {
		c2hier_funcinfo_t* funcinfo = scan->data;
		freechain(funcinfo->list_args);
		if(funcinfo->vex_ret!=NULL) hvex_free(funcinfo->vex_ret);
		free(funcinfo);
	}
	avl_pp_reset(&shFuncInfo);

	return 0;
}

int C2Hier(implem_t* Implem, char* filename, int argc, char** argv) {
	if(Implem->H!=NULL) {
		errprintf("A Hier graph already exists. Won't replace.\n");
		return -1;
	}
	Implem->H = Hier_New();

	c_tree_node *TreeNode = c_tree_parse_file(filename, argc, argv);

	#if 0  // Debug: display the entire CTree
	c_tree_view_node_list(TreeNode);
	exit(EXIT_FAILURE);
	#endif

	int ret = C2Hier_AllCNodes(Implem, TreeNode);

	c_tree_free_all_node();

	Implem_CheckIntegrity(Implem);

	#if 0  // Debug: display the entire Hier
	Hier_Print(Implem->H);
	#endif

	return ret;
}


