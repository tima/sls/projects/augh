/*------------------------------------------------------------\
|                                                             |
| This file is part of the Alliance CAD System Copyright      |
| (C) Laboratoire LIP6 - Département ASIM Universite P&M Curie|
|                                                             |
| Home page      : http://www-asim.lip6.fr/alliance/          |
| E-mail support : mailto:alliance-support@asim.lip6.fr       |
|                                                             |
| This progam is  free software; you can redistribute it      |
| and/or modify it under the  terms of the GNU Library General|
| Public License as published by the Free Software Foundation |
| either version 2 of the License, or (at your option) any    |
| later version.                                              |
|                                                             |
| Alliance VLSI  CAD System  is distributed  in the hope that |
| it  will be useful, but WITHOUT  ANY WARRANTY;              |
| without even the  implied warranty of MERCHANTABILITY or    |
| FITNESS FOR A PARTICULAR PURPOSE. See the GNU General       |
| Public License for more details.                            |
|                                                             |
| You should have received a copy  of the GNU General Public  |
| License along with the GNU C Library; see the file COPYING. |
| If not, write to the Free Software Foundation, Inc.,        |
| 675 Mass Ave, Cambridge, MA 02139, USA.                     |
|                                                             |
\------------------------------------------------------------*/
#ifndef __P
# if defined(__STDC__) ||  defined(__GNUC__)
#  define __P(x) x
# else
#  define __P(x) ()
# endif
#endif
/*------------------------------------------------------------\
|                                                             |
| Tool    :                     C                           |
|                                                             |
| File    :                  c_cdecl.h                      |
|                                                             |
| Date    :                   09.07.99                        |
|                                                             |
| Author  :               Jacomme Ludovic                     |
|                                                             |
\------------------------------------------------------------*/

# ifndef CPP_CDECL_H
# define CPP_CDECL_H

/*------------------------------------------------------------\
|                                                             |
|                           Constants                         |
|                                                             |
\------------------------------------------------------------*/

# define C_INT_TYPE_SIZE           (sizeof(int )*8)
# define C_CHAR_TYPE_SIZE          (sizeof(char)*8)
# define C_LONG_TYPE_SIZE          (sizeof(long)*8)
# define C_LONG_LONG_TYPE_SIZE     (sizeof(long long int)*8)
# define C_SHORT_TYPE_SIZE         (sizeof(short)*8)
# define C_FLOAT_TYPE_SIZE         (sizeof(float)*8)
# define C_DOUBLE_TYPE_SIZE        (sizeof(double)*8)
# define C_LONG_DOUBLE_TYPE_SIZE   (sizeof(long double)*8)
# define C_POINTER_TYPE_SIZE       (sizeof(char *)*8)
# define C_POINTER_SIZE            C_POINTER_TYPE_SIZE

# define C_HOST_BITS_PER_WIDE_INT  (int)C_INT_TYPE_SIZE
# define C_HOST_BITS_PER_LONG      (int)C_LONG_TYPE_SIZE
# define C_HOST_BITS_PER_INT       (int)C_INT_TYPE_SIZE
# define C_HOST_BITS_PER_CHAR      (int)C_CHAR_TYPE_SIZE

# define C_HOST_WIDE_INT           int

# define C_WCHAR_TYPE_SIZE         C_INT_TYPE_SIZE
# define C_BITS_PER_UNIT           C_CHAR_TYPE_SIZE
# define C_WCHAR_BYTES             1
# define C_BYTES_BIG_ENDIAN        0
# define C_BITS_PER_WORD           (int)C_INT_TYPE_SIZE

/*------------------------------------------------------------\
|                                                             |
|                           Declar                            |
|                                                             |
\------------------------------------------------------------*/

# define C_CDECL_NORMAL    1
# define C_CDECL_FUNCDEF   2
# define C_CDECL_PARAM     3
# define C_CDECL_FIELD     4
# define C_CDECL_BIT_FIELD 5
# define C_CDECL_TYPENAME  6


#define BITS_PER_UNIT 8
#define BITS_PER_WORD                32
#define TARGET_BELL 007
#define TARGET_BS 010
#define TARGET_FF 014
#define TARGET_NEWLINE 012
#define TARGET_CR 015
#define TARGET_TAB 011
#define TARGET_VT 013

#define FATAL_EXIT_CODE 33
#define SUCCESS_EXIT_CODE 0

#define INCLUDE_DEFAULTS { \
	{ "/usr/include", 0, 0, 0 }, \
	{ "/usr/include/g++", "G++", 1, 1 }, \
	{ 0, 0, 0, 0} \
	}

#define GCC_INCLUDE_DIR "/usr/include"

#define PREFIX "/usr/local"


/* auto-config.h.  Generated automatically by configure.  */
/* config.in.  Generated automatically from configure.in by autoheader.  */
/* Define if you have a working <inttypes.h> header file.  */
/* #undef HAVE_INTTYPES_H */

/* Whether malloc must be declared even if <stdlib.h> is included.  */
/* #undef NEED_DECLARATION_MALLOC */

/* Whether realloc must be declared even if <stdlib.h> is included.  */
/* #undef NEED_DECLARATION_REALLOC */

/* Whether calloc must be declared even if <stdlib.h> is included.  */
/* #undef NEED_DECLARATION_CALLOC */

/* Whether free must be declared even if <stdlib.h> is included.  */
/* #undef NEED_DECLARATION_FREE */

/* Whether index must be declared even if <stdlib.h> is included.  */
#define NEED_DECLARATION_INDEX 1

/* Whether rindex must be declared even if <stdlib.h> is included.  */
#define NEED_DECLARATION_RINDEX 1

/* Whether getenv must be declared even if <stdlib.h> is included.  */
/* #undef NEED_DECLARATION_GETENV */

/* Whether sbrk must be declared even if <stdlib.h> is included.  */
#define NEED_DECLARATION_SBRK 1

/* Define if you have the ANSI C header files.  */
/* #undef STDC_HEADERS */

/* Define if `sys_siglist' is declared by <signal.h>.  */
/* #undef SYS_SIGLIST_DECLARED */

/* Define if you can safely include both <sys/time.h> and <time.h>.  */
#define TIME_WITH_SYS_TIME 1

/* Define if you have the bcmp function.  */
#define HAVE_BCMP 1

/* Define if you have the bcopy function.  */
#define HAVE_BCOPY 1

/* Define if you have the bzero function.  */
#define HAVE_BZERO 1

/* Define if you have the getrlimit function.  */
#define HAVE_GETRLIMIT 1

/* Define if you have the index function.  */
#define HAVE_INDEX 1

/* Define if you have the kill function.  */
#define HAVE_KILL 1

/* Define if you have the popen function.  */
#define HAVE_POPEN 1

/* Define if you have the putenv function.  */
#define HAVE_PUTENV 1

/* Define if you have the rindex function.  */
#define HAVE_RINDEX 1

/* Define if you have the setrlimit function.  */
#define HAVE_SETRLIMIT 1

/* Define if you have the strerror function.  */
/* #undef HAVE_STRERROR */

/* Define if you have the sysconf function.  */
#define HAVE_SYSCONF 1

/* Define if you have the vprintf function.  */
#define HAVE_VPRINTF 1

/* Define if you have the <fcntl.h> header file.  */
#define HAVE_FCNTL_H 1

/* Define if you have the <limits.h> header file.  */
#define HAVE_LIMITS_H 1

/* Define if you have the <stddef.h> header file.  */
#define HAVE_STDDEF_H 1

/* Define if you have the <stdlib.h> header file.  */
#define HAVE_STDLIB_H 1

/* Define if you have the <string.h> header file.  */
#define HAVE_STRING_H 1

/* Define if you have the <strings.h> header file.  */
#define HAVE_STRINGS_H 1

/* Define if you have the <sys/file.h> header file.  */
#define HAVE_SYS_FILE_H 1

/* Define if you have the <sys/param.h> header file.  */
#define HAVE_SYS_PARAM_H 1

/* Define if you have the <sys/resource.h> header file.  */
#define HAVE_SYS_RESOURCE_H 1

/* Define if you have the <sys/time.h> header file.  */
#define HAVE_SYS_TIME_H 1

/* Define if you have the <sys/times.h> header file.  */
#define HAVE_SYS_TIMES_H 1

/* Define if you have the <time.h> header file.  */
#define HAVE_TIME_H 1

/* Define if you have the <unistd.h> header file.  */
#define HAVE_UNISTD_H 1
/*------------------------------------------------------------\
|                                                             |
|                            Macro                            |
|                                                             |
\------------------------------------------------------------*/

# define c_resume_momentary(N)

/*------------------------------------------------------------\
|                                                             |
|                            Types                            |
|                                                             |
\------------------------------------------------------------*/


/*------------------------------------------------------------\
|                                                             |
|                          Variables                          |
|                                                             |
\------------------------------------------------------------*/


/*------------------------------------------------------------\
|                                                             |
|                          Functions                          |
|                                                             |
\------------------------------------------------------------*/


# endif
