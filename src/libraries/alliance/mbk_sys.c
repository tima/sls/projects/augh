/* 
 * This file is part of the Alliance CAD System
 * Copyright (C) Laboratoire LIP6 - Département ASIM
 * Universite Pierre et Marie Curie
 * 
 * Home page          : http://www-asim.lip6.fr/alliance/
 * E-mail support     : mailto:alliance-support@asim.lip6.fr
 * 
 * This library is free software; you  can redistribute it and/or modify it
 * under the terms  of the GNU Library General Public  License as published
 * by the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 * 
 * Alliance VLSI  CAD System  is distributed  in the hope  that it  will be
 * useful, but WITHOUT  ANY WARRANTY; without even the  implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 * 
 * You should have received a copy  of the GNU General Public License along
 * with the GNU C Library; see the  file COPYING. If not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/* 
 * Purpose : system dependant functions
 * Date    : 06/03/92
 * Author  : Frederic Petrot <Frederic.Petrot@lip6.fr>
 * Modified by Czo <Olivier.Sirol@lip6.fr> 1997,98
 */

/*
static char rcsid[] = "$Id: mbk_sys.c 1 2010-04-12 12:02:04Z ia $";
*/

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <errno.h>

#include "mut.h"
#include "mbk_sys.h"

static char filename[4096];

long mbkalloc_stat = 0; /* statistics on maximun allocated memory */

void *mbkalloc(nbytes)
unsigned int nbytes;
{
void 	*pt;

	mbkalloc_stat += nbytes;
	if (!(pt = malloc(nbytes))) {
		(void)fflush(stdout);
  		(void)fprintf(stderr,"*** mbk error ***\n");
  		(void)fprintf(stderr,"fatal mbkalloc error : not enough memory\n");
  		EXIT(1);
  	}
	return pt;
}

void *mbkrealloc(pt, nbytes)
void 	*pt;
unsigned int nbytes;
{

	mbkalloc_stat += nbytes;
	if (!(pt = realloc(pt, nbytes))) {
		(void)fflush(stdout);
  		(void)fprintf(stderr,"*** mbk error ***\n");
  		(void)fprintf(stderr,"fatal mbkrealloc error : not enough memory\n");
  		EXIT(1);
  	}
	return pt;
}

void mbkfree(ptr)
void *ptr;
{
#ifdef lint
	(void)free((char *)ptr);
#else
	(void)free(ptr); /* lint makes a mistake here : expects (char *) */
#endif
}

/* file opening :
   defines the strategy used for searching and opening file in the
   mbk environement. */
FILE *mbkfopen(name, extension, mode)
char *name, *extension, *mode;
{
FILE *in;
register int i = 0;

	if (!CATA_LIB || !WORK_LIB)
		mbkenv(); /* not done yet */

   if (name == NULL)
      return NULL;

	if (extension) /* if extension is null, no dot is required */
		(void)sprintf(filename, "%s/%s.%s", *name == '/' ? "" : WORK_LIB,
                                           name, extension);
	else
		(void)sprintf(filename, "%s/%s", *name == '/' ? "" : WORK_LIB, name);

	switch (*mode) {
       case 'w' :  case 'a':/* one shall save only in WORK_LIB */
			return fopen(filename, mode );
		case 'r' :
			if ((in = fopen(filename, mode )))
				return in;
			while (CATA_LIB[i]) {
				if (extension)
					(void)sprintf(filename, "%s/%s.%s", CATA_LIB[i++],
										name, extension);
				else
					(void)sprintf(filename, "%s/%s", CATA_LIB[i++], name);
				if ((in = fopen(filename, mode )))
					return in;
			}
			return NULL;
	}
		
	(void)fflush(stdout);
	(void)fprintf(stderr, "*** mbk error ***\n");
	(void)fprintf(stderr, "unknown file opening mode %s\n", mode);
	EXIT(1);
	return NULL; /* never reached */
}

/* unlink :
   ensures that only files in the working library may be erased. */
int mbkunlink(name, extension)
char *name, *extension;
{
	if (!CATA_LIB || !WORK_LIB)
		mbkenv(); /* not done yet */

   if (name == NULL) {
      errno = EFAULT;
      return -1;
   }

	if (extension) /* if extension is null, no dot is required */
		(void)sprintf(filename, "%s/%s.%s", WORK_LIB, name, extension);
	else
		(void)sprintf(filename, "%s/%s", WORK_LIB, name);

	return unlink(filename);
}
	
/* filepath :
   find the complete path of file from mbkfopen point of view. */
char *filepath(name, extension)
char *name, *extension;
{
FILE *in;
register int i = 0;

	if (!CATA_LIB || !WORK_LIB)
		mbkenv(); /* not done yet */

	if (extension) /* if extension is null, no dot is required */
		(void)sprintf(filename, "%s/%s.%s", WORK_LIB, name, extension);
	else
		(void)sprintf(filename, "%s/%s", WORK_LIB, name);

	if ((in = fopen(filename, READ_TEXT))) {
		(void)fclose(in);
		return filename;
	}
	while (CATA_LIB[i]) {
		if (extension)
			(void)sprintf(filename, "%s/%s.%s", CATA_LIB[i++],
								name, extension);
		else
			(void)sprintf(filename, "%s/%s", CATA_LIB[i++], name);
		if ((in = fopen(filename, READ_TEXT))) {
			(void)fclose(in);
			return filename;
		}
	}
	return NULL;
}

#if 0
/* process state :
   this gives some information on allocated memory, time spend, and so on. */
void mbkps()
{
#include <sys/time.h>
#include <sys/resource.h>
struct rusage rusage;
static int pagesize;
extern long mbkalloc_stat;
static int times;

	if (!times)
		pagesize = getpagesize() / 1024;
	(void)getrusage(RUSAGE_SELF, &rusage);
	(void)fprintf(stdout, "mbk stats          : %d call\n", ++times);
	(void)fprintf(stdout, "user     time      : %ld sec\n",
									rusage.ru_utime.tv_sec);
	(void)fprintf(stdout, "system   time      : %d sec\n",
									rusage.ru_stime.tv_usec);
	(void)fprintf(stdout, "real     size      : %ld k\n",
									rusage.ru_maxrss * pagesize);
	(void)fprintf(stdout, "mbkalloc peak size : %ld k\n",
									mbkalloc_stat / 1024);
	(void)fprintf(stdout, "mallinfo size      : %ld k\n",
									mallinfo().arena / 1024);
}
#endif
