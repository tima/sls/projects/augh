

void buildexternvelocon (lofig_list *f)
{
losig_list *s;
locon_list *c;
loins_list *i;
velosig    *vs;
velocon    *vc=NULL;
ptype_list *ps, *pc;
char *name, *lastname=NULL;
long idx;

   pc=f->USER=addptype(f->USER, VEL_CON, NULL);
   ps=f->USER=addptype(f->USER, VEL_SIG, NULL);

   for (c=f->LOCON; c; c=c->NEXT) {
      idx=vectorindex(c->NAME);
      if (lastname && (name=vectorradical(c->NAME))==lastname)
         vc->RIGHT=idx;
      else {
         if (vc) {
            vs=addchain(ps, getsigname(sig), vc->LEFT, vc->RIGHT, 'E');
            vs->NAMECHAIN=sig->NAMECHAIN;
         }
         vc=addvelocon(pc, name, idx, idx);
         if (idx==-1)
            lastname=NULL;
         else
            lastname=name;
         sig=c->SIG;
      }
   }
   if (vc) {
      vs=addchain(ps, getsigname(sig), vc->LEFT, vc->RIGHT, 'E');
      vs->NAMECHAIN=sig->NAMECHAIN;
   }
} 
