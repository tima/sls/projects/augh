
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "../../augh/auto/auto.h"
#include "../../augh/plugins/plugins.h"
#include "../../augh/auto/command.h"
#include "exec_python.h"

#define PLUGIN_NAME     "augh-python"
#define PLUGIN_VERSION  "0.1"



static int plugin_command(plugin_t* plugin, implem_t* Implem, command_t* cmd_data) {
	const char* cmd = Command_PopParam(cmd_data);
	if(cmd==NULL) {
		printf("Error [plugin '%s'] : No command received. Try 'help'.\n", plugin->name);
		return -1;
	}

	if(strcmp(cmd, "help")==0) {
		printf("This is plugin '%s' version %s.\n", plugin->name, plugin->version);
		printf(
			"Commands:\n"
			"  help            Display this help.\n"
			"  script <file>   Execute a Python script file.\n"
		);
		return 0;
	}

	if(strcmp(cmd, "script")==0) {
		const char* filename = Command_PopParam(cmd_data);
		if(filename==NULL) {
			printf("Error [plugin '%s'] : Missing parameter for command '%s'.\n", plugin->name, cmd);
			return 1;
		}
		return exec_python_file(Implem, filename);
	}

	else {
		printf("Error [plugin '%s'] : Unknown command '%s'.\n", plugin->name, cmd);
		return -1;
	}

	return 0;
}


int plugin_init(plugin_t* plugin) {
	plugin->api_version = namealloc(AUGH_PLUGIN_API_VERSION);
	plugin->name        = namealloc(PLUGIN_NAME);
	plugin->version     = namealloc(PLUGIN_VERSION);

	int z = Plugin_Declare(plugin);
	if(z!=0) return z;

	plugin->callbacks.interpreter = plugin_command;

	AughCmd_Decl_Interp(namealloc("py"), exec_python_file);

	return 0;
}

