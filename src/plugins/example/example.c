
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "../../augh/auto/auto.h"
#include "../../augh/plugins/plugins.h"
#include "../../augh/auto/command.h"

#define PLUGIN_NAME     "example"
#define PLUGIN_VERSION  "0.1"



static int plugin_command(plugin_t* plugin, implem_t* Implem, command_t* cmd_data) {
	const char* cmd = Command_PopParam(cmd_data);
	if(cmd==NULL) {
		printf("Error [plugin '%s'] : No command received. Try 'help'.\n", plugin->name);
		return -1;
	}

	if(strcmp(cmd, "help")==0) {
		printf("This is plugin '%s' version %s.\n", plugin->name, plugin->version);
		printf(
			"Commands:\n"
			"  help        Display this help.\n"
			"  hello       Say hello.\n"
		);
		return 0;
	}

	if(strcmp(cmd, "hello")==0) {
		printf("Hello World! From plugin '%s' version %s.\n", plugin->name, plugin->version);
	}
	else {
		printf("Error [plugin '%s'] : Unknown command '%s'.\n", plugin->name, cmd);
		return -1;
	}

	return 0;
}


int plugin_init(plugin_t* plugin) {
	plugin->api_version = namealloc(AUGH_PLUGIN_API_VERSION);
	plugin->name        = namealloc(PLUGIN_NAME);
	plugin->version     = namealloc(PLUGIN_VERSION);

	// Just for this example plugin, do something visible at load time
	printf("Hello world! This is plugin '%s' version %s, initializing.\n", plugin->name, plugin->version);

	if(plugin->params!=NULL) {
		printf("Parameters: \"%s\"\n", plugin->params);
	}
	else {
		printf("No parameters specified.\n");
	}

	int z = Plugin_Declare(plugin);
	if(z!=0) return z;

	plugin->callbacks.interpreter = plugin_command;

	return 0;
}

