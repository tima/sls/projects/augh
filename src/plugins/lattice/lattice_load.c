
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <stdbool.h>
#include <ctype.h>

#include "../../augh/indent.h"
#include "../../augh/auto/auto.h"
#include "../../augh/auto/techno.h"
#include "../../augh/netlist/netlist.h"
#include "../../augh/netlist/netlist_comps.h"
#include "../../augh/netlist/netlist_access.h"
#include "../../augh/plugins/plugins_techno.h"

#include "lattice_techno.h"
#include "lattice_yosys.h"



//======================================================================
// Extra data for FPGAs of boards
//======================================================================

lattice_board_data_t* Lattice_BoardData_New() {
	lattice_board_data_t* data = calloc(1, sizeof(*data));
	data->cfgbvs_en = false;
	return data;
}


//======================================================================
// Board descriptions
//======================================================================

// The iCEstick Evaluation Kit
// http://www.latticesemi.com/icestick
board_t* Lattice_BuildBoard_IceStick() {
	board_t* board = Board_New();

	board->name = namealloc("icestick");
	board->description = stralloc("iCEstick Evaluation Kit. The FPGA is iCE40HX-1k.");

	// Add a board_fpga_t structure
	board_fpga_t* board_fpga = Board_FPGA_New();
	board_fpga->name = namealloc("fpga");
	board_fpga->model = Techno_GetFPGA(namealloc("hx1k"));
	assert(board_fpga->model!=NULL);
	board_fpga->speed = Techno_FPGA_GetSpeed(board_fpga->model, namealloc("hx"));
	assert(board_fpga->speed!=NULL);
	board_fpga->package = Techno_FPGA_GetPkg(board_fpga->model, namealloc("tq144"));
	assert(board_fpga->package!=NULL);

	board_fpga->board = board;
	avl_pp_add_overwrite(&board->fpgas, board_fpga->name, board_fpga);

	// The JTAG id (assuming the first is 1)
	board_fpga->jtag_id = 1;

	// Add board connectivity

	netlist_comp_t* comp = board_fpga->connect = Netlist_Comp_Top_New(board_fpga->name);
	chain_list* pinsattr;

	// The 12 MHz clock

	netlist_port_t* port_clk12m = Netlist_Comp_AddPortAccess_Clock(comp, namealloc("icestick_clock12"));
	Netlist_Access_Clock_SetFrequency(port_clk12m->access, 12e6);
	// Set as default for the FPGA
	board_fpga->default_clock = port_clk12m->access;

	pinsattr = ListPin_MakeListAttr("LOC = 21", NULL);
	avl_pp_add_overwrite(&board_fpga->port2pin, port_clk12m, pinsattr);

	// The LEDs D1 to D5

	netlist_port_t* port_leds5 = Netlist_Comp_AddPortAccess_WireOut(comp, namealloc("icestick_leds5"), 5);

	// Here processed in this order: D1 to D5
	pinsattr = NULL;
	pinsattr = ListPin_AddListAttr(pinsattr, "LOC = 99", NULL);
	pinsattr = ListPin_AddListAttr(pinsattr, "LOC = 98", NULL);
	pinsattr = ListPin_AddListAttr(pinsattr, "LOC = 97", NULL);
	pinsattr = ListPin_AddListAttr(pinsattr, "LOC = 96", NULL);
	pinsattr = ListPin_AddListAttr(pinsattr, "LOC = 95", NULL);

	avl_pp_add_overwrite(&board_fpga->port2pin, port_leds5, reverse(pinsattr));

	// The IrDA RXD input

	netlist_port_t* port_irda_rxd = Netlist_Comp_AddPortAccess_WireIn(comp, namealloc("icestick_irda_rxd"), 1);
	pinsattr = ListPin_AddListAttr(NULL, "LOC = 106", NULL);
	avl_pp_add_overwrite(&board_fpga->port2pin, port_irda_rxd, pinsattr);

	// The IrDA RXD output

	netlist_port_t* port_irda_txd = Netlist_Comp_AddPortAccess_WireOut(comp, namealloc("icestick_irda_txd"), 1);
	pinsattr = ListPin_AddListAttr(NULL, "LOC = 105", NULL);
	avl_pp_add_overwrite(&board_fpga->port2pin, port_irda_txd, pinsattr);

	// The IrDA SD output

	netlist_port_t* port_irda_sd = Netlist_Comp_AddPortAccess_WireOut(comp, namealloc("icestick_irda_sd"), 1);
	pinsattr = ListPin_AddListAttr(NULL, "LOC = 107", NULL);
	avl_pp_add_overwrite(&board_fpga->port2pin, port_irda_sd, pinsattr);

	// FIXME It would be interesting to use UART over IR

	// Finally, return the board
	return board;
}



//======================================================================
// Reading techno data from JSON files
//======================================================================

// Note: JSON format: http://www.json.org/
// Note: jansson API: http://www.digip.org/jansson/

#include <jansson.h>

// Fix for systems that have jansson 2.4 instead of 2.5+
#ifndef json_array_foreach
#define json_array_foreach(array, index, value) \
	for(index = 0; index < json_array_size(array) && (value = json_array_get(array, index)); index++)
#endif

// Functions to write JSON files

static void fdump_chip(FILE* F, fpga_model_t* chip) {
	fprintf(F, "\"%s\": {\n", chip->name);

	if(chip->description!=NULL) {
		fprintf(F, "	\"desc\": \"%s\"\n", chip->description);
	}

	fprintf(F, "	\"speeds\": [");
	foreach(chip->timings, scan) {
		techno_timing_t* timing = scan->DATA;
		if(scan!=chip->timings) fprintf(F, ", ");
		fprintf(F, "\"%s\"", timing->name);
	}
	fprintf(F, "],\n");

	fprintf(F, "	\"packages\": [");
	foreach(chip->packages, scan) {
		fpga_pkg_model_t* pkg = scan->DATA;
		if(scan!=chip->packages) fprintf(F, ", ");
		fprintf(F, "\"%s\"", pkg->name);
	}
	fprintf(F, "],\n");

	lattice_res_t* xres = chip->resources;
	fprintf(F, "	\"resources\": {\n");
	fprintf(F, "		\"lut4\"   : %u,\n", xres->lut4);
	fprintf(F, "		\"ff\"     : %u,\n", xres->ff);
	fprintf(F, "		\"ram4k\"  : %u\n", xres->ram4k);
	fprintf(F, "	}\n");

	fprintf(F, "}\n");
}

void Lattice_Techno_WriteJSON(plugin_t* plugin) {

	// Read all chips declared by the current plugin
	// Dump their JSON representation in stdout

	avl_pp_foreach(&plugin->fpgas, scan) {
		fpga_model_t* chip = scan->data;
		fdump_chip(stdout, chip);
	}

	// FIXME for debug
	exit(EXIT_SUCCESS);
}

// Functions to read JSON files

static bool parse_json_verbose = false;

static void parse_chip(plugin_t* plugin, fpga_model_t* chip, json_t *json) {
	if(json_is_object(json)==false) {
		printf("Error: Wrong JSON type for chip '%s', techno '%s'\n", chip->name, chip->techno->name);
		exit(EXIT_FAILURE);
	}

	if(parse_json_verbose==true) {
		printf("Info: Parsing chip '%s', 'techno '%s'\n", chip->name, chip->techno->name);
	}

	unsigned index;
	const char *key;
	json_t *json_value;

	// Read the description
	json_t *json_desc = json_object_get(json, "desc");
	if(json_desc!=NULL) {
		if(json_is_string(json_desc)==false) {
			printf("Error: Wrong JSON type for description of chip '%s', techno '%s'\n", chip->name, chip->techno->name);
			exit(EXIT_FAILURE);
		}
		chip->description = stralloc(json_string_value(json_desc));
	}

	// Read the speed grades
	json_t *json_speeds = json_object_get(json, "speeds");
	if(json_speeds==NULL) {
		printf("Error: Missing speed grades for chip '%s', techno '%s'\n", chip->name, chip->techno->name);
		exit(EXIT_FAILURE);
	}
	if(json_is_array(json_speeds)==false) {
		printf("Error: Wrong JSON type for speeds of chip '%s', techno '%s'\n", chip->name, chip->techno->name);
		exit(EXIT_FAILURE);
	}
	json_array_foreach(json_speeds, index, json_value) {
		if(json_is_string(json_value)==false) {
			printf("Error: Wrong JSON type for speed name for chip '%s', techno '%s'\n", chip->name, chip->techno->name);
			exit(EXIT_FAILURE);
		}
		char* name = namealloc(json_string_value(json_value));
		techno_timing_t* timing = Techno_GetSpeed(chip->techno, name);
		if(timing==NULL) {
			printf("Error: Unknown speed grade '%s' for chip '%s', techno '%s'\n", name, chip->name, chip->techno->name);
			exit(EXIT_FAILURE);
		}
		chip->timings = addchain(chip->timings, timing);
	}

	// Read the packages
	json_t *json_pkgs = json_object_get(json, "packages");
	if(json_pkgs==NULL) {
		printf("Error: Missing packages for chip '%s', techno '%s'\n", chip->name, chip->techno->name);
		exit(EXIT_FAILURE);
	}
	if(json_is_array(json_pkgs)==false) {
		printf("Error: Wrong JSON type for packages of chip '%s', techno '%s'\n", chip->name, chip->techno->name);
		exit(EXIT_FAILURE);
	}
	json_array_foreach(json_pkgs, index, json_value) {
		if(json_is_string(json_value)==false) {
			printf("Error: Wrong JSON type for package for chip '%s', techno '%s'\n", chip->name, chip->techno->name);
			exit(EXIT_FAILURE);
		}
		char* name = namealloc(json_string_value(json_value));
		fpga_pkg_model_t* pkg = Techno_GetPackage(chip->techno, name);
		if(pkg==NULL) {
			printf("Error: Unknown package '%s' for chip '%s', techno '%s'\n", name, chip->name, chip->techno->name);
			exit(EXIT_FAILURE);
		}
		chip->packages = addchain(chip->packages, pkg);
	}

	// Read the resources
	json_t *json_resources = json_object_get(json, "resources");
	if(json_resources==NULL) {
		printf("Error: Missing resources for chip '%s', techno '%s'\n", chip->name, chip->techno->name);
		exit(EXIT_FAILURE);
	}
	if(json_is_object(json_resources)==false) {
		printf("Error: Wrong JSON type for resources of chip '%s', techno '%s'\n", chip->name, chip->techno->name);
		exit(EXIT_FAILURE);
	}
	lattice_res_t* xres = Lattice_Res_New();
	chip->resources = xres;
	json_object_foreach(json_resources, key, json_value) {
		// All values must be integers
		if(json_is_integer(json_value)==false) {
			printf("Error: Wrong JSON type for resource '%s' for chip '%s', techno '%s'\n", key, chip->name, chip->techno->name);
			exit(EXIT_FAILURE);
		}
		unsigned value_u = json_integer_value(json_value);
		if     (strcmp(key, "lut4")==0)   xres->lut4 = value_u;
		else if(strcmp(key, "ff")==0)     xres->ff = value_u;
		else if(strcmp(key, "ram4k")==0)  xres->ram4k = value_u;
		else {
			printf("Error: Unknown resource '%s' for chip '%s', techno '%s'\n", key, chip->name, chip->techno->name);
			exit(EXIT_FAILURE);
		}
	}
}

static void parse_timing(plugin_t* plugin, techno_timing_t* timing, json_t *json) {
	if(json_is_object(json)==false) {
		printf("Error: Wrong JSON type for timing '%s', techno '%s'\n", timing->name, timing->techno->name);
		exit(EXIT_FAILURE);
	}

	if(parse_json_verbose==true) {
		printf("Info: Parsing timing '%s', 'techno '%s'\n", timing->name, timing->techno->name);
	}

	lattice_timing_t* xtiming = calloc(1, sizeof(*xtiming));
	timing->data = xtiming;

	// Parse all elements of the speed grade
	const char *key;
	json_t *json_value;
	json_object_foreach(json, key, json_value) {
		if(strcmp(key, "desc")==0) {
			if(json_is_string(json_value)==false) {
				printf("Error: Wrong JSON type for description of timing '%s', techno '%s'\n", timing->name, timing->techno->name);
				exit(EXIT_FAILURE);
			}
			timing->description = stralloc(json_string_value(json_value));
			continue;
		}

		// Here the value can only be a number (a double, in ns)
		if(json_is_number(json_value)==false) {
			printf("Error: Wrong JSON type for timing elements '%s' of timing '%s', techno '%s'\n", key, timing->name, timing->techno->name);
			exit(EXIT_FAILURE);
		}
		double value_dbl = json_number_value(json_value);

		// Parse the different possible values

		if     (strcmp(key, "lut4_delay")==0)    xtiming->lut4_delay = value_dbl;
		else if(strcmp(key, "wire_delay")==0)    xtiming->wire_delay = value_dbl;

		else if(strcmp(key, "carry_ci2co")==0)   xtiming->carry_ci2co = value_dbl;
		else if(strcmp(key, "carry_data2co")==0) xtiming->carry_data2co = value_dbl;

		else if(strcmp(key, "ff_bef_wd")==0)     xtiming->ff_bef_wd = value_dbl;
		else if(strcmp(key, "ff_bef_we")==0)     xtiming->ff_bef_we = value_dbl;
		else if(strcmp(key, "ff_aft_rd")==0)     xtiming->ff_aft_rd = value_dbl;

		else if(strcmp(key, "ram4k_bef_wd")==0)  xtiming->ram4k_bef_wd = value_dbl;
		else if(strcmp(key, "ram4k_bef_wa")==0)  xtiming->ram4k_bef_wa = value_dbl;
		else if(strcmp(key, "ram4k_bef_we")==0)  xtiming->ram4k_bef_we = value_dbl;
		else if(strcmp(key, "ram4k_bef_ra")==0)  xtiming->ram4k_bef_ra = value_dbl;
		else if(strcmp(key, "ram4k_bef_re")==0)  xtiming->ram4k_bef_re = value_dbl;
		else if(strcmp(key, "ram4k_aft_rd")==0)  xtiming->ram4k_aft_read = value_dbl;

		else {
			printf("Error: Unknown JSON key '%s' in timing '%s', techno '%s'\n", key, timing->name, timing->techno->name);
			exit(EXIT_FAILURE);
		}

	}

}  // Parse timing

static void parse_techno(plugin_t* plugin, techno_t* techno, json_t *json) {
	if(json_is_object(json)==false) {
		printf("Error: Wrong JSON type for techno '%s'\n", techno->name);
		exit(EXIT_FAILURE);
	}

	if(parse_json_verbose==true) {
		printf("Info: Parsing techno '%s'\n", techno->name);
	}

	// Parse all elements of the techno
	json_t *json_value;

	// The description
	json_value = json_object_get(json, "desc");
	if(json_value!=NULL) {
		if(json_is_string(json_value)==false) {
			printf("Error: Wrong JSON type for description of techno '%s'\n", techno->name);
			exit(EXIT_FAILURE);
		}
		techno->description = stralloc(json_string_value(json_value));
	}

	// The speeds
	json_value = json_object_get(json, "speeds");
	if(json_value!=NULL) {
		if(json_is_object(json_value)==false) {
			printf("Error: Wrong JSON type for speeds of techno '%s'\n", techno->name);
			exit(EXIT_FAILURE);
		}
		const char *key;
		json_t *json_timing;
		json_object_foreach(json_value, key, json_timing) {
			techno_timing_t* timing = Techno_Timing_New();
			timing->name = namealloc(key);
			Lattice_Techno_LinkTiming(techno, timing);
			parse_timing(plugin, timing, json_timing);
		}
	}

	// The packages
	json_value = json_object_get(json, "packages");
	if(json_value!=NULL) {
		if(json_is_object(json_value)==false) {
			printf("Error: Wrong JSON type for packages of techno '%s'\n", techno->name);
			exit(EXIT_FAILURE);
		}
		const char *key;
		json_t *json_pkg;
		json_object_foreach(json_value, key, json_pkg) {
			fpga_pkg_model_t* pkg = FPGA_PkgModel_New();
			pkg->name = namealloc(key);

			if(parse_json_verbose==true) {
				printf("Info: Parsing package '%s' of techno '%s'\n", pkg->name, techno->name);
			}

			avl_pp_add_overwrite(&techno->packages, pkg->name, pkg);
			if(json_is_string(json_pkg)==false) {
				printf("Error: Wrong JSON type for package '%s' of techno '%s'\n", pkg->name, techno->name);
				exit(EXIT_FAILURE);
			}
			pkg->description = stralloc(json_string_value(json_pkg));
		}
	}

	// Set all callback functions
	Lattice_Techno_SetCB(techno);

	// Set appropriate backend control callbacks
	// Note: by default there is no callback set
	Yosys_Techno_SetCB(techno);

	// FIXME here we assume there is no fail
	Plugin_DeclTechno(plugin, techno);

	// The chip references
	json_value = json_object_get(json, "chips");
	if(json_value!=NULL) {
		if(json_is_object(json_value)==false) {
			printf("Error: Wrong JSON type for packages of techno '%s'\n", techno->name);
			exit(EXIT_FAILURE);
		}
		const char *key;
		json_t *json_chip;
		json_object_foreach(json_value, key, json_chip) {
			fpga_model_t* chip = FPGA_Model_New();
			chip->name = namealloc(key);
			chip->techno = techno;
			parse_chip(plugin, chip, json_chip);
			// FIXME here we assume there is no fail
			Plugin_DeclFPGA(plugin, chip);
		}
	}
}

// Main function
void Lattice_Techno_ReadJSON(plugin_t* plugin) {

	char* path = GetExePath_CutBin();
	char buffer[strlen(path)+40];
	sprintf(buffer, "%s/share/augh/lattice/technos.json", path);

	if(parse_json_verbose==true) {
		printf("Info: Reading JSON file: %s\n", buffer);
	}

	FILE* F = fopen(buffer, "rb");
	if(F==NULL) {
		fprintf(stderr, "Error: Couldn't open the file %s\n", buffer);
		exit(EXIT_FAILURE);
	}

	json_error_t json_error;
	json_t* json_root = json_loadf(F, 0, &json_error);

	fclose(F);

	if(json_root==NULL) {
		fprintf(stderr, "JSON Error: line %d, message: %s\n", json_error.line, json_error.text);
		exit(EXIT_FAILURE);
	}

	// Remove comments
	void remove_comments(json_t* json) {
		if(json_is_object(json)==true) {
			json_object_del(json, "#");
			const char *key;
			json_t *json_value;
			json_object_foreach(json, key, json_value) remove_comments(json_value);
		}
		else if(json_is_array(json)==true) {
			size_t index;
			json_t *json_value;
			json_array_foreach(json, index, json_value) remove_comments(json_value);
		}
	}
	remove_comments(json_root);

	// Read the JSON data tree

	const char *key;
	json_t *json_value;
	json_object_foreach(json_root, key, json_value) {
		techno_t* techno = Techno_New();
		techno->name = namealloc(key);
		parse_techno(plugin, techno, json_value);
	}

	// Clean
	json_decref(json_root);
}


