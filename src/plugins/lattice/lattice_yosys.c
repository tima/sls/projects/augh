
// Generation of Yosys / Arachne-pnr synthesis project files
// http://www.clifford.at/yosys/
// https://github.com/cseed/arachne-pnr

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

// To create and scan directories
#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>

#include "../../augh/auto/auto.h"
#include "../../augh/auto/command.h"
#include "../../augh/plugins/plugins.h"
#include "../../augh/netlist/netlist.h"
#include "../../augh/netlist/netlist_access.h"

#include "lattice_techno.h"
#include "lattice_yosys.h"



typedef struct yosys_proj_conf_t {
	char* dirname;
	char* topname;
	char* target_fpga;
	char* speed_grade;
	char* package;
	chain_list* list_dir_src;
	chain_list* list_file_src;
} yosys_proj_conf_t;

// Generated file names
static char* filename_blif = "project.blif";
static char* filename_asc  = "project.asc";
static char* filename_pcf  = "project.pcf";
static char* filename_bin  = "project.bin";
static char* filename_make = "Makefile";

// FIXME Missing Makefile rules for programming of the board

static int Yosys_Gen_Project_conf(implem_t* Implem, yosys_proj_conf_t* conf) {

	printf("Generating project files for Yosys / Arachne-pnr...\n");

	// Define the part name
	char partname[200];
	sprintf(partname, "%s%s-%s", conf->target_fpga, conf->package, conf->speed_grade);

	dbgprintf("Partname: %s\n", partname);

	bool pcf_file_incomplete = false;

	//lattice_techdata_t* techdata = Implem->synth_target->techno_data;

	char* path_comeback = ".";
	if(strcmp(conf->dirname, ".")!=0) {
		mkdir(conf->dirname,  S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
		path_comeback = "..";
	}

	char pathbuf[strlen(conf->dirname) + 30];

	// Note: The file will be created only if needed
	FILE *F = NULL;

	// The placement constraints (XDC file)
	dbgprintf("Generating PCF file...\n");

	{  // These brackets are only here to fold the code :-)
		sprintf(pathbuf, "%s/%s", conf->dirname, filename_pcf);
		F = fopen(pathbuf, "wb");
		if(F==NULL) {
			printf("ERROR: could not open the file '%s'.\n", pathbuf);
			return -1;
		}

		board_fpga_t* board_fpga = Implem->synth_target->board_fpga;
		//lattice_board_data_t* fpga_techdata = NULL;

		if(board_fpga!=NULL) {
			//fpga_techdata = board_fpga->techdata;

			fputc('\n', F);
			fprintf(F, "# Constraints for the board '%s', FPGA '%s'\n", board_fpga->board->name, board_fpga->name);
			if(board_fpga->board->description!=NULL) {
				fprintf(F, "#   Board description: %s\n", board_fpga->board->description);
			}
			if(board_fpga->description!=NULL) {
				fprintf(F, "#   Chip description: %s\n", board_fpga->description);
			}
			if(board_fpga->model!=NULL) {
				fprintf(F, "# Targeted FPGA: %s\n", board_fpga->model->name);
				if(board_fpga->model->description!=NULL) {
					fprintf(F, "#   Description: %s\n", board_fpga->model->description);
				}
			}
			fprintf(F, "# Targeted package: %s\n", conf->package);
			fputc('\n', F);
		}
		else {
			fputc('\n', F);
			fprintf(F, "# User-defined target: chip '%s' package '%s'\n", conf->target_fpga, conf->package);
			fputc('\n', F);
		}

		// Version for tcl to generate xdc file for vivado using
		void gen_rule_port_pcf_single(netlist_port_t* port, chain_list* port_pin_desc) {
			// Print descriptive comment
			fprintf(F, "# Port '%s'", port->name);
			if(port->access==NULL) {
				fprintf(F, " (not part of an access structure)\n");
			}
			else {
				fprintf(F, " of access '%s' model '%s'\n", port->access->name, port->access->model->name);
			}
			// Print attributes
			unsigned pin_idx = 0;
			foreach(port_pin_desc, scanPin) {
				chain_list* attributes = scanPin->DATA;
				foreach(attributes, scanAttr) {
					char* str_attr = scanAttr->DATA;
					// Only keep pin location
					if(strncasecmp(str_attr, "LOC", 3)!=0) continue;
					fprintf(F, "set_io ");
					if(port->width==1) fprintf(F, "%s", port->name);
					else fprintf(F, "%s[%u]", port->name, pin_idx);
					char* ptr = scanAttr->DATA;
					for( ; (*ptr)!=0; ptr++) if(*ptr=='=') break;
					if(*ptr==0) abort();
					fprintf(F, "%s", ptr+1);
					fprintf(F, "\n");
				}
				pin_idx ++;
			}
		}

		avl_pp_foreach(&Implem->netlist.top->ports, scan) {
			netlist_port_t* port = scan->data;
			chain_list* port_pin_desc = NULL;

			char* no_attr_reason = NULL;

			// Try user-defined port attributes
			avl_pp_find_data(&Implem->synth_target->port2pin, port, (void**)&port_pin_desc);
			if(port_pin_desc!=NULL) {

				if(Implem->synth_target->board_fpga!=NULL) {
					fprintf(F, "# Note: Pin attributes for this port are user-defined.\n");
				}

				// Write the rules for this port
				gen_rule_port_pcf_single(port, port_pin_desc);
				fputc('\n', F);

				continue;
			}

			// Try board-defined port attributes
			if(Implem->synth_target->board_fpga!=NULL) {

				// Get the standard identifier of the port, inside its access structure
				if(port->access==NULL) {
					no_attr_reason = "Not part of an access structure";
					goto NOATTR;
				}

				// Get the corresponding access from the board_fpga
				netlist_access_t* board_access = NULL;
				avl_pp_find_data(&Implem->synth_target->impacc2boardacc, port->access, (void**)&board_access);
				if(board_access==NULL) {
					no_attr_reason = "No corresponding board access structure";
					goto NOATTR;
				}

				// Get the standard name of the port
				char* stdname = Netlist_Access_Port_GetName(port);
				if(stdname==NULL) {
					no_attr_reason = "Can't identify in access structure";
					goto NOATTR;
				}

				// Get the corresponding board port
				netlist_port_t* board_port = Netlist_Access_Name2Port(board_access, stdname);
				if(board_port==NULL) {
					no_attr_reason = "Can't get corresponding board port";
					goto NOATTR;
				}

				// Finally, get the pin attributes
				port_pin_desc = NULL;
				avl_pp_find_data(&Implem->synth_target->board_fpga->port2pin, board_port, (void**)&port_pin_desc);
				if(port_pin_desc==NULL) {
					no_attr_reason = "The board has no attributes";
					goto NOATTR;
				}

				// Write the rules for this port
				gen_rule_port_pcf_single(port, port_pin_desc);
				fputc('\n', F);

				continue;
			}

			// Here there is no attributes

			// Utility function
			void printfnoattr(FILE* locF, char* pref) {
				if(pref!=NULL) fprintf(locF, "%s", pref);
				fprintf(locF, "WARNING: No pin attributes for port '%s'.", port->name);
				if(no_attr_reason!=NULL) fprintf(locF, " Reason: %s.", no_attr_reason);
				fputc('\n', locF);
			}

			NOATTR:

			printfnoattr(stdout, NULL);
			printfnoattr(F, "# ");
			fputc('\n', F);

			pcf_file_incomplete = true;

		}  // Scan all ports

		// FIXME Missing declaration of clock config
		//   arachne-pnr does not handle that yet

		if(pcf_file_incomplete==true) {
			printf("WARNING: Some ports had no pin attributes. The PCF file may be incomplete.\n");
		}

		fclose(F);
	}  // These brackets are only here to fold the code :-)

	// FIXME Currently no .ys script file is used for Yosis => it may be useful, as a template

	// The general Makefile to launch vhd2vl, yosys, arachne-pnr
	dbgprintf("Generating Makefile...\n");

	{  // These brackets are only here to fold the code :-)
		sprintf(pathbuf, "%s/%s", conf->dirname, filename_make);
		F = fopen(pathbuf, "wb");
		if(F==NULL) {
			printf("ERROR: could not open the file %s.\n", pathbuf);
			return -1;
		}

		fprintf(F,
			"\n"
			"#================================================\n"
			"# Synthesis with Yosys / Arachne-pnr\n"
			"#================================================\n"
			"\n"
			"ALLVHDL ="
		);

		// Add the VHDL directories
		foreach(conf->list_dir_src, scanSrc) {
			char* dirname_vhd = scanSrc->DATA;
			fprintf(F, " $(wildcard %s/%s/*.vhd)", path_comeback, dirname_vhd);
		}

		// Add the VHDL files given directly
		foreach(conf->list_file_src, scanSrc) {
			char* file_name = scanSrc->DATA;
			fprintf(F, " %s/%s", path_comeback, file_name);
		}
		fprintf(F, "\n");

		fprintfm(F,
			"ALLVL   = $(patsubst %.vhd,%.v,$(ALLVHDL))\n"
			"\n"
			".PHONY: all vhd2vl synth pnr pack clean\n"
			"\n"
			"all    : %s\n", filename_bin,
			"\n"
			"vhd2vl : $(ALLVL)\n"
			"synth  : %s\n", filename_blif,
			"pnr    : %s\n", filename_asc,
			"pack   : %s\n", filename_bin,
			"\n"
			"%%.v: %%.vhd\n"
			"	vhd2vl $< $@\n"
			"\n"
			"%s: $(ALLVL)\n", filename_blif,
			"	yosys -q -p \"synth_ice40 -blif $@\" $(ALLVL)\n"
			"\n"
			"%s: %s %s\n", filename_asc, filename_blif, filename_pcf,
			"	arachne-pnr -p %s $< -o $@\n", filename_pcf,
			"\n"
			"%s: %s\n", filename_bin, filename_asc,
			"	icepack $< $@\n"
			"\n"
			"clean:\n"
			"	rm -f *.blif *.asc *.bin\n"
			"\n"
			"\n"
			"#================================================\n"
			"# Program the FPGA with iceprog\n"
			"#================================================\n"
			"\n"
			".PHONY: upload\n"
			"\n"
			"upload: %s\n", filename_bin,
			"	iceprog $<\n"
			"\n",
			NULL
		);

		fprintf(F, "\n");
		fclose(F);
	}  // These brackets are only here to fold the code :-)

	printf("End of Yosys / Arachne-pnr project generation\n");

	return 0;
}

int Yosys_Gen_Project(implem_t* Implem, char* dirname) {
	lattice_techdata_t* techdata = Implem->synth_target->techno_data;

	// Config structure
	yosys_proj_conf_t conf_i;
	yosys_proj_conf_t* conf = &conf_i;
	memset(&conf_i, 0, sizeof(conf_i));

	conf->topname = Implem->netlist.top->name;

	conf->dirname = dirname;
	if(conf->dirname==NULL) conf->dirname = techdata->synth_prj_dir;
	if(conf->dirname==NULL) conf->dirname = YOSYS_PRJ_DEFAULT;

	// Save the project directory in the techdata, in case it isn't
	techdata->synth_prj_dir = conf->dirname;

	unsigned errors_nb = 0;

	if(Implem->synth_target->model!=NULL) conf->target_fpga = Implem->synth_target->model->name;
	else conf->target_fpga = techdata->chip_name;

	if(Implem->synth_target->timing!=NULL) conf->speed_grade = Implem->synth_target->timing->name;
	else conf->speed_grade = techdata->chip_speed;

	if(Implem->synth_target->package!=NULL) conf->package = Implem->synth_target->package->name;
	else conf->package = techdata->chip_package;

	if(conf->target_fpga==NULL) {
		printf("Error [techno '%s']: No FPGA chip target defined.\n", Implem->synth_target->techno->name);
		errors_nb ++;
	}
	if(conf->speed_grade==NULL) {
		printf("Error [techno '%s']: No FPGA speed grade defined.\n", Implem->synth_target->techno->name);
		errors_nb ++;
	}
	if(conf->package==NULL) {
		printf("Error [techno '%s']: No FPGA package defined.\n", Implem->synth_target->techno->name);
		errors_nb ++;
	}

	if(errors_nb!=0) return -1;

	conf->list_dir_src = addchain(NULL, Implem->env.vhdl_dir);

	// Actual generation
	int z = Yosys_Gen_Project_conf(Implem, conf);

	// Clean
	freechain(conf->list_dir_src);

	return z;
}


// Function to use as callback in techno_t structure
static int Yosys_Gen_Project_callback(implem_t* Implem) {
	return Yosys_Gen_Project(Implem, NULL);
}

// Launch back-end logic synthesis, place and route
// Return 0 if OK
int Yosys_Launch(implem_t* Implem) {
	lattice_techdata_t* techdata = Implem->synth_target->techno_data;

	if(techdata->synth_prj_dir==NULL) {
		printf("Error: The back-end project files are not generated. Can't launch back-end.\n");
		return -1;
	}

	// Build the command-line: make -C <dirname>
	char buf[20 + strlen(techdata->synth_prj_dir)];
	sprintf(buf, "make -C %s", techdata->synth_prj_dir);

	fflush(NULL);

	return system(buf);
}


void Yosys_Techno_SetCB(techno_t* techno) {
	techno->synth_gen_prj = Yosys_Gen_Project_callback;
	techno->synth_launch  = Yosys_Launch;
}


