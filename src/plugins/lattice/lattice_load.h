
#ifndef _LATTICE_LOAD_H_
#define _LATTICE_LOAD_H_


board_t* Lattice_BuildBoard_IceStick();

void Lattice_Techno_WriteJSON(plugin_t* plugin);
void Lattice_Techno_ReadJSON(plugin_t* plugin);


#endif  // _LATTICE_LOAD_H_

