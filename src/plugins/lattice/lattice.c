
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

#include "../../augh/auto/auto.h"
#include "../../augh/plugins/plugins.h"
#include "../../augh/netlist/netlist.h"
#include "../../augh/auto/command.h"

#include "lattice_techno.h"
#include "lattice_yosys.h"

#define PLUGIN_NAME     "lattice"
#define PLUGIN_VERSION  "0.1"



static int plugin_command(plugin_t* plugin, implem_t* Implem, command_t* cmd_data) {
	const char* cmd = Command_PopParam(cmd_data);
	if(cmd==NULL) {
		printf("Error [plugin '%s'] : No command received. Try 'help'.\n", plugin->name);
		return -1;
	}

	// Many commands use this structure, so get it only once
	lattice_techdata_t* techdata = Implem->synth_target->techno_data;

	if(strcmp(cmd, "help")==0) {
		printf("This is plugin '%s' version %s.\n", plugin->name, plugin->version);
		printfm(
			"Commands:\n"
			"  help             Display this help.\n"
			"  set-chip-package <value>\n"
			"                   Set the chip package. Example: ffg1157\n"
			"  set-chip-speed <value>\n"
			"                   Set the chip speed grade. Example: 1\n"
			"  set-chip-name <name>\n"
			"                   Set the targeted chip name. Example: xc7v585t\n"
			"  set-prj-dir <dir>\n"
			"                   Set the name of the directory where to generate back-end synthesis project files.\n"
			"  gen-yosys-project [<dir>]\n"
			"                   Generate Yosys project files in a local directory.\n"
			"                   Default directory: %s.\n", YOSYS_PRJ_DEFAULT,
			NULL
		);
		return 0;
	}

	if(strcmp(cmd, "set-chip-package")==0) {
		char* param = Command_PopParam_stralloc(cmd_data);
		if(param==NULL) {
			printf("Error [plugin '%s'] : Missing parameter for command '%s'.\n", plugin->name, cmd);
			return -1;
		}
		techdata->chip_package = param;
	}
	else if(strcmp(cmd, "set-chip-speed")==0) {
		char* param = Command_PopParam_stralloc(cmd_data);
		if(param==NULL) {
			printf("Error [plugin '%s'] : Missing parameter for command '%s'.\n", plugin->name, cmd);
			return -1;
		}
		techdata->chip_speed = param;
	}
	else if(strcmp(cmd, "set-chip-name")==0) {
		char* param = Command_PopParam_stralloc(cmd_data);
		if(param==NULL) {
			printf("Error [plugin '%s'] : Missing parameter for command '%s'.\n", plugin->name, cmd);
			return -1;
		}
		techdata->chip_name = param;
	}
	else if(strcmp(cmd, "set-prj-dir")==0) {
		char* param = Command_PopParam_stralloc(cmd_data);
		if(param==NULL) {
			printf("Error [plugin '%s'] : Missing parameter for command '%s'.\n", plugin->name, cmd);
			return -1;
		}
		techdata->synth_prj_dir = param;
	}

	else if(strcmp(cmd, "gen-yosys-project")==0) {
		char* dirname = Command_PopParam_stralloc(cmd_data);
		return Yosys_Gen_Project(Implem, dirname);
	}

	else {
		printf("Error [plugin '%s'] : Unknown command '%s'.\n", plugin->name, cmd);
		return -1;
	}

	return 0;
}


int plugin_init(plugin_t* plugin) {
	plugin->api_version = namealloc(AUGH_PLUGIN_API_VERSION);
	plugin->name        = namealloc(PLUGIN_NAME);
	plugin->version     = namealloc(PLUGIN_VERSION);

	int z = Plugin_Declare(plugin);
	if(z!=0) return z;

	plugin->callbacks.interpreter = plugin_command;

	Lattice_Techno_Init(plugin);

	return 0;
}

