
#ifndef _LATTICE_TECHNO_H_
#define _LATTICE_TECHNO_H_

#include "../../augh/plugins/plugins.h"


//======================================================================
// Structures
//======================================================================

// Note: time unit is ns
typedef struct lattice_timing_t {

	double lut4_delay;

	double carry_ci2co;
	double carry_data2co;

	double ff_bef_wd;  // before clock front, write data
	double ff_bef_we;  // before clock front, write enable
	double ff_aft_rd;  // after clock front, read data

	double ram4k_bef_wd;    // before clock front, write data
	double ram4k_bef_wa;    // before clock front, write addres
	double ram4k_bef_we;    // before clock front, write enable
	double ram4k_bef_ra;    // before clock front, read addres
	double ram4k_bef_re;    // before clock front, read enable
	double ram4k_aft_read;  // after clock front, read data

	double wire_delay;  // Average, arbitrary wire delay

} lattice_timing_t;

typedef struct lattice_res_t {
	unsigned lut4;
	unsigned ff;
	unsigned ram4k;
} lattice_res_t;

typedef struct lattice_techdata_t {
	// Control of back-end tools
	bool keep_hier;
	bool use_dsp;
	bool use_bram;
	// Generation of back-end project files
	char* synth_prj_dir;
	char* chip_package;
	char* chip_speed;
	char* chip_name;
	// Generation of clock declaration in project files
	// No clock: <0, auto: 0, force gen: >0
	int force_gen_clock;
} lattice_techdata_t;

// Extra data for FPGAs of boards
typedef struct lattice_board_data_t {
	// The parameter CFGBVS, if applicable
	bool   cfgbvs_en;
	char*  param_cfgbvs;
	double param_config_voltage;
} lattice_board_data_t;


//======================================================================
// Shared variables
//======================================================================

extern char* namealloc_ff;
extern char* namealloc_lut4;
extern char* namealloc_bram4k;


//======================================================================
// Functions
//======================================================================

lattice_res_t* Lattice_Res_New();
void Lattice_Res_Free(lattice_res_t* res);

void Lattice_Techno_Init(plugin_t* plugin);

// Utility functions to help build techno structures
void Lattice_Techno_SetCB(techno_t* techno);
void Lattice_Techno_LinkTiming(techno_t* techno, techno_timing_t* timing);

// Utility functions to describe FPGA board
chain_list* SplitPinAttr(char* str);
chain_list* MakeListAttr(char* attr, ...);
chain_list* ListPin_AddListAttr(chain_list* listpin, char* attr, ...);
chain_list* ListPin_MakeListAttr(char* attr, ...);


#endif  // _LATTICE_TECHNO_H_

