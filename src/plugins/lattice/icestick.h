
#ifndef _ICESTICK_H_
#define _ICESTICK_H_


// This declaration is not meant to be used by the user.
// This is to prevent the user to re-declare them without knowing their use.
bool icestick_clock12;

// Miscellaneous I/O
uint5_t icestick_leds5;

#define ICESTICK_LED1 0x01
#define ICESTICK_LED2 0x02
#define ICESTICK_LED3 0x04
#define ICESTICK_LED4 0x08
#define ICESTICK_LED5 0x10

// IrDA (infrared)
bool    icestick_irda_rxd;
bool    icestick_irda_txd;
bool    icestick_irda_sd;


#endif  // _ICESTICK_H_

