
#ifndef _LATTICE_YOSYS_H_
#define _LATTICE_YOSYS_H_


#define YOSYS_PRJ_DEFAULT "yosysprj"

int Yosys_Gen_Project(implem_t* Implem, char* dirname);
int Yosys_Launch(implem_t* Implem);

void Yosys_Techno_SetCB(techno_t* techno);


#endif  // _LATTICE_YOSYS_H_

