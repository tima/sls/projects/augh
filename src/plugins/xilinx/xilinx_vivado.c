
// Generation of Vivado project

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

// To create and scan directories
#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>

#include "../../augh/auto/auto.h"
#include "../../augh/auto/command.h"
#include "../../augh/plugins/plugins.h"
#include "../../augh/netlist/netlist.h"
#include "../../augh/netlist/netlist_access.h"

#include "xilinx_techno.h"
#include "xilinx_vivado.h"



typedef struct vivado_proj_conf_t {
	char* dirname;
	char* topname;
	char* target_fpga;
	char* speed_grade;
	char* package;
	chain_list* list_dir_src;
	chain_list* list_file_src;
} vivado_proj_conf_t;

// Generated file names
static char* filename_xdc  = "project.xdc";
static char* filename_bit  = "project.bit";
static char* filename_tcl  = "project.tcl";
static char* filename_tcl_prog = "program.tcl";
static char* filename_make = "Makefile";

// FIXME Missing template Makefile rules for postsynth and postpar simulations

static int Vivado_Gen_Project_conf(implem_t* Implem, vivado_proj_conf_t* conf) {

	printf("Generating project files for Xilinx Vivado...\n");

	// Define the part name
	char partname[200];
	sprintf(partname, "%s%s-%s", conf->target_fpga, conf->package, conf->speed_grade);

	dbgprintf("Partname: %s\n", partname);

	bool xdc_file_incomplete = false;

	xilinx_techdata_t* techdata = Implem->synth_target->techno_data;
	board_fpga_t* board_fpga = Implem->synth_target->board_fpga;

	char* path_comeback = ".";
	if(strcmp(conf->dirname, ".")!=0) {
		mkdir(conf->dirname,  S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
		path_comeback = "..";
	}

	char pathbuf[strlen(conf->dirname) + 30];

	// Note: The file will be created only if needed
	FILE *F = NULL;

	// The placement constraints (XDC file)
	dbgprintf("Generating XDC file...\n");

	{  // These brackets are only here to fold the code :-)
		sprintf(pathbuf, "%s/%s", conf->dirname, filename_xdc);
		F = fopen(pathbuf, "wb");
		if(F==NULL) {
			printf("ERROR: could not open the file '%s'.\n", pathbuf);
			return -1;
		}

		xilinx_board_data_t* fpga_techdata = NULL;

		if(board_fpga!=NULL) {
			fpga_techdata = board_fpga->techdata;

			fputc('\n', F);
			fprintf(F, "# Constraints for the board '%s', FPGA '%s'\n", board_fpga->board->name, board_fpga->name);
			if(board_fpga->board->description!=NULL) {
				fprintf(F, "#   Board description: %s\n", board_fpga->board->description);
			}
			if(board_fpga->description!=NULL) {
				fprintf(F, "#   Chip description: %s\n", board_fpga->description);
			}
			if(board_fpga->model!=NULL) {
				fprintf(F, "# Targeted FPGA: %s\n", board_fpga->model->name);
				if(board_fpga->model->description!=NULL) {
					fprintf(F, "#   Description: %s\n", board_fpga->model->description);
				}
			}
			fprintf(F, "# Targeted package: %s\n", conf->package);
			fputc('\n', F);
		}
		else {
			fputc('\n', F);
			fprintf(F, "# User-defined target: chip '%s' package '%s'\n", conf->target_fpga, conf->package);
			fputc('\n', F);
		}

		// Version for tcl to generate xdc file for vivado using
		void gen_rule_port_tcl_single(netlist_port_t* port, chain_list* port_pin_desc) {
			// Print descriptive comment
			fprintf(F, "# Port '%s'", port->name);
			if(port->access==NULL) {
				fprintf(F, " (not part of an access structure)\n");
			}
			else {
				fprintf(F, " of access '%s' model '%s'\n", port->access->name, port->access->model->name);
			}
			// Print attributes
			unsigned pin_idx = 0;
			foreach(port_pin_desc, scanPin) {
				chain_list* attributes = scanPin->DATA;
				foreach(attributes, scanAttr) {
					char* strattr = scanAttr->DATA;
					// FIXME The attribute "timing ignore" is not handled bu=y the Vivado project generator
					if(strcasecmp(strattr, "TIG")==0) continue;
					// If there is a symbol =, get what's after
					char* afterequal = strchr(strattr, '=');
					if(afterequal!=NULL) afterequal++;
					// Write the constraint
					fprintf(F, "set_property ");
					if(strncasecmp(strattr, "LOC", 3)==0) {
						fprintf(F, "PACKAGE_PIN %s", afterequal);
					}
					else {
						fprintf(F, "%s", str_replace_one_buf(scanAttr->DATA, '=', ' '));
					}
					if(port->width==1) fprintf(F, " [get_ports %s]", port->name);
					else fprintf(F, " [get_ports {%s[%u]}]", port->name, pin_idx);
					fprintf(F, ";\n");
				}
				pin_idx ++;
			}
		}

		avl_pp_foreach(&Implem->netlist.top->ports, scan) {
			netlist_port_t* port = scan->data;
			chain_list* port_pin_desc = NULL;

			char* no_attr_reason = NULL;

			// Try user-defined port attributes
			avl_pp_find_data(&Implem->synth_target->port2pin, port, (void**)&port_pin_desc);
			if(port_pin_desc!=NULL) {

				if(Implem->synth_target->board_fpga!=NULL) {
					fprintf(F, "# Note: Pin attributes for this port are user-defined.\n");
				}

				// Write the rules for this port
				gen_rule_port_tcl_single(port, port_pin_desc);
				fputc('\n', F);

				continue;
			}

			// Try board-defined port attributes
			if(Implem->synth_target->board_fpga!=NULL) {

				// Get the standard identifier of the port, inside its access structure
				if(port->access==NULL) {
					no_attr_reason = "Not part of an access structure";
					goto NOATTR;
				}

				// Get the corresponding access from the board_fpga
				netlist_access_t* board_access = NULL;
				avl_pp_find_data(&Implem->synth_target->impacc2boardacc, port->access, (void**)&board_access);
				if(board_access==NULL) {
					no_attr_reason = "No corresponding board access structure";
					goto NOATTR;
				}

				// Get the standard name of the port
				char* stdname = Netlist_Access_Port_GetName(port);
				if(stdname==NULL) {
					no_attr_reason = "Can't identify in access structure";
					goto NOATTR;
				}

				// Get the corresponding board port
				netlist_port_t* board_port = Netlist_Access_Name2Port(board_access, stdname);
				if(board_port==NULL) {
					no_attr_reason = "Can't get corresponding board port";
					goto NOATTR;
				}

				// Finally, get the pin attributes
				port_pin_desc = NULL;
				avl_pp_find_data(&Implem->synth_target->board_fpga->port2pin, board_port, (void**)&port_pin_desc);
				if(port_pin_desc==NULL) {
					no_attr_reason = "The board has no attributes";
					goto NOATTR;
				}

				// Write the rules for this port
				gen_rule_port_tcl_single(port, port_pin_desc);
				fputc('\n', F);

				continue;
			}

			// Here there is no attributes

			// Utility function
			void printfnoattr(FILE* locF, char* pref) {
				if(pref!=NULL) fprintf(locF, "%s", pref);
				fprintf(locF, "WARNING: No pin attributes for port '%s'.", port->name);
				if(no_attr_reason!=NULL) fprintf(locF, " Reason: %s.", no_attr_reason);
				fputc('\n', locF);
			}

			NOATTR:

			printfnoattr(stdout, NULL);
			printfnoattr(F, "# ");
			fputc('\n', F);

			xdc_file_incomplete = true;

		}  // Scan all ports

		// Declare the clock
		if(Implem->netlist.port_clock!=NULL) {
			bool clkgen_comment = false;

			// Mode: Force no generation of clock config
			if(techdata->force_gen_clock < 0) {
				char* str = "Warning: No clock is declared because of user override.\n";
				fprintf(F, "# %s", str);
				printf(str);
				// The declaration will be commented
				clkgen_comment = true;
			}

			// Mode: auto
			if(techdata->force_gen_clock == 0) {
				if(Implem->time.max_cycles_per_state > 1) {
					char* str = "Warning: No clock is declared because of multi-cycle states.\n";
					fprintf(F, "# %s", str);
					printf(str);
					// The declaration will be commented
					clkgen_comment = true;
				}
				if(Implem->synth_target->clk_period_ns==0) {
					char* str = "Warning: No clock is declared because no frequency/period is set.\n";
					fprintf(F, "# %s", str);
					printf(str);
					// The declaration will be commented
					clkgen_comment = true;
				}
			}

			// Mode: Force generation of clock config
			if(techdata->force_gen_clock > 0) {
				if(Implem->time.max_cycles_per_state > 1) {
					char* str = "Warning: There are multi-cycle states but a clock is declared because of user override.\n";
					fprintf(F, "# %s", str);
					printf(str);
				}
			}

			// Default values
			double clkgen_freq = 100;  // in MHz
			double clkgen_per = 10;  // in ns

			if(Implem->synth_target->clk_period_ns==0) {
				if(clkgen_comment==false) {
					char buf[1024];
					sprintf(buf, "Warning: No clock period is defined. Setting %g MHz by default.\n", clkgen_freq);
					fprintf(F, "# %s", buf);
					printf(buf);
				}
			}
			else {
				clkgen_freq = Implem->synth_target->clk_freq/1e6;
				clkgen_per = Implem->synth_target->clk_period_ns;
			}

			// Write the clock declaration into the file
			if(clkgen_comment==true) {
				fprintf(F, "# Note: Uncomment to activate the clock declaration\n");
			}
			fprintf(F, "# Declaration of the main clock: frequency = %g MHz, period = %g ns\n", clkgen_freq, clkgen_per);
			if(clkgen_comment==true) fprintf(F, "#");
			fprintf(F, "create_clock -name %s -period %g [get_ports %s]\n\n",
				Implem->netlist.port_clock->name, clkgen_per, Implem->netlist.port_clock->name
			);

		}  // End generation of clock config

		// Extra parameters
		if(fpga_techdata!=NULL) {
			if(fpga_techdata->cfgbvs_en==true) {
				fprintfm(F,
					"# Config voltage during programming of the FPGA\n"
					"# For more information, see the configuration user guide for the FPGA\n"
					"set_property CFGBVS %s [current_design]\n", fpga_techdata->param_cfgbvs,
					"set_property CONFIG_VOLTAGE %g [current_design]\n", fpga_techdata->param_config_voltage,
					"\n",
					NULL
				);
			}
		}

		if(xdc_file_incomplete==true) {
			printf("WARNING: Some ports had no pin attributes. The xdc rules may be incomplete.\n");
		}

		fclose(F);
	}

	// The TCL file: all vivado commands
	dbgprintf("Generating TCL file...\n");

	{  // These brackets are only here to fold the code :-)
		sprintf(pathbuf, "%s/%s", conf->dirname, filename_tcl);
		F = fopen(pathbuf, "wb");
		if(F==NULL) {
			printf("ERROR: could not open the file %s.\n", pathbuf);
			return -1;
		}

		fprintfm(F,
			"# This TCL script was generated by AUGH\n"
			"\n"
			"# Note: This is a non-project script\n"
			"# For more information, please refer to the Xilinx document UG894\n"
			"# \"Vivado Design Suite User Guide: Using TCL Scripting\", version 2014.1, page 11\n"
			"# For details about the TCL commands, please refer to the Xilinx document UG835\n"
			"# \"Vivado Design Suite Tcl Command Reference Guide\"\n"
			"\n"
			"# Define the output directory area. Mostly for debug purposes.\n"
			"set outputDir \"./\"\n"
			"file mkdir $outputDir\n"
			"\n"
			"set part %s\n", partname,
			"set top %s\n", conf->topname,
			"\n"
			"# Set target part to fake current project, to read IPs, if any\n"
			"# More details: http://www.xilinx.com/support/answers/54317.html\n"
			"set_part $part\n"
			"set_property part $part [current_project]\n"
			"\n"
			"\n"
			"\n"
			"# Setup design sources\n",
			NULL
		);

		// This part generates a list of vhdl files to read

		foreach(conf->list_dir_src, scanSrc) {
			char* dirname_vhd = scanSrc->DATA;
			DIR *dp = opendir(dirname_vhd);
			if(dp==NULL) {
				printf("WARNING: Can't scan the folder '%s' for VHDL files. Skipping.\n", dirname_vhd);
				continue;
			}
			do {
				struct dirent *ep = readdir(dp);
				if(ep==NULL) break;
				// Get the file extension
				char* beg_ext = NULL;
				char* scan_name = ep->d_name;
				while(*scan_name!=0) {
					if(*scan_name=='.') beg_ext = scan_name+1;
					scan_name++;
				}
				// Check the file extension
				if(beg_ext==NULL) continue;
				if(strcasecmp(beg_ext, "vhd")!=0) continue;
				fprintf(F, "read_vhdl \"%s/%s/%s\"\n", path_comeback, dirname_vhd, ep->d_name);
			}while(1);

			closedir(dp);
		}  // Scan the list of directories

		// Add the VHDL files given directly
		foreach(conf->list_file_src, scanSrc) {
			char* file_name = scanSrc->DATA;
			fprintf(F, "read_vhdl \"%s/%s\"\n", path_comeback, file_name);
		}

		fprintfm(F,
			"\n"
			"# Setup constraints\n"
			"read_xdc ./project.xdc\n"
			"\n"
			"# Run logic synthesis\n"
			"# Short note about options for logic synthesis:\n"
			"#  -flatten_hierarchy <arg>      Argument can be full, none, rebuilt (default)\n"
			"#  -max_dsp <n>, -max_bram <n>   Take absolute number of primitives as argument\n"
			"#                                -1 means full (default), 0 means none\n"
			"#  -directive AreaOptimizedHigh  More area optimization\n"
			"#  -keep_equivalent_registers    This is clear\n"
			"#  -generic name=value           Optionally override generics (multiple commands can be used)\n"
			"synth_design -top $top -part $part"
			" -flatten_hierarchy %s", techdata->keep_hier==true ? "none" : "full",
			" -resource_sharing on"
			" -max_dsp %i", techdata->use_dsp==true ? -1 : 0,
			//" -max_bram %i", techdata->use_bram==true ? -1 : 0,
			"\n" // End of previous line
			"\n"
			"# Run logic optimization\n"
			"opt_design\n"
			"\n"
			"# Report timing and utilization estimates\n"
			"report_timing_summary -file $outputDir/post_synth_timing_summary.rpt\n"
			"report_utilization -file $outputDir/post_synth_util.rpt\n"
			"\n"
			"# Write design checkpoint\n"
			"write_checkpoint -force $outputDir/post_synth.dcp\n"
			"\n"
			"\n"
			"\n"
			"# Note: For debug purposes, if you want to start from post- logic synthesis state,\n"
			"# uncomment the following 2 lines and remove all above commands\n"
			"#read_checkpoint $outputDir/post_synth.dcp\n"
			"#link_design -top $top -part $part\n"
			"\n"
			"# Run placement and physical logic optimization\n"
			"place_design\n"
			"#place_design -directive WLDrivenBlockPlacement\n"
			"#place_design -directive ExtraNetDelay_high\n"
			"\n"
			"# Optionally run physical logic optimization if there are timing violations\n"
			"if {[get_property SLACK [get_timing_paths -max_paths 1 -nworst 1 -setup]] < 0} {\n"
			"	puts \"Found setup timing violations => running physical optimization\"\n"
			"	phys_opt_design\n"
			"	#phys_opt_design -directive AggressiveExplore\n"
			"}\n"
			"\n"
			"# Report utilization and timing estimates\n"
			"report_clock_utilization -file $outputDir/clock_util.rpt\n"
			"report_utilization -file $outputDir/post_place_util.rpt\n"
			"report_timing_summary -file $outputDir/post_place_timing_summary.rpt\n"
			"\n"
			"# Write design checkpoint\n"
			"write_checkpoint -force $outputDir/post_place.dcp\n"
			"\n"
			"\n"
			"\n"
			"# Note: For debug purposes, if you want to start from post- placement state,\n"
			"# uncomment the following 2 lines and remove all above commands\n"
			"#read_checkpoint $outputDir/post_place.dcp\n"
			"#link_design -top $top -part $part\n"
			"\n"
			"# Run routing\n"
			"route_design\n"
			"\n"
			"# Write the post-route design checkpoint\n"
			"write_checkpoint -force $outputDir/post_route.dcp\n"
			"\n"
			"# Report the routing status, timing, power, design rule check\n"
			"report_route_status -file $outputDir/post_route_status.rpt\n"
			"report_timing_summary -file $outputDir/post_route_timing_summary.rpt\n"
			"report_power -file $outputDir/post_route_power.rpt\n"
			"report_drc -file $outputDir/post_route_drc.rpt\n"
			"\n"
			"\n"
			"\n"
			"# Note: For debug purposes, if you want to start from post- routing state,\n"
			"# uncomment the following 2 lines and remove all above commands\n"
			"#read_checkpoint $outputDir/post_route.dcp\n"
			"#link_design -top $top -part $part\n"
			"\n"
			"# Optionally generate post- routing simulation model\n"
			"#set postparDir $outputDir/vhdl-postpar/\n"
			"#file mkdir $postparDir\n"
			"#write_vhdl -force $postparDir/top.vhd\n"
			"#write_sdf -force $postparDir/top.sdf\n"
			"\n"
			"# Generate the bitstream\n"
			"write_bitstream -force $outputDir/project.bit\n"
			"\n",
			NULL
		);

		fclose(F);
	}

	// The TCL file to program the board
	if(board_fpga!=NULL && board_fpga->jtag_id > 0) {
		dbgprintf("Generating TCL file for Vivado hw_server...\n");

		sprintf(pathbuf, "%s/%s", conf->dirname, filename_tcl_prog);
		F = fopen(pathbuf, "wb");
		if(F==NULL) {
			printf("ERROR: could not open the file %s.\n", pathbuf);
			return -1;
		}

		// FIXME frequency should not be hardcoded here. It should be in the board config.
		//   Note: for the board zc706 the frequency 30 MHz was successfullly tested
		fprintfm(F,
			"# This TCL script was generated by AUGH\n"
			"\n"
			"# Note: This is a non-project script\n"
			"# For details about the TCL commands, please refer to the Xilinx document UG908\n"
			"# \"Vivado Design Suite User Guide - Programming and Debugging\"\n"
			"\n"
			"open_hw\n"
			"connect_hw_server\n"
			"\n"
			"current_hw_target [get_hw_targets]\n"
			"set_property PARAM.FREQUENCY 15000000 [lindex [get_hw_targets] 0]\n"
			"\n"
			"open_hw_target\n"
			"\n"
			"set_property PROGRAM.FILE {project.bit} [lindex [get_hw_devices] %u]\n", board_fpga->jtag_id-1,
			"program_hw_devices [lindex [get_hw_devices] %u]\n", board_fpga->jtag_id-1,
			"\n"
			"close_hw_target\n"
			"disconnect_hw_server\n"
			"close_hw\n"
			"\n",
			NULL
		);

		fclose(F);
	}

	// The general Makefile
	dbgprintf("Generating Makefile...\n");

	{  // These brackets are only here to fold the code :-)
		sprintf(pathbuf, "%s/%s", conf->dirname, filename_make);
		F = fopen(pathbuf, "wb");
		if(F==NULL) {
			printf("ERROR: could not open the file %s.\n", pathbuf);
			return -1;
		}

		fprintfm(F,
			"\n"
			"#================================================\n"
			"# Synthesis with Xilinx Vivado\n"
			"#================================================\n"
			"\n"
			"FILENAME_TCL = %s\n", filename_tcl,
			"\n"
			".PHONY: all clean\n"
			"\n"
			"all : %s\n", filename_bit,
			"\n"
			"%s:\n", filename_bit,
			"	vivado -mode batch -messageDb vivado.pb -log vivado.vdi -source $(FILENAME_TCL)\n"
			"\n"
			"clean:\n"
			"	rm -rf *.jou *.pb *.vdi *.bit *.dcp .*.rst *.rpt .Xil\n",
			NULL
		);

		if(board_fpga!=NULL && board_fpga->jtag_id > 0) {

			fprintfm(F,
				"\n"
				"\n"
				"#================================================\n"
				"# Program the FPGA with Vivado hw_server\n"
				"#================================================\n"
				"\n"
				".PHONY: program\n"
				"\n"
				"program:\n"
				"	vivado -mode batch -messageDb vivado-prog.pb -log vivado-prog.vdi -source program.tcl\n",
				NULL
			);

			if(board_fpga->xc3sprog_cable!=NULL) {
				fprintfm(F,
					"\n"
					"\n"
					"#================================================\n"
					"# Program the FPGA with xc3sprog\n"
					"#================================================\n"
					"\n"
					".PHONY: xc3sprog xc3sprog-program\n"
					"\n"
					"xc3sprog: xc3sprog-program\n"
					"\n"
					"xc3sprog-program:\n"
					"	xc3sprog -c %s -v -p %u %s:w::bit\n", board_fpga->xc3sprog_cable, board_fpga->jtag_id-1, filename_bit,
					NULL
				);
			}

		}

		fprintf(F, "\n");
		fclose(F);
	}

	printf("End of Vivado project generation\n");

	return 0;
}

int Vivado_Gen_Project(implem_t* Implem, char* dirname) {
	xilinx_techdata_t* techdata = Implem->synth_target->techno_data;

	// Config structure
	vivado_proj_conf_t conf_i;
	vivado_proj_conf_t* conf = &conf_i;
	memset(&conf_i, 0, sizeof(conf_i));

	conf->topname = Implem->netlist.top->name;

	conf->dirname = dirname;
	if(conf->dirname==NULL) conf->dirname = techdata->synth_prj_dir;
	if(conf->dirname==NULL) conf->dirname = VIVADO_PRJ_DEFAULT;

	// Save the project directory in the techdata, in case it isn't
	techdata->synth_prj_dir = conf->dirname;

	unsigned errors_nb = 0;

	if(Implem->synth_target->model!=NULL) conf->target_fpga = Implem->synth_target->model->name;
	else conf->target_fpga = techdata->chip_name;

	if(Implem->synth_target->timing!=NULL) conf->speed_grade = Implem->synth_target->timing->name;
	else conf->speed_grade = techdata->chip_speed;

	if(Implem->synth_target->package!=NULL) conf->package = Implem->synth_target->package->name;
	else conf->package = techdata->chip_package;

	if(conf->target_fpga==NULL) {
		printf("Error [techno '%s']: No FPGA chip target defined.\n", Implem->synth_target->techno->name);
		errors_nb ++;
	}
	if(conf->speed_grade==NULL) {
		printf("Error [techno '%s']: No FPGA speed grade defined.\n", Implem->synth_target->techno->name);
		errors_nb ++;
	}
	if(conf->package==NULL) {
		printf("Error [techno '%s']: No FPGA package defined.\n", Implem->synth_target->techno->name);
		errors_nb ++;
	}

	if(errors_nb!=0) return -1;

	conf->list_dir_src = addchain(NULL, Implem->env.vhdl_dir);

	// Actual generation
	int z = Vivado_Gen_Project_conf(Implem, conf);

	// Clean
	freechain(conf->list_dir_src);

	return z;
}


// Function to use as callback in techno_t structure
static int Vivado_Gen_Project_callback(implem_t* Implem) {
	return Vivado_Gen_Project(Implem, NULL);
}

// Launch back-end logic synthesis, place and route
// Return 0 if OK
int Vivado_Launch(implem_t* Implem) {
	xilinx_techdata_t* techdata = Implem->synth_target->techno_data;

	if(techdata->synth_prj_dir==NULL) {
		printf("Error: The back-end project files are not generated. Can't launch back-end.\n");
		return -1;
	}

	// Build the command-line: make -C <dirname>
	char buf[20 + strlen(techdata->synth_prj_dir)];
	sprintf(buf, "make -C %s", techdata->synth_prj_dir);

	fflush(NULL);

	return system(buf);
}


void Vivado_Techno_SetCB(techno_t* techno) {
	techno->synth_gen_prj = Vivado_Gen_Project_callback;
	techno->synth_launch  = Vivado_Launch;
}


