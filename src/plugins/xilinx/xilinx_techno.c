
/*

Note:
The HOLD times are not taken into account.
We assume the signals will always hold long enough.

The default timings returned by the functions are the max values for all paths inside the operator.
To get the actual timing for a particular path (source port-> dest port), scan the list.
  -> NOT IMPLEMENTED

FIXME: the structures generic_delay_t, generic_timings_t, generic_carac_t
are not appropriate. Only xilinx_res_t is actually needed.
Something else should be designed for the fast delay estimation of VEX expressions.

*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <stdbool.h>
#include <ctype.h>

#include "../../augh/indent.h"
#include "../../augh/auto/auto.h"
#include "../../augh/auto/techno.h"
#include "../../augh/auto/techno_delay.h"
#include "../../augh/hvex/hvex_misc.h"
#include "../../augh/netlist/netlist.h"
#include "../../augh/netlist/netlist_comps.h"
#include "../../augh/netlist/netlist_access.h"
#include "../../augh/netlist/netlist_vhdl.h"
#include "../../augh/plugins/plugins_techno.h"
#include "../../augh/plugins/plugins_netlist.h"

#include "xilinx_techno.h"
#include "xilinx_load.h"
#include "xilinx_xst.h"
#include "xilinx_vivado.h"
#include "xilinx_bram.h"



// Create a pool of type : xilinx_res_t

#define POOL_prefix      pool_xilinx_res
#define POOL_data_t      xilinx_res_t
#define POOL_ALLOC_SIZE  1000

#define POOL_INSTANCE             POOL_XILINX_RES
#define POOL_INSTANCE_ATTRIBUTES  static

#include "../../augh/pool.h"

// Allocation wrappers

xilinx_res_t* Xilinx_Res_New() {
	xilinx_res_t* res = pool_xilinx_res_pop(&POOL_XILINX_RES);
	memset(res, 0, sizeof(*res));
	return res;
}
void Xilinx_Res_Free(xilinx_res_t* res) {
	pool_xilinx_res_push(&POOL_XILINX_RES, res);
}


// The names of resource types (shared values)

char* namealloc_ff      = NULL;
char* namealloc_lut6    = NULL;
char* namealloc_lutram  = NULL;
char* namealloc_dsp48e  = NULL;
char* namealloc_bram36k = NULL;



//======================================================================
// Utility functions
//======================================================================

static void add_resources(xilinx_res_t* dest, xilinx_res_t* add, unsigned n) {
	dest->lut6    += n*add->lut6;
	dest->ff      += n*add->ff;
	dest->dsp48e  += n*add->dsp48e;
	dest->bram36k += n*add->bram36k;
	dest->lutram  += n*add->lutram;
}
static void mult_resources(xilinx_res_t* res, unsigned n) {
	res->lut6    *= n;
	res->ff      *= n;
	res->dsp48e  *= n;
	res->bram36k *= n;
	res->lutram  *= n;
}
static void clear_resources(xilinx_res_t* res) {
	res->lut6    = 0;
	res->ff      = 0;
	res->dsp48e  = 0;
	res->bram36k = 0;
	res->lutram  = 0;
}



//======================================================================
// Evaluation of individual components
//======================================================================

typedef struct generic_delay_t  generic_delay_t;
struct generic_delay_t {
	double delay;            // Only for combinatorial operators
	double before_clk;       // Time before clock
	double after_clk;        // Time after clock
	unsigned clock_edges;    // Number of internal clock cycles
	double clock_period;     // Clock period. valid only if field clock_edges >= 2
};

typedef struct generic_timings_t  generic_timings_t;
struct generic_timings_t {
	struct {
		// FIXME Do not use void*. Link to the component models.
		void *src;
		void *dest;
	} ports;
	generic_delay_t delay;
	// FIXME add the list of generic_delay_t elements
	generic_timings_t *next;
};

typedef struct generic_carac_t  generic_carac_t;
struct generic_carac_t {
	xilinx_res_t*      resources;
	generic_timings_t* timings;
};

static inline void delay_init(generic_delay_t *delay) {
	delay->delay        = 0;
	delay->before_clk   = 0;
	delay->after_clk    = 0;
	delay->clock_edges  = 0;
	delay->clock_period = 0;
}
static inline void timings_init(generic_timings_t *time) {
	time->ports.src  = NULL;
	time->ports.dest = NULL;
	delay_init(&time->delay);
	time->next       = NULL;
}

static inline void carac_add_resources(generic_carac_t *dest, generic_carac_t *add, unsigned n) {
	add_resources(dest->resources, add->resources, n);
}
static inline void carac_mult_resources(generic_carac_t *carac, unsigned n) {
	mult_resources(carac->resources, n);
}



// Create a pool of type : generic_timings_t

#define POOL_prefix      pool_timing
#define POOL_data_t      generic_timings_t
#define POOL_ALLOC_SIZE  1000

#define POOL_INSTANCE             POOL_TIMING
#define POOL_INSTANCE_ATTRIBUTES  static

#include "../../augh/pool.h"

// Allocation wrappers

static generic_timings_t* timing_new() {
	generic_timings_t* timing = pool_timing_pop(&POOL_TIMING);
	timings_init(timing);
	return timing;
}
static void timing_free(generic_timings_t* timing) {
	pool_timing_push(&POOL_TIMING, timing);
}


// Create a pool of type : generic_carac_t

#define POOL_prefix      pool_carac
#define POOL_data_t      generic_carac_t
#define POOL_ALLOC_SIZE  1000

#define POOL_INSTANCE             POOL_CARAC
#define POOL_INSTANCE_ATTRIBUTES  static

#include "../../augh/pool.h"

// Allocation wrappers

static generic_carac_t* carac_new() {
	generic_carac_t* carac = pool_carac_pop(&POOL_CARAC);
	carac->resources = Xilinx_Res_New();
	carac->timings = timing_new();
	return carac;
}
static void carac_free(generic_carac_t* carac) {
	if(carac->resources!=NULL) Xilinx_Res_Free(carac->resources);
	if(carac->timings!=NULL) timing_free(carac->timings);
	pool_carac_push(&POOL_CARAC, carac);
}



// Conversion of hardware resources, from internal structure to generic lists with names
static ptype_list* NameRes_FromCarac(xilinx_res_t* carac) {
	ptype_list* list = NULL;
	if(carac->ff>0)      list = addptype(list, carac->ff, namealloc_ff);
	if(carac->lut6>0)    list = addptype(list, carac->lut6, namealloc_lut6);
	if(carac->dsp48e>0)  list = addptype(list, carac->dsp48e, namealloc_dsp48e);
	if(carac->bram36k>0) list = addptype(list, carac->bram36k, namealloc_bram36k);
	if(carac->lutram>0)  list = addptype(list, carac->lutram, namealloc_lutram);
	return list;
}
static ptype_list* NameRes_AddCarac(ptype_list* list, xilinx_res_t* res) {
	ptype_list* tmp = NameRes_FromCarac(res);
	list = Techno_NameRes_Add(list, tmp);
	freeptype(tmp);
	return list;
}
__attribute((__unused__))
static bitype_list* NameOpRes_AddCarac(bitype_list* list, char* name, xilinx_res_t* res) {
	bitype_list* elt = ChainBiType_SearchFrom(list, name);
	if(elt==NULL) {
		list = addbitype(list, 0, name, NULL);
		elt = list;
	}
	elt->DATA_TO = NameRes_AddCarac(elt->DATA_TO, res);
	return list;
}




// Compares two signals with each other. Only equality checked.
// Also used by the MUX Direct selection.
static generic_carac_t* GetCarac_Cmp_eq(synth_target_t* synth, unsigned width) {

	if(width==0) {
		fprintf(stderr, "ERROR %s : width=%u\n", __func__, width);
		abort();
	}

	xilinx_timing_t* xtiming = synth->timing->data;

	generic_carac_t* carac = carac_new();

	// Compute for one bit
	unsigned level_luts_nb;
	unsigned prev_level_sig_nb;

	// First level
	level_luts_nb = width/3;
	if(3*level_luts_nb<width) level_luts_nb++;

	carac->resources->lut6 += level_luts_nb;
	carac->timings->delay.delay += xtiming->lut6_delay;

	// Next levels
	prev_level_sig_nb = level_luts_nb;
	while(prev_level_sig_nb>1) {
		level_luts_nb = prev_level_sig_nb/6;
		if(6*level_luts_nb<prev_level_sig_nb) level_luts_nb++;
		prev_level_sig_nb = level_luts_nb;
		carac->resources->lut6 += level_luts_nb;
		carac->timings->delay.delay += xtiming->lut6_delay;
		carac->timings->delay.delay += xtiming->wire_delay;
	}

	return carac;
}

// Compares one signal with a constant value.
// Input : one vector, return 1 bit
static generic_carac_t* GetCarac_Cmp_cst(synth_target_t* synth, unsigned width) {
	xilinx_timing_t* xtiming = synth->timing->data;

	generic_carac_t* carac = carac_new();

	if(width==0) return carac;

	unsigned level = 0;
	unsigned prev_level_sig_nb = width;
	unsigned level_luts_nb;

	while(prev_level_sig_nb>1) {
		level_luts_nb = prev_level_sig_nb/6;
		if(6*level_luts_nb<prev_level_sig_nb) level_luts_nb++;
		prev_level_sig_nb = level_luts_nb;
		carac->resources->lut6 += level_luts_nb;
		carac->timings->delay.delay += xtiming->lut6_delay;
		if(level>0) carac->timings->delay.delay += xtiming->wire_delay;
		level++;
	}

	return carac;
}

// Multiplexer, direct selection (for one bit width)
typedef struct xil_muxd_implem {
	double   delay;
	unsigned lut6_nb;
	unsigned lut6_levels_nb;
} xil_muxd_implem;
static void Xil_MuxDirect_GetImplem(synth_target_t* synth, unsigned inputs, xil_muxd_implem* data) {
	xilinx_timing_t* xtiming = synth->timing->data;

	data->delay = 0;
	data->lut6_nb = 0;
	data->lut6_levels_nb = 0;

	if(inputs==0) return;

	unsigned inputs_orig = inputs;
	unsigned inputs_done = 0;

	while(inputs_orig>0 || inputs_done>1) {
		if(data->lut6_levels_nb>0) data->delay += xtiming->wire_delay;

		// Number of inputs of the remaining LUT of the current stage
		unsigned inputs_done_next = 0;

		if(inputs_done>=6) {
			unsigned q = inputs_done / 6;
			data->lut6_nb += q;
			inputs_done_next += q;
			inputs_done = inputs_done - q*6;
		}

		if(inputs_orig>=3) {
			unsigned q = inputs_orig / 3;
			data->lut6_nb += q;
			inputs_done_next += q;
			inputs_orig = inputs_orig - q*3;
		}

		// Add one LUT at this stage to merge more inputs
		if(inputs_orig>0 || inputs_done > 1) {
			unsigned in_lut_rem = 6;
			// Pack the original inputs first
			in_lut_rem -= 2*inputs_orig;
			inputs_orig = 0;
			// Fill what remains with the inputs "done"
			if(inputs_done <= in_lut_rem) {
				inputs_done = 0;
			}
			else {
				inputs_done -= in_lut_rem;
			}
			// Count the new LUT
			data->lut6_nb++;
			inputs_done_next++;
		}

		// Add all remaining inputs for the next stage
		inputs_done += inputs_done_next;

		data->lut6_levels_nb++;
	}  // Infinite loop

}
// The inputs are literals known at compilation
__attribute((__unused__))
static generic_carac_t* GetCarac_Mux_DirectCst(synth_target_t* synth, unsigned inputs, unsigned width) {
	xilinx_timing_t* xtiming = synth->timing->data;

	generic_carac_t* carac = carac_new();

	if(inputs==0 || width==0) return carac;

	// Compute the resources for one bit width
	unsigned stages_nb = 0;

	while(inputs>1) {
		if(stages_nb>0) carac->timings->delay.delay += xtiming->wire_delay;

		unsigned inputs_next = 0;

		unsigned q = inputs / 6;
		carac->resources->lut6 += q;
		inputs_next = q;
		inputs = inputs - q*6;

		// Add one LUT at this stage to merge more inputs
		if(inputs>1) {
			carac->resources->lut6++;
			inputs_next++;
		}

		// Add all remaining inputs for the next stage
		inputs = inputs_next;

		stages_nb++;
	}  // Infinite loop

	// Compute the size for all the bits
	carac_mult_resources(carac, width);

	return carac;
}

// Multiplexer, binary selection. Uses LUT6 + slice MUX.
static generic_carac_t* GetCarac_Mux_Binary(synth_target_t* synth, unsigned inputs, unsigned width) {

	if(inputs==0 || width==0) {
		fprintf(stderr, "ERROR %s : inputs=%u width=%u\n", __func__, inputs, width);
		abort();
	}

	xilinx_timing_t* xtiming = synth->timing->data;

	generic_carac_t* carac = carac_new();

	// FIXME Handle when there are 2 inputs: one LUT6 can output 2 bits

	// Compute for one bit
	if(inputs==1) {
		// There is no component inside.
	}
	else if(inputs<=4) {
		// At most 4 inputs: The entire MUX fits in one LUT6.
		carac->resources->lut6 = 1;
		carac->timings->delay.delay = xtiming->lut6_delay;
	}
	else if(inputs<=8) {
		// At most 8 inputs: The entire MUX fits in 2 LUT6 + one slice MUX.
		carac->resources->lut6 = 2;
		carac->timings->delay.delay = xtiming->lut6_delay + xtiming->mux_delay;
	}
	else if(inputs<=12) {
		// At most 12 inputs: The entire MUX fits in 3 LUT6 + 2 slice MUX.
		carac->resources->lut6 = 3;
		carac->timings->delay.delay = xtiming->lut6_delay + 2 * xtiming->mux_delay;
	}
	else if(inputs<=16) {
		// At most 16 inputs: The entire MUX fits in 4 LUT6 + 3 slice MUX (2 deep).
		carac->resources->lut6 = 4;
		carac->timings->delay.delay = xtiming->lut6_delay + 2 * xtiming->mux_delay;
	}
	else {
		// More than 16 inputs: Use are several stages.
		generic_carac_t* carac_tmp;
		// Get the greater power of 16 <= inputs
		unsigned p = uint_rndpow_floor(16, inputs);
		unsigned q = inputs/p;
		unsigned inputs_rem = inputs-(q*p);
		if(p==inputs) { p/=16; q=16; }  // Avoid infinite recursion
		// Get the biggest sub-MUX
		carac_tmp = GetCarac_Mux_Binary(synth, p, 1);
		carac_add_resources(carac, carac_tmp, q);
		carac->timings->delay.delay = carac_tmp->timings->delay.delay;
		carac_free(carac_tmp);
		// Add the extra MUX if needed
		unsigned locinputs = q;
		if(inputs_rem > 0) {
			if(inputs_rem > 1) {
				// Resources of the MUX for the remaining inputs
				carac_tmp = GetCarac_Mux_Binary(synth, inputs_rem, 1);
				carac_add_resources(carac, carac_tmp, 1);
				carac_free(carac_tmp);
			}
			// Add 1 to the number of inputs to handle by the merging logic
			locinputs ++;
		}
		// Add the merging logic
		carac->timings->delay.delay += xtiming->wire_delay;
		carac->timings->delay.delay += xtiming->lut6_delay;
		if(locinputs<=4) {
			carac->resources->lut6 += 1;
		}
		else if(locinputs<=8) {
			carac->resources->lut6 += 2;
			carac->timings->delay.delay += xtiming->mux_delay;
		}
		else if(locinputs<=12) {
			carac->resources->lut6 += 3;
			carac->timings->delay.delay += 2 * xtiming->mux_delay;
		}
		else {
			carac->resources->lut6 += 4;
			carac->timings->delay.delay += 2 * xtiming->mux_delay;
		}
	}

	// Compute the size for all the bits
	carac_mult_resources(carac, width);

	return carac;
}

// Logic operations : AND, OR, XOR and their negations
static generic_carac_t* GetCarac_Logic(synth_target_t* synth, unsigned inputs, unsigned width) {
	xilinx_timing_t* xtiming = synth->timing->data;

	generic_carac_t* carac = carac_new();
	if(inputs<2 || width==0) return carac;

	if(inputs<=6) {
		// All fits into one LUT6
		carac->resources->lut6 += 1;
		carac->timings->delay.delay += xtiming->lut6_delay;
	}
	else {
		generic_carac_t *carac_tmp = carac_new();
		// Get the greater power of 6 <= inputs
		unsigned p = uint_rndpow_floor(6, inputs);
		if(p==inputs) p/=6;
		unsigned q = inputs/p;
		unsigned inputs_rem = inputs-(q*p);
		// Add the stats of the biggest sub-MUX
		carac_tmp = GetCarac_Logic(synth, p, 1);
		carac_add_resources(carac, carac_tmp, q);
		carac->timings->delay.delay += carac_tmp->timings->delay.delay;
		carac_free(carac_tmp);
		if(q + inputs_rem<=6) {
			// The last LUT is enough
		}
		else if(inputs_rem>0) {
			// Resources of the MUX for the remaining inputs
			carac_tmp = GetCarac_Mux_Binary(synth, inputs_rem, 1);
			carac_add_resources(carac, carac_tmp, q);
			carac_free(carac_tmp);
		}
		// One last LUT
		carac->timings->delay.delay += xtiming->wire_delay;
		carac->resources->lut6 += 1;
		carac->timings->delay.delay += xtiming->lut6_delay;
	}

	// Compute the size for all the bits
	carac_mult_resources(carac, width);

	return carac;
}

// Rotation
static generic_carac_t* GetCarac_Rot(synth_target_t* synth, unsigned width_shift, unsigned width_data) {
	xilinx_timing_t* xtiming = synth->timing->data;

	generic_carac_t* carac = carac_new();
	if(width_data==0 || width_shift==0) return carac;

	// Compute the number of stages
	unsigned stages_nb = width_shift / 2;
	if(stages_nb*2 < width_shift) stages_nb++;

	carac->resources->lut6 = stages_nb * width_data;
	carac->timings->delay.delay = stages_nb * xtiming->lut6_delay + (stages_nb-1) * xtiming->wire_delay;

	return carac;
}

// Logic operations : the NOT
static generic_carac_t* GetCarac_Not(synth_target_t* synth, unsigned width) {

	if(width==0) {
		fprintf(stderr, "ERROR %s : width=%u\n", __func__, width);
		abort();
	}

	xilinx_timing_t* xtiming = synth->timing->data;
	generic_carac_t* carac = carac_new();

	// Get the number of LUT5
	int lut5_nb = width/2;
	int rem = width - 2*lut5_nb;

	// Add the LUTs
	carac->resources->lut6 += lut5_nb;
	carac->resources->lut6 += rem;
	carac->timings->delay.delay += xtiming->lut6_delay;

	return carac;
}

#if 0  // Not used yet
// Logic unit : one LUT per output bit
static generic_carac_t* GetCarac_LU(synth_target_t* synth, unsigned width) {
	xilinx_timing_t* xtiming = synth->timing->data;
	generic_carac_t* carac = carac_new();

	if(width==0) return carac;

	carac->resources->lut6 = width;
	carac->timings->delay.delay += xtiming->lut6_delay;

	return carac;
}
#endif

// Shifters.
// FIXME This eval technique is WRONG. Must use barrel shifters.
static generic_carac_t* GetCarac_Shift(synth_target_t* synth, unsigned width) {

	if(width==0) {
		fprintf(stderr, "ERROR %s : width=%u\n", __func__, width);
		abort();
	}

	xilinx_timing_t* xtiming = synth->timing->data;
	generic_carac_t* carac = carac_new();

	carac->resources->lut6 += width;
	carac->timings->delay.delay += xtiming->lut6_delay;

	return carac;
}

// Simple adder, sub or both: same characteristics
static generic_carac_t* GetCarac_Add_Sub(synth_target_t* synth, unsigned width) {

	if(width<1) {
		fprintf(stderr, "ERROR %s : width=%u\n", __func__, width);
		abort();
	}

	xilinx_timing_t* xtiming = synth->timing->data;
	generic_carac_t* carac = carac_new();

	carac->resources->lut6 += width;
	// First level is a LUT and one MUXCY (input S, slow)
	// Other levels are one LUT and one MUXCY (input CI, fast)
	carac->timings->delay.delay += xtiming->lut6_delay;
	if(width>=2) {
		carac->timings->delay.delay += xtiming->CARRY_S_delay;
		carac->timings->delay.delay += (width-2)*xtiming->CARRY_CI_delay;
		carac->timings->delay.delay += xtiming->XORCY_CI_delay;
	}

	return carac;
}


/* Unsigned multiplier. Computes the full untruncated result.
At the end there is a factor pplied to the LUT6 number. This is because XST more LUTs than this estimation

	Size   Xilinx Synth   Xilinx Map   estimation    ratio

	32x32      1576          1138         1018       1.118
	16x32      758           534          491        1.088
	8x32       357           243          228        1.066
	4x32       xxx           xxx          97         xxxx

	24x24      886           652          571        1.142
	16x24      575           417          371        1.124

	16x16      391           297          251        1.183
	8x16       181           131          116        1.129
	4x16        80            52          49         1.061
*/
static generic_carac_t* GetCarac_MUL_u_full(synth_target_t* synth, unsigned size1, unsigned size2) {

	if(size1==0 || size2==0) {
		fprintf(stderr, "ERROR %s : size1=%u size2=%u\n", __func__, size1, size2);
		abort();
	}

	xilinx_timing_t* xtiming = synth->timing->data;

	generic_carac_t* carac = carac_new();
	generic_carac_t* carac_tmp;

	// Create the adders
	unsigned values_nb, adders_size;
	if(size1<size2) { values_nb=size1; adders_size=size2; }
	else            { values_nb=size2; adders_size=size1; }

	for(unsigned level=0; values_nb>1; level++) {
		unsigned adders_nb = values_nb/2;
		carac_tmp = GetCarac_Add_Sub(synth, adders_size);
		carac_add_resources(carac, carac_tmp, adders_nb);
		if(level>0) carac->timings->delay.delay += xtiming->wire_delay;
		carac->timings->delay.delay += carac_tmp->timings->delay.delay;
		carac_free(carac_tmp);
		// Number of values to add at the next level
		if(2*adders_nb==values_nb) values_nb = 0; else values_nb = 1;
		values_nb += adders_nb;
		adders_size ++;
	}

	// This is a correction. Empirically found.
	carac->resources->lut6 = (119 * carac->resources->lut6) / 100;

	return carac;
}

// Multiplier using DSP cores
static generic_carac_t* GetCarac_Mul_WithDSP(synth_target_t* synth, unsigned widthA, unsigned widthB, unsigned width_out, bool is_signed) {

	if(widthA==0 || widthB==0) {
		fprintf(stderr, "ERROR %s : widthA=%u widthB=%u\n", __func__, widthA, widthA);
		abort();
	}

	xilinx_timing_t* xtiming = synth->timing->data;

	generic_carac_t* carac = carac_new();

	if( (widthA<=16 && widthB<=16) || width_out<=16) {
		// Only one DSP core, no extra adder
		carac->resources->dsp48e = 1;
		carac->timings->delay.delay += xtiming->dsp48e.ab_mul_cout;
		return carac;
	}

	// Simplified case where the entire multiplier holds into one DSP core

	unsigned width_large = widthA;
	unsigned width_short = widthB;
	if(widthA < widthB) {
		width_large = widthB;
		width_short = widthA;
	}

	if(
		(is_signed==true && width_large<=25 && width_short<=18) ||
		(is_signed==false && width_large<=24 && width_short<=17)
	) {
		carac->resources->dsp48e = 1;
		carac->timings->delay.delay = xtiming->dsp48e.ab_mul_cout;
		return carac;
	}

	generic_carac_t* carac_tmp;

	/* Use DSP48E components to compute most of the data
	Example when split by 16 bits : the inputs are A2 & A1 & A0, and B1 & B0.

	          A2 A1 A0
	 *           B1 B0
	===================
	             B0xA0
	          B0xA1  |
	       B0xA2  |  |
	          B1xA0  |
	       B1xA1  |  |
	 +  B1xA2  |  |  |
	===================
	   (4)(3)(2)(1)(0)

	Only the partial products B1xA2, B1xA2 B0xA2, B1xA1 are signed.
	This reduces the number of additions that have to be done after.
	*/

	// Simplified case : split input words in 16-bits wide segments.
	// All partial products will easily fit into the 18x25 multipliers.
	unsigned seg16_inA = (widthA+15)/16;
	unsigned seg16_inB = (widthB+15)/16;

	unsigned nb_partprod_useful = 0;
	unsigned nb_partprod_signed = 0;
	for(unsigned idxA=0; idxA<seg16_inA; idxA++) {
		for(unsigned idxB=0; idxB<seg16_inB; idxB++) {
			if(16*(idxA+idxB)>=width_out) continue;
			nb_partprod_useful ++;
			if(idxA==seg16_inA-1 || idxB==seg16_inB-1) nb_partprod_signed ++;
		}
	}

	// Count the DSP cores
	carac->resources->dsp48e = nb_partprod_useful;
	carac->timings->delay.delay += xtiming->dsp48e.ab_mul_cout;

	// Estimation: all additions have width = width_out-16 bits.
	// Estimation: only nb_partprod_signed additions are needed.
	unsigned values_nb = nb_partprod_signed;

	// Create the adders
	carac_tmp = GetCarac_Add_Sub(synth, width_out-16);
	carac_add_resources(carac, carac_tmp, values_nb);

	// Delay: count the number of levels
	while(values_nb>1) {
		carac->timings->delay.delay += xtiming->wire_delay;
		carac->timings->delay.delay += carac_tmp->timings->delay.delay;
		values_nb = (values_nb+1)/2;
	}

	carac_free(carac_tmp);

	return carac;
}

static generic_carac_t* GetCarac_Mul(synth_target_t* synth, unsigned widthA, unsigned widthB, unsigned width_out, bool is_signed) {
	xilinx_timing_t* xtiming = synth->timing->data;
	xilinx_techdata_t* techdata = synth->techno_data;
	if(xtiming->dsp48e.avail==true && techdata->use_dsp==true) {
		bool use_dsp = true;
		if(synth->resources!=NULL) {
			// If no DSP in targeted resource, disable DSP
			use_dsp = false;
			foreach(synth->resources, scan) if(scan->DATA==namealloc_dsp48e) { use_dsp = true; break; }
		}
		if(use_dsp==true) {
			return GetCarac_Mul_WithDSP(synth, widthA, widthB, width_out, is_signed);
		}
	}
	return GetCarac_MUL_u_full(synth, widthA, widthB);
}


static generic_carac_t* GetCarac_DivQR(synth_target_t* synth, unsigned width_num, unsigned width_den) {
	if(width_num<1 || width_den<1) {
		fprintf(stderr, "ERROR %s : width_num=%u width_den=%u\n", __func__, width_num, width_den);
		abort();
	}

	xilinx_timing_t* xtiming = synth->timing->data;
	generic_carac_t* carac = carac_new();

	// There are width_num levels of SUB components, each of width width_den
	carac->resources->lut6 += width_num * width_den;
	// There are also width_num-1 levels of MUX to route the right part of the denominator to next levels
	carac->resources->lut6 += (width_num-1) * width_den;

	// TODO for the last level, if the REM output is not used, the MUX can be implemented inside the LUTs of the SUB
	// But this requires an additional parameter to indicate the presence of that port

	// TODO Ensure the SUB delay will actually be evaluated with a carry chain,
	// because here we need all LUT6 inputs to implement the MUX
	// However, the first SUB level can be evaluated any way.

	// Get the delay of one SUB
	generic_carac_t* carac_tmp = GetCarac_Add_Sub(synth, width_den);
	double onelevel_latency = carac_tmp->timings->delay.delay;
	carac_free(carac_tmp);

	// For each level, the critical path is on the numerator part: through a MUX or through the previous SUB
	// The worst case delay is through the previous SUB
	carac->timings->delay.delay = width_num * onelevel_latency + (width_num-1) * xtiming->wire_delay;

	return carac;
}


// The number of ports include the R/W port.
// All ports shall be independent. All are asynchronous Read ports. One port is R/W.
static generic_carac_t* GetCarac_LUTRAM(synth_target_t* synth, unsigned ports, unsigned cells, unsigned width) {
	xilinx_timing_t* xtiming = synth->timing->data;

	generic_carac_t* carac = carac_new();
	carac->timings->delay.clock_edges = 1;

	if(width==0) {
		fprintf(stderr, "WARNING %s : width=%u\n", __func__, width);
		//abort();
		return carac;
	}
	if(cells==0) {
		fprintf(stderr, "WARNING %s : cells=%u\n", __func__, cells);
		//abort();
		return carac;
	}
	if(ports==0) {
		fprintf(stderr, "WARNING %s : ports=%u\n", __func__, ports);
		//abort();
		return carac;
	}

	generic_carac_t* carac_tmp;

	if(cells <= 32) {
		// Use 2 bits/LUT6. 4 ports per slice.
		carac->timings->delay.delay      = xtiming->lut6_delay;
		carac->timings->delay.after_clk  = xtiming->lutram_aft;
		carac->timings->delay.before_clk = xtiming->lutram_bef_wd;

		// Compute the number of LUT6 for one port
		unsigned luts = (width + 1)/2;

		if(ports<=4) {
			carac->resources->lut6   += ports*luts;
			carac->resources->lutram += ports*luts;
		}
		else {
			carac->resources->lut6   += 4*luts;
			carac->resources->lutram += 4*luts;
			// More slices have to be used to have more ports
			// FIXME This is not efficient at all, count how many full slices have to be used + what remains (and adapt other cases of this function)
			carac_tmp = GetCarac_LUTRAM(synth, ports-3, cells, width);
			carac_add_resources(carac, carac_tmp, 1);
			carac_free(carac_tmp);
		}
	}

	else if(cells <= 64) {
		// Use LUT6. 1 bit per LUT-> 4 ports per slice.
		carac->timings->delay.delay      = xtiming->lut6_delay;
		carac->timings->delay.before_clk = xtiming->lutram_bef_wd;
		carac->timings->delay.after_clk  = xtiming->lutram_aft;

		if(ports<=4) {
			carac->resources->lut6   += ports*width;
			carac->resources->lutram += ports*width;
		}
		else {
			carac->resources->lut6   += 4*width;
			carac->resources->lutram += 4*width;
			// More slices have to be used to have more ports
			carac_tmp = GetCarac_LUTRAM(synth, ports-3, cells, width);
			carac_add_resources(carac, carac_tmp, 1);
			carac_free(carac_tmp);
		}
	}

	else if(cells <= 128) {
		// Use 2 LUT6 + one slice MUX per bit. 2 ports per slice.
		carac->timings->delay.delay      = xtiming->lut6_delay + xtiming->mux_delay;
		carac->timings->delay.before_clk = xtiming->lutram_bef_wd;
		carac->timings->delay.after_clk  = xtiming->lutram_aft + xtiming->mux_delay;

		if(ports<=2) {
			carac->resources->lut6   += ports*width*2;
			carac->resources->lutram += ports*width*2;
		}
		else {
			carac->resources->lut6   += 2*width;
			carac->resources->lutram += 2*width;
			// More slices have to be used to have more ports
			carac_tmp = GetCarac_LUTRAM(synth, ports-1, cells, width);
			carac_add_resources(carac, carac_tmp, 1);
			carac_free(carac_tmp);
		}
	}

	else if(cells <= 256) {
		if(ports==1) {
			// Use 4 LUT6 + 2 slice MUX per bit. 1 port per slice.
			carac->timings->delay.delay      = xtiming->lut6_delay + 2 * xtiming->mux_delay;
			carac->timings->delay.before_clk = xtiming->lutram_bef_wd;
			carac->timings->delay.after_clk  = xtiming->lutram_aft + 2 * xtiming->mux_delay;
			carac->resources->lut6   += width*4;
			carac->resources->lutram += width*4;
		}
		else {
			// We can have more than one port. That needs a bit more logic.
			// Use 4 64-addresses LUTRAMs
			carac_tmp = GetCarac_LUTRAM(synth, ports, 64, width);
			carac_add_resources(carac, carac_tmp, 4);
			carac->timings->delay.delay      = carac_tmp->timings->delay.delay;
			carac->timings->delay.before_clk = carac_tmp->timings->delay.before_clk;
			carac->timings->delay.after_clk  = carac_tmp->timings->delay.after_clk;
			carac_free(carac_tmp);
			// Each 64-addresses block has an Enable LUT on the write enable input
			carac->resources->lut6 += 4;
			carac->timings->delay.before_clk += xtiming->lut6_delay;
			// Each port has a 4->1 MUX on the output (1 LUT6 per bit : 4 wires for data, 2 wires for selection)
			carac->resources->lut6 += ports*width;
			carac->timings->delay.delay     += xtiming->wire_delay + xtiming->lut6_delay;
			carac->timings->delay.after_clk += xtiming->wire_delay + xtiming->lut6_delay;
		}
	}

	else if(cells <= 512) {
		// Use 4 128-addresses LUTRAMs
		carac_tmp = GetCarac_LUTRAM(synth, ports, 128, width);
		carac_add_resources(carac, carac_tmp, 4);
		carac->timings->delay.delay      = carac_tmp->timings->delay.delay;
		carac->timings->delay.before_clk = carac_tmp->timings->delay.before_clk;
		carac->timings->delay.after_clk  = carac_tmp->timings->delay.after_clk;
		carac_free(carac_tmp);
		// Each 64-addresses block has an Enable LUT on the write enable input
		carac->resources->lut6 += 4;
		carac->timings->delay.before_clk += xtiming->lut6_delay;
		// Each port has a 4->1 MUX on the output (1 LUT6 per bit : 4 wires for data, 2 wires for selection)
		carac->resources->lut6 += ports*width;
		carac->timings->delay.delay     += xtiming->wire_delay + xtiming->lut6_delay;
		carac->timings->delay.after_clk += xtiming->wire_delay + xtiming->lut6_delay;
	}

	else {
		unsigned banks;
		if(ports==1) {
			// Use 4 256-addresses LUTRAMs
			banks = (cells+255)/256;
			carac_tmp = GetCarac_LUTRAM(synth, ports, 256, width);
		}
		else {
			// Use 128-addresses LUTRAMs
			banks = (cells+127)/128;
			carac_tmp = GetCarac_LUTRAM(synth, ports, 128, width);
		}
		carac_add_resources(carac, carac_tmp, banks);
		carac->timings->delay.delay      = carac_tmp->timings->delay.delay;
		carac->timings->delay.before_clk = carac_tmp->timings->delay.before_clk;
		carac->timings->delay.after_clk  = carac_tmp->timings->delay.after_clk;
		carac_free(carac_tmp);
		// Each 64-addresses block has an Enable LUT on the write enable input
		// FIXME only one LUT?
		carac->resources->lut6 += banks;
		carac->timings->delay.before_clk += xtiming->lut6_delay;
		// Each Read port has a binary MUX
		carac_tmp = GetCarac_Mux_Binary(synth, banks, width);
		carac_add_resources(carac, carac_tmp, ports);
		carac->timings->delay.delay     += xtiming->wire_delay + carac_tmp->timings->delay.delay;
		carac->timings->delay.after_clk += xtiming->wire_delay + carac_tmp->timings->delay.delay;
		carac_free(carac_tmp);
	}

	return carac;
}

// Assume only one Write port and one Read port, independent.
static generic_carac_t* GetCarac_Bram36k(synth_target_t* synth, unsigned cells, unsigned width) {
	xilinx_timing_t* xtiming = synth->timing->data;

	generic_carac_t* carac = carac_new();
	carac->timings->delay.clock_edges = 1;

	if(width==0 || cells==0) {
		fprintf(stderr, "WARNING %s : width=%u cells=%u\n", __func__, width, cells);
		//abort();
		return carac;
	}

	// Possible configurations for bram36k blocks (lines x width):
	//     512x72
	//    1024x36
	//    2048x18
	//    4096x9
	//    8192x4
	//   16384x2
	//   32768x1
	// And by using 2 cascaded BRAM36K;
	//   64536x1

	if(cells <= 512) {
		// Use 512x72 configuration
		carac->resources->bram36k = (width + 71) / 72;
		// FIXME Timings are very badly handled
		carac->timings->delay.after_clk  = xtiming->bram36k_aft_read;
		carac->timings->delay.before_clk = xtiming->bram36k_bef_ra;
	}

	else if(cells <= 1024) {
		// Use 1024x36 configuration
		carac->resources->bram36k = (width + 35) / 36;
		// FIXME Timings are very badly handled
		carac->timings->delay.after_clk  = xtiming->bram36k_aft_read;
		carac->timings->delay.before_clk = xtiming->bram36k_bef_ra;
	}

	else if(cells <= 2048) {
		// Use 2048x18 configuration
		carac->resources->bram36k = (width + 17) / 18;
		// FIXME Timings are very badly handled
		carac->timings->delay.after_clk  = xtiming->bram36k_aft_read;
		carac->timings->delay.before_clk = xtiming->bram36k_bef_ra;
	}

	else if(cells <= 4096) {
		// Use 4096x9 configuration
		carac->resources->bram36k = (width + 8) / 9;
		// FIXME Timings are very badly handled
		carac->timings->delay.after_clk  = xtiming->bram36k_aft_read;
		carac->timings->delay.before_clk = xtiming->bram36k_bef_ra;
	}

	else if(cells <= 8192) {
		// Use 8192x4 configuration
		carac->resources->bram36k = (width + 3) / 4;
		// FIXME Timings are very badly handled
		carac->timings->delay.after_clk  = xtiming->bram36k_aft_read;
		carac->timings->delay.before_clk = xtiming->bram36k_bef_ra;
	}

	else if(cells <= 16384) {
		// Use 16384x2 configuration
		carac->resources->bram36k = (width + 1) / 2;
		// FIXME Timings are very badly handled
		carac->timings->delay.after_clk  = xtiming->bram36k_aft_read;
		carac->timings->delay.before_clk = xtiming->bram36k_bef_ra;
	}

	else if(cells <= 32768) {
		// Use 32768x1 configuration
		carac->resources->bram36k = width;
		// FIXME Timings are very badly handled
		carac->timings->delay.after_clk  = xtiming->bram36k_aft_read;
		carac->timings->delay.before_clk = xtiming->bram36k_bef_ra;
	}

	else if(cells <= 65536) {
		// Use 65536x1 configuration, with cascaded RAM blocks
		carac->resources->bram36k = 2 * width;
		// FIXME Timings are very badly handled
		carac->timings->delay.after_clk  = xtiming->bram36k_aft_read_casc;
		carac->timings->delay.before_clk = xtiming->bram36k_bef_ra;
	}

	else {
		// Use 65536x1 configuration + multiplexers
		unsigned bram36k_height = (cells + 65535) / 65536 * 2;
		unsigned bram36k_width = width;
		carac->resources->bram36k = bram36k_height * bram36k_width;
		// FIXME Timings are very badly handled
		carac->timings->delay.after_clk  = xtiming->bram36k_aft_read;
		carac->timings->delay.before_clk = xtiming->bram36k_bef_ra;
		// Take the read MUX into account
		generic_carac_t* carac_tmp;
		carac_tmp = GetCarac_Mux_Binary(synth, bram36k_height, width);
		carac_add_resources(carac, carac_tmp, 1);
		carac->timings->delay.delay     += xtiming->wire_delay + carac_tmp->timings->delay.delay;
		carac->timings->delay.after_clk += xtiming->wire_delay + carac_tmp->timings->delay.delay;
		carac_free(carac_tmp);
	}

	return carac;
}

// Note: This simple function may seem unuseful, but future developments
// may take into account boundary scan, hardening etc
static generic_carac_t* GetCarac_FF(synth_target_t* synth, unsigned width) {

	if(width==0) {
		fprintf(stderr, "ERROR %s:%u : width=%u\n", __FILE__, __LINE__, width);
		abort();
	}

	xilinx_timing_t* xtiming = synth->timing->data;
	generic_carac_t* carac = carac_new();

	carac->timings->delay.clock_edges = 1;

	carac->resources->ff = width;
	carac->timings->delay.before_clk = GetMax(xtiming->FF_bef_wd, xtiming->FF_bef_ce);
	carac->timings->delay.after_clk  = xtiming->FF_aft_rd;

	return carac;
}

// Multiport memory. Internally, the cells are implemented as registers.
static generic_carac_t* GetCarac_Multiport(
	synth_target_t* synth,
	unsigned cells_nb, unsigned width, unsigned addr_width,
	unsigned ports_w, unsigned ports_r, bool direct
) {

	if(width==0) {
		fprintf(stderr, "ERROR %s : width=%u\n", __func__, width);
		abort();
	}
	if(cells_nb==0) {
		fprintf(stderr, "ERROR %s : cells_nb=%u\n", __func__, cells_nb);
		abort();
	}
	if(addr_width==0) {
		fprintf(stderr, "ERROR %s : addr_width=%u\n", __func__, addr_width);
		abort();
	}

	if(direct==false && ports_w<2) {
		return GetCarac_LUTRAM(synth, ports_r, cells_nb, width);
	}

	// Here the component is implemented as separated registers.

	// Number of ports per cell
	unsigned total_ports_w = ports_w + (direct==true ? 1 : 0);

	xilinx_timing_t* xtiming = synth->timing->data;

	generic_carac_t* carac = carac_new();
	carac->timings->delay.clock_edges = 1;

	// Add the size of all the registers
	carac->resources->ff = cells_nb * width;

	// Get the size of the logic block in front of each register cell
	generic_carac_t* carac_logicblock = carac_new();
	if(total_ports_w>=2) {
		xil_muxd_implem mux_data;
		Xil_MuxDirect_GetImplem(synth, total_ports_w, &mux_data);
		carac_logicblock->resources->lut6 += mux_data.lut6_nb * width;
		// Add one LUT to merge the Write Enable inputs (FIXME it's arbitrary, use a constant comparator model)
		carac_logicblock->resources->lut6 += 1;
		// The main delay is the big MUX
		carac->timings->delay.before_clk = mux_data.delay + xtiming->wire_delay + GetMax(xtiming->FF_bef_wd, xtiming->FF_bef_ce);
	}
	if(ports_w>0) {
		generic_carac_t* carac_cmp = GetCarac_Cmp_cst(synth, addr_width);
		carac_add_resources(carac_logicblock, carac_cmp, ports_w);
		if(total_ports_w==1) {
			// The delay before clock is the CMP that leads to the register Write Enable.
			carac->timings->delay.before_clk = carac_cmp->timings->delay.delay + xtiming->wire_delay;
		}
		carac_free(carac_cmp);
	}

	// Add the resources of all logic blocks
	carac_add_resources(carac, carac_logicblock, cells_nb);
	carac_free(carac_logicblock);

	// Now handle the Read part

	// The delay is the one of the big Read MUX, if present
	// FIXME here we should have 2 cases : read via a direct port, or via an Address port...
	//   This leads to data structures specialized by component...

	if(ports_r>0) {
		generic_carac_t* carac_mux = GetCarac_Mux_Binary(synth, cells_nb, width);
		carac_add_resources(carac, carac_mux, ports_r);
		carac->timings->delay.after_clk = xtiming->FF_aft_rd + xtiming->wire_delay + carac_mux->timings->delay.delay;
		carac->timings->delay.delay = carac_mux->timings->delay.delay;
		carac_free(carac_mux);
	}
	else {
		carac->timings->delay.after_clk = xtiming->FF_aft_rd;
	}

	return carac;
}

// PingPong Buffer
static generic_carac_t* GetCarac_PingPong(synth_target_t* synth, netlist_comp_t* comp) {
	int i;
	netlist_pingpong_t* pp_data = comp->data;

	xilinx_timing_t* xtiming = synth->timing->data;
	generic_carac_t* carac = carac_new();

	bool is_input = false;
	if(comp->model==NETLIST_COMP_PINGPONG_IN) is_input = true;

	bool simplify = (pp_data->port_full_c == NULL && pp_data->port_ratio_c == NULL)
		|| (pp_data->port_full_n == NULL && pp_data->port_ratio_n == NULL);

	if(pp_data->data_width==0) {
		fprintf(stderr, "ERROR %s : width=%u\n", __func__, pp_data->data_width);
		abort();
	}
	if(pp_data->cells_nb==0) {
		fprintf(stderr, "ERROR %s : cells_nb=%u\n", __func__, pp_data->cells_nb);
		abort();
	}
	if(pp_data->addr_width==0) {
		fprintf(stderr, "ERROR %s : addr_width=%u\n", __func__, pp_data->addr_width);
		abort();
	}

	// A simple case: the PP is implemented as 2 banks of registers
	if(pp_data->is_direct && pp_data->banks_nb==2) {

		// The main storage
		carac->resources->ff += pp_data->banks_nb * pp_data->cells_nb * pp_data->data_width;

		// The counters
		unsigned width_fill = uint_bitsnb(pp_data->cells_nb);
		carac->resources->ff += width_fill;
		if(is_input==false && (pp_data->port_ratio_c!=NULL || pp_data->port_full_c!=NULL)) carac->resources->ff += width_fill;

		generic_carac_t* carac_tmp;

		// The add, or sub for the fifo counter
		carac_tmp = GetCarac_Add_Sub(synth, width_fill);
		carac_add_resources(carac, carac_tmp, 1);
		carac_free(carac_tmp);

		// Comparison of counter values (with cells_nb for in, zero for out)
		carac_tmp = GetCarac_Cmp_cst(synth, width_fill);
		carac_add_resources(carac, carac_tmp, 1);
		if(is_input==true && pp_data->port_full_c!=NULL) carac_add_resources(carac, carac_tmp, 1);
		carac_free(carac_tmp);

		// The big MUX for output FIFO
		if(is_input==false) {
			xil_muxd_implem mux_data;
			Xil_MuxDirect_GetImplem(synth, pp_data->cells_nb, &mux_data);
			carac->resources->lut6 += mux_data.lut6_nb * pp_data->data_width;
		}
		// The little comparators for regs of FIFO bank for input FIFO
		if(is_input==false) {
			carac_tmp = GetCarac_Cmp_cst(synth, width_fill);
			carac_add_resources(carac, carac_tmp, pp_data->cells_nb);
			carac_free(carac_tmp);
		}

		// For input PP, the little MUX for regs that also have circuit-side write
		unsigned nb_circuit_writes = 0;
		avl_ip_foreach(&pp_data->access_ports_d, scan) {
			netlist_access_t* access = scan->data;
			netlist_access_mem_direct_t* memd_data = access->data;
			if(memd_data->port_wd==NULL) continue;
			nb_circuit_writes++;
		}
		if(nb_circuit_writes>0) {
			xil_muxd_implem mux_data;
			Xil_MuxDirect_GetImplem(synth, 2, &mux_data);
			carac->resources->lut6 += mux_data.lut6_nb * pp_data->data_width * nb_circuit_writes;
		}

		// Timings

		// FIXME missing timings

		return carac;
	}

	// For one RAM in the component PingPong
	if(is_input == true){
		carac = GetCarac_Multiport(synth,
			pp_data->cells_nb, pp_data->data_width, pp_data->addr_width,
			pp_data->ports_wa_nb + 1, pp_data->ports_ra_nb, false
		);  // +1 is the input fifo's side
	}
	else{
		carac = GetCarac_Multiport(synth,
			pp_data->cells_nb, pp_data->data_width, pp_data->addr_width,
			pp_data->ports_wa_nb, pp_data->ports_ra_nb + 1, false
		); // +1 is the output fifo's side
	}

	generic_carac_t* carac_mux;

	// If it is an output ping-pong buffer
	if(is_input == false){
		// For the mux on write_enable of RAM
		carac_mux = GetCarac_Mux_Binary(synth, 2, 1);
		carac_add_resources(carac, carac_mux, 1);
		carac->timings->delay.after_clk = xtiming->FF_aft_rd + xtiming->wire_delay + carac_mux->timings->delay.delay;
		carac->timings->delay.delay = carac_mux->timings->delay.delay;
		carac_free(carac_mux);
	}

	carac_mult_resources(carac,pp_data->banks_nb); // multiplying the needed ressources by the number of RAM in the buffer

	// For the increment of writing address
	generic_carac_t* carac_cmp = GetCarac_Add_Sub(synth, pp_data->addr_width);
	carac_add_resources(carac, carac_cmp, 1);
	carac->timings->delay.before_clk = carac_cmp->timings->delay.delay + xtiming->wire_delay;
	carac_free(carac_cmp);

	carac_cmp = GetCarac_FF(synth, pp_data->addr_width);
	carac_add_resources(carac, carac_cmp, 1);
	carac->timings->delay.before_clk = carac_cmp->timings->delay.delay + xtiming->wire_delay;
	carac_free(carac_cmp);

	// For the comparison for the full information
	carac_cmp = GetCarac_Cmp_cst(synth, pp_data->data_width);
	carac_add_resources(carac, carac_cmp, 1);
	carac->timings->delay.before_clk = carac_cmp->timings->delay.delay + xtiming->wire_delay;
	carac_free(carac_cmp);

	// only one counter and one comparator are needed but 3 muxes have to be added
	if(simplify == true){
		carac_mux = GetCarac_Mux_Binary(synth, 2, 1);
		carac_add_resources(carac, carac_mux, 1);
		carac->timings->delay.after_clk = xtiming->FF_aft_rd + xtiming->wire_delay + carac_mux->timings->delay.delay;
		carac->timings->delay.delay = carac_mux->timings->delay.delay;
		carac_free(carac_mux);

		carac_mux = GetCarac_Mux_Binary(synth, 2, 1);
		carac_add_resources(carac, carac_mux, 1);
		carac->timings->delay.after_clk = xtiming->FF_aft_rd + xtiming->wire_delay + carac_mux->timings->delay.delay;
		carac->timings->delay.delay = carac_mux->timings->delay.delay;
		carac_free(carac_mux);

		carac_mux = GetCarac_Mux_Binary(synth, 2, 1);
		carac_add_resources(carac, carac_mux, 1);
		carac->timings->delay.after_clk = xtiming->FF_aft_rd + xtiming->wire_delay + carac_mux->timings->delay.delay;
		carac->timings->delay.delay = carac_mux->timings->delay.delay;
		carac_free(carac_mux);
	}
	// an other counter and an other comparator are need
	else{
		// For the increment of writing address
		carac_cmp = GetCarac_Add_Sub(synth, pp_data->addr_width);
		carac_add_resources(carac, carac_cmp, 1);
		carac->timings->delay.before_clk = carac_cmp->timings->delay.delay + xtiming->wire_delay;
		carac_free(carac_cmp);

		carac_cmp = GetCarac_FF(synth, pp_data->addr_width);
		carac_add_resources(carac, carac_cmp, 1);
		carac->timings->delay.before_clk = carac_cmp->timings->delay.delay + xtiming->wire_delay;
		carac_free(carac_cmp);

		// For the comparison for the full information
		carac_cmp = GetCarac_Cmp_cst(synth, pp_data->data_width);
		carac_add_resources(carac, carac_cmp, 1);
		carac->timings->delay.before_clk = carac_cmp->timings->delay.delay + xtiming->wire_delay;
		carac_free(carac_cmp);
	}


	// For the mux for full1 selection
	if(pp_data->port_full_c!= NULL){
		carac_mux = GetCarac_Mux_Binary(synth, 2, 1);
		carac_add_resources(carac, carac_mux, 1);
		carac->timings->delay.after_clk = xtiming->FF_aft_rd + xtiming->wire_delay + carac_mux->timings->delay.delay;
		carac->timings->delay.delay = carac_mux->timings->delay.delay;
		carac_free(carac_mux);
	}

	// For the mux for full2 selection
	if(pp_data->port_full_c!= NULL){
		carac_mux = GetCarac_Mux_Binary(synth, 2, 1);
		carac_add_resources(carac, carac_mux, 1);
		carac->timings->delay.after_clk = xtiming->FF_aft_rd + xtiming->wire_delay + carac_mux->timings->delay.delay;
		carac->timings->delay.delay = carac_mux->timings->delay.delay;
		carac_free(carac_mux);
	}

	// For the mux for ratio1 selection
	if(pp_data->port_ratio_n!= NULL){
		carac_mux = GetCarac_Mux_Binary(synth, 2, pp_data->addr_width);
		carac_add_resources(carac, carac_mux, 1);
		carac->timings->delay.after_clk = xtiming->FF_aft_rd + xtiming->wire_delay + carac_mux->timings->delay.delay;
		carac->timings->delay.delay = carac_mux->timings->delay.delay;
		carac_free(carac_mux);
	}

	// For the mux for ratio2 selection
	if(pp_data->port_full_n!= NULL){
		carac_mux = GetCarac_Mux_Binary(synth, 2, pp_data->addr_width);
		carac_add_resources(carac, carac_mux, 1);
		carac->timings->delay.after_clk = xtiming->FF_aft_rd + xtiming->wire_delay + carac_mux->timings->delay.delay;
		carac->timings->delay.delay = carac_mux->timings->delay.delay;
		carac_free(carac_mux);
	}

	generic_carac_t* carac_tot_ra=carac_new();

	// For the mux for output from each access
	for(i=0; i<pp_data->ports_ra_nb+1 ; i++) 	{
		carac_mux = GetCarac_Mux_Binary(synth, 2, pp_data->data_width);
		carac_add_resources(carac_tot_ra, carac_mux, 1);
		carac_tot_ra->timings->delay.after_clk = xtiming->FF_aft_rd + xtiming->wire_delay + carac_mux->timings->delay.delay;
		carac_tot_ra->timings->delay.delay = carac_mux->timings->delay.delay;
		carac_free(carac_mux);
	}

	// For the mux of the FIFO's output of output ping-pong buffer
	if(is_input == false) {
		carac_mux = GetCarac_Mux_Binary(synth, 2, pp_data->data_width);
		carac_add_resources(carac_tot_ra, carac_mux, 1);
		carac_tot_ra->timings->delay.after_clk = xtiming->FF_aft_rd + xtiming->wire_delay + carac_mux->timings->delay.delay;
		carac_tot_ra->timings->delay.delay = carac_mux->timings->delay.delay;
		carac_free(carac_mux);
	}

	carac_add_resources(carac, carac_tot_ra, 1);
	carac_free(carac_tot_ra);

	// For the internal FSM
	if(simplify == true) carac->resources->ff += pp_data->banks_nb + 4 + 1; // 1 per state + 1 to delay the full signal
	else                 carac->resources->ff += pp_data->banks_nb + 4 + 2; // 1 per state + 2 to delay the full signals
	carac->resources->lut6 += 6; // 1 per state

	return carac;
}



//======================================================================
// Eval size of netlist components
//======================================================================

typedef ptype_list* (*type_cb_size)(void* synth, netlist_comp_t* comp);

// Evaluate the HW resources for each component type
static ptype_list* Xilinx_EvalSize_Comp_Top(synth_target_t* synth, netlist_comp_t* comp) {
	return Techno_EvalSize_addrecurs(NULL, synth, comp);
}

static ptype_list* Xilinx_EvalSize_Comp_Sig(synth_target_t* synth, netlist_comp_t* comp) {
	return NULL;
}

static ptype_list* Xilinx_EvalSize_Comp_FSM_OneHot(synth_target_t* synth, netlist_comp_t* comp) {
	netlist_fsm_t* comp_fsm = comp->data;

	generic_carac_t* carac = carac_new();

	// Compute the size of the state register
	carac->resources->ff += comp_fsm->states_nb;
	if(comp_fsm->outputs_buf==true) {
		// The size of the buffers for outputs
		avl_pp_foreach(&comp_fsm->output_values, scanOut) {
			netlist_port_t* port = scanOut->key;
			carac->resources->ff += port->width;
		}
	}

	// The logic to change state
	// FIXME Assume 1 LUT per state
	carac->resources->lut6 += comp_fsm->states_nb;

	// Generation of the outputs
	avl_pp_foreach(&comp_fsm->output_values, scanOut) {
		netlist_port_t* port = scanOut->key;
		netlist_fsm_outval_t* outval = scanOut->data;

		unsigned number_actions = ChainList_Count(outval->actions);
		if(number_actions>0) number_actions++;  // Include the default value

		// Approximation : the logic is similar to a big OR of actions
		// FIXME For buffered outputs, take into account all previous states
		generic_carac_t *carac_mux = GetCarac_Mux_DirectCst(synth, number_actions, port->width);
		carac_add_resources(carac, carac_mux, 1);
		carac_free(carac_mux);
	}

	ptype_list* list = NameRes_FromCarac(carac->resources);
	carac_free(carac);
	return list;
}

static ptype_list* Xilinx_EvalSize_Comp_Reg(synth_target_t* synth, netlist_comp_t* comp) {
	netlist_register_t* comp_reg = comp->data;
	generic_carac_t* carac = GetCarac_FF(synth, comp_reg->width);
	ptype_list* list = NameRes_FromCarac(carac->resources);
	carac_free(carac);
	return list;
}

static ptype_list* Xilinx_EvalSize_Comp_Mux(synth_target_t* synth, netlist_comp_t* comp) {
	netlist_mux_t* comp_mux = comp->data;
	xilinx_res_t* xres = Xilinx_Res_New();
	xil_muxd_implem mux_implem;
	Xil_MuxDirect_GetImplem(synth, comp_mux->inputs_nb, &mux_implem);
	xres->lut6 += mux_implem.lut6_nb * comp_mux->width;
	ptype_list* list = NameRes_FromCarac(xres);
	Xilinx_Res_Free(xres);
	return list;
}

static ptype_list* Xilinx_EvalSize_Comp_MuxBin(synth_target_t* synth, netlist_comp_t* comp) {
	netlist_muxbin_t* comp_muxbin = comp->data;
	// FIXME Using a MuxImplem structure like done for MuxDirect would be much better
	generic_carac_t* carac = GetCarac_Mux_Binary(synth, comp_muxbin->inputs_nb, comp_muxbin->width_data);
	ptype_list* list = NameRes_FromCarac(carac->resources);
	carac_free(carac);
	return list;
}

static ptype_list* Xilinx_EvalSize_Comp_Memory(synth_target_t* synth, netlist_comp_t* comp) {
	netlist_memory_t* comp_mem = comp->data;

	generic_carac_t* carac = NULL;
	bool sync_found = Netlist_Mem_IsSimpleSyncRead(comp);

	if(sync_found==true) {
		carac = GetCarac_Bram36k(synth, comp_mem->cells_nb, comp_mem->data_width);
		carac_mult_resources(carac, comp_mem->ports_ra_nb);
	}
	else {
		carac = GetCarac_Multiport(synth,
			comp_mem->cells_nb, comp_mem->data_width, comp_mem->addr_width,
			comp_mem->ports_wa_nb, comp_mem->ports_ra_nb, comp_mem->direct_en
		);
	}

	ptype_list* list = NameRes_FromCarac(carac->resources);
	carac_free(carac);
	return list;
}

static ptype_list* Xilinx_EvalSize_Comp_PingPong(synth_target_t* synth, netlist_comp_t* comp) {
	generic_carac_t* carac;
	carac = GetCarac_PingPong(synth, comp);
	ptype_list* list = NameRes_FromCarac(carac->resources);
	carac_free(carac);
	return list;
}

static ptype_list* Xilinx_EvalSize_Comp_Asb(synth_target_t* synth, netlist_comp_t* comp) {
	netlist_asb_t* comp_asb = comp->data;
	generic_carac_t* carac = GetCarac_Add_Sub(synth, comp_asb->width);
	if(comp_asb->sign_extend!=NULL) {
		netlist_asb_sign_t* asb_sign = comp_asb->sign_extend;
		if(asb_sign->port_ov!=NULL) carac->resources->lut6 ++;
		if(asb_sign->port_gt!=NULL) carac->resources->lut6 ++;
		if(asb_sign->port_ge!=NULL) carac->resources->lut6 ++;
		if(asb_sign->port_lt!=NULL) carac->resources->lut6 ++;
		if(asb_sign->port_le!=NULL) carac->resources->lut6 ++;
		// For some operations, an embedded comparator is needed
		if(asb_sign->port_ge!=NULL || asb_sign->port_lt!=NULL) {
			generic_carac_t* carac_sign = GetCarac_Cmp_eq(synth, comp_asb->width);
			carac_add_resources(carac, carac_sign, 1);
			carac_free(carac_sign);
		}
	}
	ptype_list* list = NameRes_FromCarac(carac->resources);
	carac_free(carac);
	return list;
}

static ptype_list* Xilinx_EvalSize_Comp_Mul(synth_target_t* synth, netlist_comp_t* comp) {
	netlist_mul_t* comp_mul = comp->data;
	generic_carac_t* carac = GetCarac_Mul(synth, comp_mul->width_inA, comp_mul->width_inB, comp_mul->width_out, comp_mul->is_signed);
	ptype_list* list = NameRes_FromCarac(carac->resources);
	carac_free(carac);
	return list;
}

static ptype_list* Xilinx_EvalSize_Comp_DivQR(synth_target_t* synth, netlist_comp_t* comp) {
	netlist_divqr_t* comp_div = comp->data;
	generic_carac_t* carac = GetCarac_DivQR(synth, comp_div->width_num, comp_div->width_den);
	ptype_list* list = NameRes_FromCarac(carac->resources);
	carac_free(carac);
	return list;
}

// FIXME This component is always evaluated as 2-inputs
static ptype_list* Xilinx_EvalSize_Comp_Cmp(synth_target_t* synth, netlist_comp_t* comp) {
	netlist_cmp_t* comp_cmp = comp->data;
	generic_carac_t* carac = GetCarac_Cmp_eq(synth, comp_cmp->width);
	ptype_list* list = NameRes_FromCarac(carac->resources);
	carac_free(carac);
	return list;
}

static ptype_list* Xilinx_EvalSize_Comp_Logic(synth_target_t* synth, netlist_comp_t* comp) {
	netlist_logic_t* comp_logic = comp->data;
	generic_carac_t* carac = GetCarac_Logic(synth, comp_logic->inputs_nb, comp_logic->width);
	ptype_list* list = NameRes_FromCarac(carac->resources);
	carac_free(carac);
	return list;
}

static ptype_list* Xilinx_EvalSize_Comp_Not(synth_target_t* synth, netlist_comp_t* comp) {
	netlist_logic_t* comp_logic = comp->data;
	generic_carac_t* carac = GetCarac_Not(synth, comp_logic->width);
	ptype_list* list = NameRes_FromCarac(carac->resources);
	carac_free(carac);
	return list;
}

#if 0  // Not used yet
static ptype_list* Xilinx_EvalSize_Comp_LU(synth_target_t* synth, netlist_comp_t* comp) {
	netlist_logic_t* comp_logic = comp->data;
	generic_carac_t* carac = GetCarac_LU(synth, comp_logic->width);
	ptype_list* list = NameRes_FromCarac(carac->resources);
	carac_free(carac);
	return list;
}
#endif

static ptype_list* Xilinx_EvalSize_Comp_ShiftRot(synth_target_t* synth, netlist_comp_t* comp) {
	netlist_shift_t* comp_shift = comp->data;
	generic_carac_t* carac = GetCarac_Rot(synth, comp_shift->width_shift, comp_shift->width_data);
	ptype_list* list = NameRes_FromCarac(carac->resources);
	carac_free(carac);
	return list;
}

static ptype_list* Xilinx_EvalSize_Comp_ClkDiv(synth_target_t* synth, netlist_comp_t* comp) {
	netlist_clkdiv_t* clk_data = comp->data;
	xilinx_res_t* xres = Xilinx_Res_New();

	// The output register + input logic
	xres->ff += 1;
	xres->lut6 += 1;

	// The counter
	unsigned cnt_width = uint_bitsnb((clk_data->divider/2)-1);
	xres->ff += cnt_width;
	// Logic in front of the register
	xres->lut6 += cnt_width;
	// The adder
	xres->lut6 += cnt_width;

	// The comparator
	generic_carac_t* carac_tmp = GetCarac_Cmp_cst(synth, cnt_width);
	add_resources(xres, carac_tmp->resources, 2);
	carac_free(carac_tmp);

	ptype_list* list = NameRes_FromCarac(xres);
	Xilinx_Res_Free(xres);

	return list;
}

static ptype_list* Xilinx_EvalSize_Comp_Uart(synth_target_t* synth, netlist_comp_t* comp) {
	netlist_uart_t* uart_data = comp->data;
	xilinx_res_t* xres = Xilinx_Res_New();

	generic_carac_t* carac_tmp = NULL;

	// The clock dividers

	// The register
	unsigned clkdiv_width = uint_bitsnb(uart_data->clk_divider-1);
	xres->ff += clkdiv_width * 2;
	// The adders
	xres->lut6 += clkdiv_width * 2;
	// The comparators (equality): compare with N-1 and N/2
	carac_tmp = GetCarac_Cmp_cst(synth, clkdiv_width);
	add_resources(xres, carac_tmp->resources, 2);
	carac_free(carac_tmp);
	// The comparator (inequality) with clkdiv/2, SUB component, RX side only
	xres->lut6 += clkdiv_width;

	// The registers for the data bits
	xres->ff += uart_data->data_width * 2;
	// Some logic in front of this reg to implement shift register
	xres->lut6 += uart_data->data_width * 2;

	// The counters for the number of bits
	unsigned bitcnt_width = uint_bitsnb(uart_data->data_width-1);
	xres->ff += bitcnt_width * 2;
	// The adders
	xres->lut6 += bitcnt_width * 2;
	// The comparators: compare with N-1
	carac_tmp = GetCarac_Cmp_cst(synth, bitcnt_width);
	add_resources(xres, carac_tmp->resources, 2);
	carac_free(carac_tmp);

	// The parity bits
	if(uart_data->parity==true) {
		xres->ff += 2;
		xres->lut6 += 2;
	}

	// The 'available' flags
	xres->ff += 2;
	xres->lut6 += 2;

	// The FSMs
	// Assume binary encoding, 2 bits per FSM, one LUT in front of each bit
	xres->ff += 2 * 2;
	xres->lut6 += 2 * 2;

	ptype_list* list = NameRes_FromCarac(xres);
	Xilinx_Res_Free(xres);

	return list;
}

static ptype_list* Xilinx_EvalSize_Comp_Fifo(synth_target_t* synth, netlist_comp_t* comp) {
	netlist_fifo_t* fifo_data = comp->data;
	if(fifo_data->cells_nb==0) return NULL;

	xilinx_res_t* xres = Xilinx_Res_New();

	generic_carac_t* carac_tmp = NULL;
	unsigned idx_width = uint_bitsnb(fifo_data->cells_nb-1);

	// The memory
	carac_tmp = GetCarac_Multiport(synth,
		fifo_data->cells_nb, fifo_data->data_width, idx_width, 1, 1, false
	);
	add_resources(xres, carac_tmp->resources, 1);
	carac_free(carac_tmp);

	// The indexes
	xres->ff += idx_width * 2;
	// The adders
	xres->lut6 += idx_width * 2;
	// The comparators (equality): compare with N-1
	carac_tmp = GetCarac_Cmp_cst(synth, idx_width);
	add_resources(xres, carac_tmp->resources, 2);
	carac_free(carac_tmp);

	// The comparator (equality): compare the two next indexes
	carac_tmp = GetCarac_Cmp_eq(synth, idx_width);
	add_resources(xres, carac_tmp->resources, 1);
	carac_free(carac_tmp);

	// The logic to generate mem write enable
	xres->lut6 += 1;
	// The logic to generate rdy signal of fifos
	xres->lut6 += 2;
	// The logic for the flag for empty mem
	xres->lut6 += 1;

	ptype_list* list = NameRes_FromCarac(xres);
	Xilinx_Res_Free(xres);

	return list;
}



//======================================================================
// Propagate delays in components
//======================================================================

typedef void (*type_cb_delay)(void* data, netlist_port_t* port, double *delays);
typedef void (*type_cb_delay_wc)(void* data, netlist_comp_t* comp, double* delay, double* delay_we);
typedef void (*type_cb_delay_wa)(void* data, netlist_access_t* access, double* delay, double* delay_we);
typedef void (*type_cb_delay_tr)(void* data, netlist_comp_t* comp, double* delay);

static void Xilinx_PropagDelay_Comp_Sig(techno_delay_t* data, netlist_port_t* port, double* delays) {
	netlist_signal_t* sig_data = port->component->data;
	// Compute delays from the input port
	double* delays_in = Techno_Delay_PortDelays_GetAdd(data, sig_data->port_in);
	Techno_Delay_Process_PortIn(data, sig_data->port_in, delays_in);
	// Save delays
	// Note: The wire delay is assumed to be added at the source non-signal component
	for(unsigned i=0; i<port->width; i++) delays[i] = delays_in[i];
}

static void Xilinx_PropagDelay_Comp_Mux(techno_delay_t* data, netlist_port_t* port, double* delays) {
	netlist_mux_t* mux_data = port->component->data;
	synth_target_t* synth = data->Implem->synth_target;
	xilinx_timing_t* xtiming = synth->timing->data;
	// Get the max delay to ALL enable ports (let time for them to come back to zero, or to be set to 1)
	double max_delay_en = 0;
	avl_pp_foreach(&port->component->accesses, scanAccess) {
		netlist_access_t* access = scanAccess->data;
		netlist_mux_access_t* muxIn_data = access->data;
		// Process the Enable port
		if(muxIn_data->port_en!=NULL) {  // Check in case it's the default data source
			double* delays_en = Techno_Delay_PortDelays_GetAdd(data, muxIn_data->port_en);
			Techno_Delay_Process_PortIn(data, muxIn_data->port_en, delays_en);
			double d = delays_en[0];
			if(d>max_delay_en) max_delay_en = d;
		}
		// Process the data port
		if(avl_p_isthere(&data->curstate->mux_inputs, access)==true) {
			double* delays_data = Techno_Delay_PortDelays_GetAdd(data, muxIn_data->port_data);
			Techno_Delay_Process_PortIn(data, muxIn_data->port_data, delays_data);
			for(unsigned i=0; i<port->width; i++) delays[i] = GetMax(delays[i], delays_data[i]);
		}
	}
	// Add the delay on the Enable inputs and internal MUX delay
	for(unsigned i=0; i<port->width; i++) delays[i] = GetMax(delays[i], max_delay_en);
	// Get the internal Mux delay
	xil_muxd_implem mux_implem;
	Xil_MuxDirect_GetImplem(synth, mux_data->inputs_nb, &mux_implem);
	for(unsigned i=0; i<port->width; i++) delays[i] += mux_implem.delay + xtiming->wire_delay;
}

static void Xilinx_PropagDelay_Comp_MuxBin(techno_delay_t* data, netlist_port_t* port, double* delays) {
	netlist_muxbin_t* muxbin_data = port->component->data;
	synth_target_t* synth = data->Implem->synth_target;
	xilinx_timing_t* xtiming = synth->timing->data;
	// Get the max delay to the selection input
	double max_delay_sel = 0;
	double* delays_sel = Techno_Delay_PortDelays_GetAdd(data, muxbin_data->port_sel);
	Techno_Delay_Process_PortIn(data, muxbin_data->port_sel, delays_sel);
	for(unsigned i=0; i<muxbin_data->width_sel; i++) max_delay_sel = GetMax(delays_sel[i], max_delay_sel);
	// Initialize the output delays to the selection delay
	for(unsigned i=0; i<port->width; i++) delays[i] = max_delay_sel;
	// Get the max delay for each bit and each data input
	for(unsigned idxin=0; idxin<muxbin_data->inputs_nb; idxin++) {
		netlist_port_t* loc_port = muxbin_data->ports_datain[idxin];
		double* delays_datain = Techno_Delay_PortDelays_GetAdd(data, loc_port);
		Techno_Delay_Process_PortIn(data, loc_port, delays_datain);
		for(unsigned i=0; i<port->width; i++) delays[i] = GetMax(delays[i], delays_datain[i]);
	}
	// Get the internal Mux delay
	// FIXME using a MuxImplem structure like for MuxDirect would be better
	generic_carac_t* carac_tmp = GetCarac_Mux_Binary(synth, muxbin_data->inputs_nb, muxbin_data->width_data);
	double d = carac_tmp->timings->delay.delay;
	carac_free(carac_tmp);
	// Add the internal delay + wire
	for(unsigned i=0; i<port->width; i++) delays[i] += d + xtiming->wire_delay;
}

static void Xilinx_PropagDelay_Comp_Reg(techno_delay_t* data, netlist_port_t* port, double* delays) {
	xilinx_timing_t* xtiming = data->Implem->synth_target->timing->data;
	for(unsigned i=0; i<port->width; i++) delays[i] = xtiming->FF_aft_rd + xtiming->wire_delay;
}

static void Xilinx_PropagDelay_Comp_Fsm(techno_delay_t* data, netlist_port_t* port, double* delays) {
	xilinx_timing_t* xtiming = data->Implem->synth_target->timing->data;
	netlist_fsm_outval_t* outval = Netlist_Comp_Fsm_GetOutVal(port);
	double d = 0;
	if(outval==NULL) {
		printf("Warning: Missing Outval structure for port '%s' of FSM\n", port->name);
		// Arbitrary delay: Reg setup + wire delay
		d = xtiming->FF_aft_rd;
	}
	else {
		if(outval->is_buffered==true) {
			d = xtiming->FF_aft_rd;
		}
		else {
			// Take into account the number of States that drive the output
			// Get the delay of the associated logic
			unsigned states_nb = ChainList_Count(outval->actions);
			unsigned lut6_levels = 0;
			for(unsigned i=states_nb; i>1; i/=6) lut6_levels++;
			d = xtiming->FF_aft_rd + lut6_levels * (xtiming->wire_delay + xtiming->lut6_delay);
			// Take into account the retiming couter, if any
			if(data->curstate->state->rtm_counter!=NULL) {
				d += xtiming->wire_delay + xtiming->lut6_delay;
			}
		}
	}
	d += xtiming->wire_delay;
	for(unsigned i=0; i<port->width; i++) delays[i] = d;
}

static void Xilinx_PropagDelay_Comp_Mem(techno_delay_t* data, netlist_port_t* port, double* delays) {
	netlist_memory_t* mem_data = port->component->data;
	synth_target_t* synth = data->Implem->synth_target;
	xilinx_timing_t* xtiming = synth->timing->data;
	netlist_access_t* access = port->access;

	bool sync_found = Netlist_Mem_IsSimpleSyncRead(port->component);

	double d = 0;  // Uniform delay value

	if(access==NULL) {
		printf("Warning: Port '%s' of component '%s' model '%s' is not associated to an access structure.\n",
			port->name, port->component->name, port->component->model->name
		);
		// Arbitrary delay: Reg setup + wire delay
		d = xtiming->FF_aft_rd;
	}

	else if(access->model==NETLIST_ACCESS_MEM_RA) {
		netlist_access_mem_addr_t* memA_data = access->data;
		// Get address port delays
		double* delays_addr = Techno_Delay_PortDelays_GetAdd(data, memA_data->port_a);
		Techno_Delay_Process_PortIn(data, memA_data->port_a, delays_addr);
		for(unsigned i=0; i<memA_data->port_a->width; i++) d = GetMax(d, delays_addr[i]);

		// Get the component delay details
		generic_carac_t* carac_tmp = NULL;
		if(sync_found==true) {
			carac_tmp = GetCarac_Bram36k(synth, mem_data->cells_nb, mem_data->data_width);
			carac_mult_resources(carac_tmp, mem_data->ports_ra_nb);
		}
		else {
			carac_tmp = GetCarac_Multiport(synth,
				mem_data->cells_nb, mem_data->data_width, mem_data->addr_width,
				mem_data->ports_wa_nb, mem_data->ports_ra_nb, mem_data->direct_en
			);
		}

		// Add internal combinatorial read delay
		d += carac_tmp->timings->delay.delay;
		// Handle sequential delay
		d = GetMax(d, carac_tmp->timings->delay.after_clk);
		// Clean
		carac_free(carac_tmp);
	}

	else if(access->model==NETLIST_ACCESS_MEM_D) {
		// FIXME Handle when it's a read or a write, handle comparator before the REG when writing, handle WD vs WE...
		d = xtiming->FF_aft_rd;
	}

	else {
		printf("Warning: Port '%s' of component '%s' model '%s' is part of access '%s' model '%s' which is not handled.\n",
			port->name, port->component->name, port->component->model->name, access->name, access->model->name
		);
		// Arbitrary delay: Reg setup + wire delay
		d = xtiming->FF_aft_rd;
	}

	// Set the delay value
	d += xtiming->wire_delay;
	for(unsigned i=0; i<port->width; i++) delays[i] = d;
}

static void Xilinx_PropagDelay_Comp_Asb(techno_delay_t* data, netlist_port_t* port, double* delays) {
	xilinx_timing_t* xtiming = data->Implem->synth_target->timing->data;
	netlist_comp_t* comp = port->component;
	netlist_asb_t* asb_data = comp->data;
	// Input A
	double* delays_A = Techno_Delay_PortDelays_GetAdd(data, asb_data->port_inA);
	Techno_Delay_Process_PortIn(data, asb_data->port_inA, delays_A);
	// Input B
	double* delays_B = Techno_Delay_PortDelays_GetAdd(data, asb_data->port_inB);
	Techno_Delay_Process_PortIn(data, asb_data->port_inB, delays_B);
	// Carry input
	double delay_carry = 0;
	if(asb_data->port_ci!=NULL) {
		double* delays_ci = Techno_Delay_PortDelays_GetAdd(data, asb_data->port_ci);
		Techno_Delay_Process_PortIn(data, asb_data->port_ci, delays_ci);
		delay_carry = delays_ci[0];
	}
	// The output port
	double* delays_out = delays;
	if(port!=asb_data->port_out) {
		delays_out = Techno_Delay_PortDelays_GetAdd(data, asb_data->port_out);
	}
	// Compute all delays along the carry chain
	for(unsigned i=0; i<asb_data->width; i++) {
		if(i>0) delay_carry += xtiming->CARRY_CI_delay;
		double delay_maxin = GetMax(delays_A[i], delays_B[i]) + xtiming->lut6_delay;
		delays_out[i] = GetMax(delay_maxin, delay_carry) + xtiming->XORCY_CI_delay + xtiming->wire_delay;
		delay_carry = GetMax(delay_carry, delay_maxin + xtiming->CARRY_S_delay);
	}
	// Set the CO port
	if(asb_data->port_co!=NULL) {
		double* delays_co = delays;
		if(port!=asb_data->port_co) {
			delays_co = Techno_Delay_PortDelays_GetAdd(data, asb_data->port_co);
		}
		delays_co[0] = delay_carry + xtiming->CARRY_CI_delay + xtiming->wire_delay;
	}
	// Sign extension!
	if(asb_data->sign_extend!=NULL) {
		netlist_asb_sign_t* sign_extend = asb_data->sign_extend;
		// Handle delay of sign input
		double delay_sign = 0;
		if(sign_extend->port_sign!=NULL) {
			double* delays_sign = Techno_Delay_PortDelays_GetAdd(data, sign_extend->port_sign);
			Techno_Delay_Process_PortIn(data, sign_extend->port_sign, delays_sign);
			delay_sign = delays_sign[0];
		}
		// Handle delay of external comparator input
		double delay_cmp = 0;
		if(sign_extend->port_extern_eq!=NULL) {
			double* delays_cmp = Techno_Delay_PortDelays_GetAdd(data, sign_extend->port_extern_eq);
			Techno_Delay_Process_PortIn(data, sign_extend->port_extern_eq, delays_cmp);
			delay_cmp = delays_cmp[0];
		}
		else {
			for(unsigned i=0; i<asb_data->width; i++) delay_cmp = GetMax(delays_A[i], delays_B[i]);
			// FIXME Arbitrary delay for internal comparator
			// FIXME sometimes this comparator is not needed
			delay_cmp += xtiming->lut6_delay + xtiming->wire_delay;

		}
		// Get the max arrival time of all input bits for computation of sign stuff
		double delay_maxin = GetMax(delay_sign, delay_cmp);
		double delay_maxA = 0;
		for(unsigned i=0; i<asb_data->width; i++) delay_maxA = GetMax(delay_maxA, delays_A[i]);
		delay_maxin = GetMax(delay_maxin, delay_maxA);
		double delay_maxB = 0;
		for(unsigned i=0; i<asb_data->width; i++) delay_maxB = GetMax(delay_maxB, delays_B[i]);
		delay_maxin = GetMax(delay_maxin, delay_maxB);
		double delay_maxout = 0;
		for(unsigned i=0; i<asb_data->width; i++) delay_maxout = GetMax(delay_maxB, delays_out[i]);
		delay_maxin = GetMax(delay_maxin, delay_maxout);
		// Add the LUT delay + wire
		double d = delay_maxin + xtiming->lut6_delay + xtiming->wire_delay;
		// Set the delay to the outputs
		void set_out_val(netlist_port_t* loc_port) {
			if(loc_port==NULL) return;
			double* loc_delays = Techno_Delay_PortDelays_GetAdd(data, loc_port);
			loc_delays[0] = d;
		}
		set_out_val(sign_extend->port_ov);
		set_out_val(sign_extend->port_ge);
		set_out_val(sign_extend->port_gt);
		set_out_val(sign_extend->port_le);
		set_out_val(sign_extend->port_lt);
	}
	// Flag all output ports as processed
	avl_pp_foreach(&comp->ports, scanPort) {
		netlist_port_t* loc_port = scanPort->data;
		avl_p_add_overwrite(&data->ports_used, loc_port, loc_port);
	}
}

static void Xilinx_PropagDelay_Comp_Mul(techno_delay_t* data, netlist_port_t* port, double* delays) {
	netlist_mul_t* mul_data = port->component->data;
	synth_target_t* synth = data->Implem->synth_target;
	xilinx_timing_t* xtiming = synth->timing->data;
	// Input A
	double* delays_A = Techno_Delay_PortDelays_GetAdd(data, mul_data->port_inA);
	Techno_Delay_Process_PortIn(data, mul_data->port_inA, delays_A);
	// Input B
	double* delays_B = Techno_Delay_PortDelays_GetAdd(data, mul_data->port_inB);
	Techno_Delay_Process_PortIn(data, mul_data->port_inB, delays_B);
	// Get the worst delay on all input bits
	double max_delay_in = 0;
	for(unsigned i=0; i<mul_data->port_inA->width; i++) max_delay_in = GetMax(max_delay_in, delays_A[i]);
	for(unsigned i=0; i<mul_data->port_inB->width; i++) max_delay_in = GetMax(max_delay_in, delays_B[i]);
	// Compute the delay
	// FIXME This is a constant delay for all output bits
	generic_carac_t* carac_tmp = GetCarac_Mul(
		synth, mul_data->port_inA->width, mul_data->port_inB->width, mul_data->port_out->width, mul_data->is_signed
	);
	double d = max_delay_in + carac_tmp->timings->delay.delay + xtiming->wire_delay;
	carac_free(carac_tmp);
	// Apply the delay
	for(unsigned i=0; i<port->width; i++) delays[i] = d;
}

static void Xilinx_PropagDelay_Comp_DivQR(techno_delay_t* data, netlist_port_t* port, double* delays) {
	netlist_divqr_t* div_data = port->component->data;
	synth_target_t* synth = data->Implem->synth_target;
	xilinx_timing_t* xtiming = synth->timing->data;
	// Input Num
	double* delays_num = Techno_Delay_PortDelays_GetAdd(data, div_data->port_in_num);
	Techno_Delay_Process_PortIn(data, div_data->port_in_num, delays_num);
	// Input Den
	double* delays_den = Techno_Delay_PortDelays_GetAdd(data, div_data->port_in_den);
	Techno_Delay_Process_PortIn(data, div_data->port_in_den, delays_den);
	// Get the worst delay on all input bits
	double max_delay_in = 0;
	for(unsigned i=0; i<div_data->port_in_num->width; i++) max_delay_in = GetMax(max_delay_in, delays_num[i]);
	for(unsigned i=0; i<div_data->port_in_num->width; i++) max_delay_in = GetMax(max_delay_in, delays_den[i]);
	// Compute the delay
	// FIXME This is a constant delay for all output bits
	generic_carac_t* carac_tmp = GetCarac_DivQR(
		synth, div_data->port_in_num->width, div_data->port_in_den->width
	);
	double d = max_delay_in + carac_tmp->timings->delay.delay + xtiming->wire_delay;
	carac_free(carac_tmp);
	// Apply the delay
	for(unsigned i=0; i<port->width; i++) delays[i] = d;
}

static void Xilinx_PropagDelay_Comp_ShiftRot(techno_delay_t* data, netlist_port_t* port, double* delays) {
	synth_target_t* synth = data->Implem->synth_target;
	xilinx_timing_t* xtiming = synth->timing->data;
	netlist_shift_t* shift_data = port->component->data;
	// Input data
	double* delays_din = Techno_Delay_PortDelays_GetAdd(data, shift_data->port_in);
	Techno_Delay_Process_PortIn(data, shift_data->port_in, delays_din);
	// Input shift
	double* delays_sh = Techno_Delay_PortDelays_GetAdd(data, shift_data->port_shift);
	Techno_Delay_Process_PortIn(data, shift_data->port_shift, delays_sh);
	// Input padding
	double delay_pad = 0;
	if(shift_data->port_padding!=NULL) {
		double* delays_pad = Techno_Delay_PortDelays_GetAdd(data, shift_data->port_padding);
		Techno_Delay_Process_PortIn(data, shift_data->port_padding, delays_pad);
		delay_pad = delays_pad[0];
	}
	// Get the worst delay on all input bits
	// FIXME Using a constant value for all bits is not great
	double max_delay_din = 0;
	for(unsigned i=0; i<shift_data->width_data; i++) max_delay_din = GetMax(max_delay_din, delays_din[i]);
	// Apply the delays
	// Note With LUT6, each stage of the barrel shifter used 2 bits of the shift value
	double d = max_delay_din;
	for(unsigned i=0; i<shift_data->width_shift; i+=2) {
		double loc_max_in = GetMax(delays_sh[i], delay_pad);
		d = GetMax(d, loc_max_in) + xtiming->lut6_delay + xtiming->wire_delay;
	}
	// Apply the delay
	for(unsigned i=0; i<port->width; i++) delays[i] = d;
}

static void Xilinx_PropagDelay_Comp_Logic(techno_delay_t* data, netlist_port_t* port, double* delays) {
	netlist_logic_t* logic_data = port->component->data;
	synth_target_t* synth = data->Implem->synth_target;
	xilinx_timing_t* xtiming = synth->timing->data;
	// Add the delay on the Enable inputs and internal MUX delay
	avl_pp_foreach(&port->component->ports, scanPort) {
		netlist_port_t* loc_port = scanPort->data;
		if(loc_port->direction!=NETLIST_PORT_DIR_IN) continue;
		double* delays_in = Techno_Delay_PortDelays_GetAdd(data, loc_port);
		Techno_Delay_Process_PortIn(data, loc_port, delays_in);
		for(unsigned i=0; i<port->width; i++) delays[i] = GetMax(delays[i], delays_in[i]);
	}
	// Add the internal delay of the component
	unsigned lut6_stages = 0;
	for(unsigned i=logic_data->inputs_nb; i>0; i/=6) lut6_stages++;
	double internal = (xtiming->lut6_delay + xtiming->wire_delay) * lut6_stages;
	for(unsigned i=0; i<port->width; i++) delays[i] += internal;
}

static void Xilinx_PropagDelay_Comp_Cmp(techno_delay_t* data, netlist_port_t* port, double* delays) {
	netlist_cmp_t* cmp_data = port->component->data;
	synth_target_t* synth = data->Implem->synth_target;
	xilinx_timing_t* xtiming = synth->timing->data;
	// Add the delay on the Enable inputs and internal MUX delay
	double max_delay_in = 0;
	avl_pp_foreach(&port->component->ports, scanPort) {
		netlist_port_t* loc_port = scanPort->data;
		if(loc_port->direction!=NETLIST_PORT_DIR_IN) continue;
		double* delays_in = Techno_Delay_PortDelays_GetAdd(data, loc_port);
		Techno_Delay_Process_PortIn(data, loc_port, delays_in);
		for(unsigned i=0; i<port->width; i++) max_delay_in = GetMax(max_delay_in, delays_in[i]);
	}
	// Add the internal delay of the component
	unsigned lut6_stages = 0;
	if(cmp_data->inputs_nb>3) {
		for(unsigned i=cmp_data->inputs_nb; i>0; i/=6) lut6_stages++;
		for(unsigned i=cmp_data->width; i>0; i/=6) lut6_stages++;
	}
	else {
		unsigned bits_per_lut = 6/cmp_data->inputs_nb;
		unsigned bits_to_merge = cmp_data->width / bits_per_lut;
		if(cmp_data->width > bits_to_merge * bits_per_lut) bits_to_merge ++;
		for(unsigned i=bits_to_merge; i>0; i/=6) lut6_stages++;
	}
	double d = max_delay_in + (xtiming->lut6_delay + xtiming->wire_delay) * lut6_stages;
	// Set the result to all outputs of the component
	void set_port_delay(netlist_port_t* loc_port) {
		if(loc_port!=NULL || loc_port!=port) return;
		double* delays_loc = Techno_Delay_PortDelays_GetAdd(data, loc_port);
		delays_loc[0] = d;
		avl_p_add_overwrite(&data->ports_used, loc_port, loc_port);
	}
	set_port_delay(cmp_data->port_eq);
	set_port_delay(cmp_data->port_ne);
	// Set the timings to the current output port
	for(unsigned i=0; i<port->width; i++) delays[i] = d;
}

static void Xilinx_PropagDelay_Comp_Uart(techno_delay_t* data, netlist_port_t* port, double* delays) {
	synth_target_t* synth = data->Implem->synth_target;
	xilinx_timing_t* xtiming = synth->timing->data;
	// All outputs should be buffered
	double d = xtiming->FF_aft_rd + xtiming->wire_delay;
	// Apply the delay
	for(unsigned i=0; i<port->width; i++) delays[i] = d;
}

static void Xilinx_WriteDelay_Reg_Comp(techno_delay_t* data, netlist_comp_t* comp, double* delay, double* delay_we) {
	synth_target_t* synth = data->Implem->synth_target;
	xilinx_timing_t* xtiming = synth->timing->data;
	netlist_register_t* reg_data = comp->data;
	double max_delay_din = Techno_Delay_Process_PortIn_GetMax(data, reg_data->port_in);
	double max_delay_we = Techno_Delay_Process_PortIn_GetMax(data, reg_data->port_en);
	*delay = GetMax(max_delay_din + xtiming->FF_bef_wd, max_delay_we + xtiming->FF_bef_ce);
	*delay_we = max_delay_we + xtiming->FF_bef_ce;
}

static void Xilinx_WriteDelay_Mem_Access(techno_delay_t* data, netlist_access_t* access, double* delay, double* delay_we) {
	synth_target_t* synth = data->Implem->synth_target;
	xilinx_timing_t* xtiming = synth->timing->data;
	netlist_memory_t* mem_data = access->component->data;

	bool sync_found = Netlist_Mem_IsSimpleSyncRead(access->component);

	if(access->model==NETLIST_ACCESS_MEM_WA) {
		netlist_access_mem_addr_t* memA_data = access->data;
		double max_delay_d = Techno_Delay_Process_PortIn_GetMax(data, memA_data->port_d);
		double max_delay_e = Techno_Delay_Process_PortIn_GetMax(data, memA_data->port_e);
		double max_delay_a = Techno_Delay_Process_PortIn_GetMax(data, memA_data->port_e);
		// Set results depending on the actual mem implem
		if(mem_data->ports_wa_nb > 1 || mem_data->direct_en==true) {
			// Implementation as registers
			unsigned nb_ports_w = mem_data->ports_wa_nb;
			if(mem_data->direct_en==true) nb_ports_w++;
			if(mem_data->ports_wa_nb==1 && mem_data->direct_en==false) {
				// There is no big data MUX. Arbitrarily assume only one LUT on the CE pin.
				*delay = max_delay_d + xtiming->FF_bef_wd;
				*delay_we = GetMax(max_delay_a, max_delay_e) + xtiming->lut6_delay + xtiming->FF_bef_ce;
			}
			else {
				// Get the delay of the big MUX for data
				xil_muxd_implem mux_implem;
				Xil_MuxDirect_GetImplem(synth, nb_ports_w, &mux_implem);
				// Arbitrary LUT for the EQ of the address and the AND with the WE
				double max_delay = GetMax(max_delay_a, max_delay_e) + xtiming->lut6_delay + xtiming->wire_delay;
				// Set the results
				*delay = GetMax(max_delay, max_delay_d) + mux_implem.delay + xtiming->FF_bef_wd;
				*delay_we = max_delay + xtiming->lut6_delay + xtiming->FF_bef_ce;  // Arbitrary LUT for the OR of all WE signals
			}
		}
		else {
			if(sync_found==true) {
				// Implementation as BRAM
				double d = max_delay_d + xtiming->bram36k_bef_wd;
				d = GetMax(d, max_delay_a + xtiming->bram36k_bef_wa);
				d = GetMax(d, max_delay_e + xtiming->bram36k_bef_we);
				*delay = d;
				*delay_we = max_delay_e + xtiming->bram36k_bef_we;
			}
			else {
				// Implementation as simple LUTRAM
				double d = max_delay_d + xtiming->lutram_bef_wd;
				d = GetMax(d, max_delay_a + xtiming->lutram_bef_wa);
				d = GetMax(d, max_delay_e + xtiming->lutram_bef_we);
				*delay = d;
				*delay_we = max_delay_e + xtiming->lutram_bef_we;
			}
		}
	}

	else if(access->model==NETLIST_ACCESS_MEM_D) {
		netlist_access_mem_direct_t* memD_data = access->data;
		double max_delay_wd = Techno_Delay_Process_PortIn_GetMax(data, memD_data->port_wd);
		double max_delay_we = Techno_Delay_Process_PortIn_GetMax(data, memD_data->port_we);
		// Set results depending on the actual mem implem
		if(mem_data->ports_wa_nb==0) {
			// There is no big data MUX
			*delay_we = max_delay_we + xtiming->FF_bef_ce;
			*delay = GetMax(*delay_we, max_delay_wd + xtiming->FF_bef_wd);
		}
		else {
			// Get the delay of the big MUX for data
			xil_muxd_implem mux_implem;
			Xil_MuxDirect_GetImplem(synth, mem_data->ports_wa_nb + 1, &mux_implem);
			*delay = max_delay_wd + mux_implem.delay + xtiming->FF_bef_wd;
			// FIXME: One arbitrary LUT for the OR of all WE signals
			*delay_we = max_delay_we + xtiming->lut6_delay + xtiming->FF_bef_ce;
			*delay = GetMax(*delay, *delay_we);
		}
	}

	else {
		printf("Warning: Access '%s' model '%s' of component '%s' model '%s' is not handled as datapath destination.\n",
			access->name, access->model->name, access->component->name, access->component->model->name
		);
		*delay = 0;
		*delay_we = 0;
	}
}

static void Xilinx_TransDelay_Fsm(techno_delay_t* data, netlist_comp_t* comp, double* delay) {
	synth_target_t* synth = data->Implem->synth_target;
	xilinx_timing_t* xtiming = synth->timing->data;

	// From the current state, compute the max delay for each of the possible next states and the associated buffered outputs
	// Also take into account the delay of the retiming counter.

	// FIXME Ideally we have to ensure no glitch can reach any state register bit, and any output buffer.
	// So we should take into account the size of the logic in from of each bit of the FSM state register,
	// and in front of each output buffer.

	double max_delay = 0;

	avl_p_tree_t tree_symbols;
	avl_p_init(&tree_symbols);

	void rule_scan(netlist_fsm_rule_t* rule, double max_delay_in) {
		if(rule==NULL) return;
		if(rule->state!=NULL) {
			unsigned prev_rules_nb = ChainList_Count(rule->state->prev_rules);
			assert(prev_rules_nb>0);
			// Get the delay of this kind of logic... FIXME approximate
			unsigned lut6_levels = 0;
			for(unsigned i=prev_rules_nb; i>1; i/=6) lut6_levels++;
			double delay_loc = 0;
			if(lut6_levels==0) delay_loc = max_delay_in + xtiming->lut6_delay + xtiming->FF_bef_wd;
			else delay_loc = max_delay_in + lut6_levels * xtiming->lut6_delay + (lut6_levels - 1) * xtiming->lut6_delay + xtiming->FF_bef_wd;
			// Save the max delay
			max_delay = GetMax(max_delay, delay_loc);
			// Take into account the delay to preset the output buffers of this next state
			foreach(rule->state->actions, scanAct) {
				netlist_fsm_action_t* action = scanAct->DATA;
				if(action->outval->is_buffered==false) continue;
				unsigned states_nb = ChainList_Count(action->outval->actions);
				// Get the delay of the associated logic
				unsigned lut6_levels_buf = 0;
				for(unsigned i=states_nb; i>1; i/=6) lut6_levels_buf++;
				unsigned lut6_levels_total = lut6_levels + lut6_levels_buf;
				if(lut6_levels_total==0) lut6_levels_total = 1;
				delay_loc = max_delay_in + lut6_levels_total * (xtiming->lut6_delay + xtiming->wire_delay) + xtiming->FF_bef_wd;

				// FIXME Take into account the delay of the retiming counter, if any

				// Save the max delay
				max_delay = GetMax(max_delay, delay_loc);
			}
			return;
		}
		// Get the name of the FSM inputs and the max delay on them
		hvex_GetSymbols_tree(rule->condition, &tree_symbols, NULL);
		avl_p_foreach(&tree_symbols, scanSym) {
			char* name = scanSym->data;
			netlist_port_t* port = NULL;
			avl_pp_find_data(&comp->ports, name, (void**)&port);
			assert(port!=NULL);
			assert(port->direction==NETLIST_PORT_DIR_IN);
			double delay_in = Techno_Delay_Process_PortIn_GetMax(data, port);
			max_delay_in = GetMax(max_delay_in, delay_in);
		}
		avl_p_reset(&tree_symbols);
		// Parse the child rules
		rule_scan(rule->rule_true, max_delay_in);
		rule_scan(rule->rule_false, max_delay_in);
	}

	// Launch the scan
	rule_scan(data->curstate->state->rules, 0);

	*delay = max_delay;
}

// FIXME missing CB functions for circbuf, PP, UART etc



//======================================================================
// VHDL generators for specific components
//======================================================================

static void Xilinx_DiffClock_GenVHDL(FILE* F, netlist_vhdl_t* params, netlist_comp_t* comp) {
	netlist_diffclock_t* dclk_data = comp->data;
	netlist_access_diffclock_t* accdclk_data = dclk_data->access_diffclock->data;

	fprintf(F,
		"library unisim;\n"
		"use unisim.vcomponents.all;\n"
		"\n"
	);

	Netlist_GenVHDL_Entity(F, comp, params);

	fprintfm(F,
		"\n"
		"architecture %s of %s is\n", params->architecture, Netlist_Comp_GetRename_Comp(comp, params),
		"\n"
		"	component IBUFDS is\n"
		"		port(\n"
		"			O  : out std_logic;\n"
		"			I  : in  std_logic;\n"
		"			IB : in  std_logic\n"
		"		);\n"
		"	end component;\n"
		"\n"
		"begin\n"
		"\n"
		"	IBUFDS_inst : IBUFDS\n"
		"	port map (\n"
		"		O  => %s,\n", dclk_data->port_clk_out->name,
		"		I  => %s,\n", accdclk_data->port_p->name,
		"		IB => %s\n", accdclk_data->port_n->name,
		"	);\n"
		"\n"
		"end architecture;\n"
		"\n",
		NULL
	);

}



//======================================================================
// Declaration of callback functions of component models
//======================================================================

static void Xilinx_EvalCompSize_DeclCB(plugin_t* plugin) {

	// Note: to declare an eval function for a component model known by name,
	// Do this:
	//decl_cb_size("name", My_Super_Eval_Func);
	void decl_cb_size(char* name, ptype_list* (*func)(synth_target_t* synth, netlist_comp_t* comp)) {
		netlist_comp_model_t* model = Netlist_CompModel_Get(namealloc(name));
		if(model==NULL) return;
		model->func_eval_size = (type_cb_size)func;
	}
	void decl_cb_delay(char* name, void (*func)(techno_delay_t* data, netlist_port_t* port, double* delays)) {
		netlist_comp_model_t* model = Netlist_CompModel_Get(namealloc(name));
		if(model==NULL) return;
		model->func_propag_delays = (type_cb_delay)func;
	}

	NETLIST_COMP_TOP     ->func_eval_size = (type_cb_size)Xilinx_EvalSize_Comp_Top;
	NETLIST_COMP_SIGNAL  ->func_eval_size = (type_cb_size)Xilinx_EvalSize_Comp_Sig;
	NETLIST_COMP_SIGNAL  ->func_propag_delays = (type_cb_delay)Xilinx_PropagDelay_Comp_Sig;

	NETLIST_COMP_FSM     ->func_eval_size = (type_cb_size)Xilinx_EvalSize_Comp_FSM_OneHot;
	NETLIST_COMP_FSM     ->func_propag_delays = (type_cb_delay)Xilinx_PropagDelay_Comp_Fsm;
	NETLIST_COMP_FSM     ->func_trans_delay = (type_cb_delay_tr)Xilinx_TransDelay_Fsm;

	NETLIST_COMP_REGISTER->func_eval_size = (type_cb_size)Xilinx_EvalSize_Comp_Reg;
	NETLIST_COMP_REGISTER->func_propag_delays = (type_cb_delay)Xilinx_PropagDelay_Comp_Reg;
	NETLIST_COMP_REGISTER->func_write_comp = (type_cb_delay_wc)Xilinx_WriteDelay_Reg_Comp;

	NETLIST_COMP_MUX     ->func_eval_size = (type_cb_size)Xilinx_EvalSize_Comp_Mux;
	NETLIST_COMP_MUXBIN  ->func_eval_size = (type_cb_size)Xilinx_EvalSize_Comp_MuxBin;
	NETLIST_COMP_MUX     ->func_propag_delays = (type_cb_delay)Xilinx_PropagDelay_Comp_Mux;
	NETLIST_COMP_MUXBIN  ->func_propag_delays = (type_cb_delay)Xilinx_PropagDelay_Comp_MuxBin;

	NETLIST_COMP_MEMORY  ->func_eval_size = (type_cb_size)Xilinx_EvalSize_Comp_Memory;
	NETLIST_COMP_MEMORY  ->func_propag_delays = (type_cb_delay)Xilinx_PropagDelay_Comp_Mem;
	NETLIST_COMP_MEMORY  ->func_write_access = (type_cb_delay_wa)Xilinx_WriteDelay_Mem_Access;

	NETLIST_COMP_ADD->func_eval_size = (type_cb_size)Xilinx_EvalSize_Comp_Asb;
	NETLIST_COMP_SUB->func_eval_size = (type_cb_size)Xilinx_EvalSize_Comp_Asb;
	NETLIST_COMP_MUL->func_eval_size = (type_cb_size)Xilinx_EvalSize_Comp_Mul;
	NETLIST_COMP_DIVQR->func_eval_size = (type_cb_size)Xilinx_EvalSize_Comp_DivQR;
	NETLIST_COMP_ADD->func_propag_delays = (type_cb_delay)Xilinx_PropagDelay_Comp_Asb;
	NETLIST_COMP_SUB->func_propag_delays = (type_cb_delay)Xilinx_PropagDelay_Comp_Asb;
	NETLIST_COMP_MUL->func_propag_delays = (type_cb_delay)Xilinx_PropagDelay_Comp_Mul;
	NETLIST_COMP_DIVQR->func_propag_delays = (type_cb_delay)Xilinx_PropagDelay_Comp_DivQR;

	NETLIST_COMP_CMP->func_eval_size = (type_cb_size)Xilinx_EvalSize_Comp_Cmp;
	NETLIST_COMP_CMP->func_propag_delays = (type_cb_delay)Xilinx_PropagDelay_Comp_Cmp;

	NETLIST_COMP_AND ->func_eval_size = (type_cb_size)Xilinx_EvalSize_Comp_Logic;
	NETLIST_COMP_NAND->func_eval_size = (type_cb_size)Xilinx_EvalSize_Comp_Logic;
	NETLIST_COMP_OR  ->func_eval_size = (type_cb_size)Xilinx_EvalSize_Comp_Logic;
	NETLIST_COMP_NOR ->func_eval_size = (type_cb_size)Xilinx_EvalSize_Comp_Logic;
	NETLIST_COMP_XOR ->func_eval_size = (type_cb_size)Xilinx_EvalSize_Comp_Logic;
	NETLIST_COMP_NXOR->func_eval_size = (type_cb_size)Xilinx_EvalSize_Comp_Logic;
	NETLIST_COMP_AND ->func_propag_delays = (type_cb_delay)Xilinx_PropagDelay_Comp_Logic;
	NETLIST_COMP_NAND->func_propag_delays = (type_cb_delay)Xilinx_PropagDelay_Comp_Logic;
	NETLIST_COMP_OR  ->func_propag_delays = (type_cb_delay)Xilinx_PropagDelay_Comp_Logic;
	NETLIST_COMP_NOR ->func_propag_delays = (type_cb_delay)Xilinx_PropagDelay_Comp_Logic;
	NETLIST_COMP_XOR ->func_propag_delays = (type_cb_delay)Xilinx_PropagDelay_Comp_Logic;
	NETLIST_COMP_NXOR->func_propag_delays = (type_cb_delay)Xilinx_PropagDelay_Comp_Logic;

	NETLIST_COMP_NOT->func_eval_size = (type_cb_size)Xilinx_EvalSize_Comp_Not;
	NETLIST_COMP_NOT->func_propag_delays = (type_cb_delay)Xilinx_PropagDelay_Comp_Logic;

	NETLIST_COMP_SHL ->func_eval_size = (type_cb_size)Xilinx_EvalSize_Comp_ShiftRot;
	NETLIST_COMP_SHL ->func_propag_delays = (type_cb_delay)Xilinx_PropagDelay_Comp_ShiftRot;
	NETLIST_COMP_SHR ->func_eval_size = (type_cb_size)Xilinx_EvalSize_Comp_ShiftRot;
	NETLIST_COMP_SHR ->func_propag_delays = (type_cb_delay)Xilinx_PropagDelay_Comp_ShiftRot;
	NETLIST_COMP_ROTL->func_eval_size = (type_cb_size)Xilinx_EvalSize_Comp_ShiftRot;
	NETLIST_COMP_ROTL->func_propag_delays = (type_cb_delay)Xilinx_PropagDelay_Comp_ShiftRot;
	NETLIST_COMP_ROTR->func_eval_size = (type_cb_size)Xilinx_EvalSize_Comp_ShiftRot;
	NETLIST_COMP_ROTR->func_propag_delays = (type_cb_delay)Xilinx_PropagDelay_Comp_ShiftRot;

	NETLIST_COMP_CLKDIV->func_eval_size = (type_cb_size)Xilinx_EvalSize_Comp_ClkDiv;

	NETLIST_COMP_FIFO->func_eval_size = (type_cb_size)Xilinx_EvalSize_Comp_Fifo;

	NETLIST_COMP_PINGPONG_IN ->func_eval_size = (type_cb_size)Xilinx_EvalSize_Comp_PingPong;
	NETLIST_COMP_PINGPONG_OUT->func_eval_size = (type_cb_size)Xilinx_EvalSize_Comp_PingPong;

	NETLIST_COMP_UART->func_eval_size = (type_cb_size)Xilinx_EvalSize_Comp_Uart;
	NETLIST_COMP_UART->func_propag_delays = (type_cb_delay)Xilinx_PropagDelay_Comp_Uart;

	NETLIST_COMP_DIFFCLOCK->func_gen_vhdl = Xilinx_DiffClock_GenVHDL;
}



//======================================================================
// Coarse-grain estimation of propagation delay in a VEX expression
//======================================================================

/* INFO
- The operators are supposed to have the same size as the VEX elements.
- No multiplexer is taken into account.
- All successive logic operations are considered to be mapped on one LUT6 stage.
	This is to avoid the over-fragmentation of the AUGH operators.
	In a VEX expression there is no expressions sharing, and operations on vectors
	are often detailed for each bit. This creates an artificially huge number
	of small logic operators, which actually do not exist because the operation is mapped
	on LUT6...

A negative return value means error (function call...)
*/

// This must be initialized for each Act computation
static inline double estim_mux_lat(xilinx_timing_t* xtim) {
	return xtim->lut6_delay;
}

static double Techno_Eval_Delay_Vex_internal(implem_t* Implem, hvex_t* Expr, bool father_was_logic) {
	xilinx_timing_t* xtiming = Implem->synth_target->timing->data;

	if(Expr->model==HVEX_LITERAL) return 0;
	else if(Expr->model==HVEX_VECTOR) return xtiming->FF_aft_rd;

	else if(Expr->model==HVEX_INDEX) {
		double l = Techno_Eval_Delay_Vex_internal(Implem, Expr->operands, 0);
		if(l<0) return l;
		// Get the component
		char* name = hvex_index_getname(Expr);
		netlist_comp_t* comp = Map_Netlist_GetComponent(Implem, name);
		if(comp!=NULL) {
			generic_carac_t* carac_tmp = NULL;
			// Get the component delays
			if(comp->model==NETLIST_COMP_MEMORY) {
				netlist_memory_t* mem_data = comp->data;
				carac_tmp = GetCarac_Multiport(Implem->synth_target,
					mem_data->cells_nb, mem_data->data_width, mem_data->addr_width,
					mem_data->ports_wa_nb, mem_data->ports_ra_nb, mem_data->direct_en
				);
			}
			// FIXME Handle Pingpongs etc
			// Handle when the component was properly recognized
			if(carac_tmp!=NULL) {
				double d = carac_tmp->timings->delay.delay + xtiming->wire_delay + estim_mux_lat(xtiming) + xtiming->wire_delay + l;
				carac_free(carac_tmp);
				return d;
			}
		}
		// Default behaviour
		return xtiming->lutram_aft + xtiming->wire_delay + estim_mux_lat(xtiming) + xtiming->wire_delay + l;
	}

	bool is_logic = false;
	if(
		Expr->model==HVEX_CONCAT ||
		Expr->model==HVEX_NOT ||
		Expr->model==HVEX_MUXDECIN ||
		Expr->model==HVEX_EQ || Expr->model==HVEX_NE ||
		Expr->model==HVEX_AND || Expr->model==HVEX_OR || Expr->model==HVEX_XOR ||
		Expr->model==HVEX_NAND || Expr->model==HVEX_NOR || Expr->model==HVEX_NXOR
	) is_logic = true;

	double max_op_latency = 0;
	hvex_foreach(Expr->operands, VexOp) {
		double l = Techno_Eval_Delay_Vex_internal(Implem, VexOp, is_logic);
		if(l<0) return l;
		if(l>max_op_latency) max_op_latency = l;
	}

	if(Expr->model==HVEX_CONCAT) {
		return max_op_latency;
	}

	else if(Expr->model==HVEX_MUXB) {
		generic_carac_t* carac_tmp = GetCarac_Mux_Binary(Implem->synth_target, hvex_countops(Expr)-1, 1);
		// FIXME this MUX is estimated as only one LUT
		double d = carac_tmp->timings->delay.delay + xtiming->wire_delay + estim_mux_lat(xtiming) + xtiming->wire_delay + max_op_latency;
		carac_free(carac_tmp);
		return d;
	}
	else if(Expr->model==HVEX_MUXDEC) {
		xil_muxd_implem mux_data;
		Xil_MuxDirect_GetImplem(Implem->synth_target, hvex_countops(Expr), &mux_data);
		return mux_data.delay + xtiming->wire_delay + max_op_latency;
	}
	else if(Expr->model==HVEX_MUXDECIN) {
		return max_op_latency;
	}

	else if(
		Expr->model==HVEX_NOT ||
		Expr->model==HVEX_EQ || Expr->model==HVEX_NE ||
		Expr->model==HVEX_AND || Expr->model==HVEX_OR || Expr->model==HVEX_XOR ||
		Expr->model==HVEX_NAND || Expr->model==HVEX_NOR || Expr->model==HVEX_NXOR
	) {
		if(father_was_logic==0) return xtiming->lut6_delay + xtiming->wire_delay + estim_mux_lat(xtiming) + xtiming->wire_delay + max_op_latency;
		return max_op_latency;
	}

	else if(Expr->model==HVEX_ADD || Expr->model==HVEX_SUB || Expr->model==HVEX_NEG) {
		generic_carac_t* carac_tmp = GetCarac_Add_Sub(Implem->synth_target, Expr->width);
		double d = carac_tmp->timings->delay.delay + xtiming->wire_delay + estim_mux_lat(xtiming) + xtiming->wire_delay + max_op_latency;
		carac_free(carac_tmp);
		return d;
	}

	else if(
		Expr->model==HVEX_LT || Expr->model==HVEX_LE || Expr->model==HVEX_GT || Expr->model==HVEX_GE ||
		Expr->model==HVEX_LTS || Expr->model==HVEX_LES || Expr->model==HVEX_GTS || Expr->model==HVEX_GES
	) {
		generic_carac_t* carac_tmp = GetCarac_Add_Sub(Implem->synth_target, Expr->width);
		double d = carac_tmp->timings->delay.delay + xtiming->wire_delay + estim_mux_lat(xtiming) + xtiming->wire_delay + max_op_latency;
		carac_free(carac_tmp);
		return d;
	}

	else if(Expr->model==HVEX_MUL) {
		if(Expr->operands==NULL || Expr->operands->next==NULL || Expr->operands->next->next!=NULL) return -1;
		unsigned size1, size2;
		size1 = Expr->operands->width;
		size2 = Expr->operands->next->width;
		generic_carac_t* carac_tmp = GetCarac_Mul(Implem->synth_target, size1, size2, Expr->width, hvex_is_signed(Expr));
		double d = carac_tmp->timings->delay.delay + xtiming->wire_delay + estim_mux_lat(xtiming) + xtiming->wire_delay + max_op_latency;
		carac_free(carac_tmp);
		return d;
	}

	else if(Expr->model==HVEX_DIVQ || Expr->model==HVEX_DIVR) {
		unsigned size1 = Expr->operands->width;
		unsigned size2 = Expr->operands->next->width;
		generic_carac_t* carac_tmp = GetCarac_DivQR(Implem->synth_target, size1, size2);
		double d = carac_tmp->timings->delay.delay + xtiming->wire_delay + estim_mux_lat(xtiming) + xtiming->wire_delay + max_op_latency;
		carac_free(carac_tmp);
		return d;
	}

	else if(Expr->model==HVEX_SHL || Expr->model==HVEX_SHR || Expr->model==HVEX_ROTL || Expr->model==HVEX_ROTR) {
		unsigned size_data, size_shift;
		size_data = Expr->operands->width;
		size_shift = Expr->operands->next->width;
		generic_carac_t* carac_tmp = GetCarac_Rot(Implem->synth_target, size_shift, size_data);
		double d = carac_tmp->timings->delay.delay + xtiming->wire_delay + estim_mux_lat(xtiming) + xtiming->wire_delay + max_op_latency;
		carac_free(carac_tmp);
		return d;
	}

	//errprintf("Unsupported hvex model '%s'.\n", Expr->model);
	return -1;
}

static double Techno_Eval_Delay_VexRead(implem_t* Implem, hvex_t* Expr) {
	// Launch computing
	return Techno_Eval_Delay_Vex_internal(Implem, Expr, false);
}

static double Xilinx_Eval_Delay_VexAsg(implem_t* Implem, hvex_t* Expr) {
	if(hvex_model_is_asg(Expr->model)==false) return Techno_Eval_Delay_VexRead(Implem, Expr);

	hvex_t* vex_expr = hvex_asg_get_expr(Expr);
	hvex_t* vex_addr = hvex_asg_get_addr(Expr);
	hvex_t* vex_cond = hvex_asg_get_cond(Expr);

	double lat_read = 0;
	double lat_dest = 0;

	// Compute the delay of the Read side

	if(vex_expr!=NULL) {
		double l = Techno_Eval_Delay_VexRead(Implem, vex_expr);
		if(l<0) return -__LINE__;
		if(l>lat_read) lat_read = l;
	}
	if(vex_addr!=NULL) {
		double l = Techno_Eval_Delay_VexRead(Implem, vex_addr);
		if(l<0) return -__LINE__;
		if(l>lat_read) lat_read = l;
	}
	if(vex_cond!=NULL) {
		double l = Techno_Eval_Delay_VexRead(Implem, vex_cond);
		if(l<0) return -__LINE__;
		if(l>lat_read) lat_read = l;
	}

	// Compute the delay/setup of the Destination side

	xilinx_timing_t* xtiming = Implem->synth_target->timing->data;

	if(vex_addr!=NULL) {
		lat_dest = xtiming->lutram_bef_wa;
	}
	else {
		lat_dest = xtiming->FF_bef_wd;
	}

	// Compute the delay of the complete datapath

	double delay = lat_dest + xtiming->wire_delay + estim_mux_lat(xtiming) + xtiming->wire_delay + lat_read;

	return delay;
}



//======================================================================
// Initialization of the techno stuff declared
//======================================================================

// Callbacks
static void Xilinx_Timing_DispData(FILE* F, techno_timing_t* techno_timing, indent_t* indent) {
	findentprintf(F, indent, "Name ......... %s\n", techno_timing->name);
	if(techno_timing->description!=NULL) {
		findentprintf(F, indent, "Description .. %s\n", techno_timing->description);
	}
	xilinx_timing_t* timing = techno_timing->data;
	findentprintf(F, indent, "Delay values (unit is nanosecond):\n");
	findentprintf(F, indent, "Combinational, misc .... %s=%g, %s=%g, %s=%g\n",
		"wire", timing->wire_delay,
		"lut6", timing->lut6_delay,
		"mux", timing->mux_delay
	);
	findentprintf(F, indent, "Sequential, flip-flop .. %s=%g, %s=%g, %s=%g\n",
		"bef_wd", timing->FF_bef_wd,
		"bef_we", timing->FF_bef_ce,
		"aft", timing->FF_aft_rd
	);
	findentprintf(F, indent, "Sequential, lutram ..... %s=%g, %s=%g, %s=%g, %s=%g\n",
		"bef_wd", timing->lutram_bef_wd,
		"bef_wa", timing->lutram_bef_wa,
		"bef_we", timing->lutram_bef_we,
		"aft", timing->lutram_aft
	);
	findentprintf(F, indent, "Sequential, bram36k .... %s=%g, %s=%g, %s=%g, %s=%g, %s=%g, %s=%g, %s=%g\n",
		"bef_wd", timing->bram36k_bef_wd,
		"bef_wa", timing->bram36k_bef_wa,
		"bef_we", timing->bram36k_bef_we,
		"bef_ra", timing->bram36k_bef_ra,
		"bef_re", timing->bram36k_bef_re,
		"aft_rd", timing->bram36k_aft_read,
		"aft_rd_casc", timing->bram36k_aft_read_casc
	);
	findentprintf(F, indent, "DSP48 cores ............ %s\n", timing->dsp48e.avail==true ? "yes" : "no");
	if(timing->dsp48e.avail==true) {
		findentprintf(F, indent, "DSP48 Combinational, in->out ...... %s=%g, %s=%g, %s=%g\n",
			"ab_mul_cout", timing->dsp48e.ab_mul_cout,
			"ab_p", timing->dsp48e.ab_p,
			"c_p", timing->dsp48e.c_p
		);
		findentprintf(F, indent, "DSP48 Combinational, CascIn->out .. %s=%g, %s=%g, %s=%g, %s=%g, %s=%g, %s=%g\n",
			"CascAB_mul_p", timing->dsp48e.CascAB_mul_p,
			"CascAB_p", timing->dsp48e.CascAB_p,
			"CascAB_CascAB", timing->dsp48e.CascAB_CascAB,
			"CascAB_mul_CascC", timing->dsp48e.CascAB_mul_CascC,
			"CascP_p", timing->dsp48e.CascP_p,
			"CascP_CascC", timing->dsp48e.CascP_CascC
		);
		findentprintf(F, indent, "DSP48 Combinational, in->CascOut .. %s=%g, %s=%g, %s=%g, %s=%g\n",
			"ab_CascAB", timing->dsp48e.ab_CascAB,
			"ab_mul_CascC", timing->dsp48e.ab_mul_CascC,
			"ab_CascC", timing->dsp48e.ab_CascC,
			"c_CascC", timing->dsp48e.c_CascC
		);
	}
}
static ptype_list* Xilinx_GetChipResources(fpga_model_t* model) {
	xilinx_res_t* xres = model->resources;
	ptype_list* list = NameRes_FromCarac(xres);
	return list;
}

static void Xilinx_Resources_WriteNames(FILE* F, const char* beg, const char* sep, const char* end) {
	if(beg!=NULL) fprintf(F, "%s", beg);

	fprintf(F, "%s", namealloc_lut6);
	if(sep!=NULL) fprintf(F, "%s", sep);
	fprintf(F, "%s", namealloc_lutram);
	if(sep!=NULL) fprintf(F, "%s", sep);
	fprintf(F, "%s", namealloc_ff);
	if(sep!=NULL) fprintf(F, "%s", sep);
	fprintf(F, "%s", namealloc_dsp48e);
	if(sep!=NULL) fprintf(F, "%s", sep);
	fprintf(F, "%s", namealloc_bram36k);

	if(end!=NULL) fprintf(F, "%s", end);
}
static void Xilinx_Resources_WriteUsage(FILE* F, ptype_list* list, const char* beg, const char* sep, const char* end) {
	ptype_list* elt;
	if(beg!=NULL) fprintf(F, "%s", beg);

	elt = ChainPType_SearchData(list, namealloc_lut6);
	if(elt==NULL) fprintf(F, "0");
	else fprintf(F, "%ld", elt->TYPE);

	if(sep!=NULL) fprintf(F, "%s", sep);

	elt = ChainPType_SearchData(list, namealloc_lutram);
	if(elt==NULL) fprintf(F, "0");
	else fprintf(F, "%ld", elt->TYPE);

	if(sep!=NULL) fprintf(F, "%s", sep);

	elt = ChainPType_SearchData(list, namealloc_ff);
	if(elt==NULL) fprintf(F, "0");
	else fprintf(F, "%ld", elt->TYPE);

	if(sep!=NULL) fprintf(F, "%s", sep);

	elt = ChainPType_SearchData(list, namealloc_dsp48e);
	if(elt==NULL) fprintf(F, "0");
	else fprintf(F, "%ld", elt->TYPE);

	if(sep!=NULL) fprintf(F, "%s", sep);

	elt = ChainPType_SearchData(list, namealloc_bram36k);
	if(elt==NULL) fprintf(F, "0");
	else fprintf(F, "%ld", elt->TYPE);

	if(end!=NULL) fprintf(F, "%s", end);
}

// Used to check if the resource limits set in the command line are valid
static bool Xilinx_IsResourceTypeValid(const char* name) {
	if(name==namealloc_lut6)    return true;
	if(name==namealloc_lutram)  return true;
	if(name==namealloc_ff)      return true;
	if(name==namealloc_dsp48e)  return true;
	if(name==namealloc_bram36k) return true;
	return false;
}

// Special evaluations

// Evaluate the HW cost of transforming a memory into registers
// For consistency, the mem is supposed to be accessed only by literal adresses.
static ptype_list* Xilinx_Netlist_Eval_Mem2Reg(implem_t* Implem, const char* name) {
	// Get the component
	netlist_comp_t* comp = Netlist_Comp_GetChild(Implem->netlist.top, name);
	if(comp==NULL) {
		printf("WARNING %s:%d : Netlist instance for array '%s' not found.\n", __FILE__, __LINE__, name);
		return NULL;
	}

	if(comp->model==NETLIST_COMP_MEMORY) {
		netlist_memory_t* mem_data = comp->data;

		if(mem_data->direct_en==true || mem_data->ports_wa_nb>1) {
			// No hardware cost. The memory is already a heap of independent registers.
		}
		else if(mem_data->ports_wa_nb==0) {
			// This is a ROM. No hardware cost.
			//list = NameRes_Add_One(NULL, namealloc_lut6, lut_factor * mem_data->cells_nb * mem_data->data_width);
		}
		else {
			// Get the current size of the component
			generic_carac_t* carac = GetCarac_Multiport(Implem->synth_target,
				mem_data->cells_nb, mem_data->data_width, mem_data->addr_width,
				mem_data->ports_wa_nb, mem_data->ports_ra_nb, mem_data->direct_en
			);
			ptype_list* resources_orig = NameRes_FromCarac(carac->resources);
			carac_free(carac);

			// Arbitrary factor : the number of LUTs added per bit of memory
			// FIXME Take the MUX into account somehow better than this
			double lut_factor = 1.5;

			// Estimation of the resources after transformation
			xilinx_res_t estim_res;
			clear_resources(&estim_res);
			estim_res.ff = mem_data->cells_nb * mem_data->data_width;
			estim_res.lut6 = lut_factor * mem_data->cells_nb * mem_data->data_width;
			ptype_list* list = NameRes_FromCarac(&estim_res);

			// Compute the difference
			Techno_NameRes_Sub_raw(list, resources_orig, 1, 1);
			freeptype(resources_orig);

			return list;
		}
	}

	else if(comp->model==NETLIST_COMP_PINGPONG_IN || comp->model==NETLIST_COMP_PINGPONG_OUT) {
		netlist_pingpong_t* pp_data = comp->data;

		if(pp_data->is_direct==true) {
			// Nothing to do
		}
		else {
			// Get the current size of the component
			ptype_list* resources_orig = Techno_EvalSize_Comp(Implem->synth_target, comp);

			// Estimation of the resources after transformation
			netlist_comp_t* dupcomp = Netlist_Comp_Dup(comp);
			Netlist_PingPong_EnableDirect(dupcomp);
			ptype_list* resources_dup = Techno_EvalSize_Comp(Implem->synth_target, dupcomp);
			Netlist_Comp_Free(dupcomp);

			// Compute the difference
			Techno_NameRes_Sub_raw(resources_dup, resources_orig, 1, 1);
			freeptype(resources_orig);

			return resources_dup;
		}

	}  // End case ping-pong

	else {
		abort();
	}

	return NULL;
}

// Evaluate the HW cost of adding Read ports.
static ptype_list* Xilinx_Netlist_Eval_AddReadPorts(implem_t* Implem, const char* name, unsigned ports_add_nb) {

	// Get the hardware instance
	netlist_comp_t* comp = Netlist_Comp_GetChild(Implem->netlist.top, name);
	if(comp==NULL) {
		printf("WARNING %s:%d : Netlist instance for array '%s' not found.\n", __FILE__, __LINE__, name);
		return NULL;
	}

	ptype_list* resources_orig = NULL;
	ptype_list* resources_after = NULL;

	if(comp->model==NETLIST_COMP_MEMORY){
		netlist_memory_t* compMem_data = comp->data;
		generic_carac_t* carac;

		// Get the current size of the component

		carac = GetCarac_Multiport(Implem->synth_target,
			compMem_data->cells_nb, compMem_data->data_width, compMem_data->addr_width,
			compMem_data->ports_wa_nb, compMem_data->ports_ra_nb, compMem_data->direct_en
		);
		resources_orig = NameRes_FromCarac(carac->resources);
		carac_free(carac);

		carac = GetCarac_Multiport(Implem->synth_target,
			compMem_data->cells_nb, compMem_data->data_width, compMem_data->addr_width,
			compMem_data->ports_wa_nb, compMem_data->ports_ra_nb + ports_add_nb, compMem_data->direct_en
		);
		resources_after = NameRes_FromCarac(carac->resources);
		carac_free(carac);
	}

	else if(comp->model==NETLIST_COMP_PINGPONG_IN || comp->model==NETLIST_COMP_PINGPONG_OUT) {
		netlist_pingpong_t* compPP_data = comp->data;
		generic_carac_t* carac;

		// FIXMEEEE these functions do not take into account the MUX in the PP component

		// Get the current size of the component

		carac = GetCarac_Multiport(Implem->synth_target,
			compPP_data->cells_nb, compPP_data->data_width, compPP_data->addr_width,
			compPP_data->ports_wa_nb + 1, compPP_data->ports_ra_nb, false
		);
		carac_mult_resources(carac, 2);
		resources_orig = NameRes_FromCarac(carac->resources);
		carac_free(carac);

		// Get the size after adding ports

		carac = GetCarac_Multiport(Implem->synth_target,
			compPP_data->cells_nb, compPP_data->data_width, compPP_data->addr_width,
			compPP_data->ports_wa_nb + 1,
			compPP_data->ports_ra_nb + ports_add_nb,
			false
		);
		carac_mult_resources(carac, 2);
		resources_after = NameRes_FromCarac(carac->resources);
		carac_free(carac);
	}
	else {
		printf("WARNING %s:%d : Can't evaluate cost of adding ports for comp '%s' type '%s'.\n", __FILE__, __LINE__, comp->name, comp->model->name);
		return NULL;
	}

	// Compute the difference
	Techno_NameRes_Sub_raw(resources_after, resources_orig, 1, 1);
	freeptype(resources_orig);

	return resources_after;
}

// FIXME only using the output width is not enough for multiplier and divider
// FIXME input from a comp model structure would be cleaner
static ptype_list* Xilinx_Netlist_Get_NameRes_FromOutWidth(implem_t* Implem, const char* type, unsigned width) {
	generic_carac_t* carac = NULL;

	if     (type==NETLIST_COMP_ADD->name)   carac = GetCarac_Add_Sub(Implem->synth_target, width);
	else if(type==NETLIST_COMP_SUB->name) {
		carac = GetCarac_Add_Sub(Implem->synth_target, width);
		carac->resources->lut6 += 5;  // For the sign outputs
		// And the optional embedded comparator
		generic_carac_t* carac_cmp = GetCarac_Cmp_eq(Implem->synth_target, width);
		carac_add_resources(carac, carac_cmp, 1);
		carac_free(carac_cmp);
	}
	else if(type==NETLIST_COMP_MUL->name)   carac = GetCarac_Mul(Implem->synth_target, width, width, width, true);
	else if(type==NETLIST_COMP_DIVQR->name) carac = GetCarac_DivQR(Implem->synth_target, width, width);
	else if(type==NETLIST_COMP_SHL->name)   carac = GetCarac_Shift(Implem->synth_target, width);
	else if(type==NETLIST_COMP_SHR->name)   carac = GetCarac_Shift(Implem->synth_target, width);

	else {
		printf("WARNING %s:%d : Operator type '%s' is not handled here.\n", __FILE__, __LINE__, type);
		return NULL;
	}

	ptype_list* list = NameRes_FromCarac(carac->resources);
	carac_free(carac);
	return list;
}

static ptype_list* Xilinx_EvalRes_Mux_Direct(implem_t* Implem, unsigned width, unsigned inputs) {
	xilinx_res_t* xres = Xilinx_Res_New();
	xil_muxd_implem mux_implem;
	Xil_MuxDirect_GetImplem(Implem->synth_target, inputs, &mux_implem);
	xres->lut6 += mux_implem.lut6_nb * width;
	ptype_list* list = NameRes_FromCarac(xres);
	Xilinx_Res_Free(xres);
	return list;
}

static ptype_list* Xilinx_EvalRes_FsmOut_OneBit_OneHot(implem_t* Implem, unsigned actions_nb) {
	generic_carac_t* carac = GetCarac_Logic(Implem->synth_target, actions_nb, 1);
	ptype_list* list = NameRes_FromCarac(carac->resources);
	carac_free(carac);
	return list;
}
static ptype_list* Xilinx_EvalRes_FsmStates_OneHot(implem_t* Implem, unsigned states_nb) {
	ptype_list* list = NULL;
	list = addptype(list, states_nb, namealloc_ff);
	list = addptype(list, states_nb * 2, namealloc_lut6);
	return list;
}
// Count 1/6 LUT per Action (because of the way FSM outputs are computed)
static ptype_list* Xilinx_EvalRes_FsmStatesActs_OneHot(implem_t* Implem, unsigned states_nb, unsigned actions_nb) {
	ptype_list* list = NULL;
	list = addptype(list, states_nb, namealloc_ff);
	list = addptype(list, states_nb + (actions_nb+5)/6, namealloc_lut6);
	return list;
}

// Callback to list the memory components for which adding a read buffer is needed/appropriate
static chain_list* Xilinx_ListMem_ReadBuf(implem_t* Implem) {
	chain_list* list_mem = NULL;

	avl_pp_foreach(&Implem->netlist.top->children, scanComp) {
		netlist_comp_t* comp = scanComp->data;
		if(comp->model!=NETLIST_COMP_MEMORY) continue;
		netlist_memory_t* mem_data = comp->data;
		if(mem_data->addr_width > 6) {
			list_mem = addchain(list_mem, comp);
		}
	}

	return list_mem;
}

// Callbacks for manipulation of port/pin attributes
// For a port, it is a chain_list* : one element per bit
//   Each element contains another chain_list* of attributes (strings) given to namealloc()
static void portattr_free(void* data) {
	chain_list* port_attr = data;
	foreach(port_attr, scan) freechain(scan->DATA);
}
static void* portattr_dup(void* data) {
	chain_list* port_attr = data;
	chain_list* newport_attr = dupchain(port_attr);
	foreach(newport_attr, scan) scan->DATA = dupchain(scan->DATA);
	return newport_attr;
}

// Callbacks for create/free of Techno private config
static void privdata_free(synth_target_t* synth) {
	if(synth->techno_data==NULL) return;
	free(synth->techno_data);
	synth->techno_data = NULL;
}
static void privdata_init(synth_target_t* synth) {
	if(synth->techno_data!=NULL) privdata_free(synth);
	synth->techno_data = calloc(1, sizeof(xilinx_techdata_t));
	xilinx_techdata_t* techdata = synth->techno_data;
	// Initialize fields
	techdata->keep_hier = false;
	techdata->use_dsp = true;
	techdata->use_bram = false;
	// Clock generation: auto
	techdata->force_gen_clock = 0;
}

#include "../../augh/auto/command.h"

// Techno command interpreter
static int Xilinx_Techno_Command(implem_t* Implem, command_t* cmd_data) {
	const char* cmd = Command_PopParam(cmd_data);
	if(cmd==NULL) {
		printf("Error [techno '%s'] : No command received. Try 'help'.\n", Implem->synth_target->techno->name);
		return -1;
	}

	if(strcmp(cmd, "help")==0) {
		printf("This is techno '%s'\n", Implem->synth_target->techno->name);
		printf(
			"Commands:\n"
			"  help                Display this help.\n"
			"  use-dsp [<bool>]    Enable/disable use of DSP primitives.\n"
			"                      (init=true, default=true)\n"
			"  use-bram [<bool>]   Enable/disable use of BRAM primitives.\n"
			"                      (init=false, default=true)\n"
			"  keep-hier [<bool>]  Enable/disable hierarchy conservation.\n"
			"                      (init=false, default=true)\n"
			"  backend-set-xst     Use XST toolchain for backend synthesis.\n"
			"  backend-set-vivado  Use Vivado toolchain for backend synthesis.\n"
			"  gen-clock [auto|yes|no]\n"
			"                      Force generation of clock configuration in Xilinx project files.\n"
			"                      Only applies if there is a top-level clock port.\n"
		);
		return 0;
	}

	xilinx_techdata_t* techdata = Implem->synth_target->techno_data;

	if(strcmp(cmd, "use-dsp")==0) {
		int z = Command_PopParam_YesNoDef_Verbose(cmd_data, true, cmd);
		if(z<0) return -1;
		techdata->use_dsp = z;
		return 0;
	}
	else if(strcmp(cmd, "use-bram")==0) {
		int z = Command_PopParam_YesNoDef_Verbose(cmd_data, true, cmd);
		if(z<0) return -1;
		techdata->use_bram = z;
		return 0;
	}
	else if(strcmp(cmd, "keep-hier")==0) {
		int z = Command_PopParam_YesNoDef_Verbose(cmd_data, true, cmd);
		if(z<0) return -1;
		techdata->keep_hier = z;
		return 0;
	}

	else if(strcmp(cmd, "backend-xst")==0) {
		XST_Techno_SetCB(Implem->synth_target->techno);
	}
	else if(strcmp(cmd, "backend-vivado")==0) {
		Vivado_Techno_SetCB(Implem->synth_target->techno);
	}

	else if(strcmp(cmd, "gen-clock")==0) {
		const char* param = Command_PopParam(cmd_data);
		if(param==NULL) {
			printf("Error [techno '%s'] : Missing parameter for command '%s'.\n", Implem->synth_target->techno->name, cmd);
			return -1;
		}
		if     (strcmp(param, "auto")==0) techdata->force_gen_clock = 0;
		else if(strcmp(param, "yes")==0)  techdata->force_gen_clock = 1;
		else if(strcmp(param, "no")==0)   techdata->force_gen_clock = -1;
		else {
			printf("Error [techno '%s'] : Command '%s': Unrecognized parameter '%s'.\n", Implem->synth_target->techno->name, cmd, param);
			return -1;
		}
	}

	else {
		printf("Error [techno '%s'] : Unknown command '%s'.\n", Implem->synth_target->techno->name, cmd);
		return -1;
	}

	return 0;
}

void Xilinx_Techno_SetCB(techno_t* techno) {
	techno->name = namealloc(techno->name);

	techno->fprint_timing         = Xilinx_Timing_DispData;
	techno->getres                = Xilinx_GetChipResources;
	techno->reswritenames         = Xilinx_Resources_WriteNames;
	techno->reswriteusage         = Xilinx_Resources_WriteUsage;
	techno->isresnamevalid        = Xilinx_IsResourceTypeValid;
	techno->vexcoarseevaldelay    = Xilinx_Eval_Delay_VexAsg;
	techno->sizeeval_mem2reg      = Xilinx_Netlist_Eval_Mem2Reg;
	techno->sizeeval_addreadports = Xilinx_Netlist_Eval_AddReadPorts;
	techno->sizeeval_compfromoutwidth = Xilinx_Netlist_Get_NameRes_FromOutWidth;
	techno->sizeeval_muxdirect    = Xilinx_EvalRes_Mux_Direct;
	techno->sizeeval_fsmout1      = Xilinx_EvalRes_FsmOut_OneBit_OneHot;
	techno->sizeeval_fsmstates    = Xilinx_EvalRes_FsmStates_OneHot;
	techno->sizeeval_fsmstatesacts = Xilinx_EvalRes_FsmStatesActs_OneHot;
	techno->listmem_readbuf       = Xilinx_ListMem_ReadBuf;

	techno->privatedata_init      = privdata_init;
	techno->privatedata_free      = privdata_free;

	techno->portattr_free         = portattr_free;
	techno->portattr_dup          = portattr_dup;

	// Note: the callbacks for XST or Vivado are set elsewhere

	techno->command               = Xilinx_Techno_Command;
}

void Xilinx_Techno_LinkTiming(techno_t* techno, techno_timing_t* timing) {
	timing->name = namealloc(timing->name);
	avl_pp_add_overwrite(&techno->timings, timing->name, timing);
	timing->techno = techno;
}



//======================================================================
// Manipulation of FPGA pin parameters
//======================================================================

/* How pins are identified

To launch XST logic synthesis, and place & route, an UCF file must be generated.
Each top-level signal must be associated to an FPGA pin and a pin config.
However, in AUGH, a port can be a vector.

The data stored in the tree port2pin (structure fpga_board_t) is a chain_list* : one element per bit.
The elements are present from right to left, or least significant bit to most significant bit of the port.
The DATA stored is another chain_list* of attributes (strings).

Example: board xupv5, 100 MHz clock named "USER_CLK"
	DATA = namealloc("LOC = AH15"), namealloc("IOSTANDARD=LVCMOS33")

Example: board xupv5, CPU reset button named "CPU RESET"
	DATA = namealloc("LOC = E9"), namealloc("IOSTANDARD=LVCMOS33"), namealloc("PULLUP")

Notes about pin attributes:
- DRIVE <value>
  For an output port. Specify the current limit in mA.
    IOSTANDARD = LVTTL    : 2, 4, 6, 8, 12, 16, 24
    IOSTANDARD = LVCMOS12 : 2, 4, 6, 8, 12, 16
    IOSTANDARD = LVCMOS15 : 2, 4, 6, 8, 12, 16
    IOSTANDARD = LVCMOS18 : 2, 4, 6, 8, 12, 16
    IOSTANDARD = LVCMOS25 : 2, 4, 6, 8, 12, 16, 24
    IOSTANDARD = LVCMOS33 : 2, 4, 6, 8, 12, 16, 24
- PULLDOWN, PULLUP
  For input pins and tristate pins. Connect an internal pull-down /pull-up resistor.
- SLEW = SLOW (default)
         FAST
  For output pins. Specifies the slew rate (speed of transition)

*/

// Generate a list of stralloc()-ated attributes (separate at the '|' characters)
chain_list* SplitPinAttr(char* str) {
	chain_list* list = NULL;
	do {
		// Find beginning of attribute
		for( ; (*str)!=0; str++) if(isspace(*str)==0) break;
		if((*str)==0) break;
		// Mark the beginning
		char* ptr_beg = str;
		unsigned length = 0;
		unsigned length_lastchar = 0;
		// Find the end of the attribute
		for( ; (*str)!=0; str++) {
			if((*str)=='|') { str++; break; }
			length ++;
			if(isspace(*str)==0) { length_lastchar = length; }
		}
		// Add the attribute to the list
		if(length_lastchar > 0) {
			char buffer[length_lastchar + 1];
			strncpy(buffer, ptr_beg, length_lastchar);
			buffer[length_lastchar] = 0;
			list = addchain(list, stralloc(buffer));
		}
		if((*str)==0) break;
	}while(1);
	// Reverse the list to get the right sense
	return reverse(list);
}

#include <stdarg.h>

// The list must end with NULL
static chain_list* MakeListAttr_va(char* attr, va_list ap) {
	chain_list* list = NULL;
	while(attr!=NULL) {
		list = addchain(list, stralloc(attr));
		attr = va_arg(ap, char*);
	}
	return reverse(list);
}
__attribute((__unused__))
chain_list* MakeListAttr(char* attr, ...) {
	va_list ap;
	va_start(ap, attr);
	chain_list* list = MakeListAttr_va(attr, ap);
	va_end(ap);
	return list;
}
chain_list* ListPin_AddListAttr(chain_list* listpin, char* attr, ...) {
	va_list ap;
	va_start(ap, attr);
	chain_list* list = MakeListAttr_va(attr, ap);
	va_end(ap);
	return addchain(listpin, list);
}
chain_list* ListPin_MakeListAttr(char* attr, ...) {
	va_list ap;
	va_start(ap, attr);
	chain_list* list = MakeListAttr_va(attr, ap);
	va_end(ap);
	return addchain(NULL, list);
}



//======================================================================
// Main initialization
//======================================================================

void Xilinx_Techno_Init(plugin_t* plugin) {

	// Resource names
	namealloc_ff       = namealloc("dff");
	namealloc_lut6     = namealloc("lut6");
	namealloc_lutram   = namealloc("lutram");
	namealloc_dsp48e   = namealloc("dsp48e");
	namealloc_bram36k  = namealloc("bram36");

	// Declare callbacks for evaluation of component size
	Xilinx_EvalCompSize_DeclCB(plugin);

	// Declare the XilBram component model
	netlist_comp_model_t* comp_model = Netlist_Comp_XilBram_GetModel();
	Plugin_DeclCompModel(plugin, comp_model);

	// Read the techno data from data file
	Xilinx_Techno_ReadJSON(plugin);

	// Declare boards
	// FIXME Here it is assumed there will be no error

	board_t* board_xupv5 = Xilinx_BuildBoard_Xupv5();
	Plugin_DeclBoard(plugin, board_xupv5);

	board_t* board_zybo = Xilinx_BuildBoard_Zybo();
	Plugin_DeclBoard(plugin, board_zybo);

	board_t* board_zedboard = Xilinx_BuildBoard_ZedBoard();
	Plugin_DeclBoard(plugin, board_zedboard);

	board_t* board_zc706 = Xilinx_BuildBoard_Zc706();
	Plugin_DeclBoard(plugin, board_zc706);

	board_t* board_vc709 = Xilinx_BuildBoard_Vc709();
	Plugin_DeclBoard(plugin, board_vc709);

}


