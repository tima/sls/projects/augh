
#ifndef _XILINX_XST_H_
#define _XILINX_XST_H_


#define XST_PRJ_DEFAULT "xstprj"

int XST_Gen_Project(implem_t* Implem, char* dirname);
int XST_Launch(implem_t* Implem);

void XST_Techno_SetCB(techno_t* techno);


#endif  // _XILINX_XST_H_

