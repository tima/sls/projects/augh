
#ifndef _XUPV5_H_
#define _XUPV5_H_


// These declarations are not meant to be used by the user.
// This is to prevent the user to re-declare them without knowing their use.
bool xupv5_clock100;
bool xupv5_clock33;
bool xupv5_clock27;
bool xupv5_cpureset;

// Communication interfaces
#if 1
bool xupv5_uart1_rx;
bool xupv5_uart1_tx;
bool xupv5_uart2_rx;
bool xupv5_uart2_tx;
#endif

#if 0  // Old code
uint8_t xupv5_uart1;
uint8_t xupv5_uart2;
#endif

// Miscellaneous I/O
uint8_t xupv5_leds8;
uint8_t xupv5_dipsw;
bool    xupv5_lederr1;
bool    xupv5_lederr2;
bool    xupv5_piezzo;

// Switches and LEDs North, South, East, West, Center

uint5_t xupv5_ledspos;
uint5_t xupv5_swpos;

#define XUPV5_POS_NORTH  0x10
#define XUPV5_POS_EAST   0x08
#define XUPV5_POS_SOUTH  0x04
#define XUPV5_POS_WEST   0x02
#define XUPV5_POS_CENTER 0x01

// Rotary encoder
// Note: 15 pulses per revolution
// Note: clockwise rotation: AB: 00 -> 01 -> 11 -> 10

uint3_t xupv5_rotary;

#define XUPV5_ROTARY_INCA  0x04
#define XUPV5_ROTARY_INCB  0x02
#define XUPV5_ROTARY_PUSH  0x01
#define XUPV5_ROTARY_INCAB (XUPV5_ROTARY_INCA | XUPV5_ROTARY_INCB)

#define XUPV5_ROTARY_CHECKMOVE_CLOCKWISE(old, new) ( \
	( ((old | new) & XUPV5_ROTARY_INCAB) == XUPV5_ROTARY_INCB && (old & XUPV5_ROTARY_INCB) == 0 ) || \
	( ((old & new) & XUPV5_ROTARY_INCAB) == XUPV5_ROTARY_INCB && (new & XUPV5_ROTARY_INCA) != 0 ) || \
	( ((old & new) & XUPV5_ROTARY_INCAB) == XUPV5_ROTARY_INCA && (old & XUPV5_ROTARY_INCB) != 0 ) || \
	( ((old | new) & XUPV5_ROTARY_INCAB) == XUPV5_ROTARY_INCA && (new & XUPV5_ROTARY_INCA) == 0 ) \
)

static inline bool xupv5_rotary_getmove_clock(uint3_t old_val, uint3_t new_val) {
	return XUPV5_ROTARY_CHECKMOVE_CLOCKWISE(old_val, new_val) ? true : false;
}
static inline bool xupv5_rotary_getmove_counter(uint3_t old_val, uint3_t new_val) {
	return XUPV5_ROTARY_CHECKMOVE_CLOCKWISE(new_val, old_val) ? true : false;
}


#endif  // _XUPV5_H_

