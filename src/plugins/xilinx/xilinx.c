
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

#include "../../augh/auto/auto.h"
#include "../../augh/plugins/plugins.h"
#include "../../augh/netlist/netlist.h"
#include "../../augh/netlist/netlist_vhdl.h"  // For test: generation of component
#include "../../augh/auto/command.h"


#include "xilinx_techno.h"
#include "xilinx_xst.h"
#include "xilinx_vivado.h"
#include "xilinx_bram.h"  // For test: generation of component

#define PLUGIN_NAME     "xilinx"
#define PLUGIN_VERSION  "0.1"



static int plugin_command(plugin_t* plugin, implem_t* Implem, command_t* cmd_data) {
	const char* cmd = Command_PopParam(cmd_data);
	if(cmd==NULL) {
		printf("Error [plugin '%s'] : No command received. Try 'help'.\n", plugin->name);
		return -1;
	}

	// Many commands use this structure, so get it only once
	xilinx_techdata_t* techdata = Implem->synth_target->techno_data;

	if(strcmp(cmd, "help")==0) {
		printf("This is plugin '%s' version %s.\n", plugin->name, plugin->version);
		printfm(
			"\n"
			"Commands:\n"
			"  help             Display this help.\n"
			"  set-chip-package <value>\n"
			"                   Set the chip package. Example: ffg1157\n"
			"  set-chip-speed <value>\n"
			"                   Set the chip speed grade. Example: 1\n"
			"  set-chip-name <name>\n"
			"                   Set the targeted chip name. Example: xc7v585t\n"
			"  set-prj-dir <dir>\n"
			"                   Set the name of the directory where to generate back-end synthesis project files.\n"
			"  gen-xst-project [<dir>]\n"
			"                   Generate XST project files in a local directory.\n"
			"                   Default directory: %s.\n", XST_PRJ_DEFAULT,
			"  gen-vivado-project [<dir>]\n"
			"                   Generate Vivado project files in a local directory.\n"
			"                   Default directory: %s.\n", VIVADO_PRJ_DEFAULT,
			"\n"
			"Other commands:\n"
			"  gen-comp-bram <name> <cells> <dataw>\n"
			"                   Generate a memory component that uses BRAM.\n"
			"\n",
			NULL
		);
		return 0;
	}

	if(strcmp(cmd, "set-chip-package")==0) {
		char* param = Command_PopParam_stralloc(cmd_data);
		if(param==NULL) {
			printf("Error [plugin '%s'] : Missing parameter for command '%s'.\n", plugin->name, cmd);
			return -1;
		}
		techdata->chip_package = param;
	}
	else if(strcmp(cmd, "set-chip-speed")==0) {
		char* param = Command_PopParam_stralloc(cmd_data);
		if(param==NULL) {
			printf("Error [plugin '%s'] : Missing parameter for command '%s'.\n", plugin->name, cmd);
			return -1;
		}
		techdata->chip_speed = param;
	}
	else if(strcmp(cmd, "set-chip-name")==0) {
		char* param = Command_PopParam_stralloc(cmd_data);
		if(param==NULL) {
			printf("Error [plugin '%s'] : Missing parameter for command '%s'.\n", plugin->name, cmd);
			return -1;
		}
		techdata->chip_name = param;
	}
	else if(strcmp(cmd, "set-prj-dir")==0) {
		char* param = Command_PopParam_stralloc(cmd_data);
		if(param==NULL) {
			printf("Error [plugin '%s'] : Missing parameter for command '%s'.\n", plugin->name, cmd);
			return -1;
		}
		techdata->synth_prj_dir = param;
	}

	else if(strcmp(cmd, "gen-xst-project")==0) {
		char* dirname = Command_PopParam_stralloc(cmd_data);
		return XST_Gen_Project(Implem, dirname);
	}
	else if(strcmp(cmd, "gen-vivado-project")==0) {
		char* dirname = Command_PopParam_stralloc(cmd_data);
		return Vivado_Gen_Project(Implem, dirname);
	}

	else if(strcmp(cmd, "replace-mem-bram")==0) {

		// To be sure: Rebuild all links between ports
		Netlist_PortTargets_CompBuild(Implem->netlist.top);

		// Get the mem comps that have a read buffer
		chain_list* list_mem = NULL;
		avl_pp_foreach(&Implem->netlist.top->children, scanComp) {
			netlist_comp_t* comp = scanComp->data;
			if(comp->model==NETLIST_COMP_MEMORY) list_mem = addchain(list_mem, comp);
		}

		// Scan the list of mem comps and replace when possible
		foreach(list_mem, scanComp) {
			netlist_comp_t* compMem = scanComp->DATA;
			netlist_memory_t* mem_data = compMem->data;

			if(mem_data->ports_cam_nb != 0) continue;
			if(mem_data->ports_wa_nb > 1) continue;
			if(mem_data->ports_ra_nb != 1) continue;
			if(mem_data->direct_en==true) continue;

			netlist_access_t* access_ra = mem_data->access_ports_ra.root->data;
			netlist_comp_t* compReg = Netlist_Mem_AccessRA_GetReadBuf(access_ra);
			if(compReg==NULL) continue;

			netlist_register_t* reg_data = compReg->data;
			if(reg_data->init_value != NULL) {
				printf("Warning: Not replacing mem '%s' by BRAM-based component because reg init value is not handled yet\n", compMem->name);
				continue;
			}

			// Here the mem comp is a simple dual-port synchronous mem (possibly ROM)
			// Perform replacement

			printf("Replacing by BRAM-based component: mem '%s' + reg '%s'\n", compMem->name, compReg->name);

			// CHeck if ROM
			bool is_rom = true;
			netlist_access_t* access_wa = NULL;
			netlist_access_mem_addr_t* access_wa_data = NULL;
			if(mem_data->ports_wa_nb > 0) {
				access_wa = mem_data->access_ports_wa.root->data;
				access_wa_data = access_wa->data;
				is_rom = false;
			}

			netlist_comp_t* compBram = Netlist_Comp_XilBram_New(compMem->name, mem_data->cells_nb, mem_data->data_width, mem_data->addr_width, is_rom);
			netlist_xilbram_t* xilbram_data = compBram->data;

			// Modify links between ports

			netlist_access_mem_addr_t* access_ra_data = access_ra->data;

			xilbram_data->port_clk->source = Netlist_ValCat_DupList(mem_data->port_clk->source);
			Netlist_PortTargets_SetSource(xilbram_data->port_clk);

			Netlist_PortTargets_ReplaceOutByPort(reg_data->port_out, xilbram_data->port_rd);

			xilbram_data->port_ra->source = Netlist_ValCat_DupList(access_ra_data->port_a->source);
			xilbram_data->port_re->source = Netlist_ValCat_DupList(reg_data->port_en->source);
			Netlist_PortTargets_SetSource(xilbram_data->port_ra);
			Netlist_PortTargets_SetSource(xilbram_data->port_re);

			if(access_wa!=NULL) {
				xilbram_data->port_wd->source = Netlist_ValCat_DupList(access_wa_data->port_d->source);
				xilbram_data->port_wa->source = Netlist_ValCat_DupList(access_wa_data->port_a->source);
				xilbram_data->port_we->source = Netlist_ValCat_DupList(access_wa_data->port_e->source);
				Netlist_PortTargets_SetSource(xilbram_data->port_wd);
				Netlist_PortTargets_SetSource(xilbram_data->port_wa);
				Netlist_PortTargets_SetSource(xilbram_data->port_we);
			}

			if(mem_data->init_vex != NULL) {
				xilbram_data->init_vex = malloc(mem_data->cells_nb * sizeof(*xilbram_data->init_vex));
				for(unsigned i=0; i<mem_data->cells_nb; i++) {
					xilbram_data->init_vex[i] = hvex_dup(mem_data->init_vex[i]);
				}
			}

			// Finally, remove the previous components and add the new BRAM-based memory component

			Netlist_PortTargets_CompUnlinkIn(compMem);
			Netlist_PortTargets_CompUnlinkIn(compReg);
			Netlist_Comp_Free(compMem);
			Netlist_Comp_Free(compReg);

			Netlist_Comp_SetChild(Implem->netlist.top, compBram);

		}  // Scan all mem components

		// Clean
		freechain(list_mem);

		#ifndef NDEBUG
		Netlist_Debug_CheckIntegrity(Implem->netlist.top);
		#endif

		return 0;
	}

	else if(strcmp(cmd, "gen-comp-bram")==0) {
		char* name = Command_PopParam_namealloc(cmd_data);

		const char* strcells = Command_PopParam(cmd_data);
		const char* strdataw = Command_PopParam(cmd_data);

		if(strdataw==NULL) {
			printf("Error [plugin '%s'] : Missing parameters for command '%s'.\n", plugin->name, cmd);
			return -1;
		}

		unsigned cells = atoi(strcells);
		unsigned dataw = atoi(strdataw);
		unsigned addrw = uint_bitsnb(cells);

		netlist_comp_t* comp = Netlist_Comp_XilBram_New(name, cells, dataw, addrw, false);

		Netlist_GenVHDL_simple(comp);
		Netlist_Comp_Free(comp);

		return 0;
	}

	else {
		printf("Error [plugin '%s'] : Unknown command '%s'.\n", plugin->name, cmd);
		return -1;
	}

	return 0;
}


int plugin_init(plugin_t* plugin) {
	plugin->api_version = namealloc(AUGH_PLUGIN_API_VERSION);
	plugin->name        = namealloc(PLUGIN_NAME);
	plugin->version     = namealloc(PLUGIN_VERSION);

	int z = Plugin_Declare(plugin);
	if(z!=0) return z;

	plugin->callbacks.interpreter = plugin_command;

	Xilinx_Techno_Init(plugin);

	return 0;
}

