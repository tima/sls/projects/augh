
#ifndef _ZC706_H_
#define _ZC706_H_


// The clock sources are not meant to be used by the user.
// These declarations are to prevent the user to re-declare them without knowing their use.
// Note: For now, differential clocks are not declared because the AUGH functionality is not fully ready yet
//bool zc706_sysclk200;
//bool zc706_sysclk200_p;
//bool zc706_sysclk200_n;

//bool zc706_usrclk156;
//bool zc706_usrclk156_p;
//bool zc706_usrclk156_n;

//bool zc706_user_sma_clock;
//bool zc706_user_sma_clock_p;
//bool zc706_user_sma_clock_n;

//bool zc706_sma_mgt_refclk;
//bool zc706_sma_mgt_refclk_p;
//bool zc706_sma_mgt_refclk_n;

bool zc706_pl_cpu_reset;

// Miscellaneous I/O
bool    zc706_led_0;
uint3_t zc706_leds;
uint4_t zc706_dipswitch;

// Push buttons Left, Center, Right

uint3_t zc706_pushbuttons;

#define ZC706_MASK_LEFT   0x04
#define ZC706_MASK_CENTER 0x02
#define ZC706_MASK_RIGHT  0x01

// The fan
bool zc706_fan_pwm;
bool zc706_fan_tach;


#endif  // _ZC706_H_

