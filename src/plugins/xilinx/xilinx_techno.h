
#ifndef _XILINX_TECHNO_H_
#define _XILINX_TECHNO_H_

#include "../../augh/plugins/plugins.h"


//======================================================================
// Structures
//======================================================================

// Structure that describes the current technology
// FIXME missing information: intra-CLB timings
//   LUT->outside, LUT->register, LUT->MUX
//   XORCY->REG, XORCY->outside, REG->outside
typedef struct xilinx_timing_t {

	// Latency unit is ns
	double lut6_delay;
	double mux_delay;
	double wire_delay;

	double CARRY_S_delay;
	double CARRY_CI_delay;
	double XORCY_CI_delay;

	double FF_bef_wd;    // before clock, write data
	double FF_bef_ce;    // before clock, write enable
	double FF_aft_rd;    // after clock front

	double lutram_bef_wd;  // before clock front, write data
	double lutram_bef_wa;  // before clock front, write address
	double lutram_bef_we;  // before clock front, write enable
	double lutram_aft;     // read, delay after clock front

	double bram36k_bef_wd;    // before clock front, write data
	double bram36k_bef_wa;    // before clock front, write addres
	double bram36k_bef_we;    // before clock front, write enable
	double bram36k_bef_ra;    // before clock front, read addres
	double bram36k_bef_re;    // before clock front, read enable
	double bram36k_aft_read;  // after clock front, read data
	double bram36k_aft_read_casc;  // after clock front, read data, for cascaded BRAM36K blocks

	// The DSP48 core
	struct {
		bool   avail;
		// Inputs to outputs
		double ab_mul_cout;
		double ab_p;
		double c_p;
		// From Cascading ports
		double CascAB_mul_p;
		double CascAB_p;
		double CascAB_CascAB;
		double CascAB_mul_CascC;
		double CascP_p;
		double CascP_CascC;
		// To Cascading ports
		double ab_CascAB;
		double ab_mul_CascC;
		double ab_CascC;
		double c_CascC;
	}dsp48e;

} xilinx_timing_t;

typedef struct xilinx_res_t {
	unsigned lut6;
	unsigned lutram;
	unsigned ff;
	unsigned dsp48e;
	unsigned bram36k;
} xilinx_res_t;

typedef struct xilinx_techdata_t {
	// Control of back-end tools
	bool keep_hier;
	bool use_dsp;
	bool use_bram;
	// Generation of back-end project files
	char* synth_prj_dir;
	char* chip_package;
	char* chip_speed;
	char* chip_name;
	// Generation of clock declaration in project files
	// No clock: <0, auto: 0, force gen: >0
	int force_gen_clock;
} xilinx_techdata_t;

// Extra data for FPGAs of boards
typedef struct xilinx_board_data_t {
	// The parameter CFGBVS, if applicable
	bool   cfgbvs_en;
	char*  param_cfgbvs;
	double param_config_voltage;
} xilinx_board_data_t;


//======================================================================
// Shared variables
//======================================================================

extern char* namealloc_ff;
extern char* namealloc_lut6;
extern char* namealloc_lutram;
extern char* namealloc_dsp48e;
extern char* namealloc_bram36k;


//======================================================================
// Functions
//======================================================================

xilinx_res_t* Xilinx_Res_New();
void Xilinx_Res_Free(xilinx_res_t* res);

void Xilinx_Techno_Init(plugin_t* plugin);

// Utility functions to help build techno structures
void Xilinx_Techno_SetCB(techno_t* techno);
void Xilinx_Techno_LinkTiming(techno_t* techno, techno_timing_t* timing);

// Utility functions to describe FPGA board
chain_list* SplitPinAttr(char* str);
chain_list* MakeListAttr(char* attr, ...);
chain_list* ListPin_AddListAttr(chain_list* listpin, char* attr, ...);
chain_list* ListPin_MakeListAttr(char* attr, ...);


#endif  // _XILINX_TECHNO_H_

