
#ifndef _XILINX_LOAD_H_
#define _XILINX_LOAD_H_


board_t* Xilinx_BuildBoard_Xupv5();
board_t* Xilinx_BuildBoard_Zybo();
board_t* Xilinx_BuildBoard_ZedBoard();
board_t* Xilinx_BuildBoard_Zc706();
board_t* Xilinx_BuildBoard_Vc709();

void Xilinx_Techno_WriteJSON(plugin_t* plugin);
void Xilinx_Techno_ReadJSON(plugin_t* plugin);


#endif  // _XILINX_LOAD_H_

