
#ifndef _VC709_H_
#define _VC709_H_


// These declarations are not meant to be used by the user.
// This is to prevent the user to re-declare them without knowing their use.
// Note: For now, differential clocks are not declared because the AUGH functionality is not fully ready yet
//bool vc709_sysclk200;
//bool vc709_sysclk200_p;
//bool vc709_sysclk200_n;

//bool vc709_sysclk233;
//bool vc709_sysclk233_p;
//bool vc709_sysclk233_n;

//bool vc709_usrclk;
//bool vc709_usrclk_p;
//bool vc709_usrclk_n;

bool vc709_emcclk80;

bool vc709_cpureset;

// Communication interfaces
bool vc709_uart_rx;
bool vc709_uart_tx;

// Miscellaneous I/O
uint8_t vc709_leds8;
uint8_t vc709_dipsw;

// Directional pushbuttons North, South, East, West, Center

uint5_t vc709_swpos;

#define VC709_POS_NORTH  0x10
#define VC709_POS_EAST   0x08
#define VC709_POS_SOUTH  0x04
#define VC709_POS_WEST   0x02
#define VC709_POS_CENTER 0x01

// The fan
bool vc709_fan_pwm;
bool vc709_fan_tach;


#endif  // _VC709_H_

