
// Generation of ISE project

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

// To create and scan directories
#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>

#include "../../augh/auto/auto.h"
#include "../../augh/auto/command.h"
#include "../../augh/plugins/plugins.h"
#include "../../augh/netlist/netlist.h"
#include "../../augh/netlist/netlist_access.h"

#include "xilinx_techno.h"
#include "xilinx_xst.h"



typedef struct xst_proj_conf_t {
	char* dirname;
	char* topname;
	char* target_fpga;
	char* speed_grade;
	char* package;
	chain_list* list_dir_src;
	chain_list* list_file_src;
	// Some config flags
	bool is_virtex5;
	bool is_virtex7;
} xst_proj_conf_t;


/*
Generated folder architecture :

<dirname>/project.xst
<dirname>/project.prj
<dirname>/project.ucf
<dirname>/Makefile
*/

static char* filename_xst = "project.xst";
static char* filename_prj = "project.prj";
static char* filename_syr = "project.syr";
static char* filename_ucf = "project.ucf";
static char* filename_ut  = "project.ut";

static char* dirname_postsynth = "vhdl-postsynth";
static char* dirname_postpar   = "vhdl-postpar";

static char* filename_impact = "project.impact";

// return 0 if OK
static int XST_Gen_Project_conf(implem_t* Implem, xst_proj_conf_t* conf) {
	xilinx_techdata_t* techdata = Implem->synth_target->techno_data;

	printf("Generating project files for Xilinx XST...\n");

	char* path_comeback = ".";
	if(strcmp(conf->dirname, ".")!=0) {
		mkdir(conf->dirname,  S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
		path_comeback = "..";
	}

	char pathbuf[strlen(conf->dirname) + 30];

	// The configuration file for xst

	sprintf(pathbuf, "%s/%s", conf->dirname, filename_xst);
	FILE* F = fopen(pathbuf, "wb");
	if(F==NULL) {
		printf("ERROR : could not open the file '%s'\n", pathbuf);
		return __LINE__;
	}

	//fprintf(F, "set -xsthdpdir \"xst\"\n");
	//fprintf(F, "set -tmpdir \"xst/projnav.tmp\"\n");
	fprintfm(F,
		"run\n"
		"-ifn %s\n", filename_prj,
		"-ifmt mixed\n"
		"-ofn %s\n", conf->topname,
		"-ofmt NGC\n"
		"-p %s-%s-%s\n", conf->target_fpga, conf->speed_grade, conf->package,
		"-top %s\n", conf->topname,
		"-opt_mode area\n"  // area, speed
		"-opt_level 1\n"
		"-power no\n"
		"-iuc no\n"
		//"#-lso design.lso\n"
		"-keep_hierarchy %s\n", techdata->keep_hier==true ? "yes" : "no",  // yes, no
		"-netlist_hierarchy as_optimized\n"
		"-rtlview yes\n"
		"-glob_opt AllClockNets\n"
		"-read_cores yes\n"
		"-write_timing_constraints no\n"
		"-cross_clock_analysis no\n"
		"-hierarchy_separator /\n"
		"-bus_delimiter <>\n"
		"-case maintain\n"
		"-slice_utilization_ratio 100\n"
		"-bram_utilization_ratio 100\n"
		"-dsp_utilization_ratio 100\n"
		"-lc off\n"
		"-reduce_control_sets off\n"

		#if 1
		"-fsm_extract no\n"
		"-fsm_encoding One-Hot\n"
		"-fsm_style lut\n"
		#endif

		#if 0
		"-fsm_extract no\n"
		"-fsm_encoding auto\n"
		"-fsm_style lut\n"
		#endif

		"-safe_implementation no\n"
		"-shreg_extract yes\n"

		"-ram_extract yes\n"
		"-ram_style %s\n", techdata->use_bram==true ? "auto" : "distributed",  // auto, distributed, block
		"-rom_extract yes\n"
		"-rom_style %s\n", techdata->use_bram==true ? "auto" : "distributed",  // auto, distributed, block
		"-auto_bram_packing yes\n"

		"-resource_sharing yes\n"
		"-async_to_sync no\n"
		"-use_dsp48 %s\n", techdata->use_dsp==true ? "yes" : "no",  // auto, automax, yes, no
		// Note: iobuf=yes is necessary to ngdbuild to find UCF rule target nets
		"-iobuf yes\n"  // yes, no
		//"-max_fanout 100000\n"
		//"-bufg 32\n"
		"-register_duplication yes\n"
		"-register_balancing yes\n"
		"-optimize_primitives no\n"
		"-use_clock_enable auto\n"
		"-use_sync_set auto\n"
		"-use_sync_reset auto\n"
		"-iob auto\n"
		"-equivalent_register_removal yes\n"
		"-slice_utilization_ratio_maxmargin 5\n",
		NULL
	);

	fclose(F);

	// The list of VHDL files
	// Names are relative to where the synthesis will be launched.

	sprintf(pathbuf, "%s/%s", conf->dirname, filename_prj);
	F = fopen(pathbuf, "wb");
	if(F==NULL) {
		printf("ERROR : could not open the file '%s'\n", pathbuf);
		return __LINE__;
	}

	foreach(conf->list_dir_src, scanSrc) {
		char* dirname_vhd = scanSrc->DATA;
		DIR *dp = opendir(dirname_vhd);
		if(dp==NULL) {
			printf("WARNING : Can't scan the folder '%s' for VHDL files. Skipping.\n", dirname_vhd);
			continue;
		}

		do {
			struct dirent *ep = readdir(dp);
			if(ep==NULL) break;
			// Get the file extension
			char* beg_ext = NULL;
			char* scan_name = ep->d_name;
			while(*scan_name!=0) {
				if(*scan_name=='.') beg_ext = scan_name+1;
				scan_name++;
			}
			// Check the file extension
			if(beg_ext==NULL) continue;
			if(strcasecmp(beg_ext, "vhd")!=0) continue;
			fprintf(F, "vhdl work \"%s/%s/%s\"\n", path_comeback, dirname_vhd, ep->d_name);
		}while(1);

		closedir(dp);
	}  // Scan the list of directories

	// Add the VHDL files given directly
	foreach(conf->list_file_src, scanSrc) {
		char* file_name = scanSrc->DATA;
		fprintf(F, "vhdl work \"%s/%s\"\n", path_comeback, file_name);
	}

	fclose(F);


	// The placement constraints (UCF file)

	// The file will be created only if needed
	F = NULL;
	bool ucf_file_incomplete = false;

	// Utility function to open the file
	int ucf_fopen() {
		sprintf(pathbuf, "%s/%s", conf->dirname, filename_ucf);
		F = fopen(pathbuf, "wb");
		if(F==NULL) {
			printf("ERROR : could not open the file '%s'.\n", pathbuf);
			return -1;
		}
		board_fpga_t* board_fpga = Implem->synth_target->board_fpga;
		if(board_fpga!=NULL) {
			fputc('\n', F);
			fprintf(F, "# Constraints for the board '%s', FPGA '%s'\n", board_fpga->board->name, board_fpga->name);
			if(board_fpga->board->description!=NULL) {
				fprintf(F, "#   Board description: %s\n", board_fpga->board->description);
			}
			if(board_fpga->description!=NULL) {
				fprintf(F, "#   Chip description: %s\n", board_fpga->description);
			}
			if(board_fpga->model!=NULL) {
				fprintf(F, "# Targeted FPGA: %s\n", board_fpga->model->name);
				if(board_fpga->model->description!=NULL) {
					fprintf(F, "#   Description: %s\n", board_fpga->model->description);
				}
			}
			fprintf(F, "# Targeted package: %s\n", conf->package);
			fputc('\n', F);
		}
		else {
			fputc('\n', F);
			fprintf(F, "# User-defined target: chip '%s' package '%s'\n", conf->target_fpga, conf->package);
			fputc('\n', F);
		}
		return 0;
	}
	void gen_rule_port(netlist_port_t* port, chain_list* port_pin_desc) {
		unsigned pin_count = 0;
		while(port_pin_desc!=NULL) {
			// Write config for the current pin
			if(port->width==1) fprintf(F, "Net %s", port->name);
			else fprintf(F, "Net %s<%u>", port->name, pin_count);
			chain_list* attributes = port_pin_desc->DATA;
			foreach(attributes, attr) {
				if(attr==attributes) fprintf(F, "  "); else fprintf(F, " | ");
				fprintf(F, "%s", (char*)attr->DATA);
			}
			fprintf(F, ";\n");
			// Next pin
			port_pin_desc = port_pin_desc->NEXT;
			pin_count ++;
		}
	}

	avl_pp_foreach(&Implem->netlist.top->ports, scan) {
		netlist_port_t* port = scan->data;
		chain_list* port_pin_desc = NULL;

		// Try user-defined port attributes
		avl_pp_find_data(&Implem->synth_target->port2pin, port, (void**)&port_pin_desc);
		if(port_pin_desc!=NULL) {
			// Ensure the UCF file is open
			if(F==NULL) {
				int z = ucf_fopen();
				if(z!=0) break;
			}
			if(Implem->synth_target->board_fpga!=NULL) {
				fprintf(F, "# Info: Pin attributes for this port are user-defined.\n");
			}
			// Write the rules for this port
			gen_rule_port(port, port_pin_desc);
			fputc('\n', F);
			continue;
		}

		// Try board-defined port attributes
		if(Implem->synth_target->board_fpga!=NULL) {

			// Get the standard identifier of the port, inside its access structure
			if(port->access==NULL) {
				printf("WARNING: Can't get pin attributes for port '%s' (no access structure).\n", port->name);
				ucf_file_incomplete = true;
				continue;
			}
			// Get the corresponding access from the board_fpga
			netlist_access_t* board_access = NULL;
			avl_pp_find_data(&Implem->synth_target->impacc2boardacc, port->access, (void**)&board_access);
			if(board_access==NULL) {
				printf("WARNING: Can't get pin attributes for port '%s' (no corresponding board access).\n", port->name);
				ucf_file_incomplete = true;
				continue;
			}
			// Get the standard name of the port
			char* stdname = Netlist_Access_Port_GetName(port);
			if(stdname==NULL) {
				printf("WARNING: Can't get pin attributes for port '%s' (no standard name).\n", port->name);
				ucf_file_incomplete = true;
				continue;
			}
			// Get the corresponding board port
			netlist_port_t* board_port = Netlist_Access_Name2Port(board_access, stdname);
			if(board_port==NULL) {
				printf("WARNING: Can't get pin attributes for port '%s' (no corresponding board port).\n", port->name);
				ucf_file_incomplete = true;
				continue;
			}

			// Finally, get the pin attributes
			port_pin_desc = NULL;
			avl_pp_find_data(&Implem->synth_target->board_fpga->port2pin, board_port, (void**)&port_pin_desc);
			if(port_pin_desc==NULL) {
				printf("WARNING: No corresponding pin attributes for port '%s'.\n", port->name);
				ucf_file_incomplete = true;
				continue;
			}

			// Ensure the UCF file is open
			if(F==NULL) {
				int z = ucf_fopen();
				if(z!=0) break;
			}
			// Write the rules for this port
			gen_rule_port(port, port_pin_desc);
			fputc('\n', F);
			continue;
		}

		else {
			//fprintf(F, "# WARNING: No attributes for port '%s'\n", port->name);
			//fputc('\n', F);
			// Also print a user warning
			printf("WARNING: No pin attributes for port '%s'. The UCF rules may be incomplete.\n", port->name);
		}

	}  // Scan all ports

	bool ucf_file_exists = false;
	if(F!=NULL) {
		ucf_file_exists = true;
		fclose(F);
	}


	// The .ut file (configuration for bitgen)

	sprintf(pathbuf, "%s/%s", conf->dirname, filename_ut);
	F = fopen(pathbuf, "wb");
	if(F==NULL) {
		printf("ERROR : could not open the file %s.\n", pathbuf);
		return -1;
	}

	fprintf(F,
		"-w\n"
		"-g DebugBitstream:No\n"
		"-g Binary:no\n"
		"-g CRC:Enable\n"
		"-g ConfigRate:2\n"
		"-g CclkPin:PullUp\n"
		"-g M0Pin:PullUp\n"
		"-g M1Pin:PullUp\n"
		"-g M2Pin:PullUp\n"
		"-g ProgPin:PullUp\n"
		"-g DonePin:PullUp\n"
		"-g InitPin:Pullup\n"
	);

	if(conf->is_virtex5==true) {
		fprintf(F,
			"-g CsPin:Pullup\n"
			"-g DinPin:Pullup\n"
			"-g BusyPin:Pullup\n"
			"-g RdWrPin:Pullup\n"
			"-g HswapenPin:PullUp\n"
		);
	}

	fprintf(F,
		"-g TckPin:PullUp\n"
		"-g TdiPin:PullUp\n"
		"-g TdoPin:PullUp\n"
		"-g TmsPin:PullUp\n"
		"-g UnusedPin:PullDown\n"
		"-g UserID:0xFFFFFFFF\n"
		"-g ConfigFallback:Enable\n"
		"-g SelectMAPAbort:Enable\n"
		"-g BPI_page_size:1\n"
		"-g OverTempPowerDown:Disable\n"
	);

	if(conf->is_virtex5==true) {
		fprintf(F,
			"-g JTAG_SysMon:Enable\n"
		);
	}

	fprintf(F,
		"-g DCIUpdateMode:AsRequired\n"
		"-g StartUpClk:CClk\n"
		"-g DONE_cycle:4\n"
		"-g GTS_cycle:5\n"
		"-g GWE_cycle:6\n"
		"-g LCK_cycle:NoWait\n"
		"-g Match_cycle:NoWait\n"
		"-g Security:None\n"
		"-g DonePipe:No\n"
		"-g DriveDone:No\n"
		"-g Encrypt:No\n"
	);

	fclose(F);

	// The TCL command file to program the FPGA, with Impact

	bool impact_cmd_exists = false;

	board_fpga_t* board_fpga = Implem->synth_target->board_fpga;
	if(board_fpga!=NULL && board_fpga->jtag_id > 0) {
		sprintf(pathbuf, "%s/%s", conf->dirname, filename_impact);
		F = fopen(pathbuf, "wb");
		if(F==NULL) {
			printf("ERROR : could not open the file %s.\n", pathbuf);
			return -1;
		}

		unsigned jtag_id = board_fpga->jtag_id;

		fprintfm(F,
			"setMode -bscan\n"
			"setCable -p auto\n"
			"identify\n"
			"assignfile -p %u -file %s.bit\n", jtag_id, conf->topname,
			"program -p %u\n", jtag_id,
			"quit\n"
			"\n",
			NULL
		);

		fclose(F);
		impact_cmd_exists = true;
	}

	// The Makefile

	sprintf(pathbuf, "%s/%s", conf->dirname, "Makefile");
	F = fopen(pathbuf, "wb");
	if(F==NULL) {
		printf("ERROR : could not open the file %s.\n", pathbuf);
		return -1;
	}

	fprintf(F, "\n");
	fprintf(F, "HDL_FILES   =");
	foreach(conf->list_dir_src, scanSrc) {
		char* dirname_vhd = scanSrc->DATA;
		fprintf(F, " $(wildcard %s/%s/*.vhd)", path_comeback, dirname_vhd);
	}
	foreach(conf->list_file_src, scanSrc) {
		char* file_name = scanSrc->DATA;
		fprintf(F, " %s/%s", path_comeback, file_name);
	}
	fprintf(F, "\n");

	fprintfm(F,
		"TARGET_FPGA = %s-%s-%s\n", conf->target_fpga, conf->speed_grade, conf->package,
		"\n"
		".PHONY: all synth postsynth translate map par timing bitgen postpar\n"
		"\n"
		"# Default action\n"
		"all: bitgen\n"
		"\n"
		"# All available actions\n"
		"synth:     %s.ngc\n", conf->topname,
		"postsynth: %s/%s.vhd\n", dirname_postsynth, conf->topname,
		"translate: %s.ngd\n", conf->topname,
		"map:       %s_map.ngd\n", conf->topname,
		"par:       %s.ncd\n", conf->topname,
		"timing:    %s.twr\n", conf->topname,
		"bitgen:    %s.bit\n", conf->topname,
		"postpar:   %s/%s.vhd\n", dirname_postpar, conf->topname,
		"\n"
		"\n"
		"#================================================\n"
		"# Logic synthesis\n"
		"#================================================\n"
		"\n"
		"# Input  *.vhdl .xst (.prj is referenced in .xst)\n"
		"# Output .ngc .ngr .srp .lso (.srp is synthesis report)\n"
		"%s.ngc: $(HDL_FILES) %s %s\n", conf->topname, filename_xst, filename_prj,
		"	xst -intstyle ise -ifn %s -ofn %s\n", filename_xst, filename_syr,
		"\n"
		"\n"
		"#================================================\n"
		"# Post- logic synthesis netlist\n"
		"#================================================\n"
		"\n"
		"# Input  .ngc\n"
		"# Output .vhd .nlf in directory %s\n", dirname_postsynth,
		"%s/%s.vhd: %s.ngc\n", dirname_postsynth, conf->topname, conf->topname,
		"	netgen -intstyle ise -sim -dir %s -w -fn -ofmt vhdl %s.ngc\n", dirname_postsynth, conf->topname,
		"\n"
		"\n"
		"#================================================\n"
		"# Bitstream generation\n"
		"#================================================\n"
		"\n"
		"# Translate\n"
		"# Input  .ngc .ucf\n"
		"# Output .ngd\n"
		"# Log    .bld *_ngdbuild.xrpt (XML file)\n",
		NULL
	);

	fprintf(F, "%s.ngd: %s.ngc", conf->topname, conf->topname);
	if(ucf_file_exists==true) fprintf(F, " %s", filename_ucf);
	fprintf(F, "\n");

	fprintf(F, "	ngdbuild -intstyle ise -dd _ngo -nt timestamp");
	if(ucf_file_exists==true) fprintf(F, " -uc %s", filename_ucf);
	if(ucf_file_exists==false || ucf_file_incomplete==true) fprintf(F, " -a");
	fprintf(F, " -p $(TARGET_FPGA) %s.ngc %s.ngd\n", conf->topname, conf->topname);

	fprintfm(F,
		"\n"
		"# Map\n"
		"# Input  .ngd\n"
		"# Output *_map.ncd *_map.pcf\n"
		"# Log    .mrp (map report)\n"
		"%s_map.ncd: %s.ngd\n", conf->topname, conf->topname,
		"	map -intstyle ise -p $(TARGET_FPGA) -w -logic_opt off -ol high -t 1 -register_duplication off -global_opt off -mt off -ir off -pr off -lc off -power off -o %s_map.ncd %s.ngd\n", conf->topname, conf->topname,
		"\n"
		"# Place & Route\n"
		"# Input  *_map.ncd *_map.pcf\n"
		"# Output .ncd\n"
		"%s.ncd: %s_map.ncd\n", conf->topname, conf->topname,
		"	par -w -intstyle ise -ol high -mt off %s_map.ncd %s.ncd %s_map.pcf\n", conf->topname, conf->topname, conf->topname,
		"\n"
		"# Generate Post-Place & Route Static Timing\n"
		"# Input  .ncd *_map.pcf\n"
		"# Output .twr .twx\n"
		"%s.twr: %s.ncd\n", conf->topname, conf->topname,
		"	trce -intstyle ise -v 3 -s 1 -n 3 -fastpaths -xml %s.twx %s.ncd -o %s.twr %s_map.pcf\n", conf->topname, conf->topname, conf->topname, conf->topname,
		"\n"
		"# Generate Programming File\n"
		"# Input  .ut .ncd\n"
		"# Output .bit\n"
		"%s.bit: %s.ncd %s\n", conf->topname, conf->topname, filename_ut,
		"	bitgen -intstyle ise -f %s %s.ncd\n", filename_ut, conf->topname,
		"\n"
		"\n"
		"#================================================\n"
		"# Post- place-and-route netlist\n"
		"#================================================\n"
		"\n"
		"# Input  .ncd\n"
		"# Output .vhd .nlf .sdf in directory %s\n", dirname_postpar,
		"%s/%s.vhd: %s.ncd\n", dirname_postpar, conf->topname, conf->topname,
		"	netgen -intstyle ise -sim -dir %s -w -fn -ofmt vhdl %s.ncd\n", dirname_postpar, conf->topname,
		NULL
	);

	if(impact_cmd_exists==true) {
		fprintfm(F,
			"\n"
			"\n"
			"#================================================\n"
			"# Program the FPGA with Xilinx Impact\n"
			"#================================================\n"
			"\n"
			".PHONY: program synthprog\n"
			"\n"
			"program:\n"
			"	impact -batch %s\n", filename_impact,
			"\n"
			"synthprog: %s.bit\n", conf->topname,
			"	impact -batch %s\n", filename_impact,
			NULL
		);
	}

	if(board_fpga!=NULL && board_fpga->jtag_id > 0 && board_fpga->xc3sprog_cable!=NULL) {
		fprintfm(F,
			"\n"
			"\n"
			"#================================================\n"
			"# Program the FPGA with xc3sprog\n"
			"#================================================\n"
			"\n"
			".PHONY: xc3sprog xc3sprog-program\n"
			"\n"
			"xc3sprog: xc3sprog-program\n"
			"\n"
			"xc3sprog-program:\n"
			"	xc3sprog -c %s -v -p %u %s.bit:w::bit\n", board_fpga->xc3sprog_cable, board_fpga->jtag_id-1, conf->topname,
			NULL
		);
	}

	fprintf(F, "\n");

	fclose(F);

	return 0;
}

int XST_Gen_Project(implem_t* Implem, char* dirname) {
	xilinx_techdata_t* techdata = Implem->synth_target->techno_data;

	// Config structure
	xst_proj_conf_t conf_i;
	xst_proj_conf_t* conf = &conf_i;
	memset(&conf_i, 0, sizeof(conf_i));
	conf->is_virtex5 = false;
	conf->is_virtex7 = false;

	conf->topname = Implem->netlist.top->name;

	conf->is_virtex5 = strcmp(Implem->synth_target->techno->name, "virtex-5")==0 ? true : false;
	conf->is_virtex7 = strcmp(Implem->synth_target->techno->name, "virtex-7")==0 ? true : false;

	conf->dirname = dirname;
	if(conf->dirname==NULL) conf->dirname = techdata->synth_prj_dir;
	if(conf->dirname==NULL) conf->dirname = XST_PRJ_DEFAULT;

	// Save the project directory in the techdata, in case it isn't
	techdata->synth_prj_dir = conf->dirname;

	unsigned errors_nb = 0;

	if(Implem->synth_target->model!=NULL) conf->target_fpga = Implem->synth_target->model->name;
	else conf->target_fpga = techdata->chip_name;

	if(Implem->synth_target->timing!=NULL) conf->speed_grade = Implem->synth_target->timing->name;
	else conf->speed_grade = techdata->chip_speed;

	if(Implem->synth_target->package!=NULL) conf->package = Implem->synth_target->package->name;
	else conf->package = techdata->chip_package;

	if(conf->target_fpga==NULL) {
		printf("Error [techno '%s']: No FPGA chip target defined.\n", Implem->synth_target->techno->name);
		errors_nb ++;
	}
	if(conf->speed_grade==NULL) {
		printf("Error [techno '%s']: No FPGA speed grade defined.\n", Implem->synth_target->techno->name);
		errors_nb ++;
	}
	if(conf->package==NULL) {
		printf("Error [techno '%s']: No FPGA package defined.\n", Implem->synth_target->techno->name);
		errors_nb ++;
	}

	if(errors_nb!=0) return -1;

	conf->list_dir_src = addchain(NULL, Implem->env.vhdl_dir);

	// Actual generation
	int z = XST_Gen_Project_conf(Implem, conf);

	// Clean
	freechain(conf->list_dir_src);

	return z;
}

// Function to use as callback in techno_t structure
static int XST_Gen_Project_callback(implem_t* Implem) {
	return XST_Gen_Project(Implem, NULL);
}

// Launch back-end logic synthesis, place and route
// Return 0 if OK
int XST_Launch(implem_t* Implem) {
	xilinx_techdata_t* techdata = Implem->synth_target->techno_data;

	if(techdata->synth_prj_dir==NULL) {
		printf("Error: The back-end project files are not generated. Can't launch back-end.\n");
		return -1;
	}

	// Build the command-line: make -C <dirname>
	char buf[20 + strlen(techdata->synth_prj_dir)];
	sprintf(buf, "make -C '%s'", techdata->synth_prj_dir);

	fflush(NULL);

	return system(buf);
}


void XST_Techno_SetCB(techno_t* techno) {
	techno->synth_gen_prj = XST_Gen_Project_callback;
	techno->synth_launch  = XST_Launch;
}


