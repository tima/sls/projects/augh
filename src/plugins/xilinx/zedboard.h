
#ifndef _ZEDBOARD_H_
#define _ZEDBOARD_H_


// These declarations are not meant to be used by the user.
// This is to prevent the user to re-declare them without knowing their use.
bool zedboard_clock100m;

// Miscellaneous I/O
uint8_t zedboard_led8;
uint8_t zedboard_sw8;
uint8_t zedboard_btn5;

#define XUPV5_POS_UP     0x10
#define XUPV5_POS_RIGHT  0x08
#define XUPV5_POS_DOWN   0x04
#define XUPV5_POS_LEFT   0x02
#define XUPV5_POS_CENTER 0x01


#endif  // _ZEDBOARD_H_

