
#ifndef _XILINX_BRAM_H_
#define _XILINX_BRAM_H_



extern netlist_comp_model_t* NETLIST_COMP_XILBRAM;

typedef struct netlist_xilbram_t  netlist_xilbram_t;
struct netlist_xilbram_t {
	// Main parameters
	unsigned data_width;
	unsigned addr_width;
	unsigned cells_nb;

	// The ports
	netlist_port_t* port_clk;
	netlist_port_t* port_rd;
	netlist_port_t* port_ra;
	netlist_port_t* port_re;
	// Note: The write side is optional
	netlist_port_t* port_wd;
	netlist_port_t* port_wa;
	netlist_port_t* port_we;

	// The INIT values, stored as VEX
	hvex_t** init_vex;
};

netlist_comp_t* Netlist_Comp_XilBram_New(char* name, unsigned cells_nb, unsigned data_width, unsigned addr_width, bool is_rom);

netlist_comp_model_t* Netlist_Comp_XilBram_GetModel();



#endif  // _XILINX_BRAM_H_

