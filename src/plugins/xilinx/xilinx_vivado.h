
#ifndef _XILINX_VIVADO_H_
#define _XILINX_VIVADO_H_


#define VIVADO_PRJ_DEFAULT "vivadoprj"

int Vivado_Gen_Project(implem_t* Implem, char* dirname);
int Vivado_Launch(implem_t* Implem);

void Vivado_Techno_SetCB(techno_t* techno);


#endif  // _XILINX_VIVADO_H_

