
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#include "../../augh/indent.h"
#include "../../augh/chain.h"
#include "../../augh/auto/auto.h"
#include "../../augh/hvex/hvex.h"

#include "../../augh/netlist/netlist.h"
#include "../../augh/netlist/netlist_comps.h"
#include "../../augh/netlist/netlist_access.h"
#include "../../augh/netlist/netlist_vhdl.h"
#include "../../augh/netlist/netlist_cmd.h"

#include "xilinx_bram.h"



netlist_comp_model_t* NETLIST_COMP_XILBRAM = NULL;

netlist_comp_t* Netlist_Comp_XilBram_New(char* name, unsigned cells_nb, unsigned data_width, unsigned addr_width, bool is_rom) {
	netlist_comp_t* comp = Netlist_Comp_New(NETLIST_COMP_XILBRAM);
	comp->name = name;

	netlist_xilbram_t* xilbram_data = comp->data;

	xilbram_data->data_width = data_width;
	xilbram_data->addr_width = addr_width;
	xilbram_data->cells_nb   = cells_nb;

	char* port_name;

	port_name = namealloc("clk");
	xilbram_data->port_clk = Netlist_Comp_AddPortAccess_Clock(comp, port_name);

	port_name = namealloc("port_rd");
	xilbram_data->port_rd = Netlist_Comp_AddPort_Out(comp, port_name, data_width);
	port_name = namealloc("port_ra");
	xilbram_data->port_ra = Netlist_Comp_AddPort_In(comp, port_name, addr_width);
	port_name = namealloc("port_re");
	xilbram_data->port_re = Netlist_Comp_AddPort_In(comp, port_name, 1);

	if(is_rom==false) {
		port_name = namealloc("port_wd");
		xilbram_data->port_wd = Netlist_Comp_AddPort_In(comp, port_name, data_width);
		port_name = namealloc("port_wa");
		xilbram_data->port_wa = Netlist_Comp_AddPort_In(comp, port_name, addr_width);
		port_name = namealloc("port_we");
		xilbram_data->port_we = Netlist_Comp_AddPort_In(comp, port_name, 1);
	}

	return comp;
}

// Note: the given array is directly set in the component (no duplication)
// No check is done. The array size must be EQUAL to the number of cells of the memory.
// The init strings can be NULL. It means the result is implementation-dependent.
// If strings_alloc=true, at component free the INIT strings are also freed.
void Netlist_Comp_XilBram_InitValues_Free(netlist_comp_t* comp) {
	netlist_xilbram_t* xilbram_data = comp->data;
	if(xilbram_data->init_vex==NULL) return;
	for(unsigned i=0; i<xilbram_data->cells_nb; i++) {
		if(xilbram_data->init_vex[i]!=NULL) hvex_free(xilbram_data->init_vex[i]);
	}
	free(xilbram_data->init_vex);
	xilbram_data->init_vex = NULL;
}

static void Netlist_Comp_XilBram_Free_Data(netlist_comp_t* comp) {
	netlist_xilbram_t* xilbram_data = comp->data;
	Netlist_Comp_XilBram_InitValues_Free(comp);
	free(xilbram_data);
}
static void Netlist_Comp_XilBram_Dup(avl_pp_tree_t* tree_old2new, netlist_comp_t* comp, netlist_comp_t* newcomp) {
	netlist_xilbram_t* xilbram_data = comp->data;
	netlist_xilbram_t* newxilbram_data = newcomp->data;

	newxilbram_data->data_width = xilbram_data->data_width;
	newxilbram_data->addr_width = xilbram_data->addr_width;
	newxilbram_data->cells_nb   = xilbram_data->cells_nb;

	void* pointer_old2new(void* oldptr) {
		void* newptr = NULL;
		avl_pp_find_data(tree_old2new, oldptr, &newptr);
		return newptr;
	}

	newxilbram_data->port_clk = pointer_old2new(xilbram_data->port_clk);

	newxilbram_data->port_rd = pointer_old2new(xilbram_data->port_rd);
	newxilbram_data->port_ra = pointer_old2new(xilbram_data->port_ra);
	newxilbram_data->port_re = pointer_old2new(xilbram_data->port_re);

	newxilbram_data->port_wd = pointer_old2new(xilbram_data->port_wd);
	newxilbram_data->port_wa = pointer_old2new(xilbram_data->port_wa);
	newxilbram_data->port_we = pointer_old2new(xilbram_data->port_we);

	// The Init values
	if(xilbram_data->init_vex!=NULL) {
		newxilbram_data->init_vex = malloc(xilbram_data->cells_nb * sizeof(*newxilbram_data->init_vex));
		for(unsigned i=0; i<newxilbram_data->cells_nb; i++) {
			newxilbram_data->init_vex[i] = hvex_dup(xilbram_data->init_vex[i]);
		}
	}
}

// Note: Documentation: see UG473 - 7 Series FPGAs Memory Resources
static void Netlist_Comp_XilBram_GenVHDL(FILE* F, netlist_vhdl_t* params, netlist_comp_t* comp) {

	fprintf(F,
		"\n"
		"library ieee;\n"
		"use ieee.numeric_std.all;\n"
		"\n"
		"library unimacro;\n"
		"use unimacro.vcomponents.all;\n"
		"\n"
	);

	Netlist_GenVHDL_Entity(F, comp, params);

	netlist_xilbram_t* xilbram_data = comp->data;

	unsigned blocks_nb_height = 0;
	unsigned blocks_nb_width = 0;

	unsigned block_data_width = 0;
	unsigned block_datap_width = 0;
	unsigned block_addr_width = 0;

	bool     addr_reg_need = false;
	unsigned addr_reg_width = 0;

	// Possible configurations for bram36k blocks (lines x width):
	//     512 x 72
	//    1024 x 36
	//    2048 x 18
	//    4096 x 9
	//    8192 x 4
	//   16384 x 2
	//   32768 x 1
	// FIXME Not using cascaded RAM blocks for now

	if(xilbram_data->cells_nb <= 512) {
		// Use 512 x 72 configuration
		block_data_width = 72;
		block_datap_width = 8;
		block_addr_width = 9;
		blocks_nb_height = 1;
		blocks_nb_width  = (xilbram_data->data_width + block_data_width - 1) / block_data_width;
	}

	else if(xilbram_data->cells_nb <= 1024) {
		// Use 1024 x 36 configuration
		block_data_width = 36;
		block_datap_width = 4;
		block_addr_width = 10;
		blocks_nb_height = 1;
		blocks_nb_width  = (xilbram_data->data_width + block_data_width - 1) / block_data_width;
	}

	else if(xilbram_data->cells_nb <= 2048) {
		// Use 2048 x 18 configuration
		block_data_width = 18;
		block_datap_width = 2;
		block_addr_width = 11;
		blocks_nb_height = 1;
		blocks_nb_width  = (xilbram_data->data_width + block_data_width - 1) / block_data_width;
	}

	else if(xilbram_data->cells_nb <= 4096) {
		// Use 4096 x 9 configuration
		block_data_width = 9;
		block_datap_width = 1;
		block_addr_width = 12;
		blocks_nb_height = 1;
		blocks_nb_width  = (xilbram_data->data_width + block_data_width - 1) / block_data_width;
	}

	else if(xilbram_data->cells_nb <= 8192) {
		// Use 8192 x 4 configuration
		block_data_width = 4;
		block_addr_width = 13;
		blocks_nb_height = 1;
		blocks_nb_width  = (xilbram_data->data_width + block_data_width - 1) / block_data_width;
	}

	else if(xilbram_data->cells_nb <= 16384) {
		// Use 16384 x 2 configuration
		block_data_width = 2;
		block_addr_width = 14;
		blocks_nb_height = 1;
		blocks_nb_width  = (xilbram_data->data_width + block_data_width - 1) / block_data_width;
	}

	else if(xilbram_data->cells_nb <= 32768) {
		// Use 32768 x 1 configuration
		block_data_width = 1;
		block_addr_width = 15;
		blocks_nb_height = 1;
		blocks_nb_width  = (xilbram_data->data_width + block_data_width - 1) / block_data_width;
	}

	else {
		// Use 32768 x 1 configuration
		block_data_width = 1;
		block_addr_width = 15;
		blocks_nb_height = (xilbram_data->cells_nb + 32768 - 1) / 32768;
		blocks_nb_width  = xilbram_data->data_width;
		addr_reg_need    = true;
		addr_reg_width   = xilbram_data->addr_width - block_addr_width;
	}

	unsigned total_bram_width = block_data_width * blocks_nb_width;
	unsigned block_cells_nb   = 1 << block_addr_width;
	unsigned block_use_addrw  = GetMin(block_addr_width, xilbram_data->addr_width);

	fprintfm(F,
		"\n"
		"architecture %s of %s is\n", params->architecture, Netlist_Comp_GetRename_Comp(comp, params),
		"\n"
		"	-- Note: Wanted memory size %u x %u\n", xilbram_data->cells_nb, xilbram_data->data_width,
		"	-- Note: Using configuration %u x %u\n", 1 << block_addr_width, block_data_width,
		"	-- Note: %u blocks in height, %u blocks in width\n", blocks_nb_height, blocks_nb_width,
		"\n",
		NULL
	);

	for(unsigned i=0; i<blocks_nb_height; i++) {
		fprintf(F, "	signal sig_block%u_rd : std_logic_vector(%u downto 0) := (others => '0');\n", i, total_bram_width-1);
		fprintf(F, "	signal sig_block%u_ra : std_logic_vector(%u downto 0) := (others => '0');\n", i, block_addr_width-1);
		fprintf(F, "	signal sig_block%u_wd : std_logic_vector(%u downto 0) := (others => '0');\n", i, total_bram_width-1);
		fprintf(F, "	signal sig_block%u_wa : std_logic_vector(%u downto 0) := (others => '0');\n", i, block_addr_width-1);
		fprintf(F, "	signal sig_block%u_we : std_logic := '0';\n", i);
		fprintf(F, "\n");
	}

	if(addr_reg_need==true) {
		fprintf(F, "	signal reg_ra     : std_logic_vector(%u downto 0) := (others => '0');\n", addr_reg_width-1);
		fprintf(F, "	signal sig_mux_rd : std_logic_vector(%u downto 0) := (others => '0');\n", xilbram_data->data_width-1);
		fprintf(F, "\n");
	}

	fprintf(F,
		"begin\n"
		"\n"
		"	-- Assign read-side signals\n"
		"\n"
	);

	if(addr_reg_need==true) {
		fprintfm(F,
			"	process(%s)\n", xilbram_data->port_clk->name,
			"	begin\n"
			"		if %s(%s) then\n", Netlist_GenVHDL_ClockEdge_TestFunc_port(xilbram_data->port_clk), xilbram_data->port_clk->name,
			"			if %s = '1' then\n", xilbram_data->port_re->name,
			"				reg_ra <= %s(%u downto %u);\n", xilbram_data->port_ra->name, xilbram_data->addr_width-1, block_addr_width,
			"			end if;\n"
			"		end if;\n"
			"	end process;\n"
			"\n",
			NULL
		);
	}

	for(unsigned i=0; i<blocks_nb_height; i++) {
		if(xilbram_data->addr_width==1) fprintf(F, "	sig_block%u_ra(%u) <= %s;\n", i, block_addr_width-1, xilbram_data->port_ra->name);
		else                            fprintf(F, "	sig_block%u_ra(%u downto 0) <= %s(%u downto 0);\n", i, block_use_addrw-1, xilbram_data->port_ra->name, block_use_addrw-1);
	}
	fprintf(F, "\n");

	if(xilbram_data->port_wd != NULL) {
		fprintf(F,
			"	-- Assign write-side signals\n"
			"\n"
		);
		for(unsigned i=0; i<blocks_nb_height; i++) {
			if(xilbram_data->data_width==1) fprintf(F, "	sig_block%u_wd(%u) <= %s;\n", i, total_bram_width-1, xilbram_data->port_wd->name);
			else                            fprintf(F, "	sig_block%u_wd(%u downto 0) <= %s(%u downto 0);\n", i, xilbram_data->data_width-1, xilbram_data->port_wd->name, xilbram_data->data_width-1);
			if(xilbram_data->addr_width==1) fprintf(F, "	sig_block%u_wa(%u) <= %s;\n", i, block_addr_width-1, xilbram_data->port_wa->name);
			else                            fprintf(F, "	sig_block%u_wa(%u downto 0) <= %s(%u downto 0);\n", i, block_use_addrw-1, xilbram_data->port_wa->name, block_use_addrw-1);
			if(blocks_nb_height==1) {
				fprintf(F, "	sig_block%u_we <= %s;\n", i, xilbram_data->port_we->name);
			}
			else {
				fprintf(F, "	sig_block%u_we <= %s when to_unsigned(sig_mux_rd) = %u else '0';\n", i, xilbram_data->port_we->name, i);
			}
			fprintf(F, "\n");
		}
	}

	fprintf(F,
		"	-- Instantiate the RAM blocks\n"
		"\n"
	);

	for(unsigned h=0; h<blocks_nb_height; h++) {
		for(unsigned w=0; w<blocks_nb_width; w++) {

			fprintfm(F,
				"	bram_h%uw%u_i : BRAM_SDP_MACRO\n", h, w,
				"		generic map (\n"
				"			BRAM_SIZE => \"36Kb\",  -- Target BRAM, 18Kb or 36Kb\n"
				"			DEVICE => \"7SERIES\",  -- Target device: VIRTEX5, VIRTEX6, 7SERIES, SPARTAN6\n"
				"			WRITE_WIDTH => %u,    -- Valid values are 1-72 (37-72 only valid when BRAM_SIZE = 36Kb)\n", block_data_width,
				"			READ_WIDTH => %u,     -- Valid values are 1-72 (37-72 only valid when BRAM_SIZE = 36Kb)\n", block_data_width,
				"			DO_REG => 0,          -- Optional output register (0 or 1)\n"
				"			-- Set/Reset value for port output\n"
				"			SRVAL => X\"000000000000000000\",\n"
				"			-- Specify READ_FIRST for same clock or synchronous clocks, or WRITE_FIRST for asynchrononous clocks on ports\n"
				"			WRITE_MODE => \"READ_FIRST\",\n"
				"			-- Initial values on output port\n"
				"			INIT => X\"000000000000000000\",\n"
				"			-- Initial values when stored in a file\n"
				"			INIT_FILE => \"NONE\"",
				NULL
			);

			if(xilbram_data->init_vex == NULL) {
				fprintf(F, "\n");
			}
			else {
				fprintf(F, ",\n");
				fprintf(F, "			-- Initial values\n");

				// First step: write the init part that doesn't contain parity bits
				// Note: The parity bits are the MSB of the data port

				unsigned data_right = w * block_data_width;
				unsigned data_left = w * block_data_width + (block_data_width - block_datap_width) - 1;
				if(data_left > xilbram_data->data_width - 1) data_left = xilbram_data->data_width - 1;
				assert(data_left >= data_right);
				unsigned data_width = data_left - data_right + 1;

				unsigned addr_min = h * block_cells_nb;
				unsigned addr_max = h * block_cells_nb + block_cells_nb - 1;
				if(addr_max > xilbram_data->cells_nb - 1) addr_max = xilbram_data->cells_nb - 1;
				assert(addr_max >= addr_min);

				unsigned data_per_init = 256 / (block_data_width - block_datap_width);
				char init_array[257];

				unsigned init_cur_id = 0;
				unsigned addr_cur = addr_min;

				// Write the INIT attributes one by one
				do {

					// Prepare one INIT attribute
					for(unsigned i=0; i<256; i++) init_array[i] = '0';
					init_array[256] = 0;
					unsigned array_idx_cur = 0;
					for(unsigned i=0; i<data_per_init; i++) {
						hvex_t* vex = xilbram_data->init_vex[addr_cur];
						// Add that data chunk
						if(vex != NULL) {
							char* lit = hvex_lit_getval(vex);
							for(unsigned k=0; k<data_width; k++) {
								init_array[256 - 1 - array_idx_cur - k] = lit[vex->width - 1 - data_right - k];
							}
						}
						// Increments and exit conditions
						array_idx_cur += block_data_width - block_datap_width;
						if(addr_cur >= addr_max) break;
						addr_cur ++;
					}

					// Generate the attribute
					if(init_cur_id > 0) fprintf(F, ",\n");
					fprintf(F, "			INIT_%02X => X\"", init_cur_id);
					char* ptr = init_array;
					for(unsigned i=0; i<64; i++) {
						unsigned digit = 0;
						for(unsigned k=0; k<4; k++) { digit = (digit << 1) + (*ptr == '0' ? 0 : 1); ptr++; }
						fprintf(F, "%X", digit);
					}
					fprintf(F, "\"");

					// Exit conditions
					if(addr_cur >= addr_max) break;
					init_cur_id ++;
					assert(init_cur_id < 128);

				} while(1);  // Write the INIT attribute one by one


				// Second step: write the INITP attributes, about parity bits (only if needed)

				unsigned do_parity = true;
				if(w == blocks_nb_width - 1 && data_width <= block_data_width) do_parity = false;

				if(block_datap_width > 0 && do_parity==true) {

					data_right = w * block_data_width + (block_data_width - block_datap_width);
					data_left = w * block_data_width + block_data_width - 1;
					if(data_left > xilbram_data->data_width - 1) data_left = xilbram_data->data_width - 1;
					assert(data_left >= data_right);
					data_width = data_left - data_right + 1;

					data_per_init = 256 / block_datap_width;

					init_cur_id = 0;
					addr_cur = addr_min;

					// Write the INITP attributes one by one
					do {

						// Prepare one INIT attribute
						for(unsigned i=0; i<256; i++) init_array[i] = '0';
						init_array[256] = 0;
						unsigned array_idx_cur = 0;
						for(unsigned i=0; i<data_per_init; i++) {
							hvex_t* vex = xilbram_data->init_vex[addr_cur];
							// Add that data chunk
							if(vex != NULL) {
								char* lit = hvex_lit_getval(vex);
								for(unsigned k=0; k<data_width; k++) {
									init_array[256 - 1 - array_idx_cur - k] = lit[vex->width - 1 - data_right - k];
								}
							}
							// Increments and exit conditions
							array_idx_cur += block_datap_width;
							if(addr_cur >= addr_max) break;
							addr_cur ++;
						}

						// Generate the attribute
						fprintf(F, ",\n");
						fprintf(F, "			INITP_%02X => X\"", init_cur_id);
						char* ptr = init_array;
						for(unsigned i=0; i<64; i++) {
							unsigned digit = 0;
							for(unsigned k=0; k<4; k++) { digit = (digit << 1) + (*ptr == '0' ? 0 : 1); ptr++; }
							fprintf(F, "%X", digit);
						}
						fprintf(F, "\"");

						// Exit conditions
						if(addr_cur >= addr_max) break;
						init_cur_id ++;
						assert(init_cur_id < 16);

					} while(1);  // Write the INIT attribute one by one

				}  // End writing of INITP attributes, for parity bits

				fprintf(F, "\n");

			}  // End write INIT attributes

			fprintfm(F,
				"		)\n"
				"		port map (\n"
				"			-- Output read data port, width defined by READ_WIDTH parameter\n"
				"			DO     => sig_block%u_rd(%u downto %u),\n", h, (w+1) * block_data_width - 1, w * block_data_width,
				"			-- Input write data port, width defined by WRITE_WIDTH parameter\n"
				"			DI     => sig_block%u_wd(%u downto %u),\n", h, (w+1) * block_data_width - 1, w * block_data_width,
				"			-- Input read address, width defined by read port depth\n"
				"			RDADDR => sig_block%u_ra,\n", h,
				"			-- 1-bit input read clock\n"
				"			RDCLK  => clk,\n"
				"			-- 1-bit input read port enable\n"
				"			RDEN   => %s,\n", xilbram_data->port_re->name,
				"			-- 1-bit input read output register clock enable\n"
				"			REGCE  => '1',\n"
				"			-- 1-bit input reset\n"
				"			RST    => '0',\n"
				"			-- Input write enable, width defined by write port depth\n"
				"			WE     => \"11111111\",\n"
				"			-- Input write address, width defined by write port depth\n"
				"			WRADDR => sig_block%u_wa,\n", h,
				"			-- 1-bit input write clock\n"
				"			WRCLK  => clk,\n"
				"			-- 1-bit input write port enable\n"
				"			WREN   => sig_block%u_we\n", h,
				"		);\n"
				"\n",
				NULL
			);

		}
	}

	fprintf(F,
		"	-- Assignment of output port\n"
		"\n"
	);

	if(blocks_nb_height == 1) {
		if(xilbram_data->data_width == 1) fprintf(F, "	%s <= sig_block0_rd(0);\n", xilbram_data->port_rd->name);
		else                              fprintf(F, "	%s <= sig_block0_rd(%u downto 0);\n", xilbram_data->port_rd->name, xilbram_data->data_width-1);
	}
	else {
		// Note: Using an intermediate signal for genericity and simplicity about std_logic vs. vector
		fprintf(F, "	sig_mux_rd <=\n");
		for(unsigned h=0; h<blocks_nb_height; h++) {
			fprintf(F, "		sig_block%u_rd(%u downto 0) when unsigned(reg_ra) = %u else\n", h, xilbram_data->data_width-1, h);
		}
		fprintf(F, "		(others => '0');\n");
		fprintf(F, "\n");
		if(xilbram_data->data_width == 1) fprintf(F, "	%s <= sig_mux_rd(0);\n", xilbram_data->port_rd->name);
		else                              fprintf(F, "	%s <= sig_mux_rd(%u downto 0);\n", xilbram_data->port_rd->name, xilbram_data->data_width-1);
	}

	fprintf(F,
		"\n"
		"end architecture;\n"
		"\n"
	);

}

netlist_comp_model_t* Netlist_Comp_XilBram_GetModel() {
	netlist_comp_model_t* model = Netlist_CompModel_New();
	model->name = namealloc("xilbram");

	model->data_size         = sizeof(netlist_xilbram_t);
	model->func_free_data    = Netlist_Comp_XilBram_Free_Data;
	model->func_dup_internal = Netlist_Comp_XilBram_Dup;
	model->func_gen_vhdl     = Netlist_Comp_XilBram_GenVHDL;

	NETLIST_COMP_XILBRAM = model;
	return model;
}


