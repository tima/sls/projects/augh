
#ifndef _ZYBO_H_
#define _ZYBO_H_


// These declarations are not meant to be used by the user.
// This is to prevent the user to re-declare them without knowing their use.
bool zybo_clock125m;

// Miscellaneous I/O
uint4_t zybo_sw4;
uint4_t zybo_btn4;
uint4_t zybo_led4;

#if 0  // Pmod connectors. Disabled to implement a UART with JE.
// Most significant bits are pins 10-7, least significan bits are pins 4-1
uint8_t zybo_pmod_a;  // XADC
uint8_t zybo_pmod_b;  // High speed
uint8_t zybo_pmod_c;  // High speed
uint8_t zybo_pmod_d;  // High speed
uint8_t zybo_pmod_e;  // Standard

// Note: the board has another Pmod connector, JF, but it's connected to MIO
#endif

#if 1  // Implement a UART with Pmod JE
bool zybo_uart_je_rx;
bool zybo_uart_je_tx;
#endif


#endif  // _ZYBO_H_

