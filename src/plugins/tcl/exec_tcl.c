
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <tcl.h>

#include "../../augh/auto/auto.h"
#include "../../augh/plugins/plugins.h"
#include "../../augh/auto/command.h"
#include "exec_tcl.h"


typedef struct command_tcl_t  command_tcl_t;
struct command_tcl_t {
	implem_t*   Implem;
	Tcl_Interp* interp;
	// To process command
	int         objc;
	Tcl_Obj**   objv;
	int         idx;
};


//================================
// The command callbacks
//================================

static int callback_cmd_augh(ClientData clientData, Tcl_Interp *interp, int objc, Tcl_Obj *const objv[]){
	command_t* cmd_data = clientData;
	command_tcl_t* cmd_tcl = cmd_data->data;

	// Set the objv[] in the command interpreter data structure
	cmd_tcl->objc = objc;
	cmd_tcl->objv = (Tcl_Obj**)objv;
	cmd_tcl->idx  = 0;

	// Get the main command
	const char* cmd = Tcl_GetString(objv[0]);

	// Special command "augh"
	if(strcmp(cmd, "augh")==0) {
		// Sanity check
		if (objc < 2) {
			char errmsg[100];
			sprintf(errmsg, "Error [TCL interpreter] : Command '%s' : Too few arguments", Tcl_GetString(objv[0]));
			Tcl_SetResult(interp, errmsg, TCL_VOLATILE);
			return TCL_ERROR;
		}
		// Skip the word "augh"
		cmd_tcl->idx = 1;
	}

	// Execute the command with augh
	int z = AughCmd_Exec(cmd_tcl->Implem, cmd_data);

	if(z!=0) return TCL_ERROR;
	return TCL_OK;
}

// Declare our new TCL commands
static int Declare_TCL_commands(Tcl_Interp *interp, command_t* cmd_data) {

	// The command "augh" enables to launch everything within augh
	Tcl_CreateObjCommand(interp, "augh", callback_cmd_augh, (ClientData)cmd_data, NULL);

	// Sub-command interpreters within augh
	Tcl_CreateObjCommand(interp, "build",     callback_cmd_augh, (ClientData)cmd_data, NULL);
	Tcl_CreateObjCommand(interp, "hier",      callback_cmd_augh, (ClientData)cmd_data, NULL);
	Tcl_CreateObjCommand(interp, "techno",    callback_cmd_augh, (ClientData)cmd_data, NULL);
	Tcl_CreateObjCommand(interp, "sched",     callback_cmd_augh, (ClientData)cmd_data, NULL);
	Tcl_CreateObjCommand(interp, "op",        callback_cmd_augh, (ClientData)cmd_data, NULL);
	Tcl_CreateObjCommand(interp, "forksynth", callback_cmd_augh, (ClientData)cmd_data, NULL);
	Tcl_CreateObjCommand(interp, "core",      callback_cmd_augh, (ClientData)cmd_data, NULL);
	Tcl_CreateObjCommand(interp, "map",       callback_cmd_augh, (ClientData)cmd_data, NULL);
	Tcl_CreateObjCommand(interp, "netlist",   callback_cmd_augh, (ClientData)cmd_data, NULL);
	Tcl_CreateObjCommand(interp, "plugin",    callback_cmd_augh, (ClientData)cmd_data, NULL);
	Tcl_CreateObjCommand(interp, "impmod",    callback_cmd_augh, (ClientData)cmd_data, NULL);

	// Main top-level augh commands, just a subset for now...
	Tcl_CreateObjCommand(interp, "postprocess", callback_cmd_augh, (ClientData)cmd_data, NULL);
	Tcl_CreateObjCommand(interp, "vhdl",        callback_cmd_augh, (ClientData)cmd_data, NULL);

	return TCL_OK;
}

// The callback to get one command word, for AUGH embedded command interpreter
static const char* Command_callback_nextword(command_t* cmd_data) {
	command_tcl_t* cmd_tcl = cmd_data->data;
	if(cmd_tcl->idx >= cmd_tcl->objc) return NULL;
	return Tcl_GetString(cmd_tcl->objv[cmd_tcl->idx++]);
}

// The callback to get one command word, for AUGH embedded command interpreter
static void Command_callback_result_finish(command_t* cmd_data) {
	if(cmd_data->list_res == NULL) return;

	command_tcl_t* cmd_tcl = cmd_data->data;
	Tcl_Interp* interp = cmd_tcl->interp;

	// One element
	if(cmd_data->list_res->NEXT == NULL) {
		Tcl_Obj* return_val = Tcl_NewStringObj((char*)cmd_data->list_res->DATA, -1);
		Tcl_SetObjResult(interp, return_val);
	}
	// Multiple elements
	else {
		cmd_data->list_res = reverse(cmd_data->list_res);
		Tcl_Obj* return_list = Tcl_NewListObj(0, NULL);
		foreach(cmd_data->list_res, scanRes) {
			Tcl_Obj* val = Tcl_NewStringObj((char*)scanRes->DATA, -1);
			Tcl_ListObjAppendElement(interp, return_list, val);
		}
		Tcl_SetObjResult(interp, return_list);
	}

}

// Main function to call TCL interpreter
int exec_tcl_file(implem_t* Implem, const char* filename) {
	int z = 0;

	// Allocate TCL interpreter
	Tcl_Interp *interp = Tcl_CreateInterp();

	// Data structure to launch execution of commands
	command_t cmd_data_i;
	command_t* cmd_data = &cmd_data_i;
	memset(cmd_data, 0, sizeof(*cmd_data));
	// Data structure to store status of parsing of current command
	command_tcl_t cmd_tcl_i;
	command_tcl_t* cmd_tcl = &cmd_tcl_i;
	memset(cmd_tcl,  0, sizeof(*cmd_tcl));

	// Init fields
	cmd_data->data = cmd_tcl;
	cmd_data->callback_nextword = Command_callback_nextword;
	cmd_data->callback_result_finish = Command_callback_result_finish;
	// Init fields
	cmd_tcl->Implem = Implem;
	cmd_tcl->interp = interp;

	// Initialize TCL interpreter
	z = Tcl_Init(interp);
	if(z != TCL_OK) {
		fprintf(stderr ,"Tcl_Init error: %s\n", Tcl_GetStringResult(interp));
		Tcl_Exit(EXIT_FAILURE);
	}

	// Declare our commands
	z = Declare_TCL_commands(interp, cmd_data);
	if(z != TCL_OK) {
		fprintf(stderr ,"ExtendTcl error: %s\n", Tcl_GetStringResult(interp));
		Tcl_Exit(EXIT_FAILURE);
	}

	// Execute the TCL script file
	z = Tcl_EvalFile(interp, filename);
	if(z == TCL_ERROR) {
		fprintf(stderr ,"Tcl_Eval error: %s\n", Tcl_GetStringResult(interp));
		Tcl_Exit(EXIT_FAILURE);
	}

	// Clean
	Tcl_Finalize();

	return 0;
}

