#include "../../augh/chain.h"
#include "../../augh/auto/auto.h"

// modified copy of Hier_Latency_GetOrderedList to support sorting by post-map
chain_pd_t* Hier_DelayPostmapOrNot_GetOrderedList(implem_t* Implem,int postmap) {
  hier_t* H = Implem->H;

  chain_pd_t* list_trans = NULL;

  avl_p_foreach(&H->NODES[HIERARCHY_STATE], scanNode) {
  	hier_node_t* node = scanNode->data;
  	hier_state_t* state_data = &node->NODE.STATE;
  	if(state_data->time.delay<=0) continue;
  	list_trans = chain_pd_add(list_trans, node,(!postmap)?state_data->time.delay:state_data->time.map_delay);
  }  // Scan all the hierarchy nodes
  
  list_trans = chain_pd_sort_dec(list_trans);
  return list_trans;
}

double worst_delay(implem_t* Implem,int postmap)
{
	chain_pd_t* list_trans = Hier_DelayPostmapOrNot_GetOrderedList(Implem,postmap);
  hier_node_t *firstnode=NULL;
  double delay;

	foreach(list_trans, scanNode) {
		firstnode = scanNode->p;
    break;
	}

  if (!firstnode)
    delay=0.0;
  else
  {
    if (!postmap)
      delay=firstnode->NODE.STATE.time.delay;
    else
      delay=firstnode->NODE.STATE.time.map_delay;
  }

  chain_pd_del(list_trans);
  return delay;
}
