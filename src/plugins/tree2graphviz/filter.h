#ifndef I_FILTER
#define I_FILTER

typedef struct struct_t_filterdata {
  // public
  FILE *input; // a stream file, tmpfile(), use as buffer.
  // private
  //nothing
} t_filterdata;

int  filter_init( t_filterdata *filterdata);
void filter_close(t_filterdata *filterdata);

int filter_actions(     FILE *outputfile,t_filterdata *filterdata,t_graphvizopt *opt);
int filter_oneline_hvex(FILE *outputfile,t_filterdata *filterdata,t_graphvizopt *opt);
int filter_dotattribut( FILE *outputfile,t_filterdata *filterdata,t_graphvizopt *opt);

#endif
