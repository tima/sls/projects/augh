#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <math.h>
#include "../../augh/auto/auto.h"
#include "../../augh/auto/command.h"
#include "../../augh/plugins/plugins.h"
#include "../../augh/netlist/netlist_comps.h"
#include "graphviz.h"
#include "filter.h"
#include "delay.h"

// inspired form Hier_Print_raw in
// ../../augh/auto/hierarchy.c

// TODO:
//   * force horizontal position (pos for the graphviz tool neato)
//   * use state->index to print fsm state_cur bit associated to
//     each state for allowing comparing simulated wave and our
//     plotted graph.

void indent(FILE* file,int level)
{
  int i;
  for(i=0;i<level+1;i++)
    fprintf(file,"  ");
}

int print_lines(FILE *file,const char *attribut,bitype_list* source_lines,t_filterdata *filterdata,t_graphvizopt *opt)
{
  if (source_lines)
  {
    if (attribut)
      fprintf(file,"%s=\"",attribut);

    foreach(source_lines,scan_source_lines)
    {
      if (source_lines!=scan_source_lines)
        fprintf(filterdata->input," ");
      fprintf(filterdata->input,"%s",(char*)(scan_source_lines->DATA_FROM));
      foreach((num_list*)scan_source_lines->DATA_TO, scan_line) {
        fprintf(filterdata->input,":%ld",scan_line->DATA);
      }
    }
    if (!(filter_dotattribut(file,filterdata,opt))) {
      printf("Error: print lines failed: filter failed\n");
      return 0;
    }

    if (attribut)
      fprintf(file,"\"");
  }
  return 1;
}

void hvex_fprint_opt(FILE* F, hvex_t* vex,t_graphvizopt *opt)
{
  if (opt->hvexprintbracket)
    hvex_fprint_b(F,vex);
  else
    hvex_fprint(F,vex);
}

// identify the fsm index in the netlist of the state "node"
// inspired from Map_Netlist_DumpFsmStates
int get_state_fsm_index(hier_node_t *node,t_graphvizopt *opt,unsigned *pstate_index)
{
  if (!(opt->comp_fsm))
    return 0;
  if (!node)
    return 0;
  if (!pstate_index)
    return 0;

  foreach(opt->comp_fsm->states, scanState) {
    netlist_fsm_state_t* state = scanState->DATA;
    hier_node_t* state_node = Map_FsmState2Node(opt->map_data, state);

    if (state_node==node)
    {
      (*pstate_index) = state->index;
      return 1;
    }
  }
  return 0;
}

int print_node(FILE* file,hier_node_t *node,t_filterdata *filterdata,t_graphvizopt *opt,int level,int *cpt,char *req_nodename_start,char *req_nodename_end)
{
  #define NAMELEN 4096
  char nodename_start[NAMELEN];
  char nodename_end[NAMELEN];
  char previousnodename[NAMELEN];
  char *previoustransitionoption=NULL;
  char     *nexttransitionoption=NULL;
  int first_start=1;
  int nbactions;
  double delay,delaynorm;
  unsigned state_index;

  nodename_start[NAMELEN-1]='\0';
  nodename_end[NAMELEN-1]='\0';
  previousnodename[0]='\0';
  while (node)
  {
    nodename_start[0]='\0';
    nodename_end[0]='\0';
    if (opt->commentdebug) {
      indent(file,level); fprintf(file,"// a node (%d) %d\n",*cpt,node->TYPE);
    }
    switch(node->TYPE) {

      case HIERARCHY_PROCESS:
        snprintf(nodename_end,NAMELEN-1,"\"process_%s\"",node->NODE.PROCESS.name_orig);
        indent(file,level); fprintf(file,"%s [shape=invhouse,label=\"%s\"",nodename_end,node->NODE.PROCESS.name_orig);
        if (!print_lines(file,",tooltip",node->source_lines,filterdata,opt))
          return 0;
        fprintf(file,"];\n");
        break;

      case HIERARCHY_BB:
        if (opt->commentdebug) {
          indent(file,level); fprintf(file,"// basic block (%d): %d BBs\n",*cpt,Hier_Level_GetNbOfNodes(node->NODE.BB.body));
        }
        indent(file,level); fprintf(file,"// basic block (%d): %d BBs\n",*cpt,Hier_Level_GetNbOfNodes(node->NODE.BB.body));
        indent(file,level); fprintf(file,"// ");
          if (!print_lines(file,"debug BB",node->source_lines,filterdata,opt))
            return 0;
          fprintf(file,"\n");

        if (!print_node(file,node->NODE.BB.body,filterdata,opt,level+1,cpt,nodename_start,nodename_end))
          return 0;
        break;

      case HIERARCHY_STATE:
        if (opt->commentdebug) {
          indent(file,level); fprintf(file,"// state (%d): %d actions\n",*cpt,ChainList_Count(node->NODE.STATE.actions));
        }
        snprintf(nodename_start,NAMELEN-1,"state_%d",*cpt);
        memcpy(nodename_end,nodename_start,NAMELEN);
        if (opt->printactions) {
          indent(file,level); fprintf(file,"%s [shape=plaintext,label=<<TABLE BORDER=\"0\" CELLBORDER=\"1\" CELLSPACING=\"0\" CELLPADDING=\"1\">\n",nodename_start);
          nbactions=0;
          foreach(node->NODE.STATE.actions, scanAction) {
            nbactions++;
            hier_action_t* action = scanAction->DATA;
            indent(file,level+2); fprintf(file,"<TR><TD  HREF=\"#\" TITLE=\"");
            // warning map delays are available only if the command
            //   map postmap-timing true
            // was executed before elaboration
            fprintf(file,"delay=%g ns(%d cycles), map_delay=%g ns(%d cycles)",
                         action->delay,action->nb_clk,
                         node->NODE.STATE.time.map_delay,node->NODE.STATE.time.map_nb_clk);
            if (get_state_fsm_index(node,opt,&state_index)) // could be called only one time by state
            {
              fprintf(file,", fsm=%u",state_index);
            }
            if (action->source_lines)
            {
              fprintf(file,", ");
              if (!print_lines(file,NULL,action->source_lines,filterdata,opt))
                return 0;
            }
            fprintf(file,"\"");
            if (opt->colorwhat)
            {
              delay=0.0;
              if (opt->colorwhat==1) // premap
                delay=action->delay;
              else if (opt->colorwhat==2) // postmap
                delay=node->NODE.STATE.time.map_delay;
              delaynorm=(delay - opt->delaymin)/(opt->delaymax - opt->delaymin);
              if (opt->nbcolor > 0)
                delaynorm=                         round(delaynorm*((    opt->nbcolor)-1.0))+1.0;
              else // reverse color order
                delaynorm=((0.0-opt->nbcolor)*1.0)-round(delaynorm*((0.0-opt->nbcolor)-1.0));
              fprintf(file," BGCOLOR=\"%d\"",(int)delaynorm);
            }
            fprintf(file,">");
            // insert title by print line
            hvex_fprint_opt(filterdata->input,action->expr,opt);
            if (!(filter_actions(file,filterdata,opt))) {
              printf("Error: print action in states failed (%d): filter failed\n",*cpt);
              return 0;
            }
            fprintf(file,"</TD></TR>\n");
          }
          if (nbactions==0)
          {
            indent(file,level+2); fprintf(file,"<TR><TD>");
                                  fprintf(file,"no action (empty state)");
                                  fprintf(file,"</TD></TR>\n");

          }
          indent(file,level+1);
            fprintf(file,"</TABLE>>");
          //if (!print_lines(file,",tooltip",node->source_lines,filterdata,opt))
          //  return 0;
          fprintf(file,"];\n");
        }
        else {
          indent(file,level); fprintf(file,"%s [shape=box,label=\"%d actions\"",nodename_start,ChainList_Count(node->NODE.STATE.actions));
          if (!print_lines(file,",tooltip",node->source_lines,filterdata,opt))
            return 0;
          fprintf(file,"];\n");
        }
        break;

      case HIERARCHY_SWITCH: {
        int switchcpt=(*cpt);
        if (opt->commentdebug) {
          indent(file,level); fprintf(file,"// switch (%d)\n",switchcpt);
        }
        snprintf(nodename_start,NAMELEN-1,"switch_start_test_%d",switchcpt);
        snprintf(nodename_end,NAMELEN-1,"switch_end_%d",switchcpt);
        if (node->NODE.SWITCH.info.switch_test) {
          indent(file,level); fprintf(file,"%s [shape=trapezium,label=\"",nodename_start);

            hvex_fprint_opt(filterdata->input,node->NODE.SWITCH.info.switch_test,opt); // write content for the filter
            if (!(filter_oneline_hvex(file,filterdata,opt))) {
              printf("Error: print action in states failed (%d): filter failed\n",*cpt);
              return 0;
            }
            fprintf(file,"\"");
            if (!print_lines(file,",tooltip",node->source_lines,filterdata,opt))
              return 0;
            fprintf(file,"];\n");
        }
        else {
          indent(file,level); fprintf(file,"%s [shape=trapezium,label=\"SWITCH\"",nodename_start); // no test
          if (!print_lines(file,",tooltip",node->source_lines,filterdata,opt))
            return 0;
          fprintf(file,"];\n");
        }
        indent(file,level); fprintf(file,"%s [shape=invtrapezium,label=\"SWITCH END\"];\n",nodename_end);
        foreach(node->CHILDREN, scanChild) {
          hier_node_t *nodecase = scanChild->DATA;
          char case_nodename_start[NAMELEN];
          char case_nodename_end[NAMELEN];
          case_nodename_start[0]='\0';
          case_nodename_end[0]='\0';
          if (!( (nodecase->TYPE) == HIERARCHY_CASE )) {
            printf("Error: one switch has a child that is not a case.\n");
            return 0;
          }
          if (!print_node(file,nodecase->NODE.CASE.body,filterdata,opt,level+1,cpt,case_nodename_start,case_nodename_end))
            return 0;
          if (case_nodename_start[0]) {
            if (!(case_nodename_end[0])) {
              printf("Error: one case with start but no end\n");
              return 0;
            }
            indent(file,level); fprintf(file,"%s -> %s;\n",case_nodename_end,nodename_end);
            indent(file,level); fprintf(file,"%s -> %s [label=\"",nodename_start,case_nodename_start);
          }
          else
          {
            if (case_nodename_end[0]) {
              printf("Error: one case with end but no start\n");
              return 0;
            }
            indent(file,level); fprintf(file,"%s -> %s [label=\"",nodename_start,nodename_end);
          }
          // switch condition
          avl_p_foreach(&(nodecase->NODE.CASE.values), scanval) {
            hier_testinfo_t* testinfo = scanval->data;
            hvex_t* vex_val = Hier_SwitchCase_GetTestValue(nodecase, testinfo);
            if(vex_val!=NULL) {
              fprintf(filterdata->input, "0b%s", hvex_lit_getval(vex_val));
            }
            else {
              hvex_fprint_opt(filterdata->input, testinfo->cond,opt);
            }
            if (avl_p_next(&(nodecase->NODE.CASE.values), scanval))
              fprintf(filterdata->input,"; ");
          }
          if (!(filter_oneline_hvex(file,filterdata,opt))) {
            printf("Error: print switch condition failed (%d): filter failed\n",switchcpt);
            return 0;
          }

          if(nodecase->NODE.CASE.is_default) {
            fprintf(file, "default");
          }
          fprintf(file,"\"]\n");
        }
        break;
      }

      case HIERARCHY_LOOP: {
        int loopcpt=(*cpt);
        char loop_nodename_after_start[NAMELEN];
        char loop_nodename_after_end[NAMELEN];
        char loop_nodename_before_start[NAMELEN];
        char loop_nodename_before_end[NAMELEN];
        loop_nodename_after_start[0]='\0';
        loop_nodename_after_end[0]='\0';
        loop_nodename_before_start[0]='\0';
        loop_nodename_before_end[0]='\0';

        if (opt->commentdebug) {
          indent(file,level); fprintf(file,"// loop (%d)\n",loopcpt);
        }

        // loop nodes:
        if (node->NODE.LOOP.body_after) {
          if (!print_node(file,node->NODE.LOOP.body_after,filterdata,opt,level+1,cpt,loop_nodename_after_start,loop_nodename_after_end))
            return 0;
        }
        if (node->NODE.LOOP.testinfo->cond) {
          indent(file,level);
            fprintf(file,"loop_cond_%d [shape=circle,label=\"",loopcpt);
            if(hier_for_chkok(node->NODE.LOOP.flags))
              fprintf(file,"FOR"); // for loop
            else
              fprintf(file,"LOOP"); // other loop
            fprintf(file,"\"");
            if (!print_lines(file,",tooltip",node->source_lines,filterdata,opt))
              return 0;
            fprintf(file,"];\n");
        }
        if (node->NODE.LOOP.body_before) {
          if (!print_node(file,node->NODE.LOOP.body_before,filterdata,opt,level+1,cpt,loop_nodename_before_start,loop_nodename_before_end))
            return 0;
        }

        // nodename_start (body_after)
        if (node->NODE.LOOP.body_after) {
          memcpy(nodename_start,loop_nodename_after_start,NAMELEN);
        }
        else if (node->NODE.LOOP.testinfo->cond) {
          snprintf(nodename_start,NAMELEN-1,"loop_cond_%d",loopcpt);
        }
        else if (node->NODE.LOOP.body_before) {
          memcpy(nodename_start,loop_nodename_before_start,NAMELEN);
        }
        else {
          printf("Warning: useless condition found (no test, no after, no before) (tree2graphviz plugin)\n");
        }

        // nodename_end (condition)
        if (node->NODE.LOOP.testinfo->cond) {
          snprintf(nodename_end,NAMELEN-1,"loop_cond_%d",loopcpt);
        }
        else if (node->NODE.LOOP.body_after) {
          memcpy(nodename_end,loop_nodename_after_end,NAMELEN);
        }
        else if (node->NODE.LOOP.body_before) {
          memcpy(nodename_end,loop_nodename_before_end,NAMELEN);
        }

        // close the loop with edges
        indent(file,level); fprintf(file,"// |-> ");
        if (node->NODE.LOOP.body_after) {
          fprintf(file,"%s [style=dashed];\n",loop_nodename_after_start);
          indent(file,level); fprintf(file,"%s -> ",loop_nodename_after_end);
        }
        if (node->NODE.LOOP.testinfo->cond) {
          fprintf(file,"loop_cond_%d [style=dashed];\n",loopcpt);
          indent(file,level); fprintf(file,"loop_cond_%d -> ",loopcpt);
        }
        if (node->NODE.LOOP.body_before)
          fprintf(file,"%s [style=dashed",loop_nodename_before_start);
        else
          fprintf(file,"%s [style=dashed",nodename_start);
        if (node->NODE.LOOP.testinfo->cond) {
          fprintf(file,",label=\"");

          hvex_fprint_opt(filterdata->input,node->NODE.LOOP.testinfo->cond,opt); // write content for the filter
          if (!(filter_oneline_hvex(file,filterdata,opt))) {
            printf("Error: print loop condition failed (%d): filter failed\n",loopcpt);
            return 0;
          }
          fprintf(file,"\"");
        }
        fprintf(file,"];\n");
        if (node->NODE.LOOP.body_before) {
          indent(file,level); fprintf(file,"%s -> %s\n;",loop_nodename_before_end,nodename_start);
        }

        //if (node->NODE.LOOP.label_name) {
        //  fprintf(file,"\"label_%s\" [shape=invhouse,label=\"%s\"];\n",node->NODE.LOOP.label_name,node->NODE.LOOP.label_name);
        //  fprintf(file,"\"label_%s\" -> %s;\n",node->NODE.LOOP.label_name,nodename_start);
        //}
        break;
      }

      case HIERARCHY_RETURN:
        if (node->NODE.JUMP.target->TYPE==HIERARCHY_PROCESS) {
          snprintf(nodename_start,NAMELEN-1,"\"return_process_%s\"",node->NODE.JUMP.target->NODE.PROCESS.name_orig);
          indent(file,level); fprintf(file,"%s [shape=invhouse,label=\"%s\"",
                                nodename_start,
                                node->NODE.JUMP.target->NODE.PROCESS.name_orig);
          if (!print_lines(file,",tooltip",node->source_lines,filterdata,opt))
            return 0;
          fprintf(file,"];\n");
        }
        else {
          printf("Error: unknown node (return) type (tree2graphviz plugin)\n");
          return 0;
        }
        break;

      case HIERARCHY_CALL:
        if (node->NODE.JUMP.target->TYPE==HIERARCHY_PROCESS) {
          snprintf(nodename_start,NAMELEN-1,"\"call_%d_%s\"",*cpt,node->NODE.JUMP.target->NODE.PROCESS.name_orig);
          memcpy(nodename_end,nodename_start,NAMELEN);
          indent(file,level); fprintf(file,"%s [shape=hexagon,label=\"%s\"",
                                nodename_start,
                                node->NODE.JUMP.target->NODE.PROCESS.name_orig);
          if (!print_lines(file,",tooltip",node->source_lines,filterdata,opt))
            return 0;
          fprintf(file,"];\n");
        }
        else {
          printf("Error: unknown node (return) type (tree2graphviz plugin)\n");
          return 0;
        }
        break;

  		case HIERARCHY_LABEL:
        snprintf(nodename_start,NAMELEN-1,"\"label_%d\"",*cpt);
        memcpy(nodename_end,nodename_start,NAMELEN);
        indent(file,level); fprintf(file,"%s [shape=note,label=\"",
                              nodename_start);
                            if (node->NODE.LABEL.name)
                              fprintf(file,"%s",node->NODE.LABEL.name);
                            else
                              fprintf(file,"%s","unamed");
                            fprintf(file,"\"");
        if (!print_lines(file,",tooltip",node->source_lines,filterdata,opt))
          return 0;
        fprintf(file,"];\n");
  			break;

    case HIERARCHY_JUMP:
      snprintf(nodename_start,NAMELEN-1,"\"jump_%d\"",*cpt);
      memcpy(nodename_end,nodename_start,NAMELEN);
      nexttransitionoption="style=dashed";
      indent(file,level); fprintf(file,"%s [shape=box3d,label=\"",nodename_start);
      fprintf(file,"jump ");
			switch(node->NODE.JUMP.type) {
				case HIER_JUMP_NONE  : break;
				case HIER_JUMP_BREAK : fprintf(file, "(break)"); break;
				case HIER_JUMP_CONT  : fprintf(file, "(continue)"); break;
				case HIER_JUMP_GOTO  : fprintf(file, "(goto)"); break;
				case HIER_JUMP_CALL  : fprintf(file, "(call)"); break;
				case HIER_JUMP_RET   : fprintf(file, "(return)"); break;
			}
      assert(node->NODE.JUMP.target->TYPE==HIERARCHY_LABEL);
      if(node->NODE.JUMP.target->NODE.LABEL.name)
        fprintf(file," -> %s",node->NODE.JUMP.target->NODE.LABEL.name);
      else
        fprintf(file," -> %s","unnamed");
      fprintf(file,"\"");
      if (!print_lines(file,",tooltip",node->source_lines,filterdata,opt))
        return 0;
      fprintf(file,"];\n");
			break;


      default:
        printf("Error: unknown node type [%s] (tree2graphviz plugin)\n",Hier_NodeType2str(node->TYPE));
        return 0;
    }
    (*cpt)++;


    if ( (first_start) && (nodename_start[0]) ) {
      first_start=0;
      if (req_nodename_start)
        memcpy(req_nodename_start,nodename_start,NAMELEN);
    }

    //fprintf(file,"// [%s] [%s -> %s]\n",previousnodename,nodename_start,nodename_end);
    //if (!(previousnodename[0]))
    //  fprintf(file,"// NO PREV\n");
    //if (!(nodename_start[0]))
    //  fprintf(file,"// NO NOW\n");
    if ( (previousnodename[0]) && (nodename_start[0]) ) {
      if (previoustransitionoption)
      {
        indent(file,level); fprintf(file,"%s -> %s [%s];\n",previousnodename,nodename_start,previoustransitionoption);
      }
      else
      {
        indent(file,level); fprintf(file,"%s -> %s;\n",previousnodename,nodename_start);
      }
      previoustransitionoption=nexttransitionoption;
      nexttransitionoption=NULL;
    }
    memcpy(previousnodename,nodename_end,NAMELEN);

    node=node->NEXT;
  }
  if (req_nodename_end)
    memcpy(req_nodename_end,previousnodename,NAMELEN);
  return 1;
}

int print_graphviz(FILE* file,implem_t* Implem,t_graphvizopt *opt) {
  int cpt=0;
  int colori,colorn;
  hier_t *H=Implem->H;

  // filling the chain for assicating between fsm state and hier node
  // copy from Map_Netlist_DumpFsmStates
  opt->comp_fsm=NULL;
  opt->map_data=NULL;
  if ( (Implem->netlist.fsm) && (Implem->netlist.mapping) )
  {
    opt->map_data=Implem->netlist.mapping;
	  Netlist_Comp_Fsm_SetStateIdx(Implem->netlist.fsm);
	  opt->comp_fsm =                   Implem->netlist.fsm->data;
  }

  if (opt->colorwhat)
  {
    if (opt->colorwhat==1)
    {
      // premap -> no postmap
      opt->delaymax=worst_delay(Implem,0);
    }
    else if (opt->colorwhat==2)
    {
      // postmap
      opt->delaymax=worst_delay(Implem,0);
    }
    else
    {
      fprintf(stderr,"Error: unknown opt->colorwhat (%d) (tree2graphviz plugin)\n",opt->colorwhat);
      return 0;
    }
    opt->delaymin=0.0;
  }
  t_filterdata filterdata;
  if (!filter_init(&filterdata))
  {
    fprintf(stderr,"Error: init filter failed (tree2graphviz plugin)\n");
    return 0;
  }


  fprintf(file,"digraph aughtree {\n"
               "\n");
  if (opt->colorwhat)
  {
    fprintf(file,"  node [colorscheme=rdylgn11];\n");
    opt->nbcolor = -11; // 11 colors used in reverse order
    // legend:
    fprintf(file,"  colorlegend [shape=plaintext,label=<<TABLE BORDER=\"0\" CELLBORDER=\"1\" CELLSPACING=\"0\" CELLPADDING=\"1\">\n");
    if (opt->nbcolor)
      colorn = 0 - opt->nbcolor;
    else
      colorn = opt->nbcolor;
    for (colori=0;colori<colorn;colori++)
    {
      fprintf(file,"    <TR><TD HEIGHT=\"60\" HREF=\"#\" TITLE=\"legend\" BGCOLOR=\"%d\">",(opt->nbcolor>0)?colori+1:(0-opt->nbcolor)-colori);
      fprintf(filterdata.input,"delay >= %g ns",opt->delaymin + (0.0 + colori)*(opt->delaymax - opt->delaymin)/(0.0 + colorn));
      if (!(filter_actions(file,&filterdata,opt))) {
        printf("Error: printing color legend: filter failed\n");
        return 0;
      }
      fprintf(file,"</TD></TR>\n");
    }
    fprintf(file,"  </TABLE>>];\n\n");
  }

  // Print the top-level process
  if(H->PROCESS!=NULL) {
    indent(file,0); fprintf(file,"// main process\n");
    if (!print_node(file,H->PROCESS,&filterdata,opt,0,&cpt,NULL,NULL))
    {
      filter_close(&filterdata);
      return 0;
    }
    fprintf(file,"\n");
  }
  // Print all other processes
  avl_p_foreach(&H->NODES[HIERARCHY_PROCESS], scan) {
    hier_node_t* node = scan->data;
    if(node==H->PROCESS) continue;
    indent(file,0); fprintf(file,"// called process\n");
    if (!print_node(file,node,&filterdata,opt,0,&cpt,NULL,NULL))
    {
      filter_close(&filterdata);
      return 0;
    }
    fprintf(file,"\n");
  }
  // Print all Levels that begin by a Label reachable only through Jumps
  avl_p_foreach(&H->NODES[HIERARCHY_LABEL], scan) {
    hier_node_t* node = scan->data;
    if(node->PREV!=NULL || node->PARENT!=NULL) continue;
    indent(file,0); fprintf(file,"// label\n");
    if (!print_node(file,node,&filterdata,opt,0,&cpt,NULL,NULL))
    {
      filter_close(&filterdata);
      return 0;
    }
    fprintf(file,"\n");
  }

  fprintf(file,"}\n");

  filter_close(&filterdata);
  return 1;
}

