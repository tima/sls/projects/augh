#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <string.h>
#include <ctype.h>
#include "../../augh/auto/auto.h"
#include "../../augh/auto/command.h"
#include "../../augh/plugins/plugins.h"
#include "graphviz.h"
#include "filter.h"

int filter_init(t_filterdata *filterdata)
{
  filterdata->input=tmpfile();
  if (!(filterdata->input))
  {
    perror("Error: filter_init: temp file creating failed\n");
    return 0;
  }
  setlinebuf(filterdata->input); // enable the buffer in the stream (default)
  return 1;
}

void filter_close(t_filterdata *filterdata)
{
  fclose(filterdata->input);
}

int filter_before(t_filterdata *filterdata)
{
  rewind(filterdata->input); // seek to the buffer start
  return 1;
}

int filter_after(t_filterdata *filterdata)
{
  // truncate the temporary file

  setbuf(filterdata->input,NULL); // disable the buffer in the stream
  if (ftruncate(fileno(filterdata->input),0)<0)
  {
    perror("Error: filter_after, pop buffer failed\n");
    return 0;
  }
  setlinebuf(filterdata->input); // re-enable the buffer in the stream (default)
  rewind(filterdata->input); // seek to the buffer start
  return 1;
}

// useful filters:

#define BUFFERLEN 1024

void ignore(char* bufc,const char *toignore,char **ptocopy,FILE *outputfile)
{
  int i,len;
  len=strlen(toignore);

  for (i=0;i<len;i++)
  {
    if ((*bufc)==toignore[i])
    {
      if ((*ptocopy)<bufc) // flush the buffer
        fwrite((*ptocopy),1,bufc-(*ptocopy),outputfile);
      (*ptocopy)=bufc+1; // ignore the current char
      (*bufc)='\0';
      return;
    }
  }
}

void htmlescape(char* bufc,char **ptocopy,FILE *outputfile)
{
  int i,len;
  const char toreplace[]="&<>\"";
  len=strlen(toreplace);

  for (i=0;i<len;i++)
  {
    if ((*bufc)==toreplace[i])
    {
      if ((*ptocopy)<bufc) // flush the buffer
        fwrite((*ptocopy),1,bufc-(*ptocopy),outputfile);
      (*ptocopy)=bufc+1; // ignore the current char

      switch (*bufc)
      {
        case '&':
          fprintf(outputfile,"&amp;");
          break;
        case '<':
          fprintf(outputfile,"&lt;");
          break;
        case '>':
          fprintf(outputfile,"&gt;");
          break;
        case '"':
          fprintf(outputfile,"&quot;");
          break;
      }

      (*bufc)='\0';
      return;
    }
  }
}

#define PARMAXLEN 512
void updatelengthdisplaying(char* bufc,char **ptocopy,FILE *outputfile,int hvexhumanreadablelen,char *leninpar,int *pleninpar_i)
{
  unsigned int right,left,lenght;
  char sign;

  // parse (X:X:X:[us])
  if ( ((*pleninpar_i)<0) && ((*bufc)=='(') )
  {
    if ((*ptocopy)<bufc) // flush the buffer
      fwrite((*ptocopy),1,bufc-(*ptocopy),outputfile);
    (*ptocopy)=bufc+1; // ignore the current char
    (*bufc)='\0';
    (*pleninpar_i)=0;
    return;
  }
  if ((*pleninpar_i)>=0)
  {
    if ( (isdigit(*bufc)) ||
         ((*bufc)==':')   ||
         ((*bufc)=='s')   ||
         ((*bufc)=='u')
       )
    {
      if ((*ptocopy)<bufc) // flush the buffer
        fwrite((*ptocopy),1,bufc-(*ptocopy),outputfile);
      (*ptocopy)=bufc+1; // ignore the current char

      // save the content
      leninpar[(*pleninpar_i)++]=(*bufc);

      (*bufc)='\0';

      // avoid overflow
      if ((*pleninpar_i)>=(PARMAXLEN-1))
      {
        leninpar[PARMAXLEN-1]='\0';
        fprintf(outputfile,"(%s",leninpar);
        (*pleninpar_i) = -1; // reset parenthesis
      }
    }
    else if ((*bufc)==')')
    {
      // end of the parenthesis
      if ((*ptocopy)<bufc) // flush the buffer
        fwrite((*ptocopy),1,bufc-(*ptocopy),outputfile);
      (*ptocopy)=bufc+1; // ignore the current char

      (*bufc)='\0';

      leninpar[*pleninpar_i]='\0';

      if (sscanf(leninpar,"%u:%u:%u:%c",&right,&left,&lenght,&sign)==4)
      {
        // pattern reading success
        if (hvexhumanreadablelen>0)
        {
          fprintf(outputfile,"(%u:%u)",right,left);
        }
        // else (hvexhumanreadablelen<0) just ignore them
      }
      else
      {
        // no fit failed
        fprintf(outputfile,"(%s)",leninpar);
      }
      (*pleninpar_i) = -1; // reset parenthesis
    }
    else
    {
      // no fit pattern
      leninpar[*pleninpar_i]='\0';
      fprintf(outputfile,"(%s",leninpar);
      (*pleninpar_i) = -1; // reset parenthesis
    }
  }
}

int filter_oneline_hvex_private(FILE *outputfile,t_filterdata *filterdata,t_graphvizopt *opt,int htmllabel)
{
  int i,len;
  char buf[BUFFERLEN];
  char *tocopy,*bufc;

  char leninpar[PARMAXLEN];
  int leninpar_i = -1;

  if (!filter_before(filterdata))
    return 0;

  while ((len=fread(buf,1,BUFFERLEN,filterdata->input))>0)
  {
    tocopy = &(buf[0]);
    for (i=0;i<len;i++)
    {
      bufc = &(buf[i]);

      ignore(bufc,"\n",&tocopy,outputfile);

      if (opt->hvexhumanreadablelen)
        updatelengthdisplaying(bufc,&tocopy,outputfile,opt->hvexhumanreadablelen,leninpar,&leninpar_i);

      if (htmllabel)
        htmlescape(bufc,&tocopy,outputfile);
      else
        ignore(bufc,"\"",&tocopy,outputfile);
    }
    if (tocopy<(buf+len)) // flush the end of the buffer
      fwrite(tocopy,1,buf+len-tocopy,outputfile);
  }

  if (!filter_after(filterdata))
    return 0;
  if (len==0)
    return 1; // normal EOF
  else
  {
    printf("Error: filter_actions: read filter input failed\n");
    return 0; // read error
  }
}

int filter_actions(FILE *outputfile,t_filterdata *filterdata,t_graphvizopt *opt)
{
  // with html escaping
  return filter_oneline_hvex_private(outputfile,filterdata,opt,1);
}

int filter_oneline_hvex(FILE *outputfile,t_filterdata *filterdata,t_graphvizopt *opt)
{
  return filter_oneline_hvex_private(outputfile,filterdata,opt,0);
}

int filter_dotattribut(FILE *outputfile,t_filterdata *filterdata,t_graphvizopt *opt)
{
  int i,len;
  char buf[BUFFERLEN];
  char *tocopy,*bufc;

  if (!filter_before(filterdata))
    return 0;

  while ((len=fread(buf,1,BUFFERLEN,filterdata->input))>0)
  {
    tocopy = &(buf[0]);
    for (i=0;i<len;i++)
    {
      bufc = &(buf[i]);

      ignore(bufc,"\n",&tocopy,outputfile);
      ignore(bufc,"\"",&tocopy,outputfile);
    }
    if (tocopy<(buf+len)) // flush the end of the buffer
      fwrite(tocopy,1,buf+len-tocopy,outputfile);
  }

  if (!filter_after(filterdata))
    return 0;
  if (len==0)
    return 1; // normal EOF
  else
  {
    printf("Error: filter_actions: read filter input failed\n");
    return 0; // read error
  }
}

