#ifndef I_GRAPHVIZ
#define I_GRAPHVIZ

typedef struct struct_t_graphvizopt {
  int commentdebug;
  int printactions;         // 1 (default) -> verbose print of actions in states
                            //
                            // 0 -> no print. Just enumerate actions in states.

  int hvexprintbracket;     // 0 -> print bracket, default: 1 -> no bracket
                            //      it is to represent cast, I think.

  int hvexhumanreadablelen; // -1 -> no variable or litteral length (and signess) in parenthesis
                            // 
                            // 0  -> print complete variable and litteral length (and signess) in parenthesis
                            //       (63:0:64:u)
                            // 1  -> print only range (compressed) of variable and litteral in square brackets
                            //       [63:0]
                            // default: 1
  int colorwhat;            // 0 -> nothing
                            // 1 -> premap  delay
                            // 2 -> postmap delay
  // internal:
  //   - color delay
  double delaymax,delaymin;
  int    nbcolor;
  //   - fsm state <-> hier node
  netlist_fsm_t* comp_fsm;
  map_netlist_data_t* map_data;

} t_graphvizopt;

// return 0 on error
int print_graphviz(FILE* file,implem_t* Implem,t_graphvizopt *opt);

// for filter.c
void indent(FILE* file,int level);

#endif
