
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>

#include "../../augh/auto/auto.h"
#include "../../augh/auto/command.h"
#include "../../augh/plugins/plugins.h"

#include "graphviz.h"

#define PLUGIN_NAME     "tree2graphviz"
#define PLUGIN_VERSION  "0.08"

// TODO:
//   * feature_test_macros for strtok_r
//   * allow space in filename ?


void usage(plugin_t* plugin) {
  printf("This is plugin '%s' version %s.\n", plugin->name, plugin->version);
  printf("\n"
         "usage: %s [--help] [--out outputfile.dot]\n"
         "\n"
         "  --help             Display this help.\n"
         "\n"
         "  --out filename     Specify an output filename\n"
         "                     instead of stdout.\n"
         "\n"
         "display configuration:\n"
         "\n"
         "  --printactions     detail state content (default)\n"
         "  --noprintactions   no state content\n"
         "\n"
         "  --printbrackets    detail cast (default)\n"
         "  --noprintbrackets  no cast detail\n"
         "\n"
         "  --verbosebits      detail about selected bits (default)\n"
         "  --compressbits     a more compressed version\n"
         "  --nobits           no display bits\n"
         "\n"
         "  --compact          select display configuration to plot\n"
         "                     a compact graph\n"
         "  --color [premap|postmap]\n"
         "                     coloring actions respecting to their delays.\n"
         "\n",plugin->name);
}

static int plugin_command(plugin_t* plugin, implem_t* Implem, command_t* cmd_data) {
  char *filename=NULL;
  char *colorwhat=NULL;
  FILE *file=NULL;
  int   ret;
  t_graphvizopt opt;

  bzero(&opt,sizeof(t_graphvizopt));
  opt.printactions=1;
  opt.hvexprintbracket=1;
  opt.hvexhumanreadablelen=0;
  opt.colorwhat=0;

  do {

    const char* cmd = Command_PopParam(cmd_data);
    if(cmd==NULL) {
      break;
    }

    if (!strcasecmp(cmd, "--help")) {
      usage(plugin);
      return 0;
    } else if(!strcasecmp(cmd, "--out")) {
      filename=Command_PopParam_stralloc(cmd_data);
      if (!filename) {
        printf("Error [plugin '%s'] : --out requires a filename argument\n\n",plugin->name);
        usage(plugin);
        return -1;
      }
    } else if(!strcasecmp(cmd, "--printactions")) {
      opt.printactions=1;
    } else if(!strcasecmp(cmd, "--noprintactions")) {
      opt.printactions=0;
    } else if(!strcasecmp(cmd, "--printbrackets")) {
      opt.hvexprintbracket=1;
    } else if(!strcasecmp(cmd, "--noprintbrackets")) {
      opt.hvexprintbracket=0;
    } else if(!strcasecmp(cmd, "--verbosebits")) {
      opt.hvexhumanreadablelen=  0;
    } else if(!strcasecmp(cmd, "--compressbits")) {
      opt.hvexhumanreadablelen=  1;
    } else if(!strcasecmp(cmd, "--nobits")) {
      opt.hvexhumanreadablelen= -1;
    } else if(!strcasecmp(cmd, "--compact")) {
      opt.printactions=1;
      opt.hvexprintbracket=0;
      opt.hvexhumanreadablelen=  1;
    } else if(!strcasecmp(cmd, "--color")) {
      colorwhat=Command_PopParam_stralloc(cmd_data);
      if     (!strcasecmp(colorwhat, "premap"))
        opt.colorwhat=1;
      else if(!strcasecmp(colorwhat, "postmap"))
        opt.colorwhat=2;
      else {
        printf("Error [plugin '%s'] : --color argument can be \"premap\" or \"postmap\" but not \"%s\".\n\n", plugin->name, colorwhat);
        usage(plugin);
        return -1;
      }
    } else {
      printf("Error [plugin '%s'] : unknown argument \"%s\".\n\n", plugin->name, cmd);
      usage(plugin);
      return -1;
    }

  } while(1);

  if (!filename)
    file=stdout;
  else
  {
    file=fopen(filename,"w");
    if (!file) {
      printf("Error [plugin '%s'] : open file failed \"%s\".\n",plugin->name,filename);
      return -1;
    }
  }

  if (!print_graphviz(file,Implem,&opt)) {
    printf("Error [plugin '%s'] : generate the graphviz graph failed.\n",plugin->name);
    ret=-1; // fail
  }
  else
    ret=0; // success

  if (filename)
    fclose(file);
  return ret;
}


int plugin_init(plugin_t* plugin) {
	plugin->api_version = namealloc(AUGH_PLUGIN_API_VERSION);
	plugin->name        = namealloc(PLUGIN_NAME);
	plugin->version     = namealloc(PLUGIN_VERSION);

  int z = Plugin_Declare(plugin);
  if(z!=0) return z;

	plugin->callbacks.interpreter = plugin_command;

  return 0;
}

