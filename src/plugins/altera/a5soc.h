

#ifndef _A5SOC_H_
#define _A5SOC_H_


// The clock sources are not meant to be used by the user.
// These declarations are to prevent the user to re-declare them without knowing their use.
// Note: For now, differential clocks are not declared because the AUGH functionality is not fully ready yet

bool a5soc_clk0p;
bool a5soc_clk1p;
bool a5soc_clk2p;
bool a5soc_clk3p;
bool a5soc_clk10p;

//bool a5soc_clk4p;
//bool a5soc_clk4p_p;
//bool a5soc_clk4p_n;


// Miscellaneous I/O
uint4_t a5soc_buttons;
uint4_t a5soc_dipsw;
uint4_t a5soc_leds;


#endif  // _A5SOC_H_

