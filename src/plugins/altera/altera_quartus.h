
#ifndef _ALTERA_QUARTUS_H_
#define _ALTERA_QUARTUS_H_


#define QUARTUS_PRJ_DEFAULT "quartusprj"

int Quartus_Gen_Project(implem_t* Implem, char* dirname);
int Quartus_Launch(implem_t* Implem);

void Quartus_Techno_SetCB(techno_t* techno);


#endif  // _ALTERA_QUARTUS_H_

