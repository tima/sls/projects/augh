
#ifndef _ALTERA_LOAD_H_
#define _ALTERA_LOAD_H_


board_t* Altera_BuildBoard_ArriaVSoc();

void Altera_Techno_WriteJSON(plugin_t* plugin);
void Altera_Techno_ReadJSON(plugin_t* plugin);

fpga_model_t* Altera_BuildChipModel(const char* strmodel, bool verbose);
fpga_model_t* Altera_BuildChipModel_silent(const char* strmodel);


#endif  // _ALTERA_LOAD_H_

