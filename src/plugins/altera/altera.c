
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

#include "../../augh/auto/auto.h"
#include "../../augh/plugins/plugins.h"
#include "../../augh/netlist/netlist.h"
#include "../../augh/auto/command.h"

#include "altera_techno.h"
#include "altera_load.h"
#include "altera_quartus.h"

#define PLUGIN_NAME     "altera"
#define PLUGIN_VERSION  "0.1"



static int plugin_command(plugin_t* plugin, implem_t* Implem, command_t* cmd_data) {
	const char* cmd = Command_PopParam(cmd_data);
	if(cmd==NULL) {
		printf("Error [plugin '%s'] : No command received. Try 'help'.\n", plugin->name);
		return -1;
	}

	// Many commands use this structure, so get it only once
	altera_techdata_t* techdata = Implem->synth_target->techno_data;

	if(strcmp(cmd, "help")==0) {
		printf("This is plugin '%s' version %s.\n", plugin->name, plugin->version);
		printfm(
			"Commands:\n"
			"  help             Display this help.\n"
			"  tryload-code <code>\n"
			"                   Try to load an FPGA model from a given code. Verbose.\n"
			"  set-prj-dir <dir>\n"
			"                   Set the name of the directory where to generate back-end synthesis project files.\n"
			"  gen-quartus-project [<dir>]\n"
			"                   Generate Quartus project files in a local directory.\n"
			"                   Default directory: %s.\n", QUARTUS_PRJ_DEFAULT,
			NULL
		);
		return 0;
	}

	if(strcmp(cmd, "tryload-code")==0) {
		char* code = Command_PopParam_stralloc(cmd_data);
		if(code==NULL) {
			printf("Error [plugin '%s'] : Missing parameter for command '%s'.\n", plugin->name, cmd);
			return -1;
		}
		fpga_model_t* fpga = Altera_BuildChipModel(code, true);
		if(fpga==NULL) printf("Unknown chip code.\n");
		else printf("This is valid chip code.\n");
	}

	else if(strcmp(cmd, "set-prj-dir")==0) {
		char* path = Command_PopParam_stralloc(cmd_data);
		if(path==NULL) {
			printf("Error [plugin '%s'] : Missing parameter for command '%s'.\n", plugin->name, cmd);
			return -1;
		}
		techdata->synth_prj_dir = path;
	}

	else if(strcmp(cmd, "gen-quartus-project")==0) {
		char* path = Command_PopParam_stralloc(cmd_data);
		return Quartus_Gen_Project(Implem, path);
	}

	else {
		printf("Error [plugin '%s'] : Unknown command '%s'.\n", plugin->name, cmd);
		return -1;
	}

	return 0;
}


int plugin_init(plugin_t* plugin) {
	plugin->api_version = namealloc(AUGH_PLUGIN_API_VERSION);
	plugin->name        = namealloc(PLUGIN_NAME);
	plugin->version     = namealloc(PLUGIN_VERSION);

	int z = Plugin_Declare(plugin);
	if(z!=0) return z;

	plugin->callbacks.interpreter = plugin_command;

	Altera_Techno_Init(plugin);

	return 0;
}

