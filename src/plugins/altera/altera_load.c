
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <stdbool.h>
#include <ctype.h>

#include "../../augh/indent.h"
#include "../../augh/auto/auto.h"
#include "../../augh/auto/techno.h"
#include "../../augh/netlist/netlist.h"
#include "../../augh/netlist/netlist_comps.h"
#include "../../augh/netlist/netlist_access.h"
#include "../../augh/plugins/plugins_techno.h"

#include "altera_techno.h"
#include "altera_quartus.h"



//======================================================================
// Board descriptions
//======================================================================

board_t* Altera_BuildBoard_ArriaVSoc() {
	board_t* board = Board_New();

	board->name = namealloc("a5soc");
	board->description = stralloc("Arria V SoC Development Board. The FPGA is 5ASTFD5K3F40I3.");

	// Add a board_fpga_t structure
	board_fpga_t* board_fpga = Board_FPGA_New();
	board_fpga->name = namealloc("fpga");
	board_fpga->model = Techno_GetFPGA(namealloc("5ASTFD5K3F40I3"));
	assert(board_fpga->model!=NULL);
	board_fpga->speed = Techno_FPGA_GetSpeed(board_fpga->model, namealloc("3"));
	assert(board_fpga->speed!=NULL);

	board_fpga->board = board;
	avl_pp_add_overwrite(&board->fpgas, board_fpga->name, board_fpga);

	// The JTAG id (assuming the first is 1)
	board_fpga->jtag_id = 2;

	// Add board connectivity

	netlist_comp_t* comp = board_fpga->connect = Netlist_Comp_Top_New(board_fpga->name);
	chain_list* pinsattr;

	// The differential 100 MHz programmable clock REFCLKR3 (alias REFCLK_QR2 on schematics, part X2 on the board, chip reference Si570)
	// FIXME This arrives in a transceiver of the Arria V SoC, how to use that?

	// The differential 148.5 MHz programmable clock REFCLKL5 (alias CLK_148 on schematics, part X3 on the board, chip reference Si571)
	// FIXME This arrives in a transceiver of the Arria V SoC, how to use that?

	// The programmable 100 MHz clock CLK0P (alias CLK_BOT1 on schematics, part U35 on the board, chip reference Si5338A)

	netlist_port_t* port_clk0p = Netlist_Comp_AddPortAccess_Clock(comp, namealloc("a5soc_clk0p"));
	Netlist_Access_Clock_SetFrequency(port_clk0p->access, 100e6);
	pinsattr = ListPin_MakeListAttr("pin AP34", "io_standard 1.5 V", NULL);
	avl_pp_add_overwrite(&board_fpga->port2pin, port_clk0p, pinsattr);

	// Set as default for the FPGA
	board_fpga->default_clock = port_clk0p->access;

	// The 156.25 MHz programmable clock CLK1P (alias CLK_TOP1 on schematics, part U35 on the board, chip reference Si5338A)

	netlist_port_t* port_clk1p = Netlist_Comp_AddPortAccess_Clock(comp, namealloc("a5soc_clk1p"));
	Netlist_Access_Clock_SetFrequency(port_clk1p->access, 156.25e6);
	pinsattr = ListPin_MakeListAttr("pin AK34", "io_standard 1.5 V", NULL);
	avl_pp_add_overwrite(&board_fpga->port2pin, port_clk1p, pinsattr);

	// The 25 MHz clock CLK2P (alias CLK_ENET_FPGA_PHY on schematics, part U42 on the board, chip reference Si5335)

	netlist_port_t* port_clk2p = Netlist_Comp_AddPortAccess_Clock(comp, namealloc("a5soc_clk2p"));
	Netlist_Access_Clock_SetFrequency(port_clk2p->access, 25e6);
	pinsattr = ListPin_MakeListAttr("pin AM33", "io_standard 1.5 V", NULL);
	avl_pp_add_overwrite(&board_fpga->port2pin, port_clk2p, pinsattr);

	// The 50 MHz clock CLK3P (alias CLK_50M_FPGA on schematics, part X4 on the board, chip reference SL 18860C)

	netlist_port_t* port_clk3p = Netlist_Comp_AddPortAccess_Clock(comp, namealloc("a5soc_clk3p"));
	Netlist_Access_Clock_SetFrequency(port_clk3p->access, 50e6);
	pinsattr = ListPin_MakeListAttr("pin AU32", "io_standard 1.5 V", NULL);
	avl_pp_add_overwrite(&board_fpga->port2pin, port_clk3p, pinsattr);

	// The differential 125 MHz clock CLK4P (alias CLK_ENET_FPGA on schematics, part X5 on the board)

	netlist_access_t* access_clk4p = Netlist_Comp_AddAccess_DiffClock(comp, namealloc("a5soc_clk4p"));
	Netlist_Access_DiffClock_SetFrequency(access_clk4p, 125e6);
	netlist_access_diffclock_t* access_clk4p_data = access_clk4p->data;
	pinsattr = ListPin_MakeListAttr("pin AV19", "io_standard 2.5 V", NULL);
	avl_pp_add_overwrite(&board_fpga->port2pin, access_clk4p_data->port_p, pinsattr);
	pinsattr = ListPin_MakeListAttr("pin AU19", "io_standard 2.5 V", NULL);
	avl_pp_add_overwrite(&board_fpga->port2pin, access_clk4p_data->port_n, pinsattr);

	// The 100 MHz clock CLK10P (alias CLK_100M_FPGA on schematics, part U42 on the board, chip reference Si5335)

	netlist_port_t* port_clk10p = Netlist_Comp_AddPortAccess_Clock(comp, namealloc("a5soc_clk10p"));
	Netlist_Access_Clock_SetFrequency(port_clk10p->access, 25e6);
	pinsattr = ListPin_MakeListAttr("pin AL20", "io_standard 2.5 V", NULL);
	avl_pp_add_overwrite(&board_fpga->port2pin, port_clk10p, pinsattr);

	// The push buttons (note: active low)

	netlist_port_t* port_buttons = Netlist_Comp_AddPortAccess_WireIn(comp, namealloc("a5soc_buttons"), 4);

	// Push buttons: here processed from 0 to 3 (board reference S4, S3, S2, S1, respectively)
	pinsattr = NULL;
	pinsattr = ListPin_AddListAttr(pinsattr, "pin AT23", "io_standard 1.5 V", NULL);
	pinsattr = ListPin_AddListAttr(pinsattr, "pin AP24", "io_standard 1.5 V", NULL);
	pinsattr = ListPin_AddListAttr(pinsattr, "pin AW24", "io_standard 1.5 V", NULL);
	pinsattr = ListPin_AddListAttr(pinsattr, "pin AW23", "io_standard 1.5 V", NULL);
	avl_pp_add_overwrite(&board_fpga->port2pin, port_buttons, reverse(pinsattr));

	// The DIP Switches (note: active low)

	netlist_port_t* port_dipsw = Netlist_Comp_AddPortAccess_WireIn(comp, namealloc("a5soc_dipsw"), 4);

	// DIP Switches: here processed from 0 to 3 (board reference 4, 6, 7, 8, respectively)
	pinsattr = NULL;
	pinsattr = ListPin_AddListAttr(pinsattr, "pin AL24", "io_standard 1.5 V", NULL);
	pinsattr = ListPin_AddListAttr(pinsattr, "pin AF24", "io_standard 1.5 V", NULL);
	pinsattr = ListPin_AddListAttr(pinsattr, "pin AE24", "io_standard 1.5 V", NULL);
	pinsattr = ListPin_AddListAttr(pinsattr, "pin AU23", "io_standard 1.5 V", NULL);
	avl_pp_add_overwrite(&board_fpga->port2pin, port_dipsw, reverse(pinsattr));

	// The LEDs 4 bits (note: active low)

	netlist_port_t* port_leds = Netlist_Comp_AddPortAccess_WireOut(comp, namealloc("a5soc_leds"), 4);

	// LEDs: here processed from 0 to 3 (board reference D12, D11, D10, D9, respectively)
	pinsattr = NULL;
	pinsattr = ListPin_AddListAttr(pinsattr, "pin AH24", "io_standard 1.5 V", NULL);
	pinsattr = ListPin_AddListAttr(pinsattr, "pin AU24", "io_standard 1.5 V", NULL);
	pinsattr = ListPin_AddListAttr(pinsattr, "pin AT24", "io_standard 1.5 V", NULL);
	pinsattr = ListPin_AddListAttr(pinsattr, "pin AD24", "io_standard 1.5 V", NULL);
	avl_pp_add_overwrite(&board_fpga->port2pin, port_leds, reverse(pinsattr));

	// Finally, return the board
	return board;
}



//======================================================================
// Reading techno data from JSON files
//======================================================================

// Note: JSON format: http://www.json.org/
// Note: jansson API: http://www.digip.org/jansson/

#include <jansson.h>

// Fix for systems that have jansson 2.4 instead of 2.5+
#ifndef json_array_foreach
#define json_array_foreach(array, index, value) \
	for(index = 0; index < json_array_size(array) && (value = json_array_get(array, index)); index++)
#endif

// Functions to write JSON files

static void fdump_chip(FILE* F, fpga_model_t* chip) {
	fprintf(F, "\"%s\": {\n", chip->name);

	if(chip->description!=NULL) {
		fprintf(F, "	\"desc\": \"%s\"\n", chip->description);
	}

	fprintf(F, "	\"speeds\": [");
	foreach(chip->timings, scan) {
		techno_timing_t* timing = scan->DATA;
		if(scan!=chip->timings) fprintf(F, ", ");
		fprintf(F, "\"%s\"", timing->name);
	}
	fprintf(F, "],\n");

	fprintf(F, "	\"packages\": [");
	foreach(chip->packages, scan) {
		fpga_pkg_model_t* pkg = scan->DATA;
		if(scan!=chip->packages) fprintf(F, ", ");
		fprintf(F, "\"%s\"", pkg->name);
	}
	fprintf(F, "],\n");

	altera_res_t* xres = chip->resources;
	fprintf(F, "	\"resources\": {\n");
	fprintf(F, "		\"alm\"    : %u,\n", xres->alm);
	fprintf(F, "		\"reg\"    : %u,\n", xres->reg);
	fprintf(F, "		\"mlab\"   : %u,\n", xres->mlab);
	fprintf(F, "		\"m10k\"   : %u,\n", xres->m10k);
	fprintf(F, "		\"m20k\"   : %u,\n", xres->m20k);
	fprintf(F, "		\"dsp\"    : %u,\n", xres->dsp);
	fprintf(F, "		\"pll\"    : %u,\n", xres->pll);
	fprintf(F, "		\"trans\"  : %u\n",  xres->trans);
	fprintf(F, "	}\n");

	fprintf(F, "}\n");
}

void Altera_Techno_WriteJSON(plugin_t* plugin) {

	// Read all chips declared by the current plugin
	// Dump their JSON representation in stdout

	avl_pp_foreach(&plugin->fpgas, scan) {
		fpga_model_t* chip = scan->data;
		fdump_chip(stdout, chip);
	}

	// FIXME for debug
	exit(EXIT_SUCCESS);
}

// Functions to read JSON files

static bool parse_json_verbose = false;

static void parse_timing(plugin_t* plugin, techno_timing_t* timing, json_t *json) {
	if(json_is_object(json)==false) {
		printf("Error: Wrong JSON type for timing '%s', techno '%s'\n", timing->name, timing->techno->name);
		exit(EXIT_FAILURE);
	}

	if(parse_json_verbose==true) {
		printf("Info: Parsing timing '%s', 'techno '%s'\n", timing->name, timing->techno->name);
	}

	altera_timing_t* xtiming = calloc(1, sizeof(*xtiming));
	timing->data = xtiming;

	// Parse all elements of the speed grade
	const char *key;
	json_t *json_value;
	json_object_foreach(json, key, json_value) {

		if(strcmp(key, "desc")==0) {
			if(json_is_string(json_value)==false) {
				printf("Error: Wrong JSON type for description of timing '%s', techno '%s'\n", timing->name, timing->techno->name);
				exit(EXIT_FAILURE);
			}
			timing->description = stralloc(json_string_value(json_value));
			continue;
		}

		// Here the value can only be a number (a double, in ns)
		if(json_is_number(json_value)==false) {
			printf("Error: Wrong JSON type for timing elements '%s' of timing '%s', techno '%s'\n", key, timing->name, timing->techno->name);
			exit(EXIT_FAILURE);
		}
		double value_dbl = json_number_value(json_value);

		// Parse the different possible values

		if     (strcmp(key, "alm_delay")==0)        xtiming->alm_delay      = value_dbl;
		else if(strcmp(key, "wire_adj_delay")==0)   xtiming->wire_adj_delay = value_dbl;
		else if(strcmp(key, "wire_delay")==0)       xtiming->wire_delay     = value_dbl;

		else if(strcmp(key, "carry_lut2do")==0)     xtiming->carry_lut2do = value_dbl;
		else if(strcmp(key, "carry_lut2co")==0)     xtiming->carry_lut2co = value_dbl;
		else if(strcmp(key, "carry_ci2do")==0)      xtiming->carry_ci2do  = value_dbl;
		else if(strcmp(key, "carry_ci2co")==0)      xtiming->carry_ci2co  = value_dbl;

		else if(strcmp(key, "ff_bef_wd")==0)        xtiming->reg_bef_wd = value_dbl;
		else if(strcmp(key, "ff_bef_we")==0)        xtiming->reg_bef_we = value_dbl;
		else if(strcmp(key, "ff_aft_rd")==0)        xtiming->reg_aft_rd = value_dbl;

		else if(strcmp(key, "mlab_bef_wd")==0)      xtiming->mlab_bef_wd = value_dbl;
		else if(strcmp(key, "mlab_bef_wa")==0)      xtiming->mlab_bef_wa = value_dbl;
		else if(strcmp(key, "mlab_bef_we")==0)      xtiming->mlab_bef_we = value_dbl;
		else if(strcmp(key, "mlab_aft_rd")==0)      xtiming->mlab_aft_rd = value_dbl;
		else if(strcmp(key, "mlab_read")==0)        xtiming->mlab_read   = value_dbl;

		else if(strcmp(key, "m10k_bef_wd")==0)      xtiming->m10k_bef_wd   = value_dbl;
		else if(strcmp(key, "m10k_bef_wa")==0)      xtiming->m10k_bef_wa   = value_dbl;
		else if(strcmp(key, "m10k_bef_we")==0)      xtiming->m10k_bef_we   = value_dbl;
		else if(strcmp(key, "m10k_bef_ra")==0)      xtiming->m10k_bef_ra   = value_dbl;
		else if(strcmp(key, "m10k_bef_re")==0)      xtiming->m10k_bef_re   = value_dbl;
		else if(strcmp(key, "m10k_aft_read")==0)    xtiming->m10k_aft_read = value_dbl;

		else if(strcmp(key, "dsp_mul_comb")==0)     xtiming->dsp_mul_comb = value_dbl;

		else {
			printf("Error: Unknown JSON key '%s' in timing '%s', techno '%s'\n", key, timing->name, timing->techno->name);
			exit(EXIT_FAILURE);
		}

	}  // Scan all objects

}  // Parse timing

static void parse_techno(plugin_t* plugin, techno_t* techno, json_t *json) {
	if(json_is_object(json)==false) {
		printf("Error: Wrong JSON type for techno '%s'\n", techno->name);
		exit(EXIT_FAILURE);
	}

	if(parse_json_verbose==true) {
		printf("Info: Parsing techno '%s'\n", techno->name);
	}

	// Parse all elements of the techno
	json_t *json_value;

	// The description
	json_value = json_object_get(json, "desc");
	if(json_value!=NULL) {
		if(json_is_string(json_value)==false) {
			printf("Error: Wrong JSON type for description of techno '%s'\n", techno->name);
			exit(EXIT_FAILURE);
		}
		techno->description = stralloc(json_string_value(json_value));
	}

	// The speeds
	json_value = json_object_get(json, "speeds");
	if(json_value!=NULL) {
		if(json_is_object(json_value)==false) {
			printf("Error: Wrong JSON type for speeds of techno '%s'\n", techno->name);
			exit(EXIT_FAILURE);
		}
		const char *key;
		json_t *json_timing;
		json_object_foreach(json_value, key, json_timing) {
			techno_timing_t* timing = Techno_Timing_New();
			timing->name = namealloc(key);
			Altera_Techno_LinkTiming(techno, timing);
			parse_timing(plugin, timing, json_timing);
		}
	}

	// Set all callback functions
	Altera_Techno_SetCB(techno);

	// Set appropriate backend control callbacks
	// Note: by default there is no callback set
	Quartus_Techno_SetCB(techno);

	// FIXME here we assume there is no fail
	Plugin_DeclTechno(plugin, techno);
}

// Remove comments
static void remove_comments(json_t* json) {
	if(json_is_object(json)==true) {
		json_object_del(json, "#");
		const char *key;
		json_t *json_value;
		json_object_foreach(json, key, json_value) remove_comments(json_value);
	}
	else if(json_is_array(json)==true) {
		size_t index;
		json_t *json_value;
		json_array_foreach(json, index, json_value) remove_comments(json_value);
	}
}

// FIXME This should be placed into a data structure specific to the current plugin
static json_t* shJsonFile = NULL;
static plugin_t* shAlteraPlugin = NULL;

// Main function
void Altera_Techno_ReadJSON(plugin_t* plugin) {

	char* path = GetExePath_CutBin();
	char buffer[strlen(path)+40];
	sprintf(buffer, "%s/share/augh/altera/technos.json", path);

	if(parse_json_verbose==true) {
		printf("Info: Reading JSON file: %s\n", buffer);
	}

	FILE* F = fopen(buffer, "rb");
	if(F==NULL) {
		fprintf(stderr, "Error: Couldn't open the file %s\n", buffer);
		exit(EXIT_FAILURE);
	}

	json_error_t json_error;
	json_t* json_root = json_loadf(F, 0, &json_error);

	fclose(F);

	if(json_root==NULL) {
		fprintf(stderr, "JSON Error: line %d, message: %s\n", json_error.line, json_error.text);
		exit(EXIT_FAILURE);
	}
	if(json_is_object(json_root)==false) {
		fprintf(stderr, "JSON Error: The file contents is not of type object.\n");
		exit(EXIT_FAILURE);
	}

	// Remove comments from JSON data
	remove_comments(json_root);

	// Read the JSON data tree

	const char *key;
	json_t *json_value;
	json_object_foreach(json_root, key, json_value) {
		techno_t* techno = Techno_New();
		techno->name = namealloc(key);
		parse_techno(plugin, techno, json_value);
	}

	// Save the entire Json tree into a shared variable
	if(shJsonFile!=NULL) json_decref(shJsonFile);
	shJsonFile = json_root;
	shAlteraPlugin = plugin;

	// Clean
	//json_decref(json_root);
}


// Return 0 if OK
static int parse_json_resource_count(json_t *json_resources, altera_res_t* xres) {
	if(json_is_object(json_resources)==false) {
		errprintf("Error: Wrong JSON type, expected object\n");
		return -1;
	}

	const char *key;
	json_t *json_value;

	json_object_foreach(json_resources, key, json_value) {
		// All values must be integers
		if(json_is_integer(json_value)==false) {
			errprintf("Error: Wrong JSON type for resource '%s', expected integer\n", key);
			return -1;
		}
		unsigned value_u = json_integer_value(json_value);
		if     (strcmp(key, namealloc_alm)==0)      xres->alm   = value_u;
		else if(strcmp(key, namealloc_reg)==0)      xres->reg   = value_u;
		else if(strcmp(key, namealloc_mlab)==0)     xres->mlab  = value_u;
		else if(strcmp(key, namealloc_m10k)==0)     xres->m10k  = value_u;
		else if(strcmp(key, namealloc_m20k)==0)     xres->m20k  = value_u;
		else if(strcmp(key, namealloc_dsp)==0)      xres->dsp   = value_u;
		else if(strcmp(key, namealloc_pll)==0)      xres->pll   = value_u;
		else if(strcmp(key, namealloc_trans)==0)    xres->trans = value_u;
		else if(strcmp(key, namealloc_pciectrl)==0) xres->pciectrl = value_u;
		else if(strcmp(key, namealloc_memctrl)==0)  xres->memctrl  = value_u;
		else {
			errprintf("Error: Unknown resource '%s'\n", key);
			return -1;
		}
	}

	return 0;
}

// Get a chip structure from the full chip code
fpga_model_t* Altera_BuildChipModel(const char* strmodel, bool verbose) {

	// Extract all codes from the model name
	if(strlen(strmodel) < 2) {
		if(verbose==true) printf("Error: Chip code '%s' is too short\n", strmodel);
		return NULL;
	}

	char buf[32];
	int z;

	// Get the family signature

	strncpy(buf, strmodel, 2); buf[2] = 0;
	char* family_signature = stralloc(buf);

	// Get the JSON node that corresponds to that family signature
	// Get the techno

	techno_t* techno = NULL;

	const char *key;
	json_t *json_node;

	json_t *json_techno = NULL;
	json_object_foreach(shJsonFile, key, json_node) {
		char* techno_name = namealloc(key);
		// Get the node for the family code
		json_t* json_family_signature = json_object_get(json_node, "family-signature");
		if(json_family_signature==NULL) continue;
		if(json_is_string(json_family_signature)==false) continue;
		// Get the family code
		char *value = stralloc(json_string_value(json_family_signature));
		if(strcasecmp(family_signature, value)==0) {
			techno = Techno_GetTechno(techno_name);
			json_techno = json_node;
			break;
		}
	}  // Scan all technos from the JSON file

	if(techno==NULL) {
		if(verbose==true) printf("Error: Could not get techno for family signature '%s'.\n", family_signature);
		return NULL;
	}

	if(verbose==true) printf("Techno: '%s'.\n", techno->name);

	// FIXME From here we are parsing stuff special to Arria V techno

	if(strcasecmp(techno->name, "arria-5")!=0) {
		if(verbose==true) printf("Error: Only techno Arria V is handled for now.\n");
		return NULL;
	}

	if(strlen(strmodel) < 14) {
		if(verbose==true) printf("Error: The given code '%s' is too short to fully specify the FPGA.\n", strmodel);
		return NULL;
	}

	// Keep only the first 14 characters
	if(strlen(strmodel) > 14) {
		strncpy(buf, strmodel, 14); buf[14] = 0;
		strmodel = stralloc(buf);
	}

	fpga_model_t* chip = Techno_GetKnownFPGA(namealloc(strmodel));
	if(chip!=NULL) return chip;

	// From here, a new chip model has to be built

	chip = FPGA_Model_New();
	chip->name = namealloc(strmodel);
	chip->techno = techno;

	altera_chipdetails_t* details = Altera_ChipDetails_New();
	chip->resources = details;

	// The family variant

	// Get the code for family variant
	strncpy(buf, strmodel+2, 2); buf[2] = 0;
	char* code_family_variant = stralloc(buf);

	// Get the JSON node for all family variants
	json_t* json_variants = json_object_get(json_techno, "code-variant");
	if(json_variants==NULL) {
		if(verbose==true) printf("Error: Missing family variants from techno '%s'.\n", techno->name);
		goto ERROR_CASE;
	}
	if(json_is_object(json_variants)==false) {
		if(verbose==true) printf("Error: Wrong JSON structure for family variants of techno '%s'.\n", techno->name);
		goto ERROR_CASE;
	}

	// Get the JSON node for the specified family variant
	json_t *json_family_variant = NULL;
	json_object_foreach(json_variants, key, json_node) {
		char* variant_name = namealloc(key);
		if(strcasecmp(variant_name, code_family_variant)==0) {
			json_family_variant = json_node;
			break;
		}
	}
	if(json_family_variant==NULL) {
		if(verbose==true) printf("Error: Unknown family variant '%s' of techno '%s'.\n", code_family_variant, techno->name);
		goto ERROR_CASE;
	}

	if(verbose==true) printf("Family variant: '%s'.\n", code_family_variant);

	// The embedded hard IPs

	// Get the code for embedded hard IPs
	strncpy(buf, strmodel+4, 1); buf[1] = 0;
	char* code_hard_ips = stralloc(buf);

	// Get the JSON node for all code for hard IPs
	json_t* json_all_hard_ips = json_object_get(json_family_variant, "code-hard-ips");
	if(json_all_hard_ips==NULL) {
		if(verbose==true) printf("Error: Missing embedded hard IPs from techno '%s' variant '%s'.\n", techno->name, code_family_variant);
		goto ERROR_CASE;
	}
	if(json_is_object(json_all_hard_ips)==false) {
		if(verbose==true) printf("Error: Wrong JSON structure for embedded hard IPs from techno '%s' variant '%s'.\n", techno->name, code_family_variant);
		goto ERROR_CASE;
	}

	// Get the JSON node for the specified code
	json_t *json_hard_ips = NULL;
	json_object_foreach(json_all_hard_ips, key, json_node) {
		char* code = namealloc(key);
		if(strcasecmp(code, code_hard_ips)==0) {
			json_hard_ips = json_node;
			break;
		}
	}
	if(json_hard_ips==NULL) {
		if(verbose==true) printf("Error: Unknown code for hard IPs '%s' from techno '%s' variant '%s'.\n", code_hard_ips, techno->name, code_family_variant);
		goto ERROR_CASE;
	}

	if(verbose==true) printf("Hard IPs code: '%s'.\n", code_hard_ips);

	// Add the corresponding resources to the FPGA
	z = parse_json_resource_count(json_hard_ips, details->resources);
	if(z!=0) goto ERROR_CASE;

	// The member code

	// Get the member code
	strncpy(buf, strmodel+5, 2); buf[2] = 0;
	char* code_member = stralloc(buf);

	// Get the JSON node for all code members
	json_t* json_all_members = json_object_get(json_family_variant, "code-member");
	if(json_all_members==NULL) {
		if(verbose==true) printf("Error: Missing code members from techno '%s' variant '%s'.\n", techno->name, code_family_variant);
		goto ERROR_CASE;
	}
	if(json_is_object(json_all_members)==false) {
		if(verbose==true) printf("Error: Wrong JSON structure for code members from techno '%s' variant '%s'.\n", techno->name, code_family_variant);
		goto ERROR_CASE;
	}

	// Get the JSON node for the specified code member
	json_t *json_member = NULL;
	json_object_foreach(json_all_members, key, json_node) {
		char* code = namealloc(key);
		if(strcasecmp(code, code_member)==0) {
			json_member = json_node;
			break;
		}
	}
	if(json_member==NULL) {
		if(verbose==true) printf("Error: Unknown code member '%s' from techno '%s' variant '%s'.\n", code_member, techno->name, code_family_variant);
		goto ERROR_CASE;
	}

	if(verbose==true) printf("Member code: '%s'.\n", code_member);

	// Add the corresponding resources to the FPGA
	z = parse_json_resource_count(json_member, details->resources);
	if(z!=0) goto ERROR_CASE;

	// The transceiver count

	// Get the code for transceiver count
	strncpy(buf, strmodel+7, 1); buf[1] = 0;
	char* code_trans_count = stralloc(buf);

	// Get the JSON node for all transceiver counts
	json_t* json_all_trans_count = json_object_get(json_family_variant, "code-transceiver-count");
	if(json_all_trans_count==NULL) {
		if(verbose==true) printf("Error: Missing transceiver counts from techno '%s' variant '%s'.\n", techno->name, code_family_variant);
		goto ERROR_CASE;
	}
	if(json_is_object(json_all_trans_count)==false) {
		if(verbose==true) printf("Error: Wrong JSON structure for transceiver counts from techno '%s' variant '%s'.\n", techno->name, code_family_variant);
		goto ERROR_CASE;
	}

	// Get the JSON node for the specified count code
	json_t *json_trans_count = NULL;
	json_object_foreach(json_all_trans_count, key, json_node) {
		char* code = namealloc(key);
		if(strcasecmp(code, code_trans_count)==0) {
			json_trans_count = json_node;
			break;
		}
	}
	if(json_trans_count==NULL) {
		if(verbose==true) printf("Error: Unknown transceiver count code '%s' from techno '%s' variant '%s'.\n", code_trans_count, techno->name, code_family_variant);
		goto ERROR_CASE;
	}

	if(verbose==true) printf("Transceiver count code: '%s'.\n", code_trans_count);

	// Add the corresponding resources to the FPGA
	z = parse_json_resource_count(json_trans_count, details->resources);
	if(z!=0) goto ERROR_CASE;

	// The transceiver speed grade

	// Get the code for transceiver speed grade
	strncpy(buf, strmodel+8, 1); buf[1] = 0;
	char* code_trans_speed = stralloc(buf);

	// Get the JSON node for all transceiver counts
	json_t* json_all_trans_speed = json_object_get(json_techno, "code-transceiver-speed");
	if(json_all_trans_speed==NULL) {
		if(verbose==true) printf("Error: Missing transceiver speed from techno '%s'.\n", techno->name);
		goto ERROR_CASE;
	}
	if(json_is_object(json_all_trans_speed)==false) {
		if(verbose==true) printf("Error: Wrong JSON structure for transceiver counts from techno '%s'.\n", techno->name);
		goto ERROR_CASE;
	}

	// Get the JSON node for the specified speed code
	json_t *json_trans_speed = NULL;
	json_object_foreach(json_all_trans_speed, key, json_node) {
		char* code = namealloc(key);
		if(strcasecmp(code, code_trans_speed)==0) {
			json_trans_speed = json_node;
			break;
		}
	}
	if(json_trans_speed==NULL) {
		if(verbose==true) printf("Error: Unknown transceiver speed code '%s' from techno '%s'.\n", code_trans_speed, techno->name);
		goto ERROR_CASE;
	}

	// Get the corresponding speed value
	if(json_is_number(json_trans_speed)==false) {
		if(verbose==true) printf("Error: Wrong JSON type for transceiver speed from techno '%s'.\n", techno->name);
		goto ERROR_CASE;
	}

	if(verbose==true) printf("Transceiver speed code: '%s'.\n", code_trans_speed);

	// The package type

	// Get the code for package type
	strncpy(buf, strmodel+9, 1); buf[1] = 0;
	char* code_package_type = stralloc(buf);

	// Get the JSON node for all transceiver counts
	json_t* json_all_package_types = json_object_get(json_techno, "code-package-type");
	if(json_all_package_types==NULL) {
		if(verbose==true) printf("Error: Missing package types from techno '%s'.\n", techno->name);
		goto ERROR_CASE;
	}
	if(json_is_object(json_all_package_types)==false) {
		if(verbose==true) printf("Error: Wrong JSON structure for package types from techno '%s'.\n", techno->name);
		goto ERROR_CASE;
	}

	// Simply ensure the package type exists
	json_t *json_package_type = NULL;
	json_object_foreach(json_all_package_types, key, json_node) {
		char* code = namealloc(key);
		if(strcasecmp(code, code_package_type)==0) {
			json_package_type = json_node;
			break;
		}
	}
	if(json_package_type==NULL) {
		if(verbose==true) printf("Error: Unknown package type code '%s' from techno '%s'.\n", code_package_type, techno->name);
		goto ERROR_CASE;
	}

	if(verbose==true) printf("Package type: '%s'.\n", code_package_type);

	// The package code

	// Get the code for package
	strncpy(buf, strmodel+10, 2); buf[2] = 0;
	char* code_package = stralloc(buf);

	// Get the JSON node for all transceiver counts
	json_t* json_all_packages = json_object_get(json_techno, "code-package");
	if(json_all_packages==NULL) {
		if(verbose==true) printf("Error: Missing packages from techno '%s'.\n", techno->name);
		goto ERROR_CASE;
	}
	if(json_is_object(json_all_packages)==false) {
		if(verbose==true) printf("Error: Wrong JSON structure for packages from techno '%s'.\n", techno->name);
		goto ERROR_CASE;
	}

	// Simply ensure the package code exists
	json_t *json_package = NULL;
	json_object_foreach(json_all_packages, key, json_node) {
		char* code = namealloc(key);
		if(strcasecmp(code, code_package)==0) {
			json_package = json_node;
			break;
		}
	}
	if(json_package==NULL) {
		if(verbose==true) printf("Error: Unknown package code '%s' from techno '%s'.\n", code_package, techno->name);
		goto ERROR_CASE;
	}

	if(verbose==true) printf("Package code: '%s'.\n", code_package);

	// The temperature code

	// Get the temperature code
	strncpy(buf, strmodel+12, 1); buf[1] = 0;
	char* code_temperature = stralloc(buf);

	// Get the JSON node for all transceiver counts
	json_t* json_all_temps = json_object_get(json_techno, "code-temperature");
	if(json_all_temps==NULL) {
		if(verbose==true) printf("Error: Missing operating temperatures from techno '%s'.\n", techno->name);
		goto ERROR_CASE;
	}
	if(json_is_object(json_all_temps)==false) {
		if(verbose==true) printf("Error: Wrong JSON structure for operating temperatures from techno '%s'.\n", techno->name);
		goto ERROR_CASE;
	}

	// Simply ensure the package code exists
	json_t *json_temperature = NULL;
	json_object_foreach(json_all_temps, key, json_node) {
		char* code = namealloc(key);
		if(strcasecmp(code, code_temperature)==0) {
			json_temperature = json_node;
			break;
		}
	}
	if(json_temperature==NULL) {
		if(verbose==true) printf("Error: Unknown temperature code '%s' from techno '%s'.\n", code_temperature, techno->name);
		goto ERROR_CASE;
	}

	if(verbose==true) printf("Temperature code: '%s'.\n", code_temperature);

	// The speed grade

	// Get the temperature code
	strncpy(buf, strmodel+13, 1); buf[1] = 0;
	char* code_speed = stralloc(buf);

	// Get the speed grade
	techno_timing_t* speed_grade = Techno_GetSpeed(techno, code_speed);
	if(speed_grade==NULL) {
		if(verbose==true) printf("Error: Unknown speed grade '%s' from techno '%s'.\n", code_speed, techno->name);
		goto ERROR_CASE;
	}

	if(verbose==true) printf("Speed grade: '%s'.\n", code_speed);

	// Save all values in the chip model structure

	details->code_family_signature = family_signature;
	details->code_family_variant   = code_family_variant;
	details->code_hard_ips         = code_hard_ips;
	details->code_member           = code_member;
	details->code_trans_count      = code_trans_count;
	details->code_trans_speed      = code_trans_speed;
	details->trans_speed           = json_number_value(json_trans_speed);
	details->code_package_type     = code_package_type;
	details->code_package          = code_package;
	details->code_temperature      = code_temperature;
	details->code_speed            = code_speed;

	chip->timings = addchain(chip->timings, speed_grade);

	if(strcasecmp(code_family_variant, "GZ")==0) {
		details->memblock_is_20k = true;
		details->mlab_does_64x1  = true;
		details->dsp_does_36x36  = true;
	}
	else {
		details->memblock_is_20k = false;
		details->mlab_does_64x1  = false;
		details->dsp_does_36x36  = false;
	}

	// Finally, insert the chip in augh data structures

	// FIXME here we assume there is no fail
	Plugin_DeclFPGA(shAlteraPlugin, chip);

	return chip;

	ERROR_CASE:

	if(details!=NULL) Altera_ChipDetails_Free(details);
	if(chip!=NULL) FPGA_Model_Free(chip);

	return NULL;
}

// The callback
fpga_model_t* Altera_BuildChipModel_silent(const char* strmodel) {
	return Altera_BuildChipModel(strmodel, false);
}


