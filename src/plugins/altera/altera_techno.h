
#ifndef _ALTERA_TECHNO_H_
#define _ALTERA_TECHNO_H_

#include "../../augh/plugins/plugins.h"


//======================================================================
// Structures
//======================================================================

// Structure that describes the timings of a particular speed grade
// Note: Latency unit is ns
typedef struct altera_timing_t {

	double alm_delay;

	double wire_adj_delay;
	double wire_delay;

	double carry_lut2do;
	double carry_lut2co;
	double carry_ci2do;
	double carry_ci2co;

	double reg_bef_wd;     // before clock, write data
	double reg_bef_we;     // before clock, write enable
	double reg_aft_rd;     // after clock front

	double mlab_bef_wd;    // before clock front, write data
	double mlab_bef_wa;    // before clock front, write addres
	double mlab_bef_we;    // before clock front, write enable
	double mlab_aft_rd;    // after clock front, data valid inside mlab
	double mlab_read;      // read address -> output data

	double m10k_bef_wd;    // before clock front, write data
	double m10k_bef_wa;    // before clock front, write addres
	double m10k_bef_we;    // before clock front, write enable
	double m10k_bef_ra;    // before clock front, read addres
	double m10k_bef_re;    // before clock front, read enable
	double m10k_aft_read;  // after clock front, read data

	double dsp_mul_comb;

} altera_timing_t;

typedef struct altera_res_t {
	unsigned alm;
	unsigned reg;
	unsigned mlab;
	unsigned m10k;
	unsigned m20k;
	unsigned dsp;
	unsigned pll;
	unsigned trans;
	unsigned pciectrl;
	unsigned memctrl;
} altera_res_t;

typedef struct altera_chipdetails_t {
	// The resources
	altera_res_t* resources;

	// The type of RAM block in device (default is 10k)
	bool memblock_is_20k;
	// Whether or not MLAB primitives can do 64x1 (default is no)
	bool mlab_does_64x1;
	// Whether or not DSP blocks can do 36x36 when used by pairs
	bool dsp_does_36x36;
	// The real transceiver speed, in GHz
	double trans_speed;

	// Identification of target FPGA for generation of back-end project files
	char* code_family_signature;
	char* code_family_variant;
	char* code_hard_ips;
	char* code_member;
	char* code_trans_count;
	char* code_trans_speed;
	char* code_package_type;
	char* code_package;
	char* code_temperature;
	char* code_speed;

} altera_chipdetails_t;

typedef struct altera_techdata_t {

	// The type of RAM block in device (default is 10k)
	bool memblock_is_20k;
	// Whether or not MLAB primitives can do 64x1 (default is no)
	bool mlab_does_64x1;
	// Whether or not DSP blocks can do 36x36 when used by pairs
	bool dsp_does_36x36;
	// The real transceiver speed, in GHz
	double trans_speed;

	// Control of back-end tools
	bool keep_hier;
	bool use_dsp;
	bool use_bram;

	// Directory for generation of back-end project files
	char* synth_prj_dir;
	// Generation of clock declaration in project files
	// No clock: <0, auto: 0, force gen: >0
	int force_gen_clock;

} altera_techdata_t;


//======================================================================
// Shared variables
//======================================================================

extern char* namealloc_alm;
extern char* namealloc_reg;
extern char* namealloc_mlab;
extern char* namealloc_m10k;
extern char* namealloc_m20k;
extern char* namealloc_dsp;
extern char* namealloc_pll;
extern char* namealloc_trans;
extern char* namealloc_pciectrl;
extern char* namealloc_memctrl;


//======================================================================
// Functions
//======================================================================

altera_res_t* Altera_Res_New();
void Altera_Res_Free(altera_res_t* res);

altera_chipdetails_t* Altera_ChipDetails_New();
void Altera_ChipDetails_Free(altera_chipdetails_t* details);

void Altera_Techno_Init(plugin_t* plugin);

// Utility functions to help build techno structures
void Altera_Techno_SetCB(techno_t* techno);
void Altera_Techno_LinkTiming(techno_t* techno, techno_timing_t* timing);

// Utility functions to describe FPGA board
chain_list* SplitPinAttr(char* str);
chain_list* MakeListAttr(char* attr, ...);
chain_list* ListPin_AddListAttr(chain_list* listpin, char* attr, ...);
chain_list* ListPin_MakeListAttr(char* attr, ...);


#endif  // _ALTERA_TECHNO_H_

